(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 300,
	height: 250,
	fps: 25,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"/pub/eurolink/background.jpg?1479975401859", id:"background"},
		{src:"/pub/eurolink/EuroLink_obicen_300x250_juni_2016_atlas_P_.png?1479975401856", id:"EuroLink_obicen_300x250_juni_2016_atlas_P_"}
	]
};



lib.ssMetadata = [
		{name:"EuroLink_obicen_300x250_juni_2016_atlas_P_", frames: [[604,1456,300,250],[604,1708,260,66],[302,1204,300,600],[604,0,300,600],[302,602,300,600],[604,602,300,600],[0,1204,300,600],[866,1708,22,30],[604,1204,300,250],[302,0,300,600],[0,602,300,600],[0,0,300,600]]}
];


// symbols:



(lib.aparat = function() {
	this.spriteSheet = ss["EuroLink_obicen_300x250_juni_2016_atlas_P_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.background = function() {
	this.initialize(img.background);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,600);


(lib.logo = function() {
	this.spriteSheet = ss["EuroLink_obicen_300x250_juni_2016_atlas_P_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.mleko = function() {
	this.spriteSheet = ss["EuroLink_obicen_300x250_juni_2016_atlas_P_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.naocari = function() {
	this.spriteSheet = ss["EuroLink_obicen_300x250_juni_2016_atlas_P_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.papucaDeva = function() {
	this.spriteSheet = ss["EuroLink_obicen_300x250_juni_2016_atlas_P_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.papucaLeva = function() {
	this.spriteSheet = ss["EuroLink_obicen_300x250_juni_2016_atlas_P_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.pasosh = function() {
	this.spriteSheet = ss["EuroLink_obicen_300x250_juni_2016_atlas_P_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.raka = function() {
	this.spriteSheet = ss["EuroLink_obicen_300x250_juni_2016_atlas_P_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();



(lib.sheshir = function() {
	this.spriteSheet = ss["EuroLink_obicen_300x250_juni_2016_atlas_P_"];
	this.gotoAndStop(8);
}).prototype = p = new cjs.Sprite();



(lib.tabletpngcopy = function() {
	this.spriteSheet = ss["EuroLink_obicen_300x250_juni_2016_atlas_P_"];
	this.gotoAndStop(9);
}).prototype = p = new cjs.Sprite();



(lib.zvezda = function() {
	this.spriteSheet = ss["EuroLink_obicen_300x250_juni_2016_atlas_P_"];
	this.gotoAndStop(10);
}).prototype = p = new cjs.Sprite();



(lib.zvezda2 = function() {
	this.spriteSheet = ss["EuroLink_obicen_300x250_juni_2016_atlas_P_"];
	this.gotoAndStop(11);
}).prototype = p = new cjs.Sprite();



(lib.Tween139 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.logo();
	this.instance.parent = this;
	this.instance.setTransform(-128,-37);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-128,-37,260,66);


(lib.Tween133 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.raka();
	this.instance.parent = this;
	this.instance.setTransform(-14.6,-20,1.333,1.333);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14.6,-20,29.4,40);


(lib.Tween132 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.raka();
	this.instance.parent = this;
	this.instance.setTransform(-14.6,-20,1.333,1.333);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14.6,-20,29.4,40);


(lib.Tween131 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.aparat();
	this.instance.parent = this;
	this.instance.setTransform(-150,-300);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-300,300,250);


(lib.Tween129 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.mleko();
	this.instance.parent = this;
	this.instance.setTransform(-150,-300);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-300,300,600);


(lib.Tween127 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.naocari();
	this.instance.parent = this;
	this.instance.setTransform(-150,-300);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-300,300,600);


(lib.Tween125 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.papucaDeva();
	this.instance.parent = this;
	this.instance.setTransform(-150,-300);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-300,300,600);


(lib.Tween123 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.papucaLeva();
	this.instance.parent = this;
	this.instance.setTransform(-150,-300);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-300,300,600);


(lib.Tween121 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.pasosh();
	this.instance.parent = this;
	this.instance.setTransform(-150,-300);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-300,300,600);


(lib.Tween119 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.sheshir();
	this.instance.parent = this;
	this.instance.setTransform(-150,-300);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-300,300,250);


(lib.Tween117 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.tabletpngcopy();
	this.instance.parent = this;
	this.instance.setTransform(-150,-300);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-300,300,600);


(lib.Tween115 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.zvezda();
	this.instance.parent = this;
	this.instance.setTransform(-150,-300);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-300,300,600);


(lib.Tween113 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.zvezda2();
	this.instance.parent = this;
	this.instance.setTransform(-150,-300);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-300,300,600);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAYCTIgGg2IgmAAIgGA2IgdAAIAhklIAtAAIAiElgAgQA1IAeAAIgOiSg");
	this.shape.setTransform(79.2,20.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgvBMIAAggIAeAAIAAAjQABAdAQAAQAHAAAEgHQAEgIABgSIAAgXQAAgVgGgIQgEgJgKAAIgIAAIAAgoIAKAAQASAAAAgfIAAgQQgBgTgEgIQgEgHgHAAQgQAAgBAdIAAAYIgdAAIAAgWQgBhJAvAAQAYAAAMAUQAMATAAAjIAAAIQAAAugWAOQAWAMAAAwIAAAXQAABKgwAAQgvAAAAhKg");
	this.shape_1.setTransform(67.3,20.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAXCSIgFg0IgmAAIgGA0IgeAAIAikjIAtAAIAiEjgAgQA1IAdAAIgOiSg");
	this.shape_2.setTransform(72.2,59.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIAxAAQAYgBALASQALAQABAiIAAALQgBAtgUAOQAYAMAAAxIAAAYQAABFgxAAgAgQBpIARAAQAQAAABgdIAAgaQAAgUgGgIQgEgHgKAAIgOAAgAgQgaIAMAAQASAAAAgeIAAgRQAAgfgOAAIgQAAg");
	this.shape_3.setTransform(60.3,59.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgpCTIAAgpQAOABAGgFQAHgFACgPIAAgDIgtjhIAgAAIAaCoIAXioIAiAAIgjDaQgGAqgNARQgLAQgZAAIgJAAg");
	this.shape_4.setTransform(48.1,59.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgPCSIAAj6IgiAAIAAgpIBjAAIAAApIgiAAIAAD6g");
	this.shape_5.setTransform(36.7,59.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAXCSIgFg0IgmAAIgFA0IgfAAIAikjIAtAAIAiEjgAgQA1IAdAAIgNiSg");
	this.shape_6.setTransform(25.5,59.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AARCSIAAj6IgjAAIAAD6IggAAIAAkjIBlAAIAAEjg");
	this.shape_7.setTransform(13.1,59.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgjCCQgNgTAAgjIAAiXQAAgkANgTQAMgTAXAAQAXAAANATQANATAAAkIAACXQAAAjgNATQgNAUgXAAQgXAAgMgUgAgQhOIAACdQAAAdAQAAQAQAAAAgdIAAidQAAgdgQAAQgQAAAAAdg");
	this.shape_8.setTransform(-3.6,59.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AASCSIAAh+IgjAAIAAB+IghAAIAAkjIAhAAIAAB+IAjAAIAAh+IAhAAIAAEjg");
	this.shape_9.setTransform(-15.8,59.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AA1CSIgeiCIgIAXIAABrIgeAAIAAhrIgIgXIgdCCIgiAAIAoirIgoh4IAhAAIAmB2IAAh2IAeAAIAAB2IAnh2IAhAAIgoB3IAoCsg");
	this.shape_10.setTransform(-31.1,59.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAWCSIAAjNIgnDNIghAAIAAkjIAdAAIAAC4IAki4IAkAAIAAEjg");
	this.shape_11.setTransform(-46.3,59.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgvCSIAAkjIAvAAQAwgBAABKIAAAlQAABHgwAAIgOAAIAABugAgOgEIAOAAQAPAAAAgcIAAgrQAAgdgPAAIgOAAg");
	this.shape_12.setTransform(-57.8,59.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgpCSIAAkjIBTAAIAAApIgzAAIAAD6g");
	this.shape_13.setTransform(-69.1,59.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgjCCQgMgTAAgjIAAggIAeAAIAAAjQAAAdARAAQAHAAAEgHQAEgIAAgSIAAgXQAAgVgEgIQgGgJgJAAIgIAAIAAgoIAKAAQASAAgBgfIAAgQQAAgTgEgIQgEgHgHAAQgRAAAAAdIAAAYIgeAAIAAgWQABhJAuAAQAXAAANATQAMATAAAkIAAAIQAAAvgXANQAXAMAAAwIAAAXQAAAjgMATQgNAUgXAAQgWAAgNgUg");
	this.shape_14.setTransform(-80.5,59.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgrCSIAAkjIBXAAIAAApIg3AAIAABTIArAAIAAAnIgrAAIAABXIA3AAIAAApg");
	this.shape_15.setTransform(-91.6,59.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIBaAAIAAApIg5AAIAABMIAQAAQAyAAAABHIAAAfQAABIgyAAgAgQBpIAQAAQARgBAAgbIAAglQAAgcgRAAIgQAAg");
	this.shape_16.setTransform(-103.3,59.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgqCSIAAkjIBVAAIAAAfIg0AAIAABcIAgAAIAAAdIggAAIAABsIA0AAIAAAfg");
	this.shape_17.setTransform(104.1,59.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgPCSIAAiPIgkAAIAACPIggAAIAAkjIAgAAIAAB3IAkAAIAAh3IAfAAIAAB3IANAAQAVAAAQAOQAJAJAFAKQAEANAAAWIAAAoQAAAkgMAPQgIAJgKADQgGABgMAAgAAQBzIAMAAQAMAAAFgFQAGgGAAgQIAAgtQAAgXgGgIQgGgJgOAAIgJAAg");
	this.shape_18.setTransform(88.8,59.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgqCSIAAkjIBVAAIAAAfIg0AAIAABcIAgAAIAAAdIggAAIAABsIA0AAIAAAfg");
	this.shape_19.setTransform(47,20.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgPCSIAAiPIgjAAIAACPIgiAAIAAkjIAiAAIAAB3IAjAAIAAh3IAfAAIAAB3IAOAAQAUAAAQAOQAKAJAEAKQAEANABAWIAAAoQgBAkgNAPQgLANgXAAgAAQBzIAMAAQALAAAGgFQAGgGAAgQIAAgtQAAgXgGgIQgGgJgPAAIgIAAg");
	this.shape_20.setTransform(31.7,20.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAYCSIgGg2IgjAAIgGA2IgiAAIAlkjIAoAAIAlEjgAgOA9IAdAAIgPiPIAAAAg");
	this.shape_21.setTransform(15.3,20.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIAxAAQAYAAANAQQALANAAAiIAAAdQAAAWgFAKQgIANgOACIAAACQASAEAHAPQAEAKAAAWIAAAqQAAAbgHALQgLATgYAAgAgQBzIAOAAQAKAAAGgHQADgFAAgPIAAgsQAAgVgEgHQgGgKgOAAIgJAAgAgQgWIANAAQAJAAAFgGQAGgGAAgRIAAgjQgBgQgFgGQgIgGgKAAIgJAAg");
	this.shape_22.setTransform(3.4,20.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("Ag3CRIAAgfQAaAIAMgQQAJgKgFgUIgsjeIAkAAIAXCnIABAAIABgUIATiTIAjAAIgmDsQgFAhgPANQgLALgSAAQgQAAgKgCg");
	this.shape_23.setTransform(-9.4,20.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIAxAAQAbAAAMARQALAOAAAbIAAAyQAAAagLAMQgKAOgUABQgJABgQgDIAACEgAgQgPIAPAAIAHgBQAGgBADgGQACgEAAgQIAAgvQAAgNgFgGQgFgFgIAAIgPAAg");
	this.shape_24.setTransform(-21.5,20.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("Ag3CRIAAgfQAaAIAMgQQAIgJgEgVIgsjeIAkAAIAXCnIABAAIABgUIATiTIAjAAIgmDsQgGAigOAMQgLALgTAAQgQAAgJgCg");
	this.shape_25.setTransform(-34.3,20.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgoCSIAAkjIBRAAIAAAfIgxAAIAAEEg");
	this.shape_26.setTransform(-44.8,20.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AARCSIAAiSIACgVIgjBvIAAA4IghAAIAAkjIAhAAIAACLIgCATIAjhvIAAgvIAhAAIAAEjg");
	this.shape_27.setTransform(-57,20.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AglCDQgMgRAAgmIAAifQAAghAMgPQAOgTAZAAQAYAAAMASQAJAKABASQACAGAAATIAAAhIggAAIAAgsQAAgRgEgGQgFgGgJAAQgGAAgFAGQgEAGgBARIAACuQABAWADAGQADAIAJAAQAKAAAEgIQADgGAAgWIAAgwIAhAAIAAAxQAAAggKAOQgNAUgbAAQgXAAgOgUg");
	this.shape_28.setTransform(-69.5,20.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgmCFQgHgKgCgQIgBgZIAAijIABgZQACgQAHgKQAOgSAYAAQAZAAAOASQAHAKADAQIAAAZIAACjIAAAZQgDAQgHAKQgOASgZAAQgYAAgOgSgAgNhvQgDAFAAASIAACxQAAASADAFQAEAIAJAAQAKAAAEgIQACgEAAgTIAAixQAAgTgCgEQgEgIgKAAQgJAAgEAIg");
	this.shape_29.setTransform(-82.2,20.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgmCFQgIgKgBgQQgBgGAAgTIAAijQAAgTABgGQABgQAIgKQAPgSAXAAQAYAAAPASQAIAKACAQIAAAZIAACjIAAAZQgCAQgIAKQgPASgYAAQgXAAgPgSgAgNhvQgDAFAAASIAACxQAAASADAFQAEAIAJAAQAKAAAEgIQADgFAAgSIAAixQAAgSgDgFQgEgIgKAAQgJAAgEAIg");
	this.shape_30.setTransform(42.2,-19.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AARCSQgFgLAAgKIAAhYQAAgRgGgGQgFgGgIAAIgMAAIAACKIghAAIAAkjIAhAAIAAB8IALAAQAHAAADgGQAEgFAAgIIAAhHQAAgXANgIQAGgDAPAAIAKAAIAAAhIgGABQgHABAAAKIAABAQAAASgQAJIAAACQALADAHAIQAGALAAAWIAABKQAAAOACAHQABAGAFAHIAAABg");
	this.shape_31.setTransform(30.1,-19.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AAQCSIAAh/IgVAAQgaAAgJgOQgJgIAAgfIAAhvIAhAAIAABtQAAAQACAEQAEAGAJAAIARAAIAAiHIAhAAIAAEjg");
	this.shape_32.setTransform(16.9,-19.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AARCSIAAiSIACgVIgjBvIAAA4IghAAIAAkjIAhAAIAACLIgCATIAjhvIAAgvIAhAAIAAEjg");
	this.shape_33.setTransform(4.3,-19.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AARCSIAAiPIghAAIAACPIghAAIAAkjIAhAAIAAB3IAhAAIAAh3IAhAAIAAEjg");
	this.shape_34.setTransform(-8.7,-19.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgOCSIAAkEIgoAAIAAgfIBtAAIAAAfIgnAAIAAEEg");
	this.shape_35.setTransform(-20.9,-19.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AAYCSIgGg2IgjAAIgGA2IghAAIAlkjIAnAAIAmEjgAgOA9IAdAAIgPiPIAAAAg");
	this.shape_36.setTransform(-31.6,-19.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AARCSIAAkEIghAAIAAEEIghAAIAAkjIBjAAIAAEjg");
	this.shape_37.setTransform(-44.2,-19.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AARCSIAAiPIghAAIAACPIghAAIAAkjIAhAAIAAB3IAhAAIAAh3IAhAAIAAEjg");
	this.shape_38.setTransform(65.5,-59.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgjCDQgIgKgCgQQgBgGAAgTIAAg4IAeAAIAABAQAAASACAEQAGAHAIAAQAHAAAEgHQADgGAAgQIAAjrIAhAAIAADjIgBAZQgDAQgHAKQgLASgXAAQgXAAgOgSg");
	this.shape_39.setTransform(52.8,-59.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AAYCSIgGg2IgjAAIgGA2IghAAIAkkjIAoAAIAlEjgAgOA9IAdAAIgPiPIAAAAg");
	this.shape_40.setTransform(41.1,-59.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AAWCSIAAkEIgVAAIAABGQAAAugDA3QgDAkgJAUQgMAhgYAAIgDAAIAAgfQAHgEAFgKQAHgOAFg2QADgoABgxIAAhZIBQAAIAAEjg");
	this.shape_41.setTransform(28,-59.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AARCSIAAiPIghAAIAACPIghAAIAAkjIAhAAIAAB3IAhAAIAAh3IAhAAIAAEjg");
	this.shape_42.setTransform(15.7,-59.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AglCFQgIgKgDgQIgBgZIAAijIABgZQADgQAIgKQANgSAYAAQAZAAANASQAJAKABAQIABAZIAACjIgBAZQgBAQgJAKQgNASgZAAQgYAAgNgSgAgNhvQgCAEgBATIAACxQABATACAEQAEAIAJAAQAKAAAEgIQACgEABgTIAAixQgBgTgCgEQgEgIgKAAQgJAAgEAIg");
	this.shape_43.setTransform(2.8,-59.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AARCSIAAiSIACgVIgjBvIAAA4IghAAIAAkjIAhAAIAACLIgCATIAjhvIAAgvIAhAAIAAEjg");
	this.shape_44.setTransform(-19.8,-59.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AARCSIAAkEIghAAIAAEEIghAAIAAkjIBjAAIAAEjg");
	this.shape_45.setTransform(-32.8,-59.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("Ag3CRIAAgfQAOAEALgDQAJgCAEgHQAIgJgEgVIgsjeIAkAAIAXCnIABAAIABgUIATiTIAjAAIgmDsQgGAigOAMQgLALgTAAQgQAAgJgCg");
	this.shape_46.setTransform(-45.6,-59.6);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AARCSQgFgJAAgMIAAhYQAAgRgGgGQgGgGgHAAIgMAAIAACKIghAAIAAkjIAhAAIAAB8IALAAQAHAAADgGQAEgFAAgIIAAhHQAAgXAMgIQAHgDAPAAIAKAAIAAAhIgGABQgHABAAAKIAABAQAAASgQAJIAAACQALADAHAIQAFAKAAAXIAABKQAAALACAKIAHANIAAABg");
	this.shape_47.setTransform(-57.6,-59.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-108.3,-74.8,216.8,149.6);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAYCTIgGg2IgmAAIgGA2IgdAAIAhklIAtAAIAiElgAgQA1IAeAAIgOiSg");
	this.shape.setTransform(79.2,20.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgvBMIAAggIAeAAIAAAjQABAdAQAAQAHAAAEgHQAEgIABgSIAAgXQAAgVgGgIQgEgJgKAAIgIAAIAAgoIAKAAQASAAAAgfIAAgQQgBgTgEgIQgEgHgHAAQgQAAgBAdIAAAYIgdAAIAAgWQgBhJAvAAQAYAAAMAUQAMATAAAjIAAAIQAAAugWAOQAWAMAAAwIAAAXQAABKgwAAQgvAAAAhKg");
	this.shape_1.setTransform(67.3,20.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAXCSIgFg0IgmAAIgGA0IgeAAIAikjIAtAAIAiEjgAgQA1IAdAAIgOiSg");
	this.shape_2.setTransform(72.2,59.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIAxAAQAYgBALASQALAQABAiIAAALQgBAtgUAOQAYAMAAAxIAAAYQAABFgxAAgAgQBpIARAAQAQAAABgdIAAgaQAAgUgGgIQgEgHgKAAIgOAAgAgQgaIAMAAQASAAAAgeIAAgRQAAgfgOAAIgQAAg");
	this.shape_3.setTransform(60.3,59.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgpCTIAAgpQAOABAGgFQAHgFACgPIAAgDIgtjhIAgAAIAaCoIAXioIAiAAIgjDaQgGAqgNARQgLAQgZAAIgJAAg");
	this.shape_4.setTransform(48.1,59.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgPCSIAAj6IgiAAIAAgpIBjAAIAAApIgiAAIAAD6g");
	this.shape_5.setTransform(36.7,59.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAXCSIgFg0IgmAAIgFA0IgfAAIAikjIAtAAIAiEjgAgQA1IAdAAIgNiSg");
	this.shape_6.setTransform(25.5,59.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AARCSIAAj6IgjAAIAAD6IggAAIAAkjIBlAAIAAEjg");
	this.shape_7.setTransform(13.1,59.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgjCCQgNgTAAgjIAAiXQAAgkANgTQAMgTAXAAQAXAAANATQANATAAAkIAACXQAAAjgNATQgNAUgXAAQgXAAgMgUgAgQhOIAACdQAAAdAQAAQAQAAAAgdIAAidQAAgdgQAAQgQAAAAAdg");
	this.shape_8.setTransform(-3.6,59.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AASCSIAAh+IgjAAIAAB+IghAAIAAkjIAhAAIAAB+IAjAAIAAh+IAhAAIAAEjg");
	this.shape_9.setTransform(-15.8,59.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AA1CSIgeiCIgIAXIAABrIgeAAIAAhrIgIgXIgdCCIgiAAIAoirIgoh4IAhAAIAmB2IAAh2IAeAAIAAB2IAnh2IAhAAIgoB3IAoCsg");
	this.shape_10.setTransform(-31.1,59.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAWCSIAAjNIgnDNIghAAIAAkjIAdAAIAAC4IAki4IAkAAIAAEjg");
	this.shape_11.setTransform(-46.3,59.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgvCSIAAkjIAvAAQAwgBAABKIAAAlQAABHgwAAIgOAAIAABugAgOgEIAOAAQAPAAAAgcIAAgrQAAgdgPAAIgOAAg");
	this.shape_12.setTransform(-57.8,59.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgpCSIAAkjIBTAAIAAApIgzAAIAAD6g");
	this.shape_13.setTransform(-69.1,59.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgjCCQgMgTAAgjIAAggIAeAAIAAAjQAAAdARAAQAHAAAEgHQAEgIAAgSIAAgXQAAgVgEgIQgGgJgJAAIgIAAIAAgoIAKAAQASAAgBgfIAAgQQAAgTgEgIQgEgHgHAAQgRAAAAAdIAAAYIgeAAIAAgWQABhJAuAAQAXAAANATQAMATAAAkIAAAIQAAAvgXANQAXAMAAAwIAAAXQAAAjgMATQgNAUgXAAQgWAAgNgUg");
	this.shape_14.setTransform(-80.5,59.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgrCSIAAkjIBXAAIAAApIg3AAIAABTIArAAIAAAnIgrAAIAABXIA3AAIAAApg");
	this.shape_15.setTransform(-91.6,59.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIBaAAIAAApIg5AAIAABMIAQAAQAyAAAABHIAAAfQAABIgyAAgAgQBpIAQAAQARgBAAgbIAAglQAAgcgRAAIgQAAg");
	this.shape_16.setTransform(-103.3,59.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgqCSIAAkjIBVAAIAAAfIg0AAIAABcIAgAAIAAAdIggAAIAABsIA0AAIAAAfg");
	this.shape_17.setTransform(104.1,59.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgPCSIAAiPIgkAAIAACPIggAAIAAkjIAgAAIAAB3IAkAAIAAh3IAfAAIAAB3IANAAQAVAAAQAOQAJAJAFAKQAEANAAAWIAAAoQAAAkgMAPQgIAJgKADQgGABgMAAgAAQBzIAMAAQAMAAAFgFQAGgGAAgQIAAgtQAAgXgGgIQgGgJgOAAIgJAAg");
	this.shape_18.setTransform(88.8,59.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgqCSIAAkjIBVAAIAAAfIg0AAIAABcIAgAAIAAAdIggAAIAABsIA0AAIAAAfg");
	this.shape_19.setTransform(47,20.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgPCSIAAiPIgjAAIAACPIgiAAIAAkjIAiAAIAAB3IAjAAIAAh3IAfAAIAAB3IAOAAQAUAAAQAOQAKAJAEAKQAEANABAWIAAAoQgBAkgNAPQgLANgXAAgAAQBzIAMAAQALAAAGgFQAGgGAAgQIAAgtQAAgXgGgIQgGgJgPAAIgIAAg");
	this.shape_20.setTransform(31.7,20.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAYCSIgGg2IgjAAIgGA2IgiAAIAlkjIAoAAIAlEjgAgOA9IAdAAIgPiPIAAAAg");
	this.shape_21.setTransform(15.3,20.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIAxAAQAYAAANAQQALANAAAiIAAAdQAAAWgFAKQgIANgOACIAAACQASAEAHAPQAEAKAAAWIAAAqQAAAbgHALQgLATgYAAgAgQBzIAOAAQAKAAAGgHQADgFAAgPIAAgsQAAgVgEgHQgGgKgOAAIgJAAgAgQgWIANAAQAJAAAFgGQAGgGAAgRIAAgjQgBgQgFgGQgIgGgKAAIgJAAg");
	this.shape_22.setTransform(3.4,20.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("Ag3CRIAAgfQAaAIAMgQQAJgKgFgUIgsjeIAkAAIAXCnIABAAIABgUIATiTIAjAAIgmDsQgFAhgPANQgLALgSAAQgQAAgKgCg");
	this.shape_23.setTransform(-9.4,20.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIAxAAQAbAAAMARQALAOAAAbIAAAyQAAAagLAMQgKAOgUABQgJABgQgDIAACEgAgQgPIAPAAIAHgBQAGgBADgGQACgEAAgQIAAgvQAAgNgFgGQgFgFgIAAIgPAAg");
	this.shape_24.setTransform(-21.5,20.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("Ag3CRIAAgfQAaAIAMgQQAIgJgEgVIgsjeIAkAAIAXCnIABAAIABgUIATiTIAjAAIgmDsQgGAigOAMQgLALgTAAQgQAAgJgCg");
	this.shape_25.setTransform(-34.3,20.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgoCSIAAkjIBRAAIAAAfIgxAAIAAEEg");
	this.shape_26.setTransform(-44.8,20.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AARCSIAAiSIACgVIgjBvIAAA4IghAAIAAkjIAhAAIAACLIgCATIAjhvIAAgvIAhAAIAAEjg");
	this.shape_27.setTransform(-57,20.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AglCDQgMgRAAgmIAAifQAAghAMgPQAOgTAZAAQAYAAAMASQAJAKABASQACAGAAATIAAAhIggAAIAAgsQAAgRgEgGQgFgGgJAAQgGAAgFAGQgEAGgBARIAACuQABAWADAGQADAIAJAAQAKAAAEgIQADgGAAgWIAAgwIAhAAIAAAxQAAAggKAOQgNAUgbAAQgXAAgOgUg");
	this.shape_28.setTransform(-69.5,20.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgmCFQgHgKgCgQIgBgZIAAijIABgZQACgQAHgKQAOgSAYAAQAZAAAOASQAHAKADAQIAAAZIAACjIAAAZQgDAQgHAKQgOASgZAAQgYAAgOgSgAgNhvQgDAFAAASIAACxQAAASADAFQAEAIAJAAQAKAAAEgIQACgEAAgTIAAixQAAgTgCgEQgEgIgKAAQgJAAgEAIg");
	this.shape_29.setTransform(-82.2,20.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgmCFQgIgKgBgQQgBgGAAgTIAAijQAAgTABgGQABgQAIgKQAPgSAXAAQAYAAAPASQAIAKACAQIAAAZIAACjIAAAZQgCAQgIAKQgPASgYAAQgXAAgPgSgAgNhvQgDAFAAASIAACxQAAASADAFQAEAIAJAAQAKAAAEgIQADgFAAgSIAAixQAAgSgDgFQgEgIgKAAQgJAAgEAIg");
	this.shape_30.setTransform(42.2,-19.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AARCSQgFgLAAgKIAAhYQAAgRgGgGQgFgGgIAAIgMAAIAACKIghAAIAAkjIAhAAIAAB8IALAAQAHAAADgGQAEgFAAgIIAAhHQAAgXANgIQAGgDAPAAIAKAAIAAAhIgGABQgHABAAAKIAABAQAAASgQAJIAAACQALADAHAIQAGALAAAWIAABKQAAAOACAHQABAGAFAHIAAABg");
	this.shape_31.setTransform(30.1,-19.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AAQCSIAAh/IgVAAQgaAAgJgOQgJgIAAgfIAAhvIAhAAIAABtQAAAQACAEQAEAGAJAAIARAAIAAiHIAhAAIAAEjg");
	this.shape_32.setTransform(16.9,-19.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AARCSIAAiSIACgVIgjBvIAAA4IghAAIAAkjIAhAAIAACLIgCATIAjhvIAAgvIAhAAIAAEjg");
	this.shape_33.setTransform(4.3,-19.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AARCSIAAiPIghAAIAACPIghAAIAAkjIAhAAIAAB3IAhAAIAAh3IAhAAIAAEjg");
	this.shape_34.setTransform(-8.7,-19.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgOCSIAAkEIgoAAIAAgfIBtAAIAAAfIgnAAIAAEEg");
	this.shape_35.setTransform(-20.9,-19.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AAYCSIgGg2IgjAAIgGA2IghAAIAlkjIAnAAIAmEjgAgOA9IAdAAIgPiPIAAAAg");
	this.shape_36.setTransform(-31.6,-19.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AARCSIAAkEIghAAIAAEEIghAAIAAkjIBjAAIAAEjg");
	this.shape_37.setTransform(-44.2,-19.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AARCSIAAiPIghAAIAACPIghAAIAAkjIAhAAIAAB3IAhAAIAAh3IAhAAIAAEjg");
	this.shape_38.setTransform(65.5,-59.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgjCDQgIgKgCgQQgBgGAAgTIAAg4IAeAAIAABAQAAASACAEQAGAHAIAAQAHAAAEgHQADgGAAgQIAAjrIAhAAIAADjIgBAZQgDAQgHAKQgLASgXAAQgXAAgOgSg");
	this.shape_39.setTransform(52.8,-59.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AAYCSIgGg2IgjAAIgGA2IghAAIAkkjIAoAAIAlEjgAgOA9IAdAAIgPiPIAAAAg");
	this.shape_40.setTransform(41.1,-59.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AAWCSIAAkEIgVAAIAABGQAAAugDA3QgDAkgJAUQgMAhgYAAIgDAAIAAgfQAHgEAFgKQAHgOAFg2QADgoABgxIAAhZIBQAAIAAEjg");
	this.shape_41.setTransform(28,-59.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AARCSIAAiPIghAAIAACPIghAAIAAkjIAhAAIAAB3IAhAAIAAh3IAhAAIAAEjg");
	this.shape_42.setTransform(15.7,-59.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AglCFQgIgKgDgQIgBgZIAAijIABgZQADgQAIgKQANgSAYAAQAZAAANASQAJAKABAQIABAZIAACjIgBAZQgBAQgJAKQgNASgZAAQgYAAgNgSgAgNhvQgCAEgBATIAACxQABATACAEQAEAIAJAAQAKAAAEgIQACgEABgTIAAixQgBgTgCgEQgEgIgKAAQgJAAgEAIg");
	this.shape_43.setTransform(2.8,-59.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AARCSIAAiSIACgVIgjBvIAAA4IghAAIAAkjIAhAAIAACLIgCATIAjhvIAAgvIAhAAIAAEjg");
	this.shape_44.setTransform(-19.8,-59.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AARCSIAAkEIghAAIAAEEIghAAIAAkjIBjAAIAAEjg");
	this.shape_45.setTransform(-32.8,-59.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("Ag3CRIAAgfQAOAEALgDQAJgCAEgHQAIgJgEgVIgsjeIAkAAIAXCnIABAAIABgUIATiTIAjAAIgmDsQgGAigOAMQgLALgTAAQgQAAgJgCg");
	this.shape_46.setTransform(-45.6,-59.6);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AARCSQgFgJAAgMIAAhYQAAgRgGgGQgGgGgHAAIgMAAIAACKIghAAIAAkjIAhAAIAAB8IALAAQAHAAADgGQAEgFAAgIIAAhHQAAgXAMgIQAHgDAPAAIAKAAIAAAhIgGABQgHABAAAKIAABAQAAASgQAJIAAACQALADAHAIQAFAKAAAXIAABKQAAALACAKIAHANIAAABg");
	this.shape_47.setTransform(-57.6,-59.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-108.3,-74.8,216.8,149.6);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAYCTIgGg2IgmAAIgGA2IgdAAIAhklIAtAAIAiElgAgQA1IAeAAIgOiSg");
	this.shape.setTransform(79.2,20.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgvBMIAAggIAeAAIAAAjQABAdAQAAQAHAAAEgHQAEgIABgSIAAgXQAAgVgGgIQgEgJgKAAIgIAAIAAgoIAKAAQASAAAAgfIAAgQQgBgTgEgIQgEgHgHAAQgQAAgBAdIAAAYIgdAAIAAgWQgBhJAvAAQAYAAAMAUQAMATAAAjIAAAIQAAAugWAOQAWAMAAAwIAAAXQAABKgwAAQgvAAAAhKg");
	this.shape_1.setTransform(67.3,20.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAXCSIgFg0IgmAAIgGA0IgeAAIAikjIAtAAIAiEjgAgQA1IAdAAIgOiSg");
	this.shape_2.setTransform(72.2,59.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIAxAAQAYgBALASQALAQABAiIAAALQgBAtgUAOQAYAMAAAxIAAAYQAABFgxAAgAgQBpIARAAQAQAAABgdIAAgaQAAgUgGgIQgEgHgKAAIgOAAgAgQgaIAMAAQASAAAAgeIAAgRQAAgfgOAAIgQAAg");
	this.shape_3.setTransform(60.3,59.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgpCTIAAgpQAOABAGgFQAHgFACgPIAAgDIgtjhIAgAAIAaCoIAXioIAiAAIgjDaQgGAqgNARQgLAQgZAAIgJAAg");
	this.shape_4.setTransform(48.1,59.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgPCSIAAj6IgiAAIAAgpIBjAAIAAApIgiAAIAAD6g");
	this.shape_5.setTransform(36.7,59.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAXCSIgFg0IgmAAIgFA0IgfAAIAikjIAtAAIAiEjgAgQA1IAdAAIgNiSg");
	this.shape_6.setTransform(25.5,59.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AARCSIAAj6IgjAAIAAD6IggAAIAAkjIBlAAIAAEjg");
	this.shape_7.setTransform(13.1,59.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgjCCQgNgTAAgjIAAiXQAAgkANgTQAMgTAXAAQAXAAANATQANATAAAkIAACXQAAAjgNATQgNAUgXAAQgXAAgMgUgAgQhOIAACdQAAAdAQAAQAQAAAAgdIAAidQAAgdgQAAQgQAAAAAdg");
	this.shape_8.setTransform(-3.6,59.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AASCSIAAh+IgjAAIAAB+IghAAIAAkjIAhAAIAAB+IAjAAIAAh+IAhAAIAAEjg");
	this.shape_9.setTransform(-15.8,59.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AA1CSIgeiCIgIAXIAABrIgeAAIAAhrIgIgXIgdCCIgiAAIAoirIgoh4IAhAAIAmB2IAAh2IAeAAIAAB2IAnh2IAhAAIgoB3IAoCsg");
	this.shape_10.setTransform(-31.1,59.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAWCSIAAjNIgnDNIghAAIAAkjIAdAAIAAC4IAki4IAkAAIAAEjg");
	this.shape_11.setTransform(-46.3,59.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgvCSIAAkjIAvAAQAwgBAABKIAAAlQAABHgwAAIgOAAIAABugAgOgEIAOAAQAPAAAAgcIAAgrQAAgdgPAAIgOAAg");
	this.shape_12.setTransform(-57.8,59.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgpCSIAAkjIBTAAIAAApIgzAAIAAD6g");
	this.shape_13.setTransform(-69.1,59.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgjCCQgMgTAAgjIAAggIAeAAIAAAjQAAAdARAAQAHAAAEgHQAEgIAAgSIAAgXQAAgVgEgIQgGgJgJAAIgIAAIAAgoIAKAAQASAAgBgfIAAgQQAAgTgEgIQgEgHgHAAQgRAAAAAdIAAAYIgeAAIAAgWQABhJAuAAQAXAAANATQAMATAAAkIAAAIQAAAvgXANQAXAMAAAwIAAAXQAAAjgMATQgNAUgXAAQgWAAgNgUg");
	this.shape_14.setTransform(-80.5,59.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgrCSIAAkjIBXAAIAAApIg3AAIAABTIArAAIAAAnIgrAAIAABXIA3AAIAAApg");
	this.shape_15.setTransform(-91.6,59.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIBaAAIAAApIg5AAIAABMIAQAAQAyAAAABHIAAAfQAABIgyAAgAgQBpIAQAAQARgBAAgbIAAglQAAgcgRAAIgQAAg");
	this.shape_16.setTransform(-103.3,59.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgqCSIAAkjIBVAAIAAAfIg0AAIAABcIAgAAIAAAdIggAAIAABsIA0AAIAAAfg");
	this.shape_17.setTransform(104.1,59.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgPCSIAAiPIgkAAIAACPIggAAIAAkjIAgAAIAAB3IAkAAIAAh3IAfAAIAAB3IANAAQAVAAAQAOQAJAJAFAKQAEANAAAWIAAAoQAAAkgMAPQgIAJgKADQgGABgMAAgAAQBzIAMAAQAMAAAFgFQAGgGAAgQIAAgtQAAgXgGgIQgGgJgOAAIgJAAg");
	this.shape_18.setTransform(88.8,59.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgqCSIAAkjIBVAAIAAAfIg0AAIAABcIAgAAIAAAdIggAAIAABsIA0AAIAAAfg");
	this.shape_19.setTransform(47,20.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgPCSIAAiPIgjAAIAACPIgiAAIAAkjIAiAAIAAB3IAjAAIAAh3IAfAAIAAB3IAOAAQAUAAAQAOQAKAJAEAKQAEANABAWIAAAoQgBAkgNAPQgLANgXAAgAAQBzIAMAAQALAAAGgFQAGgGAAgQIAAgtQAAgXgGgIQgGgJgPAAIgIAAg");
	this.shape_20.setTransform(31.7,20.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAYCSIgGg2IgjAAIgGA2IgiAAIAlkjIAoAAIAlEjgAgOA9IAdAAIgPiPIAAAAg");
	this.shape_21.setTransform(15.3,20.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIAxAAQAYAAANAQQALANAAAiIAAAdQAAAWgFAKQgIANgOACIAAACQASAEAHAPQAEAKAAAWIAAAqQAAAbgHALQgLATgYAAgAgQBzIAOAAQAKAAAGgHQADgFAAgPIAAgsQAAgVgEgHQgGgKgOAAIgJAAgAgQgWIANAAQAJAAAFgGQAGgGAAgRIAAgjQgBgQgFgGQgIgGgKAAIgJAAg");
	this.shape_22.setTransform(3.4,20.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("Ag3CRIAAgfQAaAIAMgQQAJgKgFgUIgsjeIAkAAIAXCnIABAAIABgUIATiTIAjAAIgmDsQgFAhgPANQgLALgSAAQgQAAgKgCg");
	this.shape_23.setTransform(-9.4,20.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIAxAAQAbAAAMARQALAOAAAbIAAAyQAAAagLAMQgKAOgUABQgJABgQgDIAACEgAgQgPIAPAAIAHgBQAGgBADgGQACgEAAgQIAAgvQAAgNgFgGQgFgFgIAAIgPAAg");
	this.shape_24.setTransform(-21.5,20.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("Ag3CRIAAgfQAaAIAMgQQAIgJgEgVIgsjeIAkAAIAXCnIABAAIABgUIATiTIAjAAIgmDsQgGAigOAMQgLALgTAAQgQAAgJgCg");
	this.shape_25.setTransform(-34.3,20.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgoCSIAAkjIBRAAIAAAfIgxAAIAAEEg");
	this.shape_26.setTransform(-44.8,20.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AARCSIAAiSIACgVIgjBvIAAA4IghAAIAAkjIAhAAIAACLIgCATIAjhvIAAgvIAhAAIAAEjg");
	this.shape_27.setTransform(-57,20.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AglCDQgMgRAAgmIAAifQAAghAMgPQAOgTAZAAQAYAAAMASQAJAKABASQACAGAAATIAAAhIggAAIAAgsQAAgRgEgGQgFgGgJAAQgGAAgFAGQgEAGgBARIAACuQABAWADAGQADAIAJAAQAKAAAEgIQADgGAAgWIAAgwIAhAAIAAAxQAAAggKAOQgNAUgbAAQgXAAgOgUg");
	this.shape_28.setTransform(-69.5,20.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgmCFQgHgKgCgQIgBgZIAAijIABgZQACgQAHgKQAOgSAYAAQAZAAAOASQAHAKADAQIAAAZIAACjIAAAZQgDAQgHAKQgOASgZAAQgYAAgOgSgAgNhvQgDAFAAASIAACxQAAASADAFQAEAIAJAAQAKAAAEgIQACgEAAgTIAAixQAAgTgCgEQgEgIgKAAQgJAAgEAIg");
	this.shape_29.setTransform(-82.2,20.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgmCFQgIgKgBgQQgBgGAAgTIAAijQAAgTABgGQABgQAIgKQAPgSAXAAQAYAAAPASQAIAKACAQIAAAZIAACjIAAAZQgCAQgIAKQgPASgYAAQgXAAgPgSgAgNhvQgDAFAAASIAACxQAAASADAFQAEAIAJAAQAKAAAEgIQADgFAAgSIAAixQAAgSgDgFQgEgIgKAAQgJAAgEAIg");
	this.shape_30.setTransform(42.2,-19.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AARCSQgFgLAAgKIAAhYQAAgRgGgGQgFgGgIAAIgMAAIAACKIghAAIAAkjIAhAAIAAB8IALAAQAHAAADgGQAEgFAAgIIAAhHQAAgXANgIQAGgDAPAAIAKAAIAAAhIgGABQgHABAAAKIAABAQAAASgQAJIAAACQALADAHAIQAGALAAAWIAABKQAAAOACAHQABAGAFAHIAAABg");
	this.shape_31.setTransform(30.1,-19.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AAQCSIAAh/IgVAAQgaAAgJgOQgJgIAAgfIAAhvIAhAAIAABtQAAAQACAEQAEAGAJAAIARAAIAAiHIAhAAIAAEjg");
	this.shape_32.setTransform(16.9,-19.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AARCSIAAiSIACgVIgjBvIAAA4IghAAIAAkjIAhAAIAACLIgCATIAjhvIAAgvIAhAAIAAEjg");
	this.shape_33.setTransform(4.3,-19.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AARCSIAAiPIghAAIAACPIghAAIAAkjIAhAAIAAB3IAhAAIAAh3IAhAAIAAEjg");
	this.shape_34.setTransform(-8.7,-19.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgOCSIAAkEIgoAAIAAgfIBtAAIAAAfIgnAAIAAEEg");
	this.shape_35.setTransform(-20.9,-19.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AAYCSIgGg2IgjAAIgGA2IghAAIAlkjIAnAAIAmEjgAgOA9IAdAAIgPiPIAAAAg");
	this.shape_36.setTransform(-31.6,-19.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AARCSIAAkEIghAAIAAEEIghAAIAAkjIBjAAIAAEjg");
	this.shape_37.setTransform(-44.2,-19.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AARCSIAAiPIghAAIAACPIghAAIAAkjIAhAAIAAB3IAhAAIAAh3IAhAAIAAEjg");
	this.shape_38.setTransform(65.5,-59.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgjCDQgIgKgCgQQgBgGAAgTIAAg4IAeAAIAABAQAAASACAEQAGAHAIAAQAHAAAEgHQADgGAAgQIAAjrIAhAAIAADjIgBAZQgDAQgHAKQgLASgXAAQgXAAgOgSg");
	this.shape_39.setTransform(52.8,-59.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AAYCSIgGg2IgjAAIgGA2IghAAIAkkjIAoAAIAlEjgAgOA9IAdAAIgPiPIAAAAg");
	this.shape_40.setTransform(41.1,-59.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AAWCSIAAkEIgVAAIAABGQAAAugDA3QgDAkgJAUQgMAhgYAAIgDAAIAAgfQAHgEAFgKQAHgOAFg2QADgoABgxIAAhZIBQAAIAAEjg");
	this.shape_41.setTransform(28,-59.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AARCSIAAiPIghAAIAACPIghAAIAAkjIAhAAIAAB3IAhAAIAAh3IAhAAIAAEjg");
	this.shape_42.setTransform(15.7,-59.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AglCFQgIgKgDgQIgBgZIAAijIABgZQADgQAIgKQANgSAYAAQAZAAANASQAJAKABAQIABAZIAACjIgBAZQgBAQgJAKQgNASgZAAQgYAAgNgSgAgNhvQgCAEgBATIAACxQABATACAEQAEAIAJAAQAKAAAEgIQACgEABgTIAAixQgBgTgCgEQgEgIgKAAQgJAAgEAIg");
	this.shape_43.setTransform(2.8,-59.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AARCSIAAiSIACgVIgjBvIAAA4IghAAIAAkjIAhAAIAACLIgCATIAjhvIAAgvIAhAAIAAEjg");
	this.shape_44.setTransform(-19.8,-59.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AARCSIAAkEIghAAIAAEEIghAAIAAkjIBjAAIAAEjg");
	this.shape_45.setTransform(-32.8,-59.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("Ag3CRIAAgfQAOAEALgDQAJgCAEgHQAIgJgEgVIgsjeIAkAAIAXCnIABAAIABgUIATiTIAjAAIgmDsQgGAigOAMQgLALgTAAQgQAAgJgCg");
	this.shape_46.setTransform(-45.6,-59.6);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AARCSQgFgJAAgMIAAhYQAAgRgGgGQgGgGgHAAIgMAAIAACKIghAAIAAkjIAhAAIAAB8IALAAQAHAAADgGQAEgFAAgIIAAhHQAAgXAMgIQAHgDAPAAIAKAAIAAAhIgGABQgHABAAAKIAABAQAAASgQAJIAAACQALADAHAIQAFAKAAAXIAABKQAAALACAKIAHANIAAABg");
	this.shape_47.setTransform(-57.6,-59.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-108.3,-74.8,216.8,149.6);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAYCTIgGg2IgmAAIgGA2IgdAAIAhklIAtAAIAiElgAgQA1IAeAAIgOiSg");
	this.shape.setTransform(79.2,20.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgvBMIAAggIAeAAIAAAjQABAdAQAAQAHAAAEgHQAEgIABgSIAAgXQAAgVgGgIQgEgJgKAAIgIAAIAAgoIAKAAQASAAAAgfIAAgQQgBgTgEgIQgEgHgHAAQgQAAgBAdIAAAYIgdAAIAAgWQgBhJAvAAQAYAAAMAUQAMATAAAjIAAAIQAAAugWAOQAWAMAAAwIAAAXQAABKgwAAQgvAAAAhKg");
	this.shape_1.setTransform(67.3,20.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAXCSIgFg0IgmAAIgGA0IgeAAIAikjIAtAAIAiEjgAgQA1IAdAAIgOiSg");
	this.shape_2.setTransform(72.2,59.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIAxAAQAYgBALASQALAQABAiIAAALQgBAtgUAOQAYAMAAAxIAAAYQAABFgxAAgAgQBpIARAAQAQAAABgdIAAgaQAAgUgGgIQgEgHgKAAIgOAAgAgQgaIAMAAQASAAAAgeIAAgRQAAgfgOAAIgQAAg");
	this.shape_3.setTransform(60.3,59.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgpCTIAAgpQAOABAGgFQAHgFACgPIAAgDIgtjhIAgAAIAaCoIAXioIAiAAIgjDaQgGAqgNARQgLAQgZAAIgJAAg");
	this.shape_4.setTransform(48.1,59.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgPCSIAAj6IgiAAIAAgpIBjAAIAAApIgiAAIAAD6g");
	this.shape_5.setTransform(36.7,59.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAXCSIgFg0IgmAAIgFA0IgfAAIAikjIAtAAIAiEjgAgQA1IAdAAIgNiSg");
	this.shape_6.setTransform(25.5,59.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AARCSIAAj6IgjAAIAAD6IggAAIAAkjIBlAAIAAEjg");
	this.shape_7.setTransform(13.1,59.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgjCCQgNgTAAgjIAAiXQAAgkANgTQAMgTAXAAQAXAAANATQANATAAAkIAACXQAAAjgNATQgNAUgXAAQgXAAgMgUgAgQhOIAACdQAAAdAQAAQAQAAAAgdIAAidQAAgdgQAAQgQAAAAAdg");
	this.shape_8.setTransform(-3.6,59.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AASCSIAAh+IgjAAIAAB+IghAAIAAkjIAhAAIAAB+IAjAAIAAh+IAhAAIAAEjg");
	this.shape_9.setTransform(-15.8,59.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AA1CSIgeiCIgIAXIAABrIgeAAIAAhrIgIgXIgdCCIgiAAIAoirIgoh4IAhAAIAmB2IAAh2IAeAAIAAB2IAnh2IAhAAIgoB3IAoCsg");
	this.shape_10.setTransform(-31.1,59.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAWCSIAAjNIgnDNIghAAIAAkjIAdAAIAAC4IAki4IAkAAIAAEjg");
	this.shape_11.setTransform(-46.3,59.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgvCSIAAkjIAvAAQAwgBAABKIAAAlQAABHgwAAIgOAAIAABugAgOgEIAOAAQAPAAAAgcIAAgrQAAgdgPAAIgOAAg");
	this.shape_12.setTransform(-57.8,59.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgpCSIAAkjIBTAAIAAApIgzAAIAAD6g");
	this.shape_13.setTransform(-69.1,59.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgjCCQgMgTAAgjIAAggIAeAAIAAAjQAAAdARAAQAHAAAEgHQAEgIAAgSIAAgXQAAgVgEgIQgGgJgJAAIgIAAIAAgoIAKAAQASAAgBgfIAAgQQAAgTgEgIQgEgHgHAAQgRAAAAAdIAAAYIgeAAIAAgWQABhJAuAAQAXAAANATQAMATAAAkIAAAIQAAAvgXANQAXAMAAAwIAAAXQAAAjgMATQgNAUgXAAQgWAAgNgUg");
	this.shape_14.setTransform(-80.5,59.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgrCSIAAkjIBXAAIAAApIg3AAIAABTIArAAIAAAnIgrAAIAABXIA3AAIAAApg");
	this.shape_15.setTransform(-91.6,59.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIBaAAIAAApIg5AAIAABMIAQAAQAyAAAABHIAAAfQAABIgyAAgAgQBpIAQAAQARgBAAgbIAAglQAAgcgRAAIgQAAg");
	this.shape_16.setTransform(-103.3,59.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgqCSIAAkjIBVAAIAAAfIg0AAIAABcIAgAAIAAAdIggAAIAABsIA0AAIAAAfg");
	this.shape_17.setTransform(104.1,59.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgPCSIAAiPIgkAAIAACPIggAAIAAkjIAgAAIAAB3IAkAAIAAh3IAfAAIAAB3IANAAQAVAAAQAOQAJAJAFAKQAEANAAAWIAAAoQAAAkgMAPQgIAJgKADQgGABgMAAgAAQBzIAMAAQAMAAAFgFQAGgGAAgQIAAgtQAAgXgGgIQgGgJgOAAIgJAAg");
	this.shape_18.setTransform(88.8,59.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgqCSIAAkjIBVAAIAAAfIg0AAIAABcIAgAAIAAAdIggAAIAABsIA0AAIAAAfg");
	this.shape_19.setTransform(47,20.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgPCSIAAiPIgjAAIAACPIgiAAIAAkjIAiAAIAAB3IAjAAIAAh3IAfAAIAAB3IAOAAQAUAAAQAOQAKAJAEAKQAEANABAWIAAAoQgBAkgNAPQgLANgXAAgAAQBzIAMAAQALAAAGgFQAGgGAAgQIAAgtQAAgXgGgIQgGgJgPAAIgIAAg");
	this.shape_20.setTransform(31.7,20.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAYCSIgGg2IgjAAIgGA2IgiAAIAlkjIAoAAIAlEjgAgOA9IAdAAIgPiPIAAAAg");
	this.shape_21.setTransform(15.3,20.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIAxAAQAYAAANAQQALANAAAiIAAAdQAAAWgFAKQgIANgOACIAAACQASAEAHAPQAEAKAAAWIAAAqQAAAbgHALQgLATgYAAgAgQBzIAOAAQAKAAAGgHQADgFAAgPIAAgsQAAgVgEgHQgGgKgOAAIgJAAgAgQgWIANAAQAJAAAFgGQAGgGAAgRIAAgjQgBgQgFgGQgIgGgKAAIgJAAg");
	this.shape_22.setTransform(3.4,20.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("Ag3CRIAAgfQAaAIAMgQQAJgKgFgUIgsjeIAkAAIAXCnIABAAIABgUIATiTIAjAAIgmDsQgFAhgPANQgLALgSAAQgQAAgKgCg");
	this.shape_23.setTransform(-9.4,20.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgxCSIAAkjIAxAAQAbAAAMARQALAOAAAbIAAAyQAAAagLAMQgKAOgUABQgJABgQgDIAACEgAgQgPIAPAAIAHgBQAGgBADgGQACgEAAgQIAAgvQAAgNgFgGQgFgFgIAAIgPAAg");
	this.shape_24.setTransform(-21.5,20.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("Ag3CRIAAgfQAaAIAMgQQAIgJgEgVIgsjeIAkAAIAXCnIABAAIABgUIATiTIAjAAIgmDsQgGAigOAMQgLALgTAAQgQAAgJgCg");
	this.shape_25.setTransform(-34.3,20.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgoCSIAAkjIBRAAIAAAfIgxAAIAAEEg");
	this.shape_26.setTransform(-44.8,20.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AARCSIAAiSIACgVIgjBvIAAA4IghAAIAAkjIAhAAIAACLIgCATIAjhvIAAgvIAhAAIAAEjg");
	this.shape_27.setTransform(-57,20.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AglCDQgMgRAAgmIAAifQAAghAMgPQAOgTAZAAQAYAAAMASQAJAKABASQACAGAAATIAAAhIggAAIAAgsQAAgRgEgGQgFgGgJAAQgGAAgFAGQgEAGgBARIAACuQABAWADAGQADAIAJAAQAKAAAEgIQADgGAAgWIAAgwIAhAAIAAAxQAAAggKAOQgNAUgbAAQgXAAgOgUg");
	this.shape_28.setTransform(-69.5,20.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgmCFQgHgKgCgQIgBgZIAAijIABgZQACgQAHgKQAOgSAYAAQAZAAAOASQAHAKADAQIAAAZIAACjIAAAZQgDAQgHAKQgOASgZAAQgYAAgOgSgAgNhvQgDAFAAASIAACxQAAASADAFQAEAIAJAAQAKAAAEgIQACgEAAgTIAAixQAAgTgCgEQgEgIgKAAQgJAAgEAIg");
	this.shape_29.setTransform(-82.2,20.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgmCFQgIgKgBgQQgBgGAAgTIAAijQAAgTABgGQABgQAIgKQAPgSAXAAQAYAAAPASQAIAKACAQIAAAZIAACjIAAAZQgCAQgIAKQgPASgYAAQgXAAgPgSgAgNhvQgDAFAAASIAACxQAAASADAFQAEAIAJAAQAKAAAEgIQADgFAAgSIAAixQAAgSgDgFQgEgIgKAAQgJAAgEAIg");
	this.shape_30.setTransform(42.2,-19.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AARCSQgFgLAAgKIAAhYQAAgRgGgGQgFgGgIAAIgMAAIAACKIghAAIAAkjIAhAAIAAB8IALAAQAHAAADgGQAEgFAAgIIAAhHQAAgXANgIQAGgDAPAAIAKAAIAAAhIgGABQgHABAAAKIAABAQAAASgQAJIAAACQALADAHAIQAGALAAAWIAABKQAAAOACAHQABAGAFAHIAAABg");
	this.shape_31.setTransform(30.1,-19.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AAQCSIAAh/IgVAAQgaAAgJgOQgJgIAAgfIAAhvIAhAAIAABtQAAAQACAEQAEAGAJAAIARAAIAAiHIAhAAIAAEjg");
	this.shape_32.setTransform(16.9,-19.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AARCSIAAiSIACgVIgjBvIAAA4IghAAIAAkjIAhAAIAACLIgCATIAjhvIAAgvIAhAAIAAEjg");
	this.shape_33.setTransform(4.3,-19.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AARCSIAAiPIghAAIAACPIghAAIAAkjIAhAAIAAB3IAhAAIAAh3IAhAAIAAEjg");
	this.shape_34.setTransform(-8.7,-19.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgOCSIAAkEIgoAAIAAgfIBtAAIAAAfIgnAAIAAEEg");
	this.shape_35.setTransform(-20.9,-19.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AAYCSIgGg2IgjAAIgGA2IghAAIAlkjIAnAAIAmEjgAgOA9IAdAAIgPiPIAAAAg");
	this.shape_36.setTransform(-31.6,-19.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AARCSIAAkEIghAAIAAEEIghAAIAAkjIBjAAIAAEjg");
	this.shape_37.setTransform(-44.2,-19.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AARCSIAAiPIghAAIAACPIghAAIAAkjIAhAAIAAB3IAhAAIAAh3IAhAAIAAEjg");
	this.shape_38.setTransform(65.5,-59.7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgjCDQgIgKgCgQQgBgGAAgTIAAg4IAeAAIAABAQAAASACAEQAGAHAIAAQAHAAAEgHQADgGAAgQIAAjrIAhAAIAADjIgBAZQgDAQgHAKQgLASgXAAQgXAAgOgSg");
	this.shape_39.setTransform(52.8,-59.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AAYCSIgGg2IgjAAIgGA2IghAAIAkkjIAoAAIAlEjgAgOA9IAdAAIgPiPIAAAAg");
	this.shape_40.setTransform(41.1,-59.7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AAWCSIAAkEIgVAAIAABGQAAAugDA3QgDAkgJAUQgMAhgYAAIgDAAIAAgfQAHgEAFgKQAHgOAFg2QADgoABgxIAAhZIBQAAIAAEjg");
	this.shape_41.setTransform(28,-59.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AARCSIAAiPIghAAIAACPIghAAIAAkjIAhAAIAAB3IAhAAIAAh3IAhAAIAAEjg");
	this.shape_42.setTransform(15.7,-59.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AglCFQgIgKgDgQIgBgZIAAijIABgZQADgQAIgKQANgSAYAAQAZAAANASQAJAKABAQIABAZIAACjIgBAZQgBAQgJAKQgNASgZAAQgYAAgNgSgAgNhvQgCAEgBATIAACxQABATACAEQAEAIAJAAQAKAAAEgIQACgEABgTIAAixQgBgTgCgEQgEgIgKAAQgJAAgEAIg");
	this.shape_43.setTransform(2.8,-59.7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AARCSIAAiSIACgVIgjBvIAAA4IghAAIAAkjIAhAAIAACLIgCATIAjhvIAAgvIAhAAIAAEjg");
	this.shape_44.setTransform(-19.8,-59.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AARCSIAAkEIghAAIAAEEIghAAIAAkjIBjAAIAAEjg");
	this.shape_45.setTransform(-32.8,-59.7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("Ag3CRIAAgfQAOAEALgDQAJgCAEgHQAIgJgEgVIgsjeIAkAAIAXCnIABAAIABgUIATiTIAjAAIgmDsQgGAigOAMQgLALgTAAQgQAAgJgCg");
	this.shape_46.setTransform(-45.6,-59.6);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AARCSQgFgJAAgMIAAhYQAAgRgGgGQgGgGgHAAIgMAAIAACKIghAAIAAkjIAhAAIAAB8IALAAQAHAAADgGQAEgFAAgIIAAhHQAAgXAMgIQAHgDAPAAIAKAAIAAAhIgGABQgHABAAAKIAABAQAAASgQAJIAAACQALADAHAIQAFAKAAAXIAABKQAAALACAKIAHANIAAABg");
	this.shape_47.setTransform(-57.6,-59.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-108.3,-74.8,216.8,149.6);


(lib.Symbol62 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#ED1C24").s().p("A3bTiMAAAgnDMAu3AAAMAAAAnDg");
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Symbol44 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A3bTiMAAAgnDMAu3AAAMAAAAnDg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-125,300,250);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgfPAwmMAAAhhLMA+fAAAMAAABhLg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-200,-311,400.1,622.2);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.background();
	this.instance.parent = this;
	this.instance.setTransform(-150,-124.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A3bTrMAAAgnWMAu3AAAMAAAAnWg");
	this.shape.setTransform(0,1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-125,300,600.1);


(lib.hit = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Eg32AidMAAAhE5MBvsAAAMAAABE5g");
	this.shape.setTransform(357.5,220.5);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.ClipGroup = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("EgXbAu4MAAAhdvMAu2AAAMAAABdvg");
	mask.setTransform(150,300);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#009EE2","#312782"],[0,1],0,0,0,0,0,150.1).s().p("EgXbAu4MAAAhdvMAu2AAAMAAABdvg");
	this.shape.setTransform(150,125.5,1,0.418);

	this.shape.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,250.9);


(lib.ClipGroup_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("EgXbAu4MAAAhdvMAu2AAAMAAABdvg");
	mask_1.setTransform(150,300);

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAVCWIgriRIAACRIghAAIAAkqIAhAAIAAB/IAlh/IAkAAIguCNIAzCdg");
	this.shape_1.setTransform(266.8,300);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAsCWIAAjKIADgoIgBAAIgiDyIgXAAIghjyIgCAAIAEAoIAADKIgiAAIAAkqIA0AAIAYC+IAAAAIAai+IAzAAIAAEqg");
	this.shape_2.setTransform(250.4,300);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgPAZIAAgxIAfAAIAAAxg");
	this.shape_3.setTransform(238.2,312.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AArCWIAAjKIAEgoIgBAAIgiDyIgXAAIgijyIgBAAIAEAoIAADKIgiAAIAAkqIA0AAIAYC+IAAAAIAZi+IA0AAIAAEqg");
	this.shape_4.setTransform(226,300);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgmCIQgJgLgBgPQgCgHAAgTIAAimQAAgUACgHQABgQAJgKQAOgSAYAAQAaAAANASQAIAKADAQIABAbIAACmIgBAaQgDAPgIALQgNARgaAAQgYAAgOgRgAgOhxQgCAEAAATIAAC0QAAAUACAEQAFAIAJAAQAJAAAGgIQACgFAAgTIAAi0QAAgSgCgFQgGgIgJAAQgJAAgFAIg");
	this.shape_5.setTransform(210.3,300);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgnCFQgLgQABgnIAAiiQgBgjALgPQAPgTAaAAQAZAAAMASQAJAKABASIABAaIAAAiIgfAAIAAgtQAAgRgFgGQgFgGgIAAQgGAAgHAGQgEAGAAARIAACyQAAAWADAGQADAIAKAAQAKAAADgIQAEgGAAgWIAAgyIAhAAIAAAzQABAhgLAOQgNATgbAAQgZABgOgVg");
	this.shape_6.setTransform(197.6,300);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgPAZIAAgxIAfAAIAAAxg");
	this.shape_7.setTransform(188.2,312.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAVCWIgriRIAACRIghAAIAAkqIAhAAIAAB/IAlh/IAkAAIguCNIAzCdg");
	this.shape_8.setTransform(179.3,300);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAPCWIgojkIgCABIACDjIghAAIAAkqIAvAAIAmDcIABAAIgCgZIAAjDIAhAAIAAEqg");
	this.shape_9.setTransform(164.7,300);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgPCWIAAkqIAfAAIAAEqg");
	this.shape_10.setTransform(154,300);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgrCWIAAkqIAhAAIAAEJIA2AAIAAAhg");
	this.shape_11.setTransform(146.2,300);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgnCIQgHgLgDgPIgBgaIAAimIABgbQADgQAHgKQAPgSAYAAQAZAAAOASQAJAKABAQIABAbIAACmIgBAaQgBAPgJALQgOARgZAAQgYAAgPgRgAgNhxQgDAFAAASIAAC0QAAATADAFQADAIAKAAQAKAAAEgIQADgFAAgTIAAi0QAAgSgDgFQgEgIgKAAQgKAAgDAIg");
	this.shape_12.setTransform(133.8,300);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AARCWQgHgNAAgXIAAhIQAAgRgDgFQgHgJgJABIgKAAIAACKIgiAAIAAkqIAzAAQAagBANAVQAKANAAAaIAAAkQAAAbgJAKQgFAJgPADIAAABQALADAEAFQALAMAAAZIAABBIABAXQACAKAHAHIAAADgAgTgSIANAAQALAAAFgIQAFgFAAgSIAAgpQAAgPgFgFQgFgHgNAAIgLAAg");
	this.shape_13.setTransform(121.5,300);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgmCGQgJgLgBgPIgCgaIAAjoIAiAAIAADwQAAAQACAGQAFAIAJAAQAKAAAFgIQACgEAAgSIAAjwIAiAAIAADoQAAATgBAHQgCAPgIALQgPARgZAAQgYAAgOgRg");
	this.shape_14.setTransform(108.1,300.2);

	this.shape_1.mask = this.shape_2.mask = this.shape_3.mask = this.shape_4.mask = this.shape_5.mask = this.shape_6.mask = this.shape_7.mask = this.shape_8.mask = this.shape_9.mask = this.shape_10.mask = this.shape_11.mask = this.shape_12.mask = this.shape_13.mask = this.shape_14.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(103,284.6,169.5,30.8);


(lib.Tween135 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.ClipGroup_1();
	this.instance.parent = this;
	this.instance.setTransform(0,0,1,1,0,0,0,150,300);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgqCWIAAkrIBVAAIAAAhIg0AAIAABdIAhAAIAAAeIghAAIAABuIA0AAIAAAhg");
	this.shape.setTransform(-53.2,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgPAZIAAgxIAfAAIAAAxg");
	this.shape_1.setTransform(-62.1,12.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AATCWIgTjUIAAAAIAAAbIgSC5IgnAAIgikrIAjAAIASDIIAAARIABAAIATjZIAlAAIASDZIADAAIAAgRIASjIIAhAAIghErg");
	this.shape_2.setTransform(-74.7,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AATCWIgTjUIAAAAIgTDUIgmAAIgikrIAjAAIARDIIAAARIACAAIATjZIAkAAIAUDZIACAAIAAgRIARjIIAjAAIgiErg");
	this.shape_3.setTransform(-93.9,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AATCWIgTjUIAAAAIgSDUIgnAAIghkrIAiAAIARDIIAAARIADAAIATjZIAkAAIATDZIACAAIAAgRIARjIIAiAAIghErg");
	this.shape_4.setTransform(-113.2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-300,300,600);


(lib.Tween134 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.ClipGroup_1();
	this.instance.parent = this;
	this.instance.setTransform(0,0,1,1,0,0,0,150,300);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgqCWIAAkrIBVAAIAAAhIg0AAIAABdIAhAAIAAAeIghAAIAABuIA0AAIAAAhg");
	this.shape.setTransform(-53.2,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgPAZIAAgxIAfAAIAAAxg");
	this.shape_1.setTransform(-62.1,12.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AATCWIgTjUIAAAAIAAAbIgSC5IgnAAIgikrIAjAAIASDIIAAARIABAAIATjZIAlAAIASDZIADAAIAAgRIASjIIAhAAIghErg");
	this.shape_2.setTransform(-74.7,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AATCWIgTjUIAAAAIgTDUIgmAAIgikrIAjAAIARDIIAAARIACAAIATjZIAkAAIAUDZIACAAIAAgRIARjIIAjAAIgiErg");
	this.shape_3.setTransform(-93.9,0);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AATCWIgTjUIAAAAIgSDUIgnAAIghkrIAiAAIARDIIAAARIADAAIATjZIAkAAIATDZIACAAIAAgRIARjIIAiAAIghErg");
	this.shape_4.setTransform(-113.2,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-150,-300,300,600);


// stage content:
(lib.EuroLink_obicen_300x250_juni_2016 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var frequency = 20;
		stage.enableMouseOver(frequency);
		var $this = this;
		
		this.btnInv.addEventListener("click", fl_MouseClickHandler_4);
		
		function fl_MouseClickHandler_4()
		{
		window.open(clickTag, "blank");
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(345));

	// invMAsk
	this.rectMask_mc = new lib.Symbol44();
	this.rectMask_mc.parent = this;
	this.rectMask_mc.setTransform(150,125);
	this.rectMask_mc.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.rectMask_mc).wait(345));

	// invbtn
	this.btnInv = new lib.Symbol62();
	this.btnInv.parent = this;
	this.btnInv.setTransform(150,125);
	new cjs.ButtonHelper(this.btnInv, 0, 1, 2, false, new lib.Symbol62(), 3);

	this.timeline.addTween(cjs.Tween.get(this.btnInv).wait(345));

	// ramka
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CCCCCC").ss(1,1,1).p("A3YzeMAuxAAAMAAAAm9MguxAAAg");
	this.shape.setTransform(149.8,124.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(345));

	// flash0.ai
	this.instance = new lib.Tween139("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(149,125.1);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(298).to({_off:false},0).to({alpha:1},7).wait(32).to({startPosition:0},0).to({alpha:0},7).wait(1));

	// Layer 6
	this.instance_1 = new lib.Symbol12();
	this.instance_1.parent = this;
	this.instance_1.setTransform(174,305.1);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(291).to({_off:false},0).to({alpha:1},7).wait(47));

	// flash0.ai
	this.instance_2 = new lib.Tween134("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(150,126.1);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.instance_3 = new lib.Tween135("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(150,126.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(234).to({_off:false},0).to({_off:true,alpha:1},7).wait(104));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(234).to({_off:false},7).to({startPosition:0},43).to({alpha:0},7).wait(54));

	// flash0.ai
	this.instance_4 = new lib.Tween1("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(150.5,125);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.instance_5 = new lib.Tween2("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(150.5,125);

	this.instance_6 = new lib.Tween3("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(150.5,125);
	this.instance_6._off = true;

	this.instance_7 = new lib.Tween4("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(150.5,125);
	this.instance_7.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_4}]},168).to({state:[{t:this.instance_5}]},7).to({state:[{t:this.instance_6}]},53).to({state:[{t:this.instance_7}]},6).wait(111));
	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(168).to({_off:false},0).to({_off:true,alpha:1},7).wait(170));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(228).to({_off:false},0).to({_off:true,alpha:0},6).wait(111));

	// flash0.ai
	this.instance_8 = new lib.ClipGroup();
	this.instance_8.parent = this;
	this.instance_8.setTransform(149,300.1,1,1,0,0,0,150,300);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(160).to({_off:false},0).to({alpha:1},8).wait(177));

	// raka
	this.instance_9 = new lib.Tween132("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(400.4,226.4,0.81,0.81);
	this.instance_9._off = true;

	this.instance_10 = new lib.Tween133("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(82.7,35.4,0.81,0.81);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(5).to({_off:false},0).to({_off:true,x:82.7,y:35.4},6,cjs.Ease.get(0.85)).wait(334));
	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(5).to({_off:false},6,cjs.Ease.get(0.85)).wait(2).to({x:102.7,y:62.4},0).to({x:-44,y:135.4},5).to({x:29.6,y:170.7},6,cjs.Ease.get(0.85)).wait(4).to({y:171.7},0).to({regX:4,regY:2,x:-69.1,y:106.9},5).to({regY:1.9,x:39.1,y:227.7},7,cjs.Ease.get(0.85)).wait(5).to({x:43.1,y:228.7},0).to({regX:3.9,x:154.5,y:-43},6).to({regX:4,regY:2,x:224.3,y:44.3},7,cjs.Ease.get(0.85)).wait(6).to({y:45.3},0).to({x:383.1,y:390.6},6).to({x:270.1,y:123.6},7,cjs.Ease.get(0.85)).wait(5).to({y:123.7},0).to({x:375.8,y:336.3},5).to({x:202.4,y:97},7,cjs.Ease.get(0.85)).wait(5).to({x:232.4,y:158},0).to({x:128.9,y:341.3},6).to({x:146.2,y:142.1},7).wait(5).to({startPosition:0},0).to({x:-18,y:251.2},5).to({regY:1.9,x:219.1,y:172},7,cjs.Ease.get(0.85)).wait(7).to({startPosition:0},0).to({alpha:0},7).wait(202));

	// sheshir.png
	this.instance_11 = new lib.Tween119("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(442,401.2,0.98,0.98);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(5).to({_off:false},0).to({x:126.4,y:215.8},6,cjs.Ease.get(0.85)).wait(334));

	// papucaLeva.png
	this.instance_12 = new lib.Tween123("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(15.8,143.6,0.745,0.745);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(18).to({_off:false},0).to({x:83.3,y:168.4},6,cjs.Ease.get(0.85)).wait(321));

	// papucaDeva.png
	this.instance_13 = new lib.Tween125("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(-13.2,78,0.745,0.745);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(33).to({_off:false},0).to({x:85.2,y:174.2},7,cjs.Ease.get(0.85)).wait(305));

	// aparat.png
	this.instance_14 = new lib.Tween131("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(88.9,183.2,0.893,0.893,0.3);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(51).to({_off:false},0).to({rotation:-0.3,x:161.6,y:269},7,cjs.Ease.get(0.85)).wait(287));

	// mleko.png
	this.instance_15 = new lib.Tween129("synched",0);
	this.instance_15.parent = this;
	this.instance_15.setTransform(327.9,408.2,0.623,0.623,-59.7);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(70).to({_off:false},0).to({x:211.8,y:143.8},7,cjs.Ease.get(0.85)).wait(268));

	// naocari.png
	this.instance_16 = new lib.Tween127("synched",0);
	this.instance_16.parent = this;
	this.instance_16.setTransform(291.5,414.1,0.81,0.81);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(87).to({_off:false},0).to({rotation:-20.7,x:156.1,y:196},7,cjs.Ease.get(0.85)).wait(251));

	// tablet.png copy
	this.instance_17 = new lib.Tween117("synched",0);
	this.instance_17.parent = this;
	this.instance_17.setTransform(152.7,205.2,0.81,0.81);
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(105).to({_off:false},0).to({x:162.4,y:6.9},7).wait(233));

	// pasosh.png
	this.instance_18 = new lib.Tween121("synched",0);
	this.instance_18.parent = this;
	this.instance_18.setTransform(-37.6,267.4,0.81,0.81);
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(122).to({_off:false},0).to({rotation:-3.2,x:198,y:193},7,cjs.Ease.get(0.85)).wait(216));

	// zvezda2.png
	this.instance_19 = new lib.Tween113("synched",0);
	this.instance_19.parent = this;
	this.instance_19.setTransform(208,71.7,0.81,0.81);
	this.instance_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(5).to({_off:false},0).wait(340));

	// zvezda.png
	this.instance_20 = new lib.Tween115("synched",0);
	this.instance_20.parent = this;
	this.instance_20.setTransform(172.9,32,0.81,0.81);
	this.instance_20._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(5).to({_off:false},0).wait(340));

	// Layer 8
	this.instance_21 = new lib.Symbol12();
	this.instance_21.parent = this;
	this.instance_21.setTransform(174,305.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_21).to({alpha:0},5).to({_off:true},1).wait(339));

	// pozadina-zatvorena
	this.instance_22 = new lib.Symbol11("synched",0);
	this.instance_22.parent = this;
	this.instance_22.setTransform(150,124.5);
	this.instance_22.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_22).to({alpha:1},5).wait(340));

	// hit area
	this.instance_23 = new lib.hit();
	this.instance_23.parent = this;
	this.instance_23.setTransform(0,0,0.416,0.567,0,-0.5,0);
	new cjs.ButtonHelper(this.instance_23, 0, 1, 2, false, new lib.hit(), 3);

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(345));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(124,119,400.1,622.2);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;