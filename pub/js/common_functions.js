//prikaz na "popust" za odbrana opcija od ponuda
function dealOptionDiscountDisplay(price, price_discount) 
{
	var retStr = "";
	var discVal = 0;
	
	//presmetaj ddv za eden vaucer
	if(price > 0)
	{
		discVal = dealOptionDiscountCalc(price, price_discount);
		retStr = discVal+"%";
	}
		
	
	
	return retStr ;
}

//presmetka na "popust" za odbrana opcija od ponuda
function dealOptionDiscountCalc(price, price_discount) 
{
	discRet = 0;
	
	//presmetaj ddv za eden vaucer
	discRet = Math.round(100 - (price_discount / price) * 100);
	
	
	return discRet ;
}


//prikaz na "zasteduvas" za odbrana opcija od ponuda
function dealOptionSavingsDisplay(price, price_discount) 
{
	var retStr = "";
	var saveVal = 0;
	
	//presmetaj ddv za eden vaucer
	if(price > 0)
	{
		discount = dealOptionDiscountCalc(price, price_discount);
		saveVal = dealOptionSavingsCalc(price, discount);
		retStr = saveVal+" ден.";
	}
		
	
	
	return retStr ;
}

//presmetka na "zasteduvas" za odbrana opcija od ponuda
function dealOptionSavingsCalc(price, discount) 
{
	saveRet = 0;
	
	//presmetaj ddv za eden vaucer
	saveRet = Math.round(price * discount / 100);
	
	
	return saveRet ;
}


function validateEmail(email) {
    if (!email) {
        alert($("#invalidmail").val())
        return false;
    }
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(email) == false) {
        alert($("#invalidmail").val())
        return false;
    }
    return true;
}

//osnova za poenit za EDEN vaucer
function points_osnova_funkcija(price_discount, provizija, tip_danocna_sema, ddv_stapka, tip_ponuda) {
	
	osnova = 0;
	
	//presmetaj ddv za eden vaucer
	ddv = ddv_funkcija(price_discount, provizija, tip_danocna_sema, ddv_stapka, tip_ponuda, 1);
	
	//presmetaj poeni
	//if(tip_danocna_sema == "Не ДДВ Обврзник" && tip_ponuda == 'cela_cena')
	//	osnova = 0;
	//else
		osnova = provizija - ddv;
	
	return osnova ;
}

//presmetaka na poenite zavisno kolku e brojot na vauceri
function points_funkcija(osnova, broj_vauceri) {

		points_return = 0;
		points = osnova*0.1;
		
		points_return = Math.floor(points)*broj_vauceri;
		
		return points_return ;
		
		//po staro
        //return Math.floor(price*0.1)*numberOfVouchers ;
}

//go vrakja presmetaniot DDV danok izrazeno vo denari spored kriteriumite. 
//Ako se vnese broj na vauceri => go dava vkupniot danok za site vauceri. Ako ne se vnese broj na vauceri => go dava DDV-to za poedinecen vaucer
function ddv_funkcija(price_discount, provizija, tip_danocna_sema, ddv_stapka, tip_ponuda, broj_vauceri) {
	
	ddv = 0;
	
	//cena_na_ponudata = (tip_ponuda == 'cena_na_vaucer') ? provizija : price_discount;
	
	switch (tip_danocna_sema) {
		case "ДДВ Обврзник":
			
			//presmetaj ddv
			ddv = ((provizija*ddv_stapka)/100)*broj_vauceri;
				
			break;

		case "Не ДДВ Обврзник":
			
			//presmetaj ddv
			ddv = ((provizija*ddv_stapka)/100)*broj_vauceri;

			break;
		case "Ослободен согласно законот":
			
			//presmetaj ddv
			ddv = ((provizija*ddv_stapka)/100)*broj_vauceri;

			
			break;
		case "Промет остварен во странство":
			
			//presmetaj ddv
			//tuka ne se presmetuva ddv. ddv-to e nula
			
			break;
	}
	

  return ddv ;
}



//prikaz na "dobivate poeni" za odbrana opcija od ponuda
function dealOptionDobivatePoeniDisplay(price_discount, price_voucher, tip_danocna_sema, ddv_stapka, tip) 
{
	var retStr = "";
	var poeniVal = 0;
	
	var points_osnova = points_osnova_funkcija(price_discount, price_voucher, tip_danocna_sema, ddv_stapka, tip);
	
	//presmetaj ddv za eden vaucer
	if(points_osnova > 9)
	{
		retStr += "<p>";
		retStr += "Со купување на овој ваучер добивате ";
		
		poeniVal = points_funkcija(points_osnova, 1);
		
		retStr += poeniVal + ' "kupinapopust" ';
		
		if(poeniVal == 1)
			retStr += 'поен.';
		else
			retStr += 'поени.';
		
		retStr += "</p>";
	}
		
	
	
	return retStr ;
}


function updateInstallmentDD(selectElementName = "installment", sumaZaNaplata = 0) {
    
	//IZBRISI GI SITE OPCII NA POCETOK
    $('#'+selectElementName)
    .find('option')
    .remove();

    // alert(sumaZaNaplata);
    
    //PO DEFAULT - EDNOKRATNO
    $('#'+selectElementName).append('<option value="1">Еднократно</option>');

    if(sumaZaNaplata >= 2000)
	{
		$('#'+selectElementName).append('<option value="2">2 рати</option>');
		$('#'+selectElementName).append('<option value="4">4 рати</option>');
		$('#'+selectElementName).append('<option value="6">6 рати</option>');
	}

    if(sumaZaNaplata >= 6000)
	{
		$('#'+selectElementName).append('<option value="10">10 рати</option>');
		$('#'+selectElementName).append('<option value="12">12 рати</option>');
	}

    //selektiraj ja default-nata
    $('#'+selectElementName).val('1');

}


function cardOnFileFunction(emailElementName = "Email", selectElementName = "saved_cc", divCardOnFileRow = "card_on_file_row_id") {

	email = $("#"+emailElementName).val();

	// alert(email);

    jQuery.ajax({
        url: '/buy/ajax_card_on_file',
        data: { email: email },
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            if (data && data.status == 'success') {
            	// alert('#td_'+voucher_id);
            	// $('#td_'+voucher_id).html(data.retStr);

            	// alert(data.cardTokensDropDownStr);

				//IZBRISI GI SITE OPCII NA POCETOK
			    $('#'+selectElementName).find('option').remove();

            	//popolni go dropdown-ot so zacuvanite platezni karticki
            	$('#'+selectElementName).append(data.cardTokensDropDownStr);

            	//dodadi opcija - nova platezna karticka
            	$('#'+selectElementName).append('<option value="0">Нова платежна картичка</option>');

			    //selektiraj ja default-nata
			    $('#'+selectElementName+' option:first-child').attr("selected", "selected");

			    //dokolku ima zacuvano platezni karticki togas krig do DD-to sto prasuva dali da se zacuva plateznata karticka
			    showHideDivSaveCreditCard(selectElementName, divCardOnFileRow);
            	
            } else {
                
            }
        }// end successful POST function
    }); // end jQuery ajax call

}


function showHideDivSaveCreditCard(selectElementName = "saved_cc", divName = "card_on_file_row_id")
{
    if($('#'+selectElementName).val() != "0")
		$('#'+divName).hide();
	else
		$('#'+divName).show();

}