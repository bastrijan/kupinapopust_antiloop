function updateValues(checkAmoutnPay) {

    if ($("#points").length > 0)
        pointSelected =$("#points").val();
    else
        pointSelected = 0;

    base_price = $("#AmountToPay").val();
    amountPay = $("#amount").val() * base_price;
    
    // priceDiscount = $("#priceDiscount").val();
    // priceVoucher = $("#priceVoucher").val();
    // tipDanocnaSema = $("#tipDanocnaSema").val();
    // ddvStapka = $("#ddvStapka").val();
    // tipPonuda = $("#tipPonuda").val();
    
    if (checkAmoutnPay && amountPay < 150)
    {
       alert("Почитувани, поени може да се искористат само за плаќања поголеми од 150 денари.");

       $("#points").val(0);
	   $("#dealprice").text(amountPay);
	   
	   // points_osnova = points_osnova_funkcija(priceDiscount, priceVoucher, tipDanocnaSema, ddvStapka, tipPonuda);
	   // $("#credits").text(points_funkcija(points_osnova, $("#amount").val()));
	   
       return false;
    }
    
    if (pointSelected > amountPay) {
       
       alert("Почитувани, Ве молиме изберете исто или помалку поени од цената на понудата.");
       
       $("#points").val(0);
	   $("#dealprice").text(amountPay);
	   
	   // points_osnova = points_osnova_funkcija(priceDiscount, priceVoucher, tipDanocnaSema, ddvStapka, tipPonuda);
	   // $("#credits").text(points_funkcija(points_osnova, $("#amount").val()));
	   return false;
    }
    
    if ($("#points").length > 0)
        amountPay = amountPay - pointSelected;        
    

// alert($("#points").length);
    // points_osnova = points_osnova_funkcija(priceDiscount, priceVoucher, tipDanocnaSema, ddvStapka, tipPonuda);
    // $("#credits").text(points_funkcija(points_osnova, $("#amount").val()));
    $("#dealprice").text(amountPay);
    
    //odreduvanje na dozvolen broj na rati
    if (pointSelected == 0)
    {
        updateInstallmentDD("installment", amountPay );
        // $("#installment_row_id").show();
    }
    else
    {
        updateInstallmentDD("installment", 0 );
        // $("#installment_row_id").hide();
    }
}

function SubmitForm() {
  $("#paymentform").submit();
}


$(document).ready(function() {

    if($("#points").length == 0)
        $("#points").val(0);

    $("#pay").click(function (){

        //normalno kupuvanje
        if (validateEmail($("#Email").val())) {
      
            SubmitForm();
                
        }
        return false;

    })

    $("#email_field").focus(function (){
        defaultText = $("#defaulttext").val();
        if ($(this).val() == defaultText) {
            $(this).val("");
        }
    })
    $("#email_field").blur(function (){
        defaultText = '<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>';
        if ($(this).val() == "") {
            $(this).val(defaultText);
        }
    })
    
    $("#amount").change(function (){
        updateValues(false);
    });
    $("#points").change(function (){
        updateValues(true);
    });

    updateInstallmentDD("installment", $("#amount").val() * $("#AmountToPay").val() );

    //otkako kje se vnese email se povikuva ovoj event
    $("#Email").on('input', function() { 
        // alert("ddd");
        if($("#card_on_file_row_id").is(":visible"))
            cardOnFileFunction("Email", "saved_cc", "card_on_file_row_id");
    });

    $("#saved_cc").change(function (){
        showHideDivSaveCreditCard("saved_cc", "card_on_file_row_id");
    });

});