(function($) {
	$.countdown.regional['mk'] = {
		labels: ['години', 'месеци', 'недели', 'дена', 'ч.', 'мин.', 'сек.'],
		labels1: ['година', 'месец', 'недела', 'ден', 'ч.', 'мин.', 'сек.'],
		compactLabels: ['г.', 'м.', 'н.', 'д.'],
		timeSeparator: ':', isRTL: false};
	$.countdown.setDefaults($.countdown.regional['mk']);
})(jQuery);
                                      