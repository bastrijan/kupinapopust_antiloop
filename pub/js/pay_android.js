function validateEmail(email) {
    if (!email) {
        $( "#invalidmailPopup" ).popup( "open" );
        return false;
    }
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(email) == false) {
        $( "#invalidmailPopup" ).popup( "open" );
        return false;
    }
    return true;
}

//osnova za poenit za EDEN vaucer
function points_osnova_funkcija(price_discount, provizija, tip_danocna_sema, ddv_stapka, tip_ponuda) {
	
	osnova = 0;
	
	//presmetaj ddv za eden vaucer
	ddv = ddv_funkcija(price_discount, provizija, tip_danocna_sema, ddv_stapka, tip_ponuda, 1);
	
	//presmetaj poeni
	//if(tip_danocna_sema == "�� ��� ��������" && tip_ponuda == 'cela_cena')
	//	osnova = 0;
	//else
		osnova = provizija - ddv;
	
	return osnova ;
}

//presmetaka na poenite zavisno kolku e brojot na vauceri
function points_funkcija(osnova, broj_vauceri) {

		points_return = 0;
		points = osnova*0.1;
		
		points_return = Math.floor(points)*broj_vauceri;
		
		return points_return ;
		
		//po staro
        //return Math.floor(price*0.1)*numberOfVouchers ;
}

//go vrakja presmetaniot DDV danok izrazeno vo denari spored kriteriumite. 
//Ako se vnese broj na vauceri => go dava vkupniot danok za site vauceri. Ako ne se vnese broj na vauceri => go dava DDV-to za poedinecen vaucer
function ddv_funkcija(price_discount, provizija, tip_danocna_sema, ddv_stapka, tip_ponuda, broj_vauceri) {
	
	ddv = 0;
	
	//cena_na_ponudata = (tip_ponuda == 'cena_na_vaucer') ? provizija : price_discount;
	
	switch (tip_danocna_sema) {
		case "��� ��������":
			
			//presmetaj ddv
			ddv = ((provizija*ddv_stapka)/100)*broj_vauceri;
				
			break;

		case "�� ��� ��������":
			
			//presmetaj ddv
			ddv = ((provizija*ddv_stapka)/100)*broj_vauceri;

			break;
		case "��������� �������� �������":
			
			//presmetaj ddv
			ddv = ((provizija*ddv_stapka)/100)*broj_vauceri;

			
			break;
		case "������ �������� �� ���������":
			
			//presmetaj ddv
			//tuka ne se presmetuva ddv. ddv-to e nula
			
			break;
	}
	

  return ddv ;
}


function updateValues(checkAmoutnPay) {
    pointSelected =$("#points").val()
    base_price = $("#AmountToPay").val();
    amountPay = $("#amount").val() * base_price;
    
    priceDiscount = $("#priceDiscount").val();
    priceVoucher = $("#priceVoucher").val();
    tipDanocnaSema = $("#tipDanocnaSema").val();
    ddvStapka = $("#ddvStapka").val();
    tipPonuda = $("#tipPonuda").val();
    
    if (checkAmoutnPay && amountPay < 150)
    {
       alert("����������, ����� ���� �� �� ���������� ���� �� ������� �������� �� 150 ������.");

       $("#points").val(0);
	   $("#dealprice").text(amountPay);
	   
	   points_osnova = points_osnova_funkcija(priceDiscount, priceVoucher, tipDanocnaSema, ddvStapka, tipPonuda);
	   $("#credits").text(points_funkcija(points_osnova, $("#amount").val()));
	   
       return false;
    }
    
    if (pointSelected > amountPay) {
       
       alert("����������, �� ������ �������� ���� ��� ������� ����� �� ������ �� ��������.");
       
       $("#points").val(0);
	   $("#dealprice").text(amountPay);
	   
	   points_osnova = points_osnova_funkcija(priceDiscount, priceVoucher, tipDanocnaSema, ddvStapka, tipPonuda);
	   $("#credits").text(points_funkcija(points_osnova, $("#amount").val()));
	   return false;
    }
    
    if ($("#points").length > 0)
        amountPay = amountPay - pointSelected;        
    

// alert($("#points").length);
    points_osnova = points_osnova_funkcija(priceDiscount, priceVoucher, tipDanocnaSema, ddvStapka, tipPonuda);
    $("#credits").text(points_funkcija(points_osnova, $("#amount").val()));
    $("#dealprice").text(amountPay);
    
}

function SubmitForm() {
  $("#paymentform").submit();
}
  
$(document).ready(function() {

    $("#pay").click(function (){
        
        //privremeno za da se zabrani kupuvanjeto
        if(false)
        {
			$( "#baniraj_plakjanje" ).dialog(
			{
				width: 950,
				modal: true,
				resizable: false,
				zIndex: 3999999,
				closeOnEscape: false,
				open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }

			});
                                      
                                      
            $("#instrukcii_baniraj").click(function (){
                   
                $("#baniraj_plakjanje").dialog( "close" );
            })
            
            return false;
        }
        
        //normalno kupuvanje
        if (validateEmail($("#Email").val())) {
      
            if($("#points").length == 0)
                $("#points").val(0);

            if ($("#dialog-info-message").length > 0 && ($("#points").val() != ($("#AmountToPay").val() * $("#amount").val())))
            {
                if($('#payment-0').is(':checked')) 
                { 
                    /* PRIVREMENO NE SE PRIKAZUVA DIALOG BOX-OT
                    $( "#dialog-info-message" ).dialog(
                    {
                        width: 950,
                        modal: true,
                        resizable: false,
                        zIndex: 3999999,
                        closeOnEscape: false,
                        open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }
     
                    });
                    
                    
                    $(":button:contains('Ok')").hide();
                    
                    
                    // button pane style
                   
                    $("#dialog-info-message").dialog("widget").find(".ui-dialog-buttonpane")
                                      .css({"border":"0","padding":"0","margin":"0"} );
                   
                        
                    $("#instrukcii_ok").click(function (){
                            SubmitForm();

                    });
                     */
                     
                    SubmitForm();
                 
                }
                else
                     SubmitForm();

            }
            else
                SubmitForm();
                
        }
        return false;

    })

    $("#email_field").focus(function (){
        defaultText = $("#defaulttext").val();
        if ($(this).val() == defaultText) {
            $(this).val("");
        }
    })
    $("#email_field").blur(function (){
        defaultText = '<?php print kohana::lang("prevod.������� �� ������ e-mail ������"); ?>';
        if ($(this).val() == "") {
            $(this).val(defaultText);
        }
    })
    
    $("#amount").change(function (){
        updateValues(false);
    });
    $("#points").change(function (){
        updateValues(true);
    });
});