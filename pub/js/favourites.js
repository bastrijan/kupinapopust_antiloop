$(document)
	.ready(function () {
		$(".fav-button").bind("click", FavoritesHandler);
		$(".fav-remove").bind("click", FavoritesRemoveHandler);
		
		SetFavouritesMenuLink();
	})
	.ajaxSuccess(function() {
		//za da moze i ponudite dobieni preku ajax paging da dobijat tootip
		$('[data-toggle="tooltip"]').tooltip(); 
	    
		//ova e staro, vo slucaj da mora prvo da se napravi unbind pa posle bind
	    //$(document.body).find(".fav-button").unbind("click", FavoritesHandler);

	    //za da moze i ponudite dobieni preku ajax paging da dobijat click event do FavoritesHandler
		$(document.body).find(".fav-button").bind("click", FavoritesHandler);
	});

function FavoritesHandler(event) {

	event.preventDefault();
	
	var the_target = $(event.target);

	//dokolku klikne tocno na zvezdata (<i class="fa fa-star"></i>) togas preku <i> tagot da se zeme negoviot parent (<a> tagot) i potoa se si prodolzuva po staro.
	if (the_target.is("i")) {
		the_target = the_target.parent();
	}
	
	//ako ima (fav-active) klasa toa znaci deka vekje e stavena vo OMILENI i treba da se trgne
	if (the_target.hasClass("fav-active")) {
		// REMOVE from favourites
		the_target.removeClass("fav-active");
		// change appearance
		the_target.css("color", "#333");
		// change tool-tip
		the_target.tooltip("hide")
			.attr("data-original-title", "Додади во омилени")
			;
		
		// get the ID of the deal
		var deal_id = the_target.children(".infoDealID").html();

		// get cookie
		var cookieValue = $.cookie("kupinapopust_fav");
		
		// check if the cookie exists
		if (cookieValue != undefined) {
			// cookie exists
			var fav_deals = JSON.parse(cookieValue);
			// remove the ID from the array
			fav_deals = jQuery.grep(fav_deals, function(value) {
				return value != deal_id;
			});
			if (fav_deals.length === 0) {
				// delete the cookie
				$.removeCookie("kupinapopust_fav", { path: '/' });
			} else {
				// store the modified array in the cookie
				$.cookie("kupinapopust_fav", JSON.stringify(fav_deals), { expires: 365, path: "/" });
			}
		}
		
	} else { //ako nema (fav-active) klasa toa znaci deka treba da se dodade vo OMILENi
		// ADD to favourites
		the_target.addClass("fav-active");
		// change appearance
		the_target.css("color", "white");
		// change tool-tip
		the_target.tooltip("hide")
			.attr("data-original-title", "Одземи од омилени")
			;
		
		// get the ID of the deal
		var deal_id = the_target.children(".infoDealID").html();

		// get the cookie
		var cookieValue = $.cookie("kupinapopust_fav");
		
		// check if the cookie exists
		if (cookieValue == undefined) {
		  // cookie is not set
		  var fav_deals = new Array();
		  fav_deals.push(deal_id);
		  $.cookie("kupinapopust_fav", JSON.stringify(fav_deals), { expires: 365, path: "/" });
		} else {
			// the cookie exists, so get the stored array
			var fav_deals = JSON.parse(cookieValue);
			// add the new ID
			fav_deals.push(deal_id);
			// store the modified array in the cookie
			$.cookie("kupinapopust_fav", JSON.stringify(fav_deals), { expires: 365, path: "/" });
		}
	}
	
	SetFavouritesMenuLink();
}

function FavoritesRemoveHandler(event) {

	event.preventDefault();
	
	var the_target = $(event.target);
	if (the_target.is("i")) {
		the_target = the_target.parent();
	}
	
	// get the ID of the deal
	var deal_id = the_target.children(".infoDealID").html();
	// get cookie
	var cookieValue = $.cookie("kupinapopust_fav");
	
	// check if the cookie exists
	if (cookieValue != undefined) {
		// cookie exists, get the array of favourites offers
		var fav_deals = JSON.parse(cookieValue);			

		// remove the ID from the array
		fav_deals = jQuery.grep(fav_deals, function(value) {
			return value != deal_id;
		});
		
		// store the modified array in the cookie
		$.cookie("kupinapopust_fav", JSON.stringify(fav_deals), { expires: 365, path: "/" });
	}

	//da se prikaze slikata za reload
	var img = the_target.closest(".product-thumb").children(".product-header").find("img");
	var h = img.height();
	
	img.attr("src", "/pub/img/loader.gif");
	img.height(h).width(h);
	
	//za reload (staro)
	//setTimeout(function() { location.reload(true); }, 500);

	//za brisenje bez odenje online
	$("#"+deal_id).remove();

	SetFavouritesMenuLink();
}


//Go ureduva kopceto OMILENI vo glavnoto meni (so increment i dicrement na brojacot na omileni)
function SetFavouritesMenuLink() 
{
	
	var target_i = $("#fav-menu-link").children("i");
	var target_span = $("#fav-menu-link").children("span");
	
	// get cookie
	var cookieValue = $.cookie("kupinapopust_fav");
	
	target_span.html("");
	if ( target_i.hasClass("fav-menu-active") ) {
		target_i.removeClass("fav-menu-active");
	}

	// check if the cookie exists
	if (cookieValue != undefined) {
		// cookie exists
		var fav_deals = JSON.parse(cookieValue);
		// get the number of favourite offers
		var n = fav_deals.length;


		if (n > 0) 
		{
			target_span.html(n);

			if (!target_i.hasClass("fav-menu-active") ) {
				target_i.addClass("fav-menu-active");
			}
		} 

	} 

}

/*
function SetFavouritesMenuLink() {
	
	var target = $("#fav-menu-link").children("i");
	
	// get cookie
	var cookieValue = $.cookie("kupinapopust_fav");
	
	// check if the cookie exists
	if (cookieValue != undefined) {
		// cookie exists
		var fav_deals = JSON.parse(cookieValue);
		// get the number of favourite offers
		var n = fav_deals.length;
		
		if (n > 0) {
			target.siblings("span").replaceWith("<span>Омилени - " + n + "</span>");
			if ( ! target.hasClass("fav-menu-active") ) {
				target.addClass("fav-menu-active");
			}
		} else {
			target.siblings("span").replaceWith("<span>Омилени</span>");
			if ( target.hasClass("fav-menu-active") ) {
				target.removeClass("fav-menu-active");
			}
		}
	} else {
		target.siblings("span").replaceWith("<span>Омилени</span>");
		if ( target.hasClass("fav-menu-active") ) {
			target.removeClass("fav-menu-active");
		}
	}
}
*/