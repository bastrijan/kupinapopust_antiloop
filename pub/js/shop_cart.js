$(document)
	.ready(function () {
		$(".shop-cart-button").bind("click", ShopCartHandler);
		$(".shop-cart-remove").bind("click", ShopCartRemoveHandler);
		$(".shop-cart-many-options").bind("click", ShopCartManyOptionsHandler);
		
		SetShopCartMenuLink();
	})
	.ajaxSuccess(function() {
		//za da moze i ponudite dobieni preku ajax paging da dobijat tootip
		$('[data-toggle="tooltip"]').tooltip(); 
	    
		//ova e staro, vo slucaj da mora prvo da se napravi unbind pa posle bind
	    //$(document.body).find(".fav-button").unbind("click", ShopCartHandler);

	    //za da moze i ponudite dobieni preku ajax paging da dobijat click event do ShopCartHandler
		$(document.body).find(".shop-cart-button").bind("click", ShopCartHandler);
	});


function ShopCartManyOptionsHandler(event) {

	event.preventDefault();
	
	var the_target = $(event.target);

	//dokolku klikne tocno na zvezdata (<i class="fa fa-star"></i>) togas preku <i> tagot da se zeme negoviot parent (<a> tagot) i potoa se si prodolzuva po staro.
	if (the_target.is("i")) {
		the_target = the_target.parent();
	}
	
	alert("Кликнете на понудата и изберете која опција сакате да ја вклучите во кошничката.");

}

function ShopCartHandler(event) {

	event.preventDefault();
	
	var the_target = $(event.target);

	//dokolku klikne tocno na zvezdata (<i class="fa fa-star"></i>) togas preku <i> tagot da se zeme negoviot parent (<a> tagot) i potoa se si prodolzuva po staro.
	if (the_target.is("i")) {
		the_target = the_target.parent();
	}
	
	//ako ima (fav-active) klasa toa znaci deka vekje e stavena vo OMILENI i treba da se trgne
	if (the_target.hasClass("shop-cart-active")) {
		// REMOVE from favourites
		the_target.removeClass("shop-cart-active");
		// change appearance
		the_target.css("color", "#333");
		// change tool-tip
		the_target.tooltip("hide")
			.attr("data-original-title", "Додади во кошничка")
			;
		
		// get the ID of the deal
		var deal_option_id = the_target.children(".infoDealOptionID").html();

		// get cookie
		var cookieValue = $.cookie("kupinapopust_shop_cart");
		
		// check if the cookie exists
		if (cookieValue != undefined) {
			// cookie exists
			var shop_cart_deals = JSON.parse(cookieValue);
			// remove the ID from the array
			shop_cart_deals = jQuery.grep(shop_cart_deals, function(value) {
				return value != deal_option_id;
			});
			if (shop_cart_deals.length === 0) {
				// delete the cookie
				$.removeCookie("kupinapopust_shop_cart", { path: '/' });
			} else {
				// store the modified array in the cookie
				$.cookie("kupinapopust_shop_cart", JSON.stringify(shop_cart_deals), { expires: 365, path: "/" });
			}
		}
		
	} else { //ako nema (fav-active) klasa toa znaci deka treba da se dodade vo OMILENi
		// ADD to favourites
		the_target.addClass("shop-cart-active");
		// change appearance
		the_target.css("color", "white");
		// change tool-tip
		the_target.tooltip("hide")
			.attr("data-original-title", "Одземи од кошничка")
			;
		
		// get the ID of the deal option
		var deal_option_id = the_target.children(".infoDealOptionID").html();

		// get the cookie
		var cookieValue = $.cookie("kupinapopust_shop_cart");
		
		// check if the cookie exists
		if (cookieValue == undefined) {
		  // cookie is not set
		  var shop_cart_deals = new Array();
		  shop_cart_deals.push(deal_option_id);
		  $.cookie("kupinapopust_shop_cart", JSON.stringify(shop_cart_deals), { expires: 365, path: "/" });
		} else {
			// the cookie exists, so get the stored array
			var shop_cart_deals = JSON.parse(cookieValue);
			// add the new ID
			shop_cart_deals.push(deal_option_id);
			// store the modified array in the cookie
			$.cookie("kupinapopust_shop_cart", JSON.stringify(shop_cart_deals), { expires: 365, path: "/" });
		}
	}
	
	SetShopCartMenuLink();
}

function ShopCartRemoveHandler(event) {

	event.preventDefault();
	
	var the_target = $(event.target);
	if (the_target.is("i")) {
		the_target = the_target.parent();
	}
	
	// get the ID of the deal
	var deal_option_id = the_target.children(".infoDealOptionID").html();
	// alert(deal_option_id+'=deal_option_id');
	// get cookie
	var cookieValue = $.cookie("kupinapopust_shop_cart");
	
	// check if the cookie exists
	if (cookieValue != undefined) {
		// cookie exists, get the array of favourites offers
		var shop_cart_deals = JSON.parse(cookieValue);			

		// alert(shop_cart_deals.length + '=shop_cart_deals.length 1');
		// remove the ID from the array
		shop_cart_deals = jQuery.grep(shop_cart_deals, function(value) {
			return value != deal_option_id;
		});

		// alert(shop_cart_deals.length + '=shop_cart_deals.length 2');
		
		// store the modified array in the cookie
		$.cookie("kupinapopust_shop_cart", JSON.stringify(shop_cart_deals), { expires: 365, path: "/" });
	}

	//da se prikaze slikata za reload
	// var img = the_target.closest(".product-thumb").children(".product-header").find("img");
	// var h = img.height();
	
	// img.attr("src", "/pub/img/loader.gif");
	// img.height(h).width(h);
	
	//za reload (staro)
	//setTimeout(function() { location.reload(true); }, 500);

	//za brisenje bez odenje online
	$("#"+deal_option_id).remove();

	//update na vkupnata suma
	updateValues(false, deal_option_id);

	//update na ikonata za shopping cart
	SetShopCartMenuLink();
}


//Go ureduva kopceto OMILENI vo glavnoto meni (so increment i dicrement na brojacot na omileni)
function SetShopCartMenuLink() 
{
	
	var target_i = $("#shop-cart-menu-link").children("i");
	var target_span = $("#shop-cart-menu-link").children("span");

	target_span.css("background-color", "#FF6200");
	
	// get cookie
	var cookieValue = $.cookie("kupinapopust_shop_cart");
	
	target_span.html("");
	// target_span.css("background-color", "#999999");
	
	if ( target_i.hasClass("shop-cart-menu-active") ) {
		target_i.removeClass("shop-cart-menu-active");
	}

	// check if the cookie exists
	if (cookieValue != undefined) {
		// cookie exists
		var shop_cart_deals = JSON.parse(cookieValue);
		// get the number of favourite offers
		var n = shop_cart_deals.length;


		if (n > 0) 
		{
			target_span.html(n);

			if (!target_i.hasClass("shop-cart-menu-active") ) {
				target_i.addClass("shop-cart-menu-active");
			}
		} 
		else
		{
			target_span.html('0');
			$("#shop-cart-common-part").hide();
		}

	}
	else
	{
		target_span.html('0');
	}

}

