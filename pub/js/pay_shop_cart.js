function updateValues(checkAmoutnPay, deal_option_id = 0) {
    
    if ($("#points").length > 0)
        pointSelected =$("#points").val();
    else
        pointSelected = 0;
    
    base_price = $("#AmountToPay_"+deal_option_id).val();
    amountPay = $("#amount_"+deal_option_id).val() * base_price;
    
    // priceDiscount = $("#priceDiscount_"+deal_option_id).val();
    // priceVoucher = $("#priceVoucher_"+deal_option_id).val();
    // tipDanocnaSema = $("#tipDanocnaSema_"+deal_option_id).val();
    // ddvStapka = $("#ddvStapka_"+deal_option_id).val();
    // tipPonuda = $("#tipPonuda_"+deal_option_id).val();
    
    if (checkAmoutnPay && amountPay < 150)
    {
       alert("Почитувани, поени може да се искористат само за плаќања поголеми од 150 денари.");

       $("#points").val(0);
	   $("#dealprice_"+deal_option_id).text(amountPay);
	   
	   // points_osnova = points_osnova_funkcija(priceDiscount, priceVoucher, tipDanocnaSema, ddvStapka, tipPonuda);
	   // $("#credits").text(points_funkcija(points_osnova, $("#amount").val()));
	   
       return false;
    }
    
    if (pointSelected > amountPay) {
       
       alert("Почитувани, Ве молиме изберете исто или помалку поени од цената на понудата.");
       
       $("#points").val(0);
	   $("#dealprice_"+deal_option_id).text(amountPay);
	   
	   // points_osnova = points_osnova_funkcija(priceDiscount, priceVoucher, tipDanocnaSema, ddvStapka, tipPonuda);
	   // $("#credits").text(points_funkcija(points_osnova, $("#amount").val()));
	   return false;
    }


    
    // alert($("#points").length);
    // points_osnova = points_osnova_funkcija(priceDiscount, priceVoucher, tipDanocnaSema, ddvStapka, tipPonuda);
    // $("#credits").text(points_funkcija(points_osnova, $("#amount").val()));
    $("#dealprice_"+deal_option_id).text(amountPay);
    

    //zemi ja totalnata suma
    dealprice_total = calcTotalShopCartValue();

    // alert("length="+$("#points").length);
    // alert("pointSelected="+pointSelected);

    if ($("#points").length > 0)
        dealprice_total = dealprice_total - pointSelected; 

    // alert("dealprice_total="+dealprice_total);

    $("#dealprice_total").text(dealprice_total);

    //odreduvanje na dozvolen broj na rati
    if (pointSelected == 0)
    {
        updateInstallmentDD("installment", dealprice_total );
        // $("#installment_row_id").show();
    }
    else
    {
        updateInstallmentDD("installment", 0 );
        // $("#installment_row_id").hide();
    }
}

function calcTotalShopCartValue() {
  retValue = 0;

  $( ".deal_option_ids" ).each(function( index, elem ) {
      deal_option_id = $(elem).val();
      retValue += $("#amount_"+deal_option_id).val() * $("#AmountToPay_"+deal_option_id).val();

  });

  return retValue;
}


function SubmitForm() {
  $("#paymentform").submit();
}
  
$(document).ready(function() {

    if($("#points").length == 0)
        $("#points").val(0);

    $("#pay").click(function (){

        //normalno kupuvanje
        if (validateEmail($("#Email").val())) {
      
            SubmitForm();
                
        }
        return false;

    })

  
    $(".amount").change(function (){
        // alert("eden");
        // alert("samiot ==="+$(this).html());
        // alert("tatko mu ==="+$(this).parent().html());


        grand_parent = $(this).parent().parent().parent().parent();

        deal_option_id = grand_parent.find(".infoDealOptionID").html();

        // alert(deal_option_id);

        updateValues(false, deal_option_id);
    });

    $("#points").change(function (){
        // alert("vo points");
        
        updateValues(true);
    });


    var emailFriendValidators = {
                validators: {
                    notEmpty: {
                        message: 'Полето за Email адреса на пријателот мора да биде пополнето'
                          }, // notEmpty
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'Е-mail адресата на пријателот не е валидна.'
                    }
                }
            }

    $(".kupi-kako-podarok-shop-cart").click(function (event){

        event.preventDefault();

        grand_parent = $(this).parent().parent();
        grand_grand_parent = grand_parent.parent();
        deal_option_id = grand_grand_parent.find(".infoDealOptionID").html();
        target_element = $("#kupi-kako-podarok-elements_" + deal_option_id);

        // console.log(grand_parent.html());
        // console.log(grand_grand_parent.html());
        // alert("deal_option_id="+deal_option_id);

        grand_parent.hide();
        target_element.show();

        //postavi go hidden elementot za GIFT na 1
        $("#gift_" + deal_option_id).val(1);

        //postavi validacija za Email adresata na prijatelot
        $('#paymentform').bootstrapValidator('addField', 'email_friend_' + deal_option_id , emailFriendValidators);

    });

    updateInstallmentDD("installment", calcTotalShopCartValue() );

    //otkako kje se vnese email se povikuva ovoj event
    $("#Email").on('input', function() { 
        // alert("ddd");
        if($("#card_on_file_row_id").is(":visible"))
            cardOnFileFunction("Email", "saved_cc", "card_on_file_row_id");
    });

    $("#saved_cc").change(function (){
        showHideDivSaveCreditCard("saved_cc", "card_on_file_row_id");
    });

});