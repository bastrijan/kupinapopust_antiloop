<?php defined('SYSPATH') OR die('No direct access allowed.') ; ?>
<?php //require_once 'menu.php' ; ?>
<div class="container_12 homepage-billboard">
  <div class="clearfix" style="margin-bottom: 30px">
    <div class="grid_12 alpha" style="border: black 1px dotted">
      <?php
      if ($dealData) {
        print "<strong>" .  ($dealData->options_cnt > 1 ? $dealData->title_mk_deal." - ": "").$dealData->title_mk  . "</strong>" ;
        print "<br/>" ;
        print "<strong>Период на искористување: " .  date("d/m/Y", strtotime($dealData->valid_from))." - ".date("d/m/Y", strtotime($dealData->valid_to))  . "</strong>" ;
        print "<br/>" ;
      }
      ?>
    </div>
    <div class="grid_12 alpha" style="border: black 1px dotted">
    <br/>
    
      <?php
      if ($vouchers) {
        $customerModel = new Customer_Model() ;
        foreach (array_reverse($vouchers) as $voucher) {
            
            // $checked = '';
            // if($voucher->used == "1"){
            //     $checked = 'checked';
            // }
    			  // print '<input  type="checkbox" '. $checked .' name="voucherUsed[]" value="'.$voucher->id.'">Искористен (Marked as used)<br/>';	

        if($voucher->confirm_paid_off)
          echo "<strong>Потврденo од ваша страна како исплатен</strong>";
        elseif($voucher->paid_off)
          echo "<strong>Маркиранo од kupinapopust.mk како исплатен</strong>";
        elseif($voucher->used)
          echo "<strong>Искористен</strong>";
        elseif(time() > strtotime($dealData->valid_to))
            echo "<strong style='color: red;'>Истечен</strong>";
          else
            echo "<strong>Неискористен</strong>";

        echo "<br/>";  

        print "<strong>" . $customerModel->getCustomerMail($voucher->customer_id) . "</strong>&nbsp;&nbsp;&nbsp;&nbsp;" ;
			  print "<strong>" . $voucher->code . "</strong>&nbsp;&nbsp;&nbsp;&nbsp;" ;
			  print "&nbsp;&nbsp;&nbsp;&nbsp;Креиран на (Created):&nbsp;" . date("d/m/Y H:i", strtotime($voucher->time)) ;
			  print "<hr>" ;
        }
      }
      else {
        print 'Нема ваучери во моментов' . "    " ;
      }

      //print html::anchor("/admin/deal", "Креирај попуст");
      ?>

    </div>    
  </div>
</div>