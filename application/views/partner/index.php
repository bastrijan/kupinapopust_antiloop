<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>


<!-- ZA ISPLATA -->
<div class="col-md-12 bg-white table-wrapper">
	ИСПЛАТЕНО: <strong><?php echo $sumaZaPotvrdaNaIsplata; ?> ден.</strong>	
	&nbsp;&nbsp;
	Ве молиме кликнете <a href="/partner/confirm_as_paid" target="_blank">овде</a> за да ја потврдите исплатата.
</div>

<div class="gap"></div>

<div class="col-md-12">
   <div class="product-sort">
		<span class="product-sort-selected">
			
		<?php 
			$selected_name = "активни понуди";

			if($selected == "all")
				$selected_name = "сите понуди";
			elseif($selected == "past")
				$selected_name = "изминати понуди";

			print "филтрирај по <b>".$selected_name."</b>";
		?>
		</span>
			<span class="product-sort-order fa fa-angle-down"></span>
			<ul>
				<li><a href="/partner?type=all">сите понуди</a>
				</li>
				<li><a href="/partner?type=current">активни понуди</a>
				</li>
				<li><a href="/partner?type=past">изминати понуди</a>
				</li>						  
			</ul>
		<div class="gap-mini hidden-md hidden-lg"></div>
		<!--
		<button class="btn green ml20">ПРЕВЗЕМИ ФАКТУРА</button>
		-->
	</div>
</div>

 <div class="col-md-12 bg-white table-wrapper">
	<table class="table table-order">
		<thead>
			<tr>
				<!--   <th> ПОНУДА </th>   -->
				<th class="hidden-xs hidden-sm">Бр.</th>
				<th>Наслов на понуда и линк до истата</th>
				<th class="hidden-xs hidden-sm">Купени</th>
				<th class="hidden-xs hidden-sm">Почеток</th>
				<th class="hidden-xs hidden-sm">Крај</th>
				<th class="hidden-xs hidden-sm">Период на искористување</th>
				<th>Детали</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$ponuda_br = 0;
			
            if ($dealsData) {
				foreach ($dealsData as $voucher) {
					$ponuda_br++;
			?>
					<tr>
						<td class="hidden-xs hidden-sm"><?php echo $ponuda_br; ?></td>
						<td>
							<a href="/partner/dealdetails/<?php echo $voucher->deal_id; ?>/<?php echo $voucher->deal_option_id;?>" ><?php echo ($voucher->options_cnt > 1 ? $voucher->title_mk_deal." - ": "").$voucher->title_mk ; ?></a>
							<span class="hidden-md hidden-lg block"><b>Понуда бр.</b> <?php echo $ponuda_br; ?></span> 
							<span class="hidden-md hidden-lg block"><b>Купени:</b> <?php echo $vouchers_count[$voucher->deal_id][$voucher->deal_option_id]; ?></span>
							<span class="hidden-md hidden-lg block"><b>Почеток:</b> <?php echo date("d/m/Y", strtotime($voucher->start_time)); ?></span>
							<span class="hidden-md hidden-lg block"><b>Крај:</b> <?php echo date("d/m/Y", strtotime($voucher->end_time)); ?></span>
							<span class="hidden-md hidden-lg block mb10"><b>Период на искористување:</b> <?php echo date("d/m/Y", strtotime($voucher->valid_from))." - ".date("d/m/Y", strtotime($voucher->valid_to)); ?></span>				
						</td>
						<td class="hidden-xs hidden-sm"><?php echo $vouchers_count[$voucher->deal_id][$voucher->deal_option_id]; ?></td>
						<td class="width-date hidden-xs hidden-sm"  data-sort="<?php echo $voucher->start_time; ?>"><?php echo date("d/m/Y", strtotime($voucher->start_time)); ?></td>
						<td class="width-date hidden-xs hidden-sm"  data-sort="<?php echo $voucher->end_time; ?>"><?php echo date("d/m/Y", strtotime($voucher->end_time)); ?></td>
						<td class="width-date hidden-xs hidden-sm"  ><?php echo date("d/m/Y", strtotime($voucher->valid_from))." - ".date("d/m/Y", strtotime($voucher->valid_to)); ?></td>
						<td class="width-details">
							<a href="/partner/dealdetails/<?php echo $voucher->deal_id; ?>/<?php echo $voucher->deal_option_id;?>" class="btn btn-sm green" data-toggle="tooltip" data-placement="top" data-title="Детали" data-original-title="" title=""><i class="fa fa-bars"></i></a>
							<a href="/partner/printvoucherlist/<?php echo $voucher->deal_id; ?>/<?php echo $voucher->deal_option_id;?>" target="_blank" class="btn btn-sm blue" data-toggle="tooltip" data-placement="top" data-title="Испринтај детали" data-original-title="" title=""><i class="fa  fa-print"></i></a> 
						</td>
					</tr>
			<?php
				} // END OF: foreach ($dealsData as $voucher)
			} // END OF: if ($dealsData)
			?>
		</tbody>
	</table>
</div>




<script type="text/javascript">

	$(document).ready(function () {

		$('.table-order').dataTable({
			paging: false,
			'aoColumnDefs': [{
				'bSortable': false,
				'aTargets': [-1, -2, 1] /* 1st one, start by the right */
			}]
		});


	});
</script>