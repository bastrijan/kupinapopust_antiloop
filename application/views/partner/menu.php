<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<?php 
$userID = $this->session->get("user_id"); 
$partnerName = $this->session->get("partner_name"); 
?>
<style>
    .admin_menu li {
        display: inline;
        padding: 0px 0px !important;
    }
</style>
<div class="container_12 homepage-billboard">
	<div style="width: 940px;">
	<ul class="admin_menu">
	<li style="float: left;">Партнер (Partner): <?php print "<strong>$partnerName</strong>"; ?></li>
    <li style="float: right;">
		<a  href="/partner/changepassword">Промени лозинка (Change Password)</a>
		&nbsp;|&nbsp;
		<a  href="/partner/logout/<?php print base64_encode("/partner"); ?>">Logout</a>
	</li>
			   	
	</ul>
    </div>
	

    <div class="grid_10 alpha omega" style="width: 800px">
        <ul class="admin_menu">
            <li><?php print html::anchor("/partner", "Понуди (Offers)"); ?></li>
        </ul>

    </div>
 
</div>
