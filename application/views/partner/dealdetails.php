<?php defined('SYSPATH') OR die('No direct access allowed.') ; ?>

<style type="text/css">
	.green_custom { background-color:#8cc732 !important; color:white;}

	input[type=checkbox] {
	  /* All browsers except webkit*/
	  transform: scale(2);

	  /* Webkit browsers*/
	  -webkit-transform: scale(2);
	}

</style>

<script type="text/javascript">
	<?php if (isset($type) and $type == 1) { ?>
	    $(document).ready(function () {
			$().toastmessage('showToast', {
				text     : "Промените се успешно зачувани.",
				sticky   : false,
				position : 'middle-center',
				type     : 'success'
			});
	    }) 
	<?php } ?>     
</script>


<?php
	if ($dealData) {
?>	  
	<div class="col-md-12">
		<h3>Детали за понуда:   <a href="/partner/printvoucherlist/<?php echo $dealData->deal_id; ?>/<?php echo $dealData->deal_option_id; ?>" target="_blank" class="btn btn-sm blue" data-toggle="tooltip" data-placement="top" data-title="Испринтај детали" data-original-title="" title=""><i class="fa  fa-print"></i></a> </h3>
		<h4>
			<?php echo  ($dealData->options_cnt > 1 ? $dealData->title_mk_deal." - ": "").$dealData->title_mk ; ?>
		</h4>
		<div class="gap"></div>
		<h5>
			Период на искористување: <?php echo date("d/m/Y", strtotime($dealData->valid_from))." - ".date("d/m/Y", strtotime($dealData->valid_to)); ?>			
		</h5>

		<h5>
			Забелешка: Податоците се зачувуваат автоматски во системот со самото штиклирање (отштиклирање) на полето "Маркирај како искористен"			
		</h5>		
	</div>

	<div class="col-md-12 bg-white table-wrapper">
		<?php
		if ($vouchers) 
		{
				$customerModel = new Customer_Model() ;
				$br = 0;
		?>
			<form id="myform" action="" method="post" >	
				<table class="table table-order">
					<thead>
						<tr>
							<th>Бр.</th>
							<th>E-mail на корисник</th>
							<th class="hidden-xs hidden-sm">Код на ваучерот</th>
							<th class="hidden-xs hidden-sm">Купен на</th>
							<th class="hidden-xs hidden-sm">Статус</th>  
							<th>Маркирај како искористен</th>                                 
						</tr>
					</thead>

					<tbody>
					<?php
					foreach ($vouchers as $voucher) 
					{
						$br++;
					?>  
						<tr <?php if($voucher->used == 1) echo 'class="green_custom"'; ?>>
							<td><?php echo $br; ?></td>
							<td><?php echo $customerModel->getCustomerMail($voucher->customer_id); ?>
							   <span class="hidden-md hidden-lg block">Код: <?php echo $voucher->code; ?></span>
							   <span class="hidden-md hidden-lg block">Купен на: <?php echo date("d/m/Y H:i", strtotime($voucher->time)); ?></span>
							   <span class="hidden-md hidden-lg block">
							   		Статус: 
							   		<?php 
							   			if($voucher->confirm_paid_off)
							   				echo "<strong>Потврденo од ваша страна како исплатен</strong>";
							   			elseif($voucher->paid_off)
							   				echo "<strong>Маркиранo од kupinapopust.mk како исплатен</strong>";
							   			elseif($voucher->used)
							   				echo "<strong>Искористен</strong>";
							   			elseif(time() > strtotime($dealData->valid_to))
							   					echo "<strong style='color: red;'>Истечен</strong>";
							   				else
							   					echo "<strong>Неискористен</strong>";
							   		?>
							   		
							   	</span>
							</td>
							<td class="hidden-xs hidden-sm"><?php echo $voucher->code; ?></td>
							<td class="width-date hidden-xs hidden-sm" data-sort="<?php echo $voucher->time; ?>"><?php echo date("d/m/Y H:i", strtotime($voucher->time)); ?></td>
							<td class="hidden-xs hidden-sm" id="td_<?php echo $voucher->id; ?>">
						   		<?php 
						   			if($voucher->confirm_paid_off)
						   				echo "<strong>Потврденo од ваша страна како исплатен</strong>";
						   			elseif($voucher->paid_off)
						   				echo "<strong>Маркиранo од kupinapopust.mk како исплатен</strong>";
						   			elseif($voucher->used)
						   				echo "<strong>Искористен</strong>";
						   			elseif(time() > strtotime($dealData->valid_to))
						   					echo "<strong style='color: red;'>Истечен</strong>";
						   				else
						   					echo "<strong>Неискористен</strong>";
						   		?>
							</td>
							<td class="width-date">
								<?php echo ($voucher->confirm_paid_off ? '<i class="fa fa-fw text-font-20px">&#xf00c</i>': ''); ?>
								&nbsp;&nbsp;<input type="checkbox" <?php echo ($voucher->used == 1 ? 'checked' : ""); ?> <?php echo ($voucher->confirm_paid_off == 1 ? 'style="display: none"' : ""); ?> name="voucherUsed[]" value="<?php echo $voucher->id; ?>" class="editor-active" /></td>                                        
						</tr>
					<?php
					} // END OF: foreach ($vouchers as $voucher)
					?>
					</tbody>
				</table>
				<input type="hidden" id="unchecked" name="unchecked" value="" />
				<!-- <button type="submit" name="changeUsed" id="changeUsed" class="btn green pull-right mt20">ЗАЧУВАЈ ПРОМЕНИ</button> -->
			</form>
	<?php
		} // END OF: if ($vouchers)
	?>

	</div>


<?php
	}
?>

	<div class="col-md-12">
		<h5>
			<strong>Забелешка: Податоците се зачувуваат автоматски во системот со самото штиклирање (отштиклирање) на полето "Маркирај како искористен"</strong>			
		</h5>		
	</div>

<!-- JAVASCRIPTI -->
<script type="text/javascript">
	$(document).on('click', '#changeUsed', function () {
		var unhecked = '';
		$('input:checkbox').not(':checked').each(function( index ) {
			unhecked += $( this ).val() + ',';
		});
		$('#unchecked').val(unhecked);
	});
</script>
     

<script type="text/javascript">
    $(document).ready(function () {

        $('.table-order').dataTable(
            {
            	paging: false,
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }],
            "oLanguage": {
                "sLengthMenu": "Прикажи _MENU_ ваучери",
                "sInfo": "Прикажани _START_ до _END_ од _TOTAL_ ваучери"
            }
        });

		$('#myform :checkbox').click(function() {
		    var $this = $(this);

		    var parent_tr = $this.parent().parent();

		    // $this will contain a reference to the checkbox   
		    if ($this.is(':checked')) {
		        // the checkbox was checked 
		        parent_tr.addClass("green_custom");
		    } else {
		        // the checkbox was unchecked
		        parent_tr.removeClass("green_custom");
		    }

		    ////////////// AUTOSAVE
	 		var voucher_id = $(this).val();
	 		var voucher_status = 0;

	        if ($(this).is(':checked')) 
	            voucher_status = 1;

	        jQuery.ajax({
	            url: '/partner/ajax_autosave_dealdetails',
	            data: { voucher_id: voucher_id, voucher_status: voucher_status },
	            type: 'POST',
	            dataType: 'json',
	            success: function (data) {
	                if (data && data.status == 'success') {
	                	// alert('#td_'+voucher_id);
	                	$('#td_'+voucher_id).html(data.retStr);
	                	
	                } else {
	                    clearInterval(setIntervals);
	                }
	            }// end successful POST function
	        }); // end jQuery ajax call

		});



    });
</script>