<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<!-- include summernote -->
<link rel="stylesheet" href="/pub/summernote/summernote.css">
<script type="text/javascript" src="/pub/summernote/summernote.js"></script>


<div class="row">
    <div class="col-md-12 ">
        <!-- LOGIN REGISTER LINKS CONTENT -->

                    <h3>Потврди како исплатено</h3>
                    <div class="gap"></div>


                    <form id="myformConfirmPaid" action="/partner/confirm_as_paid/" method="post" >
                        
                        <div class="form-group">
                            ИСПЛАТЕНО: <strong><?php echo $sumaZaPotvrdaNaIsplata; ?> ден.</strong>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <button type="submit" name="btnConfirmAsPaidOff" id="btnConfirmAsPaidOff"  onclick="return confirm('Дали сте сигурни дека сакате сумата да ја потврдите како исплатенa?')" class="btn btn-primary btn-lg">ПОТВРДИ КАКО ИСПЛАТЕНО</button>
                        </div>
   
                    </form>


                    <div class="gap"></div>  

                    <form id="myformSaveNotes" action="/partner/paid_off_notes/" method="post" >
                        
                        <div class="form-group">
                            <strong>Забелешка</strong>
                            <textarea class="paid_off_notes" name="paid_off_notes" ><?php print $partnerData[0]->paid_off_notes; ?></textarea>
                            

                            <span >(Забелешката автоматски се зачувува на секои 10 секунди)</span>
                            <br/><br/>
                            <button type="submit" name="btnNotes" id="btnNotes" class="btn btn-primary btn-lg">ЗАЧУВАЈ ЗАБЕЛЕШКА</button>
                        </div>
   
                        <input type="hidden" name="autosave" value="false">
                    </form>

    </div>

</div>


<script type="text/javascript">

    var setIntervals;

    function activateAutoSaveFromDHTML() 
    {
        if ($('input[name="autosave"]').val() == 'false') {
            $('input[name="autosave"]').val('true');
            setIntervals = setInterval(start_autosave, 10 * 1000);
        }
    };

    function start_autosave() {

        $('textarea[name="paid_off_notes"]').val($('.paid_off_notes').code());

//        var DataSer = new FormData('form');
        var DataSer = $("#myformSaveNotes").serializeArray();
        jQuery.ajax({
            url: '/partner/ajax_autosave_notes',
            data: DataSer,
            enctype: 'multipart/form-data',
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data && data.status == 'success') 
                {
                    //ako se e vo red => NE PRAVI NISTO
                } 
                else 
                {
                    //ako nesto ne e vo red => prestani da so povikuvanje na funcionalnosta za AJAX
                    clearInterval(setIntervals);
                }
            }// end successful POST function
        }); // end jQuery ajax call
    }

    $(document).ready(function () {

        $("#myformSaveNotes").change(function () {
             
            activateAutoSaveFromDHTML();
                
        });



    <?php if (isset($updateSuccess) and $updateSuccess == 1) { ?>
            $().toastmessage('showToast', {
                text     : "Промените се успешно зачувани.",
                sticky   : false,
                position : 'middle-center',
                type     : 'success'
            }); 

    <?php } ?>


      $('.paid_off_notes').summernote({
            onKeyup: function(e) {
                activateAutoSaveFromDHTML();
            },
            height: 150
      });



    });
</script>
