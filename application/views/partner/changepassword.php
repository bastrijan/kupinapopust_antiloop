<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php // require_once 'menu.php'; ?>


<div class="row">
    <div class="col-md-6 ">
        <!-- LOGIN REGISTER LINKS CONTENT -->

                    <h3>Промена на лозинка (Change Password)</h3>
                    <div class="gap"></div>

                     <?php if (isset($error)) { ?>
                            <div style="color: red; ">
                                <?php print $error; ?>
                            </div>
                        <?php } ?>
                        <?php if (isset($success)) { ?>
                            <div style="color: green; ">
                                <?php print $success; ?>
                            </div>
                        <?php } ?>
                            

                    <form method="post" action="" id="deal_save_form">
                        
                        <div class="form-group">
                            <label for="Email">Старата лозинка (Old Password)</label>
                            <input id="password" name="password"  type="password" value="" placeholder="" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label for="password">Новата лозинка (New Password)</label>
                            <input type="password" value="" id="password1" name="password1" placeholder="" class="form-control" />
                        </div>
                       <div class="form-group">
                            <label for="password">Новата лозинка - повторно (New Password - again)</label>
                            <input type="password" value="" id="password2" name="password2" placeholder="" class="form-control" />
                        </div>
   
                        <div class="form-group">
                            <input type="submit" value="Зачувај (Save)" id="pay" class="btn btn-primary btn-lg" />  
                        </div>
   
                    </form>
                    <div class="gap"></div>  
    </div>
    <div class="col-md-6 ">
    </div>
</div>