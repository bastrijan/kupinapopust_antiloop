﻿<div class="homepage-content" style="margin-top: 50px;">
    <div class="layout-wrapper">

        <div class="subpage-content">
            <div class="section-container">
                <div class="caption">
                    <table>
                        <tr>
                            <td style="padding: 8px 18px;">
                                <div>
                                    <img src="/pub/img/layout/ok20x25.png" style="width: 25px"/>
                                    <span style="font-size: 13px"><?php print Kohana::lang("index.Ви Благодариме! Успешно резервиравте ваучер за вашата избрана понуда") ?></span>

                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <p>По извршената уплата ве молиме информирајте не преку e-mail на contact@kupinapopust.mk за да го евидентираме вашето плаќање.</p>
                <p>Потребни ни се:</p>
                <p>1. Вашето Име и Презиме</p>
                <p>2. За која понуда сте уплатиле </p>
                <p>3. Сумата која сте ја уплатиле</p>
                <p>Обично се потребни 1-2 дена за да се изврши преносот на пари. Затоа ве молиме за трпеливост. Доколки  ни испратите скенирана копија од уплатницата по e-mail, тогаш веднаш ви го испраќаме ваучерот.</p>

                <p>
                    <span>
                        <input class="submit button" type="button" value="OK" onclick="location.href='/';">
                    </span>
                </p>
            </div>
        </div>
        <div class="static_page_contact_parent">
            <div class="static_page_contact">
                <?php require_once APPPATH . 'views/layouts/contact.php'; ?>
            </div>
        </div>
    </div>
</div>