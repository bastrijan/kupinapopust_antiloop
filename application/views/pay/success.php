<script type="text/javascript">
<?php
if (isset($_GET["exitif"]) and $_GET["exitif"]) {
  print "parent.document.getElementById('content').style.display='none';" ;
  print 'window.parent.location = "http://' . $_SERVER["HTTP_HOST"] . '/pay/success"' ;
}
else {
  print "history.forward();" ;
}
?>
</script>
<div class="homepage-content" style="margin-top: 50px;">
  <div class="layout-wrapper">

    <div class="subpage-content">
      <div class="section-container">
        <div class="caption">
          <table>
            <tr>
              <td style="padding: 8px 18px">
                <div>
                  <img src="/pub/img/layout/ok20x25.png" style=""/>
                  <span style="font-size: 13px">
                    <?php print Kohana::lang("index.Трансакцијата е успешна!")?>
                  </span>
                </div>                   
                
              </td>
            </tr>
          </table>
        </div>
        <p style="font-size: 12pt;font-weight: bold;"><?php print Kohana::lang("index.Ви Благодариме на довербата")?></p>
        <p><?php print Kohana::lang("index.За подетални информации ве молиме проверете го вашиот регистриран e-mail")?></p>

        <p>
          <span>
            <input class="submit button" type="button" value="OK" onclick="location.href='/';">
          </span>
        </p>
      </div>
    </div>
    <div class="static_page_contact_parent">
      <div class="static_page_contact">
        <?php require_once APPPATH . 'views/layouts/contact.php' ; ?>
      </div>
    </div>
  </div>
</div>