<script type="text/javascript">
	<?php

		$cenaNaVaucerCalc = ($dealData->tip == 'cena_na_vaucer' ? $dealData->price_voucher : $dealData->price_discount);

		//ako doaga od CASYS (uspesen response) togas thankyou bi se prikazal vo iframe-ot. 
		//Zatoa mora so javascript da se skrie glavniot content i
		//da se smeni napravi refresh na glavnata strana so lokacijata za thankyou page 
		if (isset($_GET["exitif"]) and $_GET["exitif"]) 
		{
			print "parent.document.getElementById('content').style.display='none';" ;
			print 'window.parent.location = "'.Kohana::config('facebook.facebookSiteProtocol').'://' . Kohana::config('facebook.facebookSiteURL') . '/pay_casys/success/'.$payAttemptData->id.'"' ;
		}
		else //sega e napraven reshresh-ot na glavnata strana i sme na thankyou na glavniot prozorec
		{
			
			//STARO
			//print "history.forward();" ;

	?>

			//ZA GOOGLE ADS
			ga('require', 'ecommerce');

			ga('ecommerce:addTransaction', {
			  'id': <?php echo "'".$payAttemptData->id."'" ?>,      // Transaction ID. Required.
			  'affiliation': 'Kupinapopust.mk',   // Affiliation or store name.
			  'revenue': <?php echo "'".($payAttemptData->quantity*$cenaNaVaucerCalc - $payAttemptData->points_used)."'" ?>,               // Grand Total.
			  'shipping': '0',                  // Shipping.
			  'tax': '0',                     // Tax.
			  'currency': 'MKD'  // local currency code.
			});

			ga('ecommerce:addItem', {
			  'id': <?php echo "'".$payAttemptData->id."'" ?>,                     // Transaction ID. Required.
			  'name': <?php echo "'".trim(strip_tags($dealData->title_mk_clean))."'" ?>,    // Product name. Required.
			  'sku': '',                 // SKU/code.
			  'category': <?php echo "'".$categoryData->name."'" ?>,         // Category or variation.
			  'price': <?php echo "'".$cenaNaVaucerCalc."'" ?>,                 // Unit price.
			  'quantity': <?php echo "'".$payAttemptData->quantity."'" ?>,                   // Quantity.
			  'currency': 'MKD' // local currency code.
			});

			ga('ecommerce:send');


			//FACEBOOK PIXEL
				// Purchase
				// Track purchases or checkout flow completions (ex. landing on "Thank You" or confirmation page)
			fbq('track', 'Purchase', {value: '1.00', currency: 'MKD'});
	<?php		
		} // else
	?>

</script>

<?php		
	if ($dealData->card)
		$url = Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/card";	
	else	
		$url = Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/deal/index/" . $payAttemptData->deal_id;
?>

<div class="row">
	<div class="col-md-4">
		<aside class="sidebar-left">
			<?php require_once APPPATH . 'views/layouts/contact.php'; ?>
			<?php require_once APPPATH . 'views/layouts/bezbednost.php'; ?>
			<?php require_once APPPATH . 'views/layouts/satisfaction.php'; ?>
			<div class="gap hidden-xs"></div>
		</aside>
	</div>
	
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-12 bg-white my-padd">
		
				<h3><i class="fa fa-check-square"></i> <?php print Kohana::lang("index.Трансакцијата е успешна!"); ?></h3>
				<div class="gap"></div>
				<div class="row border-bottom mb10">
					<div class="col-md-12">
						<?php print Kohana::lang("index.Ви Благодариме на довербата"); ?>
						<br /><?php print Kohana::lang("index.За подетални информации ве молиме проверете го вашиот регистриран e-mail"); ?>
						<br />
						<?php if(isset($vaucer_nadminat_limit) && $vaucer_nadminat_limit == 1) { ?>
							<br />
							<p style="color: red">
								<strong>
									ВАЖНО!!!! <br />
									Почитувани, истовремено и друг посетител има купено од понудата и со тоа е надминат лимитот на понудата.<br />
									Ве молиме, во најбрз рок, <a target="_blank" href="/static/page/kontakt">Контактирајте</a> нѐ за да може да ја разрешиме настаната ситуација.
								</strong>
							</p>
							<br />
						<?php } // if(isset($vaucer_nadminat_limit) && $vaucer_nadminat_limit == 1) { ?>
						<br /><a class="btn btn-lg green mt20" href="/">OK</a>
					</div>
				</div>

				<div class="row mb10  border-bottom">
					<div class="col-md-12">
						<h3> Споделете ја понудата со вашите пријатели.</h3>
						<h4> Убаво е и тие да дозанаат за попустот.</h4>
					</div>
					

						<div class="col-md-12 mb10">
							<!-- <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="recommend" data-show-faces="true" data-share="true"></div> -->
							<fb:like href="<?php print $url; ?>" send="true" width="450" show_faces="false" action="recommend" font="arial"></fb:like>
						</div>
						
						<div class="col-md-12 mb10">
							<!--
							<a href="https://twitter.com/share" class="twitter-share-button" data-url="@Model.FriendlyUrl" data-text="@Model.Name" data-via="KlikniJadi">Tweet</a>
							<script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + '://platform.twitter.com/widgets.js'; fjs.parentNode.insertBefore(js, fjs); } }(document, 'script', 'twitter-wjs');</script>
							-->
							<a href="https://twitter.com/share" class="twitter-share-button" data-lang="en" data-url="<?php print $url; ?>" >Tweet</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
						</div>
					
						<div class="col-md-12 mb10">
							<!-- <div class="g-plusone" data-size="medium"></div> -->
							
							<!-- Place this tag where you want the +1 button to render -->
							<g:plusone size="medium" href="<?php print $url; ?>"></g:plusone>

							<!-- Place this render call where appropriate -->
							<script type="text/javascript">
								(function() {
									var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
									po.src = 'https://apis.google.com/js/plusone.js';
									var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
								})();
							</script>
						</div>

					
					<div class="col-md-12 mb10">
						<a class="btn btn-md green popup-text" href="#tellafriend_button" data-effect="mfp-move-from-top">Испрати e-mail</a>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="gap"></div>
</div>

<?php require_once APPPATH . 'views/layouts/tellafriend_popup.php'; ?>

<!-- LYONESS TRACKING CODE - Confirmation Imgage Pixel -->
<?php echo $lyoness_confirmation_img_pixel; ?>



<!-- Facebook Conversion Code for Checkouts - kupinapopust tim account 1 -->
<!--
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6032481398751', {'value':'0.00','currency':'EUR'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6032481398751&amp;cd[value]=0.00&amp;cd[currency]=EUR&amp;noscript=1" /></noscript>
-->


<!--
Start oof Floodlight Tag on behalf of AdTradr Corporation: Please do not remove
Activity name of this tag: Kupinapopust.mk Thank you
URL of the webpage where the tag is expected to be placed: http://kupinapopust.mk/all/category/bansko-25 Thank you
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 09/30/2015
-->
<!--
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="https://4768706.fls.doubleclick.net/activityi;src=4768706;type=invmedia;cat=3dbskye1;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="https://4768706.fls.doubleclick.net/activityi;src=4768706;type=invmedia;cat=3dbskye1;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
-->
<!-- End of Floodlight Tag on behalf of AdTradr Corporation: Please do not remove -->
