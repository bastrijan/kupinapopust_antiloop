﻿<div class="homepage-content" style="margin-top: 50px;">
    <div class="layout-wrapper">

        <div class="subpage-content">
            <div class="section-container">
                <div class="caption">
                    <table>
                        <tr>
                            <td style="padding: 8px 18px;">
                                <div>
                                    <img src="/pub/img/layout/ok20x25.png" style="width: 25px"/>
                                    <span style="font-size: 13px">
                                        Ви благодариме. Успешно резервиравте подарок, електронска “kupinapopust“ картичка. Вие сте вистински пријател и избравте оригинален подарок.
                                        <br><br><img src="/pub/img/layout/podarok_karticka.png"/> 
                                        <br><br>За подетални информации околу плаќањето ве молиме проверете го вашиот регистриран e-mail.

                                        <br><br>Со почит,
                                        <br>Kupinapopust.mk тим
                                    </span>

                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <p>
                    <span>
                        <input class="submit button" type="button" value="OK" onclick="location.href='/';">
                    </span>
                </p>
            </div>
        </div>
        <div class="static_page_contact_parent">
            <div class="static_page_contact">
                <?php require_once APPPATH . 'views/layouts/contact.php'; ?>
            </div>
        </div>
    </div>
</div>

