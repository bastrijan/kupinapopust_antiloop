<?php 
defined('SYSPATH') OR die('No direct access allowed.'); 

$vouchersModel = new Vouchers_Model() ;
$dealsModel = new Deals_Model();
?>

<!-- include summernote -->
<link rel="stylesheet" href="/pub/summernote/summernote.css">
<script type="text/javascript" src="/pub/summernote/summernote.js"></script>

<?php require_once APPPATH.'views/admin/menu.php' ; ?>

<script type="text/javascript">

    $(document).ready(function() {

          $('.notes').summernote({
    			height: 300
    	  });

    });

    <?php if (isset($update_success) && $update_success) { ?>
        $(document).ready(function () {

            $().toastmessage('showToast', {
                text     : "Нарачката е успешно изменета.",
                sticky   : false,
                position : 'top-center',
                type     : 'success'
            });
        }) 
    <?php } ?>   
</script>


<h1>Уреди нарачка број: <?php echo $narackaData[0]->id; ?></h1>

<?php

//izvadi gi kreiranite vauceri
$whereVouchers = array('attempt_id' => $narackaData[0]->id) ;
$vouchers = $vouchersModel->getData($whereVouchers) ;

$where_options_str = "d.id = ".$narackaData[0]->deal_id." AND do.id = ".$narackaData[0]->deal_option_id;
$dealData = $dealsModel->getDealOptionsData($where_options_str);

print "<div>";
    print "<strong>Тип:</strong> " . $narackaTipArr[$narackaData[0]->type] . "&nbsp;&nbsp;";
    print "<strong>Датум:</strong> " . date("d/m/Y H:i:s", strtotime($narackaData[0]->ts)) . "&nbsp;&nbsp;";
    print "<strong>Понуда ID:</strong> " . html::anchor("deal/index/".$narackaData[0]->deal_id, $narackaData[0]->deal_id, array('target' => '_blank')). "&nbsp;&nbsp;";
    print "<strong>Опција ID:</strong> " . html::anchor("/admin2/uredi_dealoption/".$narackaData[0]->deal_option_id."/".$narackaData[0]->deal_id, $narackaData[0]->deal_option_id, array('target' => '_blank')). "&nbsp;&nbsp;";
    print "<strong>Количина:</strong> " . $narackaData[0]->quantity . "&nbsp;&nbsp;";
    print "<strong>Креирани ваучери:</strong> " . count($vouchers) . "&nbsp;&nbsp;";

    print "<br/>";
    print "<strong>Email:</strong> " . $narackaData[0]->email . "&nbsp;&nbsp;";
    print "<strong>Искористени поени:</strong> " . $narackaData[0]->points_used . "&nbsp;&nbsp;";
    
    print "<br/>";
    print "<strong>CPAY Референца:</strong> " . ( trim($narackaData[0]->ref) != "" ? '<strong class="admin_success_msg">'.trim($narackaData[0]->ref).'</strong>' : '<i class="fa fa-times admin_error_msg" style="opacity: 1;"></i>' ) . "&nbsp;&nbsp;";
    print "<strong>На рати:</strong> " . ($narackaData[0]->installments > 1 ? $narackaData[0]->installments." рати" : "Еднократно")  . "&nbsp;&nbsp;";
    print "<strong>Shopping cart:</strong> " . ($narackaData[0]->shop_cart_id > 0 ? html::anchor("/admin_naracki/index?preselectedShoppingCartID=".$narackaData[0]->shop_cart_id, '<i class="fa fa-shopping-cart admin_success_msg" style="opacity: 1;"></i> '.$narackaData[0]->shop_cart_id, array('target' => '_blank')) : "НЕ")  . "&nbsp;&nbsp;";
    

    print "<br/>";
    print "<strong>Подарок:</strong> " . ($narackaData[0]->gift ? "Да": "Не") . "&nbsp;&nbsp;";
    print "<strong>Е-mail на пријателот:</strong> " . $narackaData[0]->copy_email. "&nbsp;&nbsp;" ; 

    if($dealData[0]->proizvod_flag)
    {
        print "<br/>";
        print "<br/>";
        print "<strong style='color: #00b2cc;'>ПОНУДАТА Е ПРОИЗВОД</strong>";
        print "<br/>"; 
        print "<strong>Име и презиме:</strong> " . $narackaData[0]->otkup_ime_prezime. "&nbsp;&nbsp;" ; 
        print "<strong>Телефон:</strong> " . $narackaData[0]->otkup_telefon. "&nbsp;&nbsp;" ; 
        print "<strong>Превземање:</strong> " . ( in_array($narackaData[0]->otkup_prevzemanje, array(1, 2)) ? $otkupPrevzemanjeArr[$narackaData[0]->otkup_prevzemanje] : ""). "&nbsp;&nbsp;" ; 

        print "<br/>";
        print "<strong>Точна адреса со кратко појаснување:</strong> " . $narackaData[0]->otkup_adresa. "&nbsp;&nbsp;" ; 

        print "<br/>";
        print "<strong>Бесплатна достава - платежна картичка:</strong> " . ($dealData[0]->proizvod_besplatna_dostava ? '<i class="fa fa-check admin_success_msg" style="opacity: 1;"></i>' : '<i class="fa fa-times admin_error_msg" style="opacity: 1;"></i>'). "&nbsp;&nbsp;";
        print "<strong>Бесплатна достава - откуп:</strong> " . ($dealData[0]->proizvod_besplatna_dostava_otkup ? '<i class="fa fa-check admin_success_msg" style="opacity: 1;"></i>' : '<i class="fa fa-times admin_error_msg" style="opacity: 1;"></i>'). "&nbsp;&nbsp;";
    }

    
print "</div>";
?>





<form method="post" action="" id="naracka_form">
    <input type="hidden" name="deal_id" value="<?php echo $narackaData[0]->deal_id; ?>">
    <input type="hidden" name="quantity" value="<?php echo $narackaData[0]->quantity; ?>">
    <input type="hidden" name="type" value="<?php echo $narackaData[0]->type; ?>">

    <div class="container_12 homepage-billboard-dhtml">
        <div class="grid_12 alpha omega">
           
                <?php if($dealData[0]->proizvod_flag) { ?> 
                    <div class="row clearfix">
                        <div class="grid_4 alpha omega">Статус</div>
                        <div class="grid_8 alpha omega">
                            <select name="otkup_administracija_status">
                                <?php foreach ($otkupStatusiArr as $keyStatus => $valueStatus) { ?>
                                    <option value="<?php echo $keyStatus; ?>" <?php if ($narackaData[0]->otkup_administracija_status == $keyStatus) echo 'selected="selected"'; ?> >
                                        <?php 
                                            if($keyStatus == 2) //ako statusot e "Испратено по пошта / Корисникот информиран да ја подигне"
                                            {
                                                $otkupStatusZaStatusDvaArr = explode(" / ", $valueStatus);
                                                echo $otkupStatusZaStatusDvaArr[$narackaData[0]->otkup_prevzemanje % 2];
                                            }
                                            else
                                                echo $valueStatus; 
                                        ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="grid_12 alpha omega" style="padding-left: 20px;">
                            Појаснување: <br/>
                            1. При сетирање на статусот на нарачката на <strong><?php echo $otkupStatusiArr[3]; ?></strong> автоматски се креираат и активираат ваучерите, се праќаат по email до корисникот и се означуваат како искористени.<br/>
                            2. При сетирање на статусот на нарачката на <strong><?php echo $otkupStatusiArr[10]; ?></strong> автоматски се бришат ваучерите.
                        </div>
                    </div>  

                    <?php if($narackaData[0]->otkup_prevzemanje == 2) { ?>    
                        <div class="row clearfix">
                            <div class="grid_4 alpha omega">Адресница</div>
                            <div class="grid_8 alpha omega"><input type="text" name="otkup_adresnica"  value="<?php print $narackaData[0]->otkup_adresnica ?>" size="50" maxlength="50"></div>
                        </div>
                    <?php } //if($narackaData[0]->otkup_prevzemanje == 2) { ?>

            <?php }else{ //if ($narackaData[0]->type == "otkup") { ?> 
                <input type="hidden" name="otkup_administracija_status" value="0">
            <?php } ?> 
                
            <div class="row clearfix" style="padding: 20px;">
				<strong>Забелешка</strong>
				<textarea class="notes" name="otkup_zabeleska" id="otkup_zabeleska"><?php print $narackaData[0]->otkup_zabeleska; ?></textarea>
            </div>

            <div class="row clearfix">
                <input type="submit" value="Зачувај" value="save_btn">
            </div>
            
        </div>
    </div>
</form>