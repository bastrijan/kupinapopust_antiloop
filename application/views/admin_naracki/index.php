<?php 
defined('SYSPATH') OR die('No direct access allowed.'); 
require_once APPPATH.'views/admin/menu.php' ; 

$vouchersModel = new Vouchers_Model() ;
$dealsModel = new Deals_Model();
?>

<style type="text/css">

    input[type=checkbox] {
      /* All browsers except webkit*/
      transform: scale(2);

      /* Webkit browsers*/
      -webkit-transform: scale(2);
    }

</style>

<h1>Нарачки</h1>

<div class="container_12 homepage-billboard">
	<div class="grid_12 alpha" >
		<form method="get" action="" name="form_naracki_filter" id="form_naracki_filter">
            <input type="hidden" name="preselectedDealID" value="<?php echo $preselectedDealID?>">
            <input type="hidden" name="preselectedDealOptionID" value="<?php echo $preselectedDealOptionID?>">
            <input type="hidden" name="preselectedShoppingCartID" value="<?php echo $preselectedShoppingCartID?>">

            <?php if($preselectedDealID != "" || $preselectedDealOptionID != "" || $preselectedShoppingCartID != "") { ?>
                <div class="row clearfix" style="margin: 0px; padding: 10px 0;">
                    <div class="grid_12 alpha omega filter_label">
                        <?php echo ($preselectedDealID != "" ? "Одбрана Понуда ID: " . html::anchor("deal/index/".$preselectedDealID, $preselectedDealID, array('target' => '_blank'))."&nbsp;&nbsp;" : "") ; ?>
                        <?php echo ($preselectedDealOptionID != "" ? "Одбрана Опција ID: " . html::anchor("/admin2/uredi_dealoption/".$preselectedDealOptionID."/".$preselectedDealID, $preselectedDealOptionID, array('target' => '_blank'))."&nbsp;&nbsp;" : "") ; ?>
                        <?php echo ($preselectedShoppingCartID != "" ? "Одбран Shopping cart ID: " . '<i class="fa fa-shopping-cart admin_success_msg" style="opacity: 1;"></i> '.$preselectedShoppingCartID : "") ; ?>
                    </div>
                </div>
            <?php } ?>

            <div class="row clearfix" style="margin: 0px; padding: 10px 0;">

                <div class="grid_3 alpha omega filter_label">Тип:</div>
                <div class="grid_3 alpha omega">
		            <select class="form-control" id="naracki_tip" name="naracki_tip">
                        <option value="" <?php print ($naracki_tip == '') ? 'selected="true"' : ""  ?>>Сите</option>
		                <option value="otkup" <?php print ($naracki_tip == 'otkup') ? 'selected="true"' : ""  ?>>Откуп</option>
		                <option value="card" <?php print ($naracki_tip == 'card') ? 'selected="true"' : ""  ?>>Платежна картичка</option>
		                <option value="cache" <?php print ($naracki_tip == 'cache') ? 'selected="true"' : ""  ?>>Во готово</option>
						<option value="bank" <?php print ($naracki_tip == 'bank') ? 'selected="true"' : ""  ?>>Уплатница</option>
		            </select>                 	
                </div>

                <div class="grid_3 alpha omega filter_label">Датум:</div>
                <div class="grid_3 alpha omega">
                    <div class='input-group date' id='datetimepicker_naracka_datum'>
                        <input type='text' class="form-control" name="naracka_datum" value="<?php if ($naracka_datum != "0000-00-00" ) print $naracka_datum; ?>" id="naracka_datum" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                    <script type="text/javascript">
                        $(function () {
                            $('#datetimepicker_naracka_datum').datetimepicker({
                                format: 'YYYY-MM-DD',
                                locale: 'mk'
                            });
                        });
                    </script>
                </div>
            </div>

            <div class="row clearfix" style="margin: 0px; padding: 10px 0;">
                <div class="grid_3 alpha omega filter_label">Еmail адреса:</div>
                <div class="grid_3 alpha omega">
                    <input type="text" name="naracka_email" id="naracka_email" class="form-control" value="<?php echo $naracka_email ?>" />
                </div>

                <div class="grid_3 alpha omega filter_label">CPAY Референца:</div>
                <div class="grid_3 alpha omega">
                    <input type="text" name="naracka_ref" id="naracka_ref" class="form-control" value="<?php echo $naracka_ref ?>" />
                </div>
            </div>


<!--             <div class="row clearfix" style="margin: 0px; padding: 10px 0;">
                <div class="grid_12 alpha omega filter_label"><strong>ОТКУП</strong></div>
            </div> -->

            <div class="row clearfix" style="margin: 0px; padding: 10px 0;">
                <div class="grid_3 alpha omega filter_label">Име и презиме:</div>
                <div class="grid_3 alpha omega">
                    <input type="text" name="otkup_ime_prezime" id="otkup_ime_prezime" class="form-control" value="<?php echo $otkup_ime_prezime ?>" />
                </div>

                <div class="grid_3 alpha omega filter_label">Адресница:</div>
                <div class="grid_3 alpha omega">
                    <input type="text" name="otkup_adresnica" id="otkup_adresnica" class="form-control" value="<?php echo $otkup_adresnica ?>" />
                </div>
            </div>


            <div class="row clearfix" style="margin: 0px; padding: 10px 0;">
                <div class="grid_3 alpha omega filter_label">Статус:</div>
                <div class="grid_3 alpha omega">
                    <select class="form-control" id="otkup_administracija_status" name="otkup_administracija_status">
                        <option value="-1" <?php print ($otkup_administracija_status == -1) ? 'selected="true"' : ""  ?>>Сите</option>
                        <?php foreach ($otkupStatusiArr as $keyStatus => $valueStatus) { ?>
                            <option value="<?php echo $keyStatus; ?>" <?php if ($otkup_administracija_status == $keyStatus) echo 'selected="selected"'; ?> ><?php echo $valueStatus; ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="grid_3 alpha omega filter_label">&nbsp;</div>
                <div class="grid_3 alpha omega">&nbsp;</div>
            </div>


            <div class="row clearfix" style="margin: 0px; padding: 10px 0;">
            	<div class="grid_3 alpha omega filter_label">
					<input type="submit" name="deal_filter_submit" id="deal_filter_submit" value="Филтрирај">
				</div>
				<div class="grid_9 alpha omega">
					<strong>Број на прикажани нарачки: <?php print count($narackiData);?></strong>
				</div>
            </div>

		</form>	
	</div>	


	
    <div class="grid_12 alpha" style="border: black 1px dotted; margin-top: 10px; ">
        <?php
        if ($narackiData) {
            foreach ($narackiData as $naracka) {
                
                //izvadi gi kreiranite vauceri
                $whereVouchers = array('attempt_id' => $naracka->id) ;
                $vouchers = $vouchersModel->getData($whereVouchers) ;

                //izvadi info za ponudata i opcija
                $where_options_str = "d.id = $naracka->deal_id AND do.id = $naracka->deal_option_id";
                $dealData = $dealsModel->getDealOptionsData($where_options_str);

                print '<div style="padding: 0 10px 0 10px">';

                    print "<div style='text-align: center'>";
                        print "<strong>Нарачка ID: " . $naracka->id . "</strong>";
                        print "&nbsp;&nbsp;";
                        print html::anchor("/admin_naracki/manage/".$naracka->id, "Уреди нарачка", array('target' => '_blank'));
                    print "</div>";
                    print "<br/>";
                    print "<div>";
                        print "<strong>Тип:</strong> " . $narackaTipArr[$naracka->type] . "&nbsp;&nbsp;";
                        print "<strong>Датум:</strong> " . date("d/m/Y H:i:s", strtotime($naracka->ts)) . "&nbsp;&nbsp;";
                        print "<strong>Понуда ID:</strong> " . html::anchor("deal/index/".$naracka->deal_id, $naracka->deal_id, array('target' => '_blank')). "&nbsp;&nbsp;";
                        print "<strong>Опција ID:</strong> " . html::anchor("/admin2/uredi_dealoption/".$naracka->deal_option_id."/".$naracka->deal_id, $naracka->deal_option_id, array('target' => '_blank')). "&nbsp;&nbsp;";
                        print "<strong>Количина:</strong> " . $naracka->quantity . "&nbsp;&nbsp;";
                        print "<strong>Креирани ваучери:</strong> " . count($vouchers) . "&nbsp;&nbsp;";

                        print "<br/>";
                        print "<strong>Email:</strong> " . $naracka->email . "&nbsp;&nbsp;";
                        print "<strong>Искористени поени:</strong> " . $naracka->points_used . "&nbsp;&nbsp;";
                        
                        print "<br/>";
                        print "<strong>CPAY Референца:</strong> " . ( trim($naracka->ref) != "" ? '<strong class="admin_success_msg">'.trim($naracka->ref).'</strong>' : '<i class="fa fa-times admin_error_msg" style="opacity: 1;"></i>' ) . "&nbsp;&nbsp;";
                        print "<strong>На рати:</strong> " . ($naracka->installments > 1 ? $naracka->installments." рати" : "Еднократно")  . "&nbsp;&nbsp;";
                        print "<strong>Shopping cart:</strong> " . ($naracka->shop_cart_id > 0 ? html::anchor("/admin_naracki/index?preselectedShoppingCartID=".$naracka->shop_cart_id, '<i class="fa fa-shopping-cart admin_success_msg" style="opacity: 1;"></i> '.$naracka->shop_cart_id, array('target' => '_blank')) : "НЕ")  . "&nbsp;&nbsp;";
                        

                        print "<br/>";
                        print "<strong>Подарок:</strong> " . ($naracka->gift ? "Да": "Не") . "&nbsp;&nbsp;";
                        print "<strong>Е-mail на пријателот:</strong> " . $naracka->copy_email. "&nbsp;&nbsp;" ; 

                        if($dealData[0]->proizvod_flag)
                        {
                            print "<br/>";
                            print "<br/>";
                            print "<strong style='color: #00b2cc;'>ПОНУДАТА Е ПРОИЗВОД</strong>";
                            print "<br/>"; 
                            print "<strong>Име и презиме:</strong> " . $naracka->otkup_ime_prezime. "&nbsp;&nbsp;" ; 
                            print "<strong>Телефон:</strong> " . $naracka->otkup_telefon. "&nbsp;&nbsp;" ; 
                            print "<strong>Превземање:</strong> " . ( in_array($naracka->otkup_prevzemanje, array(1, 2)) ? $otkupPrevzemanjeArr[$naracka->otkup_prevzemanje] : ""). "&nbsp;&nbsp;" ; 

                            print "<br/>";
                            print "<strong>Точна адреса со кратко појаснување:</strong> " . $naracka->otkup_adresa. "&nbsp;&nbsp;" ; 

                            print "<br/>";

                            $otkupAdministracijaStatusFinalen = "";
                            if($naracka->otkup_administracija_status == 2) //ako statusot e "Испратено по пошта / Корисникот информиран да ја подигне"
                            {
                                $otkupStatusZaStatusDvaArr = explode(" / ", $otkupStatusiArr[2]);

                                $otkupAdministracijaStatusFinalen = $otkupStatusZaStatusDvaArr[$naracka->otkup_prevzemanje % 2];
                            }
                            else
                                $otkupAdministracijaStatusFinalen = $otkupStatusiArr[$naracka->otkup_administracija_status];

                            print "<strong>Статус:</strong> " .$otkupAdministracijaStatusFinalen. "&nbsp;&nbsp;" ; 
                            print "<strong>Адресница:</strong> " . $naracka->otkup_adresnica. "&nbsp;&nbsp;" ;

                            print "<br/>";
                            
                            print "<strong>Бесплатна достава - платежна картичка:</strong> " . ($dealData[0]->proizvod_besplatna_dostava ? '<i class="fa fa-check admin_success_msg" style="opacity: 1;"></i>' : '<i class="fa fa-times admin_error_msg" style="opacity: 1;"></i>'). "&nbsp;&nbsp;";
                            print "<strong>Бесплатна достава - откуп:</strong> " . ($dealData[0]->proizvod_besplatna_dostava_otkup ? '<i class="fa fa-check admin_success_msg" style="opacity: 1;"></i>' : '<i class="fa fa-times admin_error_msg" style="opacity: 1;"></i>'). "&nbsp;&nbsp;";
                        }

                        
                    print "</div>";

                print '</div>';
    			print "<hr style='margin-bottom: 0px;'>";
            }
        } else {
            print '<h4>Нема нарачки според избраните филтри!!!</h4><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>';
            print "<hr>";
        }

        ?>


    </div>

</div>
