<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once APPPATH.'views/admin/menu.php' ; ?>

<?php $userID = $this->session->get("user_id"); ?>

<style type="text/css">

    input[type=checkbox] {
      /* All browsers except webkit*/
      transform: scale(2);

      /* Webkit browsers*/
      -webkit-transform: scale(2);
    }

</style>

<script type="text/javascript">

    $(document).ready(function() {

		
		$( "#form_multiple_date_end" ).submit(function( event ) 
		{
			val_flag = false;
			val_txt = "";
			
			

			if($("#datepicker_end").val() == "" && $("#datepicker_valid_to").val() == "")
			{
				val_txt = "Ве молиме одберете датум за (Крај на понуда) или датумот за (Може да се искористи до) !\n";
				val_flag = true;
			}
			
			if($('.multiple_end_time_arr:checked').length < 1 && $('.multiple_valid_to_arr:checked').length < 1)
			{
				val_txt += "Ве молиме одберете барем една понуда за која го менувате датумот!";
				val_flag = true;
			}


			
			if(val_flag)
			{
				alert(val_txt);
				event.preventDefault();
			}
			
		});
		
		

		<?php if (isset($succes_msg) and $succes_msg == 1 AND isset($invalid_deals_ids) and $invalid_deals_ids == "") { ?>
					$.noticeAdd({
						text: "Успешно го сменивте датумот на избраните понуди.",
						stay: false,
						type:'notification-success'
						//                stayTime: 3000
					});
				
		<?php } ?>


        <?php if (isset($partOfGeneralOffer) and $partOfGeneralOffer == 1) { ?>

                $().toastmessage('showToast', {
                    text     : "Почитувани, понудата не може да се постави како Главна понуда затоа што е дел од Општа понуда!",
                    sticky   : true,
                    position : 'top-center',
                    type     : 'error'
                });
       
        <?php } ?> 


		 $("#checkAllKrajNaPonuda").click(function () {
		     $('.multiple_end_time_arr').not(this).prop('checked', this.checked);
		 });

		 $("#checkAllMozeIskorstiDo").click(function () {
		     $('.multiple_valid_to_arr').not(this).prop('checked', this.checked);
		 });

		//za COMBOBOX AUTOCOMPLETE
		$(document).ready(function() {
		    $('.js-example-basic-single').select2();
		});
		
    });
</script>


<?php if (isset($invalid_deals_ids) and $invalid_deals_ids != "") { ?>
  <div style="color: red; padding: 5px">
  	 <b>ВНИМАНИЕ!!!</b>
  	 <br/>
	 Крајот на понудата не е сменет за понудите со следните ID-а:<br/>
	 <b>
	 	<?php 
	 		$invalid_deals_ids_arr = explode(",", $invalid_deals_ids);

	 		foreach ($invalid_deals_ids_arr AS $arr_deal_id) 
	 		{
	 			$arr_deal_id = trim($arr_deal_id);

	 			echo html::anchor("deal/index/".$arr_deal_id, $arr_deal_id, array('target' => '_blank'))."<br/>";
	 		}

	 	?>
	 </b>
	 Причината е тоа што избраниот датум е поголем од датумот во полето <b>(Може да се искористи до)</b> на опцијата која последна истекува. Прво зголемето го полето <b>(Може да се искористи до)</b> кај барем една опција од понудата, па потоа вратете се тука да го смените овој датум.
  </div>
<?php } ?>

<div class="container_12 homepage-billboard">
	<div class="grid_12 alpha" >
		<form method="get" action="" name="form_deal_filter" id="form_deal_filter">

            <div class="row clearfix" style="margin: 0px; padding: 10px 0;">
                <div class="grid_3 alpha omega filter_label">Понуди:</div>
                <div class="grid_3 alpha omega">
		            <select class="form-control" id="offer_select" name="type">
		                <option value="all" <?php print ($selected == 'all') ? 'selected="true"' : ""  ?>>Сите понуди</option>
		                <option value="current" <?php print ($selected == 'current') ? 'selected="true"' : ""  ?>>Активни понуди</option>
		                <option value="past" <?php print ($selected == 'past') ? 'selected="true"' : ""  ?>>Изминати понуди</option>
						<option value="demo" <?php print ($selected == 'demo') ? 'selected="true"' : ""  ?>>Демо понуди</option>
						<option value="general_offers" <?php print ($selected == 'general_offers') ? 'selected="true"' : ""  ?>>Активни Општи понуди</option>
						<option value="past_general_offers" <?php print ($selected == 'past_general_offers') ? 'selected="true"' : ""  ?>>Изминати Општи понуди</option>
		            </select>                 	
                </div>

                <div class="grid_3 alpha omega filter_label">Категорија:</div>
                <div class="grid_3 alpha omega">
		            <select class="form-control" id="category_id_selected" name="category_id_selected">
						  <option <?php if($category_id_selected == 0) echo 'selected="selected"'; ?> value="0">
		                          <?php print "Сите"; ?>
		                  </option>
		                  <?php foreach ($categoriesData as $category_id => $category_name) { ?>
		                  <option <?php if($category_id_selected == $category_id) echo 'selected="selected"'; ?> value="<?php print $category_id; ?>">
		                          <?php print $category_name; ?>
		                  </option>
		                  <?php } ?>
		            </select>                	
                </div>
            </div>


            <div class="row clearfix" style="margin: 0px; padding: 10px 0;">
                <div class="grid_3 alpha omega filter_label">Партнер:</div>
                <div class="grid_9 alpha omega">
					<select class="js-example-basic-single" id="partner_id_selected" name="partner_id_selected">
					  <option <?php if(0==$partner_id_selected) echo 'selected="selected"'; ?> value="0">
							  <?php print "Сите" ?>
					  </option>
					  <?php foreach ($partnersData as $partner) { ?>
					  <option <?php if($partner->id==$partner_id_selected) echo 'selected="selected"'; ?> value="<?php print $partner->id; ?>">
							  <?php print $partner->name; ?>
					  </option>
					  <?php } ?>
					</select>
                </div>

            </div>


            <div class="row clearfix" style="margin: 0px; padding: 10px 0;">
                <div class="grid_3 alpha omega filter_label">Име на понуда:</div>
                <div class="grid_9 alpha omega">
					<input type="text" name="deal_name_filter" id="deal_name_filter" class="form-control" value="<?php echo $deal_name_filter ?>" />
                </div>


            </div>


            <?php if(in_array($userID, array(1, 23))) { ?>
	            <div class="row clearfix" style="margin: 0px; padding: 10px 0;">
	                <div class="grid_3 alpha omega filter_label">Администратор:</div>
	                <div class="grid_9 alpha omega">
						<select class="form-control" id="vraboten_id" name="vraboten_id">
						  <option <?php if(0==$vraboten_id) echo 'selected="selected"'; ?> value="0">
								  <?php print "Сите" ?>
						  </option>
						  <?php foreach ($allVraboteni as $vraboten) { 
									if(in_array($userID, array(1, 23)) || $userID == $vraboten->id)
									{
						  ?>
									  <option <?php if($vraboten->id==$vraboten_id) echo 'selected="selected"'; ?> value="<?php print $vraboten->id; ?>">
											  <?php print $vraboten->name; ?>
									  </option>
						  <?php 
									}//if($userID == 1 || $userID == $vraboten->id)
								} 
						  ?>
						</select>
	                </div>


	            </div>
            <?php }//else od if($userID > 0) {?>

            <div class="row clearfix" style="margin: 0px; padding: 10px 0;">
            	<div class="grid_3 alpha omega filter_label">
					<input type="submit" name="deal_filter_submit" id="deal_filter_submit" value="Филтрирај">
				</div>
				<div class="grid_9 alpha omega">
					<strong>Број на прикажани понуди: <?php print count($dealsData);?></strong>
				</div>
            </div>

		</form>	
	</div>	

	<br/>&nbsp;<br/>

	<?php if(!in_array($selected, array("general_offers", "past_general_offers"))) { ?>	
		<div class="grid_12 alpha" >
	        <div class="row clearfix" style="margin: 0px; padding: 10px 0;">
	            <div class="grid_6 alpha omega filter_label">
	            	<input type="checkbox" id="checkAllKrajNaPonuda">&nbsp;&nbsp;Селектирај ги сите за датумот (Крај на понуда)
	            </div>
	            <div class="grid_6 alpha omega filter_label">
	            	<input type="checkbox" id="checkAllMozeIskorstiDo">&nbsp;&nbsp;Селектирај ги сите за датумот (Може да се искористи до)    	
	            </div>


	        </div>
		</div>	
		<br/>&nbsp;<br/>
	<?php }  ?>	

	
	<form method="post" action="/admin/multiple_end_time_method" name="form_multiple_date_end" id="form_multiple_date_end">
		<input type="hidden" name="type" value="<?php echo $selected?>">
		<input type="hidden" name="category_id_selected" value="<?php echo $category_id_selected?>">
		<input type="hidden" name="vraboten_id" value="<?php echo $vraboten_id?>">
		<input type="hidden" name="deal_name_filter" value="<?php echo $deal_name_filter?>">
		<input type="hidden" name="partner_id_selected" value="<?php echo $partner_id_selected?>">


        <div class="grid_12 alpha" style="border: black 1px dotted">
            <?php
            if ($dealsData) {
                foreach ($dealsData as $deal) {
                    print "<strong>" . $deal->title_mk . "</strong>&nbsp;&nbsp;&nbsp;&nbsp;";

                    if(!$deal->is_general_deal)
                    	print html::anchor("/admin/autosave_deal/$deal->id", "Промени Понуда");
                    else
                    	print html::anchor("/admin2/autosave_general_deal/$deal->id", "Промени Општа Понуда");

                    if(!$deal->is_general_deal)
                    {
                    	print "&nbsp;&nbsp;&nbsp;&nbsp;" . html::anchor("/admin2/dealoptions/$deal->id", "Опции", array('target' => '_blank'));
                    	print "&nbsp;&nbsp;&nbsp;&nbsp;" . html::anchor("/admin/dealdetails/$deal->id", "Ваучери");
                    	print "&nbsp;&nbsp;&nbsp;&nbsp;" . html::anchor("/admin/dealexpences/$deal->id", "Трошоци");

                    	print "&nbsp;&nbsp;&nbsp;".html::anchor("/admin_deals/clone_deal/$deal->id", "Клонирај", array('target' => '_blank', 'onclick' => 'return confirm("Дали си сигурен дека сакаш да креираш клон од оваа понуда?")'));

                    	print "&nbsp;&nbsp;&nbsp;".html::anchor("/admin_naracki/index?preselectedDealID=$deal->id", "Нарачки", array('target' => '_blank'));
                    }
                    else
                    {
                    	print "&nbsp;&nbsp;&nbsp;&nbsp;" . html::anchor("/admin2/general_offer_deals/$deal->id", "Понуди", array('target' => '_blank'));

                    }
                    
					
                    print "<br/><br/>";
                    if ($deal->primary1) {
                        print "&nbsp;&nbsp;&nbsp;&nbsp;<strong>ПРВА Главна понуда</strong>";
                    } 
					elseif ($deal->primary2) {
						print "&nbsp;&nbsp;&nbsp;&nbsp;<strong>ВТОРА Главна понуда</strong>";
						print "&nbsp;&nbsp;&nbsp;".html::anchor("/admin2/remove_as_primary/$deal->id/2/".$selected, "(Отстрани од главни понуди)", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да ја отстраниш ВТОРАТА главна понуда?")'));
                    } 
					elseif ($deal->primary3) {
						print "&nbsp;&nbsp;&nbsp;&nbsp;<strong>ТРЕТА Главна понуда</strong>";
						print "&nbsp;&nbsp;&nbsp;".html::anchor("/admin2/remove_as_primary/$deal->id/3/".$selected, "(Отстрани од главни понуди)", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да ја отстраниш ТРЕТАТА главна понуда?")'));
                    } 
                    elseif ($deal->primary4) {
						print "&nbsp;&nbsp;&nbsp;&nbsp;<strong>ЧЕТВРТА Главна понуда</strong>";
						print "&nbsp;&nbsp;&nbsp;".html::anchor("/admin2/remove_as_primary/$deal->id/4/".$selected, "(Отстрани од главни понуди)", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да ја отстраниш ЧЕТВРТАТА главна понуда?")'));
                    } 
                    elseif ($deal->primary5) {
						print "&nbsp;&nbsp;&nbsp;&nbsp;<strong>ПЕТТА Главна понуда</strong>";
						print "&nbsp;&nbsp;&nbsp;".html::anchor("/admin2/remove_as_primary/$deal->id/5/".$selected, "(Отстрани од главни понуди)", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да ја отстраниш ПЕТТАТА главна понуда?")'));
                    } 
					else
					{
                        print "&nbsp;&nbsp;&nbsp;&nbsp;Постави за главна понуда: ";
						print "&nbsp;&nbsp;&nbsp;&nbsp;" . html::anchor("/admin/setprimary/$deal->id/1/false/".$selected, "ПРВА", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да ја поставиш оваа понуда за ПРВА Главна понуда?")'));
						print "&nbsp;&nbsp;" . html::anchor("/admin/setprimary/$deal->id/2/false/".$selected, "ВТОРА", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да ја поставиш оваа понуда за ВТОРА Главна понуда?")'));
						print "&nbsp;&nbsp;" . html::anchor("/admin/setprimary/$deal->id/3/false/".$selected, "ТРЕТА", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да ја поставиш оваа понуда за ТРЕТА Главна понуда?")'));
						print "&nbsp;&nbsp;" . html::anchor("/admin/setprimary/$deal->id/4/false/".$selected, "ЧЕТВРТА", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да ја поставиш оваа понуда за ЧЕТВРТА Главна понуда?")'));
						print "&nbsp;&nbsp;" . html::anchor("/admin/setprimary/$deal->id/5/false/".$selected, "ПЕТТА", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да ја поставиш оваа понуда за ПЕТТА Главна понуда?")'));
                        
                    }



					print "&nbsp;&nbsp;&nbsp;&nbsp;".html::anchor("/admin/new_main_offer_schedule/$deal->id", "(Внеси во календар на главни понуди)", array('target' => '_blank'));
					

					if($deal->is_general_deal)
						print "<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;Понуда ID:&nbsp;" . html::anchor("deal/general_deal/".$deal->id, $deal->id, array('target' => '_blank'));
					else
						print "<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;Понуда ID:&nbsp;" . html::anchor("deal/index/".$deal->id, $deal->id, array('target' => '_blank'));
			


					if($deal->demo == 1){
						print "<br>";
						print "&nbsp;&nbsp;&nbsp;&nbsp;Демо линк:&nbsp;&nbsp;" .URL::site("/deal/demo/$deal->id", Kohana::config('facebook.facebookSiteProtocol'));
					}
					print "&nbsp;&nbsp;&nbsp;&nbsp;Администратор:&nbsp;" . $vraboteniData[$deal->vraboten_id];
					
					print "&nbsp;&nbsp;&nbsp;&nbsp;Категорија:&nbsp;" . ($deal->category_id > 0 ? $categoriesData[$deal->category_id]: "");
					print "&nbsp;&nbsp;&nbsp;&nbsp;Поткатегорија:&nbsp;" . ($deal->subcategory_id > 0 ? $subCategoriesData[$deal->subcategory_id]: "");

                    print "<br/>&nbsp;&nbsp;&nbsp;&nbsp;Поч. на понуда:&nbsp;" . date("d/m/Y", strtotime($deal->start_time));
                    print "&nbsp;&nbsp;&nbsp;&nbsp;Крај на понуда:&nbsp;" . date("d/m/Y", strtotime($deal->end_time));
                    
                    if(!$deal->is_general_deal)
                    {
                    	print '&nbsp;&nbsp;&nbsp;<input type="checkbox" name="multiple_end_time_arr[]" class="multiple_end_time_arr" value="'.$deal->id.'">&nbsp;&nbsp;Смени';
					
                    	print '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="multiple_valid_to_arr[]" class="multiple_valid_to_arr" value="'.$deal->id.'">&nbsp;&nbsp;Смени го датумот (Може да се искористи до) на сите опции&nbsp;' ;

                    }

					print "<br><hr>";
                }
            } else {
                print '<h4>Нема понуда во моментов!!!</h4>';
                print "<hr>";
            }

            ?>


            <?php if(!in_array($selected, array("general_offers", "past_general_offers"))) { ?>

	            <div style="float: left">
					&nbsp;&nbsp; Датум за Крај на понуда:
				</div>	
				<div style="width: 200px; float: left">
					<div class='input-group date' id='datetimepicker_end'>
						<input type='text' class="form-control" name="datepicker_end" value="" id="datepicker_end" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					<script type="text/javascript">
						$(function () {
							$('#datetimepicker_end').datetimepicker({
								format: 'YYYY-MM-DD HH:mm:ss',
								locale: 'mk'
							});
						});
					</script>
				</div>	
				
		

	            <div style="float: left">
					&nbsp;&nbsp; Датум за (Може да се искористи до):
				</div>	
				<div style="width: 200px; float: left">
					<div class='input-group date' id='datetimepicker_valid_to'>
						<input type='text' class="form-control" name="datepicker_valid_to" value="" id="datepicker_valid_to" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					<script type="text/javascript">
						$(function () {
							$('#datetimepicker_valid_to').datetimepicker({
								format: 'YYYY-MM-DD HH:mm:ss',
								locale: 'mk'
							});
						});
					</script>
				</div>



				<div >
					&nbsp;&nbsp;
					<input type="submit" name="group_end_time_submit" id="group_end_time_submit" value="Смени">
				</div>
				<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

			<?php } //if(!$deal->is_general_deal) { ?>		

			<br/><br/>
        </div>
	</form>
		



</div>
