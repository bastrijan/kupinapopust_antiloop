<?php $userID = $this->session->get("user_id"); ?>

<script type="text/javascript">

	function sold_out() {
		/*
		$.noticeAdd({
			text : "Почитувани, Ваучерите за оваа понуда се веќе распродадени. Со почит, Kupinapopust.mk тим",
			stay : false,
			type : 'notification-success'
		});
		*/
		$().toastmessage('showToast', {
			text     : "Почитувани, Ваучерите за оваа понуда се веќе распродадени. Со почит, Kupinapopust.mk тим",
			sticky   : false,
			position : 'top-center',
			type     : 'notice'
		});
		return false;
	}
	
	<?php if (isset($message) and $message == 'success') { ?>
			$(document).ready(function () {
				/*
				$.noticeAdd({
					text: "<?php print kohana::lang("index.Успешно го одјавивте вашиот e-mail!"); ?>
					",
					stay: false,
					type:'notification-success'
				});
				*/
				$().toastmessage('showToast', {
					text     : "<?php print kohana::lang("index.Успешно го одјавивте вашиот e-mail!"); ?>",
					sticky   : false,
					position : 'top-center',
					type     : 'success'
				});
			})
	<?php } ?>

	<?php if (isset($vaucer_rasprodadeno) and $vaucer_rasprodadeno == 1) { ?>
			$(document).ready(function () {
				/*
				$.noticeAdd({
					text: "Почитувани, за жал, во меѓувреме друг посетител има купено од понудата и со тоа понудата е распродадена.",
					stay: false,
					type: 'notification-error',
					stayTime: 20000
				});
				*/
				$().toastmessage('showToast', {
					text     : "Почитувани, за жал, во меѓувреме друг посетител има купено од понудата и со тоа понудата е распродадена.",
					sticky   : false,
					position : 'top-center',
					type     : 'error'
				});
			})    
	<?php } ?> 

</script>


<?php
	$controller = Router::$controller;
	$action = Router::$method;
?>
	
<div class="row">
	
		<div class="col-md-9 col-md-push-3">

		<?php
		
			if(count($primaryDeals) == 1){
				print "<h2>Понуда на денот</h2>";
			} 
			
		?>
			<?php
			$count = 0;
			foreach ($primaryDeals as $primaryDeal) 
			{
				$count++;
				
				$timeLeft = strtotime($primaryDeal->end_time) - time();
				$timeStart = strtotime($primaryDeal->start_time);
				$timeEnd = strtotime($primaryDeal->end_time);
				$timeNow = time();
				
				if($action == "demo") { //ova e za demo ponudite, zatoa sto kaj niv ne se stava start i end
					$progress = 50;
				} else {
					$progress = ((int)($timeNow - $timeStart) / (int)($timeEnd - $timeStart)) * 100;
				}
				
				if ($progress > 100) {
					$progress = 100;
				}
				
				//se pravi logika za toa sto da se prikaze koga ke se klikne na ponudata e istecena ili rasprodadena
				/* NE SE KORISTI
				$onclick = '';
				
				if ($progress >= 100 || $progress < 0) {
					$onclick = 'return false;';
				}
				if ($primaryDeal->voucher_count >= $primaryDeal->max_ammount and $primaryDeal->max_ammount) {
					$onclick = 'sold_out(); return false;';
				}
				*/
				$title_mk_clean = "";
				$title_mk = "";
				$max_ammount = 0;
				$ceni_od_txt = "";

				if($primaryDeal->options_cnt > 1 || $primaryDeal->is_general_deal)
				{
					$title_mk_clean =  $primaryDeal->title_mk_clean_deal;
					$title_mk =  $primaryDeal->title_mk_deal;
					$max_ammount = 0;
					$ceni_od_txt = ($primaryDeal->finalna_cena > 0 ? "од ".$primaryDeal->finalna_cena." ден." : "Превземи купон");
				}
				else
				{
					$title_mk_clean =  $primaryDeal->title_mk_clean;
					$title_mk =  $primaryDeal->title_mk;
					$max_ammount = $primaryDeal->max_ammount;
					$ceni_od_txt = ($primaryDeal->finalna_cena > 0 ? $primaryDeal->finalna_cena." ден." : "Превземи купон");
				}
				
			?>
				
				<div class="product-thumb">
					
					<!-- ///////////////////////// Detali za ponudata ///////////////////////// -->
					<header class="product-header">
						<?php
							$detali_za_ponudata = "";

							if($primaryDeal->is_general_deal)
								$detali_za_ponudata = "/deal/general_deal/" . seo::DealPermaLink($primaryDeal->deal_id, $title_mk_clean, $primaryDeal->category_id, $primaryDeal->subcategory_id, $categoriesData, $subCategoriesData);
							else
								$detali_za_ponudata = "/deal/index/" . seo::DealPermaLink($primaryDeal->deal_id, $title_mk_clean, $primaryDeal->category_id, $primaryDeal->subcategory_id, $categoriesData, $subCategoriesData);
							
						?>
						
						<!-- ///////////////////////// Slika za ponudata ///////////////////////// -->
						<a href ="<?php echo $detali_za_ponudata; ?>" target="_blank">
							<img src="/pub/deals/<?php print $primaryDeal->main_img; ?>" />
						</a>
						
						<!-- ///////////////////////// Procenti popust ///////////////////////// -->
						<!-- ako cenata e pogolema od nula-->
						<!-- i ako ne e GENERAL DEAL-->
						<?php if ($primaryDeal->price > 0 && !$primaryDeal->is_general_deal) { 
							$disc = (int)round(100 - ($primaryDeal->price_discount / $primaryDeal->price) * 100);
							echo '<span class="product-save popust-na-slika">'."- " . $disc . "%".'</span>';
						} ?>
						
					</header>
					
					<div class="product-inner">

						<!-- /////////////////////////  Naslov na ponudata ///////////////////////// -->
						<a  href ="<?php echo $detali_za_ponudata; ?>" target="_blank"><h4 class="bolder"><?php print $title_mk; ?></h4></a>
						
						<!-- ///////////////////////// So kupuvanje na ovoj vaucher dobivate "X" "kupinapopust" poeni ///////////////////////// -->
						<?php 
							//samo ako ne e lyoness korisnik
							//i ako ne e GENERAL DEAL
							if(!commonshow::isLyonessUser() && !$primaryDeal->is_general_deal)
							{
								$points_osnova = calculate::points_osnova($primaryDeal->price_discount, $primaryDeal->price_voucher, $primaryDeal->tip_danocna_sema, $primaryDeal->ddv_stapka, $primaryDeal->tip);
								if (floor($points_osnova) > 9) {  
							?>
								<h5 class="mb5">
									<?php print kohana::lang("index.Со купување на овој ваучер добивате"); ?>&nbsp;
									<?php 
										$pointsCalculated = calculate::points($points_osnova, 1);
										print $pointsCalculated; 
									?>&nbsp;
									<?php $pointsCalculated == 1 ? print '"kupinapopust" поен.' : print kohana::lang("index.Kupinapopust поени") ; ?>
								</h5>
						<?php 	
								}//if (floor($points_osnova) > 9) {  
							}//if(!commonshow::isLyonessUser())
						?>
						
						<div class="product-meta mt5">
							<span class="product-time">
							
								<!-- ///////////////////////// Vreme do krajniot rok na ponudata ///////////////////////// -->
								<i class="fa fa-clock-o"></i> 
								<?php echo commonshow::staticCountDown(strtotime($primaryDeal->start_time), strtotime($primaryDeal->end_time)); ?>

								<!-- ako ne e GENERAL DEAL-->
								<?php if (!$primaryDeal->is_general_deal) { ?>
										<!-- /////////////////////////  Kupeni se... ///////////////////////// -->
										<span class="text-green ml10">
											<?php $primaryDealsTooltipTxt = commonshow::tooltip($primaryDeal->voucher_count, $primaryDeal->min_ammount, $max_ammount); ?>
											<span class="buyers-count" title= "<?php echo $primaryDealsTooltipTxt; ?>">
												<?php
													if($primaryDeal->voucher_count > 0)
													{
														print commonshow::image($primaryDeal->voucher_count, $max_ammount);
														print '<strong'.(commonshow::isSoldOut($primaryDeal->voucher_count, $max_ammount) ? " style='color: red' " : "").'><i class="fa fa-level-up"></i>&nbsp;'.commonshow::number($primaryDeal->voucher_count, $primaryDeal->min_ammount, $max_ammount).commonshow::text($primaryDeal->voucher_count).'</strong>';
													}
												?>
											</span>
											<!-- 10 купувачи -->
										</span>
								<?php } ?>

							</span>
						
							<ul class="product-price-list border-top">
								

								<!-- /////////////////////////  Redovna cena ///////////////////////// -->
								<?php if($primaryDeal->options_cnt <= 1 && !$primaryDeal->is_general_deal) { ?>
									<li>
										<span class="product-old-price">
											<?php if($primaryDeal->price > 0) { ?>
													<?php print $primaryDeal->price; ?>
													<?php print kohana::lang("prevod.ден"); ?>
											<?php } ?>
											<!-- 6400 ден. -->
										</span>
									</li>
								<?php } ?>
								
								<!-- /////////////////////////  Cena so presmetan popust ///////////////////////// -->
								<?php if(!$primaryDeal->is_general_deal) { ?>
									<li>
										<a  href ="<?php echo $detali_za_ponudata; ?>" target="_blank">
											<span class="product-price">
												<?php print $ceni_od_txt; ?>
											</span>
										</a>
									</li>
									<li class="border-top width100 mr0 hidden-lg hidden-md "></li>
								<?php } ?>

								<!-- ///////////////////////// Detali za ponudata ///////////////////////// -->
								<li class="detali"><a href ="<?php echo $detali_za_ponudata; ?>" target="_blank" class="btn btn-sm" data-toggle="tooltip" data-placement="top" data-title="Детали"><i class="fa fa-bars"></i> ДЕТАЛИ</a>
								</li>
								
		
								<li><a href="#" class="btn btn-sm fav-button<?php echo in_array($primaryDeal->deal_id, $favDeals) ? ' fav-active' : ''; ?>" data-toggle="tooltip" data-placement="top" data-title="<?php echo in_array($primaryDeal->deal_id, $favDeals) ? 'Одземи од омилени' : 'Додади во омилени'; ?>">
										<i class="fa fa-star"></i>
										<i class="infoDealID" style="display: none;"><?php echo $primaryDeal->deal_id; ?></i>
									</a>
								</li>

								<?php if(!$primaryDeal->is_general_deal) { ?>
									<li <?php if(strtotime($primaryDeal->start_time) > time()) echo 'style="display: none;"'; ?> >
										<a href="#" class="btn btn-sm <?php echo $primaryDeal->options_cnt > 1 ? 'shop-cart-many-options': 'shop-cart-button'; ?> <?php echo in_array($primaryDeal->deal_option_id, $shopCartDeals) ? ' shop-cart-active' : ''; ?>" data-toggle="tooltip" data-placement="top" data-title="<?php echo in_array($primaryDeal->deal_option_id, $shopCartDeals) ? 'Одземи од кошничка' : 'Додади во кошничка'; ?>">
											<i class="fa fa-shopping-cart"></i>
											<i class="infoDealOptionID" style="display: none;"><?php echo $primaryDeal->deal_option_id; ?></i>
										</a>
									</li>
								<?php } ?>



							</ul>
						</div>
					</div>
				</div>
				
				<div class="gap gap-small"></div>
				

			<?php 
			} // foreach ($primaryDeals as $primaryDeal)  
			?>


		<!-- Header banner 2 START-->
		<?php
			$baner_location_2 = bannersystem::getBanner(2);
			// $baner_location_2 = "";

			if($baner_location_2 != "" ) 
			{ 
		?>
	
				<div >
					<?php print $baner_location_2;?>
				</div> 
				<div class="gap gap-small"></div>
		<?php } ?>


		<?php
		// require_once APPPATH . 'mobile_detect_master/Mobile_Detect.php';
		// $detect = new Mobile_Detect;

		// if( ($detect->isMobile() || $detect->isTablet()) && (strpos($_SERVER['REQUEST_URI'], "banner") === false) ) 
		// {
		?>
			<!-- 
			<div class="col-md-12">
			</div> --> 
		<?php
		// }
		// else
		// {
		?>
			<!-- <div >
			</div> -->
		<?php
		// }
		?>			
		<!-- <div class="gap gap-small"></div> -->
		
		<!-- Header banner 2 END-->
			<!-- SIDE OFFERS - BEGINNING -->
			<?php if (count($bottomOffers) > 0) { ?>

				
						<h3>Други активни понуди</h3>

						<?php
							$bottom_offers_div_class = "col-md-4";
							require_once APPPATH . 'views/index/bottom_offers.php';
						?>
			<?php } // if(count($bottomOffers) > 0) { ?>
		</div>
		<!-- /////////////////////////  MENI  /////////////////////////right: 77%; -->
		<div class="col-md-3 col-md-pull-9" style="padding-left: 0px; padding-right: 0px;">
			<aside class="sidebar-left">
			   <!-- <h3 class="mb20">Барам понуда за</h3>-->
				<?php
					require_once APPPATH . 'views/layouts/category_menu.php';
				?>
			</aside>
		</div>
</div> <!-- END OF: <div class="row"> -->
<?php
// if ($userID == 0 && cookie::get("newsletter_popup", 1, true) != 'true') {
//     require_once APPPATH . 'views/layouts/newsletter_popup.php';
// 	cookie::set("newsletter_popup", "true", 0);
// }

?>