<?php $userID = $this->session->get("user_id"); ?>

<script type="text/javascript" src="/pub/js/common_functions.js"></script>

<script type="text/javascript">

	function sold_out() {
		/*
		$.noticeAdd({
			text : "Почитувани, Ваучерите за оваа понуда се веќе распродадени. Со почит, Kupinapopust.mk тим",
			stay : false,
			type : 'notification-success'
		});
		*/
		$().toastmessage('showToast', {
			text     : "Почитувани, Ваучерите за оваа понуда се веќе распродадени. Со почит, Kupinapopust.mk тим",
			sticky   : false,
			position : 'top-center',
			type     : 'notice'
		});
		return false;
	}
	
	<?php if (isset($message) and $message == 'success') { ?>
			$(document).ready(function () {
				/*
				$.noticeAdd({
					text: "<?php print kohana::lang("index.Успешно го одјавивте вашиот e-mail!"); ?>
					",
					stay: false,
					type:'notification-success'
				});
				*/
				$().toastmessage('showToast', {
					text     : "<?php print kohana::lang("index.Успешно го одјавивте вашиот e-mail!"); ?>",
					sticky   : false,
					position : 'top-center',
					type     : 'success'
				});
			})
	<?php } ?>

	<?php if (isset($vaucer_rasprodadeno) and $vaucer_rasprodadeno == 1) { ?>
			$(document).ready(function () {
				/*
				$.noticeAdd({
					text: "Почитувани, за жал, во меѓувреме друг посетител има купено од понудата и со тоа понудата е распродадена.",
					stay: false,
					type: 'notification-error',
					stayTime: 20000
				});
				*/
				$().toastmessage('showToast', {
					text     : "Почитувани, за жал, во меѓувреме друг посетител има купено од понудата и со тоа понудата е распродадена.",
					sticky   : false,
					position : 'top-center',
					type     : 'error'
				});
			})    
	<?php } ?> 

	
	$(document).ready(function () {
		/*
		setTimeout(function(){
			$('#pokaziOstanati').hide();
		}, 3000);
		*/

		//dodavanje na clasa za responsive efekt na site sliki koi se pod element so klasa .imgIntervention
		$(".imgIntervention img").css({height: ''}).addClass("img-responsive");

	});  
	

	// ZA OPCIITE OD PONUDATA
	<?php if (count($optionsData) > 1) { ?>
				$(document)
				    .ready(function () {

						//krij go kopceto KUPI ZA
						$("#a-link-kupi-placeholder").hide();

						//krij go kopceto KUPI KAKO PODAROK
						$("#a-link-podari-placeholder").hide();

						//krij go redot (Ваучерот може да се искористи:)
						$("#period-na-iskoristuvanje-li").hide();

						//krij go redovite (simnato popust zasteduvas)
						$("#simnato-popust-zasteduvas-li").hide();

						//krij go redovite (Со купување на овој ваучер добивате XXX kupinapopust поени.)
						$("#dobivanje-poeni-placeholder").hide();
						
						//krij go rabotite povrzani so shopping cart-ot
						$("#shop-cart-span").hide();
						

				        $(".deal-options-a").bind("click", dealOptionsClickedHandler);

					   //za toogle na kontrolata so kategorii
					    $(".deal-options-select").click(function () {
					    

					        if($(".dropdown-menu-custom").hasClass("hidden"))
					        {
					            $(".dropdown-menu-custom").removeClass("hidden");
					        }
					        else 
					            if(!$(".dropdown-menu-custom").hasClass("hidden"))
					            {
					                $(".dropdown-menu-custom").addClass("hidden");
					            }   
					       
					    });
				    });

					$(document).click(function(e) {

				      	var subject = $(".dropdown"); 
				      	//alert($(e.target).attr('class')+'--'+subject.attr('class'));

				        if($(e.target).attr('class') != subject.attr('class') && !subject.has(e.target).length)
				        {
							if(!$(".dropdown-menu-custom").hasClass("hidden"))
					        {
					            $(".dropdown-menu-custom").addClass("hidden");
					        }
				        }


					});

					function dealOptionsClickedHandler(event) {

					    event.preventDefault();

						//prikazi gi skrienite polinja zavisni od opcijata
						$("#a-link-kupi-placeholder").show();
						$("#a-link-podari-placeholder").show();
						$("#period-na-iskoristuvanje-li").show();
						$("#simnato-popust-zasteduvas-li").show();
						$("#dobivanje-poeni-placeholder").show();

					    // DISABLED
					    var disabled = $(this).find(".deal-option-hidden-disabled").val();
					    if(disabled == 1)
					        return false;

					    //dodadi css klasa za oznacuvanje deka e selektirana opcijata
					    $(".deal-options-a").removeClass("selected-deal-options");
					    $(this).addClass("selected-deal-options");
					    

					    // TITLE
					    var title_str = $(this).find(".deal-option-hidden-title").html();
					    $("#deal-option-selected-title").html(jQuery('<div />').html(title_str).text());
					    
					    // DOBIVATE
					    var dobivate_str = $(this).find(".deal-option-hidden-dobivate").html();
					    $("#dobivate-placeholder").html(jQuery('<div />').html(dobivate_str).text());

					    /**** KUPI ZA ******/
					    var kupi_za = $(this).find(".deal-option-hidden-finalna_cena").val();
					    var kupi_za_str = "";
					    
					    if(kupi_za > 0)
					    	kupi_za_str = "Купи за " + kupi_za + " ден.";
					    else
					    	kupi_za_str = "Превземи купон";

					    $("#kupi-za-placeholder").html(kupi_za_str);

					    // SIMNATO OD
					    var price = $(this).find(".deal-option-hidden-price").val();
					    $("#simnato-od-placeholder").html(price);

					    // A-LINK KUPI i PODARI
					    var deal_id = $(this).find(".deal-option-hidden-deal_id").val();
					    var deal_option_id = $(this).find(".deal-option-hidden-deal_option_id").val();
					    $("#a-link-kupi-placeholder").attr("href", "/buy/deal/"+deal_id+"/"+deal_option_id);
					    $("#a-link-podari-placeholder").attr("href", "/buy/gift/"+deal_id+"/"+deal_option_id);

					    // PERIOD NA ISKORISTUVANJE
					    var valid_from = $(this).find(".deal-option-hidden-valid_from").val();
					    var valid_to = $(this).find(".deal-option-hidden-valid_to").val();

					    $("#period-na-iskoristuvanje-od-placeholder").html(valid_from);
					    $("#period-na-iskoristuvanje-do-placeholder").html(valid_to);
						
						
						// POPUST, ZASTEDUVAS I POENI
						var price_discount = $(this).find(".deal-option-hidden-price_discount").val();
						var popust = dealOptionDiscountDisplay(price, price_discount) 
						$("#procenti-popust-placeholder").html(popust);
						
						var savings = dealOptionSavingsDisplay(price, price_discount);
						$("#zasteduvas-placeholder").html(savings);

						
						var price_voucher = $(this).find(".deal-option-hidden-price_voucher").val();

						var tip_danocna_sema = $(this).find(".deal-option-hidden-tip_danocna_sema").val();
						var ddv_stapka = $(this).find(".deal-option-hidden-ddv_stapka").val();
						var tip = $(this).find(".deal-option-hidden-tip").val();

						//presmetuvanje na poenite
						var points = "";

						<?php if(!commonshow::isLyonessUser()) { ?>
							points = dealOptionDobivatePoeniDisplay(price_discount, price_voucher, tip_danocna_sema, ddv_stapka, tip) ;
						<?php }//if(!commonshow::isLyonessUser()) { ?>

						$("#dobivanje-poeni-placeholder").html(points);


						//raboti povrzani so shopping cart-ot
						$("#shop-cart-span").show();
						$(".infoDealOptionID").html(deal_option_id);
							// get cookie
							var cookieValue = $.cookie("kupinapopust_shop_cart");
							
							// check if the cookie exists
							if (cookieValue != undefined) {
								// cookie exists
								var shop_cart_deals = JSON.parse(cookieValue);

								//dokolku deal_option_id se naoga vo cookie-to 
								if ( jQuery.inArray( deal_option_id, shop_cart_deals ) > -1 ) {
									// prikazi zelena boja
									$("#a-link-shop-cart-placeholder").addClass("shop-cart-active");
									// change appearance
									$("#a-link-shop-cart-placeholder").css("color", "white");
									// change tool-tip
									$("#a-link-shop-cart-placeholder").tooltip("hide").attr("data-original-title", "Одземи од кошничка");

								} else {
									// prikazi siva boja
									$("#a-link-shop-cart-placeholder").removeClass("shop-cart-active");
									// change appearance
									$("#a-link-shop-cart-placeholder").css("color", "#333");
									// change tool-tip
									$("#a-link-shop-cart-placeholder").tooltip("hide").attr("data-original-title", "Додади во кошничка");
								}
							}


						//zatvori
				        if(!$(".dropdown-menu-custom").hasClass("hidden"))
				        {
				            $(".dropdown-menu-custom").addClass("hidden");
				        }  

				        //scrollaj
				        $('html,body').animate({scrollTop: $('#scrollHereOptions').offset().top}, 500);
					}
	<?php } ?> 


	// Countdown
	$(function() {
	    $('.countdown').each(function() {
	        var count = $(this);
	        $(this).countdown({
	            zeroCallback: function(options) {
					$('.koga_istece').click(function() {
						event.preventDefault();
					    return false;
					});	                
	                
					$('.koga_istece').contextmenu(function() {
						event.preventDefault();
					    return false;
					});	  
	            }
	        });
	    });
	});
	
</script>


<?php
$controller = Router::$controller;
$action = Router::$method;
?>
	
	
<!-- ///////////////////////// VIEW ZA DETALI OD KONKRETNA PONUDA (DEAL) ///////////////////////// -->

<?php
	
	$timeLeft = strtotime($optionsData[0]->end_time) - time();
	$timeStart = strtotime($optionsData[0]->start_time);
	$timeEnd = strtotime($optionsData[0]->end_time);
	$timeNow = time();
	
	if($action == "demo") { //ova e za demo ponudite, zatoa sto kaj niv ne se stava start i end
		$progress = 50;
	} else {
		$progress = ((int)($timeNow - $timeStart) / (int)($timeEnd - $timeStart)) * 100;
	}
	
	if ($progress > 100) {
		$progress = 100;
	}
	
	//se pravi logika za toa sto da se prikaze koga ke se klikne na ponudata e istecena ili rasprodadena
	$onclick = '';
	$oncontextmenu = '';

	if ($progress >= 100 || $progress < 0) {
		$onclick = 'return false;';
		$oncontextmenu = 'oncontextmenu="return false;"';
	}
	if ($optionsData[0]->voucher_count >= $optionsData[0]->max_ammount and $optionsData[0]->max_ammount) {
		$onclick = 'sold_out(); return false;';
		$oncontextmenu = 'oncontextmenu="return false;"';
	}

	$voucher_count_deal = 0;
?>
		
	<div class="row">		

		<div class="col-md-3">
			<div class="product-page-meta box">
				
				<!-- /////////////////////////  Naslov na ponudata ///////////////////////// -->
				<h4 class="deal_details_font_format_title"><?php print (count($optionsData) > 1 ? $optionsData[0]->title_mk_deal : $optionsData[0]->title_mk); ?></h4>

				<!-- /////////////////////////  LISTANJE NA OPCIITE (DOPOLKU E PONUDA SO POVEKJE OPCII) ///////////////////////// -->
				<?php if(count($optionsData) > 1) { ?>
							

					<br/>
					<div class="text-font-20px" id="scrollHereOptions">Опции:</div>
					<div class="dropdown"  data-original-title="" data-animation="false" data-easein="wobble"  rel="popover" data-placement="top" data-content="Кликни за приказ на опции">
						<div class="deal-options-select" id="deal-option-selected-title"><strong>ОДБЕРЕТЕ ОПЦИЈА</strong></div>

						<!--	
					    <button class="btn btn-default dropdown-toggle" type="button" id="dealOptionSelected" data-toggle="dropdown">
					    	SELECTED
					    </button>

	                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
	                        <span class="caret"></span>
	                    </button>
						-->
					    <ul class="dropdown-menu-custom hidden" >
					     
	                         <?php
	                         	$cntOption = 1;
	                            foreach ($optionsData as $option)
	                            {

	                            	$voucher_count_deal += $option->voucher_count;

	                            	        //echo( "valid_to==".$option->valid_to);
	                            	$disabledOptionTxt = "";
	                            	$disabledBool = commonshow::disableDealOption($option->voucher_count, $option->max_ammount, $option->valid_to, $disabledOptionTxt);

	                            	$disabledOptionTxt .= $disabledBool == 1 ? "<br/>" : "";

	                            	if($cntOption > 1)
	                            		echo '<li class="divider"></li>';
	                         ?>
	                         	  

			 					  <li <?php print ( $disabledBool == 1 ? 'class="disabled"' : "" );?> >
			 					  	<a class="deal-options-a" href="#">
				 					  	<div><?php print $disabledOptionTxt.$option->title_mk; ?></div>

										<div>
											<!-- /////////////////////////  Redovna cena vo Opcija ///////////////////////// -->
											<span class="product-old-price-option">
												<?php if($option->price > 0) { ?>
														<?php print $option->price; ?>
														<?php print kohana::lang("prevod.ден"); ?>
												<?php } ?>
											</span>
											
											&nbsp;&nbsp;

											<!-- /////////////////////////  Cena so presmetan popust  vo Opcija ///////////////////////// -->
											<span class="product-price-option">
												<?php //if ($option->tip == 'cena_na_vaucer') print $option->price_voucher; else print $option->price_discount; ?>
												<?php //print $option->finalna_cena; ?>
												<?php //print kohana::lang("prevod.ден"); ?>

												<?php print ($option->finalna_cena > 0 ? $option->finalna_cena." ден." : "Превземи купон"); ?>
											</span>
										
										</div>

										<!-- hidden informacii-->
										<div class="deal-option-hidden-title" style="display: none"><?php print htmlentities($option->title_mk, ENT_QUOTES, "UTF-8"); ?></div>
										<div class="deal-option-hidden-dobivate" style="display: none"><?php print htmlentities($option->content_short_mk, ENT_QUOTES, "UTF-8"); ?></div>
										<input type="hidden" class="deal-option-hidden-deal_option_id" value="<?php print $option->deal_option_id; ?>">
										<input type="hidden" class="deal-option-hidden-price" value="<?php print $option->price; ?>">	
										<input type="hidden" class="deal-option-hidden-price_discount" value="<?php print $option->price_discount; ?>">
										<input type="hidden" class="deal-option-hidden-price_voucher" value="<?php print $option->price_voucher; ?>">
										<input type="hidden" class="deal-option-hidden-finalna_cena" value="<?php print $option->finalna_cena; ?>">

										<input type="hidden" class="deal-option-hidden-max_ammount" value="<?php print $option->max_ammount; ?>">

										<input type="hidden" class="deal-option-hidden-valid_from" value="<?php print date("d.m.Y", strtotime($option->valid_from)); ?>">
										<input type="hidden" class="deal-option-hidden-valid_to" value="<?php print date("d.m.Y", strtotime($option->valid_to));  ?>">

										<input type="hidden" class="deal-option-hidden-disabled" value="<?php print $disabledBool; ?>">

										<input type="hidden" class="deal-option-hidden-deal_id" value="<?php print $option->deal_id; ?>">

										<input type="hidden" class="deal-option-hidden-tip_danocna_sema" value="<?php print $optionsData[0]->tip_danocna_sema; ?>">
										<input type="hidden" class="deal-option-hidden-ddv_stapka" value="<?php print $optionsData[0]->ddv_stapka; ?>">
										<input type="hidden" class="deal-option-hidden-tip" value="<?php print $optionsData[0]->tip; ?>">

			 					  	</a>
			 					  </li>

	                         <?php
	                         		$cntOption++;   	
	                            }	
	                         ?>
					     <!--
					      <li ><a href="#">HTML</a></li>
					      <li class="disabled"><a href="#">CSS</a></li>
					      <li><a href="#">JavaScript</a></li>
					      <li class="divider"></li>
					      <li><a href="#">About Us</a></li>
					     -->
					    </ul>
					</div>
					<div class="gap-mini"></div>
				<?php }//if(count($optionsData) > 1) { 
					  else // ako postoi samo edna aktivna opcija
					  {
					  	$voucher_count_deal += $optionsData[0]->voucher_count;
					  }		

					?>


				<!-- ///////////////////////// So kupuvanje na ovoj vaucher dobivate "X" "kupinapopust" poeni ///////////////////////// -->
				<span id="dobivanje-poeni-placeholder">
				<?php 
					//samo ako ne e lyoness korisnik
					if(!commonshow::isLyonessUser())
					{
						$points_osnova = calculate::points_osnova($optionsData[0]->price_discount, $optionsData[0]->price_voucher, $optionsData[0]->tip_danocna_sema, $optionsData[0]->ddv_stapka, $optionsData[0]->tip);
						if (floor($points_osnova) > 9) 
						{  
					?>
							<p>
								<?php print kohana::lang("index.Со купување на овој ваучер добивате"); ?>
								<?php 
									$pointsCalculated = calculate::points($points_osnova, 1);
								?>
								
									<?php
										print $pointsCalculated; 
									?>
									
									<?php $pointsCalculated == 1 ? print '"kupinapopust" поен.' : print kohana::lang("index.Kupinapopust поени") ; ?>
								
							</p>
				<?php 
						}//if (floor($points_osnova) > 9) {  
					}//if(!commonshow::isLyonessUser())
				?>
				</span>

				<!-- /////////////////////////  Kopce: KUPI ZA ... ///////////////////////// -->
				<?php if ($optionsData[0]->voucher_count >= $optionsData[0]->max_ammount && $optionsData[0]->max_ammount) { ?>								
						<a id="a-link-kupi-placeholder" href ="/buy/deal/<?php print $optionsData[0]->deal_id; ?>/<?php print $optionsData[0]->deal_option_id; ?>" class="btn btn-lg btn-block btn-danger" onclick="<?php print $onclick; ?>" <?php print $oncontextmenu; ?>>
							<i class="fa fa-shopping-cart"></i> Распродадено
						</a>
				<?php } elseif (0 < $progress && $progress < 100) { ?>
						<a id="a-link-kupi-placeholder" href ="/buy/deal/<?php print $optionsData[0]->deal_id; ?>/<?php print $optionsData[0]->deal_option_id; ?>" class="btn btn-lg btn-block green koga_istece">
							<i class="fa fa-shopping-cart"></i> 
							
							<span id="kupi-za-placeholder">
								<?php print ($optionsData[0]->finalna_cena > 0 ? "Купи за ".$optionsData[0]->finalna_cena." ден." : "Превземи купон"); ?>
								 	
								<?php //STARO  if ($optionsData[0]->tip == 'cena_na_vaucer') print $optionsData[0]->price_voucher; else print $optionsData[0]->price_discount; ?>
							</span>	
						</a>
				<?php } else { ?>
						<a id="a-link-kupi-placeholder" href ="/buy/deal/<?php print $optionsData[0]->deal_id; ?>/<?php print $optionsData[0]->deal_option_id; ?>" class="btn btn-lg btn-block btn-danger" onclick="return false;" oncontextmenu="return false;">
							<i class="fa fa-shopping-cart"></i> <?php if($action == "demo") print "Демо"; elseif($progress < 0 )  print "Наскоро" ;else print kohana::lang("prevod.Истечено"); ?>
						</a>
				<?php } ?>

				<!-- /////////////////////////  Kupi kako podarok ///////////////////////// -->
				<?php if($optionsData[0]->email_content_buy_switch == 0) { ?>
					<a id="a-link-podari-placeholder" href="/buy/gift/<?php print $optionsData[0]->deal_id; ?>/<?php print $optionsData[0]->deal_option_id; ?>" onclick="<?php print $onclick; ?>" <?php print $oncontextmenu; ?> class="btn btn-primary btn-sm podaroce koga_istece" data-toggle="tooltip" data-placement="top" data-title="Подари">
						<i class="fa fa-gift"></i>  <span>Купи како подарок</span>
					</a>
				<?php } ?>
			  
				<span class="product-save ">
					<a href="#" class="btn btn-sm podaroce fav-button<?php echo in_array($optionsData[0]->deal_id, $favDeals) ? ' fav-active' : ''; ?>" data-toggle="tooltip" data-placement="top" data-title="<?php echo in_array($optionsData[0]->deal_id, $favDeals) ? 'Одземи од омилени' : 'Додади во омилени'; ?>">
						<i class="fa fa-star"></i>
						<i class="infoDealID" style="display: none;"><?php echo $optionsData[0]->deal_id; ?></i>
					</a>
				</span>
				
				<span class="product-save " id="shop-cart-span" <?php if(strtotime($optionsData[0]->start_time) > time() || time() > strtotime($optionsData[0]->end_time)) echo 'style="display: none;"'; ?> >
					<a id="a-link-shop-cart-placeholder" href="#" class="btn btn-sm podaroce shop-cart-button<?php echo in_array($optionsData[0]->deal_option_id, $shopCartDeals) ? ' shop-cart-active' : ''; ?>" data-toggle="tooltip" data-placement="top" data-title="<?php echo in_array($optionsData[0]->deal_option_id, $shopCartDeals) ? 'Одземи од кошничка' : 'Додади во кошничка'; ?>">
						<i class="fa fa-shopping-cart"></i>
						<i class="infoDealOptionID" style="display: none;"><?php echo $optionsData[0]->deal_option_id; ?></i>
					</a>
				</span>
			  
				<ul class="list product-page-meta-info">
					<?php if($optionsData[0]->price > 0) { ?>
						<li id="simnato-popust-zasteduvas-li">
							<ul class="list product-page-meta-price-list">
								<!-- /////////////////////////  Simnato od ///////////////////////// -->
								
	 								<li>
	 									<span class="product-page-meta-title">Симнато од</span>
	 									<span class="product-page-meta-price">
											<span id="simnato-od-placeholder">
												<?php echo $optionsData[0]->price; ?>
											</span>
											<?php echo " " .kohana::lang("prevod.ден"); ?>
											
	 									</span>
	 								</li>
								

								<!-- /////////////////////////  Procenti popust ///////////////////////// -->

 								<li>
 									<span class="product-page-meta-title">Попуст</span>
 									<span class="product-page-meta-price">
										<span id="procenti-popust-placeholder">
											<?php 
													$disc = 100 - ($optionsData[0]->price_discount / $optionsData[0]->price) * 100;
													
													// Presmetaj kolku zashteduvash, ke treba podolu
													$zasteduvas = (int)round($optionsData[0]->price * $disc / 100);
											?>

													<?php echo round($disc); ?>%
											
										</span>
 									</span>
 								</li>

								
								
								<!-- /////////////////////////  Zashteduvash ///////////////////////// -->
								<?php if (isset($zasteduvas)) { ?>
									<li>
	 									<span class="product-page-meta-title">Заштедуваш</span>
	 									<span class="product-page-meta-price">
											<span id="zasteduvas-placeholder">
												
														<?php echo $zasteduvas; ?>
														ден.
												
											</span>
	 										
	 									</span>
	 								</li>
								<?php } ?>
							</ul>
						</li>
					<?php } ?>	
					
					<!-- /////////////////////////  Preostanato vreme ///////////////////////// -->
					<?php if (0 < $progress && $progress < 100) { ?>
					<li><span class="product-page-meta-title">Преостанато време</span>
						<!-- COUNTDOWN -->
						<!-- <div data-countdown="Aug 25, 2015 10:45:00" class="countdown countdown-inline"></div> -->
							<div id="preostanato_vreme" data-countdown="<?php echo date("M j, Y H:i:s", strtotime($optionsData[0]->end_time)); ?>" class="countdown countdown-inline"></div>
					</li>
					<?php } ?>
					
					<!-- /////////////////////////  Kupuvachi ///////////////////////// -->
					<?php
					if($voucher_count_deal > 0)
					{
					?>
					<li>
						<span class="product-page-meta-title">
							<?php 
							$max_ammount = count($optionsData) > 1 ? 0 : $optionsData[0]->max_ammount;
							$primaryDealsTooltipTxt = commonshow::tooltip($voucher_count_deal, $optionsData[0]->min_ammount, $max_ammount); ?>
									<span class="buyers-count" title= "<?php echo $primaryDealsTooltipTxt; ?>">
										<?php

											print '<span class="text-font-25px" '.(commonshow::isSoldOut($voucher_count_deal, $max_ammount) ? "style='color: red' " : "style='color: #53a318' ").'>'.commonshow::number($voucher_count_deal, $optionsData[0]->min_ammount, $max_ammount).'</span><span class="text-font-20px">'.commonshow::text($voucher_count_deal).'</span>';
										
										?>
									</span>
						</span>
					</li>
					<?php
					}
					?>

					<!-- /////////////////////////  period na iskoristuvanje ///////////////////////// -->
					<li id="period-na-iskoristuvanje-li">
						<span class="product-page-meta-title" >
							<strong><?php print kohana::lang("prevod.Ваучерот може да се искористи:"); ?></strong><br/>

							<?php print kohana::lang("prevod.od"); ?>&nbsp;
							
							<span id="period-na-iskoristuvanje-od-placeholder">
								<?php print date("d.m.Y", strtotime($optionsData[0] -> valid_from)); ?>&nbsp;
							</span>
							
							<?php print kohana::lang("prevod.do"); ?>&nbsp;
 
							<span id="period-na-iskoristuvanje-do-placeholder">
								<?php print date("d.m.Y", strtotime($optionsData[0] -> valid_to)); ?>&nbsp;							
							</span>
						</span>
					</li>
				</ul>
				
				<div class="gap gap-ponuda"></div>
				
				<div class="social-med-ponuda row">                   
					<div class="col-md-12">
						<?php
						$url_fb = Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/deal/index/" . $optionsData[0]->deal_id;
						?>
						<div class="fb-like" data-href="<?php print $url_fb; ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>

						<!-- <div class="gap-small"></div> -->

						<div class="fb-send" data-href="<?php print $url_fb; ?>" data-layout="button_count"></div>

						<!-- OSTANATI SOCIAL KOPCINJA-->
						<br/><br/>
						<!--
						<a class="togle-vaucher"><i class="fa fa-plus-square"></i> Покажи останати</a>
						-->
						<div  id="pokaziOstanati">

							<!--  TWITTER -->	
							<a href="https://twitter.com/share" class="twitter-share-button" data-lang="en">Tweet</a>
	
							&nbsp;&nbsp;&nbsp;

							<!--  GOOGLE + -->	
							<!-- Place this tag where you want the +1 button to render -->
							<g:plusone size="medium"></g:plusone>

						</div>

					</div>

					<!--
					<div class="col-md-12">
						<a href="https://twitter.com/share" class="twitter-share-button" data-url="@Model.FriendlyUrl" data-text="@Model.Name" data-via="KlikniJadi">Tweet</a>
						<script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + '://platform.twitter.com/widgets.js'; fjs.parentNode.insertBefore(js, fjs); } }(document, 'script', 'twitter-wjs');</script>
					 </div>
					 
					<div class="col-md-12">
						<div class="g-plusone" data-size="medium"></div>
					</div>
					<div class="col-md-12">
						<i class="fa fa-share-alt-square fs40"></i> 
					</div>
					-->
				 </div>

			</div>
			
			<div class="gap"></div>

			<!-- Header banner 3 START-->
			<?php
				$baner_location_3 = bannersystem::getBanner(3);
				
				if($baner_location_3 != "" ) 
				{ 
			?>

					<div class="hidden-xs hidden-sm">
						<?php print $baner_location_3;?>
					</div> 
					<div class="gap gap-mini hidden-xs hidden-sm"></div>
			<?php } ?>
			<!-- Header banner 3 END-->


			<!-- BANNER ZA EUROLINK--> 

			<?php
				$eurolink_insurance_type = NULL;

				//ako sme vo kategorija PROIZVODI togas prikazi domasno osiguruvanje (property)	
				if($optionsData[0]->category_id == 25)
				{
					$eurolink_insurance_type = "travel";  
					$eurolink_insurance_img_ext = "travel.gif";  
				}
				elseif( $optionsData[0]->category_id == 20)
				{
					$eurolink_insurance_type = "property";
					$eurolink_insurance_img_ext = "property.jpg";
				}

			?>

			<?php if(!empty($eurolink_insurance_type)) { ?>
				<div class="hidden-xs hidden-sm">
					<a href="https://shop.eurolink.com.mk/EurolinkWebShop/shop/wizard?type=<?php print $eurolink_insurance_type;?>&from=770" target="_blank" >			
						<img src="/pub/img/layout/eurolink-<?php print $eurolink_insurance_img_ext;?>">
					</a>
				</div>
			<?php }//if($eurolink_insurance_type != "") { ?>
			   
		</div>

		<div class="col-md-9">

			<div class="row row-no-gutter  hidden-xs hidden-sm" id="popup-gallery">
				<?php 
					for($img_i = 0 ; $img_i <= 5 ; $img_i++)
					{
						if($img_i == 0)
							$dynamic_img_name = "side_img";
						else	
							$dynamic_img_name = "additional_img_".$img_i;
						
						if($optionsData[0]->$dynamic_img_name != "")
						{
				?>
							<div class="col-md-4">
								<!-- HOVER IMAGE -->
								<?php if(is_file(DOCROOT . 'pub/deals/'.$optionsData[0]->$dynamic_img_name)) { ?>
									<a class="hover-img popup-gallery-image" href="/pub/deals/<?php echo $optionsData[0]->$dynamic_img_name ;?>" data-effect="mfp-zoom-out">
										<img src="/pub/deals/<?php echo $optionsData[0]->$dynamic_img_name ;?>" /><i class="fa fa-resize-full hover-icon"></i>
									</a>
								<?php } ?>
							</div>
				<?php
						}//if($optionsData[0]->$dynamic_img_name != "")
					}//for($img_i = 1 ; $img_i <= 6 ; $img_i++)
				?>
			</div>

			<div class="fotorama hidden-md hidden-lg">
				<?php 
					for($img_i = 0 ; $img_i <= 5 ; $img_i++)
					{
						if($img_i == 0)
							$dynamic_img_name = "side_img";
						else	
							$dynamic_img_name = "additional_img_".$img_i;
						
						if($optionsData[0]->$dynamic_img_name != "")
						{
				?>
								<a class="hover-img popup-gallery-image" href="/pub/deals/<?php echo $optionsData[0]->$dynamic_img_name ;?>" data-effect="mfp-zoom-out">
									<img src="/pub/deals/<?php echo $optionsData[0]->$dynamic_img_name ;?>" />
								</a>
							
				<?php
						}//if($optionsData[0]->$dynamic_img_name != "")
					}//for($img_i = 1 ; $img_i <= 6 ; $img_i++)
				?>
                
            </div>
		
			<div class="tabbable mt5">
				<ul class="nav nav-tabs" id="myTab">
					<li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-tag"></i>Добивате</a>
					</li>
					<li><a href="#google-map-tab" data-toggle="tab"><i class="fa fa-map-marker"></i>Локација</a>
					</li>
					<li><a href="#tab-3" data-toggle="tab"><i class="fa fa-info"></i>Услови</a>
					</li>
					<li><a href="#tab-4" data-toggle="tab"><i class="fa fa-info"></i><?php echo ($partnerData[0]->za_kompanijata_label != "" ? $partnerData[0]->za_kompanijata_label : "За компанијата") ; ?> </a>
					</li>
				</ul>
				<div class="tab-content">
					<!-- /////////////////////////  Dobivate ///////////////////////// -->
					<div class="tab-pane fade in active" id="tab-1">
						<div class="row deal_details_font_format_tabs">
							<div class="col-md-12 imgIntervention">
								<div id="dobivate-placeholder">
									<?php
										print  (count($optionsData) > 1 ? $optionsData[0]->content_short_mk_deal : $optionsData[0]->content_short_mk);
									?>
								</div>

								<div class="gap"></div>
								
								<!-- VIDEO -->
								<?php if($optionsData[0]->ponuda_video != "" && is_file(DOCROOT . 'pub/videos/'.$optionsData[0]->ponuda_video)) { ?>
								  <video id="<?php echo "example_video_".$optionsData[0]->deal_id;?>" class="video-js vjs-default-skin" controls preload="none" width="100%" height="300"
									  poster="/pub/deals/<?php print $optionsData[0]->main_img; ?>"
									  data-setup="{}">
									<source src="/pub/videos/<?php echo $optionsData[0]->ponuda_video; ?>" type='video/<?php echo pathinfo($optionsData[0]->ponuda_video, PATHINFO_EXTENSION); ?>' />
									<!--
									<track kind="captions" src="demo.captions.vtt" srclang="en" label="English" />
									-->
								  </video>
								<?php } //if($optionsData[0]->ponuda_video != "") { ?>
							</div>						
						</div>
					</div>
					
					<!-- /////////////////////////  Lokacija ///////////////////////// -->
					<div class="tab-pane fade" id="google-map-tab">
						<div class="row deal_details_font_format_tabs">
							<div class="col-md-9">
								<?php
									$google_link_final = str_replace ( "http://" , "https://" , $partnerData[0] -> google_link );

									$google_link_final = strpos($google_link_final, Kohana::config('settings.google_api_browser_key')) !== false ? $google_link_final : $google_link_final."&key=".Kohana::config('settings.google_api_browser_key');									
								?>

								<a href="<?php print $partnerData[0] -> google_url; ?>" target="_blank">
									<img alt="" src="<?php print $google_link_final; ?>" style="width: 100%;"/>
								</a>
							</div>
							<div class="col-md-3">
								<?php
									$langAdress = "adress_" . $lang;
									print $partnerData[0]->$langAdress;
								?>
							</div>
						</div>
					</div>
					
					<!-- /////////////////////////  Uslovi ///////////////////////// -->
					<div class="tab-pane fade imgIntervention" id="tab-3">
						<div class="row deal_details_font_format_tabs">
							<?php
								$terms = "terms_" . $lang;
								print $partnerData[0]->$terms;
							?>
						</div>
					</div>

					<!-- /////////////////////////  Za kompanijata ///////////////////////// -->
					<div class="tab-pane fade imgIntervention" id="tab-4">
						<div class="row deal_details_font_format_tabs">
							<?php
								print $partnerData[0]->za_kompanijata_text;
							?>
						</div>
					</div>
				</div>
			</div>
			
			<div class="gap gap-small"></div>

		</div>
		
	</div> <!-- END OF: <div class="row"> -->
	
	<div class="row">
		<div class="col-md-3 hidden-xs hidden-sm" style="padding-left: 0px; padding-right: 0px;">
			<div class="gap gap-small"></div>
			<!-- /////////////////////////  MENI  ///////////////////////// -->
			<?php 
				require_once APPPATH . 'views/layouts/category_menu.php';
			?>
                          
			<!--
			<img class="img-responsive hidden-xs" src="img/020215014417988gamarde-baner.gif" />
			<div class="gap gap-small hidden-xs"></div>
			-->
		</div>

		<div class="col-md-9">

			<!-- Header banner 2 START-->
			<?php
				$baner_location_2 = bannersystem::getBanner(2);
				
				if($baner_location_2 != "" ) 
				{ 
			?>
		
					<div >
						<?php print $baner_location_2;?>
					</div> 
					<div class="gap gap-small"></div>
			<?php } ?>
			<!-- Header banner 2 END-->

			

			<?php if (count($bottomOffers) > 0) { ?>
		
						<h3 class="mb20">Може да ве интересираат и следниве понуди:</h3>

				<?php
					$bottom_offers_div_class = "col-md-4";
					require_once APPPATH . 'views/index/bottom_offers.php';
				?>
			<?php } // if(count($bottomOffers) > 0) { ?>



		</div> <!-- END OF: div class="col-md-9" -->
		
	</div> <!-- END OF: <div class="row"> -->




	

<link rel="stylesheet" href="/pub/css/animate.css" />

<script type="text/javascript">
	
	$(function() {
	    var b = "fadeInLeft";

		$( "div[rel=popover]" ).each(function(f) {

			$(this).popover('show');

			//alert($(this).data("easein"));
	        //f.preventDefault();
	        if ($(this).data("easein") != undefined) {
	        	// alert($(this).next().html());
	            $(this).next().removeClass($(this).data("easein")).addClass("animated " + $(this).data("easein"));
	        } else {
	        	// alert("2");
	            $(this).next().addClass("animated " + b);
	        }

		});

		var popoverSetInterval = setInterval(function(){
		 		$( "div[rel=popover]" ).popover('show');
		}, 3000);

	    $("div[rel=popover]").popover().click(function(f) {
			
		      $(this).popover('hide');

			  clearInterval(popoverSetInterval);
			  popoverSetInterval = null

	    });
		
		//da se vkluci pri loadiranje na strana
		// $( "#animated-popover" ).trigger( "click" );




	});

</script>

<style type="text/css">
	 /* Popover */
	.popover {
	    border: 2px solid #FF6600;
	    padding: 0px;
	    margin:  0px;
	}
	 

	/* Popover Body */
	.popover-content {
	    background-color: #FF6600;
	    color: #FFFFFF;
	    font-weight: bold;

	}
	 
	/* Popover Arrow */
	.popover.top > .arrow::after {

	    border-top-color: #FF6600;

	}
	
</style>