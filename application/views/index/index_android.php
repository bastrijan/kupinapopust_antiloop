<style>
    div.deal_head h3{
        text-align: center;
    }

    div.deal_head img{
        display: block;
        width: 100%;
    }

    div.clock{
        border: 1px solid #bebebe;
        border-radius: 7px;
        font-size: 17px;
        line-height: 30px;
        color: #333;
        white-space: nowrap;
        width: 100%;
        overflow: hidden;
        margin-top: 10px;
        text-align: center;
    }

    div.bay_btn{
        float: left;
        background-color: #48b548;
        box-shadow: 0 1px 2px #ccc;
        color: #fff;
        font-style: normal;
        font-weight: bold;
        line-height: 50px;
        text-shadow: 1px 1px 2px #444;
        width: 100%;
    }
    div.gift_btn{
        float: left;
        box-shadow: 0 1px 2px #ccc;
        color: #fff;
        font-style: normal;
        font-weight: bold;
        line-height: 20px;
        padding: 15px;
        text-shadow: 1px 1px 2px #444;
        width: 100%;
        background-color: #FF6600;
    }
    div.align_btn{
        display: block;
        width: 215px;
        margin: auto;
    }
    
    div.gift_btn img{
        width: 22px;
        height: 20px;
        display: inherit;
        float: left;
    }
    
    div.gift_btn a{
        float: left;
        margin-left: 12px;
        color: #fff;
    }
    
    div.deal_price, div.deal_buyers{
        float: left;
        display: block;
        margin-left: 10px;
    }

    div.deal_clock{
        float: right;
        display: block;
        margin-right: 30px;
    }

    .green_kupuvaci {
        background: url(../../pub/img/layout/layout-sprite_popust.png) 0 -217px no-repeat;
        padding-left: 25px;
    }

    .green_clock_bilboard {
        float: left;
        margin-top: 5px;
        text-align: center;
        background: url(../../pub/img/layout/layout-sprite_popust.png) 0 -196px no-repeat;
        width: 20px;
        height: 20px;
    }

    div.deal_content{
        display: block;
        margin: 12px;
    }

    .deal_content p img{
        width: 100%!important;
        height: auto!important;
    }

    div.deal_partner{
        display: block;
        padding: 10px;
        background-color: #E8E6E7;
    }
    div.deal_partner h3{
        display: block;
        font-size: 13px;
        color: #111;
    }
    div.deal_partner_info{
        border: 1px solid #bebebe;
        border-radius: 6px;
        background-color: #fff;
        padding: 12px;
        text-align: center;
    }
    
    


</style>

    <?php
    $action = Router::$method;
    $count = 0;
//                var_dump($primaryDeals);
    foreach ($primaryDeals as $primaryDeal) {
        $count ++;


        //se pravi logika za vremeto da se prikaze
        $timeLeft = strtotime($primaryDeal->end_time) - time();
        $timeStart = strtotime($primaryDeal->start_time);
        $timeEnd = strtotime($primaryDeal->end_time);
        $timeNow = time();


        if ($action == "demo")//ova e za demo ponudite, zatoa sto kaj niv ne se stava start i end
            $progress = 50;
        else
            $progress = ((int) ($timeNow - $timeStart) / (int) ($timeEnd - $timeStart)) * 100;

        if ($progress > 100) {
            $progress = 100;
        }

        //se pravi logika za toa sto da se prikaze koga ke se klikne na ponudata e istecena ili rasprodadena
        $onclick = '';
        if ($progress >= 100) {
            $onclick = 'return false;';
        }
        if ($primaryDeal->voucher_count >= $primaryDeal->max_ammount and $primaryDeal->max_ammount) {
            $onclick = 'sold_out();return false;';
        }
        ?>

        <div class="deal_head">

            <img src="/pub/deals/<?php print $primaryDeal->side_img ?>" alt="<?php print strip_tags($primaryDeal->title_mk_clean); ?>"/>
            
            <h3><?php print strip_tags($primaryDeal->title_mk); ?></h3>
            
            <div class="clock">
                
                <!--bay button-->
                <div class="bay_btn" id="deal-<?php print $primaryDeal->id; ?>">
                    <?php print kohana::lang("prevod.Купи"); ?> за <?php
                    if ($primaryDeal->tip == 'cena_na_vaucer')
                        print $primaryDeal->price_voucher;
                    else
                        print $primaryDeal->price_discount;
                    ?>
                    <?php print kohana::lang("prevod.ден"); ?> 
                </div>
                <!-- end bay button-->
                
                <!-- gift button--> 
				
                <div class="gift_btn" id="gift-<?php print $primaryDeal->id; ?>">
                    <div class="align_btn">
                        <?php if(0) { //if ($primaryDeal->tip == 'cena_na_vaucer') { ?>
                                <img alt="" src="/pub/img/layout/info_slika.png"> информација!
                        <?php } ?>

                        <?php if($primaryDeal->email_content_buy_switch == 0) { ?>
                                <img alt="" src="/pub/img/layout/poklon20x20.png"><?php print kohana::lang("prevod.Купи како подарок"); ?>
                        <?php } ?>
                    </div>
                </div>
				
                <!-- end gift button-->         
              

                <!-- Price -->
                <?php
                if ($primaryDeal->price > 0) {
                    ?>
                    <div class="deal_price">
                        <?php //print kohana::lang("prevod.Ред Цена"); ?>
                        <span style="text-decoration:line-through;">
                            <?php print $primaryDeal->price; ?>
                            <?php print kohana::lang("prevod.ден"); ?>
                        </span>
                    </div>
                    <!-- End Price -->
                    <?php
                }//if($primaryDeal->price > 0)
                ?>

                <div class="deal_buyers" >
                    <?php
                    $primaryDealsTooltipTxt = commonshow::tooltip($primaryDeal->voucher_count, $primaryDeal->min_ammount, $primaryDeal->max_ammount);
                    ?>
                    <span title= "<?php echo $primaryDealsTooltipTxt; ?>">
                        <?php
							if($primaryDeal->voucher_count > 0)
							{
								print commonshow::image($primaryDeal->voucher_count, $primaryDeal->max_ammount);
								//print '<strong' . (commonshow::isSoldOut($primaryDeal->voucher_count, $primaryDeal->max_ammount) ? " style='color: red' " : "") . '>' . commonshow::number($primaryDeal->voucher_count, $primaryDeal->min_ammount, $primaryDeal->max_ammount) . commonshow::text($primaryDeal->voucher_count) . '</strong>';
								print '<strong' . (commonshow::isSoldOut($primaryDeal->voucher_count, $primaryDeal->max_ammount) ? " style='color: red' " : "") . '>' . commonshow::number($primaryDeal->voucher_count, $primaryDeal->min_ammount, $primaryDeal->max_ammount) . '</strong>';
							}
                        ?>
                    </span>
                </div>


                <div class="deal_clock" >
                    <div class="<?php
                    if ($progress < 100)
                        print "green";
                    else
                        print "red";
                    ?>_clock_bilboard"></div>
                    <div class="time_bilboard_php<?= $count ?>"><?php if (!isset($singleOffer) || !$singleOffer) echo commonshow::staticCountDown(strtotime($primaryDeal->start_time), strtotime($primaryDeal->end_time)); ?></div>
                </div>
            </div>

        </div>

        <div class="deal_content">
            <?php print $primaryDeal->content_short_mk; ?>
        </div>

        <div class="deal_partner">
            <h3><?php print kohana::lang("prevod.podatoci_za_prodavacot"); ?></h3>
            <div class="deal_partner_info">
                <p>
                    <strong><partner_name><?php print $primaryDeal->name; ?></partner_name></strong><br>
                    <partner_adress><?php $langAdress = "adress_" . $lang ?></partner_adress>
                    <partner_data><?php print $primaryDeal->$langAdress; ?></partner_data>
                </p>
                <div class="img" style="margin-top: 15px">
                    <a href="<?php print $primaryDeal->google_url; ?>" target="_blank">
                        <img alt="" src="<?php print $primaryDeal->google_link; ?>" style="width: 100%"/>
                    </a>
                </div>
            </div>
            
            <h3><?php print kohana::lang("prevod.Ваучерот може да се искористи:"); ?></h3>
            <div class="deal_partner_info">
                <span style="font-size: 12px; font-weight: bold;">
                        <?php print kohana::lang("prevod.od"); ?>&nbsp;
                        <?php print date("d.m.Y", strtotime($primaryDeal -> valid_from)); ?>&nbsp;
                        <?php print kohana::lang("prevod.do"); ?>&nbsp;
                        <?php print date("d.m.Y", strtotime($primaryDeal -> valid_to)); ?>&nbsp;
                </span>
            </div>
            
            <h3><?php print kohana::lang("prevod.Услови за искористување на попустот"); ?></h3>
            <div class="deal_partner_info">
                <?php $langAdress = "terms_" . $lang ?>
                <?php print $primaryDeal -> $langAdress; ?>
                <p>
                        <span style="color:#ffa500;">• </span><?php print kohana::lang("index.Максимален број на ваучери по купувач:"); ?> <?php print $primaryDeal -> max_per_person; ?><br>
                        <span style="color:#ffa500;">• </span>
                        <?php if ($primaryDeal->max_ammount) { ?>
                        <?php print kohana::lang("index.На располагање има"); ?> <?php print $primaryDeal -> max_ammount; ?> <?php print kohana::lang("index.ваучери"); ?><br>
                        <?php } else { ?>
                        <?php print kohana::lang("index.На располагање има неограничен број на ваучери"); ?><br>
                        <?php } ?>
                </p>
            </div>
            
        </div>

        <div class="clear"></div>


    <?php }//foreach ($primaryDeals as $primaryDeal)        ?>



<script type="text/javascript">

<?php if (isset($singleOffer) && $action = "demo") { ?>

        $(document).ready(function () {
            $('.time_bilboard_php<?php echo $count ?>').html('Наскоро!');
        })

<?php } elseif (isset($singleOffer) && $timeNow < $timeStart) {
    ?>

        $(document).ready(function () {
            $('.time_bilboard_php<?php echo $count ?>').html('Старт на <?php print date("H:i d.m.Y", strtotime($primaryDeal->start_time)); ?>');
        })


    <?php
} elseif (isset($singleOffer) && $timeLeft >= 86400) {
    ?>
        $(document).ready(function () {
            $(".time_bilboard_php<?php echo $count ?>").countdown({
                alwaysExpire: 'true',
                format: 'DHMS',
                until: '+<?php print $timeLeft; ?>',
                layout: '<strong><?php print kohana::lang("prevod.Можете да купувате уште"); ?> {' + 'd<}{' + 'dn} {' + 'dl}{d>} и {h<}{hn} {hl} {h>}</strong>',
                expiryText: '<div class="redcount"><strong><?php print kohana::lang("prevod.Истечено"); ?><?php print date(" d.m.Y", strtotime($primaryDeal->end_time)); ?></strong></div>'});
        });
    <?php
} elseif (isset($singleOffer)) {
    ?>
        $(document).ready(function () {
            $(".time_bilboard_php<?php echo $count ?>").countdown({
                alwaysExpire: 'true',
                format: 'DHMS',
                until: '+<?php print $timeLeft; ?>',
                layout: '<?php print kohana::lang("prevod.Можете да купувате уште"); ?> <span class="redcount">{h<}<strong>{hn}</strong> {hl} {h>} {m<}<strong>{mn}</strong> {ml} {m>} {s<}<strong>{sn}</strong> {sl} {s>} </span>',
                expiryText: '<div class="redcount"><strong><?php print kohana::lang("prevod.Истечено"); ?><?php print date(" d.m.Y", strtotime($primaryDeal->end_time)); ?></strong></div>'});
        });
<?php } ?>

</script>

