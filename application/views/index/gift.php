<?php 
	$userID = $this->session->get("user_id"); 
	$form_action_additional = Kohana::config('config.payment_gateway');
?>
<script type="text/javascript" src="/pub/js/common_functions.js?1"></script>
<script type="text/javascript" src="/pub/js/pay.js?2"></script>

<style type="text/css">

    input[type=radio], input[type=checkbox] {
      /* All browsers except webkit*/
      transform: scale(2);

      /* Webkit browsers*/
      -webkit-transform: scale(2);
    }
</style>

<input type="hidden" name="invalidmail" id="invalidmail" value="<?php print kohana::lang("index.невалиден маил"); ?>"/>
<input type="hidden" name="defaulttext" id="defaulttext" value="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>"/>


<?php $langTitle = "title_" . $lang; ?>

<div class="row">
	<div class="col-md-4">
		<aside class="sidebar-left">
			<?php require_once APPPATH . 'views/layouts/contact.php'; ?>
			<?php require_once APPPATH . 'views/layouts/bezbednost.php'; ?>
			<?php require_once APPPATH . 'views/layouts/satisfaction.php'; ?>
			<div class="gap hidden-xs"></div>
		</aside>
	</div>
	
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-12 bg-white my-padd">

				<div class="row border-bottom mb10">
					<?php if (isset($error_status) and $error_status == 3) { ?>
							<div class="col-md-12">
								<span class="error_msg">Ве молиме внесете го Вашиот Е-mail!</span>
							</div>
							<div class="gap"></div>
					<?php }//if (isset($error_status) and $error_status == 3) { ?>
					
					<div class="col-md-4 ">
                        <?php if ($dealData->card) { ?>
                            <img alt="Deal Image" src="/pub/img/layout/gift-card-naslovna.png" class="img-responsive"/>
                        <?php } else { ?>
                            <img alt="Deal Image" src="/pub/deals/<?php print $dealData->side_img; ?>" class="img-responsive"/>
                        <?php } ?>
					</div>
					
					<div class="col-md-8">
						<p class="bold"> 
                            <?php if ($dealData->card) {
                                print "Подарувате “kupinapopust“ електронска картичка во вредност од ".strip_tags($dealData->$langTitle);
                            } else {
                                print strip_tags($dealData->title_mk_clean);
                            } ?>
						</p>
						
						<?php 
							//samo ako ne e lyoness korisnik
							if(!commonshow::isLyonessUser())
							{
								$points_osnova = calculate::points_osnova($dealData->price_discount, $dealData->price_voucher, $dealData->tip_danocna_sema, $dealData->ddv_stapka, $dealData->tip);
								if (floor($points_osnova) > 9) {  
							?>
									<p>
										<?php print kohana::lang("index.Со купување на овој ваучер добивате"); ?>&nbsp;
										<?php 
											$pointsCalculated = calculate::points($points_osnova, 1);
											print $pointsCalculated; 
										?>&nbsp;
										<?php $pointsCalculated == 1 ? print '"kupinapopust" поен.' : print kohana::lang("index.Kupinapopust поени") ; ?>
									</p>
						<?php 
								}//if (floor($points_osnova) > 9) {  
							}//if(!commonshow::isLyonessUser())
						?>

						<?php 
							$opisNaUslugaTxt = "";

							if(count($categoryData) > 0)
								$opisNaUslugaTxt = $categoryData[0]->service_desc;
							
							if($dealData->card) 
									$opisNaUslugaTxt = "Подарок kupinapopust електронска картичка";

							if($opisNaUslugaTxt != "") { 
						?>
								<p><strong>Опис на услуга:</strong> <?php print $opisNaUslugaTxt; ?></p>
						<?php } //if(count($categoryData) > 0) { ?>
						
					</div>
				</div>

				<form  name="payForm" id="paymentform" class="form-horizontal" method="post" action="/pay<?php echo $form_action_additional; ?>/index">
				
					
					
                        <input type="hidden" id="discount_id" name="discount_id" value="<?php print $dealData->deal_id ?>"/>
                        <input type="hidden" id="deal_option_id" name="deal_option_id" value="<?php print $dealData->deal_option_id; ?>" />
                        <input type="hidden" id="gift" name="gift" value="1"/>
                        <input type="hidden" id="AmountToPay" name="AmountToPay" value="<?php print ($dealData->tip == 'cena_na_vaucer' ? $dealData->price_voucher : $dealData->price_discount); ?>"/>
                        <input type="hidden" id="AmountCurrency" name="AmountCurrency" value="MKD"/>
                        <input type="hidden" id="Details1" name="Details1" value="<?php print strip_tags($dealData->$langTitle) ?>"/>
                        <input type="hidden" id="Details2" name="Details2" value="<?php print $dealData->deal_id ?>"/>
						
					<div class="row mb10">
						<div class="col-md-4 text-right"><?php print kohana::lang("index.Количина"); ?><!-- Количина --></div>
						<div class="col-md-8">
							<select id="amount" name="amount" onfocus="this.onmousewheel=function(){return false}" class="form-control">
								<?php
									if (!$dealData->max_per_person) {
										$dealData->max_per_person = 50;
									}
									
									$currentMaxPerPerson = $dealData->max_per_person;
									
									if ($dealData->max_ammount && (($dealData->max_ammount - $soldVoucherCount) < $dealData->max_per_person))
										$currentMaxPerPerson = ($dealData->max_ammount - $soldVoucherCount);
									
									for ($index = 1; $index <= $currentMaxPerPerson; $index++) {
										print "<option value='$index'>$index</option>";
									}
								?>
							</select>
						</div>
					</div>

					<div class="row mb10  border-bottom form-group">
						<div class="col-md-4 text-right"><?php print kohana::lang("index.Вашиот Е-mail"); ?><!-- Вашиот емаил --></div>
						<div class="col-md-8">
							<input type="text" value="" id="Email" name="Email" class="form-control" placeholder="e-mail" />
							<?php
								if ($dealData->card) {
									echo "На оваа E-mail адреса ќе биде испратена “Подарок електронската картичка“";
								} else {
									echo kohana::lang("index.На оваа e-mail адреса ќе биде испратен вашиот ваучер");
								}
							?>
						</div>
					</div>
					
					<div class="row mb10 form-group">
						<div class="col-md-4 text-right"><?php print kohana::lang("index.Е-mail на пријателот"); ?></div>
						<div class="col-md-8 small-orange">
							<input type="text" value="" id="email_friend" name="email_friend" class="form-control" placeholder="e-mail" />
							<?php
								if ($dealData->card) {
									echo "На оваа E-mail адреса ќе биде испратена “Подарок електронската картичка“";
								} else {
									echo kohana::lang("index.На оваа адреса ќе биде испратен подарениот ваучер");
								}
							?>
						</div>
					</div>
					
					<div class="row mb10  border-bottom">
						<div class="col-md-4 text-right">Посвета</div>
						<div class="col-md-8 small-orange">
							<textarea value="" id="posveta" name="posveta" class="form-control"></textarea>
                                 <?php if ($dealData->card) { ?>
                                    Овде напишете ја вашата посвета
                                <?php } else { ?>
                                    <?php print kohana::lang("index.Овде напишете ја посветата за примачот на ваучерот"); ?>
                                <?php } ?>
						</div>
					</div>

					<div class="row mb10 border-bottom">
						<div class="col-md-4 text-right"><?php print kohana::lang("index.Плаќање со:"); ?><!-- Плаќање со: --></div>
						<div class="col-md-8">
							<!-- Платежна картичка -->
							<div style="float: left; margin-top: -4px;">
								<input type="radio" value="card" id="payment-0" name="payment" class="radio plakjanje_so" checked="checked" />
							</div>
							<label for="payment-0">&nbsp;&nbsp;&nbsp;<?php print kohana::lang("index.Платежна картичка"); ?></label>

							<ul class="platezni-karticki-list-inline list-payment mt10">
								<li>
									<img src="/pub/img/payment/maestro-curved-32px.png" alt="Image Alternative text" title="Image Title" />
								</li>
								<li>
									<img src="/pub/img/payment/mastercard-curved-32px.png" alt="Image Alternative text" title="Image Title" />
								</li>
								<li>
									<img src="/pub/img/payment/visa-curved-32px.png" alt="Image Alternative text" title="Image Title" />
								</li>
								<li>
									<img src="/pub/img/payment/visa-electron-curved-32px.png" alt="Image Alternative text" title="Image Title" />
								</li>
								<li>
									<a target="_blank" href="http://www.diners.com.mk/page.asp?gID=1&sID=1 ">
										<img src="/pub/img/payment/diners-logo.jpg" alt="Image Alternative text" title="Image Title" />
									</a>
								</li>
							</ul>
							<p><?php print kohana::lang("index.Ваучерот се добива веднаш"); ?><!-- Ваучерот се добива веднаш --></p>

							<?php if ($userID == '297') {  ?>
								
									<div class="gap-small"></div>
									<!-- Плаќање во готово -->
									<div style="float: left; margin-top: -4px;">
										<input type="radio" value="cache" id="payment-1" name="payment" class="radio plakjanje_so">
									</div>
									<label for="payment-1">&nbsp;&nbsp;&nbsp;<?php print kohana::lang("index.Плаќање во готово"); ?></label>

									
									<div class="gap-small"></div>
									<!-- Плаќање со уплатница -->
									<div style="float: left; margin-top: -4px;">
										<input type="radio" value="bank" id="payment-2" name="payment" class="radio plakjanje_so">
									</div>
									<label for="payment-2">&nbsp;&nbsp;&nbsp;<?php print kohana::lang("index.Плаќање со уплатница"); ?></label>


									<p>
										<span class="payment-help" style="width: 275px; float:left">
											<?php print kohana::lang("index.Плаќање со уплатница2"); ?>
										</span>
									</p>

							<?php } ?>


						</div>
					</div>
					
					<?php
						//dokolku ima poeni i NE E lyoness korisnik
						if ($customerPoints && !commonshow::isLyonessUser()) {
					?>
							<div class="row mb10  border-bottom">
								<div class="col-md-4 text-right">Поени:</div>
								<div class="col-md-8">
									<select id="points" name="points_used" onfocus="this.onmousewheel=function(){return false}" class="form-control">
										<?php
											print "<option value='0'>0 поени</option>";
											$max = (int)($customerPoints / 10);
											if ($max) {
												for ($index = 1; $index <= $max; $index++) {
													$value = $index * 10;
													print "<option value='$value'>$value поени = $value денари</option>";
												}
											}
										?>
									</select>
								</div>
							</div>
					<?php
						}
					?>

					<!-- KUPUVANJE NA RATI -->
					<div class="row mb10  border-bottom" id="installment_row_id">
						<div class="col-md-4 text-right">На рати:</div>
						<div class="col-md-8">
							<select id="installment" name="installment" onfocus="this.onmousewheel=function(){return false}" class="form-control">
								<option value="1">Еднократно</option>
							</select>
							<a class="prevzemiVaucher togle-vaucher"><i class="fa fa-plus-square"></i> Појаснување за плаќање на рати</a>
							<div class="togle" style="display: none;">
								<strong>
									Услови за користење на плаќањето на рати: <br/>
									1. За да платите на рати вкупната сума мора да биде поголема од 1999 денари.  <br/>
									2. За сума од 2000 ден. до 6000 ден. може да платите најмногу на 2 ,4 или 6 рати <br/>
									3. За сума над 6000 ден. може да платите на 2, 4, 6, 10 или најмногу 12 рати без камата. <br/>
									4. Плаќањето на рати е можно само со кредитни картички на <font color="#ff00ff">Стопанска банка А.Д. Скопје</font> ( Visa , Master card i Visa Vero )
								</strong>
							</div>
						</div>
					</div>

					<!-- PRIKAZI ZAPOMNETI PLATEZNI KARTICKI -->
					<div class="row mb10  border-bottom" id="show_saved_cc_row_id">
						<div class="col-md-4 text-right">Одбери платежна картичка:</div>
						<div class="col-md-8">
							<select id="saved_cc" name="saved_cc" onfocus="this.onmousewheel=function(){return false}" class="form-control">
								<option value="0">Нова платежна картичка</option>
							</select>
							<a class="prevzemiVaucher togle-vaucher"><i class="fa fa-plus-square"></i> Прикажи појаснување</a>
							<div class="togle" style="display: none;">
								<strong>
									Тука се прикажуваат платежните картички кои во минатото сте избрале да бидат запомнати од системот. <br/>
									Одберете "Нова платежна картичка" доколку користите платежна картичка која не е запомната од системот.
								</strong>
							</div>
						</div>
					</div>

					<!-- ZAPOMNUVANJE NA PLATEZNA KARTICKA -->
					<div class="row mb10  border-bottom" id="card_on_file_row_id">
						<div class="col-md-4 text-right">Запомни ја картичката:</div>
						<div class="col-md-8">
							<select id="save_credit_card" name="save_credit_card" onfocus="this.onmousewheel=function(){return false}" class="form-control">
								<option value="0">Не</option>
								<option value="1">Да</option>
							</select>
							<a class="prevzemiVaucher togle-vaucher"><i class="fa fa-plus-square"></i> Прикажи појаснување</a>
							<div class="togle" style="display: none;">
								<strong>
									Доколку одберете да се запомни платежната картичка информациите што ќе ги внесете на следниот екран ќе бидат запомнати и автоматски ќе се пополнат при вашето следно купување. <br/>
									Заради ваша безбедност не се запомнува CVV бројот и секогаш ќе треба да го внесувате.
								</strong>
							</div>
						</div>
					</div>

					<!-- OTKUP - DOPOLNITELNI INFORMACII -->
					<?php if ($dealData->proizvod_flag && $dealData->finalna_cena > 0) {  ?>

						<div class="row mb10 border-bottom form-group" id="otkup_prevzemanje_div_id" >
							<div class="col-md-4 text-right">Превземање:</div>
							<div class="col-md-8">
								
								<!-- LICNO PREVZEMANJE-->
								<?php if(in_array($dealData->plakjanje_pri_prezemanje_nacin_podiganje, array(0, 1))) { ?>
									<div style="float: left; margin-top: -4px;">
										<input type="radio" value="1" id="prevzemanje_licno" name="otkup_prevzemanje" class="radio prezemanje_class" checked="checked" />
									</div>
									<label for="prevzemanje_licno">&nbsp;&nbsp;&nbsp;Производот ќе го подигнам од канцелариите на kupinapopust</label>
									<p>
										Адреса: Ул.Никола Вапцаров бр.3/1, Центар, реон 8, 1000, Скопје
										&nbsp;&nbsp;
										<a href="https://www.google.mk/maps/dir/''/%D0%BA%D1%83%D0%BF%D0%B8%D0%BD%D0%B0%D0%BF%D0%BE%D0%BF%D1%83%D1%81%D1%82/@41.9940207,21.3592541,12z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x135415b518ea92a3:0x2c0dfc5432433271!2m2!1d21.429294!2d41.994042?hl=mk" target="_blank">Покажи ми го патот</a>
										<br/>
										моб. и viber 078 439 829<br/>
										<!--
										(
										 достапни секој работен ден од 09:00 до 17:00 часот 
										-->
											<strong class="error_msg">Почитувани, <br/>
											Заради Ковид 19 ситуацијата подигнување на производите од канцелариите на kupinapopust е можно само во Четврток, секоја седмица, помеѓу 12 и 18 часот по претходна потврда од наша страна дека нарачката е подготвена за подигнување.
											</strong>
										<!-- 
										)  
										-->
									</p>
								<?php } //if(in_array($dealData->plakjanje_pri_prezemanje_nacin_podiganje, array(0, 1))) { ?>
								<!-- END - LICNO PREVZEMANJE-->

								<?php if($dealData->plakjanje_pri_prezemanje_nacin_podiganje == 0) { ?>
									<div class="gap-small"></div>
								<?php } //if(in_array($dealData->plakjanje_pri_prezemanje_nacin_podiganje, array(0))) { ?>

								<!-- DOSTAVA DO DOMA-->
								<?php if(in_array($dealData->plakjanje_pri_prezemanje_nacin_podiganje, array(0, 2))) { ?>
									<div style="float: left; margin-top: -4px;">
										<input type="radio" value="2" id="prevzemanje_dostava" name="otkup_prevzemanje" class="radio prezemanje_class" />
									</div>
									<label for="prevzemanje_dostava">
										&nbsp;&nbsp;&nbsp;
										<?php if(in_array($dealData->plakjanje_pri_prezemanje_nacin_podiganje, array(0))) { ?>
												Сакам достава<br/>
												&nbsp;&nbsp;&nbsp;
												(Ве молиме пополнете ги потребните информации подолу)
										<?php }else{ ?>	
												Cамо со достава до вашата наведена адреса. <br/>
												&nbsp;&nbsp;&nbsp;
												Ве молиме пополнете ги потребните информации подолу:
										<?php } ?>		
									</label>
									<p>
										<span id="besplatna_dostava_tekst_info" style="display: none;">
											<span style="font-family: helvetica, arial, Roboto, sans-serif; font-size: 14px; font-style: normal; font-weight: normal; background-color: rgb(255, 255, 255); color: rgb(255, 156, 0); text-indent: -24px;">•</span><span style="color: rgb(51, 51, 51); font-family: helvetica, arial, Roboto, sans-serif; font-size: 14px; font-style: normal; font-weight: 400; background-color: rgb(255, 255, 255); text-indent: -24px;">&nbsp;</span><span style="font-weight: bold;">Доставата за овој производ е бесплатна!</span>
										</span>													
										<span id="dostava_tekst_info" style="display: none;">
											<span style="font-family: helvetica, arial, Roboto, sans-serif; font-size: 14px; font-style: normal; font-weight: normal; background-color: rgb(255, 255, 255); color: rgb(255, 156, 0); text-indent: -24px;">•</span><span style="color: rgb(51, 51, 51); font-family: helvetica, arial, Roboto, sans-serif; font-size: 14px; font-style: normal; font-weight: 400; background-color: rgb(255, 255, 255); text-indent: -24px;">&nbsp;</span><span style="font-weight: bold;">Доставата се плаќа директно на курирот при превземање на производот.</span><br>		

											<span style="font-family: helvetica, arial, Roboto, sans-serif; font-size: 14px; font-style: normal; font-weight: normal; background-color: rgb(255, 255, 255); color: rgb(255, 156, 0); text-indent: -24px;">•</span><span style="color: rgb(51, 51, 51); font-family: helvetica, arial, Roboto, sans-serif; font-size: 14px; font-style: normal; font-weight: 400; background-color: rgb(255, 255, 255); text-indent: -24px;">&nbsp;</span><span style="font-weight: bold;">Доставата за Скопје за пратки до 1кг чини&nbsp;<span style="color: rgb(255, 156, 0);">само&nbsp;99 ден.</span></span><br>

											<span style="font-family: helvetica, arial, Roboto, sans-serif; font-size: 14px; font-style: normal; font-weight: normal; background-color: rgb(255, 255, 255); color: rgb(255, 156, 0); text-indent: -24px;">•</span><span style="color: rgb(51, 51, 51); font-family: helvetica, arial, Roboto, sans-serif; font-size: 14px; font-style: normal; font-weight: 400; background-color: rgb(255, 255, 255); text-indent: -24px;">&nbsp;</span><span style="font-weight: bold;">За другите градови за пратки до 1кг -&nbsp;<span style="color: rgb(255, 156, 0);">130 ден.</span></span><br>

											<span style="font-family: helvetica, arial, Roboto, sans-serif; font-style: normal; font-weight: 400; background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(255, 156, 0);">•</span><span style="font-family: helvetica, arial, Roboto, sans-serif; font-style: normal; font-weight: 400; background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(51, 51, 51);">&nbsp;</span><span style="font-weight: bold;">Пратките пристигаат за 1 до 2 дена за Скопје и 2 до 3 дена за низ републиката.</span><span style="font-family: helvetica, arial, Roboto, sans-serif; font-size: 14px; font-style: normal; font-weight: 700;"><br></span><span style="font-family: helvetica, arial, Roboto, sans-serif; font-style: normal; font-weight: 400; background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(255, 156, 0);">•</span><span style="font-family: helvetica, arial, Roboto, sans-serif; font-style: normal; font-weight: 400; background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(255, 0, 0);">&nbsp;</span><span style="font-weight: bold;"><span style="color: rgb(255, 0, 0);">Напомена:&nbsp;</span>За достава низ цела република за приградски населби, индустриски зони и села со оддалеченост до 5 км од регионален пат со пристапен пат за достава + 50 ден од цената за достава.</span>
										</span>
									</p>
								<?php } //if(in_array($dealData->plakjanje_pri_prezemanje_nacin_podiganje, array(0, 2))) { ?>
								<!-- END - DOSTAVA DO DOMA-->

							</div>
						</div>

						<div class="row mb10  border-bottom form-group" id="otkup_ime_prezime_div_id" >
							<div class="col-md-4 text-right">Име и Презиме</div>
							<div class="col-md-8">
								<input type="text" value="" id="otkup_ime_prezime" name="otkup_ime_prezime" class="form-control" placeholder="Име и Презиме" />
							</div>
						</div>

						<div class="row mb10  border-bottom form-group" id="otkup_adresa_div_id" >
							<div class="col-md-4 text-right">Точна адреса со кратко појаснување</div>
							<div class="col-md-8">
								<textarea class="form-control" id="otkup_adresa" name="otkup_adresa" placeholder="Точна адреса со кратко појаснување"></textarea>
							</div>
						</div>

						<div class="row mb10  border-bottom form-group" id="otkup_telefon_div_id" >
							<div class="col-md-4 text-right">Телефон</div>
							<div class="col-md-8">
								<input type="text" value="" id="otkup_telefon" name="otkup_telefon" class="form-control" placeholder="Телефон" />
							</div>
						</div>

					<?php } ?>

					<div class="row mb10  border-bottom">
						<div class="col-md-4 text-right"> 
							<?php print kohana::lang("prevod.Цена"); ?>: 
							<strong><span id="dealprice">
										<?php print $dealData->finalna_cena; ?>
									</span> 
									<?php print kohana::lang("prevod.ден"); ?>
							</strong>

							<br />
						</div>
						<div class="col-md-8">
							
							<?php if(time() > strtotime($dealData->end_time)) { ?>

									<a id="a-link-kupi-placeholder" href ="#" class="btn btn-lg btn-block btn-danger" onclick="return false;" oncontextmenu="return false;">
										<i class="fa fa-shopping-cart"></i> <?php print kohana::lang("prevod.Истечено"); ?>
									</a>

							<?php }else { ?>

										<input type="submit" id="submitted" value="<?php print ($dealData->finalna_cena > 0 ? "Продолжете кон плаќање" : "Превземи купон"); ?>" class="btn btn-lg green" />
							<?php } ?>

							<span class="txt16px">
								<?php if ($userID == 0 && !commonshow::isLyonessUser()) { ?>
									<p class="mt10">За искористување на вашите поени ве молиме кликнете <b><a href="/customer/login/gift_<?php print $dealData->deal_id; ?>_<?php print $dealData->deal_option_id; ?>">тука</a></b> за да се логирате.</p>
								<?php } ?>
								<?php if ($userID > 0 && !commonshow::isLyonessUser() && $this->session->get("user_type") == 'customer' && $customerPoints < 10) { ?>
									<p class="mt10">Искористувањето на вашите поени е можно ако имате освоено минимум 10 поена (10 ден.)</p>
								<?php } ?>
								<?php if (commonshow::isLyonessUser()) { ?>
									<p class="mt10">Lyoness корисниците не можат да ги користат поените при купување затоа што како Lyoness корисник добивате други поволности на нашиот веб сајт..</p>
	                            <?php } ?>
	                        </span>
						</div>
					</div>
	
				</form>
				
				<div style="text-align: center;" class="row mb30">
					<ul class="list-inline list-payment">
						<li style="padding-right: 0px;">
							<img class="img_top_banner" src="/pub/img/Halkbank.png">
						</li>
						<li style="padding-left: 0px;">
							<a target="_blank" href="http://www.diners.com.mk/page.asp?gID=1&sID=1 ">
								<img class="img_top_banner" src="/pub/img/payment/diners-logo-text.jpg" alt="Diners logo" title="Diners logo" />
							</a>
						</li>
					</ul>
				</div>


				<div class="row mb10  border-bottom">
					<div class="col-md-12">
						<?php require_once APPPATH . 'views/card/uplatnica.php'; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="gap"></div>
	</div>
</div>

<script type="text/javascript">
	<?php  
		  if (isset($user_login) and $user_login == 1)  { 
	?>
	        $(document).ready(function () {
				$().toastmessage('showToast', {
					text     : "Успешно се логиравте.",
					sticky   : false,
					position : 'middle-center',
					type     : 'success'
				});
	        }) 
	<?php } ?>

	<?php if (isset($error_status) and $error_status == 1) { ?>
        $(document).ready(function () {
			$().toastmessage('showToast', {
				text     : "Почитувани, бидејќи во меѓувреме друг посетител има купено од понудата и со тоа е надминато ограничувањето. Ве молиме изберете помала количина.",
				sticky   : false,
				position : 'middle-center',
				type     : 'error'
			});
        })     
	<?php } ?>    

	<?php if (isset($error_status) and $error_status == 2) { ?>
		        $(document).ready(function () {
					$().toastmessage('showToast', {
						text     : "Почитувани, го имате надминато ограничувањето за купени ваучери по корисник. Ве молиме изберете помала количина.",
						sticky   : false,
						position : 'middle-center',
						type     : 'error'
					});
		        })   
	<?php } ?> 


$(document).ready(function() {

     $('#paymentform').bootstrapValidator({
//        live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {

            Email: {
                validators: {
					notEmpty: {
						message: 'Полето за Email адреса мора да биде пополнето'
						  }, // notEmpty
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'Email адресата не е валидна.'
                    }
                }
            },
            email_friend: {
                validators: {
					notEmpty: {
						message: 'Полето за Email адреса на пријателот мора да биде пополнето'
						  }, // notEmpty
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'Е-mail адресата на пријателот не е валидна.'
                    }
                }
            }
        }
    });

    //dinamicko ureduvanje na formata zavisno sto odbral za nacin na plakjanje
    $(".plakjanje_so").click(function (){

    	//DEFAULT
    	buttonDefaultValue = "<?php print ($dealData->finalna_cena > 0 ? "Продолжете кон плаќање" : "Превземи купон"); ?>";
    	buttonOtkupValue = "Ја потврдувам нарачката";

		$("#installment_row_id").hide();
		$("#show_saved_cc_row_id").hide();
		$("#card_on_file_row_id").hide();

		$("#besplatna_dostava_tekst_info").hide();
		$("#dostava_tekst_info").hide();

		// Remove fields from validation
		$('#paymentform')
        .bootstrapValidator('removeField', 'otkup_ime_prezime')
        .bootstrapValidator('removeField', 'otkup_adresa')
        .bootstrapValidator('removeField', 'otkup_telefon');

        $("#submitted").prop('value', buttonDefaultValue);

        $('#paymentform')
            // Add field
            .bootstrapValidator('addField', 'otkup_ime_prezime', {
                validators: {
                    notEmpty: {
                        message: 'Внесете Име и Презиме'
                    }
                }
            })
            .bootstrapValidator('addField', 'otkup_adresa', {
                validators: {
                    notEmpty: {
                        message: 'Внесете Точна адреса со кратко појаснување'
                    }
                }
            })
            .bootstrapValidator('addField', 'otkup_telefon', {
                validators: {
                    notEmpty: {
                        message: 'Внесете Телефон'
                    },
                    regexp: {
                    	regexp: /^07\d{7}$/,
                        message: 'Телефонот може да има само бројки во формат 07XXXXXXX'
                    }
                }
            });

		//AKO ODBRAL NESTO
		if(this.id == 'payment-0') //KARTICKA
		{
    		$("#installment_row_id").show();
    		$("#show_saved_cc_row_id").show();
    		$("#card_on_file_row_id").show();

    		<?php if($dealData->proizvod_besplatna_dostava) { ?>
    					$("#besplatna_dostava_tekst_info").show();
    		<?php } else { ?>	
    					$("#dostava_tekst_info").show();
    		<?php } ?>	
		}
		else if(this.id == 'payment-1') //ako odbral plakjanje vo gotovo
    	{
    		<?php if($dealData->proizvod_besplatna_dostava) { ?>
    					$("#besplatna_dostava_tekst_info").show();
    		<?php } else { ?>	
    					$("#dostava_tekst_info").show();
    		<?php } ?>	
    	}
		else if(this.id == 'payment-2') //ako odbral plakjanje so uplatnica
    	{
    		<?php if($dealData->proizvod_besplatna_dostava) { ?>
    					$("#besplatna_dostava_tekst_info").show();
    		<?php } else { ?>	
    					$("#dostava_tekst_info").show();
    		<?php } ?>	
    	}
		else if(this.id == 'payment-3') //ako odbral OTKUP
    	{
    		$("#submitted").prop('value', buttonOtkupValue);

    		<?php if($dealData->proizvod_besplatna_dostava_otkup) { ?>
    					$("#besplatna_dostava_tekst_info").show();
    		<?php } else { ?>	
    					$("#dostava_tekst_info").show();
    		<?php } ?>	
    	}

    });

    //KAKO KE GO PREZEME PROIZVODOT
	$(".prezemanje_class").click(function (){

		$("#otkup_adresa_div_id").hide();

		if(this.id == 'prevzemanje_dostava') //DOSTAVA
			$("#otkup_adresa_div_id").show();
	});

	//PROGRAMSKI SE KLIKA DEFAULT-NIOT RADIO BUTTON ZA PLAKJANJE SO PLATEZNA KARTICKA
	$("#payment-0").click();
	
	//da se klikne prviot vidliv radio button za prezemanje
	$(".prezemanje_class:visible:first").click();

});

</script>
