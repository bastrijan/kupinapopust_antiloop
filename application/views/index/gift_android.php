<style>
    div.form_item{
        padding-top: 6px;
        padding-bottom: 6px;
        margin-bottom: 10px;
        margin-top: 10px;
        /*background-color: #fff;*/
    }
    
    div.caption p{
        margin: 0;
    }
    
      span#dealprice{
        color: #48b548;
        font-size: 22px;
    }
</style>

<?php $userID = $this->session->get("user_id"); ?>
<script type="text/javascript" src="/pub/js/pay_android.js"></script>

<div data-role="popup" id="invalidmailPopup" class="ui-content">
    <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
    <?php print kohana::lang("index.невалиден маил"); ?>
</div>

<input type="hidden" name="invalidmail" id="invalidmail" value="<?php print kohana::lang("index.невалиден маил"); ?>"/>
<input type="hidden" name="defaulttext" id="defaulttext" value="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>"/>


<script type="text/javascript">
<?php if (isset($user_login) and $user_login == 1) { ?>
        $(document).ready(function () {
            $.noticeAdd({
                text: "Успешно се логиравте.",
                stay: false,
                type: 'notification-success'
                        //                stayTime: 3000
            });
        })
<?php } ?>

<?php if (isset($vaucer_nadminat_limit) and $vaucer_nadminat_limit == 1) { ?>
        $(document).ready(function () {
            $.noticeAdd({
                text: "Почитувани, бидејќи во меѓувреме друг посетител има купено од понудата и со тоа е надминато ограничувањето. Ве молиме изберете помала количина.",
                stay: false,
                type: 'notification-error',
                stayTime: 20000
            });
        })
<?php } ?>

<?php if (isset($vaucer_nadminat_limit) and $vaucer_nadminat_limit == 2) { ?>
        $(document).ready(function () {
            $.noticeAdd({
                text: "Почитувани, го имате надминато ограничувањето за купени ваучери по корисник. Ве молиме изберете помала количина.",
                stay: false,
                type: 'notification-error',
                stayTime: 20000
            });
        })
<?php } ?>
</script>

<div class="homepage-content">
    <div class="layout-gift">

        <input type="hidden" id="priceDiscount" name="priceDiscount" value="<?php print $dealData->price_discount; ?>"/>
        <input type="hidden" id="priceVoucher" name="priceVoucher" value="<?php print $dealData->price_voucher; ?>"/>
        <input type="hidden" id="tipDanocnaSema" name="tipDanocnaSema" value="<?php print $dealData->tip_danocna_sema; ?>"/>
        <input type="hidden" id="ddvStapka" name="ddvStapka" value="<?php print $dealData->ddv_stapka; ?>"/>
        <input type="hidden" id="tipPonuda" name="tipPonuda" value="<?php print $dealData->tip; ?>"/>
        <?php
        $form_action_additional = Kohana::config('config.payment_gateway');
        $test_casys_get_variable = $this->input->get("test_casys", "");
        if ($test_casys_get_variable != "")
            $form_action_additional = $test_casys_get_variable;
        ?>
        <form  name="payForm" id="paymentform" method="post" action="/pay<?php echo $form_action_additional."_mobile"; ?>/index">
			<input type="hidden" id="app_type" name="app_type" value="Android"/>
            <?php $langTitle = "title_" . $lang ?>

            <!--<h1><?php //print $dealData->$langTitle;       ?></h1>-->


            <div class="subpage-content">
                <div class="section-container">

                    <div class="caption">
                        <div><?php print kohana::lang("index.Ваучер"); ?></div>
                        <div class="clear"></div>
                        <?php if ($dealData->card) { ?>
                            <img style="width:110px;margin-right: 5px;" src="/pub/img/layout/podarok_karticka.png" align="left">
                        <?php } else { ?>
                            <img style="width:110px;margin-right: 5px;" src="/pub/deals/<?php print $dealData->side_img ?>" align="left">
                        <?php } ?>

                        <?php
                        if ($dealData->card) {
                            print "<p>Подарувате “kupinapopust“ електронска картичка во вредност од " . strip_tags($dealData->$langTitle) . "</p>";
                        } else {
                            print $dealData->title_mk_clean;
                        }
                        ?>

                        <?php if (!$dealData->card) { ?>
                            <p> <?php print kohana::lang("index.Град"); ?>
                                <?php print kohana::lang("index.Скопје"); ?> </p>
                        <?php } ?>
                    </div>

                    <div class="section-form">

                        <input type="hidden" id="discount_id" name="discount_id" value="<?php print $dealData->id ?>"/>
                        <input type="hidden" id="gift" name="gift" value="1"/>
                        <input type="hidden" id="AmountToPay" name="AmountToPay" value="<?php print ($dealData->tip == 'cena_na_vaucer' ? $dealData->price_voucher : $dealData->price_discount); ?>"/>
                        <input type="hidden" id="AmountCurrency" name="AmountCurrency" value="MKD"/>
                        <input type="hidden" id="Details1" name="Details1" value="<?php print strip_tags($dealData->$langTitle) ?>"/>
                        <input type="hidden" id="Details2" name="Details2" value="<?php print $dealData->id ?>"/>
                                    
                        <div class="clear"></div>
                        
                        <div class="form_item">
                            <label for="amount"><?php print kohana::lang("index.Количина"); ?></label>
                            <select id="amount" name="amount" onfocus="this.onmousewheel = function () {return false}">
                            <?php
                            if (!$dealData->max_per_person) {
                                $dealData->max_per_person = 50;
                            }

                            $currentMaxPerPerson = $dealData->max_per_person;

                            if ($dealData->max_ammount && (($dealData->max_ammount - $soldVoucherCount) < $dealData->max_per_person))
                                $currentMaxPerPerson = ($dealData->max_ammount - $soldVoucherCount);


                            for ($index = 1; $index <= $currentMaxPerPerson; $index++) {
                                print "<option value='$index'>$index</option>";
                            }
                            ?>
                            </select>
                        </div>
                        
                        <div class="form_item">
                            <input type="text" value="" id="Email" name="Email" placeholder="<?php print kohana::lang("index.Вашиот Е-mail"); ?>" class="">
                            <label for="Email">
                                <?php if ($dealData->card) { ?>
                                    На оваа E-mail адреса ќе биде испратена “Подарок електронската картичка“
                                <?php } else { ?>
                                    <?php print kohana::lang("index.На оваа e-mail адреса ќе биде испратен вашиот ваучер"); ?>
                                <?php } ?>
                            </label>
                        </div>
                        
                        
                        <div class="form_item">
                            <input type="text" value="" id="email_friend" name="email_friend" placeholder="<?php print kohana::lang("index.Е-mail на пријателот"); ?>" class="text autofocus text">
                            <label for="email_friend" style="color: #F60;">
                                <?php if ($dealData->card) { ?>
                                    На оваа E-mail адреса ќе биде испратена “Подарок електронската картичка“
                                <?php } else { ?>
                                    <?php print kohana::lang("index.На оваа адреса ќе биде испратен подарениот ваучер"); ?>
                                <?php } ?>
                            </label>
                        </div>
                        
                        <div class="form_item">
                            <textarea value="" id="posveta" name="posveta" placeholder="<?php print kohana::lang("index.Посвета"); ?>" class=""></textarea>
                            <label for="posveta" style="color: #F60;">
                                <?php if ($dealData->card) { ?>
                                    Овде напишете ја вашата посвета
                                <?php } else { ?>
                                    <?php print kohana::lang("index.Овде напишете ја посветата за примачот на ваучерот"); ?>
                                <?php } ?>
                            </label>
                        </div>
                        
                        <div class="form_item">
                            <label><?php print kohana::lang("index.Плаќање со:"); ?></label>

                            <span class="payment-title">
                                <input type="radio" value="card" id="payment-0" name="payment" class="radio" checked="checked">
                                <label for="payment-0"><?php print kohana::lang("index.Платежна картичка"); ?></label>
                            </span>

                            <span class="payment-help">
                                <img src="/pub/img/layout/maestro.png" style="height: 30px"/>
                                <img src="/pub/img/layout/mastercard.png" style="height: 30px"/>
                                <img src="/pub/img/layout/visa.png" style="height: 30px"/>
                                <img src="/pub/img/layout/visa_electron.png" style="height: 30px"/>
                            </span>

                            <span class="payment-help">
                                <br/>
                                <span class="icon-card"><?php print kohana::lang("index.Ваучерот се добива веднаш"); ?></span>
                            </span>
                        </div>
                        
                        <?php if ($userID == '297') { //deactivated temporarily  ?>
                        <div class="form_item">
                            <span class="payment-title">
                                <input type="radio" value="cache" id="payment-1" name="payment" class="radio">
                            </span>
                            <span class="payment-title" style="width: 165px">
                                <label for="frmpayForm-payment-1"><?php print kohana::lang("index.Плаќање во готово"); ?></label>
                            </span>
                            <span class="payment-help" style="width: 225px; float:left">
                                <?php print "&nbsp;"; //kohana::lang("index.Плаќање во готово2");  ?>
                            </span>
                        </div>    
                        <?php } ?>

                        <?php if ($userID == '297') { //the id for jovica.belovski@gmail.com  ?>
                        <div class="form_item">
                            <input type="radio" value="bank" id="payment-2" name="payment" class="radio">
                            <label for="payment-2"><?php print kohana::lang("index.Плаќање со уплатница"); ?></label>

                            <span class="payment-help">
                            <?php print kohana::lang("index.Плаќање со уплатница2"); ?>
                            </span>
                        </div>    
                        <?php } ?>
                        
                        <?php if ($customerPoints) { ?>
                        <div class="form_item">
                            <label for="points">Поени:</label>
                            <select id="points" name="points_used" onfocus="this.onmousewheel = function () {return false}">
                                <?php
                                print "<option value='0'>0 поени</option>";
                                $max = (int) ($customerPoints / 10);
                                if ($max) {
                                    for ($index = 1; $index <= $max; $index++) {
                                        $value = $index * 10;
                                        print "<option value='$value'>$value поени = $value денари</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <?php } ?>
                        
                        <div class="form_item">
                            <input type="submit" value="<?php print kohana::lang("prevod.Купи"); ?>" class="submit" id="pay">
                        </div>
                        
                        <div class="form_item">
                            <span class="help"><?php print kohana::lang("prevod.Цена"); ?>: 
                                <strong>
                                    <span id="dealprice">
                                        <?php print ($dealData->tip == 'cena_na_vaucer' ? $dealData->price_voucher : $dealData->price_discount); ?>
                                        <?php print kohana::lang("prevod.ден"); ?>
                                    </span>
                                </strong>
                            </span>

                            <?php if ($userID == 0) { ?>
                                <span>
                                    За искористување на вашите поени ве молиме кликнете <a onclick="location.href = '/customer/login_android/gift_<?php print $dealData->id ?>'">тука</a> за да се логирате.
                                </span>
                            <?php } ?>

                            <?php if ($userID > 0 && $this->session->get("user_type") == 'customer' && $customerPoints < 10) { ?>
                                <span>
                                    Искористувањето на вашите поени е можно ако имате освоено минимум 10 поена (10 ден.)
                                </span>
                            <?php } ?>
                        </div>
                        
                    </div>
                </div>
                <?php
                $points_osnova = calculate::points_osnova($dealData->price_discount, $dealData->price_voucher, $dealData->tip_danocna_sema, $dealData->ddv_stapka, $dealData->tip);
                if (floor($points_osnova) > 9) {
                    ?>
                    <div class="subpage-help" style="margin-bottom: 20px">
                        <p class="icon-coinssmall" style="font-size: 15px;">
                            <strong>
                                <?php print kohana::lang("index.Со купување на овој ваучер добивате"); ?> 
                                <span id="credits">
                                    <?php
                                    $pointsCalculated = calculate::points($points_osnova, 1);
                                    print $pointsCalculated;
                                    ?>
                                </span> 
                                <?php $pointsCalculated == 1 ? print '"kupinapopust" поен.' : print kohana::lang("index.Kupinapopust поени"); ?>
                            </strong>
                        </p>
                    </div>
                <?php } ?>

            </div>
        </form>

    </div>
</div>