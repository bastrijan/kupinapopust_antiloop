		<div class="row row-wrap" id="deals-matrix">
		
			<?php 
			$cnt_bottom_offers = count($bottomOffers);
			$current_bottom_offer = 0;

			$reper_cnt_bottom_offers = ($cnt_bottom_offers < 6 ? $cnt_bottom_offers : 6);

			foreach ($bottomOffers as $bottomDeal) {

				$current_bottom_offer++;
			
				//se pravi logika za vremeto da se prikaze
				$timeLeft_bo = strtotime($bottomDeal-> end_time) - time();
				$timeStart_bo = strtotime($bottomDeal->start_time);
				$timeEnd_bo = strtotime($bottomDeal->end_time);
				$timeNow_bo = time();
				
				if($action == "demo") { //ova e za demo ponudite, zatoa sto kaj niv ne se stava start i end
					$progress = 50;
				} else {
					$progress = ((int)($timeNow_bo - $timeStart_bo) / (int)($timeEnd_bo - $timeStart_bo)) * 100;
				}
				
				if ($progress > 100) {
					$progress = 100;
				}
				
				//se pravi logika za toa sto da se prikaze koga ke se klikne na ponudata e istecena ili rasprodadena
				/* NE SE KORISTI
				$onclick = '';
				
				if ($progress >= 100) {
					$onclick = 'return false;';
				}
				if ($bottomDeal->voucher_count >= $bottomDeal->max_ammount and $bottomDeal->max_ammount) {
					$onclick = 'sold_out(); return false;';
				}
				*/

				$title_mk_clean = "";
				$title_mk = "";
				$max_ammount = 0;
				$ceni_od_txt = "";

				if($bottomDeal->options_cnt > 1 || $bottomDeal->is_general_deal)
				{
					$title_mk_clean =  $bottomDeal->title_mk_clean_deal;
					$title_mk =  $bottomDeal->title_mk_deal;
					$max_ammount = 0;
					if($bottomDeal->is_general_deal)
						$ceni_od_txt = "Прегледај понуди";
					else	
						$ceni_od_txt = ($bottomDeal->finalna_cena > 0 ? "од ".$bottomDeal->finalna_cena." ден." : "Превземи купон");
				}
				else
				{
					$title_mk_clean =  $bottomDeal->title_mk_clean;
					$title_mk =  $bottomDeal->title_mk; 
					$max_ammount = $bottomDeal->max_ammount;
					$ceni_od_txt = ($bottomDeal->finalna_cena > 0 ? $bottomDeal->finalna_cena." ден." : "Превземи купон");
				}
			?>
				<div class="<?php echo $bottom_offers_div_class; ?>">

					<div class="product-thumb">
						<header class="product-header">
						<?php
							if($bottomDeal->is_general_deal)
								$detali_za_ponudata = "/deal/general_deal/" . seo::DealPermaLink($bottomDeal->deal_id, $title_mk_clean, $bottomDeal->category_id, $bottomDeal->subcategory_id, $categoriesData, $subCategoriesData);
							else
								$detali_za_ponudata = "/deal/index/" . seo::DealPermaLink($bottomDeal->deal_id, $title_mk_clean, $bottomDeal->category_id, $bottomDeal->subcategory_id, $categoriesData, $subCategoriesData);
							
						?>
							<!-- /////////////// Link do ponudata /////////////// -->
							<a target="_blank" href ="<?php echo $detali_za_ponudata; ?>">
								
								<!-- /////////////// Slika od ponudata /////////////// -->
								<img src="/pub/deals/<?php print $bottomDeal->side_img; ?>" alt="<?php print str_replace("\"", "'", strip_tags($title_mk_clean)); ?>"  />
							</a>
							
							<!-- /////////////// Popust /////////////// -->
							
							<?php if($bottomDeal->price > 0 && !$bottomDeal->is_general_deal)
									print '<span class="product-save popust-na-slika">'."- " . (int)round(100 - ($bottomDeal->price_discount / $bottomDeal->price) * 100) . "%".'</span>';
							?>


							
							<span class="product-save omileni-na-slika">
								<a href="#" class="btn btn-sm fav-button<?php echo in_array($bottomDeal->deal_id, $favDeals) ? ' fav-active' : ''; ?>" data-toggle="tooltip" data-placement="top" data-title="<?php echo in_array($bottomDeal->deal_id, $favDeals) ? 'Одземи од омилени' : 'Додади во омилени'; ?>">
									<i class="fa fa-star"></i>
									<i class="infoDealID" style="display: none;"><?php echo $bottomDeal->deal_id; ?></i>
								</a>
							</span>

							<?php if (!$bottomDeal->is_general_deal) { ?>
								<span class="product-save shop-cart-na-slika" <?php if(strtotime($bottomDeal->start_time) > time()) echo 'style="display: none;"'; ?>>
									<a href="#" class="btn btn-sm <?php echo $bottomDeal->options_cnt > 1 ? 'shop-cart-many-options': 'shop-cart-button'; ?> <?php echo in_array($bottomDeal->deal_option_id, $shopCartDeals) ? ' shop-cart-active' : ''; ?>" data-toggle="tooltip" data-placement="top" data-title="<?php echo in_array($bottomDeal->deal_option_id, $shopCartDeals) ? 'Одземи од кошничка' : 'Додади во кошничка'; ?>">
										<i class="fa fa-shopping-cart"></i>
										<i class="infoDealOptionID" style="display: none;"><?php echo $bottomDeal->deal_option_id; ?></i>
									</a>
								</span>
							<?php } //if (!$bottomDeal->is_general_deal) { ?>	


						</header>
						<div class="product-inner" >
						
							<!-- /////////////// Naslov na ponudata /////////////// -->
							<?php if(!$bottomDeal->is_general_deal) { ?>
								<h5 style="height: <?php echo Kohana::config('settings.so_partner_height');?>px">
									<?php print commonshow::Truncate(strip_tags($bottomDeal->name), Kohana::config('settings.so_partner_letters')); ?>
								</h5>
								<div class="gap-small"></div>
							<?php }//if(!$bottomDeal->is_general_deal) { ?>


								
							<?php 
								$product_title_height = Kohana::config('settings.so_title_height');
								$product_title_letters = Kohana::config('settings.so_title_letters');
								//dokolku e OPSTA PONUDA
								if($bottomDeal->is_general_deal)
								{
									$product_title_height = 15 + Kohana::config('settings.so_title_height') + Kohana::config('settings.so_partner_height');
									$product_title_letters = 10 + Kohana::config('settings.so_title_letters') + Kohana::config('settings.so_partner_letters');
								}
							

							?>

							<a target="_blank" href ="<?php echo $detali_za_ponudata; ?>">
								<h5 class="product-title" style="height: <?php echo $product_title_height;?>px"><?php print commonshow::Truncate(strip_tags($title_mk_clean), $product_title_letters); ?></h5>
							</a>
							<div class="product-meta">
							
								<!-- /////////////// Vreme do krajot na ponudata /////////////// -->
								<span class="product-time"><i class="fa fa-clock-o"></i>
									<?php echo commonshow::staticCountDown(strtotime($bottomDeal->start_time), strtotime($bottomDeal->end_time)); ?>
								</span>
								
								<!-- //////////////////// Broj na kupuvaci //////////////////// -->
								<?php
						
										$count = $bottomDeal->voucher_count;
										$bottomDealsTooltipTxt = commonshow::tooltip($count, $bottomDeal->min_ammount, $max_ammount);

										print '<h6 class="text-green"><strong class="buyers-count" title="' . $bottomDealsTooltipTxt . '">';
										if($count > 0)
										{
											if(commonshow::isSoldOut($count, $max_ammount)) {
												print "<span style='color: red'>";
												$kupi_style = "btn-danger";
												$kupi_tooltip = "Распродадено";
												//$onclick = 'sold_out(); return false;';
											} else {
												$kupi_style = "green";
												$kupi_tooltip = "Купи";
												//$onclick = '';
											}
											
											print '<i class="fa fa-level-up"></i>&nbsp;';
											print commonshow::number($count, $bottomDeal->min_ammount, $max_ammount);
											print commonshow::text($count);
											
											if(commonshow::isSoldOut($count, $max_ammount))
												print "</span>";
										} else {
											print "&nbsp;";
											$kupi_style = "green";
											$kupi_tooltip = "Купи";
											//$onclick = '';
										}
										print '</strong></h6>';
									
								?>
								
								
									<ul class="product-price-list">
										
										<!-- /////////////// Cena /////////////// -->
										<li>
											<a target="_blank" href ="<?php echo $detali_za_ponudata; ?>">
												<span class="product-price">
													<?php print $ceni_od_txt; ?>
												</span>
											</a>
										</li>
										
										<!-- /////////////// Stara cena /////////////// -->
										<li>
											<?php if($bottomDeal->options_cnt <= 1 && $bottomDeal->price > 0) { ?>
												<span class="product-old-price">
													<?php	print $bottomDeal->price . " " . kohana::lang("prevod.ден"); ?>
												</span>
											<?php } ?>
										</li> 
									</ul>

							</div>
						</div>
					</div>
				</div>

					
					<?php
						if($controller != 'deal' && $controller != 'index')
						{
							$baner_location_2 = bannersystem::getBanner(2);
							
							if($baner_location_2 != "" && ($current_bottom_offer == $reper_cnt_bottom_offers) ) 
							{ 
					?>
								<!-- Header banner 2 START-->
								<div class="col-md-12">
									<?php print $baner_location_2;?>
								</div> 
								<!-- Header banner 2 END-->
					<?php 	} 
						}
					?>


			<?php } // foreach ($bottomOffers as $bottomDeal) ?>

		</div>	


		<!-- //////////////////// Prikazi povekje ponudi //////////////////// -->
		<?php if($bottomOffersPagesCnt > 1) { ?>                            
			<div class="row">
				<div class="col-md-12 text-center bold">
					<div id="LoadingImage" style="display: none;">
						<img src="/pub/img/loader.gif" alt="Испраќање..." style="display: block;margin: auto;width: 50px;" />
					</div>
					
					<a class="btn btn-lg btn-block green" id="loadMore" style="display: inline; padding: 10px">Прикажи повеќе понуди</a>

					
					<input type="hidden" id="pagination-cnt" name="pagination-cnt" value="1">
					<input type="hidden" id="dateTimeUnique" name="dateTimeUnique" value="<?php echo $dateTimeUnique; ?>">
					<input type="hidden" id="bottomOffersPagesCnt" name="bottomOffersPagesCnt" value="<?php echo $bottomOffersPagesCnt; ?>">
				</div>
			</div>
			<div class="gap gap-small"></div>
		<?php } // if($bottomOffersPagesCnt > 1) ?>                            

		<script>
		
			$(document).on('click', '#loadMore', function () {
				var url = '/index/index_ajax_paging/';
				var pagination_cnt = $('#pagination-cnt').val();
				var dateTimeUnique = $('#dateTimeUnique').val();
				var bottomOffersPagesCnt = $('#bottomOffersPagesCnt').val();
				
				$("#LoadingImage").show();
				$("#loadMore").hide();
				$.ajax({
					url: url,
					data: {
						pagination_cnt: pagination_cnt,
						dateTimeUnique: dateTimeUnique,
						type: "ajax",
						controller: <?php echo '"'.Router::$controller.'"'; ?>,
						method: <?php echo '"'.Router::$method.'"'; ?>,
						deal_id:  <?php echo (isset($deal_id) ? $deal_id : 0); ?>,
						general_offer_id:  <?php echo (isset($general_offer_id) ? $general_offer_id : 0); ?>,
						category_id:  <?php echo (isset($selectedCategory) ? $selectedCategory : 0); ?>,
						subcategory_id:  <?php echo (isset($selectedSubCategory) ? $selectedSubCategory : 0); ?>,
						mode:  <?php echo (isset($mode) ? $mode : '""'); ?>,
						tag_name:  <?php echo '"'.(isset($tag_name) ? $tag_name : '').'"'; ?>
					},
					enctype: 'multipart/form-data',
					dataType: 'json',
					type: 'POST',
					error: function () {
						document.title = 'error';
					},
					success: function (data) {
						if (data && data.status == 'success') {
							
							$('input[name="pagination-cnt"]').val(data.pagination_cnt);
							$('#deals-matrix').append(data.html);
							
							$("#LoadingImage").hide();
							
							if(data.pagination_cnt < bottomOffersPagesCnt)
								$('#loadMore').show();
							else
								$("#loadMore").hide();
							
						} else {
							alert('Се појави грешка!');
							$("#LoadingImage").hide();
						}
					}
				});
			});
			
		</script>
