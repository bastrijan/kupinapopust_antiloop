<style>
    
    span#dealprice{
        color: #48b548;
        font-size: 22px;
    }
    
</style>

<?php $userID = $this->session->get("user_id"); ?>
<script type="text/javascript" src="/pub/js/pay_android.js"></script>

<div data-role="popup" id="invalidmailPopup" class="ui-content">
  <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
  <?php print kohana::lang("index.невалиден маил"); ?>
</div>
	
<input type="hidden" name="invalidmail" id="invalidmail" value="<?php print kohana::lang("index.невалиден маил"); ?>"/>
<input type="hidden" name="defaulttext" id="defaulttext" value="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>"/>

<script type="text/javascript">
<?php if (isset($user_login) and $user_login == 1) { ?>
        $(document).ready(function () {
            $.noticeAdd({
                text: "Успешно се логиравте.",
                stay: false,
                type:'notification-success'
                //                stayTime: 3000
            });
        })    
<?php } ?>    

<?php if (isset($vaucer_nadminat_limit) and $vaucer_nadminat_limit == 1) { ?>
        $(document).ready(function () {
            $.noticeAdd({
                text: "Почитувани, бидејќи во меѓувреме друг посетител има купено од понудата и со тоа е надминато ограничувањето. Ве молиме изберете помала количина.",
                stay: false,
                type: 'notification-error',
                stayTime: 20000
            });
        })    
<?php } ?> 

<?php if (isset($vaucer_nadminat_limit) and $vaucer_nadminat_limit == 2) { ?>
        $(document).ready(function () {
            $.noticeAdd({
                text: "Почитувани, го имате надминато ограничувањето за купени ваучери по корисник. Ве молиме изберете помала количина.",
                stay: false,
                type: 'notification-error',
                stayTime: 20000
            });
        })    
<?php } ?>   
</script>

<div class="homepage-content">
    <div class="layout-buy">
	
	
		<input type="hidden" id="priceDiscount" name="priceDiscount" value="<?php print $dealData->price_discount; ?>"/>
		<input type="hidden" id="priceVoucher" name="priceVoucher" value="<?php print $dealData->price_voucher; ?>"/>
		<input type="hidden" id="tipDanocnaSema" name="tipDanocnaSema" value="<?php print $dealData->tip_danocna_sema; ?>"/>
		<input type="hidden" id="ddvStapka" name="ddvStapka" value="<?php print $dealData->ddv_stapka; ?>"/>
		<input type="hidden" id="tipPonuda" name="tipPonuda" value="<?php print $dealData->tip; ?>"/>
		<?php
			$form_action_additional = Kohana::config('config.payment_gateway');
		?>
        <form  name="payForm" id="paymentform" method="post" action="/pay<?php echo $form_action_additional."_mobile"; ?>/index">
			<input type="hidden" id="app_type" name="app_type" value="Android"/>
			
            <?php $langTitle = "title_" . $lang ?>

            <h1><?php //print $dealData->$langTitle;     ?></h1>
			
            <div class="subpage-content">
                <div class="section-container">
                    <div class="caption">
                        <table width="100%">
                            <tr>
                                <td valign="top">
                                    <img style="width:110px; margin-left: 20px;padding-top:15px;padding-right:5px;" alt="" src="/pub/deals/<?php print $dealData->side_img ?>" align="left">
									<?php print $dealData->title_mk_clean; ?>
                                </td>
                            </tr>
                            <tr>
                                    <td style="padding: 8px 18px"><?php print kohana::lang("index.Град"); ?>:<?php print kohana::lang("index.Скопје"); ?></td>
                            </tr>
                        </table>
                    </div>
                    <div class="section-form">
                        <input type="hidden" id="discount_id" name="discount_id" value="<?php print $dealData->id ?>"/>
                        <input type="hidden" id="gift" name="gift" value="0"/>
                        <input type="hidden" id="AmountToPay" name="AmountToPay" value="<?php print ($dealData->tip == 'cena_na_vaucer' ? $dealData->price_voucher : $dealData->price_discount); ?>"/>
                        <input type="hidden" id="AmountCurrency" name="AmountCurrency" value="MKD"/>
                        <input type="hidden" id="Details1" name="Details1" value="<?php print strip_tags($dealData->$langTitle) ?>"/>
                        <input type="hidden" id="Details2" name="Details2" value="<?php print $dealData->id ?>"/>

                        <table>
                            <tbody>
								<tr>
    
                                    <td> <?php print kohana::lang("index.Количина"); ?><br/>
                                        <select id="amount" name="amount" onfocus="this.onmousewheel=function(){return false}">
                                            <?php
                                            if (!$dealData->max_per_person) {
                                                $dealData->max_per_person = 50;
                                            }
											
											$currentMaxPerPerson = $dealData->max_per_person;
											
											if ($dealData->max_ammount && (($dealData->max_ammount - $soldVoucherCount) < $dealData->max_per_person) )
												$currentMaxPerPerson = ($dealData->max_ammount - $soldVoucherCount);
											
											
                                            for ($index = 1; $index <= $currentMaxPerPerson; $index++) {
                                                print "<option value='$index'>$index</option>";
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                      <td>
                                        <input type="text" value="" id="Email" name="Email" placeholder="<?php print kohana::lang("index.Вашиот Е-mail"); ?>"><br>
                                        <span style="" class=""><?php print kohana::lang("index.На оваа e-mail адреса ќе биде испратен вашиот ваучер"); ?></span>
                                    </td>
                                </tr>
                                <tr class="group">
                                    <td>
                                        <span class="payment-title"><?php print kohana::lang("index.Плаќање со:"); ?><br/>
                                            <input type="radio" value="card" id="payment-0" name="payment" class="radio" checked="checked">
                                            <label for="payment-0"><?php print kohana::lang("index.Платежна картичка"); ?></label>
                                        </span>
                                        <span class="payment-help">
                                            <img src="/pub/img/layout/maestro.png" style="height: 30px"/>
                                            <img src="/pub/img/layout/mastercard.png" style="height: 30px"/>
                                            <img src="/pub/img/layout/visa.png" style="height: 30px"/>
                                            <img src="/pub/img/layout/visa_electron.png" style="height: 30px"/>
                                        </span>
                                        <span class="payment-title">
                                            &nbsp;
                                        </span>
                                        <span class="payment-help">
                                            <br/>
                                            <span class="icon-card"><?php print kohana::lang("index.Ваучерот се добива веднаш"); ?></span>
                                        </span>
                                    </td>
                                </tr>
                                <?php if ($userID == '297') { //deactivated temporarily ?>
                                    <tr>
                                        
                                        <td>
                                            <span class="payment-title" style="width: 25px">
                                                <input type="radio" value="cache" id="payment-1" name="payment" class="radio">
                                            </span>
                                            <span class="payment-title" style="width: 165px">
                                                <label for="frmpayForm-payment-1"><?php print kohana::lang("index.Плаќање во готово"); ?></label>
                                            </span>
                                            <span class="payment-help" style="width: 275px; float:left">
                                                <?php print "&nbsp;";//kohana::lang("index.Плаќање во готово2"); ?>
                                            </span>    
                                        </td>
                                    </tr>
                                <?php } ?>
                                
                                <?php if ($userID == '297') { //the id for jovica.belovski@gmail.com ?>
                                    <tr>
                                        
                                        <td>
                                            <span class="payment-title" style="width: 25px">
                                                <input type="radio" value="bank" id="payment-2" name="payment" class="radio">
                                            </span>
                                            <span class="payment-title" style="width: 165px">
                                                <label for="frmpayForm-payment-2"><?php print kohana::lang("index.Плаќање со уплатница"); ?></label>
                                            </span>
                                            <span class="payment-help" style="width: 275px; float:left">
                                                <?php print kohana::lang("index.Плаќање со уплатница2"); ?>
                                            </span>
                                        </td>
                                    </tr>
                                <?php } ?>
                                
                                <?php if ($customerPoints and !$dealData->card) { ?>
                                    <tr class="group">
                                        <td>Поени:<br/>
                                            <span class="payment-title" style="width: 190px">
                                                <select id="points" name="points_used" onfocus="this.onmousewheel=function(){return false}">
                                                    <?php
                                                    print "<option value='0'>0 поени</option>";
                                                    $max = (int) ($customerPoints / 10);
                                                    if ($max) {
                                                        for ($index = 1; $index <= $max; $index++) {
                                                            $value = $index * 10;
                                                            print "<option value='$value'>$value поени = $value денари</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </span>
                                            <span class="payment-help" style="width: 275px; float:left">
                                                &nbsp;
                                            </span>

                                        </td>
                                    </tr>
                                <?php } ?>
                                <tr <?php if (!$customerPoints) print 'class="group"'; ?>>
                                    <td>
                                        <input type="submit" value="<?php print kohana::lang("prevod.Купи"); ?>" class="submit" id="pay">
                                        <span class="help">
                                            <?php print kohana::lang("prevod.Цена"); ?>: 
                                            <strong>
                                                <span id="dealprice">
                                                    <?php print ($dealData->tip == 'cena_na_vaucer' ? $dealData->price_voucher : $dealData->price_discount); ?>
                                                    <?php print kohana::lang("prevod.ден"); ?>
                                                </span>
                                                 
                                            </strong></span>
                                    </td>
                                </tr>
                                <?php if ($userID == 0) { ?>
                                    <tr>
                                     
                                        <td>
                                            <span>
                                                За искористување на вашите поени ве молиме кликнете <a href="/customer/login/deal_<?php print $dealData->id ?>">тука</a> за да се логирате.
                                            </span>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <?php if ($userID > 0 && $this->session->get("user_type") == 'customer' && $customerPoints < 10) { ?>
                                    <tr>
                                      
                                        <td>
                                            <span>
                                                Искористувањето на вашите поени е можно ако имате освоено минимум 10 поена (10 ден.)
                                            </span>
                                        </td>
                                    </tr>
                                <?php } ?>
								
                            </tbody>
                        </table>
                    </div>
                </div>
				<?php 
				$points_osnova = calculate::points_osnova($dealData->price_discount, $dealData->price_voucher, $dealData->tip_danocna_sema, $dealData->ddv_stapka, $dealData->tip);
				
				if (floor($points_osnova) > 9) { 
				?>
						<div class="subpage-help" style="margin-bottom: 20px">
							<p class="icon-coinssmall" style="font-size: 15px;">
								<strong>
									<?php print kohana::lang("index.Со купување на овој ваучер добивате"); ?> 
									<span id="credits">
										<?php 
											$pointsCalculated = calculate::points($points_osnova, 1);
											print $pointsCalculated; 
										?>
									</span> 
										<?php $pointsCalculated == 1 ? print '"kupinapopust" поен.' : print kohana::lang("index.Kupinapopust поени") ; ?>
								</strong>
							</p>
						</div>
				<?php } ?>
            </div>

        </form>

    </div>
</div>


