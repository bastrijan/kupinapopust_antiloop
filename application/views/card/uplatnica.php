<!--
<div class="col-md-4">
	<img src="/pub/img/layout/paynet-logo.png" />
</div>
<div class="col-md-8">
	Лесно до вашиот ваучер. Избегнете ги гужвите и банкарските провизии. Избрани понуди од kupinapopust достапни на сите терминали на Paynet низ државата. Кликнете <a target="_blank" href="https://paynet.mk/map"><b>овде</b></a> за приказ на мапата на терминалите во сите градови.
</div>


<div class="gap"></div>
-->

<div class="col-md-12">

	<div>
		<img src="/pub/img/layout/ok20x25.png" style="width: 25px" />
		<span style="font-size: 15px; padding-left: 15px">
			<b>Инструкции за плаќање со уплатница</b>
		</span>
	</div>

	<div class="gap-mini"></div>

	<p>Почитувани Корисници,</p>
	<p>Со цел да ви обезбедиме пофлексибилно плаќање, ја имате можноста да ја платите понудата преку уплатница во банка со пополнување на ПП 10 образец.</p>

	<p>На една уплатница може да платите за повеќе ваучери. По извршената уплата ве молиме информирајте не преку е-mail на <b>komercija@kupinapopust.mk</b> за да го евидентираме вашето плаќање.</p>

	<p>Потребни ни се:</p>
	<div style="margin-left: 40px">
		<p>1. Вашето име и презиме.</p>
		<p>2. За која понуда сте уплатиле.</p>
		<p>3. Сумата која сте ја уплатиле.</p>
	</div>
	<p>Обично се потребни 1-2 дена за да се изврши преносот на пари. Затоа ве молиме за трпеливост. Доколку ни испратите слика од уплатницата или скенирана копија по e-mail, тогаш веднаш ви го испраќаме ваучерот.</p>

	<p>Ова е пример како треба да биде пополнета уплатницата:</p>

	<div class="gap"></div>

	<p><img border="0" src="<?php echo Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL');?>/pub/img/layout/uplatnica-bez-platezna-karticka.jpg"></p>

	<div class="gap"></div>

	<p>Со почит,</p>
	<p><b>Kupinapopust.mk</b> тим</p>

</div>