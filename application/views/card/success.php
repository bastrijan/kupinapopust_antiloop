    <div class="row">
        <div class="col-md-8 col-md-push-4">
            <div class="row">
                <div class="col-md-12">
                    <h3>Активацијата е успешна!</h3>

                    <div class="row">
                        <div class="col-md-6 mt20">


                              <?php if ($type == 1):?>  
                              <p>Почитувани,<br/>
                                Ви благодариме и ви посакуваме пријатно купување.</p>
                              <p>&nbsp;</p>
                              <?php endif;?>
                              <?php if ($type == 2):?>  
                              <p>Почитувани,<br/>
                                На профилот на вашиот пријател се препишани поените што му ги подаривте.</p>
                              <p>Со почит, Kupinapopust тим</p>
                              <p>&nbsp;</p>
                              <?php endif;?>
                              <p>
                                <span>
                                  <input type="button" value="OK" class="btn btn-primary btn-lg" onclick="location.href='/';" />  
                                </span>
                              </p>

                        </div>
                        
                        <div class="col-md-6 mt20">
                            <img src="/pub/img/gift-coupon.jpg" />
                        </div>
                    </div>

                    
                </div>
            </div>
                    
            <div class="gap"></div>
            
        </div>
        
        <div class="col-md-4 col-md-pull-8">
            <aside class="sidebar-left">
            
                <?php
                    require_once APPPATH . 'views/layouts/contact.php';
                    require_once APPPATH . 'views/layouts/satisfaction.php';
                    require_once APPPATH . 'views/layouts/side_static_links.php';
                ?>
                <!-- 
                <div class="gap hidden-xs"></div>
                <img class="img-responsive hidden-xs" src="/pub/img/020215014417988gamarde-baner.gif" />
                <div class="gap hidden-xs"></div>
                -->
                <div class="gap hidden-xs"></div>
            </aside>            
        </div>
     
    </div>