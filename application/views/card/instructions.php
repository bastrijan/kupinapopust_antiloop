<?php
	$controller = Router::$controller;
	$action = Router::$method;
?>

	<div class="row">
		<div class="col-md-8 col-md-push-4">
			<div class="row">
				<div class="col-md-12">
					<?php require_once APPPATH . 'views/card/uplatnica.php'; ?>
				</div>
			</div>
			
			<div class="gap"></div>
			
		</div>
		
		<div class="col-md-4 col-md-pull-8">
			<aside class="sidebar-left">
			
				<?php
					require_once APPPATH . 'views/layouts/contact.php';
					require_once APPPATH . 'views/layouts/satisfaction.php';
					require_once APPPATH . 'views/layouts/side_static_links.php';
                ?>
				<!-- 
				<div class="gap hidden-xs"></div>
				<img class="img-responsive hidden-xs" src="/pub/img/020215014417988gamarde-baner.gif" />
				<div class="gap hidden-xs"></div>
				-->
				<div class="gap hidden-xs"></div>
			</aside>            

		</div>
	  
	</div>

