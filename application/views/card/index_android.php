<style type="text/css">
    div.card_images img{
        display: block;
        width: 100%;
    }
</style>

<div data-role="page">
    <div data-role="header">
        <h1>Подарок електронска картичка</h1>
    </div>
    <div data-role="main" class="ui-content">

        <p>Идеален подарок за сите што уживаат да купуваат и да штедат.</p>
        <hr>
        <p>Немате ризик дека подарокот нема да им се допадне. Тие сами си избираат за која понуда ќе го искористат подарениот кредит.</p>

        <div data-role="controlgroup" data-type="vertical">
		
            <input type="button" onclick="window.MyHandlerE_Card.buyGift(100)" value="Подари 300 ден. кредит" />
            <input type="button" onclick="window.MyHandlerE_Card.buyGift(101)"  value="Подари 500 ден. кредит" />
            <input type="button" onclick="window.MyHandlerE_Card.buyGift(102)"  value="Подари 1000 ден. кредит" />
		
            <input type="button" onclick="window.MyHandlerE_Card.CardActive()"  value="Искористи го подарениот кредит" />
        </div>

        <div class="card_images">
            <img src="/pub/img/layout/podarok_karticka.png">
            <br>
            <img scr="/pub/img/layout/500den.png">
        </div>
    </div>
</div>
