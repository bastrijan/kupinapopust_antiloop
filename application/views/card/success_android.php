<div data-role="page">
    <div class="caption">
      <table>
        <tr>
          <td style="padding: 8px 18px">
            <div>
              <img src="/pub/img/layout/ok20x25.png" style=""/>
              <span style="font-size: 12pt;font-weight: bold;">
                Активацијата е успешна.
              </span>
            </div>                   
            
          </td>
        </tr>
      </table>
    </div>
	<div style="padding-left: 8px">
		<?php if ($type == 1):?>  
		<p>Почитувани,</p>
		<p>Ви благодариме и ви посакуваме пријатно купување.</p>
		<p>&nbsp;</p>
		<?php endif;?>
		<?php if ($type == 2):?>  
		<p>Почитувани,</p>
		<p>На профилот на вашиот пријател се препишани поените што му ги подаривте.</p>
		<p>Со почит, Kupinapopust тим</p>
		<p>&nbsp;</p>
		<?php endif;?>
	</div>
</div>
