<div data-role="page">
    <div data-role="header">
        <h1>Активирање на подарениот кредит</h1>
    </div>
    <div data-role="main" class="ui-content">
        <form  name="cardForm" id="cardform" method="post" action="">
            <div class="section-form">
                
                <?php if (isset($error)) { ?>
                    <div style="color: red; ">
                        <?php print $error; ?>
                    </div>
                <?php } ?>
                <input type="text" value="" id="Email" name="Email" placeholder="<?php print kohana::lang("customer.Вашиот Е-mail"); ?>" class="">
                <input type="text" value="" id="code" name="code" placeholder="Единствен код" class="">
                <input type="text" value="" id="Email-gift" name="Email-gift" placeholder="E-mail на пријателот" class="">
				 <label style="color: #F60;">
					Со внесување на е-mail на пријателот ги подарувате вашите поени на трето лице
				</label>
                <input type="submit" value="Активирај" class="submit" id="activate" >
            </div>
        </form>
        <h3>Како да го активирате подарениот кредит?</h3>
        <p>Внесете го “Единствениот код“ што го добивте на вашиот E-mail и кликнете “Активирај“. По успешната верификација поените во вашиот профил ќе се зголемат за подарената сума.</p>
        <hr>
        <p>Доколку внесете е-mail на пријател поените му ги подарувате нему и тие ќе се препишат на неговиот профил.</p>

       
    </div>
</div>