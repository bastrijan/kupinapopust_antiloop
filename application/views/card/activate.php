    <div class="row">
        <div class="col-md-8 col-md-push-4">
            <div class="row">
                <div class="col-md-12">
                    <h3>Подарок “kupinapopust“ електронска картичка</h3>

                    <div class="row">
                        <div class="col-md-6 mt20">


                            <form  name="cardForm" id="cardform" method="post" action="">
                                <div >
                                    <?php if (isset($error)) { ?>
                                        <div style="color: red;">
                                            <?php print $error; ?>
                                        </div>
                                    <?php } ?>


                                    <div class="form-group">
                                        <label for="Email"><?php print kohana::lang("customer.Вашиот Е-mail"); ?></label>
                                        <input type="text" value="" id="Email" name="Email" placeholder="email@domain.com" class="form-control" />
                                    </div>


                                    <div class="form-group">
                                        <label for="Email">Единствен код</label>
                                        <input type="text" value="" id="code" name="code" class="form-control" />
                                    </div>

                                    <div class="form-group">
                                        <label for="Email">E-mail на пријателот</label>
                                        <input type="text" value="" id="Email-gift" name="Email-gift" placeholder="email@domain.com" class="form-control" />
                                        <br />Со внесување на е-mail на пријателот ги подарувате вашите поени на трето лице
                                    </div>


                                    <div class="form-group">
                                            <input type="submit" value="Активирај" id="activate" class="btn btn-primary btn-lg" />  
                                    </div>                

                                </div>


                            </form>

                        </div>
                        
                        <div class="col-md-6 mt20">
                            <img src="/pub/img/gift-coupon.jpg" />
                        </div>
                    </div>

                    <div class="gap"></div>
                    <h4>Како да го активирате подарениот кредит?</h4>

                    <p><span style="color:#ffa500;">• </span>Внесете го “Единствениот код“ што го добивте на вашиот E-mail и кликнете “Активирај“. По успешната верификација поените во вашиот профил ќе се зголемат за подарената сума.</p>
                    <p><span style="color:#ffa500;">• </span>Доколку внесете е-mail на пријател поените му ги подарувате нему и тие ќе се препишат на неговиот профил.</p>
                </div>
            </div>
                    
            <div class="gap"></div>
            
        </div>
        
        <div class="col-md-4 col-md-pull-8">
            <aside class="sidebar-left">
            
                <?php
                    require_once APPPATH . 'views/layouts/contact.php';
                    require_once APPPATH . 'views/layouts/satisfaction.php';
                    require_once APPPATH . 'views/layouts/side_static_links.php';
                ?>
                <!-- 
                <div class="gap hidden-xs"></div>
                <img class="img-responsive hidden-xs" src="/pub/img/020215014417988gamarde-baner.gif" />
                <div class="gap hidden-xs"></div>
                -->
                <div class="gap hidden-xs"></div>
            </aside>            
        </div>
     
    </div>