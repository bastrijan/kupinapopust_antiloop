<?php
	$controller = Router::$controller;
	$action = Router::$method;

	$dealsModel = new Deal_Options_Model() ;

	$where = array('deal_id' => 100) ;
	$dealData = $dealsModel->getData($where) ;
	$deal_100_Option = $dealData[0] ;

	$where = array('deal_id' => 101) ;
	$dealData = $dealsModel->getData($where) ;
	$deal_101_Option = $dealData[0] ;

	$where = array('deal_id' => 102) ;
	$dealData = $dealsModel->getData($where) ;
	$deal_102_Option = $dealData[0] ;
?>

	<div class="row">
		<div class="col-md-8 col-md-push-4">
			<div class="row">
				<div class="col-md-12">
					<h3>Подарок “kupinapopust“ електронска картичка</h3>
					<h5>Идеален подарок за сите што уживаат да купуваат и да штедат.</h5>
					<h5>Немате ризик дека подарокот нема да им се допадне. Тие сами си избираат за која понуда ќе го искористат подарениот кредит.</h5>

					<div class="row">
						<div class="col-md-5 mt20">
							<a class="btn-block slikicki" href="/buy/gift/100/<?php echo $deal_100_Option->id; ?>">Подари 300 ден. кредит</a>
							<a class="btn-block slikicki" href="/buy/gift/101/<?php echo $deal_101_Option->id; ?>">Подари 500 ден. кредит</a>
							<a class="btn-block slikicki" href="/buy/gift/102/<?php echo $deal_102_Option->id; ?>">Подари 1000 ден. кредит</a>
							<a class="btn-block slikicki" href="/card/activate">Искористи го подарениот кредит</a>
						</div>
						
						<div class="col-md-6 mt20">
							<img src="/pub/img/gift-coupon.jpg" />
						</div>
					</div>

					<div class="gap"></div>
					<h4>Што можете да купите, на пример за 500 ден.?</h4>

					<div class="row">
						<div class="col-md-4"><img src="/pub/img/masaza.jpg" /></div>
						<div class="col-md-4"><img src="/pub/img/spagetki.jpg" /></div>
						<div class="col-md-4"><img src="/pub/img/blago.jpg" /></div>
					</div>
				</div>
			</div>
					
			<div class="gap"></div>
			
		</div>
		
		<div class="col-md-4 col-md-pull-8">
			<aside class="sidebar-left">
			
				<?php
					require_once APPPATH . 'views/layouts/contact.php';
					require_once APPPATH . 'views/layouts/satisfaction.php';
					require_once APPPATH . 'views/layouts/side_static_links.php';
                ?>
				<!-- 
				<div class="gap hidden-xs"></div>
				<img class="img-responsive hidden-xs" src="/pub/img/020215014417988gamarde-baner.gif" />
				<div class="gap hidden-xs"></div>
				-->
				<div class="gap hidden-xs"></div>
			</aside>            
		</div>
	 
	</div>
