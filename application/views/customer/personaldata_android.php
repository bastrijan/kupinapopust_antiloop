<div id="demo" data-role="page">
    <div data-role="header">
        <h1>Лични податоци</h1>
    </div>
    <div data-role="main" class="ui-content">



        <?php if (isset($error)) { ?>
            <div style="color: red; padding: 20px">
                <?php print $error; ?>
            </div>
        <?php } ?>
        <?php if (isset($success)) { ?>
            <div style="color: green; padding: 20px">
                <?php print $success; ?>
            </div>
        <?php } ?>

        <?php if ($displayForm) { ?>
            <form id="birthdayform" method="post" action="">
                 <p>
                    Почитувани, <br>
                    Внесете го датумот на Вашиот <strong>следен роденден</strong> (не датумот на раѓање) и кликнете "Зачувај". Датумот нема да биде даден на трети лица, а ќе се користи во нашиот систем за наградување.
                </p>
<!--                <fieldset>
                    <input id="input_01" class="datepicker" name="date" type="date" placeholder="Избере дата..." autofocuss value="" data-value="">
                </fieldset>-->
                 <script type="text/javascript">
                        $(document).on( "pagecreate", "#demo", function() {
                                SyntaxHighlighter.all();
                                  var picker = $( "#date_selected", this );

                                picker.mobipick();

        //			picker.on( "change", function() {
        //					var date = $( this ).val();
        //
        //					// formatted date					
        //					var dateObject = $( this ).mobipick( "option", "date" );
        //			});
                        });
                </script>
                <input type="text" id="date_selected" name="date_selected" min="<?php echo date("Y");?>" max="<?php echo (date("Y") + 2);?>" /><br/>
                <input type="submit" value="Зачувај" class="submit" id="pay" style="margin-right: 35px">
            </form>
            <?php
        }
        ?>
        <?php
        if (!isset($success) and ! isset($error)) {
            ?>
            <img src="/pub/img/layout/rodenden_slika.png" width="100%"/>
            <p>Вие го имате одбрано <?php print date("d.m.Y", strtotime($user->birthday)); ?> за ваш роденден. Очекувајте изненадување на тој ден.</p>
            <?php
        }
        ?>

    </div>
</div>