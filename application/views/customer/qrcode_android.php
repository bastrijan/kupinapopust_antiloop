<!-- JQUERY MOBILE -->
<style>
    div.item-description{
        text-align: center;
    }
    
    div.user_desc{
        margin-top: 12px;
        margin-bottom: 12px;
    }
    
    div.user_desc h5{
        margin: 0;
        padding: 0;
    }
    div.qr_desc{
        width: 100%;
    }
    div.qr_code{
        margin: auto;
        width: 100px;
    }
</style>
<div data-role="page">
  <div data-role="header">
    <h1>QR код</h1>
  </div>
  <div data-role="main" class="ui-content">
<!-- JQUERY MOBILE END-->

<?php
require_once APPPATH . 'tcpdf/2dbarcodes.php';
$code = "$userEmail $voucherCode";

$barcodeobj = new TCPDF2DBarcode($code, 'QRCODE,H');


	$langTitle = "title_" . $lang . "_clean";
	$dealDesc = $dealData[0]->$langTitle;
?>
			<div class="deal-item">
                            <h3><?php print strip_tags($dealDesc); ?></h3>
                            <hr>
                            
				<div class="item-description">
                                    <div class="user_desc">
                                        <h5>Корисник:</h5>
                                        <h5><?php echo $userEmail; ?></h5>
                                    </div>
                                    
                                    <div class="qr_desc">
                                        <div class="qr_code"><?php echo $barcodeobj->getBarcodeHTML(3,3); ?></div>
                                        <h4><?php echo $voucherCode; ?></h4>
                                    </div>
				</div>

				<div class="clear"></div>
			</div>
		
		
<!-- JQUERY MOBILE -->		
	</div>	
</div>
<!-- JQUERY MOBILE END-->