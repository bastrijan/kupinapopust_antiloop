<div data-role="page">
  <div data-role="header">
    <h1>Kupinapopust картички</h1>
  </div>
  <div data-role="main" class="ui-content">




		<?php
		foreach ($vouchers as $voucher) {
			$dealsModel = new Deals_Model();
			$dealData = $dealsModel->getData(array("id" => $voucher->deal_id));
			$langTitle = "title_" . $lang;
			$dealDesc = $dealData[0]->$langTitle;
			$dealPrice = ($dealData[0]->tip == 'cena_na_vaucer' ? $dealData[0]->price_voucher : $dealData[0]->price_discount);
		?>
		
		
			<div class="deal-item">
				<img src="<?php echo Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL');?>/pub/img/layout/podarok_karticka.png" />
				<div class="item-description">
					Подаривте "kupinapopust" картичка во вредност од <?php print strip_tags($dealDesc); ?> 
					<?php 
						$points_osnova = calculate::points_osnova($dealData[0]->price_discount, $dealData[0]->price_voucher, $dealData[0]->tip_danocna_sema, $dealData[0]->ddv_stapka, $dealData[0]->tip);
						$pointsCalculated = calculate::points($points_osnova, 1);
						if($pointsCalculated > 0) {
					?> 
							<strong>
								<br>
									Со купување на <?php echo ($voucher->cnt > 1 ? "овие ваучери" : "овој ваучер"); ?> добивате <?php print ($pointsCalculated * $voucher->cnt); ?> 
									<?php $pointsCalculated == 1 ? print ' поен.' : print ' поени.' ; ?>
								<?php //print kohana::lang("customer.по цена од") ; ?> <?php //print $dealPrice ; ?>
							</strong>
					<?php 
						}
					?> 
					<?php if ($voucher->activated) { ?>
						<br>
						<input type="button" onclick="window.MyHandlerClientDisplay.ListCodes(<?php print $voucher->attempt_id?>, <?php print $voucher->deal_id?>)"  value="Прикажи <?php echo $voucher->cnt ?> ваучер<?php echo ($voucher->cnt > 1 ? "и" : ""); ?>" />
					<?php } ?>

				</div>

				<div class="clear"></div>
			</div>

		<?php
		}
		?>                                

					
    </div>
</div>