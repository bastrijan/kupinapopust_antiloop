<input type="hidden" name="invalidmail" id="invalidmail" value="<?php print kohana::lang("index.невалиден маил"); ?>"/>
<input type="hidden" name="defaulttext" id="defaulttext" value="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>"/>
<div class="row">
    <div class="col-md-8  col-md-push-4">
        <h1 >Избор на лозинка</h1>

         <form  name="payForm" id="paymentform" method="post" action="">

            <div >
              <?php if (isset($error)) { ?>
                <div style="color: red; ">
                  <?php print $error ; ?>
                </div>
              <?php } ?>
              <?php if (isset($success)) { ?>
                <div style="color: green; ">
                  <?php print $success ; ?>
                </div>
              <?php } ?>


               <?php if ($displayForm) { ?>
              
                <div class="form-group">
                  <label for="">Новата лозинка</label>
                  <input type="password" value="" id="password1" name="password1" class="form-control">
                </div>
                <div class="form-group">
                  <label for="">Новата лозинка - повторно</label>
                  <input type="password" value="" id="password2" name="password2" class="form-control">
                </div>
                <input type="submit" value="Зачувај" id="pay" class="btn btn-primary btn-lg" />  
              <?php } ?>

            </div>

        </form>
        <div class="gap"></div>
    </div>


    <div class="col-md-4  col-md-pull-8">
        <?php require_once APPPATH . 'views/layouts/contact.php'; ?>
    </div>


</div>




