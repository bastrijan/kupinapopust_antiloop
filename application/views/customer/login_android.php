<script type="text/javascript" src="/pub/js/pay_android.js"></script>
<script type="text/javascript">
<?php if (isset($type) and $type == 2) { ?>
        $(document).ready(function () {
            $.noticeAdd({
                text: "Успешно се одјавивте.",
                stay: false,
                type: 'notification-success'
    //        stayTime: 3000
            });
        })
<?php } ?>
</script>


<!-- JQUERY MOBILE -->
<div data-role="page">
    <div data-role="header" >
        <h1 style="text-align: left;">Најава</h1>
        <a href="#" data-role="button" class="ui-btn-right" onclick="window.MyHandlerLogin.ClientRegistracja()"  >Регистрација</a>
    </div>
    <div data-role="main" class="ui-content">
        <!-- JQUERY MOBILE END-->

        <div data-role="popup" id="invalidmailPopup" class="ui-content">
            <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
            <?php print kohana::lang("index.невалиден маил"); ?>
        </div>

        <input type="hidden" name="invalidmail" id="invalidmail" value="<?php print kohana::lang("index.невалиден маил"); ?>"/>
        <input type="hidden" name="defaulttext" id="defaulttext" value="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>"/>
        <div class="deal-item">



            <br/>
            <div class="item-description">
                <form  id="paymentform" method="post" action="" data-ajax="false">	
                    <?php if (isset($error)) { ?>
                        <div style="color: red; padding: 5px">
                            <?php print $error; ?>
                        </div>
                    <?php } ?>


                    <input placeholder="<?php print kohana::lang("customer.Вашиот Е-mail"); ?>" type="text" value="" id="Email" name="Email" >

                    <input placeholder="<?php print kohana::lang("customer.Вашата лозинка"); ?>" type="password" value="" id="password" name="password" >

                    <input type="submit" value="<?php print kohana::lang("customer.Логирај се"); ?>" id="pay" >

                </form>
                
                <input type="button" value="Заборавена лозинка?" onclick="window.MyHandlerLogin.ResetPass()" id="reset-pass" >
            </div>
        </div>


        <!-- JQUERY MOBILE -->		
    </div>	
</div>
<!-- JQUERY MOBILE END-->