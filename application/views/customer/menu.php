<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<?php 
	$controller = Router::$controller;
	$action = Router::$method; 
?>

<aside class="sidebar-left hidden-xs hidden-sm">
	<?php
		require APPPATH . 'views/customer/menu_links.php';
	?>
	
	<div class="gap gap-small"></div>
	
	<?php
		require_once APPPATH . 'views/layouts/contact.php';
		require_once APPPATH . 'views/layouts/satisfaction.php';
	?>

	<div class="gap gap-small"></div>

</aside>
