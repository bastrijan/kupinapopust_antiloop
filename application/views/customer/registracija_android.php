

<div id="demo" data-role="page">
    <div data-role="header">
        <h1>Креирајте Ваш Kupinapopust профил</h1>
    </div>
    <div data-role="main" class="ui-content">

        <script type="text/javascript" src="/pub/js/pay_android.js"></script>

        <input type="hidden" name="invalidmail" id="invalidmail" value="<?php print kohana::lang("index.невалиден маил"); ?>"/>
        <input type="hidden" name="defaulttext" id="defaulttext" value="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>"/>


        <div data-role="popup" id="invalidmailPopup" class="ui-content">
            <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
            <?php print kohana::lang("index.невалиден маил"); ?>
        </div>


        <?php if ($displayForm) { ?>
            <form  id="paymentform" method="post" action="" data-ajax="false">
            <?php } ?>
            <div class="section-form">
                <?php if (isset($error)) { ?>
                    <div style="color: red; padding: 5px">
                        <?php print $error; ?>
                    </div>
                <?php } ?>
                <?php if (isset($success)) { ?>
                    <div style="color: green; padding: 5px">
                        <?php print $success; ?>
                    </div>
                <?php } ?>

                <?php if ($displayForm) { ?>
                    <input type="text" value="" id="Email" name="Email" placeholder="<?php print kohana::lang("customer.Вашиот Е-mail"); ?>" class="">
                    <input type="password" value="" id="password" name="password" placeholder="<?php print kohana::lang("customer.Вашата лозинка"); ?>" class="">
                    <input type="password" value="" id="repeat_password" name="repeat_password" placeholder="<?php print "Повторете лозинка"; ?>" class="">

                    <p class="date_description">Почитувани,<br/>
                        Внесете го датумот на Вашиот <strong>следен роденден</strong> 
                        (не датумот на раѓање). Датумот нема да биде даден на трети лица, а ќе се користи во нашиот систем за наградување.
                    </p>
<!--                    <fieldset>
                        <input id="input_01" class="datepicker" name="date" type="date" placeholder="Избере дата..." autofocuss valuee="" data-valuee="">
                    </fieldset>
                    <script>
                        var $input = $('.datepicker').pickadate({
                            monthsFull: ['Јануари', 'Февруари', 'Март', 'Април', 'Мај', 'Јуни', 'Јули', 'Август', 'Септември', 'Октомври', 'Ноември', 'Декември'],
                            weekdaysShort: ['Н', 'П', 'В', 'С', 'Ч', 'П', 'С'],
                            firstDay: 1,
                            selectYears: true,
                            selectMonths: true,
                            clear: 'избриши',
                            formatSubmit: 'yyyy-mm-dd',
                            hiddenSuffix: '_submit',
                            onSet: function (date) {
                                $("#date_selected").val($("#input_01_hidden").val());
                            }

                        });

                        var picker = $input.pickadate('picker');

                    </script>-->    
                   <script type="text/javascript">
                        $(document).on( "pagecreate", "#demo", function() {
                                SyntaxHighlighter.all();
                                var picker = $( "#date_selected", this );

                                picker.mobipick();

        //			picker.on( "change", function() {
        //					var date = $( this ).val();
        //
        //					// formatted date					
        //					var dateObject = $( this ).mobipick( "option", "date" );
        //			});
                        });
                </script>
                    <input type="text" id="date_selected" name="date_selected" min="<?php echo date("Y");?>" max="<?php echo (date("Y") + 2);?>" />
                        
                    <input type="checkbox" name="opsti_odredbi_soglasnost" <?php if (isset($post["opsti_odredbi_soglasnost"]) && $post["opsti_odredbi_soglasnost"] == "1") echo "checked"; ?> value="1" id="opsti_odredbi_soglasnost" class="custom" />
                    <label for="opsti_odredbi_soglasnost">Се согласувам со Општите одредби на веб страната.</label>
                    <input type="submit" value="<?php print "Регистрирај се"; ?>" class="submit" id="pay" >

                <?php } ?>

            </div>
            <?php if ($displayForm) { ?>
            </form>
        <?php } ?>


    </div>
</div>