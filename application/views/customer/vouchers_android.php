<!-- JQUERY MOBILE -->
<div data-role="page">
  <div data-role="header">
    <h1>Последни ваучери</h1>
  </div>
  <div data-role="main" class="ui-content">
<!-- JQUERY MOBILE END-->


		<?php
		foreach ($vouchers as $voucher) {
									//                            var_dump($voucher);exit;
				$dealsModel = new Deals_Model();
				$dealData = $dealsModel->getData(array("id" => $voucher->deal_id));
				$langTitle = "title_" . $lang . "_clean";
				$dealDesc = $dealData[0]->$langTitle;
				$dealPrice = ($dealData[0]->tip == 'cena_na_vaucer' ? $dealData[0]->price_voucher : $dealData[0]->price_discount);
									//                            var_dump($dealData[0]);exit;
									
		?>
		
					<div class="deal-item">
			            
						<img src="/pub/deals/<?php print $dealData[0]->side_img ?>" alt="<?php print strip_tags($dealData[0]->$langTitle); ?>"/>
			            
						<h3><?php print strip_tags($dealDesc); ?></h3>
			            
						<div class="item-description">
							<?php 
							$points_osnova = calculate::points_osnova($dealData[0]->price_discount, $dealData[0]->price_voucher, $dealData[0]->tip_danocna_sema, $dealData[0]->ddv_stapka, $dealData[0]->tip);
							$pointsCalculated = calculate::points($points_osnova, 1);
							if($pointsCalculated > 0) {
							?> 
								<strong>
									Со купување на <?php echo ($voucher->cnt > 1 ? "овие ваучери" : "овој ваучер"); ?> добивате <?php print ($pointsCalculated * $voucher->cnt); ?> 
									<?php $pointsCalculated == 1 ? print ' поен.' : print ' поени.' ; ?>
									<?php //print kohana::lang("customer.по цена од") ; ?> <?php //print $dealPrice ; ?>
								</strong>
								<br>
							<?php 
							}
							?> 
							<?php echo ($voucher->cnt > 1 ? "Ваучерите" : "Ваучерот"); ?> можете да <?php echo ($voucher->cnt > 1 ? "ги" : "го"); ?> искористите до <?php print date("d.m.Y", strtotime($dealData[0]->valid_to)); ?>
							<?php if ($voucher->activated) { ?>
								<br>
								<input type="button" onclick="window.MyHandlerClientDisplay.ListCodes(<?php print $voucher->attempt_id?>, <?php print $voucher->deal_id?>)"  value="Прикажи <?php echo $voucher->cnt ?> ваучер<?php echo ($voucher->cnt > 1 ? "и" : ""); ?>" />
							<?php } ?>

						</div>

						<div class="clear"></div>
					</div>
		
		<?php } ?>

		<br/><br/>
		<table>
			<tbody>
				<tr>
					<td><strong><?php print kohana::lang("customer.Поени"); ?></strong></td>
				</tr>
				<tr>
					<td>
						<?php print kohana::lang("customer.Вкупно имате"); ?> <strong><?php print $points; ?></strong> <?php print kohana::lang("customer.Kupinapopust поени"); ?>
						<br>Поените можете да ги искористите најдоцна 2 месеци од денот на нивното доделување. 
					</td>
				</tr>
			</tbody>
		</table>
	
<!-- JQUERY MOBILE -->		
	</div>	
</div>
<!-- JQUERY MOBILE END-->