<script type="text/javascript">
<?php if (isset($type) and $type == 1) { ?>
    
        $(document).ready(function () {
            $.noticeAdd({
                text: "Успешно се логиравте.",
                stay: false,
                type:'notification-success'
                //                stayTime: 3000
            });
        })
<?php } ?>
</script>

<div data-role="page" id="pageone">
  <div data-role="main" class="ui-content">
	<div data-role="controlgroup" data-type="vertical">
		<p style="text-align: center">Мој "kupinapopust" профил<br/><?php echo $this->session->get("user_email"); ?></p>
		<input type="button" onclick="window.MyHandlerLogged.ClientDisplay('index_android')"  value="Почетна страна" />
		<input type="button" onclick="window.MyHandlerLogged.ClientDisplay('vouchers_android')"  value="Сите купени ваучери" />
		<input type="button" onclick="window.MyHandlerLogged.ClientPoints()"  value="Мои освоени поени" />
		<input type="button" onclick="window.MyHandlerLogged.ClientDisplay('cards_android')"  value="Kupinapopust картички" />
		
		<input type="button" onclick="window.MyHandlerLogged.ClientPersonalData()"  value="Мојот роденден" />
		
		<input type="button" onclick="window.MyHandlerLogged.ClientChangePassword()"  value="Промени лозинка" />

	</div>
		<a data-role="button" href="/customer/logout_android" data-ajax="false" >Одјави се</a>
  </div>
</div> 