<div data-role="page">

    <div data-role="header" >
        <h1>Заборавена лозинка</h1>
    </div>
    
    <div data-role="main" class="ui-content">
        <script type="text/javascript" src="/pub/js/pay_android.js"></script>

        <input type="hidden" name="invalidmail" id="invalidmail" value="<?php print kohana::lang("index.невалиден маил"); ?>"/>
        <input type="hidden" name="defaulttext" id="defaulttext" value="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>"/>    

        <div data-role="popup" id="invalidmailPopup" class="ui-content">
            <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
            <?php print kohana::lang("index.невалиден маил"); ?>
        </div>
        <div class="deal-item">

            <br/>
            <div class="item-description">
                <form  name="payForm" id="paymentform" method="post" action="">

                    <div class="section-form">
                        <?php if (isset($error)) { ?>
                            <div style="color: red; padding: 20px">
                                <?php print $error; ?>
                            </div>
                        <?php } ?>
                        <?php if (isset($success)) { ?>
                            <div style="color: green; padding: 20px">
                                <?php print $success; ?>
                            </div>
                        <?php } ?>
                        <?php if (!isset($success)) { ?>
                            <input type="text" value="" id="Email" name="Email" placeholder="<?php print kohana::lang("customer.Вашиот Е-mail"); ?>" class="">
                        <?php } ?>
                        <?php if (isset($success)) { ?>
							<!--
                            <input type="submit" value="ОК" class="submit" onclick="location.href = '/customer/login_android';return false;">
							-->
                        <?php } else { ?>
                            <input type="submit" value="Испрати" class="submit" id="pay" style="margin-right: 35px">
                        <?php } ?>
                    </div>
                </form>
            </div>
            
        </div>
        
    </div>
    
    
</div>