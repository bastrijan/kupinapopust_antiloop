<div data-role="page">
    <div data-role="header">
        <h1>Промена на лозинка</h1>
    </div>
    <div data-role="main" class="ui-content">

        <input type="hidden" name="invalidmail" id="invalidmail" value="<?php print kohana::lang("index.невалиден маил"); ?>"/>
        <input type="hidden" name="defaulttext" id="defaulttext" value="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>"/>

        <form  name="payForm" id="paymentform" method="post" action="">

            <?php if (isset($error)) { ?>
                <div style="color: red; padding: 20px">
                    <?php print $error; ?>
                </div>
            <?php } ?>
            <?php if (isset($success)) { ?>
                <div style="color: green; padding: 20px">
                    <?php print $success; ?>
                </div>
            <?php } ?>
            
            <input type="password" value="" id="password" name="password" placeholder="Старата лозинка" class="">
            <input type="password" value="" id="password1" name="password1" placeholder="Новата лозинка" class="">
            <input type="password" value="" id="password2" name="password2" placeholder="Новата лозинка - повторно" class="">
            <input type="submit" value="Зачувај" class="submit" id="pay" style="margin-right: 35px">
        </form>
    </div>
</div>