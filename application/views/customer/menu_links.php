<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<ul class="nav nav-pills nav-stacked nav-arrow">
	<li class="<?php if ($controller == 'customer' and $action == 'index' ) print "active"; ?>"  ><a  href="/customer">Почетна страна</a></li>
	<li class="<?php if ($controller == 'customer' and $action == 'vouchers' ) print "active"; ?>"  ><a  href="/customer/vouchers">Листа на купени ваучери</a></li>
	<li class="<?php if ($controller == 'customer' and $action == 'vouchers_gift' ) print "active"; ?>"  ><a  href="/customer/vouchers_gift">Листа на подарени ваучери</a></li>
	<li class="<?php if ($controller == 'customer' and $action == 'points' ) print "active"; ?>"  ><a  href="/customer/points">Мои освоени поени</a></li>
	<li class="<?php if ($controller == 'customer' and $action == 'cards' ) print "active"; ?>"  ><a  href="/customer/cards">Подарени kupinapopust картички</a></li>
	<li class="<?php if ($controller == 'customer' and $action == 'personaldata' ) print "active"; ?>"  ><a  href="/customer/personaldata">Мојот роденден</a></li>
	<li class="<?php if ($controller == 'customer' and $action == 'changepassword') print "active"; ?>"  ><a  href="/customer/changepassword">Промени лозинка</a></li>
	<li class="<?php if ($controller == 'customer' and $action == 'deactivate_profile') print "active"; ?>"  ><a  href="/customer/deactivate_profile">Деактивирање на профил</a></li>
	<li><a  href="/customer/logout/<?php print base64_encode("/customer/login?type=2"); ?>">Одјави се</a></li>
</ul>