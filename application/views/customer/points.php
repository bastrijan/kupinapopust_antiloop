<div class="row">
	<div class="col-md-4">
		<?php require_once 'menu.php'; ?>
	</div>
	
	<div class="col-md-8" id="scrollHere">
		<div class="row">
			<div class="col-md-12 bg-white my-padd">

				<h3>Мои поени (вкупен број на поени <?php print $points; ?>)</h3>
				
				<div class="gap"></div>
				
				<h4>Колку поени имам?</h4>  

				<?php
					if(count($recordsPoints) > 0) {
						foreach ($recordsPoints as $pointsRecord) {
							$nextPoints = null;
							if (($pointsRecord->remaining > 0) && (strtotime($pointsRecord->ends) > time())) {
								$nextPoints = $pointsRecord;
								break;
							}
						}
					}
				?>
				
				<p>Вкупно имате <strong><?php print $points; ?></strong> „kupinapopust“ поeн<?php if($points > 1) echo "и"; ?>. 
					<?php if (isset($nextPoints)){ ?>
						Од нив <strong><?php print $nextPoints->remaining; ?></strong> поен<?php if($nextPoints->remaining > 1) echo "и"; ?> истекува<?php if($nextPoints->remaining > 1) echo "ат"; ?> на 
						<strong>
							<?php print date("d.m.Y", strtotime($nextPoints->ends)); ?>
						</strong>
					<?php } ?>
				</p>

				<?php 
					if(count($records) > 0) {

						foreach (array_reverse($records) as $record) { 
				?>
							<p>
								<?php if ((int) $record->points >= 0) { ?>
								
									<?php 
									//ova e za istecenite poeni
									if (time() > strtotime($record->created . " +2 months")) { ?>
									
										<?php print str_replace("+", "-", $record->points); ?> поен<?php print ($record->points == 1 ? "" : "a"); ?> (<?php print str_replace("+", "-", $record->points); ?> ден.)  
										Истечени на <?php print date("d.m.Y", strtotime($record->created . " +2 months")); ?>			
										<br>
										<?php print $record->reason; ?>.
										<?php echo "Доделени на ".date("d.m.Y", strtotime($record->created)); ?> 
										
									<?php } else { ?>
									
										<?php print $record->points; ?> поен<?php print ($record->points == 1 ? "" : "a"); ?> (<?php print $record->points; ?> ден.) 
										Истекуваат на <?php print date("d.m.Y", strtotime($record->created . " +2 months")); ?>
										<br>
										<?php print $record->reason; ?>.
										<?php echo "Доделени на ".date("d.m.Y", strtotime($record->created)); ?>

										
											
										
										
									<?php } ?>
								
								<?php } // if ((int) $record->points > 0)
									  else { 
								?>
										<?php print $record->points; ?> поен<?php print ($record->points == 1 ? "" : "a"); ?> (<?php print $record->points; ?> ден.) 
										<?php print mb_strtolower($record->reason); ?>.
										<?php echo "<br>На ".date("d.m.Y", strtotime($record->created)); ?>
											
								<?php } // else OD if ((int) $record->points > 0)  ?>
							</p>
				<?php	}
					}
				?>
				
				<div class="gap"></div>
				
				<?php print $pageContent;?>
				
				<br /><br />
				
				<img src="/pub/img/poeni.jpg" alt="Poeni" />
			
			</div>
			
		</div>
		
	</div>
	
</div>
