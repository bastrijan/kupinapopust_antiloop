<script type="text/javascript">
	<?php if (isset($type) and $type == 1) { ?>
			$(document).ready(function () {
				/*
				$.noticeAdd({
					text: "Успешно се логиравте.",
					stay: false,
					type:'notification-success'
					// stayTime: 3000
				});
				*/
				$().toastmessage('showToast', {
					text     : "Успешно се логиравте.",
					sticky   : false,
					position : 'middle-center',
					type     : 'success'
				});
			})     
	<?php } ?> 
</script>

<div class="row">
	<div class="col-md-4">
		<?php require_once 'menu.php'; ?>
	</div>
	
	<div class="col-md-8" id="scrollHere">
		<div class="row">
			<div class="col-md-12 bg-white my-padd">	

				<h3>Листа на купени ваучери</h3>
					<a class="hidden-xs hidden-sm" href="http://get.adobe.com/reader/" target="_blank"><i class="fa fa-file-pdf-o"></i> Кликнете за да симнете PDF читач</a>
					<div class="gap hidden-xs hidden-sm"></div>
					
					<?php 
						$dealsModel = new Deals_Model();
						
					foreach ($vouchers as $voucher) {
					    $where_options_str = "d.id = $voucher->deal_id AND do.id = $voucher->deal_option_id";
					    $dealData = $dealsModel->getDealOptionsData($where_options_str);
					    
						$langTitle = "title_" . $lang . "_clean";
						$dealDesc = $dealData[0]->$langTitle;
						$dealPrice = ($dealData[0]->tip == 'cena_na_vaucer' ? $dealData[0]->price_voucher : $dealData[0]->price_discount);
						
						$list_voucher_code = $voucher->list_voucher_code;
					?>
					
						<div class="row border-bottom hidden-xs hidden-sm">
							<div class="col-md-4 pl0">
								<!--
								<a href="#">
								-->
								<img src="/pub/deals/<?php print $dealData[0]->side_img; ?>" class="img-responsive" />
								<!--
								</a>
								-->
								<?php if ($voucher->activated) { ?>
									<a href="/customer/printvoucher/<?php print $voucher->first_voucher_id; ?>" target="_blank" class="prevzemiVaucher">
										<i class="fa fa-file-pdf-o"></i> ПРЕВЗЕМИ <?php echo $voucher->cnt; ?> ВАУЧЕР<?php echo ($voucher->cnt > 1 ? "И" : ""); ?>
									</a>
								<?php } ?>
							</div>
							<div class="col-md-8">
								</p>
								<p>
									<?php print strip_tags($dealDesc); ?> 
								<?php 
									$points_osnova = calculate::points_osnova($dealData[0]->price_discount, $dealData[0]->price_voucher, $dealData[0]->tip_danocna_sema, $dealData[0]->ddv_stapka, $dealData[0]->tip);
									$pointsCalculated = calculate::points($points_osnova, 1);
									
									if($pointsCalculated > 0) {
								?> 
										<p>
											Со купување на <?php echo ($voucher->cnt > 1 ? "овие ваучери" : "овој ваучер"); ?> добивате <?php print ($pointsCalculated * $voucher->cnt); ?> 
											<?php $pointsCalculated == 1 ? print '"kupinapopust" поен.' : print kohana::lang("index.Kupinapopust поени") ; ?>
										</p>
								<?php
									}
								?>
								<p>
									<?php echo ($voucher->cnt > 1 ? "Ваучерите" : "Ваучерот"); ?> можете да <?php echo ($voucher->cnt > 1 ? "ги" : "го"); ?> искористите до <?php print date("d.m.Y", strtotime($dealData[0]->valid_to)); ?>
								</p>

								<span class=" bold">
									Искористени: <?php echo $voucher->cnt_used."/".$voucher->cnt; ?>
								</span>
								<br />

								<!--
								<a href="http://get.adobe.com/reader/" target="_blank"><i class="fa fa-file-pdf-o"></i> Кликнете за да симнете PDF читач</a>
								-->
							</div>
						</div>
						
						<div class="col-md-12 mb20 border-bottom hidden-md hidden-lg mt20">
							<img src="/pub/deals/<?php print $dealData[0]->side_img; ?>" class="img-responsive mb20" />
							<a class="prevzemiVaucher togle-vaucher"><i class="fa fa-plus-square"></i> ПОГЛЕДНИ <?php echo $voucher->cnt; ?> ВАУЧЕР<?php echo ($voucher->cnt > 1 ? "И" : "");?></a>
							<div class="togle mt20" style="display: none;">
								<h4 class="bolder">Ова <?php echo ($voucher->cnt > 1 ? "се моите ваучери" : "е мојот ваучер"); ?>:</h4>
								Услугата: <?php print strip_tags($dealDesc); ?>
								<br />
								<b>Единствен код: <?php print $list_voucher_code; ?></b>
								<br />

								<span class=" bold">
									Искористени: <?php echo $voucher->cnt_used."/".$voucher->cnt; ?>
								</span>
								<br />
								
								E-mail: <?php echo $voucher->email; ?>
								<br />
								Купен на: <?php echo date("d.m.Y", strtotime($voucher->time)); ?>
								<br />
								Може да се искористи до: <?php echo date("d.m.Y", strtotime($dealData[0]->valid_to)); ?>
								<br />
								Цена на попуст:  <?php if ($dealData[0]->tip == 'cena_na_vaucer') print $dealData[0]->price_voucher; else print $dealData[0]->price_discount; ?>&nbsp;<?php print kohana::lang("prevod.ден"); ?>

								<br />


								<?php 
									  $arr_used = explode("|", $voucher->list_used);

									  $arr_valid_to = explode("|", $voucher->list_valid_to);

									  $arr_voucher_code = explode("; ", $list_voucher_code);
									  foreach ($arr_voucher_code AS $voucher_code_key => $voucher_code_value) { 
								?>
										<img  src="/customer/qrCodeGenerator/<?php print $voucher_code_value; ?>" style="width: 150px"  /> 
										<br/>
										&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $voucher_code_value; ?>
										 
										<?php 
											//proveri dali e iskoristen
											$iskoristen_ili_izminat = "";

											$iskoristen_ili_izminat = ($arr_used[$voucher_code_key] == 1) ? '&nbsp;&nbsp;<span class="error_msg bold">Искористен</span>' : "";

											// if($iskoristen_ili_izminat == "")
											// 	$iskoristen_ili_izminat = (time() > strtotime($arr_valid_to[$voucher_code_key]) ) ? '&nbsp;&nbsp;<span class="error_msg bold">Истечен</span>' : "";

											echo $iskoristen_ili_izminat; 
										?>

										<br/><br/>
								<?php } ?>
								
							</div>

						</div>
					
					<?php
					}
					?>

					<h4>Колку поени имам?</h4>
					<p>
						<?php print kohana::lang("customer.Вкупно имате"); ?> <strong><?php print $points; ?></strong> <?php print kohana::lang("customer.Kupinapopust поени"); ?>
						<br />
						Поените можете да ги искористите најдоцна 2 месеци од денот на нивното доделување.
					</p>
					
					<div class="gap"></div>

            </div>
			
        </div>

        <div class="gap"></div>

    </div>
	
</div>