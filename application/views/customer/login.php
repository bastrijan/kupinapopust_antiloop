<script type="text/javascript" src="/pub/js/common_functions.js"></script>
<script type="text/javascript" src="/pub/js/pay.js?2"></script>
<script type="text/javascript">
<?php if (isset($type) and $type == 2) { ?>
    $(document).ready(function () {

		$().toastmessage('showToast', {
			text     : "Успешно се одјавивте.",
			sticky   : false,
			position : 'middle-center',
			type     : 'success'
		});
    }) 
<?php } ?>     


<?php if (isset($type) and $type == 3) { ?>
    $(document).ready(function () {

		$().toastmessage('showToast', {
			text     : "Успешно го деактивиравте вашиот профил.",
			sticky   : true,
			position : 'middle-center',
			type     : 'success'
		});
    }) 
<?php } ?> 


<?php if (isset($type) and $type == 4) { ?>
    $(document).ready(function () {

		$().toastmessage('showToast', {
			text     : "Профилот е деактивиран!<br/>Ве молиме контактирајте нè доколку сметате дека е настаната грешка.",
			sticky   : true,
			position : 'middle-center',
			type     : 'error'
		});
    }) 
<?php } ?> 

</script>

<div class="row">
	<div class="col-md-8 ">
	   <div class="row">
			<div class="col-md-12">
		
			   <!-- LOGIN REGISTER LINKS CONTENT -->
				<div class="col-md-8 linija-right">
					<h3>Кориснички профил - најава</h3>
					<h5>Најавете се на Вашиот Kupinapopust профил</h5>
					<div class="gap"></div>
							
					<?php if (isset($error)) { ?>
						<div style="color: red;">
							<?php print $error; ?>
						</div>
					<?php } ?>
							

					<form class="dialog-form" name="payForm" id="paymentform" method="post" action="">
						<input type="hidden" name="invalidmail" id="invalidmail" value="<?php print kohana::lang("index.невалиден маил"); ?>"/>
						<input type="hidden" name="defaulttext" id="defaulttext" value="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>"/>
						
						<div class="form-group">
							<label for="Email"><?php print kohana::lang("customer.Вашиот Е-mail"); ?></label>
							<input type="text" value="" id="Email" name="Email" placeholder="email@domain.com" class="form-control" />
						</div>
						<div class="form-group">
							<label for="password"><?php print kohana::lang("customer.Вашата лозинка") ; ?></label>
							<input type="password" value="" id="password" name="password" placeholder="Мојата лозинка" class="form-control" />
						</div>
						<!-- <div class="checkbox">
							<label>
								<input type="checkbox">Remember me
							</label>
						</div>-->
						<div class="dialog-mytext">
							<label>Лозинката е испратена на регистрираниот е-mail при вашето прво купување. Доколку сеуште немате купено ниту еден ваучер, можете да креирате ваш кориснички профил со кликање на опцијата “Регистрирај се“.</label>
						</div>
						<div class="form-group">
							<input type="submit" value="<?php print kohana::lang("customer.Логирај се") ; ?>" id="pay" class="btn btn-primary btn-lg" />  
						</div>
						<div class="form-group">
							<a href="/customer/resetpassword" class="btn btn-primary">Заборавена лозинка?</a>
							<div class="gap-ponuda hidden-md hidden-lg hidden-sm"></div>  
							<a href="/customer/registracija" class="btn green" >Регистирај се</a>
						</div>
					</form>
					<div class="gap"></div>  

				</div>

				<div class="col-md-4 pl30">
					<h3>Имате Facebook профил?</h3>
					<div class="gap"></div>
					<!--
					<img src="/pub/img/fbLogin.png" style="width:167px" />
					-->
					<div id="LoginButton">
						<div class="fb-login-button" onlogin="javascript:CallAfterLogin();" size="xlarge" scope="email" style="padding: 0 10px; margin-top: 5px"></div>
					</div>
					<div id="results" style="padding: 0 10px; margin-top: 5px"></div>
				</div>

			</div>
	 
		</div>
		
		<div class="gap"></div>

	</div>

	<div class="col-md-4">
		<aside class="sidebar-left">        
			<?php
				require_once APPPATH . 'views/layouts/contact.php';
				require_once APPPATH . 'views/layouts/satisfaction.php';
			?>
		</aside>
	</div>

</div>

