<div class="row">
	<div class="col-md-4">
		<?php require_once 'menu.php'; ?>
	</div>
	
	<div class="col-md-8" id="scrollHere">
		<div class="row">
			<div class="col-md-12 bg-white my-padd">

				<h3>Мојот роденден</h3>
				
				<div class="gap"></div>

				<?php if (isset($error)) { ?>
					<div style="color: red; padding: 20px">
						<?php print $error; ?>
					</div>
				<?php } ?>
				<?php if (isset($success)) { ?>
					<div style="color: green; padding: 20px">
						<?php print $success; ?>
						<p style="text-align: left">
							<span>
								<input class="btn btn-primary" type="button" value="OK" onclick="location.href = '/customer'; return false;">
							</span>
						</p>
					</div>
				<?php } ?>

				<?php if ($displayForm) { ?>
					<form id="birthdayform" method="post" action="">
					<!--
						<script>
							$(function() {
								$( "#datepicker" ).datepicker({
									changeMonth: true,
									changeYear: true,
									yearRange: '<?php echo date("Y");?> : <?php echo (date("Y") + 1); ?>',
									dateFormat: 'yy-mm-dd',
									onSelect: function(date) {
										$("#date_selected").val(date);
									},
									dayNamesMin: ['Н','П','В','С','Ч','П','С'],
									firstDay: 1,
									monthNamesShort: ['Јануари', 'Февруари', 'Март', 'Април', 'Мај', 'Јуни', 'Јули', 'Август', 'Септември', 'Октомври', 'Ноември', 'Декември']
								});
							});
						</script>
					-->

						<p>Почитувани, <br />
							Внесете го датумот на Вашиот <b>следен роденден</b> (не датумот на раѓање) и кликнете "Зачувај". Датумот нема да биде даден на трети лица, а ќе се користи во нашиот систем за наградување.</p>

						<div class="col-md-12 text-center">
								<!-- NOVO -->	
								<div style="overflow:hidden;">
								    <div class="form-group">
	
								                <div id="datetimepicker_rodenden">
								                	<input type="hidden" value="" id="date_selected" name="date_selected" />
		
								        		</div>
								    </div>
								    <script type="text/javascript">
								        $(function () {
								            $('#datetimepicker_rodenden').datetimepicker({
								            	format: 'YYYY-MM-DD',
                                    			locale: 'mk',
								                inline: true,
    											minDate: '<?php echo date("Y"); ?>-01-01',
    											maxDate: "<?php echo date('Y', strtotime('+2 year')); ?>-01-01",
								            })
								            //.on("dp.change", function(ev) {
			                                //        alert($("#date_selected").val());
			                                //   });
								        });
								    </script>
								</div>
								<!-- END NOVO -->	
							
							<div class="gap"></div>
							
							<div class="col-md-12 text-center"> <input type="submit" value="Зачувај промени" class="btn btn-primary" id="pay" /></div>
							
							<div class="gap"></div>
						</div>
						<!--
						<input type="hidden" value="" id="date_selected" name="date_selected"/>
						-->
					</form>
					<?php
				}
				?>
				<?php
				if (!isset($success) and !isset($error)) {
					?>
					<p>Вие го имате одбрано <?php print date("d.m.Y", strtotime($user->birthday)); ?> за ваш роденден. Очекувајте изненадување на тој ден.</p>
					<p class="text-center border-top-green">   <img src="/pub/img/HAPPyBirthday.jpg" class="rodenden-pic" /></p>
				<?php
				}
				?>
				
				<div class="gap"></div>

			</div>
			
		</div>
	</div>

</div>