<!-- JQUERY MOBILE -->
<div data-role="page">
  <div data-role="header">
    <h1>Последни ваучери</h1>
  </div>
  <div data-role="main" class="ui-content">
<!-- JQUERY MOBILE END-->

	<?php
	if ($latestVouchers) {
		$voucher = $latestVouchers;
		$dealsModel = new Deals_Model();
		$dealData = $dealsModel->getData(array("id" => $voucher->deal_id));
		$langTitle = "title_" . $lang . "_clean";
		$dealDesc = $dealData[0]->$langTitle;
		$dealPrice = ($dealData[0]->tip == 'cena_na_vaucer' ? $dealData[0]->price_voucher : $dealData[0]->price_discount);
		?>
		
		
			<div class="deal-item">
	            
				<img src="/pub/deals/<?php print $dealData[0]->side_img ?>" alt="<?php print strip_tags($dealData[0]->$langTitle); ?>"/>
	            
				<h3><?php print strip_tags($dealDesc); ?></h3>
	            
				<div class="item-description">
					<?php 
					$points_osnova = calculate::points_osnova($dealData[0]->price_discount, $dealData[0]->price_voucher, $dealData[0]->tip_danocna_sema, $dealData[0]->ddv_stapka, $dealData[0]->tip);
					$pointsCalculated = calculate::points($points_osnova, 1);
					if($pointsCalculated > 0) {
					?> 
						<strong>
							Со купување на <?php echo ($voucher->cnt > 1 ? "овие ваучери" : "овој ваучер"); ?> добивате <?php print ($pointsCalculated * $voucher->cnt); ?> 
							<?php $pointsCalculated == 1 ? print ' поен.' : print ' поени.' ; ?>
							<?php //print kohana::lang("customer.по цена од") ; ?> <?php //print $dealPrice ; ?>
						</strong>
						<br>
					<?php 
					}
					?> 
					<?php echo ($voucher->cnt > 1 ? "Ваучерите" : "Ваучерот"); ?> можете да <?php echo ($voucher->cnt > 1 ? "ги" : "го"); ?> искористите до <?php print date("d.m.Y", strtotime($dealData[0]->valid_to)); ?>
					<?php if ($voucher->activated) { ?>
						<br>
						<input type="button" onclick="window.MyHandlerClientDisplay.ListCodes(<?php print $voucher->attempt_id?>, <?php print $voucher->deal_id?>)"  value="Прикажи <?php echo $voucher->cnt ?> ваучер<?php echo ($voucher->cnt > 1 ? "и" : ""); ?>" />
					<?php } ?>

				</div>

				<div class="clear"></div>
			</div>
		
		<?php
		//                                
	}
	?>
	
	
	
	<table>
		<tbody>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>               
			<tr>
				<td colspan="2">
					<strong>Колку поени имам?</strong>
				</td>
			</tr>
			<tr>
				<td colspan="2">
						<?php
						if(count($recordsPoints) > 0) {
							foreach ($recordsPoints as $pointsRecord) {
								$nextPoints = null;
								if ( ($pointsRecord->remaining > 0) && (strtotime($pointsRecord->ends) > time()) ) 
								{
									
									$nextPoints = $pointsRecord;
									break;
								}
								//var_dump($nextPoints);exit;
							}
						}
						?>
						Вкупно имате <strong><?php print $points; ?></strong> „kupinapopust“ поeн<?php if($points > 1) echo "и"; ?>. 
						<?php if (isset($nextPoints)) { ?>
							Од нив <strong>
								<?php print $nextPoints->remaining; ?>
							</strong> поен<?php if($nextPoints->remaining > 1) echo "и"; ?> истекува<?php if($nextPoints->remaining > 1) echo "ат"; ?> на 
							<strong>
								<?php print date("d.m.Y", strtotime($nextPoints->ends)); ?>
							</strong>
						<?php } ?>
						<br>Поените можете да ги искористите најдоцна 2 месеци од денот на нивното доделување. 
					</td>
				</tr>
			</tbody>
		</table>
		<br/>
		<strong>Преглед на подарени „kupinapopust“ електронски картички:</strong>
		
		<?php
		if ($latestCards) {
			
			$voucher = $latestCards;
			$dealsModel = new Deals_Model();
			$dealData = $dealsModel->getData(array("id" => $voucher->deal_id));
			$langTitle = "title_" . $lang;
			$dealDesc = $dealData[0]->$langTitle;
			$dealPrice = ($dealData[0]->tip == 'cena_na_vaucer' ? $dealData[0]->price_voucher : $dealData[0]->price_discount);
		?>
		
			<div class="deal-item">
				<img src="<?php echo Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL');?>/pub/img/layout/podarok_karticka.png" />
				<div class="item-description">
					Подаривте "kupinapopust" картичка во вредност од <?php print strip_tags($dealDesc); ?> 
					<?php 
						$points_osnova = calculate::points_osnova($dealData[0]->price_discount, $dealData[0]->price_voucher, $dealData[0]->tip_danocna_sema, $dealData[0]->ddv_stapka, $dealData[0]->tip);
						$pointsCalculated = calculate::points($points_osnova, 1);
						if($pointsCalculated > 0) {
					?> 
							<strong>
								<br>
									Со купување на <?php echo ($voucher->cnt > 1 ? "овие ваучери" : "овој ваучер"); ?> добивате <?php print ($pointsCalculated * $voucher->cnt); ?> 
									<?php $pointsCalculated == 1 ? print ' поен.' : print ' поени.' ; ?>
								<?php //print kohana::lang("customer.по цена од") ; ?> <?php //print $dealPrice ; ?>
							</strong>
					<?php 
						}
					?> 
					<?php if ($voucher->activated) { ?>
						<br>
						<input type="button" onclick="window.MyHandlerClientDisplay.ListCodes(<?php print $voucher->attempt_id?>, <?php print $voucher->deal_id?>)"  value="Прикажи <?php echo $voucher->cnt ?> ваучер<?php echo ($voucher->cnt > 1 ? "и" : ""); ?>" />
					<?php } ?>

				</div>

				<div class="clear"></div>
			</div>
		
		
																		
	<?php
		}

	?>
																

		
<!-- JQUERY MOBILE -->		
	</div>	
</div>
<!-- JQUERY MOBILE END-->