<div class="row">
	<div class="col-md-4">
		<?php require_once 'menu.php'; ?>
	</div>
	
	<div class="col-md-8" id="scrollHere">
		<div class="row">
			<div class="col-md-12 bg-white my-padd">

				<h3>Промени лозинка</h3>
				
				<div class="gap"></div>
				
				<form name="payForm" id="paymentform" method="post" action="">
					<input type="hidden" name="invalidmail" id="invalidmail" value="<?php print kohana::lang("index.невалиден маил"); ?>"/>
					<input type="hidden" name="defaulttext" id="defaulttext" value="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>"/>
					
					<div class="form-group">
						<label for="">Старата лозинка</label>
						<input type="password" value="" id="password" name="password" value="" class="form-control">
					</div>
					<div class="form-group">
						<label for="">Новата лозинка</label>
						<input type="password" value="" id="password1" name="password1" class="form-control">
					</div>
					<div class="form-group">
						<label for="">Новата лозинка - повторно</label>
						<input type="password" value="" id="password2" name="password2" class="form-control">
					</div>
						<div class="form-group">
						<label for="" class="text-green"><!-- Лозинката е успешно променета! -->
							<?php if (isset($error)) { ?>
								<div style="color: red;">
									<?php print $error; ?>
								</div>
							<?php } ?>
							<?php if (isset($success)) { ?>
									<?php print $success; ?>
							<?php } ?>
						</label>
					</div>
					<input type="submit" value="Зачувај промени" class="btn btn-primary">
				</form>

			</div>
			
		</div>
		
	</div>
	
</div>