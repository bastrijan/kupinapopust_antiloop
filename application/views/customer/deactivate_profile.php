<div class="row">
	<div class="col-md-4">
		<?php require_once 'menu.php'; ?>
	</div>
	
	<div class="col-md-8" id="scrollHere">
		<div class="row">
			<div class="col-md-12 bg-white my-padd">

				<h3>Деактивирање на профил</h3>
				
				<div class="gap"></div>

				<h4>
					Почитувани, со деактивирање на вашиот профил: <br/>
					1. Нема повеќе да добивате понуди (newsletter) на вашата e-mail адреса! <br/>
					2. Ќе бидете одјавени од вашиот профил и повеќе нема да може да се најавувате. <br/><br/>

					Доколку сте сигурни дека сепак сакате да го деактивирате вашиот профил притиснете на копчето подолу.  <br/><br/>

					При повторно купување на ваучер преку нашата веб страна и внесување на истиот е-маил, Вашиот профил автоматски повторно ќе се активира. 
					<!-- Истото ќе се случи и доколку го завртите “Тркалото на среќата“. -->
				</h4>
				
				<br/>
				
				<form name="payForm" id="paymentform" method="post" action="" onsubmit="return confirm('Дали навистина сакате да го деактивирате вашиот профил?');">
				
					<input type="submit" value="Деактивирај профил" class="btn btn-primary">
				</form>

			</div>
			
		</div>
		
	</div>
	
</div>