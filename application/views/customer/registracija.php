<script type="text/javascript" src="/pub/js/common_functions.js"></script>
<script type="text/javascript" src="/pub/js/pay.js?2"></script>
<script type="text/javascript">
	<?php if (isset($type) and $type == 2) { ?>
	    $(document).ready(function () {
			/*
			$.noticeAdd({
				text: "Успешно се одјавивте.",
				stay: false,
				type:'notification-success'
				// stayTime: 3000
			});
			*/
			$().toastmessage('showToast', {
				text     : "Успешно се одјавивте.",
				sticky   : false,
				position : 'middle-center',
				type     : 'success'
			});
	    })     
	<?php } ?>     
</script>

<style type="text/css">

    input[type=checkbox] {
      /* All browsers except webkit*/
      transform: scale(2);

      /* Webkit browsers*/
      -webkit-transform: scale(2);
    }

</style>

<div class="row">
	<div class="col-md-8 ">
	   <div class="row">
			<div class="col-md-12">

				<!-- LOGIN REGISTER LINKS CONTENT -->

				<!-- 
					vaka treba da bide DIV-ot koga ke se prikazuva opcijata za registracija so facebook
					<div class="col-md-8 linija-right"> 
				-->

				<div class="col-md-12 linija-right">
					<h3><?php print "Кориснички профил - регистрација"; ?><!-- Кориснички профил - регистрација --></h3>
					<h5>Креирајте Ваш Kupinapopust профил</h5>
					
					<?php if (isset($error)) { ?>
						<p style="color: red">
							<?php print $error; ?>
						</p>
					<?php } ?>
					<?php if (isset($success)) { ?>
						<p style="color: green">
							<?php print $success; ?>
						</p>
					<?php } ?>
			
					<?php if ($displayForm) {?>
						<form  name="payForm" id="paymentform" method="post" action="" class="dialog-form">
						
							<input type="hidden" name="invalidmail" id="invalidmail" value="<?php print kohana::lang("index.невалиден маил"); ?>"/>
							<input type="hidden" name="defaulttext" id="defaulttext" value="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>" />
							
							<div class="form-group">
								<label for="Email"><?php print kohana::lang("customer.Вашиот Е-mail"); ?><!-- E-mail --></label>
								<input type="text" value="<?php if (isset($post["Email"])) echo $post["Email"]; ?>" id="Email" name="Email" placeholder="email@domain.com" class="form-control" />
							</div>
							
							<div class="form-group">
								<label for="password"><?php print kohana::lang("customer.Вашата лозинка"); ?><!-- Лозинка --></label>
								<input type="password" value="" id="password" name="password" placeholder="Мојата лозинка" class="form-control" />
							</div>
							
							<div class="form-group">
								<label for="repeat_password">Повторете лозинка</label>
								<input type="password" value="" id="repeat_password" name="repeat_password" placeholder="Мојата лозинка повторно" class="form-control">							
							</div>
							
							<div class="form-group">
								<label>Датум на роденден</label><br />
								<!--
								<script>
									$(function() {
										$( "#datepicker" ).datepicker({
											changeMonth: true,
											changeYear: true,
											yearRange: '<?php echo date("Y");?> : <?php echo (date("Y") + 1); ?>',
											dateFormat: 'yy-mm-dd',
											onSelect: function(date) {
												$("#date_selected").val(date);
											},
											dayNamesMin: ['Н','П','В','С','Ч','П','С'],
											firstDay: 1,
											monthNamesShort: ['Јануари', 'Февруари', 'Март', 'Април', 'Мај', 'Јуни', 'Јули', 'Август', 'Септември', 'Октомври', 'Ноември', 'Декември']
										});
									});
								</script>
								<div id="datepicker"></div>
								-->
								
								<!-- NOVO -->	
								<div style="overflow:hidden;">
								    <div class="form-group">
	
								                <div id="datetimepicker_rodenden">
								                	<input type="hidden" value="<?php if (isset($post["date_selected"])) echo $post["date_selected"]; ?>" id="date_selected" name="date_selected" />
		
								        		</div>
								    </div>
								    <script type="text/javascript">
								        $(function () {
								            $('#datetimepicker_rodenden').datetimepicker({
								            	format: 'YYYY-MM-DD',
                                    			locale: 'mk',
								                inline: true,
    											minDate: '<?php echo date("Y"); ?>-01-01',
    											maxDate: "<?php echo date('Y', strtotime('+2 year')); ?>-01-01",
								            })
								            //.on("dp.change", function(ev) {
			                                //        alert($("#date_selected").val());
			                                //   });
								        });
								    </script>
								</div>
								<!-- END NOVO -->	


								<p>Почитувани,
								   Внесете го датумот на Вашиот <b>следен роденден</b> (не датумот на раѓање). Датумот нема да биде даден на трети лица, а ќе се користи во нашиот систем за наградување.</p>
							</div>
					   
							<div class="checkbox">
								<label>
									<input type="checkbox" name="opsti_odredbi_soglasnost" <?php if (isset($post["opsti_odredbi_soglasnost"]) && $post["opsti_odredbi_soglasnost"] =="1") echo "checked"; ?> value="1" id="opsti_odredbi_soglasnost" />Се согласувам со <a href="/static/page/opstiodredbi" target="_blank">Општите одредби</a> на страната
								</label>
							</div>
							<input type="submit" value="Регистрирај се" class="btn btn-primary" id="pay" />
						</form>
					<?php } ?>
					
					<div class="gap"></div>      
				</div>

				<!-- 
				<div class="col-md-4 pl30">
					<h3>Имате Facebook профил?</h3>
					<div class="gap"></div>
					<div id="LoginButton">
						<div class="fb-login-button" onlogin="javascript:CallAfterLogin();" size="xlarge" scope="email" style="padding: 0 10px; margin-top: 5px"></div>
					</div>
					<div id="results" style="padding: 0 10px; margin-top: 5px"></div> 
				</div>
				-->
			</div>
	 
		</div>
		
		<div class="gap"></div>

	</div>

	<div class="col-md-4">
		<aside class="sidebar-left">        
			<?php
				require_once APPPATH . 'views/layouts/contact.php';
				require_once APPPATH . 'views/layouts/satisfaction.php';
			?>
		</aside>
	</div>
</div>