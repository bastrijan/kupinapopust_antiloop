<script type="text/javascript" src="/pub/js/common_functions.js"></script>
<script type="text/javascript" src="/pub/js/pay.js?2"></script>
<input type="hidden" name="invalidmail" id="invalidmail" value="<?php print kohana::lang("index.невалиден маил"); ?>"/>
<input type="hidden" name="defaulttext" id="defaulttext" value="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>"/>

<div class="row">
    <div class="col-md-8  col-md-push-4">
        <h1 >Заборавена лозинка</h1>

        <form  name="payForm" id="paymentform" method="post" action="">

            <div >
                <?php if (isset($error)) { ?>
                    <div style="color: red; ">
                        <?php print $error; ?>
                    </div>
                <?php } ?>
                <?php if (isset($success)) { ?>
                    <div style="color: green; ">
                        <?php print $success; ?>
                    </div>
                <?php } ?>


                <?php if (!isset($success)) { ?>
                
                <div class="form-group">
                    <label for="Email"><?php print kohana::lang("customer.Вашиот Е-mail"); ?></label>
                    <input type="text" value="" id="Email" name="Email" placeholder="email@domain.com" class="form-control" />
                </div>

                <?php } ?>

                <div class="form-group">
                    <?php if (isset($success)) { ?>
                        <input type="submit" value="ОК" class="btn btn-primary btn-lg" onclick="location.href = '/customer';return false;" />  
                    <?php } else { ?>
                        <input type="submit" value="Испрати" id="pay" class="btn btn-primary btn-lg" />  
                    <?php } ?>
                </div>                

            </div>


        </form>
    </div>


    <div class="col-md-4  col-md-pull-8">
        <?php require_once APPPATH . 'views/layouts/contact.php'; ?>
    </div>


</div>

