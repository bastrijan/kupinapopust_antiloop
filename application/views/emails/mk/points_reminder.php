<?php include 'mail_header.php'; ?>
        <p style="font-size: 12pt;font-weight: bold; color: #000000;">Почитувани,</p>
        <p style="font-size: 12pt;color: #000000;">
			 Како што веќе може и знаете, со секое купување на kupinapopust.mk, како и со вашите други активности поврзани со веб страната, добивате поени кои може да ги искористите како кредит при купување на некои од нашите актуелни понуди.<br/>
			 За таа цел би сакале да ве информираме дека дел од вашите освоени поени ви истекуваат за 5 дена.
			 Би било штета да ви пропаднат.<br/>
			 Затоа ви препорачуваме да ги проверите нашите актуелни понуди и да си изберете што најмногу ви се допаѓа.
		</p>

        <p>
			<a target="_blank" href="http://kupinapopust.mk/" style="background:#169d16;padding:10px;font-size:13px;text-decoration:none;" ><span style="color:#FFFFFF;">Проверете ги актуелните понуди</span></a>
        </p>

        <p style="font-size: 12pt;color: #000000;">Ви посакуваме пријатно купување и штедење.</p>

<?php include 'mail_footer.php'; ?>