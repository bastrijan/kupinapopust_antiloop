<?php require 'mail_header.php'; ?>

      <p style="font-size: 12pt;font-weight: bold; color: #000000;">Почитувани,</p>
      <p style="font-size: 12pt;color: #000000;">Вашиот профил е успешно реактивиран! </p>
      <p style="font-size: 12pt;color: #000000;">За да можете да се логирате на вашиот профил ви креиравме нова лозинка која ви ја праќаме подолу. После иницијалното логирање на вашиот профил можете да ја смените лозинката. <br/>
      Доколку се најавувате на профилот преку facebook тогаш лозинката не ви е потребна.
      </p>
      <p style="font-size: 12pt;color: #000000;">E-mail: <?php print $email ; ?></p>
      <p style="font-size: 12pt;color: #000000;">Лозинка: <?php print $password ; ?></p>
      <p style="font-size: 12pt;font-weight: bold;">Ви благодариме на довербата.</p>

<?php require 'mail_footer.php'; ?>