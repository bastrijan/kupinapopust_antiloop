<?php require 'mail_header.php'; ?>
          
        <p style="font-size: 12pt;font-weight: bold; color: #000000;">Трансакцијата е успешна. Ви честитаме.</p>

        <p style="font-size: 12pt;color: #000000;">Вие сте вистински пријател и избравте оригинален подарок.</p>
        <p style="font-size: 12pt;color: #000000;">Подаривте <?php echo $cnt; ?> електронск<?php echo ($cnt > 1 ? "и" : "а"); ?> “kupinapopust“ картичк<?php echo ($cnt > 1 ? "и" : "а"); ?> во <?php echo ($cnt > 1 ? "вкупна" : ""); ?> вредност од <?php print $suma*$cnt;?> ден.</p>
        <p><img src="http://kupinapopust.mk/pub/img/gift-coupon.jpg"></p>

        <?php if($copy_email == "") { ?>
          <p style="font-size: 12pt;color: #000000;">За да <?php echo ($cnt > 1 ? "ги испечатите ваучерите" : "го испечатите ваучерот"); ?> со “Единствениот код“ потребно е да се логирате во вашиот "Кориснички профил" и во делот "Листа на подарени електр. картички" да кликнете "Печати ваучер".</p>
          <p style="font-size: 12pt;color: #000000;">При најавувањето Ве молиме внесете ја вашата kupinapopust лозинка или креирајте си нова доколку сте ја заборавиле. Доколку е-мејлот со кој сте го купиле ваучерот е ист со е-мејлот што го имате на facebook, можете да се најавите преку facebook.</p>
          <p style="font-size: 12pt;color: #000000;"><a target="_blank" href="http://kupinapopust.mk/customer/login" style="background:#8cc732; padding:10px 20px 10px 20px; font-weight:bold; border-radius:5px; font-size:13px; text-transform:uppercase; text-decoration:none;"><span style="color:#FFFFFF;">Најави се</span></a></p>
        <?php } //if($copy_email == "") { ?>

<?php require 'mail_footer.php'; ?>