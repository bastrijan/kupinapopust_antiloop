<?php require 'mail_header.php' ; ?>
                  <p style="font-size: 12pt;font-weight: bold; color: #000000;">Корисник: <?php print $email ; ?></p>
                  <p style="font-size: 12pt;color: #000000;">Ви Честитаме. Резервиравте <?php echo $cnt; ?> Ваучер<?php echo ($cnt > 1 ? "и" : ""); ?> за вашата избрана понуда<?php print ($suma > 0 ? " и заштедивте ".($suma*$cnt)." денари" : "") ; ?>.</p>
                  <p style="font-size: 12pt;color: #000000;">Вашиот пријател добива:</p>
                  <p style="font-size: 12pt;font-weight: bold; color: #000000;"><?php print $cleanTitle;?></p>
                  <!--	
        				  <p>Напомена: На подарок ваучерот не е вметнато нашето лого ниту пак цената со попуст. Напишана е само вредноста на ваучерот кој се поклонува, кратко инфо за понудата и вашата посвета.</p>
                   -->
				          <p style="font-size: 12pt;color: #000000;"><?php echo ($cnt > 1 ? "Ваучерите се резервирани на" : "Ваучерот е резервиран на"); ?>:  <?php print date("d/m/Y", strtotime($time)) ; ?> во <?php print date("H:i", strtotime($time)) ; ?>ч.</p>
                  <p style="font-size: 12pt;color: #000000;">Почитувани Корисници,</p>
                  <p style="font-size: 12pt;color: #000000;">Со цел да ви обезбедиме пофлексибилно плаќање, ја имате можноста да ја платите понудата преку уплатница во банка со пополнување на ПП 10 образец.                  </p>
                  <p><img border="0" src="http://kupinapopust.mk/pub/img/layout/uplatnica_cache_small.png"></p>
                  <p style="font-size: 12pt;color: #000000;">По извршената уплата ве молиме информирајте не преку е-mail на contact@kupinapopust.mk за да го евидентираме вашето плаќање.</p>
                  <p style="font-size: 12pt;color: #000000;">Потребни ни се:</p>
                  <p style="font-size: 12pt;color: #000000;">1. Вашето Име и Презиме</p>
                  <p style="font-size: 12pt;color: #000000;">2.  За која понуда сте уплатиле </p>
                  <p style="font-size: 12pt;color: #000000;">3. Сумата која сте ја уплатиле</p>
                  <p style="font-size: 12pt;color: #000000;">Обично се потребни 1-2 дена за да се изврши преносот на пари. Затоа ве молиме за трпеливост. Доколку ни испратите скенирана копија од уплатницата по e-mail, тогаш веднаш ви го испраќаме ваучерот.</p>

<?php require 'mail_footer.php' ; ?>
             