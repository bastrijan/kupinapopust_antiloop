<?php require 'mail_header.php' ; ?>

          <p style="font-size: 12pt;font-weight: bold; color: #000000;">Почитувани соработници,</p>
          <p style="font-size: 12pt;color: #000000;">
          	На нашата веб страна започна со промоција и продажба следнава понуда:
          </p>	
                  
		  <p style="font-size: 12pt;color: #000000;"><?php print $deal_name ; ?> </p>
				  


		  <p style="font-size: 12pt;color: #000000; color:red">
			Напомена:<br/>
			Ве молиме не услужувајте корисници без уредно издаден ваучер.

		  </p>

		  <p style="font-size: 12pt;color: #000000;">
				  За следење на продажбата на ваучерите, како и нивно маркирање како искористени, Ве молиме најавете се на следниов линк: <a href="https://kupinapopust.mk/partner/">Kupinapopust партнер</a>
				  <br/><br/>

				  Корисничко име:  <?php print $partner_username ; ?><br/>
				  Лозинка: <?php print $partner_password ; ?>


		  </p>	

		  <p style="font-size: 12pt;color: #000000;">
				Како да означите ваучер како искористен од мобилен телефон? 
				<br/><br/>
				1. Најпрво инсталирајте мобилна апликација која дозволува бар код скенирање (QR Code Reader) или пак користете го QR код читачот на Viber. 
				<br/>
				<a href="https://play.google.com/store/apps/details?id=com.google.zxing.client.android">Превземи</a> QR код читач за Android телефон 
				<br/><br/>
				2. Отворете ја апликацијата и скенирајте го кодот од ваучерот. 
				<br/><br/>
				3. Ако е ваучерот валиден, означето го како искористен со кликање на зеленото копче. 
				<br/><br/>
				4. Услужете го корисникот! 
				<br/>
		  </p>	

          <p style="font-size: 12pt;color: #000000;">Со почит,<br/>
        	Тимот на kupinapopust.mk
          </p>


	       <span style=" width:600px; margin-left:auto; margin-right:auto; padding:10px 30px 10px 30px;  background-color: #FFFFFF; font-family:'Open Sans', Roboto, Arial; ">
	         <p style="font-size:11px; line-height:16px; color: #000000;">
				Copyright © Kupinapopust.mk, Груп Поинт дооел, ул.Никола Вапцаров бр.3/1, реон 8, нас. Центар, 1000, Скопје, Македонија, e-mail: contact@kupinapopust.mk Фиксен тел.: 02 3 256 027, моб. VIP 078 439 829 (достапни секој работен ден од 09:00 до 17:00 часот)
			 </p>
	       </span>

		  <!-- ANGLISKA VERZIJA -->
          <p style="font-size: 12pt;font-weight: bold; color: #000000;">Dear partners,</p>
          <p style="font-size: 12pt;color: #000000;">
          	We have started with promotion and sales of your offer:
          </p>	
                  
		  <p style="font-size: 12pt;color: #000000;"><?php print $deal_name ; ?> </p>
				  


		  <p style="font-size: 12pt;color: #000000; color:red">
			Attention:<br/>
			Please do not serve clients without properly issued coupon.
						
		  </p>

		  <p style="font-size: 12pt;color: #000000;">
				  For following the sales of the coupons as well as their marking as used, please log-in here:  <a href="https://kupinapopust.mk/partner/">Kupinapopust partner</a>
				  <br/><br/>

				  Username:  <?php print $partner_username ; ?><br/>
				  Password: <?php print $partner_password ; ?>


		  </p>	

		  <p style="font-size: 12pt;color: #000000;">
				How to mark a coupon as used using a mobile phone?
				<br/><br/>
				1. Install the following <a href="https://play.google.com/store/apps/details?id=com.google.zxing.client.android">app</a> on your Android smartphone.
				<br/><br/>
				2. Open the app and scan the coupon.
				<br/><br/>
				3. If the coupon is valid, mark it as used one by clicking the green button. 
				<br/><br/>
				4. Serve the customer. 
				<br/>
		  </p>	

          <p style="font-size: 12pt;color: #000000;">Best regards,<br/>
        	Kupinapopust team
          </p>

	       <span style=" width:600px; margin-left:auto; margin-right:auto; padding:10px 30px 10px 30px;  background-color: #FFFFFF; font-family:'Open Sans', Roboto, Arial; ">
	         <p style="font-size:11px; line-height:16px; color: #000000;">
				Copyright © Kupinapopust.mk, Grup Point dooel, str.Nikola Vapcarov no.3/1, region 8, Centar area, 1000, Skopje, Macedonia, e-mail: contact@kupinapopust.mk, tel. 00389 2 3 256 027, 00389 2 3 215 988, mob. 00389 71 315 460, 
				(available every working day between 09:00h. and 17:00h.)
			 </p>
	       </span>

	</div>
</body>
</html>