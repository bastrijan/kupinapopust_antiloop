<?php require 'mail_header.php' ; ?>

  <p style="font-size: 12pt;font-weight: bold; color: #000000;">Почитувани,</p>
  
  <p style="font-size: 12pt;color: #000000;">Побаравте промена на лозинката преку нашиот систем.</p>
  <p style="font-size: 12pt;color: #000000;">Доколку сеуште сакате да ја смените лозинката, ве молиме следете го следниот линк, во спротивно, игнорирајте го овој e-mail.</p>
  <p style="font-size: 12pt;color: #000000;"><a href='<?php echo 'http://' . $_SERVER["HTTP_HOST"] . '/';?>customer/choosepassword/<?php print $username ; ?>/<?php print $hash ; ?>' target='_blank'>Промена на лозинка</a></p>

<?php require 'mail_footer.php' ; ?>
