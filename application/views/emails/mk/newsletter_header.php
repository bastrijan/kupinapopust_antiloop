<?php 
$webSiteURL = Kohana::config('facebook.facebookSiteProtocol').'://' . Kohana::config('facebook.facebookSiteURL');

$ref_add = isset($ref) && $ref > 0 ? "?ref=".$ref : "";
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>
<body style="background-color:#e6e4e5; padding-top:15px">


    <div style="width: 600px; margin-left: auto; margin-right: auto; background-color:white; border-top:5px solid #f60;">
        <a target="_blank" href="<?php echo $webSiteURL.$ref_add;?>" style="display: block; padding-left:20px;">
            <img src="http://kupinapopust.mk/pub/img/logo5.png" height="62" />
        </a>

        <p style="padding-left: 20px; font-size: 16px; height: 20px; font-family: Roboto, 'Open Sans', Arial;">Активни понуди - <?php print date("d.m.Y"); ?> - Скопје
			<?php 
				if(isset($site_nasi_ponudi_on_top) && $site_nasi_ponudi_on_top == "yes") { 
			?>
				<a target="_blank" style="float: right; background: #3FA80A; padding: 5px 10px 5px 10px; text-decoration: none; color: #FFFFFF; text-transform: uppercase; margin-right:15px;" href="<?php echo $webSiteURL;?>/all<?php echo $ref_add; ?>">Сите наши понуди (<?php echo $aktivni_ponudi_cnt; ?>)
				</a>
			<?php
				}
			?>
        </p>
		
        <p style="padding-left: 20px; padding-bottom: 10px; font-size: 16px; font-family: Roboto, 'Open Sans', Arial; margin-bottom: 0px;">
            Следете нѐ на: &nbsp;&nbsp;
		
			<a href="https://www.facebook.com/Kupinapopust.mk" target="_blank" class="icon-facebook">
                <img align="center" src="<?php echo $webSiteURL;?>/pub/imgZaMail/fbround25x25.png"></a>
            &nbsp;
			<a href="https://twitter.com/kupinapopust" target="_blank" class="icon-twitter">
                <img align="center" src="<?php echo $webSiteURL;?>/pub/imgZaMail/tw25x25.png"></a>
            &nbsp;
			<a href="https://plus.google.com/u/0/105277563712882027312/posts" target="_blank">
                <img align="center" src="<?php echo $webSiteURL;?>/pub/imgZaMail/gplus25x25.png"></a>
			
			<!-- /////////// Превземи мобилна апликација: /////////// -->
			<!--
            <span  style="font-family: Roboto, 'Open Sans', Arial; margin-bottom: 0px;  padding-top:10px; padding-right:20px; display:block">
                <a target="_blank" href="#" data-toggle="tooltip" data-placement="bottom" title="Превземи android апликација" style="text-decoration:none; background-color:black; color:white; margin-right:10px; padding-top:5px; padding-bottom:7px; padding-right:5px; padding-left:3px;">
					<img align="center" src="<?php echo $webSiteURL;?>/pub/img/androidIcon1.png" style="height:30px;"> Android
				</a>
                 <a target="_blank" href="#" data-toggle="tooltip" data-placement="bottom" title="Превземи iOS апликација" style="text-decoration:none; background-color:black; color:white; margin-right:10px; padding-top:5px; padding-bottom:7px; padding-right:7px;  padding-left:3px;">
					<img align="center" src="<?php echo $webSiteURL;?>/pub/img/appleIcon1.png"style="height:30px;"> Iphone
				</a>
            </span>
			-->
			
            <span  style=" font-size: 14px; font-family: Roboto, 'Open Sans', Arial; margin-bottom: 0px; padding-bottom:15px; padding-top:10px; padding-right:20px; display:block">За да бидете сигурни дека секогаш ќе ја добивате оваа e-пошта во Вашето сандаче, Ве молиме да ја додадете оваа адреса: <a href="mailto:admin@kupinapopust.mk">admin@kupinapopust.mk</a> во Вашата листа на контакти. </span>

			<?php 
				  if (isset($newsletter_name_on_top)) { 
			?>
				<span style="color: #111; font-weight: bold; font-size: 20px;"><?php print $newsletter_name_on_top; ?></span>			
			<?php } ?>
 			
        </p>
    </div>
