<?php require 'mail_header.php'; ?>

  <p style="font-size: 12px;  color: #000000; border-left:8px solid #8cc732; padding-left:5px;">Посвета: <?php print $posveta; ?></p>

  <p style="font-size: 12pt;color: #000000;">Го добивте следниот подарок:</p>
  <p style="font-size: 12pt;font-weight: bold; color: #000000;"><?php print $voucherTitle ;?></p>
  <p style="font-size: 12pt;color: #000000;"><?php echo ($cnt > 1 ? "Ваучерите се купени на" : "Ваучерот е купен на"); ?>:  <?php print date("d/m/Y", strtotime($time)) ; ?> во <?php print date("H:i", strtotime($time)) ; ?>ч.</p>
    
  <p style="font-size: 12pt;color: #000000;">За да <?php echo ($cnt > 1 ? "ги испечатите ваучерите" : "го испечатите ваучерот"); ?> Преземете го PDF документот што е додаден во прилог на оваа email порака.</p>

		<?php if ($otkup_prevzemanje > 0) { ?>
			<p style="font-size: 12pt; color: #cc0000; ">
				<?php 
					if ($otkup_prevzemanje == 1) 
					{
						echo "Ве молиме почекајте да ве информираме кога производот би бил достапен за подигнување од нашите канцеларии. Најчесто во рок од 7 до 14 дена ги имаме производите подготвени за подигнување.";
					}
					else 
					{
						echo "Производот ќе го добиете во рок наведен во понудата.<br/>";
						echo "<b>Напомена: При превземање на производот ја плаќате цената за достава (освен за некои понуди каде што доставата е бесплатна).</b>";
					}
				?>
			</p>
		<?php } ?>

<?php require 'mail_footer.php'; ?>