<?php require_once 'newsletter_header.php'; ?>

<!-- /////////////// PRIMARY OFFERS /////////////// -->
<?php foreach ($allOffers as $dealData) { 

			$title_mk_clean = "";
			$title_mk = "";
			$max_ammount = 0;
			$ceni_od_txt = "";

			if($dealData->options_cnt > 1 || $dealData->is_general_deal)
			{
				$title_mk_clean =  $dealData->title_mk_clean_deal;
				$title_mk =  $dealData->title_mk_deal;
				$max_ammount = 0;

				if($dealData->is_general_deal)
					$ceni_od_txt = "Прегледај понуди";
				else	
					$ceni_od_txt = ($dealData->finalna_cena > 0 ? "од ".$dealData->finalna_cena." ден." : "Превземи купон");
			}
			else
			{
				$title_mk_clean =  $dealData->title_mk_clean;
				$title_mk =  $dealData->title_mk;
				$max_ammount = $dealData->max_ammount;
				$ceni_od_txt = ($dealData->finalna_cena > 0 ? $dealData->finalna_cena." ден." : "Превземи купон");
			}

			$disc = 0;
			$zasteda = 0;

			if($dealData->price > 0) {
				$disc = 100 - ($dealData->price_discount / $dealData->price) * 100;

				$zasteda = (int)round($dealData->price * $disc / 100);
			}

			if($dealData->is_general_deal)
				$detali_za_ponudata = "/deal/general_deal/".$dealData->deal_id.$ref_add ;
			else
				$detali_za_ponudata = "/deal/index/".$dealData->deal_id.$ref_add ;
?>	

	<!--edna ponuda, padding bottom verojatno ne ti treba zosto ti e vo tabela-->
    <div style="width: 570px; margin-left: auto; margin-right: auto; border-top: 2px dashed #ddd; padding: 15px; padding-bottom:50px; background-color:white">

		<a target="_blank" href="<?php echo $webSiteURL.$detali_za_ponudata;?>">
			<img alt="" src="<?php echo $webSiteURL;?>/pub/deals/<?php print $dealData->main_img; ?>" style="border: 0; width: 570px; padding-bottom: 5px;">
		</a>

        <div style="text-align: left; font-size: 16px; ">
			<a style="text-decoration: none; color: #000000; font-family: Roboto, 'Open Sans', Arial; color:#333; padding-bottom:5px; line-height:20px;  font-weight:bold;" target="_blank" href="<?php echo $webSiteURL.$detali_za_ponudata;?>"><?php print strip_tags($title_mk_clean); ?>
            </a>
        </div>
		
        <div style="margin-top: 25px; margin-bottom: 5px; display: block; font-family: Roboto, 'Open Sans', Arial; ">
            <div style="float: left;">
            	<a target="_blank" href="<?php echo $webSiteURL.$detali_za_ponudata;?>">
	                <span style="font-size: 17px; font-weight: bold; color: white; background-color:#3FA80A;  padding: 7px 15px 7px 15px; border-radius:3px;">
	                		<?php print $ceni_od_txt; ?>
	                </span>
                </a>
                
				<?php 
				if( $dealData->price > 0) { 
				?>
	                &nbsp;
					<span style="text-decoration: line-through; font-weight: bold;"><?php print $dealData->price; ?><?php print kohana::lang("prevod.ден"); ?></span>
				<?php
				}
				?>
				
				<?php 
				if($zasteda > 0) { 
				?>
					&nbsp;
					<span style="font-size: 17px; font-weight: bold; color: #333;">Заштедувате <?php print $zasteda; ?>&nbsp;<?php print kohana::lang("prevod.ден"); ?></span>
				<?php
				}
				?>
				
            </div>
			
            <div style="float: right;">
				<?php if($disc > 0) { ?>		
					<span style="background: #ff6600; padding: 7px 15px 7px 15px; border-radius:3px; text-decoration: none; font-family: Roboto, 'Open Sans', Arial;   color: #FFFFFF; font-weight: bold;">-
						<strong><?php print round($disc);?></strong>
						%
					</span>
				<?php } ?>
            </div>
        </div>
    </div>
	
<?php } // END OF: foreach ($allOffers as $dealData) ?>
 

<?php require_once 'newsletter_footer.php'; ?>
