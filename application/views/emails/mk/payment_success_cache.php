<?php require 'mail_header.php' ; ?>
                  <p style="font-size: 12pt;font-weight: bold; color: #000000;">Корисник: <?php print $email ; ?></p>
                  <p style="font-size: 12pt;color: #000000;">Ви Честитаме. Резервиравте <?php echo $cnt; ?> Ваучер<?php echo ($cnt > 1 ? "и" : ""); ?> за вашата избрана понуда<?php print ($suma > 0 ? " и заштедивте ".($suma*$cnt)." денари" : "") ; ?>.</p>
                  <p style="font-size: 12pt;font-weight: bold; color: #000000;"><?php print $offerTitle;?></p>
                  <p style="font-size: 12pt;color: #000000;"><?php echo ($cnt > 1 ? "Ваучерите се резервирани на" : "Ваучерот е резервиран на"); ?>:  <?php print date("d/m/Y", strtotime($time)) ; ?> во <?php print date("H:i", strtotime($time)) ; ?>ч.</p>

<?php require 'mail_footer.php' ; ?>
