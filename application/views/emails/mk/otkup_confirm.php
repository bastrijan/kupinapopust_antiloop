<?php require 'mail_header.php'; ?>
		
		<p style="font-size: 12pt;font-weight: bold; color: #000000;">
			Успешно г<?php echo ($cntNaracki > 1 ? "и" : "о"); ?> нарачавте ваши<?php echo ($cntNaracki > 1 ? "те" : "от"); ?> одбран<?php echo ($cntNaracki > 1 ? "и" : ""); ?> производ<?php echo ($cntNaracki > 1 ? "и" : ""); ?>!
		</p>
		
		<?php foreach ($payAttemptDataArr as $arrKey => $payAttemptData)  { ?>
			<p style=" border-left:8px solid #8cc732; padding-left:10px;font-size: 12pt;color: #000000;">	
				<strong><?php print strip_tags($dealDataArr[$arrKey]->title_mk_clean); ?></strong> <br/>
				<strong>Количина:</strong> <?php print $payAttemptData->quantity; ?>
			</p>
		<?php }  ?>
		
		
		<p style="font-size: 12pt; color: #000000; ">
			<?php 
				if ($payAttemptDataArr[0]->otkup_prevzemanje == 1) 
				{
					echo "Ве молиме почекајте да ве информираме кога производот би бил достапен за подигнување од нашите канцеларии. Најчесто во рок од 7 до 14 дена ги имаме производите подготвени за подигнување.";
				}
				else 
				{
					echo "Производот ќе го добиете во рок наведен во понудата.<br/>";
					echo "<b>Напомена: При превземање на производот ја плаќате цената на производот плус цената за достава (освен за некои понуди каде што доставата е бесплатна).</b>";
				}
			?>
		</p>

<!-- 		<p style="font-size: 12pt; color: #000000;">Сите ваши ваучери се наоѓаат на вашиот kupinapopust профил и можете во било кое време да ги превземете.</p> -->



<?php require 'mail_footer.php'; ?>