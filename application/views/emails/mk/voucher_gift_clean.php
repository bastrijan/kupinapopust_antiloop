<?php require 'mail_header.php'; ?>
    
          <p style="font-size: 12pt;font-weight: bold; color: #000000;">Ви благодариме на довербата.</p>
          <p style="font-size: 12pt;color: #000000;">Купивте одличен подарок за Вашиот пријател<?php print ($zasteda > 0 ? " и заштедивте ".($zasteda*$cnt)." денари" : "") ; ?>.</p>
        
          <p style="font-size: 12pt;color: #000000; font-weight: bold; border-left:8px solid #8cc732; padding-left:10px;">  
            <?php print strip_tags($voucherTitle); ?>
          </p>

          <p style="font-size: 12pt;color: #000000;"><?php echo ($cnt > 1 ? "Ваучерите се купени на" : "Ваучерот е купен на"); ?>:  <?php print date("d/m/Y", strtotime($time)) ; ?> во <?php print date("H:i", strtotime($time)) ; ?>ч.</p>
                  
          <?php if($copy_email == "") { ?>  
            <p style="font-size: 12pt;color: #000000;">За да <?php echo ($cnt > 1 ? "ги испечатите ваучерите" : "го испечатите ваучерот"); ?> потребно е да се логирате во вашиот "Кориснички профил" и во делот "Листа на ваучери" да кликнете "Печати ваучер".</p>
            <p style="font-size: 12pt;color: #000000;">При најавувањето Ве молиме внесете ја вашата kupinapopust лозинка или креирајте си нова доколку сте ја заборавиле. Доколку е-мејлот со кој сте го купиле ваучерот е ист со е-мејлот што го имате на facebook, можете да се најавите преку facebook.</p>
            <p style="font-size: 12pt;color: #000000;"><a target="_blank" href="http://kupinapopust.mk/customer/login" style="background:#8cc732; padding:10px 20px 10px 20px; font-weight:bold; border-radius:5px; font-size:13px; text-transform:uppercase; text-decoration:none;"><span style="color:#FFFFFF;">Најави се</span></a></p>
          <?php } //if($copy_email == "") { ?>
        
          <p style="font-size: 12pt;color: #000000;">Со секој купен ваучер добивате поени кои можете да ги искористите за да добиете уште поголем попуст, па дури и сосема бесплатен ваучер.</p>

        <?php if ($otkup_prevzemanje > 0) { ?>
          <p style="font-size: 12pt; color: #cc0000; ">
            <?php 
              if ($otkup_prevzemanje == 1) 
              {
                echo "Ве молиме почекајте да ве информираме кога производот би бил достапен за подигнување од нашите канцеларии. Најчесто во рок од 7 до 14 дена ги имаме производите подготвени за подигнување.";
              }
              else 
              {
                  echo "Производот ќе го добиете во рок наведен во понудата.<br/>";
                  echo "<b>Напомена: При превземање на производот ја плаќате цената за достава (освен за некои понуди каде што доставата е бесплатна).</b>";
              }
            ?>
          </p>
        <?php } ?>

<?php require 'mail_footer.php' ; ?>