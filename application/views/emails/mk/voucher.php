<?php require 'mail_header.php'; ?>
		
		<?php
		if($email_content_buy_switch == 0) {
		?>
			<p style="font-size: 12pt;font-weight: bold; color: #000000;">Ви благодариме на довербата.</p>
			
			<p style="font-size: 12pt;color: #000000;">Купивте <?php echo $cnt; ?> Ваучер<?php echo ($cnt > 1 ? "и" : ""); ?> за вашата избрана понуда<?php print ($zasteda > 0 ? " и заштедивте ".($zasteda*$cnt)." денари" : "") ; ?>.</p>
			
			<p style=" border-left:8px solid #8cc732; padding-left:10px;font-size: 12pt;color: #000000;">	
				<strong><?php print strip_tags($voucherTitle); ?></strong>
			</p>
			
			<p style="font-size: 12pt;color: #000000;"><?php echo ($cnt > 1 ? "Ваучерите се купени на" : "Ваучерот е купен на"); ?>:  <?php print date("d/m/Y", strtotime($time)); ?> во <?php print date("H:i", strtotime($time)); ?>ч.</p>
			
			<p style="font-size: 12pt; color: #000000; font-weight:bold;">Ваучерот е прикачен како прилог на овој e-mail и можете да го превземете отварајќи го со pdf читач.</p>

			<p style="font-size: 12pt; color: #000000;">Сите ваши ваучери се наоѓаат на вашиот kupinapopust профил и можете во било кое време да ги превземете.</p>

			
		<?php
		} else {
				echo $email_content_buy;
		} ?>

		<p><a target="_blank" href="http://kupinapopust.mk/customer/login" style="background:#8cc732; padding:10px 20px 10px 20px; font-weight:bold; border-radius:5px; font-size:12pt; text-transform:uppercase; text-decoration:none;"><span style="color:#FFFFFF;">Најави се</span></a></p>

		<?php if($email_content_buy_switch == 0) { ?>
			<p  style="font-size: 12pt;color: #000000;">Со секој купен ваучер <b>добивате поени</b> кои можете да ги искористите за да добиете уште поголем попуст, па дури и сосема бесплатен ваучер.</p>
		<?php } ?>

		<?php if ($otkup_prevzemanje > 0) { ?>
			<p style="font-size: 12pt; color: #cc0000; ">
				<?php 
					if ($otkup_prevzemanje == 1) 
					{
						echo "Ве молиме почекајте да ве информираме кога производот би бил достапен за подигнување од нашите канцеларии. Најчесто во рок од 7 до 14 дена ги имаме производите подготвени за подигнување.";
					}
					else 
					{
						echo "Производот ќе го добиете во рок наведен во понудата.<br/>";
						echo "<b>Напомена: При превземање на производот ја плаќате цената за достава (освен за некои понуди каде што доставата е бесплатна).</b>";
					}
				?>
			</p>
		<?php } ?>

<?php require 'mail_footer.php'; ?>