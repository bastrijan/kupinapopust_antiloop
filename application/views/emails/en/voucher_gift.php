<div style="background:#FF6600">
  <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
      <tr>
        <td align="center">
          <table width="600" cellspacing="0" cellpadding="20" border="0" align="center" style="background:white;margin:20px 0 20px 0">
            <tbody>
              <tr>
                <?php require_once 'mail_header_voucher.php' ; ?>
              </tr>
              <tr>
                <td style="padding-top:0">
                  <p>Congratulations. You bought a great gift for your friend and you saved <?php print $zasteda ; ?> denars.</p>
                  <p style="font-size: 18px">Your friend receives: <?php print $voucherTitle ; ?></p>
                  <p>Unique code:</p>
                  <p>The Voucher is bought on:  <?php print date("d/m/Y", strtotime($time)) ; ?> at <?php print date("H:i", strtotime($time)) ; ?>h.</p>
                  <p>The Voucher can be used at: <?php print $partnerName ; ?>, <?php print $partnerAddress ; ?></p>
                  <p>The Voucher can be used from<?php print $from ; ?>until<?php print $to ; ?></p>
                  <p>Good  to know:</p>
                  <p> The  Voucher is valid after the activation of the discount and receiving of this e-mail / The Voucher cannot be exchanged for money / It can be used only once.<br />
                  </p>
                  <p>You  have a question?</p>
                  <p>Feel free to contact us by e-mail (contact@kupinapopust.mk)  or call us by phone on 3211 901 or mob. 075 486&nbsp;560 (every working day between 09:00 – 20:00h.)   </p></td>
              </tr>
              <tr>
                <?php require_once 'mail_footer.php' ; ?>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</div>