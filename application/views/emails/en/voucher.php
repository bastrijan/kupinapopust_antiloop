<div style="background:#FF6600">
  <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
      <tr>
        <td align="center">
          <table width="600" cellspacing="0" cellpadding="20" border="0" align="center" style="background:white;margin:20px 0 20px 0">
            <tbody>
              <tr>
                <?php require_once 'mail_header_voucher.php' ; ?>
              </tr>
              <tr>
                <td style="padding-top:0">
                  <p>Congratulations. You bought a Voucher for your chosen offer and you saved <?php print $zasteda ; ?> denars</p>
                  <p style="font-size: 18px">Info about the offer: <?php print $voucherTitle ; ?></p>
                  <p><b>Unique code:</b> <?php print $code ; ?></p>
                  <p>The Voucher is bought on:  <?php print date("d/m/Y", strtotime($time)) ; ?> at <?php print date("H:i", strtotime($time)) ; ?>h.</p>
                  <p>The Voucher can be used at: <?php print $partnerName ; ?>, <?php print $partnerAddress ; ?></p>
                  <p>Th Voucher can be used from<?php print $from ; ?> until <?php print $to ; ?></p>
                  <p>Good to know:</p>
                  <p>The Voucher is valid after the activation of the discount and receiving of this e-mail / The Voucher cannot be exchanged for money / It can be used only once.<br />
                  User Instruction:</p>
                  <ol start="1" type="1">
                    <li>Print the Voucher (if you don’t have a printer it’s enough to know your Unique code. You can write it in your mobile phone or on piece on paper, but please be careful with rewriting the specific characters). </li>
                    <li>Give it to the provider of the offer or tell them your Unique code. Without having printed Voucher you will be asked to show your ID. </li>
                    <li>Enjoy:)!!! </li>
                  </ol>
                <p>You  have a question?</p>
                <p>Feel free to contact us by e-mail (contact@kupinapopust.mk)  or call us by phone on 3211 901 or mob. 075 486 560 (every working day between 09:00 – 20:00h.)</p></td>
              </tr>
              <tr>
                <?php require_once 'mail_footer.php' ; ?>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</div>