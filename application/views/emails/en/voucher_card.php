<div style="background:#FF6600">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td align="center">
                    <table width="600" cellspacing="0" cellpadding="20" border="0" align="center" style="background:white;margin:20px 0 20px 0">
                        <tbody>
                            <tr>
                                <?php require_once 'mail_header.php'; ?>
                            </tr>
                            <tr>
                                <td style="padding-top:0">
                                    <p>Congratulations. You bought a Voucher for your chosen offer and you saved xxx MKD.</p>
                                    <p>Offer Info:</p>
                                    <p>Unique Code:</p>
                                    <p>The Voucher is bought on: 10th of January, 2011 at 10:25h.</p>
                                    <p>Use the Voucher at: Partners name, Address, Contact, web page.</p>
                                    <p>The Voucher is valid from 21.05.2011 until 21.06.2011</p>
                                    <p>Good to know:</p>
                                    <p>The Voucher is valid after the activation of the discount and after we send you the validation e-mail with your unique code / The Voucher cannot be exchanged for money / It can be used only once.</p>
                                    <p>User Instruction:</p>
                                    <p>
                                        1.	Print the Voucher (if you don’t have a printer it’s enough to know your Unique code. You can write it in your mobile phone or on piece on paper, but please be careful with rewriting the specific characters). <br>
                                        2.	Give it to the provider of the service or tell them your Unique code. Without having printed Voucher you will be asked to show your ID.<br>
                                        3.	Enjoy :)!!!
                                    </p>

                                    <p>You have a question?</p>
                                    <p>Feel free to contact us by e-mail (contact@kupinapopust.mk) or call us by phone on 3211 901 or mob. 075 486 560(every working day between 09:00 – 20:00h.)</p>
                                </td>
                            </tr>
                            <tr>
                                <?php require_once 'mail_footer.php'; ?>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>