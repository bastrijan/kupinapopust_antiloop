<div style="background:#FF6600">
  <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
      <tr>
        <td align="center">
          <table width="600" cellspacing="0" cellpadding="20" border="0" align="center" style="background:white;margin:20px 0 20px 0">
            <tbody>
              <tr>
                <?php require_once 'mail_header.php' ; ?>
              </tr>
              <tr>
                <td style="padding-top:0">
                  <p>User: <?php print $email ; ?></p>
                  <p>Congratulations. You have reserved a Voucher for your chosen offer and you saved <?php print $suma; ?> denars.</p>
                  <p style="font-size: 18px"><?php print $offerTitle;?></p>
                  <p>The Voucher has been reserved on:  <?php print date("d/m/Y", strtotime($time)) ; ?>at<?php print date("H:i", strtotime($time)) ; ?>h.</p>
                  <p>Dear user,</p>
                  <p>After the activation of the discount you will receive the Voucher on your registered e-mail address.</p>
                  <p>Greetings,</p>
                  <p>Kupinapopust.mk team</p>
                </td>
              </tr>
              <tr>
                <?php require_once 'mail_footer.php' ; ?>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</div>