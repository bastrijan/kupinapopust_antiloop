<div style="background:#FF6600">
  <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
      <tr>
        <td align="center">
          <table width="600" cellspacing="0" cellpadding="20" border="0" align="center" style="background:white;margin:20px 0 20px 0">
            <tbody>
              <tr>
                <?php require_once 'mail_header.php' ; ?>
              </tr>
              <tr>
                <td style="padding-top:0">
                  <p>User: <?php print $email ; ?></p>
                  <p>Congratulations. You have reserved a Voucher for your chosen offer and you saved <?php print $suma;?> denars</p>
                  <p>Your friend receives:</p>
                  <p style="font-size: 18px"><?php print $cleanTitle;?></p>
                  <p>Note: The Gift Voucher is without our logo and the discounted price. Only the value of the Voucher is written, short info about the offer and Your dedication.</p>
                  <p>The Voucher is reserved on: <?php print date("d/m/Y", strtotime($time)) ; ?> at <?php print date("H:i", strtotime($time)) ; ?>h.</p>
                  <p>Dear,</p>
                  <p>In  order to collect your Voucher please visit us latest in 24h. after the booking  at our offices at &ldquo;Nikola Vapcarov&rdquo; street number 3/1, Skopje, Center (across  the AMSM testing centre, through the tunnel behind McDonald&rsquo;s). <br />
                    The  payment can be in Cash or by credit/debit card. It will be our pleasure to print  you the Voucher and inform you in detail about your chosen offer. <br />
                    <strong>The Reservation is binding</strong>. In case you are not able  to visit us in 24h. after you reserve the Voucher please inform us by e-mail (contact@kupinapopust.mk) or  call us on our office phones: 3211 901, mob. 075 486 560 (every working day between 09:00 – 20:00h). We will try to help you. </p>
                  <p>Greetings, <br />
                    Kupinapopust.mk team
                  </p>
                </td>
              </tr>
              <tr>
                <?php require_once 'mail_footer.php' ; ?>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</div>