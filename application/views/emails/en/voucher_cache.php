<div style="background:#FF6600">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td align="center">
                    <table width="600" cellspacing="0" cellpadding="20" border="0" align="center" style="background:white;margin:20px 0 20px 0">
                        <tbody>
                            <tr>
                                <td>
                                    <a target="_blank" href="http://kupinapopust.mk">
                                        <img width="223" height="120" border="0" alt="Kupinapopust.mk" src="http://kupinapopust.mk/pub/img/layout/logo-top.png">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:0">
                                    <p>
                                        <b>
                                            Congratulations. You have reserved a Voucher for your chosen offer and you saved xxx MKD
                                        </b>
                                    </p>
                                    <p>Offer info:</p>
                                    <p>The Voucher is reserved on: 10th of January, 2011 at 10:25h.</p>
                                    <p>Dear,</p>
                                    <p>In order to collect your Voucher please visit us latest in 24h. after the booking at our offices at “Nikola Vapcarov” street number 3/1, Skopje, Center (across the AMSM testing centre, through the tunnel behind McDonald’s).</p>
                                    <p>The payment can be in Cash or by credit/debit card. It will be our pleasure to print you the Voucher and inform you in detail about your chosen offer.</p>
                                    <p><b>The Reservation is binding.</b> In case you are not able to visit us in 24h. after you reserve the Voucher please inform us by e-mail (contact@kupinapopust.mk) or call us on our office phones: 3211 901, mob. 075 486 560 (every working day between 09:00 – 20:00h). We will try to help you.</p>
                                    <p>Yours sincerely,<br>
                                        Kupinapopusт.mk team
                                    </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>