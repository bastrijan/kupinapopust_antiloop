<div style="background:#FF6600">
  <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
      <tr>
        <td align="center">
          <table width="600" cellspacing="0" cellpadding="20" border="0" align="center" style="background:white;margin:20px 0 20px 0">
            <tbody>
              <tr>
                <?php require_once 'mail_header.php' ; ?>
              </tr>
              <tr>
                <td style="padding-top:0">
                  <p>User: <?php print $email ; ?></p>
                  <p>Congratulations. You have reserved a Voucher for your chosen offer and you saved <?php print $suma;?> denars</p>
                  <p>Dear Users, </p>
                  <p>With a goal to provide you with more flexible payment, we offer you the option to pay by putting cash to our bank account ( filling the PP 10 form) or transfer the money from yours to our bank account by filling the PP 30 form.</p>
                  <p><img border="0" src="http://kupinapopust.mk/pub/img/layout/uplatnica_bank_small.png"></p>
                  <p><img border="0" src="http://kupinapopust.mk/pub/img/layout/uplatnica_cache_small.png"></p>

                  <p>Your friend receives:</p>
                  <p style="font-size: 18px"><?php print $cleanTitle;?></p>

                  <p>Note: The Gift Voucher is without our logo and the discounted price. Only the value of the Voucher is written, short info about the offer and Your dedication.</p>
                  <p>The Voucher is reserved on:  <?php print date("d/m/Y", strtotime($time)) ; ?> at <?php print date("H:i", strtotime($time)) ; ?>h.</p>
                  <p>Dear user,</p>
                  <p>After the bank transaction PLEASE INFORM US BY E-MAIL (contact@kupinapopust.mk), so we can proceed your Voucher.</p>
                  <p>We need your:</p>
                  <p>1. Name and Surname</p>
                  <p>2. For which offer you have paid</p>
                  <p>3. The amount paid</p>
                  <p>Please note that it usually takes 2-3 days for the bank payment to be completed. Therefore we kindly ask for your patience. In case you come in our offices and you show us a copy from the bank payment slip with signature or you send us a scanned copy by e-mail, we will immediately send you the Voucher. </p>
                  <p>Greetings,</p>
                <p>Kupinapopust.mk тeam</p></td>
              </tr>
              <tr>
                <?php require_once 'mail_footer.php' ; ?>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</div>