<?php 
defined('SYSPATH') OR die('No direct access allowed.'); 

//zemi go logiraniot user
$logiran_user_id = $this->session->get("user_id");

?>

<!-- include summernote -->
<link rel="stylesheet" href="/pub/summernote/summernote.css">
<script type="text/javascript" src="/pub/summernote/summernote.js"></script>

<?php require_once APPPATH.'views/admin/menu.php' ; ?>

  <script type="text/javascript">

    $(document).ready(function() {
      $('.adress_mk').summernote({
			height: 300
	  });
	
      $('.terms_mk').summernote({
			height: 300
	  });
	
      $('.notes').summernote({
			height: 300
	  });

      $('.za_kompanijata_text').summernote({
            height: 300
      });


        <?php if (isset($errorFlag) and $errorFlag == 1) { ?>

                $().toastmessage('showToast', {
                    text     : "Почитувани, партнерот не е зачуван затоа што веќе постои партнер со истиот Username. Ве молиме одберете друг Username за новиот партнер.",
                    sticky   : true,
                    position : 'top-center',
                    type     : 'error'
                });
       
        <?php } ?> 

    });
  </script>



<script type="text/javascript">
		function ChangeDDVStapkaDiv()
		{
		
			$("#ddv_stapka_div").show();
			$("#5_posto").show();
			
			if($("#tip_danocna_sema").val() == "Промет остварен во странство")
			{
				$("#ddv_stapka_div").hide();
			}
			else
			{
				if($("#tip_danocna_sema").val() == "Ослободен согласно законот")
				{
					$('#ddv_stapka option[value="18"]').attr('selected', 'selected');
					$("#5_posto").hide();
					
				}
			}
		}
		
		
		$(document).ready(function() {
			
			ChangeDDVStapkaDiv();
			
			$("#tip_danocna_sema").change(function () {

				ChangeDDVStapkaDiv();
			});
			
		});

</script>


<h1>Уреди партнер</h1>

<?php if (!isset($dealData)) { ?>
    <form method="post" action="" id="deal_save_form">
        <div class="container_12 homepage-billboard-dhtml">
            <div class="grid_12 alpha omega">
                <input type="hidden" name="id" value="">

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Име на продавач</div>
                    <div class="grid_8 alpha omega"><input type="text" name="name" value="" ></div>
                </div>
				
				<div class="row clearfix">
                    <div class="grid_4 alpha omega">Тип на даночна шема</div>
                    <div class="grid_8 alpha omega" >
                        <select name="tip_danocna_sema" id="tip_danocna_sema">
							<option value="ДДВ Обврзник"><?php print "ДДВ Обврзник"; ?></option>
							<option value="Не ДДВ Обврзник"><?php print "Не ДДВ Обврзник"; ?></option>
							<option value="Ослободен согласно законот"><?php print "Ослободен согласно законот"; ?></option>
							<option value="Промет остварен во странство"><?php print "Промет остварен во странство"; ?></option>
                        </select>
                    </div>
                </div>   
				
                <div class="row clearfix" id="ddv_stapka_div">
                    <div class="grid_4 alpha omega">ДДВ стапка</div>
                    <div class="grid_8 alpha omega">
                        <select name="ddv_stapka" id="ddv_stapka">
							<option value="5" id="5_posto"><?php print "5%"; ?></option>
							<option value="18"><?php print "18%"; ?></option>
                        </select>
                    </div>
                </div> 


                <div class="row clearfix" style="padding: 20px;">
					<strong>Адреса, телефон и слично</strong>
					<textarea class="adress_mk" name="adress_mk"></textarea>
                </div>
				
				


                <div class="row clearfix" style="padding: 20px;">
					<strong>Услови за користење на попустот</strong>
					<textarea class="terms_mk" name="terms_mk"></textarea>
                </div>
				

				<div class="row clearfix">
                    <div class="grid_4 alpha omega">Мапа (<a target="_blank" href="/index/google_static_map">Креирај тука</a>)</div>
                    <div class="grid_8 alpha omega"><input type="text" name="google_link" value="" ></div>
                </div>
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Линк</div>
                    <div class="grid_8 alpha omega"><input type="text" name="google_url" value="" ></div>
                </div>
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">E-mail адреса</div>
                    <div class="grid_8 alpha omega"><input type="text" name="email" value="" ></div>
                </div>
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Username</div>
                    <div class="grid_8 alpha omega"><input type="text" name="username" value="" ></div>
                </div>
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Password</div>
                    <div class="grid_8 alpha omega"><input type="text" name="password" value="" ></div>
                </div>
				
				<?php if(in_array($logiran_user_id, array(1, 5))) { ?>
                <div class="row clearfix" style="padding: 20px;">
					<strong>Забелешка</strong>
					<textarea class="notes" name="notes"></textarea>
                </div>
				<?php }//else od if($logiran_user_id > 0) {?>
				
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Алтернативно име за табот "За компанијата"</div>
                    <div class="grid_8 alpha omega"><input type="text" name="za_kompanijata_label" value="" ></div>
                </div>

                <div class="row clearfix" style="padding: 20px;">
                    <strong>Содржина за табот "За компанијата"</strong>
                    <textarea class="za_kompanijata_text" name="za_kompanijata_text"></textarea>
                </div>

                <div class="row clearfix">
                    <input type="submit" value="Зачувај">
                    <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/apartners';return false;">
                </div>
                
            </div>
        </div>
    </form>
<?php } ?>

<?php if (isset($dealData)) { ?>

    <form method="post" action="" id="deal_save_form">
        <div class="container_12 homepage-billboard-dhtml">
            <div class="grid_12 alpha omega">
                <input type="hidden" name="id" value="<?php print $dealData[0]->id ?>">
               
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Име на продавач</div>
                    <div class="grid_8 alpha omega"><input type="text" name="name" value="<?php print $dealData[0]->name ?>" ></div>
                </div>

               <div class="row clearfix">
                    <div class="grid_4 alpha omega">Тип на даночна шема</div>
                    <div class="grid_8 alpha omega">
                        <select name="tip_danocna_sema"  id="tip_danocna_sema">
							<option value="ДДВ Обврзник" <?php if($dealData[0]->tip_danocna_sema=="ДДВ Обврзник") echo 'selected="selected"'; ?> ><?php print "ДДВ Обврзник"; ?></option>
							<option value="Не ДДВ Обврзник" <?php if($dealData[0]->tip_danocna_sema=="Не ДДВ Обврзник") echo 'selected="selected"'; ?> ><?php print "Не ДДВ Обврзник"; ?></option>
							<option value="Ослободен согласно законот" <?php if($dealData[0]->tip_danocna_sema=="Ослободен согласно законот") echo 'selected="selected"'; ?> ><?php print "Ослободен согласно законот"; ?></option>
							<option value="Промет остварен во странство" <?php if($dealData[0]->tip_danocna_sema=="Промет остварен во странство") echo 'selected="selected"'; ?> ><?php print "Промет остварен во странство"; ?></option>
                        </select>
                    </div>
                </div> 
				
               <div class="row clearfix" id="ddv_stapka_div">
                    <div class="grid_4 alpha omega">ДДВ стапка</div>
                    <div class="grid_8 alpha omega">
                        <select name="ddv_stapka" id="ddv_stapka">
							<option value="5" <?php if($dealData[0]->ddv_stapka=="5") echo 'selected="selected"'; ?> id="5_posto"><?php print "5%"; ?></option>
							<option value="18" <?php if($dealData[0]->ddv_stapka=="18") echo 'selected="selected"'; ?> ><?php print "18%"; ?></option>
                        </select>
                    </div>
                </div> 


                <div class="row clearfix" style="padding: 20px;">
					<strong>Адреса, телефон и слично</strong>
					<textarea class="adress_mk" name="adress_mk"><?php print $dealData[0]->adress_mk; ?></textarea>
                </div>


                <div class="row clearfix" style="padding: 20px;">
					<strong>Услови за користење на попустот</strong>
					<textarea class="terms_mk" name="terms_mk"><?php print $dealData[0]->terms_mk; ?></textarea>
                </div>

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Мапа (<a target="_blank" href="/index/google_static_map">Креирај тука</a>)</div>
                    <div class="grid_8 alpha omega"><input type="text" name="google_link" value="<?php print $dealData[0]->google_link ?>" ></div>
                </div>
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Линк</div>
                    <div class="grid_8 alpha omega"><input type="text" name="google_url" value="<?php print $dealData[0]->google_url ?>" ></div>
                </div>
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">E-mail адреса</div>
                    <div class="grid_8 alpha omega"><input type="text" name="email" value="<?php print $dealData[0]->email ?>" ></div>
                </div>
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Username</div>
                    <div class="grid_8 alpha omega"><input type="text" name="username" value="<?php print $dealData[0]->username ?>" ></div>
                </div>
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Password</div>
                    <div class="grid_8 alpha omega"><input type="text" name="password" value="<?php print $dealData[0]->password ?>" ></div>
                </div>
				
				<?php if(in_array($logiran_user_id, array(1, 5))) { ?>
                <div class="row clearfix" style="padding: 20px;">
					<strong>Забелешка</strong>
					<textarea class="notes" name="notes"><?php print $dealData[0]->notes; ?></textarea>
                </div>
				<?php }//else od if($logiran_user_id > 0) {?>
				
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Алтернативно име за табот "За компанијата"</div>
                    <div class="grid_8 alpha omega"><input type="text" name="za_kompanijata_label" value="<?php print $dealData[0]->za_kompanijata_label ?>" ></div>
                </div>

                <div class="row clearfix" style="padding: 20px;">
                    <strong>Содржина за табот "За компанијата"</strong>
                    <textarea class="za_kompanijata_text" name="za_kompanijata_text"><?php print $dealData[0]->za_kompanijata_text; ?></textarea>
                </div>

                <div class="row clearfix">
                    <input type="submit" value="Зачувај" value="save_btn">
                    <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/apartners';return false;">
                </div>
                
            </div>
        </div>
    </form>

<?php } ?>

