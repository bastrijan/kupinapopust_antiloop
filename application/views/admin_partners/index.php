<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once APPPATH.'views/admin/menu.php' ; ?>

<h1>Партнери</h1>
<br/>
<?php
	print "<a href='/apartners/manage'>Внеси нов партнер</a>";
?>
<div class="container_12 homepage-billboard">
    <div class="clearfix" style="margin-bottom: 30px">

		<table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
			<!-- HEAD -->
			<tr>
				<td><strong>Име</strong></td>
				<td><strong>Акција</strong></td>
			</tr>
			<tr>
				<td  colspan="2"><hr></td>
			</tr>
			<!-- END HEAD -->
		<?php
		if ($partnersData) {
			foreach ($partnersData as $partner) {
		?>
			<tr>
				<td ><?php echo $partner->name; ?></td>

				<td >
					<?php
						print html::anchor("/apartners/manage/$partner->id", "Промени");
						print "<br/>";
						print html::anchor("/apartners/mark_as_paid/$partner->id", "Означи како исплатено");
					?>
				</td>

			</tr>
			<tr>
				<td  colspan="2"><hr></td>
			</tr>
		<?php
			}
		}//if ($finreportdata) {
		?>		
		</table>
		
    </div>
</div>