<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<!-- include summernote -->
<link rel="stylesheet" href="/pub/summernote/summernote.css">
<script type="text/javascript" src="/pub/summernote/summernote.js"></script>

<?php require_once APPPATH.'views/admin/menu.php' ; ?>

<h1>Означи како исплатено</h1>
<br/>
<h4>Партнер: <?php print $partnerData[0]->name; ?></h4>

<div class="container_12 homepage-billboard-dhtml">
    <div class="grid_12 alpha omega">


        <form id="myformMarkPaid" action="/apartners/mark_as_paid/<?php echo $partnerData[0]->id; ?>" method="post" >
            <div class="row clearfix">
                <div class="grid_12 alpha omega">
                    &nbsp;&nbsp; 
                    <strong>ЗА ИСПЛАТА: <?php echo $sumaZaNaplata; ?> ден.</strong>

                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <button type="submit" name="btnMarkAsPaidOff" id="btnMarkAsPaidOff"  onclick="return confirm('Дали сте сигурни дека сакате сумата да ја маркирате како исплатенa?')">ОЗНАЧИ КАКО ИСПЛАТЕНО</button>
                </div>
            </div>
        </form>

        <form id="myformSaveNotes" action="/apartners/paid_off_notes_admin/<?php echo $partnerData[0]->id; ?>" method="post" >
            <div class="row clearfix">
                <div class="grid_12 alpha omega">
                    <strong>Забелешка</strong>
                    <textarea class="paid_off_notes_admin" name="paid_off_notes_admin" ><?php print $partnerData[0]->paid_off_notes_admin; ?></textarea>
                    

                    <span >(Забелешката автоматски се зачувува на секои 10 секунди)</span>
                    <br/><br/>
                    <button type="submit" name="btnNotes" id="btnNotes" >ЗАЧУВАЈ ЗАБЕЛЕШКА</button>
                </div>

            </div>

            <input type="hidden" name="partner_id" value="<?php echo $partnerData[0]->id; ?>">

            <input type="hidden" name="autosave" value="false">
        </form>
    


    </div> 
</div>

<script type="text/javascript">

    var setIntervals;

    function activateAutoSaveFromDHTML() 
    {
        if ($('input[name="autosave"]').val() == 'false') {
            $('input[name="autosave"]').val('true');
            setIntervals = setInterval(start_autosave, 10 * 1000);
        }
    };

    function start_autosave() {

        $('textarea[name="paid_off_notes_admin"]').val($('.paid_off_notes_admin').code());

//        var DataSer = new FormData('form');
        var DataSer = $("#myformSaveNotes").serializeArray();
        jQuery.ajax({
            url: '/apartners/ajax_autosave_paid_off_notes_admin',
            data: DataSer,
            enctype: 'multipart/form-data',
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data && data.status == 'success') 
                {
                    //ako se e vo red => NE PRAVI NISTO
                } 
                else 
                {
                    //ako nesto ne e vo red => prestani da so povikuvanje na funcionalnosta za AJAX
                    clearInterval(setIntervals);
                }
            }// end successful POST function
        }); // end jQuery ajax call
    }

    $(document).ready(function () {

        $("#myformSaveNotes").change(function () {
             
            activateAutoSaveFromDHTML();
                
        });



    <?php if (isset($updateSuccess) and $updateSuccess == 1) { ?>
            $().toastmessage('showToast', {
                text     : "Промените се успешно зачувани.",
                sticky   : false,
                position : 'middle-center',
                type     : 'success'
            }); 

    <?php } ?>


      $('.paid_off_notes_admin').summernote({
            onKeyup: function(e) {
                activateAutoSaveFromDHTML();
            },
            height: 150
      });



    });
</script>
