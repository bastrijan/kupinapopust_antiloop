<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php $userID = $this->session->get("user_id"); ?>
<style>
    .admin_menu li {
        display: inline;
        padding: 0px 0px !important;
    }
</style>
<div class="container_12 homepage-billboard">
    <div class="grid_10 alpha omega">
        <ul class="admin_menu">
            <li><?php print html::anchor("/smetkovodstvo_izvestaj", "Извештај"); ?></li>
        </ul>

    </div>
    <div class="grid_2 alpha omega">
        <ul class="admin_menu">
            <li>        
                <a class="right" href="/smetkovodstvo/logout/<?php print base64_encode("/smetkovodstvo"); ?>">
                    Logout
                </a>
            </li>
        </ul>

    </div>
</div>