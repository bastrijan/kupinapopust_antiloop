<?php defined('SYSPATH') OR die('No direct access allowed.') ; ?>

<?php if(!$isPrintVersion) { ?>
	<?php require_once ($this->user_type == 'admin' ? APPPATH."views/admin/": "").'menu.php' ; ?>

	<script type="text/javascript">

		$(document).ready(function() {
			$("#smetkovodstvoreport_day").change(function () {
				$("#smetkovodstvoreport_frm").submit();
			})
			
			$("#smetkovodstvoreport_month").change(function () {
				$("#smetkovodstvoreport_frm").submit();
			})

			$("#smetkovodstvoreport_year").change(function () {
				$("#smetkovodstvoreport_frm").submit();
			})

		});
	</script>
<?php } //if(!$isPrintVersion) { ?>

<div class="container_12 homepage-billboard">
  <div class="clearfix" style="margin-bottom: 30px">

		<form method="get" action="" id="smetkovodstvoreport_frm">
			<?php print "Ден: " ?> 
			<select id="smetkovodstvoreport_day" name="smetkovodstvoreport_day">
				<?php for($i=1; $i<=31; $i++) { ?>
				<option value="<?php echo $i?>" <?php print ($smetkovodstvoreport_day == $i) ? 'selected="true"' : ""  ?>><?php echo $i?></option>
				<?php }//for($i=1; $i<=12; $i++)  ?>
				
			</select>
			<?php print "Месец: " ?> 
			<select id="smetkovodstvoreport_month" name="smetkovodstvoreport_month">
				<?php for($i=1; $i<=12; $i++) { ?>
				<option value="<?php echo $i?>" <?php print ($smetkovodstvoreport_month == $i) ? 'selected="true"' : ""  ?>><?php echo $i?></option>
				<?php }//for($i=1; $i<=12; $i++)  ?>
				
			</select>
			&nbsp;&nbsp;
			<?php print "Година: " ?> 
			<select id="smetkovodstvoreport_year" name="smetkovodstvoreport_year">
				<?php for($i=2011; $i<=(date("Y")+ 1); $i++) { ?>
					<option value="<?php echo $i?>" <?php print ($smetkovodstvoreport_year == $i) ? 'selected="true"' : ""  ?>><?php echo $i?></option>
				<?php }//for($i=1; $i<=12; $i++)  ?>
			</select>
			
		<?php
		if ($smetkovodstvoreportdata && !$isPrintVersion) {
			print "&nbsp;&nbsp;&nbsp;<a href='/smetkovodstvo_izvestaj/indexprint?smetkovodstvoreport_day=".$smetkovodstvoreport_day."&smetkovodstvoreport_month=".$smetkovodstvoreport_month."&smetkovodstvoreport_year=".$smetkovodstvoreport_year."' target='_blank'>Печати</a>";
		}
		?>
		</form>
		<br/>
		
		<!-- TERMINSKI PLAN-->
		<div>Терминиски план: <br>
			 - вклучени се сите ваучери што се купени во текот на избраниот ден
		</div>
		<br><br>		
		
		
		<!-- CARD -->
		<div  style="text-align: center;font-size: 16px;"><strong>Ваучери купени со платежна картичка</strong></div>
		<br/>
		<table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
			<!-- HEAD -->
			<tr>
				<td ><strong>Партнер</strong></td>
				<td ><strong>ДДВ обврзник</strong></td>
				<td ><strong>Понуда</strong></td>
				<td ><strong>Продадени ваучери</strong></td>
				<td ><strong>Цена на ваучер</strong></td>
				<td ><strong>Вкупен приход</strong></td>
				<td ><strong>Провизија за kupinapopust.mk</strong></td>
				<td ><strong>Вкупна провизија</strong></td>
			</tr>
			<tr>
				<td  colspan="8"><hr></td>
			</tr>
			<!-- END HEAD -->
		<?php
		$sum_br_vauceri = 0;
		$sum_vkupen_prihod = 0;
		$sum_vkupna_provizija = 0;
		
		if ($smetkovodstvoreportdata) {
			foreach ($smetkovodstvoreportdata as $smetkovodstvoreportitem) {
		?>
			<tr>
				<td ><?php echo $smetkovodstvoreportitem->partner_name; ?></td>
				<td >&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $smetkovodstvoreportitem->ddv_obvrznik; ?>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td width="25%"><?php echo strip_tags($smetkovodstvoreportitem->title_mk); ?></td>
				<td >&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "<a href='/smetkovodstvo_izvestaj/pdf?report_deal_id=".$smetkovodstvoreportitem->deal_id."&report_type=card&report_year=".$smetkovodstvoreport_year."&report_month=".$smetkovodstvoreport_month."&report_day=".$smetkovodstvoreport_day."'>".$smetkovodstvoreportitem->cnt_vouchers."</a>"; ?></td>
				<td ><?php echo $smetkovodstvoreportitem->deal_cena; ?> ден.</td>
				<td ><?php echo $smetkovodstvoreportitem->vkupen_prihod; ?> ден.</td>
				<td ><?php echo $smetkovodstvoreportitem->deal_provizija; ?> ден.</td>
				<td ><?php echo $smetkovodstvoreportitem->vkupna_provizija; ?> ден.</td>
			</tr>
			<tr>
				<td  colspan="8"><hr></td>
			</tr>
		<?php
				$sum_br_vauceri += $smetkovodstvoreportitem->cnt_vouchers;
				$sum_vkupen_prihod += $smetkovodstvoreportitem->vkupen_prihod;
				$sum_vkupna_provizija  += $smetkovodstvoreportitem->vkupna_provizija;
			}
		}//if ($smetkovodstvoreportdata) {
		?>		
		</table>
		<div  style="text-align: right;"><strong>Вкупен број на ваучери: <?php 
																				if($sum_br_vauceri > 0)
																					echo "<a href='/smetkovodstvo_izvestaj/pdf?report_deal_id=0&report_type=card&report_year=".$smetkovodstvoreport_year."&report_month=".$smetkovodstvoreport_month."&report_day=".$smetkovodstvoreport_day."'>"; 
																				
																					echo $sum_br_vauceri; 
																				
																				if($sum_br_vauceri > 0)
																					echo "</a>"; 
																		 ?></strong></div>
		<div  style="text-align: right;"><strong>Вкупен приход: <?php echo $sum_vkupen_prihod; ?> ден.</strong></div>
		<div  style="text-align: right;"><strong>Вкупна провизија: <?php echo $sum_vkupna_provizija; ?> ден.</strong></div>
		<div  style="text-align: right;"><strong>ДДВ (18%): <?php $sum_ddv = ($sum_vkupna_provizija * 18)/100 ; echo $sum_ddv; ?> ден.</strong></div>
		<div  style="text-align: right;"><strong>САЛДО: <?php $sum_saldo = $sum_vkupna_provizija - $sum_ddv; echo $sum_saldo; ?> ден.</strong></div>

		
		<br/>
		<br/>
		<!-- BANK -->
		<div  style="text-align: center;font-size: 16px;"><strong>Ваучери купени со уплатница</strong></div>
		<br/>
		<table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
			<!-- HEAD -->
			<tr>
				<td ><strong>Партнер</strong></td>
				<td ><strong>ДДВ обврзник</strong></td>
				<td ><strong>Понуда</strong></td>
				<td ><strong>Продадени ваучери</strong></td>
				<td ><strong>Цена на ваучер</strong></td>
				<td ><strong>Вкупен приход</strong></td>
				<td ><strong>Провизија за kupinapopust.mk</strong></td>
				<td ><strong>Вкупна провизија</strong></td>
			</tr>
			<tr>
				<td  colspan="8"><hr></td>
			</tr>
			<!-- END HEAD -->
		<?php
		$sum_br_vauceri_bank = 0;
		$sum_vkupen_prihod_bank = 0;
		$sum_vkupna_provizija_bank = 0;
		
		if ($smetkovodstvoreportdata_bank) {
			foreach ($smetkovodstvoreportdata_bank as $smetkovodstvoreportitem_bank) {
		?>
			<tr>
				<td ><?php echo $smetkovodstvoreportitem_bank->partner_name; ?></td>
				<td >&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $smetkovodstvoreportitem_bank->ddv_obvrznik; ?>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td width="25%"><?php echo strip_tags($smetkovodstvoreportitem_bank->title_mk); ?></td>
				<td >&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "<a href='/smetkovodstvo_izvestaj/pdf?report_deal_id=".$smetkovodstvoreportitem->deal_id."&report_type=bank&report_year=".$smetkovodstvoreport_year."&report_month=".$smetkovodstvoreport_month."&report_day=".$smetkovodstvoreport_day."'>".$smetkovodstvoreportitem_bank->cnt_vouchers."</a>"; ?></td>
				<td ><?php echo $smetkovodstvoreportitem_bank->deal_cena; ?> ден.</td>
				<td ><?php echo $smetkovodstvoreportitem_bank->vkupen_prihod; ?> ден.</td>
				<td ><?php echo $smetkovodstvoreportitem_bank->deal_provizija; ?> ден.</td>
				<td ><?php echo $smetkovodstvoreportitem_bank->vkupna_provizija; ?> ден.</td>
			</tr>
			<tr>
				<td  colspan="8"><hr></td>
			</tr>
		<?php
				$sum_br_vauceri_bank += $smetkovodstvoreportitem_bank->cnt_vouchers;
				$sum_vkupen_prihod_bank += $smetkovodstvoreportitem_bank->vkupen_prihod;
				$sum_vkupna_provizija_bank  += $smetkovodstvoreportitem_bank->vkupna_provizija;
			}
		}//if ($smetkovodstvoreportdata) {
		?>		
		</table>
		<div  style="text-align: right;"><strong>Вкупен број на ваучери: <?php 
																				if($sum_br_vauceri_bank > 0)
																					echo "<a href='/smetkovodstvo_izvestaj/pdf?report_deal_id=0&report_type=bank&report_year=".$smetkovodstvoreport_year."&report_month=".$smetkovodstvoreport_month."&report_day=".$smetkovodstvoreport_day."'>"; 
																					
																					echo $sum_br_vauceri_bank; 
																					
																				if($sum_br_vauceri_bank > 0)	
																					echo "</a>"; 
																		 ?></strong></div>
		<div  style="text-align: right;"><strong>Вкупен приход: <?php echo $sum_vkupen_prihod_bank; ?> ден.</strong></div>
		<div  style="text-align: right;"><strong>Вкупна провизија: <?php echo $sum_vkupna_provizija_bank; ?> ден.</strong></div>
		<div  style="text-align: right;"><strong>ДДВ (18%): <?php $sum_ddv_bank = ($sum_vkupna_provizija_bank * 18)/100 ; echo $sum_ddv_bank; ?> ден.</strong></div>
		<div  style="text-align: right;"><strong>САЛДО: <?php $sum_saldo_bank = $sum_vkupna_provizija_bank - $sum_ddv_bank; echo $sum_saldo_bank; ?> ден.</strong></div>
		
		<br>
		<br>
		<br>
		
		<div  style="text-align: right;color: #007FFF;"><strong>Сумарен вкупен број на ваучери: <?php $sumaren_vkupen_broj_vauceri = ($sum_br_vauceri+$sum_br_vauceri_bank);
																									
																									if($sumaren_vkupen_broj_vauceri > 0)
																										echo "<a href='/smetkovodstvo_izvestaj/pdf?report_deal_id=0&report_type=&report_year=".$smetkovodstvoreport_year."&report_month=".$smetkovodstvoreport_month."&report_day=".$smetkovodstvoreport_day."'>"; 
																										
																										echo $sumaren_vkupen_broj_vauceri; 
																										
																									if($sumaren_vkupen_broj_vauceri > 0)
																										echo "</a>"; 
																								  ?></strong></div>
		<div  style="text-align: right;color: #007FFF;"><strong>Сумарен вкупен приход: <?php echo ($sum_vkupen_prihod+$sum_vkupen_prihod_bank); ?> ден.</strong></div>
		<div  style="text-align: right;color: #007FFF;"><strong>Сумарна вкупна провизија: <?php echo ($sum_vkupna_provizija+$sum_vkupna_provizija_bank); ?> ден.</strong></div>
		<div  style="text-align: right;color: #007FFF;"><strong>Сумарно ДДВ (18%): <?php echo ($sum_ddv+$sum_ddv_bank); ?> ден.</strong></div>
		<div  style="text-align: right;color: #007FFF;"><strong>Сумарно САЛДО: <?php echo ($sum_saldo+$sum_saldo_bank); ?> ден.</strong></div>
		
		
		<br>
		<br>
		<br>
		
		<!--
		<div  style="text-align: right;color: #007FFF;"><strong>Месечен број на ваучери (од клиенти ослободени од данок): 
																				<?php 
																					if($mesecenOslobodeniOdDanok[0]->cnt_vouchers > 0)
																						echo "<a href='/smetkovodstvo_izvestaj/pdf?report_deal_id=0&report_type=&report_year=".$smetkovodstvoreport_year."&report_month=".$smetkovodstvoreport_month."'>"; 

																					echo $mesecenOslobodeniOdDanok[0]->cnt_vouchers; 

																					if($mesecenOslobodeniOdDanok[0]->cnt_vouchers > 0)
																						echo "</a>"; 
																				?>
		
		</strong></div>
		<div  style="text-align: right;color: #007FFF;"><strong>Месечен вкупен приход (од клиенти ослободени од данок): <?php echo $mesecenOslobodeniOdDanok[0]->vkupen_prihod; ?> ден.</strong></div>
		-->
		
		<div  style="text-align: right;color: #007FFF;"><strong>Месечна вкупна провизија (од клиенти ослободени од данок): <?php echo $mesecenOslobodeniOdDanok[0]->vkupna_provizija; ?> ден.</strong></div>
		
		<!--
		<div  style="text-align: right;color: #007FFF;"><strong>Месечен ДДВ (18%) (од клиенти ослободени од данок): <?php $mesecenDDV = ($mesecenOslobodeniOdDanok[0]->vkupna_provizija*0.18); echo $mesecenDDV; ?> ден.</strong></div>
		<div  style="text-align: right;color: #007FFF;"><strong>Месечно САЛДО (од клиенти ослободени од данок): <?php $mesecenSaldo = ($mesecenOslobodeniOdDanok[0]->vkupna_provizija - $mesecenDDV); echo $mesecenSaldo; ?> ден.</strong></div>
		-->

  </div>
</div>