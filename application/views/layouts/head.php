<head>
	<title><?php echo html::specialchars($title); ?></title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />

	<!-- META TAGS FOR SEO -->
	<meta name="description" content="Kupinapopust.mk е веб страна која нуди голем број попусти на различни услуги и производи. Менито на нашата веб страна е составено од повеќе категории и под категории како што се: храна, банско, масажи, козметика, авто услуги, зумба фитнес, пилатес, спорт, патувања, книги, сметководство, театри, курсеви, игротека, фризерски услуги, нова година, астрологија, фотографија, здравје, IT технологија. Овие групни попусти се наменети за потребите на луѓето низ цела Македонија и пошироко. Ако славите роденден посетете ја категоријата игротека. Имате болки во грбот,главата или телото, ви предложувам да одите на масажа, да се релаксирате и опуштите. Сте станале мрзливи, неможете да станете од вашиот кревет, тогаш потребно ви е малку тренинг, малку зумба,пилатес, аеробик, зумба фитнес и нормално некаков вид на активен спорт. Размислувате каде за зимскиот одмор, за нова година или божиќните празници, не двоумете се и посетете го Банско. Ниските цени се предност за искористување на овие услуги. За безбедно патување користете зимска опрема или зимски гуми." />
	<meta name="keywords" content="Popusti, Popust, Popusti mk, Popust mk, Kupi na popust.mk, Попусти, Попуст, Сакам попуст, Групни попусти, Купи на попуст, Sakam popust, Kupi na popust, Sakampopust, Kupinapopust, Site popusti, Grupni popusti, Site popusti mk, Grupni popusti mk, Na popust, Храна, банско, bansko, банско понуди, bansko ponudi, фотографија, masaza, masaza mk, masaza vo skopje, masaza skopje, masazi skopje, масажа, масажи, масажа скопје, здравје, зумба, пилатес, zumba mk, zumba skopje, зумба фитнес, аеробик, авто услуги, sport mk, спорт, спорт мк, igroteki vo skopje, igroteki skopje, игротеки, Игротека, Зимски гуми, Gumi skopje, Zimski gumi, Depilacija, депилација, книги, курсеви, театри, патувања, фризерски услуги, нова година, сметководство, астрологија, производи, производи мк, proizvodi mk." />
	<meta name="author" content="Груп Поинт дооел е сопственик на веб страната kupinapopust.mk. Целта ни е да нудиме услуги и производи кои ги задоволуваат секојдневните потреби и желби на нашите корисници и тоа за безконкуретно ниски цени. Се трудиме да се однесуваме со нашите купувачи така, како што и ние самите посакуваме другите фирми да се однесуваат према нас. А тоа значи: - Нудиме попусти за услуги кои и самите ги посакуваме; - Немаме никакви тајни и скриени трошоци кои би предизвикале непријатност кај купувачите. Стратешки партнер на Груп Поинт е Компанија Др.Беловски дооел Скопје, Друштво за интелектуални услуги и менаџмент консалтинг." />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- END META TAGS FOR SEO -->

	<!-- META FOR FACEBOOK, G+ AND TWITTER -->
	<?php if (isset($fb_title) && $fb_title != "") : ?>
		<meta property="og:title" content="<?php print html::specialchars($fb_title); ?>" />
		<meta property="fb:app_id" content="<?php echo Kohana::config('facebook.facebookAppID'); ?>"/>
		<meta property="og:site_name" content="<?php print kohana::lang("prevod.website_title"); ?>" />
		<meta property="og:type" content="product" />
	<?php endif; ?>

	<?php if (isset($fb_link) && $fb_link != "") : ?>
		<meta property="og:url" content="<?php print html::specialchars($fb_link); ?>" />
		<link rel="canonical" href="<?php print html::specialchars($fb_link); ?>" />
	<?php endif; ?>

	<?php if (isset($fb_image) && $fb_image != "") : ?>
		<meta property="og:image" content="<?php print $fb_image; ?>" />
	<?php endif; ?>

	<?php if (isset($fb_description) && $fb_description != "") : ?>
		<meta property="og:description" content="<?php print html::specialchars($fb_description); ?>" />
	<?php endif; ?>
	<!-- END META FOR FACEBOOK, G+ AND TWITTER -->


	<link rel="shortcut icon" type="image/x-icon" href="/pub/img/layout/favikon-c.ico" />
	<link rel="shortcut icon" href="/pub/img/layout/favikon-c.ico" />

	<link rel="alternate" type="application/rss+xml" title="RSS" href="/rss" />

	<!-- Google fonts -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300' rel='stylesheet' type='text/css'>
	
	<!-- Bootstrap styles -->
	<link rel="stylesheet" href="/pub/css/boostrap.css?1">

	<!-- Za datetime picker -->
	<link rel="stylesheet" href="/pub/css/bootstrap-datetimepicker.css?1" />
	
	<!-- Font Awesome styles (icons) -->
	<link rel="stylesheet" href="/pub/css/font_awesome.css?1">
	
	<!-- Main Template styles -->
	<link rel="stylesheet" href="/pub/css/styles.css?1">
	
	<!-- IE 8 Fallback -->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="/pub/css/ie.css?1" />
	<![endif]-->

	<link rel="stylesheet" href="/pub/css/schemes/blaze-orange.css?1">

	<!-- Your custom styles (blank file) -->
	<link rel="stylesheet" href="/pub/css/mystyles.css?1">
	
	<!-- Style for jquery-toastmessage-plugin -->
	<link rel="stylesheet" href="/pub/css/jquery.toastmessage.css?1">

	<!-- Style for tables -->
	<link href="/pub/css/tables.css?1" rel="stylesheet" />

	<!-- Style for bootstrap Validator -->
	<link rel="stylesheet" href="/pub/css/bootstrapValidator.min.css?1"/>

	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
	<script src="/pub/js/jquery.js"></script>

	<!-- ova se koristi za datetime picker -->
	<script type="text/javascript" src="/pub/js/moment-with-locales.min.js"></script>
	<script type="text/javascript" src="/pub/js/bootstrap-datetimepicker.js"></script>
	
	<!-- ova se koristi za bootstrap Validator -->
	<script type="text/javascript" src="/pub/js/bootstrapValidator.min.js"></script>
	
	<script type="text/javascript">
	
		function AjaxResponse()
		{
			var myData = 'connect=1'; 
			jQuery.ajax({
				type: "POST",
				url: "<?php echo Kohana::config('facebook.facebookSiteUrlLogin') . (isset($fb_redirect_addition) && $fb_redirect_addition != "" ? "/" . $fb_redirect_addition : ""); ?>",
				dataType: "html",
				data: myData,
				success: function(response) {
					window.location.replace("<?php echo Kohana::config('facebook.facebookSiteProtocol').'://'.Kohana::config('facebook.facebookSiteURL');?>" + response); 
				},
				error: function (xhr, ajaxOptions, thrownError) {
					// alert("Настаната е грешка при логирањето. Ве молиме обидете се повторно.")
					alert("Почитувани, во моментот не е возможна регистрација со facebook. Ве молиме користете ја можноста за регистрација преку email адреса.")

					// alert(xhr.status);
			        // alert(thrownError);

				}
			});
		}
		 
		function LodingAnimate() //Show loading Image
		{
			$("#LoginButton").hide(); //hide login button once user authorize the application
			$("#results").html('<img style="width: 16px; height: 16px;" src="<?php echo Kohana::config('facebook.facebookSiteProtocol').'://' . Kohana::config('facebook.facebookSiteURL'); ?>/pub/img/layout/ajax-loader.gif" /> Ве молиме почекајте...'); //show loading image while we process user
		}

		function ResetAnimate() //Reset User button
		{
			$("#LoginButton").show(); //Show login button 
			$("#results").html(''); //reset element html
		}

		(function(l){var i,s={touchend:function(){}};for(i in s)l.addEventListener(i,s)})(document); // sticky hover fix in iOS

	</script>

	<!-- Chang URLs to wherever Video.js files will be hosted -->
	<link href="/pub/js/video-js/video-js.css?1" rel="stylesheet" type="text/css" />
	
	<!-- video.js must be in the <head> for older IEs to work. -->
	<script src="/pub/js/video-js/video.js"></script>

	<!-- Unless using the CDN hosted version, update the URL to the Flash SWF -->
	<script>
		videojs.options.flash.swf = "/pub/js/video-js/video-js.swf";

		/*
		$(function(){
		$("#webticker").show();
		$("#webticker").webTicker({speed: 40});
			
		});
		*/
	</script>

	<!-- Google Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-27557517-1', 'auto');
		ga('send', 'pageview');
	</script>
	<!-- End Google Analytics -->


	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','//connect.facebook.net/en_US/fbevents.js');

	fbq('init', '541704672650681');
	fbq('track', "PageView");
	
	// ViewContent
	// Track key page views (ex: product page, landing page or article)
	fbq('track', 'ViewContent');


	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=541704672650681&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
</head>
