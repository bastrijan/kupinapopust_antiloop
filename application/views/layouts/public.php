﻿<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php
	$user_email = $this->session->get("user_email");
	$controller = Router::$controller;
	$action = Router::$method;
	$arguments = Router::$arguments;

	$baner_location_1 = bannersystem::getBanner(1);
	// $baner_location_1 = "";
?>
<!DOCTYPE html>
<html>

	<!-- /////////////////////////////
	////////////// HEAD ////////////// 
	///////////////////////////////-->
    <?php require_once "head.php"; ?>

    <!-- GDPR POPUP START-->
    <?php 
		require_once "use_of_cookie_agreement.php"; 
    ?>
	<!-- GDPR POPUP END-->


	<!-- Ako saka da ima slika namesto pattern treba samo klasata na body-to da bide class="boxed bg-cover" -->
	<body class="boxed" >

		<div id="fb-root"></div>
		
		<script type="text/javascript">
			window.fbAsyncInit = function() {
			FB.init({appId: '<?php echo Kohana::config('facebook.facebookAppID');?>',version: 'v2.11',cookie: true,xfbml: true,channelUrl: '//<?php echo Kohana::config('facebook.facebookSiteURL');?>/pub/js/channel.php',oauth: true});};
			(function() {var e = document.createElement('script');
			e.async = true;e.src = document.location.protocol +'//connect.facebook.net/en_US/sdk.js';
			document.getElementById('fb-root').appendChild(e);}());

			function CallAfterLogin()
			{
					FB.login(function(response) 
					{		
						if (response.status === "connected") 
						{
							LodingAnimate(); //Animate login
							FB.api('/me', function(data) 
							{
								  // alert("id="+data.id);
								  // alert("email="+data.email);

								  if(data.id == null)
								  {
										//Facbeook user email is empty, you can check something like this.
										alert("За да може да успешно да се логирате, Ве молиме да ни дозволите да пристапиме до вашата E-mail адреса и Датум на роденден!"); 
										ResetAnimate();

								  }else{
										AjaxResponse();
								  }
						  });
						}
				});
			}


			<?php if (isset($welcome_again_status) && in_array($welcome_again_status, array(1, 3))  ) { ?>
			    $(document).ready(function () {

					$().toastmessage('showToast', {
						text     : 'Добредојдовте повторно на нашиот сајт!<br>Ве молиме кликнете <a target="_blank" href="/customer/login">тука</a> да се логирате на вашиот профил за да ви подариме 50 kupinapopust поени.',
						sticky   : true,
						position : 'top-center',
						type     : 'success'
					});
			    }) 
			<?php } ?>  

		</script>

		<div class="global-wrap">
			<!-- //////////////////////////////////
			////////////// MAIN HEADER////////////// 
			////////////////////////////////////-->
			
            <?php require_once "header.php"; ?>

			<!-- //////////////////////////////////
			//////////////PAGE CONTENT///////////// 
			////////////////////////////////////-->

			<!--  SIROK HORIZONTALEN BANNER -->
			<?php //if(empty($user_email) || $controller != "customer") { 
				$current_timestamp = time();
			?>
				
				<!--  HORIZONTALEN BANNER START-->
				<div style="text-align: center;" class="row mt20 ml10 mr10">
				
					
					<!-- BANNER ZA BLACK FRIDAY-->
					<!--
					<a target="_blank" href="<?php //echo ($current_timestamp >= strtotime(date("Y")."-11-29 00:00:00") ? "/deal/general_deal/17160" : "/static/page/black_friday"); ?>">			
						<img class="img_top_banner" src="/pub/img/bf2019.jpg?1">
					</a>
					<div class="gap gap-mini"></div>
					-->

					<!-- STANDARDEN BANNER	-->
					<?php
						$eurolink_insurance_type = NULL;

						//ako sme vo kategorija PROIZVODI togas prikazi domasno osiguruvanje (property)	
						if(($controller == 'all' && $action == "category" && isset($arguments[0]) && $arguments[0] == "patuvanja-25") || ($controller == 'all' && $action == "subcategory" && isset($arguments[0]) && $arguments[0] == "patuvanja-25"))
							$eurolink_insurance_type = "travel";  
						elseif( ($controller == 'all' && $action == "category" && isset($arguments[0]) && $arguments[0] == "proizvodi-20") || ($controller == 'all' && $action == "subcategory" && isset($arguments[0]) && $arguments[0] == "proizvodi-20"))
							$eurolink_insurance_type = "property";

					?>

				<?php if(!empty($eurolink_insurance_type)) { ?>
					<div class="hidden-md hidden-lg">
						<a href="https://shop.eurolink.com.mk/EurolinkWebShop/shop/wizard?type=<?php print $eurolink_insurance_type;?>&from=770" target="_blank" >			
							<img class="img_top_banner" src="/pub/img/layout/eurolink-dolg-banner-<?php print $eurolink_insurance_type;?>.jpg">
						</a>
					</div>

					<?php if($baner_location_1 != "" ) { ?>
							
							<div class="hidden-xs hidden-sm">
								<?php print $baner_location_1;?>
							</div>

					<?php } ?>

				<?php }else {//if($eurolink_insurance_type != "") { ?>
								<?php if($baner_location_1 != "" ) { ?>
							
										<div >
											<?php print $baner_location_1;?>
										</div> 

								<?php } ?>

								<?php 
								// require_once APPPATH . 'mobile_detect_master/Mobile_Detect.php';
								// $detect = new Mobile_Detect;

								// if( ($detect->isMobile() || $detect->isTablet()) && (strpos($_SERVER['REQUEST_URI'], "banner") === false) ) 
								// {

								?>
									<!-- 
									<div class="col-md-12"> 
									</div> 
									-->
								<?php
								// }
								// else
								// {
								?>
									<!-- 									
									<div style="margin: auto;  width: 82%">
									</div> -->
								<?php
								// }
								?>

				<?php }//else {//if($eurolink_insurance_type != "") { ?>
									
	
					

				<!-- HORIZONTALEN BANNER END -->
				</div>
				
				
			<?php //}//if(empty($user_email)) {  ?>




			<div id="content" class="container mt20">
				<?php if($controller == "customer" && isset($user_email) && $user_email != "") { ?>
					<div class="row">
						<div class="col-xs-12 text-center">
							<h4><strong>Најавен со: <?php echo $user_email; ?></strong></h4>
						</div>
					</div>
				<?php }//if(isset($user_email) && $user_email != "") { ?>

				<?php echo $content; ?>
            </div>

			<!-- //////////////////////////////////
			//////////////MAIN FOOTER////////////// 
			////////////////////////////////////-->
			<?php require_once "mobile_banner.php"; ?>
            <?php require_once "footer.php"; ?>

        </div>

    </body>
	
</html>