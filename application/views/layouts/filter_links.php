<?php
	$controller = Router::$controller;
	$action = Router::$method; 
	$arguments = Router::$arguments;

?>
<?php if($filter_links_include == 1) { ?>
	<script type="text/javascript">

		$(document).ready(function () {

		   //za toogle na kontrolata so kategorii
		    $("#toggle-filter").click(function () {
		        

		        var i_tag = $(this).children(".fa");

		        if(i_tag.hasClass("fa-plus-square"))
		        {
		            i_tag.removeClass("fa-plus-square");
		            i_tag.addClass("fa fa-minus-square");

		            $(".filterItem").removeClass("hidden");
		        }
		        else 
		            if(i_tag.hasClass("fa-minus-square"))
		            {
		                i_tag.removeClass("fa-minus-square");
		                i_tag.addClass("fa fa-plus-square");

		                $(".filterItem").addClass("hidden");
		            }   
		       
		    });		

		});


			
	</script>
<?php }//if($filter_links_include == 1) { ?>


<?php if($filter_links_include == 1) { ?>
		<div>
			<a class="prevzemiVaucher" id="toggle-filter"><i class="fa fa-plus-square"></i> ПОДРЕДИ ПО:</a>
		</div>
<?php }//if($filter_links_include == 1) { ?>

<ul class="nav nav-tabs nav-stacked nav-coupon-category nav-coupon-category-left <?php echo ($filter_links_include == 1 ? "hidden filterItem" : "hidden-xs hidden-sm")  ; ?>" style="margin-bottom: 0px;">

	<li class="<?php echo  ($controller == 'all' && $action == "index" && isset($arguments[0]) && $arguments[0] == 6 ) ? "active" : ""; ?>"><a href="/all/index/6"><i class="fa fa-file"></i>Бесплатни купони</a></li>
	
	<li class="<?php echo  ($controller == 'all' && $action == "index" && isset($arguments[0]) && $arguments[0] == 7 ) ? "active" : ""; ?>"><a href="/all/index/7"><i class="fa fa-money"></i>Купон + доплата</a></li>

	<li class="<?php echo  ($controller == 'all' && $action == "index" && isset($arguments[0]) && $arguments[0] == 8 ) ? "active" : ""; ?>"><a href="/all/index/8"><i class="fa fa-file-o"></i>Ваучери</a></li>

	<li class="<?php echo  ($controller == 'all' && $action == "index" && isset($arguments[0]) && $arguments[0] == 1 ) ? "active" : ""; ?>"><a href="/all/index/1"><i class="fa fa-eye"></i>Најнови</a></li>

	<li class="<?php echo  ($controller == 'all' && $action == "index" && isset($arguments[0]) && $arguments[0] == 2 ) ? "active" : ""; ?>"><a href="/all/index/2"><i class="fa fa-shopping-cart"></i>Најпродавани</a></li>

	<li class="<?php echo  ($controller == 'all' && $action == "index" && isset($arguments[0]) && $arguments[0] == 3 ) ? "active" : ""; ?>"><a href="/all/index/3"><i class="fa fa-clock-o"></i>При крај</a></li>

	<li class="<?php echo  ($controller == 'all' && $action == "index" && isset($arguments[0]) && $arguments[0] == 4 ) ? "active" : ""; ?>"><a href="/all/index/4"><i class="fa fa-sort-numeric-asc"></i>Најниска цена</a></li>

	<li class="<?php echo  ($controller == 'all' && $action == "index" && isset($arguments[0]) && $arguments[0] == 5 ) ? "active" : ""; ?>"><a href="/all/index/5"><i class="fa fa-sort-numeric-desc"></i>Највисока цена</a></li>


</ul>