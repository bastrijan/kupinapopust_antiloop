<?php
	$controller = Router::$controller;
	$action = Router::$method;
?>

<div class="hidden-xs hidden-sm">
<?php
	require APPPATH . 'views/layouts/static_links.php';
?>
</div>

<?php
	if(in_array($controller, array("card", "static") ) )
		require_once APPPATH . 'views/layouts/banner.php';
?>