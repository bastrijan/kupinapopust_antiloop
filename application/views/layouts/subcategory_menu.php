<div class="product-sort">
	<span class="product-sort-selected">
		<?php print $categoryObjectData->subcat_filter_title != "" ? $categoryObjectData->subcat_filter_title : "филтрирај по поткатегорија";?>
		<?php //print "филтрирај по <b>поткатегорија</b>";?>
	</span>

	<span class="product-sort-order fa fa-angle-down"></span>
	<ul>
		<?php foreach ($subCategoriesArray as $subcategory) { ?>
			<li>
				<a href="/all/subcategory/<?php echo seo::url_slug($subcategory->category_name, array('transliterate' => true))."-".$subcategory->category_id; ?>/<?php print seo::url_slug($subcategory->name, array('transliterate' => true))."-".$subcategory->id; ?>">
					<?php echo $subcategory -> name; ?>
					<span><?php echo $subcategory -> cnt_deals; ?></span>
				</a>
			</li>
		<?php } ?>                                
	</ul>
</div>