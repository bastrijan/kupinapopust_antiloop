
<script type="text/javascript" src="/pub/js/pay_android.js"></script>

<div id="demo" data-role="page">
    <div data-role="header">
        <h1>ПОПУСТИ ДО ДУРИ - 90% НА РАЗЛИЧНИ УСЛУГИ И ПРОИЗВОДИ</h1>
    </div>
    <div data-role="main" class="ui-content">
        <p>ПРИЈАВЕТЕ ГО ВАШИОТ E-MAIL ЗА ДА НЕ ВЕ ОДМИНЕ НИТУ ЕДНА ПОНУДА</p>
        
        <input type="hidden" name="invalidmail" id="invalidmail" value="<?php print kohana::lang("index.невалиден маил"); ?>"/>
        <input type="hidden" name="defaulttext" id="defaulttext" value="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>"/>
        
        
        <div data-role="popup" id="invalidmailPopup" class="ui-content">
            <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
            <?php print kohana::lang("index.невалиден маил"); ?>
        </div>
       
        <form id="paymentform" method="post" action="" data-ajax="false">
            <div class="section-form">
            
            <?php if (isset($error)) { ?>
                <div style="color: red; padding: 20px">
                    <?php print $error; ?>
                </div>
            <?php } ?>
            <?php if (isset($success)) { ?>
                <div style="color: green; padding: 20px">
                    <?php print $success; ?>
                </div>
            <?php } ?>
            <?php if(isset($success)){}else{ ?>    
                <input id="Email" type="text" placeholder="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>" name="Email" class="text" value=""><br/>
                <input type="submit" style="width: 100%; text-align:center;margin-top: 10px;" id="pay" value="<?php echo kohana::lang("prevod.Потврди"); ?>" name="send" class="submit button">
                </div>
            <?php } ?>
        </form>
    </div>
</div>

