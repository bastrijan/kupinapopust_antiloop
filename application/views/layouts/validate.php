﻿<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<!DOCTYPE html>
<html>

	<!-- /////////////////////////////
	////////////// HEAD ////////////// 
	///////////////////////////////-->
    <?php require_once "head.php"; ?>
	

	<!-- Ako saka da ima slika namesto pattern treba samo klasata na body-to da bide class="boxed bg-cover" -->
	<body class="boxed" >
		<div class="global-wrap">

			<!-- //////////////////////////////////
			//////////////MAIN HEADER////////////// 
			///////////////////////////////////////-->

			<header class="main">
				<div class="container bg-white width100 my-search-area">
					<div class="row ml0 mr0">
						
						<div class="col-md-12 bg-white">
							<a href="/" class="logo mt5 mb5 text-center" data-toggle="tooltip" data-placement="bottom" data-title="Почетна страна">
			                    <img src="/pub/img/logo5.png" alt="Kupinapopust.mk logo" height="40"/>
			                </a>
						</div>


					</div>

				</div>
			</header>


			<!-- //////////////////////////////////
			//////////////END MAIN HEADER////////// 
			///////////////////////////////////////-->


			<!-- //////////////////////////////////
			//////////////MAIN CONTENT////////////// 
			///////////////////////////////////////-->

			<div id="content" class="container mt20" style="max-width: 600px; text-align: center;">
				<?php echo $content; ?>
	        </div>

			<!-- //////////////////////////////////
			//////////////END MAIN CONTENT////////// 
			///////////////////////////////////////-->


			<!-- /////////////////////////////////////////
				 ////////////// MAIN FOOTER ////////////// 
				 //////////////////////////////////////-->
			<footer class="main">
				<div class="footer-copyright">
					<div class="container">
						<div class="col-md-12 text-center">
								<p>2011 - <?php echo date("Y"); ?> © Kupinapopust.mk <?php print kohana::lang("prevod.Сите права задржани"); ?></p> 
						

								<a  href="/"><i class="fa fa-home"></i> Кон kupinapopust.mk</a>
								<a  href="/static/page/kontakt"><i class="fa fa-star"></i> <?php print kohana::lang("prevod.Контакт"); ?></a>
	
						
						</div>
					</div>
				</div>
			</footer>

			<!-- Scripts queries -->
			<script src="/pub/js/jquery.cookie.js"></script>
			<script src="/pub/js/boostrap.min.js"></script>
			<script src="/pub/js/countdown.min.js"></script>

			<!-- za mobile meni -->
			<script src="/pub/js/flexnav.min.js"></script>

			<!-- za pop up, ako ne se koristi da se izvadi --> 
			<script src="/pub/js/magnific.js"></script>


			<!-- <script src="js/tweet.min.js"></script> -->
			<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

			<!-- ako ti treba za responsive video-->
			<!--
			<script src="js/fitvids.min.js"></script>
			-->

			<!-- <script src="js/mail.min.js"></script> -->
			<script src="/pub/js/ionrangeslider.js"></script>
			<script src="/pub/js/icheck.js"></script>
			<script src="/pub/js/fotorama.js"></script> 
			<!-- <script src="js/card-payment.js"></script> -->
			<script src="/pub/js/owl-carousel.js"></script>

			<script src="/pub/js/masonry.js"></script>

			<!-- ova e za scroll to top, patekata do slikickata se naoga vo samata skripta, ako ja menuvas i tamu treba da se smeni -->
			<script src="/pub/js/back-to-top.js"></script>

			<!-- ovaa valda ja imas -->
			<script src="https://apis.google.com/js/platform.js" async defer></script>
			<script src="https://platform.twitter.com/widgets.js" type="text/javascript"></script>

			<!-- skripti od kupi na popust -->

			<!-- ovaa ja kaciv najgore (ima nekoi raboti sto se koristat od 1.7, no od 1.9 navaka e standard za responsive dizajn da raboti kako sto treba) -->
			<!-- <script type="text/javascript" src="http://kupinapopust.mk/pub/js/jquery-1.7.1.min.js"></script> -->

			<!--
			<script type="text/javascript" src="/pub/js/jquery-ui.min.js" ></script> 
			<script type="text/javascript" src="/pub/js/jquery-ui-timepicker-addon.js"></script>
			-->


			<!--
			<script type="text/javascript" src="/pub/js/ui/jquery.ui.dialog.js" ></script>
			-->

			<!-- ima druga skripta za countdown, dokolku ne ti funkcionira so nea, mozes da ja smenis -->
			<!-- <script type="text/javascript" src="http://kupinapopust.mk/pub/js/jquery.countdown.js"></script>
			<script type="text/javascript" src="http://kupinapopust.mk/pub/js/jquery.countdown-mk.js"></script> -->

			<!--
			<script type="text/javascript" src="/pub/js/jquery.notice.js"></script>
			<script type="text/javascript" src="/pub/js/jquery.jeditable.js"></script>
			<script type="text/javascript" src="/pub/js/jquery.webticker.min.js"></script>
			-->

			<!-- Custom scripts -->
			<script src="/pub/js/custom.js"></script>

			<!-- jquery-toastmessage-plugin -->
			<script src="/pub/js/jquery.toastmessage.js"></script>

			<script src="/pub/js/favourites.js" charset="UTF-8"></script>
			<script type="text/javascript" src="/pub/js/shop_cart.js" charset="UTF-8"></script>

			<!-- //////////////////////////////////
			//////////////END MAIN  FOOTER///////// 
			////////////////////////////////////-->

        </div>
    </body>
	
</html>