<?php defined('SYSPATH') OR die('No direct access allowed.'); 

if(!isset($controller))
	$controller = Router::$controller;

if(!isset($action))
	$action = Router::$method;

// $arguments = Router::$arguments;



	if($controller == 'index' && $action == 'index')
	{
		
		require_once APPPATH . 'mobile_detect_master/Mobile_Detect.php';

		$detect = new Mobile_Detect;

		if($detect->isMobile() && (strpos($_SERVER['REQUEST_URI'], "banner") === false) )  
		{
			$baner_location_4 = bannersystem::getBanner(4);
			// $baner_location_4 = "";


			if($baner_location_4 != "" ) 
			// if(1) 	
			{ 
?>

				<style type="text/css">

				.mobile-banner{
				    position: fixed;
				    bottom: 5px;
				    width: 320px;
				    z-index: 3000;

					left: 50%;
					transform: translateX(-50%);
				}

				#banner-mobile-close-button{
					position:absolute; 
					top:-15px; 
					right:-13px;
					z-index: 3000;
				    color: #CC0000;
				    font-size: 30px;
				}

				.circle-icon {
				    background: #fff;
				    width: 23px;
				    height: 23px;
				    border-radius: 50%;
				    text-align: center;
				    line-height: 23px;

				}
				</style>


				<script type="text/javascript">
					$(document).ready(function() {
						$(".mobile-banner").fadeIn(1000);

						window.setTimeout(mobileBannerTurnOff, 10000);

						// $( "#mobilen-banner-test" ).click(function(event ) {
						// 	  event.preventDefault();
						// 	  $(".mobile-banner").fadeIn(5000);
						// });

						$( "#banner-mobile-close-button" ).click(function(event ) {
							  event.preventDefault();
					
							  mobileBannerTurnOff();
						});
					});
 

		            function mobileBannerTurnOff()
		            {
		            	$(".mobile-banner").fadeOut(1500);
		            }

				</script>

				<div class="mobile-banner" style="display: none;">
					<i class="fa fa-times-circle circle-icon" id="banner-mobile-close-button" ></i>
					<?php print $baner_location_4;?>
				</div>


<?php 		}//if($baner_location_4 != "" ) 
		}//if($detect->isMobile() && (strpos($_SERVER['REQUEST_URI'], "banner") === false) )  
	}//if($controller != 'customer')
?>