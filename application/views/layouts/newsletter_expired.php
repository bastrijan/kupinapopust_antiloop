<script type="text/javascript">

  function validate(email) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(email) == false) {
      alert("<?php print Kohana::lang("index.невалидeн e-mail")?>")
      return false;
    }
    return true;
  }
  $(document).ready(function() {
   
    $("#potvrdi_email").click(function (){
      if ($("#email_field").val() != '' && validate($("#email_field").val()) ) {
        $.post(
        "/ajax/newsletter",
        $("#newsletter_form").serialize(),
        function(data) {
 
          if (data.resp == 'success') {
            $.noticeAdd({
              text: "<?php print Kohana::lang("index.Успешно го пријавивте вашиот e-mail!")?>",
              stay: false,
              type:'notification-success'
            });
          } else {
                    
            $.noticeAdd({
              text: "<?php print Kohana::lang("index.Вашиот e-mail веќе е пријавен и вие треба да добивате понуди од Kupinapopust")?>",
              stay: false,
              type:'notification-success'
            });
          }
                   
        },
        "json");

        defaultText = '<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса") ; ?>';
        $("#email_field").val(defaultText);
      }
      return false;
        
    })

    $("#email_field").focus(function (){
      defaultText = '<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса") ; ?>';
      if ($(this).val() == defaultText) {
        $(this).val("");
      }
    })
    $("#email_field").blur(function (){
      defaultText = '<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса") ; ?>';
      if ($(this).val() == "") {
        $(this).val(defaultText);
      }
    })
  });
</script>

<style type="text/css">
    .section-newsletter {
        background:red;
        border:1px solid #ccc8ad;
        color: white;
        /*margin-top: 30px*/
    }

    .section-newsletter h2 {
        width: 500px;
    }
</style>
<div class="section-newsletter">
    <h2>
        <?php print kohana::lang("prevod.Zakasnivte! Ponudata e zavrsena!"); ?>
        <br>
        <?php print kohana::lang("prevod.Prijavi go tvojot email"); ?>
    </h2>
    <form id="newsletter_form" method="post" action="">
        <span>
            <input id="email_field" type="text" value="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>" name="mail" class="text">
            <input id="potvrdi_email" type="button" value="<?php print kohana::lang("prevod.Prijavi"); ?>" name="send" class="submit button">
        </span>
        <br>
        <span style="font-size: 12px"><?php print kohana::lang("prevod.Во било кое време можете вашиот e-mail да го одјавите"); ?></span>
    </form>
</div>