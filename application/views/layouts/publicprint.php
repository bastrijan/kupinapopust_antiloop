<?php defined('SYSPATH') OR die('No direct access allowed.') ; ?>
<!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#">
  <?php require_once "head.php" ; ?>
  <?php
  $defaultModel = new Default_Model() ;
  $ie = $defaultModel->detectIE() ;
  ?>
  <body id="layout">
    <div id="content" class="container_12" style="margin-left: auto; margin-right: auto">
      <div id="<?php if ($ie)
        print 'ie' ; ?>" class="" style="min-height: 700px">
           <?php echo $content ?>
      </div>
    </div>
  </body>
</html>