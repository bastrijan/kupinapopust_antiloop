<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php 
$sessionPartnerID = $this->session->get("user_id"); 
	$controller = Router::$controller;
	$action = Router::$method;
?>

<?php
	/*
	$controller = Router::$controller;
	$action = Router::$method;

	$user_email = $this->session->get("user_email");
	if (!empty($user_email)) {
		$text = $this->session->get("user_email");
		$menuClass = 'logedin';
	} else {
		$text = "Најави се";
		$menuClass = '';
	}
	*/
	
?>
		 
<div class="top-main-area gradient ptb0">
	<div class="container pr0">
		<div class="col-md-8 pl0">
			<h1 class="orange mb0 mt20" style="font-family:'Open Sans';"> KUPINAPOPUST.MK PARTNER ADMINISTRATION</h1>
		</div>
		
		<div class="col-md-4 pr0">
			<a href="/" target="_blank" class="logo mt0 pull-right padding-media">
				<img src="/pub/img/logo2.png" height="50" alt="Kupinapopust Logo" title="Kupinapopust Logo" />
			</a>
		</div>
	</div>
</div>

<header class="main">
	<div class="container">
		<div class="gap-small hidden-md hidden-lg"></div>
		<div class="row">
			
			<div class="col-md-8">
				<!-- MAIN NAVIGATION -->
				<div class="flexnav-menu-button" id="flexnav-menu-button">Мени</div>
				<nav>
					<ul class="nav nav-pills flexnav" id="flexnav" data-breakpoint="800">

						<?php if($sessionPartnerID != "") { ?>
						<li <?php if($action == "index") echo 'class="active"'; ?> ><a href="/partner">Мои понуди</a></li>
						<?php }//if($sessionPartnerID != "") { ?>

						<li ><a href="/partner/confirm_as_paid/" >Потврда на исплата</a></li>

						<li ><a href="/" target="_blank">Кон kupinapopust.mk</a></li>
						
						<?php if($sessionPartnerID != "") { ?>
						<li <?php if($action == "changepassword") echo 'class="active"'; ?> ><a href="/partner/changepassword" >Промени лозинка </a></li>
						<?php }//if($sessionPartnerID != "") { ?>

						<?php if($sessionPartnerID != "") { ?>
						<li class="ml20"><a class="" href="/partner/logout/<?php print base64_encode("/partner"); ?>" data-effect="mfp-move-from-top"><i class="fa fa-sign-out"></i>Одјави се</a></li>
						<?php }//if($sessionPartnerID != "") { ?>
					</ul>
				</nav>
				<!-- END MAIN NAVIGATION -->
			</div>
			
			<div class="col-md-4 pr0">
				<!-- LOGIN REGISTER LINKS -->
				<ul class="login-register">
					 <li>
						<a target="_blank" href="https://www.facebook.com/Kupinapopust.mk" data-toggle="tooltip" data-placement="bottom" title="Следи не на Facebook">
							<img src="/pub/img/fbround25x25.png" alt="Facebook" />
						</a>
					</li>
					<li>
						<a target="_blank" href="https://twitter.com/kupinapopust" data-toggle="tooltip" data-placement="bottom" title="Следи не на Twitter">
							<img src="/pub/img/tw25x25.png" alt="Twitter" />
						</a>
					</li>
					 <li>
						<a target="_blank" href="https://plus.google.com/+KupinapopustMk/posts" data-toggle="tooltip" data-placement="bottom" title="Следи не на Google+">
							<!-- <img align="center" src="/pub/img/googleplus.png"> -->
							<img src="/pub/img/googleplus.png" alt="Google+" />
						</a>
					</li>
					<!--
					<li>
						<a target="_blank" href="#" data-toggle="tooltip" data-placement="bottom" title="Превземи android апликација">
							<img src="/pub/img/androidIcon1.png" class="icon-size" alt="Android" /> Android
						</a>
					</li>
					<li>
						<a target="_blank" href="#" data-toggle="tooltip" data-placement="bottom" title="Превземи iOS апликација">
							<img src="/pub/img/appleIcon1.png" class="icon-size" alt="Iphone" /> Iphone
						</a>
					</li>
					-->
				</ul>
			</div>
		</div>
	</div>
</header>

<!-- ////////////////////////////////////////
////////////// END MAIN HEADER ////////////// 
//////////////////////////////////////////-->