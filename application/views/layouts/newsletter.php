<script type="text/javascript">

  function validate(email) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(email) == false) {
      alert("<?php print Kohana::lang("index.невалидeн e-mail")?>")
      return false;
    }
    return true;
  }
  $(document).ready(function() {

        $("#email_field").keypress(function(e) {
            if(e.keyCode == 13) {
                $('#potvrdi_email').click();
                return false;
            }
        });
   
    $("#potvrdi_email").click(function (){
      if ($("#email_field").val() != '' && validate($("#email_field").val()) ) {
        $.post(
        "/ajax/newsletter",
        $("#newsletter_form").serialize(),
        function(data) {
 
          if (data.resp == 'success') {
            $.noticeAdd({
              text: "<?php print Kohana::lang("index.Успешно го пријавивте вашиот e-mail!")?>",
              stay: false,
              type:'notification-success'
            });
          } else {
                    
            $.noticeAdd({
              text: "<?php print Kohana::lang("index.Вашиот e-mail веќе е пријавен и вие треба да добивате понуди од Kupinapopust")?>",
              stay: false,
              type:'notification-success'
            });
          }
                   
        },
        "json");

        defaultText = '<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса") ; ?>';
        $("#email_field").val(defaultText);
      }
      return false;
    });

    $("#email_field").focus(function (){
      defaultText = '<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса") ; ?>';
      if ($(this).val() == defaultText) {
        $(this).val("");
      }
    })
    $("#email_field").blur(function (){
      defaultText = '<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса") ; ?>';
      if ($(this).val() == "") {
        $(this).val(defaultText);
      }
    })
  });
</script>

<div class="section-newsletter" style="width: 335px; padding: 0px 0px 3px 0px; margin: 2px; background: #FFFFCC; text-align: center">
	<span style="padding: 0px">
		<?php print kohana::lang("prevod.sakam_ponidi_na_email"); ?>
	</span>
	<form id="newsletter_form" method="post" action="" style="width: 300px; padding: 0px 0px 0px 23px">
		<span>
			<input style="padding: 2px;" id="email_field" type="text" value="<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса"); ?>" name="mail" class="text" />
			<input style="padding: 2px" id="potvrdi_email" type="button" value="<?php print kohana::lang("prevod.Потврди"); ?>" name="send" class="submit button" />
		</span>
	</form>
</div>