<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <link href="/pub/js/datepicker_android/new/css/mobipick.css" rel="stylesheet" type="text/css" />
    <link href="/pub/js/datepicker_android/new/demo/shCoreDefault.css" rel="stylesheet" type="text/css" />
    <link href="/pub/js/datepicker_android/new/demo/shThemeDefault.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/pub/js/datepicker_android/new/external/jquery.mobile-1.3.0.min.css" />
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.4/jquery.mobile-1.4.4.min.css">
    <link rel="stylesheet" href="/pub/css/jquery.notice.css" media="screen">



        
     <style>
        body{
            margin: 0;
            padding: 0;
            font-family: Arial, sans-serif;
        }
        .clear{
            clear: both;
        }

        div.homepage-content{
            display: block;
            width: 100%;
        }
        div.deals-matrix{
            padding: 4%;
            background-color: #E5E5E5;
        }
        div.deal-item {
            position: relative;
            border-radius: 7px;
            background-color: #fff;
            border: 1px solid #ccc;
            margin-top: 12px;
            display: block;
        }
        div.deal-item img{
            border-radius: 7px;
        }
        div.item-description{
            padding: 5px;
        }
        div.deal-item h3 {
            padding: 5px;
            margin: 0;
            color: #000000;
        }

        div.deal-item a {
            color: #000000;
            text-decoration: none;
        }

        div.deal-item img{
            width: 100%;
            display: block;
        }

        span.discount{
            display: block;
            position: absolute;
            left: -1%;
            top: 8%;
            padding: 1px 8px 0 12px;
            white-space: nowrap;
            font-size: 15px;
            font-weight: bold;
            line-height: 28px;
            color: #fff;
            background-color: #ff6600;
            border-radius: 0 5px 5px 0;
        }
        span.newoffer{
            display: block;
            position: absolute;
            right: -1%;
            top: 8%;
            padding: 1px 8px 0 12px;
            white-space: nowrap;
            font-size: 16px;
            font-weight: bold;
            line-height: 30px;
            color: #fff;
            background-color: #cc0f0f;
            border-radius: 5px 0px 0px 5px;
            cursor: default;
        }

        /*Regitration page*/
        p.date_description, span.checkbox_desc{
            font-size: 14px;
        }
        div.layout-registracija, div.layout-buy, div.layout-gift{
            margin: 12px;
        }
        a.ui-btn{
            font-size: 14px;
            padding: 8px;
        }
        /*END Registration page*/

        div.ui-content hr{
            display: block; height: 1px;
            border: 0; border-top: 1px solid #ff6600;
            margin: 1em 0; padding: 0;
        }

        .ui-header .ui-title { margin-left: 1em; margin-right: 1em; }
        
        .mobipick-date-formatted{
            color: #fff;
            text-shadow: 0;
            font-weight: 400;
        }
        .mobipick-buttons a, .mobipick-main a{
            color: #ff6600!important;
            
        }
        
        /* Fixed Share button */
        #share_btn_bottom{
            position: fixed; 
            bottom: 6%; 
            right: 6%; 
            background: gray; 
            border-radius: 35px; 
            padding: 10px 12px 10px 8px;
            opacity: 0.7;
        }
        #share_btn_bottom:hover{
           opacity: 1;
        }

    </style>
    
        <!--Datepicker Mobile Scripts-->
    <!--<script src="/pub/js/datepicker_android/lib/picker.js"></script>
    <script src="/pub/js/datepicker_android/lib/picker.date.js"></script>
    <script src="/pub/js/datepicker_android/lib/legacy.js"></script>-->
       


	<script type="text/javascript" src="/pub/js/datepicker_android/new/demo/shCore.js"></script>
	<script type="text/javascript" src="/pub/js/datepicker_android/new/demo/shBrushJScript.js"></script>
	<script type="text/javascript" src="/pub/js/datepicker_android/new/demo/shBrushXml.js"></script>
	<script type="text/javascript" src="/pub/js/datepicker_android/new/external/modernizr.custom.min.js"></script>
        <script type="text/javascript" src="/pub/js/datepicker_android/new/external/xdate.js"></script>
        <script type="text/javascript" src="/pub/js/datepicker_android/new/external/xdate.i18n.js"></script>
        <!--<script type="text/javascript" src="external/jquery-1.9.1.min.js"></script>-->
	<!--<script type="text/javascript" src="external/jquery.mobile-1.3.0.min.js"></script>-->
        
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="http://code.jquery.com/mobile/1.4.4/jquery.mobile-1.4.4.min.js"></script>
		<script type="text/javascript" src="/pub/js/jquery.notice.js"></script>
        <script type="text/javascript" src="/pub/js/datepicker_android/new/js/mobipick.js"></script>
    
	
    <!--End Datepicker Mobile Scripts-->
    

    
 
   


    <script type="text/javascript" >
		/*
        $(document).on('pagebeforecreate', '[data-role="page"]', function(){     
            var interval = setInterval(function(){
                $.mobile.loading('show');
                clearInterval(interval);
            },1);    
        });

        $(document).on('pageshow', '[data-role="page"]', function(){  
            var interval = setInterval(function(){
                $.mobile.loading('hide');
                clearInterval(interval);
            },300);      
        });
		*/
    
    
        $(document).on('click', '#pagination-btn', function () {

            var pagination_cnt = $('#pagination-cnt').val();
            var dateTimeUnique = $('#dateTimeUnique').val();
            var url = document.URL;

            $.ajax({
                url: url,
                data: {
                    pagination_cnt: pagination_cnt,
                    dateTimeUnique: dateTimeUnique,
                    type: "ajax"
                },
                enctype: 'multipart/form-data',
                dataType: 'json',
                type: 'POST',
                error: function () {
                    document.title = 'error';
                },
                success: function (data) {
                    if (data && data.status == 'success') {

                        $('input[name="pagination-cnt"]').val(data.pagination_cnt);
                        $('div.deals-matrix').append(data.html);
                        
                        if(data.ajax_btn < 10){
                            $('#pagination-btn').hide();
                        }

                    } else {
                        alert('Се појави грешка!');
                    }
                }
            });
        });
        
        $(document).on('click', '#pagination-search-btn', function () {

            var pagination_search_cnt = $('#pagination-search-cnt').val();
            var search_keyword = $('#search-keyword').val();
            var dateTimeUnique = $('#dateTimeUnique').val();
            var url = document.URL;

            $.ajax({
                url: url,
                data: {
                    pagination_search_cnt: pagination_search_cnt,
                    dateTimeUnique: dateTimeUnique,
                    type: "ajax"
                },
                enctype: 'multipart/form-data',
                dataType: 'json',
                type: 'POST',
                error: function () {
                    document.title = 'error';
                },
                success: function (data) {
                    if (data && data.status == 'success') {
                        $('input[name="pagination-search-cnt"]').val(data.pagination_search_cnt);
                        $('div.deals-matrix').append(data.html);
                        
                        if(data.ajax_btn < 10){
                            $('#pagination-search-btn').hide();
                        }
                        
                    } else {
                        alert('Се појави грешка!');
                    }
                }
            });
        });


        $(document).on('click', '.deal-item', function () {

//          $(this).css({background: '#ff6600'});
            var item_id = $(this).attr('id').match(/[\d]+$/);
            var myInteger = parseInt(item_id);

            //alert(item_id);
            window.CategoriesHandlerDeal.setDealData(myInteger);

        });

        $(document).on('click', '.bay_btn', function () {

            var item_id = $(this).attr('id').match(/[\d]+$/);
            var deal_id = parseInt(item_id);
//		alert(deal_id);
            window.DealHandlerDealBuy.buyDeal(deal_id);

        });

        $(document).on('click', '.gift_btn', function () {

            var item_id = $(this).attr('id').match(/[\d]+$/);
            var deal_id = parseInt(item_id);
//		alert(deal_id);
            window.DealHandlerDealBuy.buyGift(deal_id);

        });
        
        $(document).on('click', '.goto_qrcode', function () {
            
            var item_id = $('#deal-id').val();
            var code_nb = $(this).attr('id').match(/[\d]+$/);
            var deal_id = parseInt(item_id);
//		alert(deal_id);
            var voucher = $("#voucher-"+code_nb).text();
//            alert(voucher);
            window.MyHandlerListCodes.PrintCode(voucher, deal_id);

        });
        
    </script>

</head>

