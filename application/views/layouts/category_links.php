<?php
	$controller = Router::$controller;
	$action = Router::$method; 
	$arguments = Router::$arguments;


	if(isset($arguments[0]))
		$selectedCategory = seo::PermaLinkConversion($arguments[0]);

	

	$dealsModel = new Deals_Model();
	$dealsCategory = $dealsModel->getAllCategoriesWithOffers();	

	$vkupno_ponudi = 0;
	foreach ($dealsCategory as $category) { 
		$vkupno_ponudi += $category->cnt_deals;
	}

?>
<?php if($category_links_include == 1) { ?>
	<script type="text/javascript">

		$(document).ready(function () {

		   //za toogle na kontrolata so kategorii
		    $("#toggle-categories").click(function () {
		        

		        var i_tag = $(this).children(".fa");

		        if(i_tag.hasClass("fa-plus-square"))
		        {
		            i_tag.removeClass("fa-plus-square");
		            i_tag.addClass("fa fa-minus-square");

		            $(".categoryItem").removeClass("hidden");
		        }
		        else 
		            if(i_tag.hasClass("fa-minus-square"))
		            {
		                i_tag.removeClass("fa-minus-square");
		                i_tag.addClass("fa fa-plus-square");

		                $(".categoryItem").addClass("hidden");
		            }   
		       
		    });		

		    <?php //if($controller == 'all') { ?>
			            //$("#toggle-categories").children(".fa").removeClass("fa-plus-square");
			            //$("#toggle-categories").children(".fa").addClass("fa fa-minus-square");
		    	  		//$(".categoryItem").removeClass("hidden");

			    	  		
		    <?php //} //if($controller != 'all') ?>
		});


			
	</script>
<?php }//if($category_links_include == 1) { ?>


<?php if($category_links_include == 1) { ?>
		<div>
			<a class="prevzemiVaucher" id="toggle-categories"><i class="fa fa-plus-square"></i> ПОНУДИ ПО КАТЕГОРИИ</a>
		</div>
<?php }//if($category_links_include == 1) { ?>

<ul class="nav nav-tabs nav-stacked nav-coupon-category nav-coupon-category-left <?php echo ($category_links_include == 1 ? "hidden categoryItem" : "hidden-xs hidden-sm")  ; ?>" style="margin-bottom: 0px;">

	<li class="<?php echo (($controller == 'all' && $action == "index" && !isset($arguments[0])) || ($controller == 'index' && $action == "index")) ? "active" : ""; ?>"><a href="/all"><i class="fa fa-ticket"></i>Сите понуди<span><?php echo $vkupno_ponudi; ?></span></a></li>
	<?php foreach ($dealsCategory as $category) {
		$style = ($controller == 'all' && in_array($action, array('category', 'subcategory')) && $category->id == $selectedCategory) ? "active" : "";
	?>
		<li class="<?php echo $style; ?>">
			<a href="/all/category/<?php print seo::url_slug($category->name, array('transliterate' => true)) . "-" . $category->id; ?>">
				<?php
						$icon = trim($category->icon_html) != "" ? trim($category->icon_html)  : "fa-star";
				?>
						<i class="fa <?php echo $icon; ?>"></i>
					    <?php echo ($category->render_bold == 1 ? "<strong style='font-size: larger;'>": "").$category->name.($category->render_bold == 1 ? "</strong>": ""); ?>

						<span><?php echo $category->cnt_deals; ?></span>
			</a>
		</li>
	<?php } ?>
</ul>