<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<?php
	//$search_keyword 
	$search_keyword = $this->input->get("search_keyword", '');
?>


<!-- //////////////////////////////////
//////////////MAIN HEADER////////////// 
///////////////////////////////////////-->

<header class="main">
	<div class="container bg-white width100 my-search-area">
		<div class="row ml0 mr0">
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 bg-white">
				<a href="/" class="logo mt5 mb5 text-center" data-toggle="tooltip" data-placement="bottom" data-title="Почетна страна">
                    <img src="/pub/img/logo5.png" alt="Kupinapopust.mk logo" height="40"/>
                </a>
			</div>
            <div class="col-lg-4 col-md-4 col-sm-6 pt10 col-xs-12 search-area search-area-white ">


            	<form action="/index/search">
           		<label>
                    <span class="menu-touch-button hidden-md hidden-lg"></span>
                    <span class="hidden-xs hidden-sm" style="font-size: 15px; margin-top: 15px;">Барам понуда за</span>
                </label>
                <div class="search-area-division search-area-division-input">
                    <input class="form-control" type="text" placeholder="Банско" name="search_keyword" value="<?php echo $search_keyword; ?>" />
                </div>
                 <button class="btn btn-white btn-mini btn-my" type="submit"><i class="fa fa-search"></i></button>
                 </form>
            </div>
	
            <div class="col-lg-5 col-md-5 pr0 col-sm-6 col-xs-12">
                <!-- LOGIN REGISTER LINKS -->
                <ul class="login-register">
                  <!--  кога си најавен да ти се покаже ова-->

                    <li>
                        <a  target="_blank" href="http://blog.kupinapopust.mk/">
                            <img style="width: 80%" src="/pub/img/Blog.png">
                        </a>
                    </li>

                  	<!--Кошничка-->
                    <li class="shopping-cart">
                        <a id="shop-cart-menu-link" href="/index/shop_cart">
                            <i class="fa fa-shopping-cart" style="<?php if($controller == "index" && $action == "shop_cart") echo 'opacity: 1;'; ?>"></i><span class="badge my-badge rounded-x"></span>
                        </a>
                    </li>

                    

					<!-- OMILENI-->                   

                    <li class="shopping-cart">
                        <a id="fav-menu-link" href="/index/favourites">
                            <i class="fa fa-star" style="<?php if($controller == "index" && $action == "favourites") echo 'opacity: 1;'; ?>"></i><span class="badge my-badge rounded-x"></span>
                        </a>
                    </li>

                    <!--<li class="shopping-cart"><a href="page-cart.html"><i class="fa fa-user"></i>Мој профил</a></li>-->
                    <?php if (empty($user_email)) { ?>
                    	<li class="ml20"><a class="" href="/customer" data-effect="mfp-move-from-top" style="<?php if($controller == "customer" && $action == "login") echo 'font-weight: bold;'; ?>"><i class="fa fa-sign-in" style="<?php if($controller == "customer" && $action == "login") echo 'opacity: 1;'; ?>"></i>Најави се</a></li>
                    <?php } else { ?>
                    	<li class="shopping-cart"><a href="/customer" style="<?php if($controller == "customer" ) echo 'font-weight: bold;'; ?>"><i class="fa fa-user fav-menu-active" style="<?php if($controller == "customer" ) echo 'opacity: 1;'; ?>"></i>Мој профил</a></li>
                    <?php } ?>	

                    <li><a href="/static/page/kontakt" style="<?php if($controller == "static" && $action == "page" && $arguments[0] == "kontakt") echo 'font-weight: bold;'; ?>">КОНТАКТ</a></li>
                    <!--<li><a class="popup-text" href="#" data-effect="mfp-move-from-top"><i class="fa fa-edit"></i>Регистрирај се</a></li>-->
                
                </ul>
            </div>

		</div>

        <div class="row ml0 mr0" id="meni_div" style="display: none;">
            <div class="col-xs-12 pr30 hidden-md hidden-lg">
                <?php
                    if (!empty($user_email) && $controller == "customer")
                        require APPPATH . 'views/customer/menu_links.php';
                    else
                        require APPPATH . 'views/layouts/static_links.php';
                ?>
                <div class="mt5"></div>
            </div>
        </div>
        <?php if (in_array($controller, array("index", "deal", "all"))) { ?>
            <div class="row ml0 mr0 hidden-md hidden-lg">
                <div class="col-xs-12 hidden-md hidden-lg">
                    <?php
                        $category_links_include = 1;
                        require APPPATH . 'views/layouts/category_links.php';
                    ?>

                    <div class="gap-small"></div>
                    
                    <?php
                        $filter_links_include = 1;//FILTER
                        require APPPATH . 'views/layouts/filter_links.php';
                    ?>

                    <div class="mt10"></div>
                </div>
            </div>
        <?php } ?>
	</div>
</header>


<!-- //////////////////////////////////
//////////////END MAIN HEADER////////// 
///////////////////////////////////////-->
