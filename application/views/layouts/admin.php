<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $lang?>" lang="<?php print $lang?>">

    <head>
        <?php require_once "head_admin.php"; ?>
    </head>
    <?php
        $defaultModel = new Default_Model();
        $ie = $defaultModel->detectIE();
    ?>
    <body id="layout">
        <div id="header" class="">
            <?php require_once "header_admin.php"; ?>
        </div>

        <div id="content" class="container_12" style="margin-left: auto; margin-right: auto">
            <div id="<?php if ($ie) print 'ie';?>" class="grid_12" style="min-height: 700px">
                <?php echo $content ?>
            </div>
        </div>

        <div id="footer" class="">
            <?php require_once "footer_admin.php"; ?>
            <div id="" class="grid_12 alpha omega">
                <p class="copyright"></p>
            </div>

        </div>

    </body>
</html>