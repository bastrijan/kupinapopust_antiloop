<head>
  <title><?php echo html::specialchars($title) ?></title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  
  <!--META TAGS FOR SEO-->
	<meta name="description" content="Kupinapopust.mk е веб страна која нуди голем број попусти на различни услуги и производи. Менито на нашата веб страна е составено од повеќе категории и под категории како што се: храна, банско, масажи, козметика, авто услуги, зумба фитнес, пилатес, спорт, патувања, книги, сметководство, театри, курсеви, игротека, фризерски услуги, нова година, астрологија, фотографија, здравје, IT технологија.
Овие групни попусти се наменети за потребите на луѓето низ цела Македонија и пошироко. Ако славите роденден посетете ја категоријата игротека. Имате болки во грбот,главата или телото, ви предложувам да одите на масажа, да се релаксирате и опуштите. Сте станале мрзливи, неможете да станете од вашиот кревет, тогаш потребно ви е малку тренинг, малку зумба,пилатес, аеробик, зумба фитнес и нормално некаков вид на активен спорт.
Размислувате каде за зимскиот одмор, за нова година или божиќните празници, не двоумете се и посетете го Банско. Ниските цени се предност за искористување на овие услуги. За безбедно патување користете зимска опрема или зимски гуми.">
	<meta name="keywords" content="Popusti , Popust, Popusti mk, Popust mk, Kupi na popust.mk , Попусти, Попуст, Сакам попуст , Групни попусти, Купи на попуст, Sakam popust, Kupi na popust , Sakampopust          	, Kupinapopust, Site popusti, Grupni popusti, Site popusti mk, Grupni popusti mk, Na popust, Храна , банско, bansko ,банско понуди, bansko ponudi , фотографија , masaza , masaza mk, masaza vo skopje , masaza skopje, masazi skopje, масажа, масажи, масажа скопје, здравје, зумба, пилатес, zumba mk, zumba skopje, зумба фитнес , аеробик, авто услуги, sport mk, спорт, спорт мк, igroteki vo skopje, igroteki skopje, игротеки, Игротека, Зимски гуми, Gumi skopje, Zimski gumi,Depilacija, депилација, книги, курсеви, театри, патувања, фризерски услуги, нова година, сметководство, астрологија, производи, производи мк, proizvodi mk.">
	<meta name="author" content="Груп Поинт дооел е сопственик на веб страната kupinapopust.mk. Целта ни е да нудиме услуги и производи кои ги задоволуваат секојдневните потреби и желби на нашите корисници и тоа за безконкуретно ниски цени. Се трудиме да се однесуваме со нашите купувачи така, како што и ние самите посакуваме другите фирми да се однесуваат према нас. А тоа значи:
- Нудиме попусти за услуги кои и самите ги посакуваме.
- Немаме никакви тајни и скриени трошоци кои би предизвикале непријатност кај купувачите.
Стратешки партнер на Груп Поинт е Компанија Др.Беловски дооел Скопје, Друштво за интелектуални услуги и менаџмент консалтинг.">
  <!--END META TAGS FOR SEO-->
  
  <!--META FOR FACEBOOK, G+ AND TWITTER-->
  <?php if(isset($fb_title) && $fb_title!="") { ?>
	  <meta property="og:title" content="<?php print html::specialchars($fb_title); ?>" />
	  <meta property="fb:app_id" content="<?php echo Kohana::config('facebook.facebookAppID');?>"/>
	  <meta property="og:site_name" content="<?php print kohana::lang("prevod.website_title") ?>" />
	  <meta property="og:type" content="product" />
  <?php } //if(isset($fb_title)) { ?>
  
  <?php if(isset($fb_link) && $fb_link!="") { ?>
	  <meta property="og:url" content="<?php print html::specialchars($fb_link); ?>" />
	  <link rel="canonical" href="<?php print html::specialchars($fb_link); ?>" />
  <?php } //if(isset($fb_link)) { ?>
  
  <?php if(isset($fb_image) && $fb_image!="") { ?>
	  <meta property="og:image" content="<?php print $fb_image; ?>" />
  <?php } //if(isset($fb_image)) { ?>
  
  <?php if(isset($fb_description) && $fb_description!="") { ?>
	  <meta property="og:description" content="<?php print html::specialchars($fb_description); ?>" />
  <?php } //if(isset($fb_description)) { ?>

  
  <!--END META FOR FACEBOOK, G+ AND TWITTER-->

  
  <link rel="shortcut icon" type="image/x-icon" href="/pub/img/layout/favikon-c.ico">
  <link rel="shortcut icon" href="/pub/img/layout/favikon-c.ico">
  

  <link rel="stylesheet" href="/pub/css/reset.css?1" media="screen">
  <link rel="stylesheet" href="/pub/css/960.css?1" media="screen">
  <link rel="stylesheet" href="/pub/css/basic.css?1" media="screen">
  <link rel="stylesheet" href="/pub/css/style_logged.css?1" media="screen">
  <link rel="stylesheet" href="/pub/css/calendar.css?1" media="screen">
  <link rel="stylesheet" href="/pub/css/jquery.notice.css?1" media="screen">

  <link rel="stylesheet" href="/pub/css/jquery-ui.css?1" type="text/css" media="all" /> 
  <link rel="stylesheet" href="/pub/css/ui.theme.css?1" type="text/css" media="all" />

  <link href="/pub/css/select2/select2.min.css?1" rel="stylesheet" />

  
  <!-- JQUERY OD NOVIOT DIZAJN -->
  <script type="text/javascript" src="/pub/js/jquery.js?1"></script>
   	

  <script src="/pub/js/jquery-ui.min.js?1" type="text/javascript"></script> 
  <script src="/pub/js/ui/jquery.ui.dialog.js?1" type="text/javascript"></script>
  
  <!-- include libraries BS3 -->
  <link rel="stylesheet" href="/pub/css/boostrap.css?1" />	
  <script src="/pub/js/boostrap.min.js?1"></script>
  <link rel="stylesheet" href="/pub/css/font_awesome.css?1" /> 
  <link rel="stylesheet" href="/pub/css/prettify-1.0.css?1" />
  <link rel="stylesheet" href="/pub/css/base.css?1" />
  <link rel="stylesheet" href="/pub/css/bootstrap-datetimepicker.css?1" />

  <!-- Style for jquery-toastmessage-plugin -->
  <link rel="stylesheet" href="/pub/css/jquery.toastmessage.css?1">

  <!-- Style for bootstrap Validator -->
  <link rel="stylesheet" href="/pub/css/bootstrapValidator.min.css?1"/>

  <!-- Style for Tag system -->
  <link rel="stylesheet" href="/pub/css/tagify.css?1"/>

  <!-- ova se koristi nasekade za da se prikazat notice poraki sto se pojavuvaat gore pa se gubat posle odredeno vreme -->
  <script type="text/javascript" src="/pub/js/jquery.notice.js?1"></script>
  
  <!-- ova se koristi vo ADMIN -> POENI za da se smeni vrednosta na poenite sto gi ima korisnikot (to click and edit the content of different html elements)-->
  <script type="text/javascript" src="/pub/js/jquery.jeditable.js?1"></script>
  
  <!-- ova se koristi za datetime picker -->
  <script type="text/javascript" src="/pub/js/moment-with-locales.min.js?1"></script>
  <script type="text/javascript" src="/pub/js/bootstrap-datetimepicker.js?1"></script>

  <!-- ova se koristi za bootstrap Validator -->
  <script type="text/javascript" src="/pub/js/bootstrapValidator.min.js?1"></script>
	
  <!-- ova se koristi za Tag system -->
  <script type="text/javascript" src="/pub/js/tagify.polyfills.js?1"></script>
  <script type="text/javascript" src="/pub/js/tagify.min.js?1"></script>

  <!-- The jQuery replacement for select boxes -->
  <script src="/pub/js/select2/select2.min.js?1"></script>

</head>