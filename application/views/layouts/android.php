<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<!DOCTYPE html>

<html>
	<?php require_once "head_android.php"; ?>
    <body>
        
		<?php echo $content ?>
				
		<?php if(isset($share_dialog_show) && $share_dialog_show) { ?>
				<a href="#" id="share_btn_bottom" onclick="return false" >
					<input type="hidden" id="share_url" value="<?php echo Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL').str_replace("_android","",$_SERVER["REQUEST_URI"]); ?>">
					<img src="/pub/img/share_ico.png" width="32px" style="display: block;" />
				</a>
			
				<script type="text/javascript" >			
			        $(document).on('touchstart', '#share_btn_bottom', function (){ 
						var share_url = $('#share_url').val();
						//alert(share_url);
						window.<?php echo $bridgeName ?>.ShareData(share_url);
					});
			        

				</script>
		<?php } //if(1) { ?>
    </body>
</html>