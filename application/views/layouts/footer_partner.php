<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<footer class="main">
	<div class="footer-copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<a href="/rss">RSS Feed</a> |
					<a href="/static/page/licnipodatoci/">Заштита на личните податоци</a> |
					<a href="/static/page/pravila/">Правила и Услови за користење на веб страната</a>
					<p>2011 - <?php echo date("Y"); ?> © Kupinapopust.mk Сите права задржани.</p> 
				</div>
				<div class="col-md-4 col-md-offset-1">
					<div class="pull-right">
						<div class="gap-mini hidden-md hidden-lg"></div>
						<img src="/pub/img/Resposnive-design.png" style="height: 38px; width: initial" alt="IF Kreativa" />
						Design by <a href="http://www.ifkreativa.com">IF Kreativa</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- ////////////////////////////////////////
////////////// END MAIN FOOTER ////////////// 
//////////////////////////////////////////-->

			
			<!--  Scripts queries -->
			<script src="/pub/js/boostrap.min.js"></script>

			<!--  za mobile meni -->
			<script src="/pub/js/flexnav.min.js"></script>

			<!--  za pop up, ako ne se koristi da se izvadi -->
			<script src="/pub/js/magnific.js"></script>

			<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
			<script src="/pub/js/ionrangeslider.js"></script>
			<script src="/pub/js/masonry.js"></script>

			<!-- ova e za scroll to top, patekata do slikickata se naoga vo samata skripta, ako ja menuvas i tamu treba da se smeni-->
			<script src="/pub/js/back-to-top.js"></script>

			<!--  ovaa valda ja imas-->
			<script src="https://apis.google.com/js/platform.js" async defer></script>


			<!--  za tabelite so sorting -->
			<script type="text/javascript" src="/pub/js/jqTables.js"></script>
	  
			<!-- Custom scripts -->
			<script src="/pub/js/custom.js"></script>
			
			<!-- jquery-toastmessage-plugin -->
			<script src="/pub/js/jquery.toastmessage.js"></script>