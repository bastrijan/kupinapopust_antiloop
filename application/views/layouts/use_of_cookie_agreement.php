		<style type="text/css">
			.m-privacy-consent{
			    background-color: rgb(60, 60, 60);
			    top: 0;
			    color: white !important;
			    font-size: 12px !important;
			    font-weight: normal !important;
			    line-height: 1.5em !important;
			    left: 0;
			    position: fixed;
			    right: 0;
			    z-index: 5000001;
			}

			.m-privacy-consent__inner {
			  margin: 0 auto;
			  max-width: 1200px;
			  padding: 10px;
			}

			.m-privacy-consent__inner button {
			  background-color: #f60;
			  border: 1px solid #e65c00;
			  color: #ffffff;
			  display: block;
			  height: 46px;
			  line-height: 46px;
			  padding: 0 2em;
			  margin: 0 auto;
			  min-width: 200px;
			}

		</style>


		<script type="text/javascript">

			$(document).ready(function() {

				function checkUseOfCookieAgreement()
				{
					var cookieValue = $.cookie("knp_useofcookie_agreed");

					if (cookieValue == undefined)
					{
						//kreiraj cookie so informacija deka se prikazal prozorecot za upotrebata na cookies od nasa strana
						$.cookie("knp_useofcookie_agreed", "yes", { expires: 365, path: "/" });

						//prikazi go prozorecot
						$("#privacy-consent").show();
					}
				}

				$( "#privacy-consent-button" ).click(function(event ) {
					event.preventDefault();
					
					//skrij go prozorecot
					$("#privacy-consent").hide();
				});

				checkUseOfCookieAgreement();
			});
		</script>

		<div class="m-privacy-consent" id="privacy-consent" style="display: none">
		  <div class="m-privacy-consent__inner">
			  <p>
			  	Почитувани,<br/>
				За ваше подобро корисничко искуство нашата веб страна користи колачиња ( cookies ). 
				Со продолжување на користење на нашата веб страна или кликнување “Во ред” се согласувате со користењето на колачињата. 
				Детално појаснување за користење на колачињата можете да прочитате <a href="/static/page/cookies/" class="orange" target="_blank">овде</a>.
			  </p>

		    <button type="button" id="privacy-consent-button">
		      <div class="m-privacy-consent__button-content">
		        <div class="m-privacy-consent__hourglass"></div>
		          Во ред
		      </div>
		    </button>
		  </div>
		</div>