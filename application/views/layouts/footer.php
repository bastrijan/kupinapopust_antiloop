<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<!-- /////////////////////////////////////////
	 ////////////// MAIN FOOTER ////////////// 
	 //////////////////////////////////////-->
<footer class="main">

	<div class="za-firmi">  За сите фирми! Привлечете нови купувачи без никаков ризик. Креирајте понуда со попуст и започнете веднаш со промоција и продажба!
		Кликнете <a href="/static/page/reklamiranje_partneri" class="orange" >овде</a> за повеќе детали.
	</div>

	<div class="footer-top-area">
		<div class="container">
			<div class="row row-wrap">
				<div class="col-md-3">
					<a href="/">
						<img src="/pub/img/logo2.png" alt="logo" title="logo" class="logo width100 " alt="Kupinapopust Logo" />
					</a>
					
					<ul class="list list-social">
					   <li>
							<a target="_blank" href="https://www.facebook.com/Kupinapopust.mk" data-toggle="tooltip" data-placement="top" title="Следи не на Facebook">
								<!-- <img align="center" src="/pub/img/fbround25x25.png"> -->
								<img src="/pub/img/fbround25x25.png" alt="Facebook" />
							</a>
						</li>
						<li>
							<a target="_blank" href="https://twitter.com/kupinapopust" data-toggle="tooltip" data-placement="top" title="Следи не на Twitter">
								<!-- <img align="center" src="/pub/img/tw25x25.png"> -->
								<img src="/pub/img/tw25x25.png" alt="Twitter" />
							</a>
						</li>
						 <li>
							<a target="_blank" href="https://plus.google.com/+KupinapopustMk/posts" data-toggle="tooltip" data-placement="top" title="Следи не на Google+">
								<!-- <img align="center" src="/pub/img/googleplus.png"> -->
								<img src="/pub/img/googleplus.png" alt="Google+" />
							</a>
						</li>

						<li>
							<a target="_blank" href="https://www.youtube.com/kupinapopust" data-toggle="tooltip" title="Youtube">
								<!-- <img align="center" src="/pub/img/youtube.png"> -->
								<img src="/pub/img/youtube.png" alt="Youtube" />
							</a>
						</li>
						<li>
							<a target="_blank" href="https://instagram.com/kupinapopust" data-toggle="tooltip" title="Instagram">
								<!-- <img align="center" src="/pub/img/instagram.png"> -->
								<img src="/pub/img/instagram.png" alt="Instagram" />
							</a>
						</li>
						<li>
							<a target="_blank" href="https://www.linkedin.com/company/kupinapopust" data-toggle="tooltip" title="" data-original-title="LinkedIn">
								<!-- <img align="center" src="/pub/img/linkedin.png"> -->
								<img src="/pub/img/linkedin.png" alt="LinkedIn" />
							</a>
						</li>

					</ul>

					<!-- /////////////////////////////////// PREVZEMI ANDROID I IOS APLIKACIJA /////////////////////////////////// -->
					<!--
					<ul class="list list-social">
						<li>
							<a target="_blank" href="#" data-toggle="tooltip"  title="Превземи android апликација">
								<img src="/pub/img/androidIcon1.png" class="icon-size" alt="Android" /> Android
							</a>
						</li>
						<li>
							<a target="_blank" href="#" data-toggle="tooltip"  title="Превземи iOS апликација">
								<img src="/pub/img/appleIcon1.png" class="icon-size" alt="Iphone" /> Iphone
							</a>
						</li>
					</ul>
					-->
					
					<h5  class="vrab">Повеќе од <span class="big-orange">40 253</span> корисници купиле над <span class="big-orange">135 222</span> купони и заштедиле преку <span class="big-orange">57 095 832 ден.</span> Бидете и <span class="big-orange">Вие</span> еден од нив!</h5>
					
					<a href="/static/page/nova_rabotna_pozicija" target="_blank">
						<!-- <img align="center" src="/pub/img/stolce1.png" class="icon-size"> -->
						<img src="/pub/img/stolce1.png" class="icon-size"  alt="Stolche" />
						<!--  <i class="fa fa-user big-orange"></i>  -->
						Вработување!<br /> Биди и ти дел од kupinapopust тимот
					</a>
					
					<div class="gap-small"></div>
					
					<div>
						<a href="/affiliate" target="_blank">
							<img class="img-responsive" src="/pub/img/affiliate-logo.jpg" alt="Affiliate програма на kupinapopust.mk" />
						</a>
					</div>

				</div>
				
				<div class="col-md-3 hidden-xs hidden-sm">
					&nbsp;
				</div>
				
				<div class="col-md-3">
					<!-- ////////////////  NE E ZAVRSHENO!!!  //////////////// --> 
				    <h4>Сакам да ги добивам понудите на e-mail!</h4>
					<div class="box">
						<form id="newsletter_form">
							<div class="form-group mb10">
								<label>E-mail</label>
								<input type="text" class="form-control" placeholder="e-mail" id="email_field" name="mail" />
							</div>
						 <!--  <p class="mb10">Ullamcorper porttitor torquent montes convallis lobortis urna</p> -->
							<input type="submit" class="btn btn-primary" value="Потврди" id="potvrdi_email" name="confirm_email" />
						</form>
						<script type="text/javascript">
						
							function validate(email) {
								var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
								if(reg.test(email) == false) {
									alert("<?php print Kohana::lang("index.невалидeн e-mail")?>")
									return false;
								}
								return true;
							}
							$(document).ready(function() {
								

								
								$("#potvrdi_email").click(function (){

									

										if ($("#email_field").val() != '' && validate($("#email_field").val()) ) {
											$.post(
												"/ajax/newsletter",
												$("#newsletter_form").serialize(),
												function(data) {
													if (data.resp == 'success') {
														/*
														$.noticeAdd({
															text: "<?php print Kohana::lang("index.Успешно го пријавивте вашиот e-mail!"); ?>",
															stay: false,
															type:'notification-success'
														});
														*/
														$().toastmessage('showToast', {
															// inEffectDuration:  600,   // in effect duration in miliseconds
															// stayTime:         3000,   // time in miliseconds before the item has to disappear
															text	 : "<?php print Kohana::lang("index.Успешно го пријавивте вашиот e-mail!"); ?>",
															sticky   : false,
															position : 'top-right',		 // top-left, top-center, top-right, middle-left, middle-center, middle-right
															type     : 'success',		 // notice, warning, error, success
															// closeText:         '',    // text which will be shown as close button,
																					     // set to '' when you want to introduce an image via css
															// close:            null    // callback function when the toastmessage is closed
														});
													} else {
														/*
														$.noticeAdd({
															text: "<?php print Kohana::lang("index.Вашиот e-mail веќе е пријавен и вие треба да добивате понуди од Kupinapopust"); ?>",
															stay: false,
															type:'notification-success'
														});
														*/
														$().toastmessage('showToast', {
															text     : "<?php print Kohana::lang("index.Вашиот e-mail веќе е пријавен и вие треба да добивате понуди од Kupinapopust"); ?>",
															sticky   : false,
															position : 'top-right',
															type     : 'notice'
														});
													}	   
												},
												"json"
											);
											defaultText = '';
											$("#email_field").val(defaultText);
										}
										return false;

								});

								
								$("#email_field").keypress(function(e) {
									if(e.keyCode == 13) {
										$('#potvrdi_email').click();
										return false;
									}
								});
								
								$("#email_field").focus(function (){
									defaultText = '<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса") ; ?>';
									if ($(this).val() == defaultText) {
										$(this).val("");
									}
								})
								
								$("#email_field").blur(function (){
									defaultText = '<?php print kohana::lang("prevod.Внесете ја вашата e-mail адреса") ; ?>';
									if ($(this).val() == "") {
										$(this).val(defaultText);
									}
								})

								//ako ne postoi baner a ima element so id = scrollHere
								/*
								if($("#bannerImg").length == 0)
								{
									//alert("banner nema");
									scrollHereCustom();	
								}
								else //ako ima banner slika
								{
									//ako ima slika so id = bannerImg togas pri onload na slikata napravi go slednoto
									$('#bannerImg').each(function(){

										if (!this.complete) {
											$(this).load(function(){
												//alert("prv pat se loadira slika");
												// handle image load event binding here
												scrollHereCustom();
											});
										} else {
											//alert("vekje ima slika");
											// handle image already loaded case
											scrollHereCustom();				
										}

									});
								}
								*/

							});
							
							/*
							function scrollHereCustom()
							{
								
								if($('#scrollHere').length != 0)
								{
									//alert("scrolHere ima");
									//$(window).scrollTop($('#scrollHere').offset().top);	
									$('html,body').animate({scrollTop: $('#scrollHere').offset().top}, 4000);
								}

									
							}
							*/
							
						</script>
					</div>
					<div class="gap-mini"></div>							
					<a class="btn-block" href="/static/page/zanas"><i class="fa fa-group"></i> <?php print kohana::lang("prevod.За нас"); ?></a>
					<a class="btn-block" href="/static/page/kontakt"><i class="fa fa-home"></i> <?php print kohana::lang("prevod.Контакт"); ?></a>
					<a class="btn-block" href="/static/page/cestiprasanja"><i class="fa fa-question"></i> <?php print Kohana::lang("prevod.Чести прашања?"); ?></a>
					<a class="btn-block" href="/static/page/kakorabotime"><i class="fa fa-star"></i> <?php print Kohana::lang("prevod.Како работиме?"); ?></a>
					<div class="gap-mini"></div>
					
					<a target="_blank" href="http://blog.kupinapopust.mk/">
                        <img src="/pub/img/blogFINAL.jpg">
                    </a>
                    
					<!-- <a class="btn btn-primary mt10" href=""> Kupinapopust.mk БЛОГ</a> -->
				</div>
				<div class="col-md-3">
					<h4>Информации</h4>
					<?php
					require APPPATH . 'views/layouts/static_links.php';
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					  <a href="/rss">RSS Feed</a> |
					<a href="/static/page/licnipodatoci/"><?php print kohana::lang("prevod.Заштита на личните податоци"); ?></a> |
					<a href="/static/page/pravila/"><?php print kohana::lang("prevod.Правила и Услови за користење на веб страната"); ?></a>
					<p>2011 - <?php echo date("Y"); ?> © Kupinapopust.mk <?php print kohana::lang("prevod.Сите права задржани"); ?></p> 
				</div>
				<div class="col-md-4 col-md-offset-1">
					<div class="pull-right">
						<div class="gap-mini hidden-md hidden-lg"></div>
						<img src="/pub/img/Resposnive-design.png" style="height: 38px; width: initial" alt="IF Kreativa" />
						Design by <a href="http://www.ifkreativa.com">IF Kreativa</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- Scripts queries -->
<script src="/pub/js/jquery.cookie.js"></script>
<script src="/pub/js/boostrap.min.js"></script>
<script src="/pub/js/countdown.min.js"></script>

<!-- za mobile meni -->
<script src="/pub/js/flexnav.min.js"></script>

<!-- za pop up, ako ne se koristi da se izvadi --> 
<script src="/pub/js/magnific.js"></script>


<!-- <script src="js/tweet.min.js"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

<!-- ako ti treba za responsive video-->
<!--
<script src="js/fitvids.min.js"></script>
-->

<!-- <script src="js/mail.min.js"></script> -->
<script src="/pub/js/ionrangeslider.js"></script>
<script src="/pub/js/icheck.js"></script>
<script src="/pub/js/fotorama.js"></script> 
<!-- <script src="js/card-payment.js"></script> -->
<script src="/pub/js/owl-carousel.js"></script>

<script src="/pub/js/masonry.js"></script>

<!-- ova e za scroll to top, patekata do slikickata se naoga vo samata skripta, ako ja menuvas i tamu treba da se smeni -->
<script src="/pub/js/back-to-top.js"></script>

<!-- ovaa valda ja imas -->
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://platform.twitter.com/widgets.js" type="text/javascript"></script>

<!-- skripti od kupi na popust -->

<!-- ovaa ja kaciv najgore (ima nekoi raboti sto se koristat od 1.7, no od 1.9 navaka e standard za responsive dizajn da raboti kako sto treba) -->
<!-- <script type="text/javascript" src="http://kupinapopust.mk/pub/js/jquery-1.7.1.min.js"></script> -->

<!--
<script type="text/javascript" src="/pub/js/jquery-ui.min.js" ></script> 
<script type="text/javascript" src="/pub/js/jquery-ui-timepicker-addon.js"></script>
-->


<!--
<script type="text/javascript" src="/pub/js/ui/jquery.ui.dialog.js" ></script>
-->

<!-- ima druga skripta za countdown, dokolku ne ti funkcionira so nea, mozes da ja smenis -->
<!-- <script type="text/javascript" src="http://kupinapopust.mk/pub/js/jquery.countdown.js"></script>
<script type="text/javascript" src="http://kupinapopust.mk/pub/js/jquery.countdown-mk.js"></script> -->

<!--
<script type="text/javascript" src="/pub/js/jquery.notice.js"></script>
<script type="text/javascript" src="/pub/js/jquery.jeditable.js"></script>
<script type="text/javascript" src="/pub/js/jquery.webticker.min.js"></script>
-->

<!-- Custom scripts -->
<script src="/pub/js/custom.js"></script>

<!-- jquery-toastmessage-plugin -->
<script src="/pub/js/jquery.toastmessage.js"></script>

<script src="/pub/js/favourites.js" charset="UTF-8"></script>
<script type="text/javascript" src="/pub/js/shop_cart.js" charset="UTF-8"></script>

<!-- //////////////////////////////////
//////////////END MAIN  FOOTER///////// 
////////////////////////////////////-->