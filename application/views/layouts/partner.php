﻿<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php
	$controller = Router::$controller;
	$action = Router::$method;
	$arguments = Router::$arguments;
?>
<!DOCTYPE html>
<html>

	<!-- /////////////////////////////
	////////////// HEAD ////////////// 
	///////////////////////////////-->
    
	<?php require_once "head.php"; ?>
	

	<body >
    
		<div class="global-wrap">

			<!-- ////////////////////////////////////
			////////////// MAIN HEADER ////////////// 
			//////////////////////////////////////-->
			
            <?php require_once "header_partner.php"; ?>

			
			<!-- //////////////////////////////////
			//////////////PAGE CONTENT///////////// 
			////////////////////////////////////-->

			<div id="content" class="container mt20">
				<div class="row">
					<?php echo $content; ?>
				</div>
				<div class="gap gap-small"></div>
			</div>
			
			<!-- ////////////////////////////////////////
			////////////// END PAGE CONTENT ///////////// 
			//////////////////////////////////////////-->
			
			
			<!-- ////////////////////////////////////
			////////////// MAIN FOOTER ////////////// 
			//////////////////////////////////////-->
			
            <?php require_once "footer_partner.php"; ?>
			
        </div>

    </body>
	
</html>