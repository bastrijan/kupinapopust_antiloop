<?php
	$controller = Router::$controller;
	$action = Router::$method; 
	$arguments = Router::$arguments;

	$eurolink_insurance_type = NULL;

	//ako sme vo kategorija PROIZVODI togas prikazi domasno osiguruvanje (property)	
	if(($controller == 'index' && $action == "index") || ($controller == 'all' && $action == "category" && isset($arguments[0]) && $arguments[0] == "patuvanja-25") || ($controller == 'all' && $action == "subcategory" && isset($arguments[0]) && $arguments[0] == "patuvanja-25"))
	{
		$eurolink_insurance_type = "travel";
		$eurolink_insurance_img_ext = "travel.gif";   
	}
	elseif( ($controller == 'all' && $action == "category" && isset($arguments[0]) && $arguments[0] == "proizvodi-20") || ($controller == 'all' && $action == "subcategory" && isset($arguments[0]) && $arguments[0] == "proizvodi-20"))
	{
		$eurolink_insurance_type = "property";
		$eurolink_insurance_img_ext = "property.jpg";
	}

?>

<!-- Header banner 3 START-->
<?php
	if($controller != 'deal')
	{

		$baner_location_3 = bannersystem::getBanner(3);
		
		if($baner_location_3 != "" ) 
		{ 
?>
			<div >
				<?php print $baner_location_3;?>
			</div> 
			<div class="gap gap-mini"></div>
<?php 	} 
	}//if($controller != 'deal')
?>
<!-- Header banner 3 END-->




<?php if(!empty($eurolink_insurance_type)) { ?>
	<div class="mb20 hidden-xs hidden-sm">
		<a href="https://shop.eurolink.com.mk/EurolinkWebShop/shop/wizard?type=<?php print $eurolink_insurance_type;?>&from=770" target="_blank" >			
			<img src="/pub/img/layout/eurolink-<?php print $eurolink_insurance_img_ext;?>">
		</a>
	</div>
<?php }//if($eurolink_insurance_type != "") { ?>



<!-- SABLON ZA KLASICEN BANNER
<div class="mb20 hidden-xs hidden-sm">
	<a href="/all/category/grci%D1%98a-leto-2016-42" target="_blank" >			
		<img src="/pub/img/layout/banner_grcija_leto_2016.png">
	</a>
</div>
-->




