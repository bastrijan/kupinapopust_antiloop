﻿<script type="text/javascript">
    function validateFriend(email) {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(reg.test(email) == false) {
            alert("<?php print Kohana::lang("index.невалидeн e-mail") ?>")
            return false;
        }
        return true;
    }
	
	$(document).ready(function() {
	
        $("#email_tellafriend_popup").keyup(function(e) {
            if (e.keyCode == $.ui.keyCode.ENTER) {
                $('#potvrdi_tellafriend_popup').click();
                return false;
            }
        });

        $("#potvrdi_tellafriend_popup").click(function (){
            if (validateFriend($("#email_tellafriend_popup").val()) && $("#email_tellafriend_popup").val() != '') {
                $.post(
                "/ajax/tellafriend",
                $("#tellafriend_form_popup").serialize(),
                function(data) {
 
                    if (data.resp == 'success') {

						$().toastmessage('showToast', {
							text     : "Содржината е успешно испратена на email адресата на вашиот пријател!",
							sticky   : false,
							position : 'middle-center',
							type     : 'success'
						});
            
                        $.magnificPopup.close();

                    } else {
                    
						$().toastmessage('showToast', {
							text     : "Настаната е грешка при испраќање на содржината!",
							sticky   : false,
							position : 'middle-center',
							type     : 'success'
						});

                        $.magnificPopup.close();
                    }
                   
                },
                "json");

				 $("#email_tellafriend_popup").val('');

            }
            return false;
        
        })
		

	});
</script>
<!--
<style type="text/css">
	
    #tellafriend_popup form input.text{
        width: 250px
    }


	.nov_popup_header
	{
		background: none repeat scroll 0 0 #FFFFCC;
		border: 0px solid #000000;
		color: #222222;
		font-weight: bold;
	}
	
	.ui-widget-content {
		border: 1px solid #000000;
	}
.ui-menu .ui-menu-item a.ui-state-hover,
.ui-menu .ui-menu-item a.ui-state-active {
	font-weight: normal;
	margin: -1px;
}
.ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus { border: 1px solid #000000/*{borderColorHover}*/; background: #FFFFCC/*{bgColorHover}*/;font-weight: normal/*{fwDefault}*/; color: #212121/*{fcHover}*/; }
.ui-state-hover a, .ui-state-hover a:hover { color: #000000/*{fcHover}*/; text-decoration: none; }
.ui-state-hover .ui-icon, .ui-state-focus .ui-icon {background-image: url(/pub/css/ui-lightness/images/ui-icons_222222_256x240.png)/*{iconsHover}*/; }

</style>
-->
	<?php
		if ($dealData->card)
			$url_email_content = Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/card";
		else	
			$url_email_content = Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/deal/index/" . $payAttemptData->deal_id;
	?>

<div id="tellafriend_button" class="white-popup mfp-hide" >
	<form id="tellafriend_form_popup" method="post" action="">
		<div>
                <div class="form-group">
                    <label>Email адресата na Вашиот пријател</label>
                    <input id="email_tellafriend_popup" type="text" value="" name="email_tellafriend_popup" placeholder="email@domain.com" class="form-control">
                </div>
                <div class="form-group">
                    <label>Содржина</label>
                   <textarea rows="5" name="content_tellafriend_popup" id="content_tellafriend_popup" class="form-control" ><?php echo ($dealData->card ? 'Подарок "kupinapopust" електронска картичка во вредност од ' : "").trim(strip_tags($dealData->title_mk_clean))."\r\n\r\n".$url_email_content;?></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" value="Испрати" id="potvrdi_tellafriend_popup" name="send" class="btn btn-md green" />  
                </div>


				<input id="your_email_tellafriend_popup" type="hidden" value="<?php echo $payAttemptData->email;?>" name="your_email_tellafriend_popup">
			


		</div>			
	</form>	
</div>