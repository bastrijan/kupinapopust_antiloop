<div class="homepage-content" style="margin-top: 50px;">
    <div class="layout-wrapper">

        <div class="subpage-content">
            <div class="section-container">
                <div class="caption">
                    <table>
                        <tr>
                            <td style="padding: 8px 18px;">
                                <div>
                                    <img src="/pub/img/layout/ok20x25.png" style="width: 25px"/>
                                    <span style="font-size: 13px">
                                        Успешно го одјавивте вашиот регистриран e-mail.
                                    </span>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

                <p>&nbsp;</p>
                <p>Почитувани, 
                <p>Повеќе нема да добивате пораки од нашата веб страна. Ви благодариме на укажаната доверба.</p>
<p>Со почит,</p>
                <p>Kupinapopust.mk тим</p>
                
                <p>&nbsp;</p>
                <p>
                    <span>
                        <input class="submit button" type="button" value="OK" onclick="location.href='/';">
                    </span>
                </p>
            </div>
        </div>
        <div class="static_page_contact_parent">
            <div class="static_page_contact">
                <?php require_once APPPATH . 'views/layouts/contact.php'; ?>
            </div>
        </div>
    </div>
</div>