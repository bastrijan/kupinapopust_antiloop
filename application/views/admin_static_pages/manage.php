<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<!-- include summernote -->
<link rel="stylesheet" href="/pub/summernote/summernote.css">
<script type="text/javascript" src="/pub/summernote/summernote.js"></script>

<?php require_once APPPATH.'views/admin/menu.php' ; ?>

  <script type="text/javascript">

    $(document).ready(function() {
      $('.content_mk').summernote({
			height: 300
	  });

    });
  </script>

<h1>Уреди содржина</h1>



<form method="post" action="" id="deal_save_form">
    <div class="container_12 homepage-billboard-dhtml">
        <div class="grid_12 alpha omega">
            <input type="hidden" name="id" value="<?php print $staticPage->id ?>">
            <div class="row clearfix">
                <div class="grid_4 alpha omega">Identifier</div>
                <div class="grid_8 alpha omega"><input type="text" name="identifier" value="<?php print $staticPage->identifier ?>" disabled></div>
            </div>
            <div class="row clearfix">
                <div class="grid_4 alpha omega">Наслов</div>
                <div class="grid_8 alpha omega"><input type="text" name="title_mk" value="<?php print $staticPage->title_mk ?>"></div>
            </div>
            <div class="row clearfix" style="padding: 20px;">
                <strong>Содржина</strong>
				<textarea class="content_mk" name="content_mk"><?php print $staticPage->content_mk; ?></textarea>
            </div>

            <div class="row clearfix">
                <input type="submit" value="Зачувај">
                <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/admin2/static_pages';return false;">
            </div>

        </div>
    </div>
</form>


