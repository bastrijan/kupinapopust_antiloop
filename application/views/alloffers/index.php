<!--
Start of Floodlight Tag on behalf of AdTradr Corporation: Please do not remove
Activity name of this tag: Kupinapopust.mk Home
URL of the webpage where the tag is expected to be placed: http://kupinapopust.mk/all/category/bansko-25
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 09/30/2015
-->
<!--
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="https://4768706.fls.doubleclick.net/activityi;src=4768706;type=invmedia;cat=wo8l6jr9;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="https://4768706.fls.doubleclick.net/activityi;src=4768706;type=invmedia;cat=wo8l6jr9;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
-->
<!-- End of Floodlight Tag on behalf of AdTradr Corporation: Please do not remove -->

<script type="text/javascript">
	function sold_out() {
		$().toastmessage('showToast', {
			text     : "Почитувани, Ваучерите за оваа понуда се веќе распродадени. Со почит, Kupinapopust.mk тим",
			sticky   : false,
			position : 'top-center',
			type     : 'notice'
		});
		return false;
	}
</script>

<div class="row">
	<div class="col-md-3 hidden-xs hidden-sm" style="padding-left: 0px; padding-right: 0px;">
		<aside class="sidebar-left">
			<!-- /////////////////////////  MENI  ///////////////////////// -->
			<?php 
				require_once APPPATH . 'views/layouts/category_menu.php';
			?>
		</aside>            
	</div>
	
	<div class="col-md-9" id="scrollHere">
		
		<h2><?php echo $scrollPageTitle; ?></h2>

		<div class="row">	
			<div class="col-md-5">
				<!-- SUBCATEGORY start -->
				<?php   
					if(isset($subCategoriesArray) && count($subCategoriesArray) > 0)
						require_once APPPATH.'views/layouts/subcategory_menu.php';	
				?>
				<!-- SUBCATEGORY end -->
				
			</div>
			
			<div class="col-md-2 col-md-offset-5">
				<div class="product-view pull-right">
					<!--  <a class="fa fa-th-large active" href="#"></a>
					<a class="fa fa-list" href="#"></a>-->
				</div>
			</div>
		</div>


		<?php 
			  if (count($bottomOffers) > 0) 
			  { 
				$bottom_offers_div_class = "col-md-4";
				require_once APPPATH . 'views/index/bottom_offers.php';
			  } // if(count($bottomOffers) > 0) { 
		?>

	</div> <!-- END OF: <div class="col-md-9"> -->
</div> <!-- END OF: <div class="row"> -->
		
