<?php 
	//$search_keyword 
	$search_keyword = $this->input->get("search_keyword", '');
	
	// $dealsModel = new Deals_Model();
	// $dealsCategory = $dealsModel->getAllCategoriesWithOffers();
?>

<script type="text/javascript">
	function sold_out() {
		$().toastmessage('showToast', {
			text     : "Почитувани, Ваучерите за оваа понуда се веќе распродадени. Со почит, Kupinapopust.mk тим",
			sticky   : false,
			position : 'top-center',
			type     : 'notice'
		});
		return false;
	}
</script>


<div class="row">
	<div class="col-md-3 hidden-xs hidden-sm" style="padding-left: 0px; padding-right: 0px;">
		<aside class="sidebar-left">
			<!-- /////////////////////////  MENI  ///////////////////////// -->
			<?php 
				require_once APPPATH . 'views/layouts/category_menu.php';
			?>
		</aside>            
	</div>
	
	<div class="col-md-9">

		<!--  ////////////////////////////////////  ПОНУДИТЕ  ////////////////////////////////////  -->
		<div class="row row-wrap">
				
		<?php

		$div_class ="col-md-4";
		
		foreach ($allOffers as $singleDeal) {

				$title_mk_clean = "";
				$title_mk = "";
				$max_ammount = 0;
				$ceni_od_txt = "";

				if($singleDeal->options_cnt > 1 || $singleDeal->is_general_deal)
				{
					$title_mk_clean =  $singleDeal->title_mk_clean_deal;
					$title_mk =  $singleDeal->title_mk_deal;
					$max_ammount = 0;
					if($singleDeal->is_general_deal)
						$ceni_od_txt = "Прегледај понуди";
					else	
						$ceni_od_txt = ($singleDeal->finalna_cena > 0 ? "од ".$singleDeal->finalna_cena." ден." : "Превземи купон");
				}
				else
				{
					$title_mk_clean =  $singleDeal->title_mk_clean;
					$title_mk =  $singleDeal->title_mk;
					$max_ammount = $singleDeal->max_ammount;
					$ceni_od_txt = ($singleDeal->finalna_cena > 0 ? $singleDeal->finalna_cena." ден." : "Превземи купон");
				}
			
			//se pravi logika za vremeto da se prikaze
			/*
			$timeLeft_bo = strtotime($singleDeal-> end_time) - time();
			$timeStart_bo = strtotime($singleDeal->start_time);
			$timeEnd_bo = strtotime($singleDeal->end_time);
			$timeNow_bo = time();
			*/

		if($singleDeal->is_general_deal)
			$detali_za_ponudata = "/deal/general_deal/" . $singleDeal->deal_id;
		else
			$detali_za_ponudata = "/deal/index/" . $singleDeal->deal_id;

		?>
			<div class="<?php echo $div_class; ?>">

				 <div class="product-thumb" >
					<header class="product-header">
						<!-- /////////////// Link do ponudata /////////////// -->
						<a href="<?php echo $detali_za_ponudata;?>">
							
							<!-- /////////////// Slika od ponudata /////////////// -->
							<img src="/pub/deals/<?php print $singleDeal->side_img; ?>" alt="<?php print str_replace("\"", "'", strip_tags($title_mk_clean)); ?>" />
						</a>
						
						<!-- /////////////// Popust /////////////// -->
						
						<?php if($singleDeal->price > 0 && !$singleDeal->is_general_deal)
								print '<span class="product-save popust-na-slika">'."- " . (int)round(100 - ($singleDeal->price_discount / $singleDeal->price) * 100) . "%".'</span>';
						?>
						
						<span class="product-save omileni-na-slika">
							<a href ="#" class="btn btn-sm fav-button<?php echo in_array($singleDeal->deal_id, $favDeals) ? ' fav-active' : ''; ?>" data-toggle="tooltip" data-placement="top" data-title="<?php echo in_array($singleDeal->deal_id, $favDeals) ? 'Одземи од омилени' : 'Додади во омилени'; ?>">
								<i class="fa fa-star"></i>
								<i class="infoDealID" style="display: none;"><?php echo $singleDeal->deal_id; ?></i>
							</a>
						</span>

						<?php if (!$singleDeal->is_general_deal) { ?>
							<span class="product-save shop-cart-na-slika" <?php if(strtotime($singleDeal->start_time) > time()) echo 'style="display: none;"'; ?>>
								<a href="#" class="btn btn-sm <?php echo $singleDeal->options_cnt > 1 ? 'shop-cart-many-options': 'shop-cart-button'; ?> <?php echo in_array($singleDeal->deal_option_id, $shopCartDeals) ? ' shop-cart-active' : ''; ?>" data-toggle="tooltip" data-placement="top" data-title="<?php echo in_array($singleDeal->deal_option_id, $shopCartDeals) ? 'Одземи од кошничка' : 'Додади во кошничка'; ?>">
									<i class="fa fa-shopping-cart"></i>
									<i class="infoDealOptionID" style="display: none;"><?php echo $singleDeal->deal_option_id; ?></i>
								</a>
							</span>
						<?php } //if (!$singleDeal->is_general_deal) { ?>

					</header>
					<div class="product-inner" >

						<?php if(!$singleDeal->is_general_deal) { ?>
							<h5 style="height: <?php echo Kohana::config('settings.so_partner_height');?>px">
								<?php print commonshow::Truncate(strip_tags($singleDeal->name), Kohana::config('settings.so_partner_letters')); ?>
							</h5>
							<div class="gap-small"></div>
						<?php }//if(!$singleDeal->is_general_deal) { ?>

						<!-- /////////////// Naslov na ponudata /////////////// -->
							<?php 
								$product_title_height = Kohana::config('settings.so_title_height');
								$product_title_letters = Kohana::config('settings.so_title_letters');
								//dokolku e OPSTA PONUDA
								if($singleDeal->is_general_deal)
								{
									$product_title_height = 15 + Kohana::config('settings.so_title_height') + Kohana::config('settings.so_partner_height');
									$product_title_letters = 10 + Kohana::config('settings.so_title_letters') + Kohana::config('settings.so_partner_letters');
								}
							

							?>

						<a href="<?php echo $detali_za_ponudata;?>">
							<h5 class="product-title" style="height: <?php echo $product_title_height;?>px">
							<?php print commonshow::highlight(commonshow::Truncate(strip_tags($title_mk_clean), $product_title_letters), $search_keyword); ?>
							</h5>
						</a>
						<div class="product-meta" >
						
							<!-- /////////////// Vreme do krajot na ponudata /////////////// -->
							<span class="product-time"><i class="fa fa-clock-o"></i>
								<?php echo commonshow::staticCountDown(strtotime($singleDeal->start_time), strtotime($singleDeal->end_time)); ?>
							</span>
							
							<!-- //////////////////// Broj na kupuvaci //////////////////// -->
							<?php
								$count = 0;
								if ( isset($singleDeal->voucher_count) ) 
									$count  = $singleDeal->voucher_count;
								
								$allDealsTooltipTxt = commonshow::tooltip($count, $singleDeal->min_ammount, $max_ammount);

								print '<h6 class="text-green"><strong class="buyers-count" title="' . $allDealsTooltipTxt . '">';
								if($count > 0)
								{
									if(commonshow::isSoldOut($count, $max_ammount)) {
										print "<span style='color: red'>";
										$kupi_style = "btn-danger";
										$kupi_tooltip = "Распродадено";
										$onclick = 'sold_out(); return false;';
									} else {
										$kupi_style = "green";
										$kupi_tooltip = "Купи";
										$onclick = '';
									}
									
									print '<i class="fa fa-level-up"></i>&nbsp;';
									print commonshow::number($count, $singleDeal->min_ammount, $max_ammount);
									print commonshow::text($count);
									
									if(commonshow::isSoldOut($count, $max_ammount))
										print "</span>";
								} else {
									print "&nbsp;";
									$kupi_style = "green";
									$kupi_tooltip = "Купи";
									$onclick = '';
								}
								print '</strong></h6>';
							?>
							
							<ul class="product-price-list">
								
								<!-- /////////////// Cena /////////////// -->
								<li>
									<a href ="<?php echo $detali_za_ponudata; ?>">
										<span class="product-price">
											<?php print $ceni_od_txt; ?>
										</span>
									</a>

								</li>
								
								<!-- /////////////// Stara cena /////////////// -->
								<li>
									<?php if($singleDeal->options_cnt <= 1 && $singleDeal->price > 0) { ?>
										<span class="product-old-price">
											<?php	print $singleDeal->price . " " . kohana::lang("prevod.ден"); ?>
										</span>
									<?php } ?>
								</li> 
							</ul>
						</div>
					</div>
				</div>
			</div>
			

							
		<?php } // foreach ($bottomOffers as $singleDeal)
		
			if (count($allOffers) == 0) {
		?>
				<div class="deal-item" style= "height: 375px;" >
					 <p style= "padding: 20px 0px 0px 35px;">
						Почитувани,<br>
						Не пронајдовме таков збор во нашите понуди. <br>
						Ве молиме обидете се повторно.<br><br>  
					 </p>
				</div>
		<?php } // if (count($allOffers) == 0) { 
		?>

		</div> <!-- END OF: <div class="row row-wrap"> -->
		<div class="gap"></div>
	</div> <!-- END OF: <div class="col-md-9"> -->
</div> <!-- END OF: <div class="row"> -->



