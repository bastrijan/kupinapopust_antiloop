<?php 
	//$search_keyword 
	$search_keyword = $this->input->get("search_keyword", '');
?>

<style>
    
    span.price{
        color: #1c8119;
        font-size: 18px;
        font-weight: 900;
    }
    
    .green_kupuvaci {
        background: url(../../pub/img/layout/layout-sprite_popust.png) 0 -217px no-repeat;
        padding-left: 25px;
    }

    .green_clock_bilboard {
        float: left;
        margin-top: 5px;
        text-align: center;
        background: url(../../pub/img/layout/layout-sprite_popust.png) 0 -196px no-repeat;
        width: 20px;
        height: 20px;
    }
    
</style>

<div class="homepage-content" id="homepage-content">

    <div class="layout-wrapper clear">

        <div class="deals-matrix">
            
            <?php
            $all_num = 0;
            
            foreach ($allOffers as $singleDeal) {

                $all_num++;

                //se pravi logika za vremeto da se prikaze
                $timeLeft = strtotime($singleDeal->end_time) - time();
                $timeStart = strtotime($singleDeal->start_time);
                $timeEnd = strtotime($singleDeal->end_time);
                $timeNow = time();
                ?>
                <div class="deal-item" id="deal-<?php print $singleDeal->id; ?>">
                    <img src="/pub/deals/<?php print $singleDeal->side_img ?>" alt="<?php print strip_tags($singleDeal->title_mk_clean); ?>"/>
                    <h3> <?php print commonshow::Truncate(strip_tags($singleDeal->title_mk_clean)); ?></h3>

                    <div class="item-description">

                        <span class="price"> <?php
                            if ($singleDeal->tip == 'cena_na_vaucer')
                                print $singleDeal->price_voucher;
                            else
                                print $singleDeal->price_discount;
                            ?> ден.
                        </span>
                        <small class="today"><?php echo commonshow::staticCountDown(strtotime($singleDeal->start_time), strtotime($singleDeal->end_time)); ?></small>

                        <span class="icon customers" style="color:  #333; font-weight: bold">
                            <?php
                            $count = 0;
                            if (isset($voucherCount[$singleDeal->id]))
                                $count = $voucherCount[$singleDeal->id];

                            $allDealsTooltipTxt = commonshow::tooltip($count, $singleDeal->min_ammount, $singleDeal->max_ammount);
                            ?>
                            <span title="<?php echo $allDealsTooltipTxt; ?>">
                                <?php
								if($count > 0)
								{
									print commonshow::image($count, $singleDeal->max_ammount);
									if (commonshow::isSoldOut($count, $singleDeal->max_ammount))
										print "<span style='color: red'>";

									print commonshow::number($count, $singleDeal->min_ammount, $singleDeal->max_ammount);
	//                                print commonshow::text($count);

									if (commonshow::isSoldOut($count, $singleDeal->max_ammount))
										print "</span>";
								}
                                ?>
                            </span>
                        </span>

                        <?php
                        if ($singleDeal->price > 0) {
                            ?>								
                            <span class="discount">
                                -
                                <strong><?php print (int) round(100 - ($singleDeal->price_discount / $singleDeal->price) * 100); ?></strong> 
                                %
                            </span>
                            <?php
                        }// if($singleDeal->price > 0) 
                        ?>

                        <?php
                        if (commonshow::newoffer(strtotime($singleDeal->start_time))) {
                            ?>								
                            <span class="newoffer">нова</span>
                            <?php
                        }// if(commonshow::newoffer(strtotime($singleDeal->start_time)))
                        ?>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>


                <?php
            } // foreach ($allOffers as $singleDeal) 
            ?>


        </div>
        <?php
        if ($all_num < 10) {
            //Hide the pagination button
        } else {
        ?>
            <div class="pagination">
                <input type="hidden" id="search-keyword" name="search-keyword" value="<?php echo $search_keyword ?>">
                <input type="hidden" id="pagination-search-cnt" name="pagination-search-cnt" value="0">
                <input type="hidden" id="dateTimeUnique" name="dateTimeUnique" value="<?php echo $dateTimeUnique; ?>">
                <button id="pagination-search-btn" class="ui-btn" name="pagination" >Прикажи повеќе понуди</button>
            </div>

        <?php } ?>

        <?php if (count($allOffers) == 0) { ?>
            <div class="deal-item" >
                <p>
                    Почитувани,<br>
                    Не пронајдовме таков збор во нашите понуди. <br>
                    Ве молиме обидете се повторно.<br><br>  
                    Со почит,<br>   
                    Kupinapopust.mk 
                </p>
                <img border="0" alt="Kupinapopust.mk" src="/pub/img/layout/novo_logo15noe2013.png">
            </div>
        <?php }//if(count($allOffers) == 0) {     ?>
    </div>		
</div>
