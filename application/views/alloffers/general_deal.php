<div class="row">
	<div class="col-md-3 hidden-xs hidden-sm" style="padding-left: 0px; padding-right: 0px;">
		<aside class="sidebar-left">
			<!-- /////////////////////////  MENI  ///////////////////////// -->
			<?php 
				require_once APPPATH . 'views/layouts/category_menu.php';
			?>
		</aside>            
	</div>
	
	<div class="col-md-9" id="scrollHere">
		
		<h2><?php echo $title_mk_clean; ?></h2>

		<div class="row">	
			
			
			<div class="col-md-2 col-md-offset-5">
				<div class="product-view pull-right">
					<!--  <a class="fa fa-th-large active" href="#"></a>
					<a class="fa fa-list" href="#"></a>-->
				</div>
			</div>
		</div>


		<?php 
			  if (count($bottomOffers) > 0) 
			  { 
				$bottom_offers_div_class = "col-md-4";
				require_once APPPATH . 'views/index/bottom_offers.php';
			  } // if(count($bottomOffers) > 0) { 
		?>

	</div> <!-- END OF: <div class="col-md-9"> -->
</div> <!-- END OF: <div class="row"> -->
		
