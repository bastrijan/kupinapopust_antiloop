<div class="row">
	<div class="col-md-4">
		<aside class="sidebar-left">
			<?php require_once APPPATH . 'views/layouts/contact.php'; ?>
			<?php require_once APPPATH . 'views/layouts/bezbednost.php'; ?>
			<?php require_once APPPATH . 'views/layouts/satisfaction.php'; ?>
			<div class="gap hidden-xs"></div>
		</aside>
	</div>
	
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-12 bg-white my-padd">
		
				<h3><i class="fa fa-check-square"></i>Успешно г<?php echo ($cntNaracki > 1 ? "и" : "о"); ?> нарачавте ваши<?php echo ($cntNaracki > 1 ? "те" : "от"); ?> одбран<?php echo ($cntNaracki > 1 ? "и" : ""); ?> производ<?php echo ($cntNaracki > 1 ? "и" : ""); ?>!</h3>
				<div class="gap"></div>

				<?php foreach ($payAttemptDataArr as $arrKey => $payAttemptData)  { ?>
					<div class="row border-bottom mb10">
						<div class="col-md-4 ">
						<img alt="Deal Image" src="/pub/deals/<?php print $dealDataArr[$arrKey]->side_img; ?>" class="img-responsive"/></div>
						<div class="col-md-8">
							<p class="bold"> <?php print strip_tags($dealDataArr[$arrKey]->title_mk_clean); ?> </p>

							<p><strong>Количина:</strong> <?php print $payAttemptData->quantity; ?></p>	
						</div>
					</div>
				<?php }  ?>
				
				<div class="gap"></div>


				<div class="row border-bottom mb10">
					<div class="col-md-12">
						<span >
							<?php 
								if ($payAttemptDataArr[0]->otkup_prevzemanje == 1) 
								{
									echo "Ве молиме почекајте да ве информираме кога производот би бил достапен за подигнување од нашите канцеларии. Најчесто во рок од 7 до 14 дена ги имаме производите подготвени за подигнување.";
								}
								else 
								{
									echo "Производот ќе го добиете во рок наведен во понудата.<br/>";
									echo "<b>Напомена: При превземање на производот ја плаќате цената на производот плус цената за достава (освен за некои понуди каде што доставата е бесплатна).</b>";								
								}
							?>
								
						</span>
					</div>
					<div class="col-md-12">
						<a class="btn btn-lg green mt20" href="/">OK</a>
					</div>
				</div>


				<div class="row mb10  border-bottom">
					<div class="col-md-12">
						<h3> Споделете ја понудата со вашите пријатели.</h3>
						<h4> Убаво е и тие да дозанаат за попустот.</h4>
					</div>
					

						<div class="col-md-12 mb10">
							<!-- <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="recommend" data-show-faces="true" data-share="true"></div> -->
							<fb:like href="<?php print $url; ?>" send="true" width="450" show_faces="false" action="recommend" font="arial"></fb:like>
						</div>
						
						<div class="col-md-12 mb10">
							<!--
							<a href="https://twitter.com/share" class="twitter-share-button" data-url="@Model.FriendlyUrl" data-text="@Model.Name" data-via="KlikniJadi">Tweet</a>
							<script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + '://platform.twitter.com/widgets.js'; fjs.parentNode.insertBefore(js, fjs); } }(document, 'script', 'twitter-wjs');</script>
							-->
							<a href="https://twitter.com/share" class="twitter-share-button" data-lang="en" data-url="<?php print $url; ?>" >Tweet</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
						</div>
					
						<div class="col-md-12 mb10">
							<!-- <div class="g-plusone" data-size="medium"></div> -->
							
							<!-- Place this tag where you want the +1 button to render -->
							<g:plusone size="medium" href="<?php print $url; ?>"></g:plusone>

							<!-- Place this render call where appropriate -->
							<script type="text/javascript">
								(function() {
									var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
									po.src = 'https://apis.google.com/js/plusone.js';
									var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
								})();
							</script>
						</div>

					
					<div class="col-md-12 mb10">
						<a class="btn btn-md green popup-text" href="#tellafriend_button" data-effect="mfp-move-from-top">Испрати e-mail</a>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="gap"></div>
</div>

<?php 
$dealData = $dealDataArr[0];
require_once APPPATH . 'views/layouts/tellafriend_popup.php'; 
?>