<script type="text/javascript">

	$(document).ready(function () {
		/*
		setTimeout(function(){
			$('#pokaziOstanati').hide();
		}, 3000);
		*/

		//
		$(".imgIntervention img").css({height: ''}).addClass("img-responsive");

	});  
	
</script>

<?php
	$controller = Router::$controller;
	$action = Router::$method;
?>

	<div class="row">
		<div class="col-md-8 col-md-push-4">
			<div class="row">
				<div class="col-md-12 imgIntervention">
					<h1 ><?php print $pageTitle; ?></h1>
					
					<?php
						print $pageContent;
						
						//if ($pageData->identifier == 'kakorabotime')
						//    require_once APPPATH . 'views/layouts/newsletter.php';
					?>


					<?php
						  if ($pageData->identifier == 'black_friday') { 
					?>
							<script language="Javascript" type="text/javascript" src="/pub/js/jquery.lwtCountdown-1.0.js"></script>
							<link rel="Stylesheet" type="text/css" href="/pub/css/finalCountDown.css"></link>

							<!-- Countdown dashboard start -->
							<div id="countdown_dashboard">


								<div class="dash days_dash">
									<span class="dash_title">Денови</span>
									<div class="digit">0</div>
									<div class="digit">0</div>
								</div>

								<div class="dash hours_dash">
									<span class="dash_title">Часови</span>
									<div class="digit">0</div>
									<div class="digit">0</div>
								</div>

								<div class="dash minutes_dash">
									<span class="dash_title">Минути</span>
									<div class="digit">0</div>
									<div class="digit">0</div>
								</div>

								<div class="dash seconds_dash">
									<span class="dash_title">Секунди</span>
									<div class="digit">0</div>
									<div class="digit">0</div>
								</div>

							</div>
							<!-- Countdown dashboard end -->

							
							<script language="javascript" type="text/javascript">
								jQuery(document).ready(function() {
									$('#countdown_dashboard').countDown({
										targetDate: {
											'day': 		29,
											'month': 	11,
											'year': 	<?php echo date("Y");?>,
											'hour': 	0,
											'min': 		0,
											'sec': 		0
										},
										omitWeeks: true
									});
						
								});
							</script>

					<?php } ?>



					<?php
						if ($pageData->identifier == 'kakodopoeni') { 
					?>
							<img src="/pub/img/poeni.jpg" alt="Poeni" />
							<!--
							<div style="width: 100%; background-color: white; margin-top: 20px;">
								<div style="margin: auto; width: 60%;">
									<img style="width: 212px; margin: 20px" src="/pub/img/layout/Slika1.jpg" alt="slika 1">
									<img style="width: 136px; margin: 20px" src="/pub/img/layout/Slika2.jpg" alt="slika 2">
								</div>
							</div>
							-->
					<?php } ?>
				</div>
			</div>
			<div class="gap"></div>
		</div>
		
		<div class="col-md-4 col-md-pull-8">
			<aside class="sidebar-left">
			
				<?php
					require_once APPPATH . 'views/layouts/contact.php';
					require_once APPPATH . 'views/layouts/satisfaction.php';
					require_once APPPATH . 'views/layouts/side_static_links.php';
                ?>
				<!-- 
				<div class="gap hidden-xs"></div>
				<img class="img-responsive hidden-xs" src="/pub/img/020215014417988gamarde-baner.gif" />
				<div class="gap hidden-xs"></div>
				-->
				<div class="gap hidden-xs"></div>
			</aside>            

		</div>
	  
	</div>

