<div class="row">
	<div class="col-md-12">
		
		<h1>Валидирање на ваучер</h1>

		<div class="row">
			<div class="col-md-12 bg-white my-padd text-center">

				<?php if($error_msg != "") { ?>
					<div class="row mb10 buyers-count error_msg bold">
						<?php print $error_msg; ?>
					</div>	
				<?php } //if($error_msg ! = "") { ?>

				<?php if($error_msg == "") { ?>
				<div class="row">
					<div class="col-md-12">
						<form  name="validateVoucherQR" id="validateVoucherQR" class="form-horizontal" method="post" action="/validate/mark_as_used">
							<input type="hidden" name="voucher_id" value="<?php print $voucherData->id; ?>" />
							<input type="submit" id="submitBtnQR" value="Означи како искористен" class="btn btn-lg green" />
						</form>	
					</div>
				</div>
				<div class="gap-small"></div>
				<?php } //if($error_msg ! = "") { ?>
				

				<div class="row border-bottom mb10 ">
					<div class="col-md-6 ">
						<img alt="Deal Image" src="/pub/deals/<?php print $dealData->side_img; ?>" class="img-responsive"/>
					</div>
					<div class="col-md-6 text-left">
						<p class="bold"> <?php print strip_tags($dealData->title_mk_clean); ?> </p>
	
					</div>
				</div>	
				<div class="row mb10 border-bottom">
					<div class="col-md-6  bold">Ваучер код</div>
					<div class="col-md-6 ">
							<?php print $voucherData->code; ?>				
					</div>
				</div>
				<div class="row mb10 border-bottom">
					<div class="col-md-6  bold">E-mail</div>
					<div class="col-md-6 ">
							<?php print $payAttemptData->email; ?>				
					</div>
				</div>

				<div class="row mb10 border-bottom">
					<div class="col-md-6  bold">Купен на</div>
					<div class="col-md-6 ">
							<?php print date("d.m.Y", strtotime($voucherData->time));; ?>				
					</div>
				</div>
				<div class="row mb10 border-bottom">
					<div class="col-md-6  bold">Може да се искористи до</div>
					<div class="col-md-6 ">
							<?php print date("d.m.Y", strtotime($dealData->valid_to)); ?>				
					</div>
				</div>
				<div class="row mb10 border-bottom">
					<div class="col-md-6  bold">Цена</div>
					<div class="col-md-6 ">
							<?php print $dealData->finalna_cena; ?>	ден.			
					</div>
				</div>


			</div>
		</div>

	</div>
</div>
<div class="gap-small"></div>