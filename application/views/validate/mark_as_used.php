

<div class="row">
	<div class="col-md-12">
		
		<h1>Валидирање на ваучер</h1>

		<div class="row">
			<div class="col-md-12 bg-white my-padd text-center">
				
				<div class="row mb10 buyers-count text-green bold">
					Ваучерот е успешно означен како искористен!
				</div>	
				
				<input type="submit" value="Листа на сите купени ваучери" class="btn btn-lg green" onclick="location.href = '/partner';return false;">

			</div>
		</div>

	</div>
</div>
<div class="gap-small"></div>