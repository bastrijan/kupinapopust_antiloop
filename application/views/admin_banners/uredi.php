<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once APPPATH.'views/admin/menu.php' ; ?>

<h1><?php echo (!isset($bannersData) ? "Внеси" : "Уреди"); ?> банер</h1>
	
<form method="post" action="" id="deal_save_form" enctype="multipart/form-data">
    <div class="container_12 homepage-billboard-dhtml">
        <div class="grid_12 alpha omega">
            
            <?php if (isset($bannersData)) { ?>
			    <input type="hidden" name="id" value="<?php print $bannersData[0]->id ?>">
            <?php } ?>

            <div class="row clearfix">
                <div class="grid_4 alpha omega">Позиција</div>
                <div class="grid_8 alpha omega" >
                    <select name="banner_location">
                        <option value="1" <?php if (isset($bannersData) && $bannersData[0]->banner_location == 1) print "selected" ?> ><?php print "Header банер 1"; ?></option>
                        <option value="2" <?php if (isset($bannersData) && $bannersData[0]->banner_location == 2) print "selected" ?> ><?php print "Header банер 2"; ?></option>
                        <option value="3" <?php if (isset($bannersData) && $bannersData[0]->banner_location == 3) print "selected" ?> ><?php print "Страничен банер"; ?></option>
                        <option value="4" <?php if (isset($bannersData) && $bannersData[0]->banner_location == 4) print "selected" ?> ><?php print "Мобилен банер"; ?></option>
                    </select>
                </div>
            </div>

            <div class="row clearfix">
                <div class="grid_4 alpha omega">Име на компанија</div>
                <div class="grid_8 alpha omega"><input type="text" name="company_name"  value="<?php if (isset($bannersData)) print $bannersData[0]->company_name ?>" ></div>
            </div>

            <div class="row clearfix">
                <div class="grid_4 alpha omega">Име на банер</div>
                <div class="grid_8 alpha omega"><input type="text" name="name"  value="<?php if (isset($bannersData)) print $bannersData[0]->name ?>" ></div>
            </div>
          
            <div class="row clearfix">
                <div class="grid_4 alpha omega">Линк</div>
                <div class="grid_8 alpha omega"><input size="90" type="text" name="banner_link"  value="<?php if (isset($bannersData)) print $bannersData[0]->banner_link ?>" ></div>
            </div>

            <div class="row clearfix">
                <div class="grid_4 alpha omega">Слика или GIF анимација
                    <?php if (isset($bannersData) && $bannersData[0]->banner_ext != "") { ?>
                        <br/><a href="/pub/img/header_banner/<?php print $bannersData[0]->id.".".$bannersData[0]->banner_ext."?x=".time(); ?>" target="_blank">Преглед</a>
                    <?php } ?>
                </div>
                <div class="grid_8 alpha omega"><input type="file" name="main_picture"></div>
            </div>

            <div class="row clearfix">
                <div class="grid_4 alpha omega">Лимит на импресии<br/>(0 за неограничено)</div>
                <div class="grid_8 alpha omega"><input type="text" name="impressions_limit"  value="<?php if (isset($bannersData)) print $bannersData[0]->impressions_limit ?>" ></div>
            </div>


            <div class="row clearfix">
                <div class="grid_4 alpha omega">Активен</div>
                <div class="grid_8 alpha omega" >
                    <select name="active">
                        <option value="0" <?php if (isset($bannersData) && $bannersData[0]->active == 0) print "selected" ?> >Не</option>
                        <option value="1" <?php if (isset($bannersData) && $bannersData[0]->active == 1) print "selected" ?> >Да</option>
                    </select>
                </div>
            </div>

            <div class="row clearfix">
                <input type="submit" value="Зачувај" value="save_btn">
                <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/abanners';return false;">
            </div>
            
        </div>
    </div>
</form>


