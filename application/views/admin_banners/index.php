<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once APPPATH.'views/admin/menu.php' ; ?>

		<h1>Банер систем</h1>
		<br/>
<div class="container_12 homepage-billboard">
    <div class="clearfix" style="margin-bottom: 30px">
		<?php
			print "<a href='/abanners/uredi'>Внеси нов банер</a>";
		?>
		<table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
			<!-- HEAD -->
			<tr>
				<td><strong>Позиција</strong></td>
				<td><strong>Име на компанија</strong></td>
				<td><strong>Име на банер</strong></td>
				<td style="text-align: center;"><strong>Лимит</strong></td>
				<td style="text-align: center;"><strong>Бројач</strong></td>
				<td style="text-align: center;"><strong>Активен</strong></td>
				<td ><strong>&nbsp;</td>
			</tr>
			<tr>
				<td  colspan="4"><hr></td>
			</tr>
			<!-- END HEAD -->
		<?php
		if ($bannersData) 
		{
			foreach ($bannersData as $banner) 
			{
				$location_str = "";

				switch ($banner->banner_location) {
					case 1:
						$location_str = "Header банер 1";
						break;
					
					case 2:
						$location_str = "Header banner 2";
						break;

					case 3:
						$location_str = "Страничен банер";
						break;

					case 4:
						$location_str = "Мобилен банер";
						break;

					default:
						$location_str = "Без локација";
						break;
				}

				$aktiven_str = "";

				switch ($banner->active) {
					case 1:
						$aktiven_str = "Да";
						break;

					default:
						$aktiven_str = "Не";
						break;
				}
		?>
			<tr>
				<td ><?php echo $location_str; ?></td>
				<td ><?php echo $banner->company_name; ?></td>
				<td ><?php echo $banner->name; ?></td>
				<td style="text-align: center;"><?php echo ($banner->impressions_limit ? $banner->impressions_limit : "Неограничено"); ?></td>
				<td style="text-align: center;"><?php echo $banner->impressions_cnt; ?></td>
				<td style="text-align: center;"><?php echo $aktiven_str; ?></td>

				<td >
					<?php
						print html::anchor("/abanners/uredi?id=$banner->id", "Промени");
						print "<br/>";
						print html::anchor("/abanners/izbrisi?id=$banner->id", "Избриши", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да го избришеш овој банер?")'));
					?>
				</td>

			</tr>
			<tr>
				<td  colspan="4"><hr></td>
			</tr>
		<?php
			}
		}//if ($finreportdata) {
		?>		
		</table>
		
    </div>
</div>