<div class="row row-wrap">
				
		<?php
		$past_num = 0 ;
		
		foreach ($allOffers as $singleDeal) 
		{
			$past_num++;
			
			//se pravi logika za vremeto da se prikaze
			/*
			$timeLeft_bo = strtotime($singleDeal-> end_time) - time();
			$timeStart_bo = strtotime($singleDeal->start_time);
			$timeEnd_bo = strtotime($singleDeal->end_time);
			$timeNow_bo = time();
			*/

			$title_mk_clean = "";
			$title_mk = "";
			$max_ammount = 0;
			$ceni_od_txt = "";

			if($singleDeal->options_cnt > 1)
			{
				$title_mk_clean =  $singleDeal->title_mk_clean_deal;
				$title_mk =  $singleDeal->title_mk_deal;
				$max_ammount = 0;
				$ceni_od_txt = ($singleDeal->finalna_cena > 0 ? "од ".$singleDeal->finalna_cena." ден." : "Превземи купон");
			}
			else
			{
				$title_mk_clean =  $singleDeal->title_mk_clean;
				$title_mk =  $singleDeal->title_mk;
				$max_ammount = $singleDeal->max_ammount;
				$ceni_od_txt = ($singleDeal->finalna_cena > 0 ? $singleDeal->finalna_cena." ден." : "Превземи купон");
			}

			$detali_za_ponudata = "/deal/index/" . seo::DealPermaLink($singleDeal->deal_id, $title_mk_clean, $singleDeal->category_id, $singleDeal->subcategory_id, $categoriesData, $subCategoriesData);

		?>
			<div class="col-md-3">

				 <div class="product-thumb">
					<header class="product-header">
						<!-- /////////////// Link do ponudata /////////////// -->
						<a href ="<?php print $detali_za_ponudata; ?>" target="_blank">
							
							<!-- /////////////// Slika od ponudata /////////////// -->
							<img src="/pub/deals/<?php print $singleDeal->side_img; ?>" alt="<?php print str_replace("\"", "'", strip_tags($title_mk_clean)); ?>"  />
						</a>
						
						<!-- /////////////// Popust /////////////// -->
						
						<?php if($singleDeal->price > 0)
								print '<span class="product-save popust-na-slika">'."- " . (int)round(100 - ($singleDeal->price_discount / $singleDeal->price) * 100) . "%".'</span>';
						?>
					

						
					</header>
					<div class="product-inner" >
						<h5 style="height: <?php echo Kohana::config('settings.so_partner_height');?>px">
							<?php print commonshow::Truncate(strip_tags($singleDeal->name), Kohana::config('settings.so_partner_letters')); ?>
						</h5>
						<div class="gap-small"></div>
							
						<!-- /////////////// Naslov na ponudata /////////////// -->
						<a href ="<?php print $detali_za_ponudata; ?>" target="_blank">
							<h5 class="product-title" style="height: <?php echo Kohana::config('settings.so_title_height');?>px"><?php print commonshow::Truncate(strip_tags($title_mk_clean), Kohana::config('settings.so_title_letters')); ?></h5>
						</a>
						<div class="product-meta">
						
							<!-- /////////////// Vreme do krajot na ponudata /////////////// -->
							<span class="product-time"><i class="fa fa-clock-o"></i>
								<small style="color:#FF0000"><?php print date("Истечено на d.m.Y",strtotime($singleDeal->end_time)); ?></small>
							</span>
							
							<!-- //////////////////// Broj na kupuvaci //////////////////// -->
							<?php
								$count = $singleDeal->voucher_count;								
								$allDealsTooltipTxt = commonshow::tooltip($count, $singleDeal->min_ammount, $max_ammount);

								print '<h6 class="text-green"><strong class="buyers-count" title="' . $allDealsTooltipTxt . '">';
								if($count > 0)
								{
									if(commonshow::isSoldOut($count, $max_ammount)) {
										print "<span style='color: red'>";
										$kupi_style = "btn-danger";
										$kupi_tooltip = "Распродадено";
										$onclick = 'sold_out(); return false;';
									} else {
										$kupi_style = "green";
										$kupi_tooltip = "Купи";
										$onclick = '';
									}
									
									print '<i class="fa fa-level-up"></i>&nbsp;';
									print commonshow::number($count, $singleDeal->min_ammount, $max_ammount);
									print commonshow::text($count);
									
									if(commonshow::isSoldOut($count, $max_ammount))
										print "</span>";
								} else {
									print "&nbsp;";
									$kupi_style = "green";
									$kupi_tooltip = "Купи";
									$onclick = '';
								}
								print '</strong></h6>';
							?>
							
							<ul class="product-price-list">
								
								<!-- /////////////// Cena /////////////// -->
								<li>
									<span class="product-price">
										<?php print $ceni_od_txt; ?>
									</span>
								</li>
								
								<!-- /////////////// Stara cena /////////////// -->
								<li>
									<?php if($singleDeal->options_cnt <= 1 && $singleDeal->price > 0) { ?>
										<span class="product-old-price">
											<?php	print $singleDeal->price . " " . kohana::lang("prevod.ден"); ?>
										</span>
									<?php } ?>
								</li>
							</ul>

						</div>
					</div>
				</div>
			</div>
							
		<?php } // foreach ($bottomOffers as $singleDeal)
		
		?>
			

</div> <!-- END OF: <div class="row row-wrap"> -->
<div style="text-align: center">
	<?php echo $this->pagination; ?>
</div>
