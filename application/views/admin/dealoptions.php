<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php
if (isset($dealData)) {
    $deal_id = $dealData[0]->id;
    $deal_name = $dealData[0]->title_mk;
} 
else {
    url::redirect("/admin");
}
?>
<?php require_once 'menu.php'; ?>

<h3>Опции на понудата:  </h3>
<?php echo $deal_name ?>
<div style="text-align: center">
<?php 
    print html::anchor("/admin/autosave_deal/".$deal_id, "<strong>Отвори ја Понудата во нов таб</strong>", array('target' => '_blank')); 
?>
<div/> 
<br/> 

<div class="container_12 homepage-billboard">
    <div class="clearfix" style="margin-bottom: 30px">

        <?php
        print "<a href='/admin2/uredi_dealoption/0/$deal_id'>Внеси нова oпција</a><br><br>";
        ?>
        <table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
            <!-- END HEAD -->
            <?php
            if ($dealOptionsData) 
            {
                foreach ($dealOptionsData as $dealOption) 
                {
                    ?>
                    <tr>
                        <td >
                            <?php echo $dealOption->title_mk; ?><br/>
                            <strong>
                                Опција ID: <?php echo $dealOption->deal_option_id; ?><br/>
                                Активна: <?php echo ($dealOption->active == 0 ? "Не" : "Да"); ?><br/>
                                Редовна цена: <?php echo $dealOption->price; ?>&nbsp;&nbsp;&nbsp;&nbsp;Намалена цена: <?php echo $dealOption->price_discount; ?>
                                &nbsp;&nbsp;&nbsp;&nbsp;Провизија: <?php echo $dealOption->price_voucher; ?>&nbsp;&nbsp;&nbsp;&nbsp;Цена кон купувач: <?php echo $dealOption->finalna_cena; ?><br/>
                                Може да се искористи : 
                                <?php echo ($dealOption->valid_from != "0000-00-00 00:00:00") ? date("d/m/Y", strtotime($dealOption->valid_from)) : ""; ?> 
                                - 
                                <?php echo ($dealOption->valid_to != "0000-00-00 00:00:00") ? date("d/m/Y", strtotime($dealOption->valid_to)) : ""; ?>
                            </strong>
                        </td>
    
                        <td >
                            <?php
                            print html::anchor("/admin2/uredi_dealoption/$dealOption->deal_option_id/$deal_id", "Промени");
                            print "<br/>";
                            print "<br/>";
                            print html::anchor("/admin_naracki/index?preselectedDealID=$deal_id&preselectedDealOptionID=$dealOption->deal_option_id", "Нарачки", array('target' => '_blank'))
                            ?>
                        </td>

                    </tr>
                    <tr>
                        <td  colspan="2"><hr></td>
                    </tr>
                    <?php
                }
            }//if ($finreportdata) {
            ?>		
        </table>

    </div>
</div>