<?php defined('SYSPATH') OR die('No direct access allowed.') ; ?>
<?php
if (isset($dealDataInfo)) {
    $deal_name = $dealDataInfo[0]->title_mk;
} 
else {
    url::redirect("/admin");
}
?>
<?php require_once 'menu.php' ; ?>

<?php $userID = $this->session->get("user_id"); ?>

<h3>Ваучери <?php print ($deal_option_id == 0 ? "по опции": "на единечна опција"); ?> од понудата:  </h3>
<?php echo $deal_name ?>
<br/> 


  <?php
      $vouchersModel = new Vouchers_Model() ;
      $customerModel = new Customer_Model() ;
      $dealsOptionsModel = new Deal_Options_Model() ;

      if($deal_option_id == 0)
      {
          $fiktivniVauceriByDeal = $dealsOptionsModel->getFiktivniVauceriByDeal($dealData[0]->deal_id) ;

          $realniVouchersCntVkupno = $vouchersModel->getCntPlateniVauceri($dealData[0]->deal_id) ;

          print '<div class="grid_12 alpha" style="border: black 1px dotted; text-align:center;">';
            print "Вкупен број на реални ваучери: <strong>".$realniVouchersCntVkupno.'</strong>'."&nbsp;&nbsp;&nbsp;Вкупен број на фиктивни ваучери: <strong>".$fiktivniVauceriByDeal.'</strong>' ;
            print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" ;
            print "<a href='/admin/printvoucherlist/".$dealData[0]->deal_id."' target='_blank'>Печати листа</a>";  
          print '</div>';      
          print '<br/><br/>';
      }  


  ?>



      <?php


      foreach ($dealData as $option) 
      {
          $realniVouchersCnt = $vouchersModel->getCntPlateniVauceri($option->deal_id, $option->deal_option_id) ;

          $where2 = array('deal_id' => $option->deal_id, 'deal_option_id' => $option->deal_option_id) ;
          $vouchers = $vouchersModel->getData($where2) ;

          print '<div class="container_12 homepage-billboard">' ;
          print "<div style='text-align:center;'>" . $option->title_mk."</div>"  ;
          print "<br>" ;
          print "Број на реални ваучери: <strong>".$realniVouchersCnt."</strong>&nbsp;&nbsp;&nbsp;Број на фиктивни ваучери: <strong>".$option->fiktivni_kuponi_num."</strong>";
      ?>

              <?php
              if ($vouchers) 
              {
                print '<br/><br/>';
                      
                      foreach (array_reverse($vouchers) as $voucher) {
                        print "<strong>" . $customerModel->getCustomerMail($voucher->customer_id) . "</strong>&nbsp;&nbsp;&nbsp;&nbsp;" ;
                        print "<strong>" . $voucher->code . "</strong>&nbsp;&nbsp;&nbsp;&nbsp;" ;
                        print "<strong>" . commonshow::textRezerviranSo($voucher->type, $voucher->confirmed) . "</strong>&nbsp;&nbsp;&nbsp;&nbsp;" ;
                        if ($voucher->confirmed && $voucher->activated) {
                          print "<strong>Активен</strong>" ;
                          print "&nbsp;&nbsp;&nbsp;&nbsp;" ;
                          print "<a href='/admin/printvoucher/".$voucher->id."' target='_blank'>Печати</a>";
                        }
                        else {
                          
                          if($voucher->confirmed == 0)
                            print      html::anchor("/admin/confirmvoucher/$voucher->id", "Потврди", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да го потврдиш овој ваучер?")'));
                          else
                            print "Потврден" ;

                          print "&nbsp;&nbsp;&nbsp;&nbsp;" ;
                          
                          print html::anchor("/admin/cancelvoucher/$voucher->id", "Откажи") ;
                        }
                    print "&nbsp;|&nbsp;" . html::anchor("/admin/editvoucherdate/$voucher->id/$option->deal_option_id", "Ур. датум");
                    print "&nbsp;|&nbsp;" . html::anchor("/admin/deletevoucher/$voucher->id", "Избриши", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да го избришеш овој ваучер?")'));
                  
                        //print "<strong>Платен со " . $voucher->type . "</strong>&nbsp;&nbsp;&nbsp;&nbsp;" ;
                        print "&nbsp;&nbsp;&nbsp;&nbsp;Креир. на:&nbsp;" . date("d/m/Y H:i", strtotime($voucher->time)) ;
                      print "&nbsp;&nbsp;&nbsp;&nbsp;Нар. бр.&nbsp;" . $voucher->attempt_id ;
                        print "<br><hr><br>" ;
                      }
              }
  

              //print html::anchor("/admin/deal", "Креирај попуст");
              ?>

          <br><br>
      <?php
          print "</div>" ;
      }//foreach ($dealData as $option)   
      ?>