<?php 
defined('SYSPATH') OR die('No direct access allowed.'); 

//zemi go logiraniot user
//$logiran_user_id = $this->session->get("user_id");

?>
<?php require_once 'menu.php'; ?>

<style type="text/css">

    input[type=checkbox] {
      /* All browsers except webkit*/
      transform: scale(2);

      /* Webkit browsers*/
      -webkit-transform: scale(2);
    }

</style>

<script type="text/javascript">

    $(document).ready(function () {

		$('input:checkbox.optionsFirstGroup').click(function() 
		{
		    if($('input:checkbox.optionsFirstGroup:checked').length == 0)
		    	$('input:checkbox.optionsSecondGroup').removeAttr("disabled");
		    else
		    	$('input:checkbox.optionsSecondGroup').attr("disabled", true);
		});

		$('input:checkbox.optionsSecondGroup').click(function() 
		{
		    if($('input:checkbox.optionsSecondGroup:checked').length == 0)
		    	$('input:checkbox.optionsFirstGroup').removeAttr("disabled");
		    else
		    	$('input:checkbox.optionsFirstGroup').attr("disabled", true);
		});

    });
 
</script>

<form method="post" action="" id="deal_save_form">
    <div class="container_12 homepage-billboard-dhtml">
        <div class="grid_12 alpha omega">
		
			<?php if (isset($error_msg)) { ?>
			  <div style="color: red; padding: 5px">
				<?php print $error_msg ; ?>
			  </div>
			<?php } ?>
			
			<?php
			print "<br />";
			
			//KOD_FRAGMENT_1-POCETOK: Vo produkcija moze da se izbrise
			if (isset($customers)) {
				foreach ($customers as $customer) {
					print $customer . "<br />";
				}
			}
			//KOD_FRAGMENT_1-KRAJ
			
			if (isset($message)) 
			{
				print "$message<br /><br />";
			} 
			else 
			{
				print '<div><strong>Наслов(Subject):</strong>&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="name" value="" size="100" ></div><br />';
				print "<strong>Одбери Newsletter:</strong> ";
				print
				"<select name='newsletter_type'>" .
					"<option value='newsletter'>Сите понуди (http://kupinapopust.mk/index/newsletter)</option>\n" .
					"<option value='newsletter_main_offers'>Главни понуди (http://kupinapopust.mk/index/newsletter_main_offers)</option>\n" .
					"<option value='newsletter_bestsellers'>Најпродавани понуди (http://kupinapopust.mk/index/newsletter_bestsellers)</option>\n";
					foreach ($categories as $key=>$category) {
						print"<option value='newsletter_category/$key'>Категорија - $category (http://kupinapopust.mk/index/newsletter_category/$key)</option>\n";
					}
				print "</select><br /><br />\n";

				print "<table border='0'>\n" .
						"<tr><td><strong>Испрати на:</strong></td><td></td></tr>\n" .
						
						"<tr style='height:30px;'><td ></td><td>(може да се одберат email адреси само од една поделба на email адреси)</td></tr>\n" .

						"<tr><td></td><td><strong>Поделба на email адреси 1:</td></tr>\n" .
						"<tr><td></td><td><input type='checkbox' name='gmail' value='true' class='optionsFirstGroup' />&nbsp;&nbsp;&nbsp;E-mail адреси од <strong>GMAIL<br/><br/></strong></td></tr>\n" .
						"<tr><td></td><td><input type='checkbox' name='yahoo' value='true' class='optionsFirstGroup' />&nbsp;&nbsp;&nbsp;E-mail адреси од <strong>YAHOO<br/><br/></strong></td></tr>\n" .
						"<tr><td></td><td><input type='checkbox' name='others' value='true' class='optionsFirstGroup' /><strong>&nbsp;&nbsp;&nbsp;Сите останати</strong> e-mail адреси</td></tr>\n" .

						"<tr style='height:30px;'><td></td><td></td></tr>\n" .
						"<tr><td></td><td><strong>Поделба на email адреси 2:</td></tr>\n" .
						"<tr><td></td><td><input type='checkbox' name='reg_korisnici_kupuvaci' value='true' class='optionsSecondGroup' />&nbsp;&nbsp;&nbsp;Регистрирани корисници - <strong>купувачи<br/><br/></strong></td></tr>\n" .
						"<tr><td></td><td><input type='checkbox' name='reg_korisnici_primaci_newsletter' value='true' class='optionsSecondGroup' />&nbsp;&nbsp;&nbsp;Регистрирани корисници - <strong>примачи на newsletter</strong></td></tr>\n" .

						"<tr style='height:30px;'><td ></td><td><hr></td></tr>\n" .
						"<tr><td></td><td><strong>Според активност:</td></tr>\n" .
						"<tr><td></td><td><input type='checkbox' name='aktivni_korisnici' value='true' class='optionsThirdGroup' checked />&nbsp;&nbsp;&nbsp;Активни корисници<br/><br/></td></tr>\n" .
						"<tr><td></td><td><input type='checkbox' name='neaktivni_korisnici' value='true' class='optionsThirdGroup' />&nbsp;&nbsp;&nbsp;Неактивни корисници</td></tr>\n" .
						"<tr style='height:30px;'><td></td><td></td></tr>\n" .

						"<tr style='height:30px;'><td></td><td></td></tr></table>";
				
	print '<div style="width: 200px; float: left"><strong>Датум и час:</strong>
	       </div>   
	       <div style="width: 250px; float: left"> 
				<div class="input-group date" id="datetimepicker_date_time">
				    <input type="text" class="form-control" name="date_time" value="" id="date_time"/>
				    <span class="input-group-addon">
				        <span class="glyphicon glyphicon-calendar"></span>
				    </span>
				</div>
								
		   </div>
		   <br/><br/>
			Забелешка: Системот е наместен да проверува на секој полн час дали постои newsletter кој треба да се испрати. Заради тоа опцијата за минути е отстранета од контролата (календарот) со кој се одбира датум и време.<br/>Важно: <strong>Не смее да има два newsletter-и во ист датум и час.</strong>
		   ';
					
				print 	"<br/><br/><div><input id='SendButton' type='submit' value='Зачувај'/>
				<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/></div>";
			}
			?>
        </div>
    </div>
</form>


<script type="text/javascript">
    $(function () {
        $('#datetimepicker_date_time').datetimepicker({
            format: 'YYYY-MM-DD HH',
            locale: 'mk'
        })
    });
</script>