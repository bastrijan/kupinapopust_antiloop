<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once 'menu.php'; ?>

<h1><?php echo (!isset($mesecniTrosociData) ? "Внеси" : "Уреди"); ?> месечен трошок</h1>
	
<?php if (!isset($mesecniTrosociData)) { ?>

    <form method="post" action="" id="deal_save_form">
		 
		 
        <div class="container_12 homepage-billboard-dhtml">
            <div class="grid_12 alpha omega">
                <input type="hidden" name="id" value="">
                <div class="row clearfix">
                    <div class="grid_4 alpha omega"><?php print "Месец: " ?></div>
                    <div class="grid_8 alpha omega">
						<?php print $finreport_month; ?>
						<input type="hidden" name="month" value="<?php print $finreport_month;?>">
					</div>
                </div>
                <div class="row clearfix">
                    <div class="grid_4 alpha omega"><?php print "Година: " ?></div>
                    <div class="grid_8 alpha omega">
						<?php print $finreport_year; ?>
						<input type="hidden" name="year" value="<?php print $finreport_year;?>">
					</div>
                </div>
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Опис</div>
                    <div class="grid_8 alpha omega"><input type="text" name="description" value="" ></div>
                </div>
				
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Трошок</div>
                    <div class="grid_8 alpha omega"><input type="text" name="price" value="" ></div>
                </div>

			
                <div class="row clearfix">
                    <input type="submit" value="Зачувај">
                    <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/admin/mesecnitrosoci?finreport_month=<?php echo $finreport_month ?>&finreport_year=<?php echo $finreport_year ?>';return false;">
                </div>
                
            </div>
        </div>
    </form>
<?php } ?>

<?php if (isset($mesecniTrosociData)) { ?>
    <form method="post" action="" id="deal_save_form">
        <div class="container_12 homepage-billboard-dhtml">
            <div class="grid_12 alpha omega">
				<input type="hidden" name="id" value="<?php print $mesecniTrosociData[0]->id ?>">
                <div class="row clearfix">
                    <div class="grid_4 alpha omega"><?php print "Месец: " ?></div>
                    <div class="grid_8 alpha omega">
						<?php print $finreport_month; ?>
						<input type="hidden" name="month" value="<?php print $finreport_month;?>">
					</div>
                </div>
                <div class="row clearfix">
                    <div class="grid_4 alpha omega"><?php print "Година: " ?></div>
                    <div class="grid_8 alpha omega">
						<?php print $finreport_year; ?>
						<input type="hidden" name="year" value="<?php print $finreport_year;?>">
					</div>
                </div>
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Опис</div>
                    <div class="grid_8 alpha omega"><input type="text" name="description" value="<?php print $mesecniTrosociData[0]->description ?>" ></div>
                </div>
				
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Трошок</div>
                    <div class="grid_8 alpha omega"><input type="text" name="price"  value="<?php print $mesecniTrosociData[0]->price ?>" ></div>
                </div>
               
                <div class="row clearfix">
                    <input type="submit" value="Зачувај" value="save_btn">
                    <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/admin/mesecnitrosoci?finreport_month=<?php echo $finreport_month ?>&finreport_year=<?php echo $finreport_year ?>';return false;">
                </div>
                
            </div>
        </div>
    </form>

<?php } ?>
