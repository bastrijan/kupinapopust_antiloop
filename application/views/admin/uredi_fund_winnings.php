<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once 'menu.php'; ?>
<h1><?php echo (!isset($action) ? '' : $action); ?> добивка</h1>

<?php
if (isset($action)) {
    if ($action == 'Промени') { ?>
    
        <form method="post" action="/fund_winnings/edit" id="deal_save_form">
            <div class="container_12 homepage-billboard-dhtml">
                <div class="grid_12 alpha omega">
                    <input type="hidden" name="id" value="<?php echo $article->id ?>">

                    <div class="row clearfix">
                        <div class="grid_4 alpha omega">Опис</div>
                        <div class="grid_8 alpha omega"><input type="text" name="description" value="<?php echo $article->description ?>" ></div>
                    </div>
                    
                    <div class="row clearfix">
                        <div class="grid_4 alpha omega">Добивка</div>
                        <div class="grid_8 alpha omega"><input type="text" name="prize" value="<?php echo $article->prize ?>" ></div>
                    </div>
                    
                    <div class="row clearfix">
                        <div class="grid_4 alpha omega">Количина</div>
                        <div class="grid_8 alpha omega"><input type="text" name="quantity" value="<?php echo $article->quantity ?>" ></div>
                    </div>

                    <div class="row clearfix">
                        <input type="submit" value="Зачувај">
                        <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/fund_winnings';
                                return false;">
                    </div>

                </div>
            </div>
        </form>

      
     <?php }elseif ($action == 'Додади') { ?>

        <form method="post" action="/fund_winnings/add" id="deal_save_form">
            <div class="container_12 homepage-billboard-dhtml">
                <div class="grid_12 alpha omega">
                    <input type="hidden" name="id" value="">

                    <div class="row clearfix">
                        <div class="grid_4 alpha omega">Опис</div>
                        <div class="grid_8 alpha omega"><input type="text" name="description" value="" ></div>
                    </div>
                    
                    <div class="row clearfix">
                        <div class="grid_4 alpha omega">Добивка</div>
                        <div class="grid_8 alpha omega"><input type="text" name="prize" value="" ></div>
                    </div>
                    
                    <div class="row clearfix">
                        <div class="grid_4 alpha omega">Количина</div>
                        <div class="grid_8 alpha omega"><input type="text" name="quantity" value="" ></div>
                    </div>

                    <div class="row clearfix">
                        <input type="submit" value="Зачувај">
                        <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/fund_winnings';
                                return false;">
                    </div>

                </div>
            </div>
        </form>

     <?php }
}
?>