<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once 'menu.php'; ?>

<h1><?php echo (!isset($categoriesData) ? "Внеси" : "Уреди"); ?> категорија</h1>
	
<form method="post" action="" id="deal_save_form" enctype="multipart/form-data">
    <div class="container_12 homepage-billboard-dhtml">
        <div class="grid_12 alpha omega">
            
            <?php if (isset($categoriesData)) { ?>
			    <input type="hidden" name="id" value="<?php print $categoriesData[0]->id ?>">
            <?php } ?>

            <div class="row clearfix">
                <div class="grid_4 alpha omega">Име</div>
                <div class="grid_8 alpha omega"><input type="text" name="name"  value="<?php if (isset($categoriesData)) print $categoriesData[0]->name ?>" ></div>
            </div>
			
            <div class="row clearfix">
                <div class="grid_4 alpha omega">Икона (<a href="/font_awesome_cheatsheet.html" target="_blank">Линк за бирање икона</a>)</div>
                <div class="grid_8 alpha omega"><input type="text" name="icon_html"  value="<?php if (isset($categoriesData)) print $categoriesData[0]->icon_html ?>" ></div>
            </div>

            <div class="row clearfix">
                <div class="grid_4 alpha omega">Опис на услуга</div>
                <div class="grid_8 alpha omega"><input type="text" name="service_desc"  value="<?php if (isset($categoriesData)) print $categoriesData[0]->service_desc ?>" ></div>
            </div>

            <div class="row clearfix">
                <div class="grid_4 alpha omega">Наслов на филтерот за поткатегорија</div>
                <div class="grid_8 alpha omega"><input type="text" name="subcat_filter_title"  value="<?php if (isset($categoriesData)) print $categoriesData[0]->subcat_filter_title ?>" ></div>
            </div>
           
            <div class="row clearfix">
                <input type="submit" value="Зачувај" value="save_btn">
                <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/acategories';return false;">
            </div>
            
        </div>
    </div>
</form>


