<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php
if (isset($generalOfferData)) {
    $deal_id = $generalOfferData[0]->id;
    $deal_name = $generalOfferData[0]->title_mk;
} 
else {
    url::redirect("/admin");
}

// die(print_r($allCurrentOffersData));
?>
<?php require_once 'menu.php'; ?>

<style type="text/css">

    input[type=checkbox] {
      /* All browsers except webkit*/
      transform: scale(2);

      /* Webkit browsers*/
      -webkit-transform: scale(2);
    }

</style>

<h3>Општа понуда:</h3>
<?php echo $deal_name ?>
<br/> 

<h4>Број на вклучени понуди: <?php echo $noIncludedRealOffers; ?></h4>
Забелешка: Подолу се прикажани само активните понуди. Доколку информацијата за бројот на вклучени понуди се разликува од бројот на штиклирани понуди тоа значи дека некои од штиклираните понуди се веќе истечени.
<!-- <br/>
За полесно наоѓање на штиклираните понуди направено е тие да се појават најгоре при листањето. -->

<div class="container_12 homepage-billboard">
    <div class="clearfix" style="margin-bottom: 30px">

        <form method="post" action="/admin2/general_offer_deals/<?php echo $deal_id;?>" name="form_general_offer_selected_deals" id="form_general_offer_selected_deals">

            <table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
                <!-- END HEAD -->
                <?php
                if ($allCurrentOffersData) 
                {
                    foreach ($allCurrentOffersData as $allCurrentOffer) 
                    {
                        ?>
                        <tr>
                            <td >
                                <?php echo $allCurrentOffer->title_mk; ?><br/>
                                <strong>
                                    Понуда ID: <?php print html::anchor("deal/index/".$allCurrentOffer->id, $allCurrentOffer->id, array('target' => '_blank')); ?><br/>
                                    Поч. на понуда : <?php echo date("d/m/Y  H:i:s", strtotime($allCurrentOffer->start_time)); ?> &nbsp;&nbsp;&nbsp;
                                    Крај на понуда : <?php echo date("d/m/Y  H:i:s", strtotime($allCurrentOffer->end_time)); ?>
                                </strong>
                                
                            </td>
        
                            <td >
                                <input type="checkbox" name="general_offer_selected_deals[]" class="general_offer_selected_deals" value="<?php echo $allCurrentOffer->id; ?>"  <?php echo ( in_array($allCurrentOffer->id, $selectedOfferArray) ? "checked" : "")?> >
                            </td>

                        </tr>
                        <tr>
                            <td  colspan="2"><hr></td>
                        </tr>
                        <?php
                    }
                }//if ($finreportdata) {
                ?>	

                        <tr>
                            <td colspan="2" style="text-align: right;">
                                
                                <input type="submit" name="general_offer_deals_submit" id="general_offer_deals_submit" value="Зачувај">
                            </td>

                        </tr>
            </table>

        </form>

    </div>
</div>