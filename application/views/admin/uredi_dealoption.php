<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php
if (isset($dealData)) {
    $deal_id = $dealData[0]->id;
    $deal_name = $dealData[0]->title_mk;
} 
else {
    url::redirect("/admin");
}
?>
<!-- include summernote -->
<link rel="stylesheet" href="/pub/summernote/summernote.css">
<script type="text/javascript" src="/pub/summernote/summernote.js"></script>

<?php require_once 'menu.php'; ?>

<style type="text/css">

    input[type=checkbox] {
      /* All browsers except webkit*/
      transform: scale(2);

      /* Webkit browsers*/
      -webkit-transform: scale(2);
    }

</style>

<script type="text/javascript">

    function activateAutoSaveFromDHTML() 
    {
        if ($('input[name="autosave"]').val() == 'false') {
            $('input[name="autosave"]').val('true');
            var setIntervals = setInterval(start_autosave, 30 * 1000);
        }
    };


    $(document).ready(function() {
      $('.title_mk').summernote({
            onKeyup: function(e) {
                activateAutoSaveFromDHTML();
            },
            height: 300
      });
      $('.title_mk_clean').summernote({
            onKeyup: function(e) {
                activateAutoSaveFromDHTML();
            },
            height: 300
      });
      $('.content_short_mk').summernote({
            onKeyup: function(e) {
                activateAutoSaveFromDHTML();
            },
            height: 300
      });

    });


    $(document).ready(function() {

         $('#deal_save_form').bootstrapValidator({
    //        live: 'disabled',
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

                max_ammount: {
                    validators: {

                        integer: {
                            message: 'Полето <strong>Макс. број на ваучери на располагање</strong> мора да биде пополнето со нумеричка вредност'
                        }
                    }
                },
                price: {
                    validators: {

                        notEmpty: {
                            message: 'Полето <strong>Редовна цена</strong> мора да биде пополнето'
                              }, // notEmpty

                        integer: {
                            message: 'Полето <strong>Редовна цена</strong> мора да биде пополнето со нумеричка вредност'
                        }
                    }
                },
                price_discount: {
                    validators: {

                        notEmpty: {
                            message: 'Полето <strong>Намалена цена</strong> мора да биде пополнето'
                              }, // notEmpty

                        integer: {
                            message: 'Полето <strong>Намалена цена</strong> мора да биде пополнето со нумеричка вредност'
                        }
                    }
                },
                price_voucher: {
                    validators: {

                        notEmpty: {
                            message: 'Полето <strong>Провизија</strong> мора да биде пополнето'
                              }, // notEmpty

                        integer: {
                            message: 'Полето <strong>Провизија</strong> мора да биде пополнето со нумеричка вредност'
                        }
                    }
                },

                fiktivni_kuponi_num: {
                    validators: {

                        integer: {
                            message: 'Полето <strong>Бр. на фиктивни ваучери</strong> мора да биде пополнето со нумеричка вредност'
                        }
                    }
                },
                valid_from: {
                    validators: {
                        notEmpty: {
                            message: 'Полето <strong>Може да се искористи од</strong> мора да биде пополнето'
                              },
                        date: {
                            format: 'YYYY-MM-DD HH:mm:ss',
                            message: 'Полето <strong>Може да се искористи од</strong> не е исправно пополнето'
                        }
                    }
                },
                valid_to: {
                    validators: {
                        notEmpty: {
                            message: 'Полето <strong>Може да се искористи до</strong> мора да биде пополнето'
                              },
                        date: {
                            format: 'YYYY-MM-DD HH:mm:ss',
                            message: 'Полето <strong>Може да се искористи до</strong> не е исправно пополнето'
                        }
                    }
                }
            }
        })
    });

</script>


<!-- IMETO NA STRANATA -->
<h3><?php echo (!isset($dealOptionData) ? "Внеси" : "Уреди"); ?> опција за понудата:  </h3>
<?php echo $deal_name ?>
<br/> 

<!-- FORMATA -->
<form method="post" action="" id="deal_save_form"  class="form-horizontal" enctype="multipart/form-data">
    <input type="hidden" name="autosave" value="false">

    <div class="container_12 homepage-billboard-dhtml">
        <div class="grid_12 alpha omega">
            <input type="hidden" name="id" value="<?php if (isset($dealOptionData)) print $dealOptionData[0]->id ?>">
            <input type="hidden" name="deal_id" value="<?php print $deal_id; ?>">

            <!--
            <div class="row clearfix">
                <div class="grid_4 alpha omega">Defaul Опција</div>
                <div class="grid_8 alpha omega"><?php print (!isset($dealOptionData) || !$dealOptionData[0]->default_option ? "Не" : "Да"); ?></div>
            </div>
            -->
            
            <div class="row clearfix" style="padding: 20px;">
                <strong>Наслов</strong>
                <textarea class="title_mk" name="title_mk"><?php if (isset($dealOptionData)) print $dealOptionData[0]->title_mk ?></textarea>
            </div>

            <div class="row clearfix" style="padding: 20px;">
                <strong>Наслов (за подарок)</strong>
                <textarea class="title_mk_clean" name="title_mk_clean"><?php if (isset($dealOptionData)) print $dealOptionData[0]->title_mk_clean ?></textarea>
            </div>

            <div class="row clearfix" style="padding: 20px;">
                <strong>Добивате</strong>
                <textarea class="content_short_mk" name="content_short_mk"><?php if (isset($dealOptionData)) print $dealOptionData[0]->content_short_mk ?></textarea>
            </div>

            <div class="row clearfix">
                <div class="grid_4 alpha omega">Активна</div>
                <div class="grid_8 alpha omega">
                    <input type="checkbox" name="active" <?php print (isset($dealOptionData) && $dealOptionData[0]->active ? "checked='checked'" : "");  ?> value="1" />

                    <!--
                    <?php //if (!isset($dealOptionData) || !$dealOptionData[0]->default_option) { ?>
                                <input type="checkbox" name="active" <?php //print (isset($dealOptionData) && $dealOptionData[0]->active ? "checked='checked'" : "");  ?> value="1" />
                    <?php //}else{ ?>
                                <input type="hidden" name="active" value="1"> 
                                <?php //print "Да";?>
                    <?php //} ?>
                    -->
                </div>
            </div>

            <div class="row clearfix">
                <div class="form-group" >
                    <div class="grid_4 alpha omega" style="padding-left: 35px;">Макс. број на ваучери на располагање</div>
                    <div class="col-md-8"><input type="text" name="max_ammount" class="form-control" value="<?php if (isset($dealOptionData)) print $dealOptionData[0]->max_ammount ?>" /></div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="form-group" >
                    <div class="grid_4 alpha omega" style="padding-left: 35px;">Редовна цена</div>
                    <div class="col-md-8"><input type="text" name="price" class="form-control" value="<?php if (isset($dealOptionData)) print $dealOptionData[0]->price ?>" /></div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="form-group" >
                    <div class="grid_4 alpha omega" style="padding-left: 35px;">Намалена цена</div>
                    <div class="col-md-8"><input type="text" name="price_discount" class="form-control" value="<?php if (isset($dealOptionData)) print $dealOptionData[0]->price_discount ?>" /></div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="form-group" >
                    <div class="grid_4 alpha omega" style="padding-left: 35px;">Провизија</div>
                    <div class="col-md-8"><input type="text" name="price_voucher" class="form-control" value="<?php if (isset($dealOptionData)) print $dealOptionData[0]->price_voucher ?>" /></div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="form-group" >
                    <div class="grid_4 alpha omega" style="padding-left: 35px;">Може да се искористи од</div>
                    <div class="col-md-8 dateContainer">
                        <div class='input-group date' id='datetimepicker_valid_start'>
                            <input type='text' class="form-control" name="valid_from" value="<?php if (isset($dealOptionData) && $dealOptionData[0]->valid_from != "0000-00-00 00:00:00") print $dealOptionData[0]->valid_from ?>" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>

                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#datetimepicker_valid_start').datetimepicker({
                        format: 'YYYY-MM-DD HH:mm:ss',
                        locale: 'mk'
                    });

                    $('#datetimepicker_valid_start').on("dp.change dp.show", function(ev) {
                            activateAutoSaveFromDHTML();
                            $('#deal_save_form').bootstrapValidator('revalidateField', 'valid_from');
                       });
                });
            </script>


            <div class="row clearfix">
                <div class="form-group" >
                    <div class="grid_4 alpha omega" style="padding-left: 35px;">Може да се искористи до</div>
                    <div class="col-md-8 dateContainer">
                        <div class='input-group date' id='datetimepicker_valid_end'>
                            <input type='text' class="form-control" name="valid_to" value="<?php if (isset($dealOptionData) && $dealOptionData[0]->valid_to != "0000-00-00 00:00:00") print $dealOptionData[0]->valid_to ?>" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>

                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#datetimepicker_valid_end').datetimepicker({
                        format: 'YYYY-MM-DD HH:mm:ss',
                        locale: 'mk'
                    });

                    $('#datetimepicker_valid_end').on("dp.change dp.show", function(ev) {
                            activateAutoSaveFromDHTML();
                            $('#deal_save_form').bootstrapValidator('revalidateField', 'valid_to');
                       });
                });
            </script>



            <div class="row clearfix">
                <div class="form-group" >
                    <div class="grid_4 alpha omega" style="padding-left: 35px;">Бр. на фиктивни ваучери</div>
                    <div class="col-md-8"><input type="text" name="fiktivni_kuponi_num" class="form-control" value="<?php if (isset($dealOptionData)) print $dealOptionData[0]->fiktivni_kuponi_num ?>" /></div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="form-group" >
                    <div class="grid_4 alpha omega" style="padding-left: 35px;">Инструкции за искористување на ваучерот</div>
                    <div class="col-md-8">
                        <textarea rows="10" cols="80" name="instrukcii_za_iskoristuvanje" maxlength="500"><?php if (isset($dealOptionData)) print $dealOptionData[0]->instrukcii_za_iskoristuvanje ?></textarea>
                    </div>
                </div>
            </div>


            <div class="row clearfix">
                <input type="submit" value="Зачувај">
                &nbsp;
                <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/admin2/dealoptions/<?php print $deal_id; ?>'; return false;">
            </div>


        </div>
    </div>
</form>


<script type="text/javascript">
    
    $("#deal_save_form").change(function () {
         
        activateAutoSaveFromDHTML();
            
    });
    
    
    function start_autosave() {

        $('textarea[name="title_mk"]').val($('.title_mk').code());
        $('textarea[name="title_mk_clean"]').val($('.title_mk_clean').code());
        $('textarea[name="content_short_mk"]').val($('.content_short_mk').code());
        

//        var DataSer = new FormData('form');
        var DataSer = $("#deal_save_form").serializeArray();
        jQuery.ajax({
            url: '/admin2/ajax_autosave_option',
            data: DataSer,
            enctype: 'multipart/form-data',
//            cache: false,
//            processData: false,
//            contentType: false,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data && data.status == 'success') {
                    $('input[name="id"]').val(data.id);
                } else {
                    clearInterval(setIntervals);
                }
            }// end successful POST function
        }); // end jQuery ajax call
    }
    

    


</script>