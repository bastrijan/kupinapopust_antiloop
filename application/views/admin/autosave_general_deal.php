<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<!-- include summernote -->
<link rel="stylesheet" href="/pub/summernote/summernote.css">
<script type="text/javascript" src="/pub/summernote/summernote.js"></script>

<?php require_once 'menu.php'; ?>


<script type="text/javascript">

    function activateAutoSaveFromDHTML() 
	{
        if ($('input[name="autosave"]').val() == 'false') {
            $('input[name="autosave"]').val('true');
            var setIntervals = setInterval(start_autosave, 30 * 1000);
        }
    };


    $(document).ready(function() {
      $('.title_mk').summernote({
			onKeyup: function(e) {
				activateAutoSaveFromDHTML();
			},
			height: 300
	  });
      $('.title_mk_clean').summernote({
			onKeyup: function(e) {
				activateAutoSaveFromDHTML();
			},
			height: 300
	  });
      

         $('#deal_save_form').bootstrapValidator({
    //        live: 'disabled',
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            }
            // ,
            // fields: {

            //     uploadImages: {
            //         selector: '.uploadImages',
            //         validators: {
            //             file: {
            //                 extension: 'png',
            //                 type: 'image/png',
            //                 maxSize: 500*1024,
            //                 message: 'Фајлот мора да биде PNG и помал од 500 kB'
            //             }
            //         }
            //     }

            // }
        });


    });


</script>

<div style="text-align: center">
    <strong>
        <?php echo "АДМИНИСТРИРАЊЕ НА ОПШТА ПОНУДА"  ?>
    </strong>
</div>


<?php if (!isset($dealData)) { ?>
    <form method="post" action="" id="deal_save_form"  class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" name="autosave" value="false">

        <div class="container_12 homepage-billboard-dhtml">
            <div class="grid_12 alpha omega">
                <input type="hidden" name="id" value="">

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Администратор</div>
                    <div class="grid_8 alpha omega">
                        <?php if (0) { //if($userID > 1)ako ne e Jovica ?>
                            <input type="hidden" name="vraboten_id" value="<?php print $userID; ?>">
                            <?php
                            foreach ($allVraboteni as $vraboten)
                                if ($vraboten->id == $userID)
                                    print $vraboten->name;
                            ?>
                            <?php }else { ?>
                                <select name="vraboten_id">
                                        <?php foreach ($allVraboteni as $vraboten) { ?>
                                        <option value="<?php print $vraboten->id; ?>" <?php if ($vraboten->active == 0) print "disabled"; ?>  >
                                        <?php print $vraboten->name; ?>
                                        </option>
                                <?php } ?>
                                </select>
                            <?php } ?>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Категорија</div>
                    <div class="grid_8 alpha omega">
                        <select name="category_id">
                                <?php foreach ($categoriesData as $category) { ?>
                                <option value="<?php print $category->id; ?>">
                                <?php print $category->name; ?>
                                </option>
    <?php } ?>
                        </select>
                    </div>
                </div>   
                
                  <div class="row clearfix">
                    <div class="grid_4 alpha omega">Поткатегорија</div>
                    <div class="grid_8 alpha omega">
                        <select name="subcategory_id">
                            <option value="0" >Избери Поткатегорија</option>
                          <?php foreach ($SubcategoriesData as $subcategory) { ?>
                            <option value="<?php print $subcategory->id; ?>">
                                  <?php print $subcategory->name; ?>
                            </option>
                          <?php } ?>
                        </select>
                    </div>
                </div>  

                <div class="row clearfix" style="padding: 20px;">
					<strong>Наслов</strong>
					<textarea class="title_mk" name="title_mk"></textarea>
                </div>
				
                 <div class="row clearfix" style="padding: 20px;">
					<strong>Наслов (за подарок)</strong>
					<textarea class="title_mk_clean" name="title_mk_clean"></textarea>
                </div>


                <div class="row clearfix">
                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">
                            Главна слика <br/>(PNG со димензии 800px X 300px) <br/>
  
                        </div>
                        <div class="col-md-8 ">
                            <input type="file" class="form-control uploadImages" name="main_picture" />
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">
                            Странична слика <br/>(PNG со димензии 800px X 600px) <br/>
  
                        </div>
                        <div class="col-md-8 ">
                            <input type="file" class="form-control uploadImages" name="side_picture" />
                        </div>
                    </div>
                </div>

	           			


                <div class="row clearfix">
                    <input type="submit" value="Зачувај и затвори" name="submit_btn">
                    &nbsp;
                    <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/admin';
                                return false;">
                </div>
                <br><br><br><br><br><br><br><br><br><br>
            </div>
        </div>
    </form>
<?php } 
      else//isset($dealData)
      { 
?>

    <form method="post" action="" id="deal_save_form" class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" name="autosave" value="false">


        <div class="container_12 homepage-billboard-dhtml">
            <div class="grid_12 alpha omega">
                <input type="hidden" name="id" value="<?php print $dealData[0]->id ?>">


                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Администратор</div>
                    <div class="grid_8 alpha omega">
                        <?php if (0) { //if($userID > 1)ako ne e Jovica  ?>
                            <input type="hidden" name="vraboten_id" value="<?php print $userID; ?>">
                            <?php
                            foreach ($allVraboteni as $vraboten)
                                if ($vraboten->id == $userID)
                                    print $vraboten->name;
                            ?>
                            <?php }else { ?>
                            <select name="vraboten_id">

                                    <?php foreach ($allVraboteni as $vraboten) { ?>
                                    <option <?php if ($vraboten->id == $dealData[0]->vraboten_id) echo 'selected="selected"'; ?> value="<?php print $vraboten->id; ?>" <?php if ($vraboten->active == 0) print "disabled"; ?>  >
                                    <?php print $vraboten->name; ?>
                                    </option>
                            <?php } ?>
                            </select>
    <?php } ?>
                    </div>
                </div>


                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Категорија</div>
                    <div class="grid_8 alpha omega">
                        <select name="category_id">

                                <?php foreach ($categoriesData as $category) { ?>
                                <option <?php if ($category->id == $dealData[0]->category_id) echo 'selected="selected"'; ?> value="<?php print $category->id; ?>">
                                <?php print $category->name; ?>
                                </option>
    <?php } ?>
                        </select>
                    </div>
                </div>
                
               <div class="row clearfix">
                    <div class="grid_4 alpha omega">Поткатегорија</div>
                    <div class="grid_8 alpha omega">
                        <select name="subcategory_id">
                            <option value="0" >Избери Поткатегорија</option>
                          <?php foreach ($SubcategoriesData as $subcategory) { ?>
                            <option <?php if($subcategory->id==$dealData[0]->subcategory_id) echo 'selected="selected"'; ?> value="<?php print $subcategory->id; ?>">
                                  <?php print $subcategory->name; ?>
                            </option>
                          <?php } ?>
                        </select>
                    </div>
                </div>

                
                <div class="row clearfix" style="padding: 20px;">
					<strong>Наслов</strong>
					<textarea class="title_mk" name="title_mk"><?php print $dealData[0]->title_mk ?></textarea>
                </div>
				
                <div class="row clearfix" style="padding: 20px;">
					<strong>Наслов (за подарок)</strong>
					<textarea class="title_mk_clean" name="title_mk_clean"><?php print $dealData[0]->title_mk_clean ?></textarea>
                </div>
				
    
                

                <div class="row clearfix">
                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">
                            Главна слика <br/>(PNG со димензии 800px X 300px) <br/>
                            <?php if(is_file(DOCROOT . 'pub/deals/'.$dealData[0]->main_img)) { ?> 
                                    <a href ="/pub/deals/<?php echo $dealData[0]->main_img; ?>" target="_blank">(Преглед)</a>
                  
                                    &nbsp;&nbsp;
                  
                                    <?php if(strpos($dealData[0]->main_img, $dealData[0]->id) !== false) { ?>
                                        <a onclick="return confirm('Дали си сигурен дека сакаш да ја избришеш сликата?')" href ="/admin/delete_image?deal_id=<?php echo $dealData[0]->id; ?>&image_name=<?php echo $dealData[0]->main_img; ?>&column_name=main_img" >(Избриши)</a>
                                    <?php }  ?> 
                            <?php }  ?> 
                        </div>
                        <div class="col-md-8 ">
                            <input type="file" class="form-control uploadImages" name="main_picture" />
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">
                            Странична слика <br/>(PNG со димензии 800px X 600px) <br/>
                            <?php if(is_file(DOCROOT . 'pub/deals/'.$dealData[0]->side_img)) { ?> 
                                    <a href ="/pub/deals/<?php echo $dealData[0]->side_img; ?>" target="_blank">(Преглед)</a>
                  
                                    &nbsp;&nbsp;
                  
                                    <?php if(strpos($dealData[0]->side_img, $dealData[0]->id) !== false) { ?>
                                        <a onclick="return confirm('Дали си сигурен дека сакаш да ја избришеш сликата?')" href ="/admin/delete_image?deal_id=<?php echo $dealData[0]->id; ?>&image_name=<?php echo $dealData[0]->side_img; ?>&column_name=side_img" >(Избриши)</a>
                                    <?php }  ?> 
                            <?php }  ?> 
                        </div>
                        <div class="col-md-8 ">
                            <input type="file" class="form-control uploadImages" name="side_picture" />
                        </div>
                    </div>
                </div>
  

				
				
				

                <div class="row clearfix">
                    <input type="submit" value="Зачувај и затвори" name="submit_btn">

                    &nbsp;
                    <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/admin';
                                return false;">
                </div>
                <br><br><br><br><br><br><br><br><br><br>
            </div>
        </div>
    </form>




<?php } 

?>


<script type="text/javascript">
    
    $("#deal_save_form").change(function () {
         
        activateAutoSaveFromDHTML();
            
    });
	
    
    function start_autosave() {

        $('textarea[name="title_mk"]').val($('.title_mk').code());
        $('textarea[name="title_mk_clean"]').val($('.title_mk_clean').code());


//        var DataSer = new FormData('form');
        var DataSer = $("#deal_save_form").serializeArray();
        jQuery.ajax({
            url: '/admin2/ajax_autosave_general_deal',
            data: DataSer,
            enctype: 'multipart/form-data',
//            cache: false,
//            processData: false,
//            contentType: false,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data && data.status == 'success') {
                    $('input[name="id"]').val(data.id);
                } else {
                    clearInterval(setIntervals);
                }
            }// end successful POST function
        }); // end jQuery ajax call
    }
    
     $('select[name="category_id"]').change(function () {
        var id = "";
        $( 'select[name="category_id"] option:selected' ).each(function() {
          id = $( this ).val();
        });
        
        if(id > 0){
            get_subcategories_by_id(id);
        }
        
    });
    
    function get_subcategories_by_id(id){
        
        jQuery.ajax({
            url: '/acategories/prezemi_podkategorii',
            data: { parent_id: id },
            enctype: 'multipart/form-data',
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data && data.status == 'success') {
                    
                    $('select[name="subcategory_id"]').empty();
                    $('select[name="subcategory_id"]').append(data.html);
                    
                } else {
                    alert('Се појави грешка!');
                }
            }// end successful POST function
        }); // end jQuery ajax call
        
    }

</script>