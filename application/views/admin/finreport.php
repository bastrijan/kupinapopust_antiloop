<?php defined('SYSPATH') OR die('No direct access allowed.') ; 

//zemi go logiraniot user
$logiran_user_id = $this->session->get("user_id");
?>

<?php if(!$isPrintVersion) { ?>
	<?php require_once 'menu.php' ; ?>

	<script type="text/javascript">

		$(document).ready(function() {
			$("#finreport_month").change(function () {
				$("#finreport_frm").submit();
			})

			$("#finreport_year").change(function () {
				$("#finreport_frm").submit();
			})
			
			$("#vraboten_id").change(function () {
				$("#finreport_frm").submit();
			});
			
			$("#category_id").change(function () {
				$("#finreport_frm").submit();
			});
			
			$("#partner_id").change(function () {
				$("#finreport_frm").submit();
			});

			//za COMBOBOX AUTOCOMPLETE
			$(document).ready(function() {
			    $('.js-example-basic-single').select2();
			});
		});
	</script>
<?php } //if(!$isPrintVersion) { ?>

<script type="text/javascript">

	$(document).ready(function() {
		
		$('#scroll_summary_to_bottom').click(function(){
			$('html,body').animate({
				scrollTop : $("#summary_beginning").offset().top
			}, 'fast');
			
			event.preventDefault();
		});
		
		$('#scroll_header_to_top').click(function(){
			$('html,body').animate({
				scrollTop : $("#header_beginning").offset().top
			}, 'fast');
			
			event.preventDefault();
		});
		
	});
</script>


<div class="container_12 homepage-billboard">
  <div class="clearfix" style="margin-bottom: 30px">

		<form method="get" action="" id="finreport_frm">
			<?php print "Месец: " ?> 
			<select id="finreport_month" name="finreport_month">
				<?php for($i=1; $i<=12; $i++) { ?>
				<option value="<?php echo $i?>" <?php print ($finreport_month == $i) ? 'selected="true"' : ""  ?>><?php echo $i?></option>
				<?php }//for($i=1; $i<=12; $i++)  ?>
				
			</select>
			&nbsp;&nbsp;
			<?php print "Година: " ?> 
			<select id="finreport_year" name="finreport_year">
				<?php for($i=2011; $i<=(date("Y")+ 1); $i++) { ?>
					<option value="<?php echo $i?>" <?php print ($finreport_year == $i) ? 'selected="true"' : ""  ?>><?php echo $i?></option>
				<?php }//for($i=1; $i<=12; $i++)  ?>
			</select>
			<?php if(in_array($logiran_user_id, array(1, 5))) { ?>
				&nbsp;&nbsp;
				<?php print "Администратор: " ?> 
				<select id="vraboten_id" name="vraboten_id">
				  <option <?php if(0==$vraboten_id) echo 'selected="selected"'; ?> value="0">
						  <?php print "Сите" ?>
				  </option>
				  <?php foreach ($allVraboteni as $vraboten) { 
							if($logiran_user_id == 1 || $logiran_user_id == $vraboten->id)
							{
				  ?>
				  <option <?php if($vraboten->id==$vraboten_id) echo 'selected="selected"'; ?> value="<?php print $vraboten->id; ?>">
						  <?php print $vraboten->name; ?>
				  </option>
				  <?php 
							}//if($logiran_user_id == 1 || $logiran_user_id == $vraboten->id)
						} 
				  ?>
				</select>
			<?php }//else od if($logiran_user_id > 0) {?>
		<?php
		if ($finreportdata && !$isPrintVersion) {
			print "&nbsp;&nbsp;&nbsp;<a href='/admin/finreportprint?finreport_month=".$finreport_month."&finreport_year=".$finreport_year."&category_id=".$category_id."&partner_id=".$partner_id.(($logiran_user_id == 1) ? "&vraboten_id=".$vraboten_id : "")."' target='_blank'>Печати</a>";
		}
		?>
		
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id="scroll_summary_to_bottom" href="javascript: return 0;" style="font-size: 16px;">Префрли се на сумарните</a>
		
				<br/>
				<?php print "Категорија: " ?> 
				<select id="category_id" name="category_id">
				  <option <?php if(0==$category_id) echo 'selected="selected"'; ?> value="0">
						  <?php print "Сите" ?>
				  </option>
				  <?php foreach ($allCategories as $category) { ?>
				  <option <?php if($category->id==$category_id) echo 'selected="selected"'; ?> value="<?php print $category->id; ?>">
						  <?php print $category->name; ?>
				  </option>
				  <?php } ?>
				</select>
				
				&nbsp;&nbsp;
				<?php print "Партнер: " ?> 
				<select class="js-example-basic-single" id="partner_id" name="partner_id">
				  <option <?php if(0==$partner_id) echo 'selected="selected"'; ?> value="0">
						  <?php print "Сите" ?>
				  </option>
				  <?php foreach ($allPartners as $partner) { ?>
				  <option <?php if($partner->id==$partner_id) echo 'selected="selected"'; ?> value="<?php print $partner->id; ?>">
						  <?php print $partner->name; ?>
				  </option>
				  <?php } ?>
				</select>
				
		</form>
		
		<?php
		if(!$isPrintVersion && $logiran_user_id == 1 && $vraboten_id != 0)
		{ 
			print "<br/><a target='_blank' href='/admin/vraboten/".$vraboten_id."'>";
			print "Белешка за вработен";
			print "</a><br/>";
		}
		?>		
		
		<br/>
		<table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
			<!-- HEAD -->
			<tr>
				<td width="40%"><strong>Понуда</strong></td>
				<td><strong>Цена<br/>на понудата</strong></td>
				<td><strong>Продадени<br/>ваучери</strong></td>
				<td><strong>Провизија</strong></td>
				<td><strong>Трошоци</strong></td>
				<td><strong>Платена реклама</strong></td>
			</tr>
			<tr>
				<td  colspan="6"><hr></td>
			</tr>
			<!-- END HEAD -->
		<?php
		//sumarni vrednosti
		$vk_zarabotka = 0;
		$prodadeni_vauceri = 0;
		$ddv_po_ponuda = 0;
		$ponudi_zapocnati_vo_mesecot = 0;
		
		$vk_provizii_site_ponudi = 0;
		$vk_platena_reklama_site_ponudi = 0;
		$vk_trosoci_site_ponudi = 0;
		$ddv_vkupno_site_ponuda = 0;
		
		$ddv_vkupno_18 = 0;
		$ddv_vkupno_5 = 0;
		$ddv_vkupno_oslobodeni = 0;
		$ddv_vkupno_stranstvo = 0;
		
		$promet_18 = 0;
		$promet_5 = 0;
		$promet_osloboden = 0;
		$promet_stranstvo = 0;
		
		$promet_18_with_card = 0;
		$promet_5_with_card = 0;
		$promet_osloboden_with_card = 0;
		$promet_stranstvo_with_card = 0;
		
		if ($finreportdata) {
			foreach ($finreportdata as $finreportitem) {

				//povikaj ja funcijata za presmetka na ddv-to
				$ddv_po_ponuda = round(calculate::ddv($finreportitem->price_discount, $finreportitem->price_voucher, $finreportitem->tip_danocna_sema, $finreportitem->ddv_stapka, $finreportitem->tip_ponuda, $finreportitem->payed_other_than_cash, ((date("Y-n", strtotime($finreportitem->platena_reklama_date)) == "$finreport_year-$finreport_month") ? $finreportitem->platena_reklama : 0)), 2);
								
				//povikaj ja funcijata za presmetka na sumarnite vrednosti za promet sto gi bara smetkovodstvo
		calculate::promet($finreportitem->price_discount, $finreportitem->price_voucher, $finreportitem->tip_danocna_sema, $finreportitem->ddv_stapka, $finreportitem->tip_ponuda, $finreportitem->prodadeni, $promet_18, $promet_5, $promet_osloboden, $promet_stranstvo, $finreportitem->payed_with_card, $promet_18_with_card, $promet_5_with_card, $promet_osloboden_with_card, $promet_stranstvo_with_card);
		?>
			<tr>
				<td width="40%" >
					<?php print strip_tags( ($finreportitem->options_cnt > 1 ? $finreportitem->title_mk_clean_deal." - ": "").$finreportitem->title_mk_clean ); ?>
					<br/><br/>
					<strong>
						Понуда ID: <?php echo html::anchor("/admin/autosave_deal/$finreportitem->deal_id", $finreportitem->deal_id, array("target" => "_blank")); ?><br/>
						Опција ID: <?php echo html::anchor("/admin2/uredi_dealoption/$finreportitem->deal_option_id/$finreportitem->deal_id", $finreportitem->deal_option_id, array("target" => "_blank")); ?><br/>
						
						Вработен админ.: <?php echo $finreportitem->vraboten_name; ?><br/>
						Почеток на попустот: <?php echo date("d/m/Y", strtotime($finreportitem->start_time)); ?><br/>
						Крај на попустот: <?php echo date("d/m/Y", strtotime($finreportitem->end_time)); ?><br/>
						Тип на понуда: <?php if($finreportitem->tip_ponuda == "cela_cena") print "Ваучер"; elseif($finreportitem->tip_ponuda == "cena_na_vaucer") print "Купон"; else  print "Наша цена";?><br>
						Тип на даночна шема: <?php echo $finreportitem->tip_danocna_sema; ?><br/>
						ДДВ стапка: <?php echo $finreportitem->ddv_stapka."%"; ?><br/>
						Категорија: <?php echo $finreportitem->category_name; ?><br/>
						Партнер: <?php echo $finreportitem->partner_name; ?>
					</strong>
				</td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp; <?php if ($finreportitem->tip_ponuda == 'cena_na_vaucer') print $finreportitem->price_voucher; else print $finreportitem->price_discount; ?></td>
				<td><?php echo $finreportitem->prodadeni; ?><br/>
						<strong>Card:</strong> <?php echo $finreportitem->payed_with_card; ?><br/>
						<strong>Bank:</strong> <?php echo $finreportitem->payed_with_bank; ?><br/>
						<strong>Cash:</strong> <?php echo $finreportitem->payed_with_cash; ?><br/><br/>
						
<!-- 					<strong>Web Site:</strong> <?php //echo $finreportitem->bought_via_website; ?><br/>
						<strong>Android:</strong> <?php //echo $finreportitem->bought_via_android; ?><br/>
						<strong>iOS:</strong> <?php //echo $finreportitem->bought_via_ios; ?> -->
				</td>
				<td>
					<strong>Поед.:</strong> <?php echo $finreportitem->price_voucher; ?> ден.<br/>
					<strong>Вк.:</strong> <?php echo $finreportitem->vk_zarabotka; ?> ден.
				</td>
				<td>
					<strong>Останати:</strong> <?php echo ((date("Y-n", strtotime($finreportitem->start_time)) == "$finreport_year-$finreport_month") ? $finreportitem->mesecni_trosoci : 0); ?> ден.<br/>
					<strong>ДДВ:</strong> <?php echo $ddv_po_ponuda; ?> ден.<br/>
					<strong>Вк.:</strong> <?php echo (((date("Y-n", strtotime($finreportitem->start_time)) == "$finreport_year-$finreport_month") ? $finreportitem->mesecni_trosoci : 0) + $ddv_po_ponuda); ?> ден.
				</td>
				<td style="text-align: center;"><?php echo ((date("Y-n", strtotime($finreportitem->platena_reklama_date)) == "$finreport_year-$finreport_month") ? $finreportitem->platena_reklama : 0); ?> ден.</td>
			</tr>
			<tr>
				<td  colspan="6"><hr></td>
			</tr>
		<?php
				//sumarni vrednosti
				$vk_zarabotka += $finreportitem->vk_zarabotka + ((date("Y-n", strtotime($finreportitem->platena_reklama_date)) == "$finreport_year-$finreport_month") ? $finreportitem->platena_reklama : 0) - (((date("Y-n", strtotime($finreportitem->start_time)) == "$finreport_year-$finreport_month") ? $finreportitem->mesecni_trosoci : 0) + $ddv_po_ponuda);
				$prodadeni_vauceri += $finreportitem->prodadeni;
				
				$vk_provizii_site_ponudi += $finreportitem->vk_zarabotka;
				$vk_platena_reklama_site_ponudi += ((date("Y-n", strtotime($finreportitem->platena_reklama_date)) == "$finreport_year-$finreport_month") ? $finreportitem->platena_reklama : 0);
				$vk_trosoci_site_ponudi += ((date("Y-n", strtotime($finreportitem->start_time)) == "$finreport_year-$finreport_month") ? $finreportitem->mesecni_trosoci : 0);
				$ddv_vkupno_site_ponuda += $ddv_po_ponuda;
				
				//sumarni ddv razbieni po stavki
				switch ($finreportitem->tip_danocna_sema) {
					case "ДДВ Обврзник":
						if($finreportitem->ddv_stapka == 18)
							$ddv_vkupno_18 += $ddv_po_ponuda;
						else
							$ddv_vkupno_5 += $ddv_po_ponuda;
						break;					
					case "Не ДДВ Обврзник":
						if($finreportitem->ddv_stapka == 18)
							$ddv_vkupno_18 += $ddv_po_ponuda;
						else
							$ddv_vkupno_5 += $ddv_po_ponuda;
						break;
					case "Ослободен согласно законот":
						$ddv_vkupno_oslobodeni += $ddv_po_ponuda;
						break;
					case "Промет остварен во странство":
						$ddv_vkupno_stranstvo += $ddv_po_ponuda;
						break;
				}
				
				//presmetka na ponudi koi se zapocnati vo mesecot
				if(date("Y-n", strtotime($finreportitem->start_time)) == "$finreport_year-$finreport_month")
					$ponudi_zapocnati_vo_mesecot++;
				
			}
		}//if ($finreportdata) {
		?>		
			<tr>
				<td width="40%" ><strong>Понуди започнати во месецот: <?php echo $ponudi_zapocnati_vo_mesecot; ?></strong></td>
				<td colspan="5"><strong>Продадени ваучери во месецот: <?php echo $prodadeni_vauceri; ?></strong></td>
			</tr>
			<tr>
				<td  colspan="6"><hr></td>
			</tr>
			<tr>
				<td width="40%" ><strong>Понуди во сите месеци: <?php echo $vkupen_broj_ponudi; ?></strong></td>
				<td colspan="5"><strong>Продадени ваучери во сите месеци: <?php echo $vkupen_broj_prodadeni_vauceri; ?></strong></td>
			</tr>
			<tr>
				<td  colspan="6"><hr></td>
			</tr>
		</table>
		
		<?php
		if ($finreportdata) 
		{
		?>
			<?php if( !(in_array($logiran_user_id, array(5)) && $vraboten_id == 0) ) { ?>	
			
				<div id="summary_beginning" style="text-align: right;"><strong>Вкупно провизии од понуди: <?php echo $vk_provizii_site_ponudi; ?> ден.</strong></div>
				<div  style="text-align: right;"><strong>Вкупно приходи од платена реклама: <?php echo $vk_platena_reklama_site_ponudi; ?> ден.</strong></div>
				<br>
				<div  style="text-align: right;color: #CC0000;"><strong>Вкупно останати трошоци од понуди: <?php echo $vk_trosoci_site_ponudi; ?> ден.</strong></div>
				<br>
				<div  style="text-align: right;color: #CC0000;"><strong>Вкупно ДДВ (18%): <?php echo $ddv_vkupno_18; ?> ден.</strong></div>
				<div  style="text-align: right;color: #CC0000;"><strong>Вкупно ДДВ (5%): <?php echo $ddv_vkupno_5; ?> ден.</strong></div>
				<div  style="text-align: right;color: #CC0000;"><strong>Вкупно ДДВ (Ослободен согласно законот): <?php echo $ddv_vkupno_oslobodeni; ?> ден.</strong></div>
				<div  style="text-align: right;color: #CC0000;"><strong>Вкупно ДДВ (Промет остварен во странство): <?php echo $ddv_vkupno_stranstvo; ?> ден.</strong></div>
				<div  style="text-align: right;color: #CC0000;"><strong>Сумарно ДДВ: <?php echo $ddv_vkupno_site_ponuda; ?> ден.</strong></div>
				<br>
				<div  style="text-align: right; color: #3FA80A;"><strong>Вкупна заработка: <?php echo $vk_zarabotka; ?> ден.</strong></div>
				<br>
			
			<?php }//if( !(in_array($logiran_user_id, array(5)) && $vraboten_id == 0) ) { ?>	
			
		<?php
		}//if ($finreportdata) {
		?>	
		<?php if($logiran_user_id == 1 && $category_id == 0 && $partner_id == 0 && $vraboten_id == 0) { ?>
				<div  style="text-align: right;">
					<strong >
							<?php
								if(!$isPrintVersion) 
									print "<a target='_blank' href='/admin/mesecniprihodi?finreport_month=".$finreport_month."&finreport_year=".$finreport_year."'>";
								
								print "Mесечни (општи) приходи:";
								
								if(!$isPrintVersion) 
									print "</a>";
							?>
							<?php echo $mesecniOpstiPrihodi; ?> ден.
					</strong>
					<br/>
					<strong style="color: #CC0000;">
							<?php
								if(!$isPrintVersion) 
									print "<a target='_blank' href='/admin/mesecnitrosoci?finreport_month=".$finreport_month."&finreport_year=".$finreport_year."'>";
								
								print "Mесечни (општи) трошоци:";
								
								if(!$isPrintVersion) 
									print "</a>";
							?>
							<?php echo $mesecniOpstiTrosoci; ?> ден.
					</strong>
					
				</div>
				<br>
				<div  style="text-align: right;font-size: 18px; color: #3FA80A;"><strong>Финално Салдо: <?php echo ($vk_zarabotka + $mesecniOpstiPrihodi - $mesecniOpstiTrosoci); ?> ден.</strong></div>
		<?php }
			  elseif($logiran_user_id == 1 && $category_id == 0 && $partner_id == 0) 
			  {
		?>
				<div  style="text-align: right;color: #CC0000;">
					<strong>
							<?php
								if(!$isPrintVersion) 
									print "<a target='_blank' href='/admin/vrabotentrosoci?finreport_month=".$finreport_month."&finreport_year=".$finreport_year."&vraboten_id=".$vraboten_id."'>";
								
								print "Трошоци по вработен:";
								
								if(!$isPrintVersion) 
									print "</a>";
							?>
							<?php echo $vrabotenTrosoci; ?> ден.
					</strong>
				</div>
				<br>
				<div  style="text-align: right;font-size: 18px; color: #3FA80A;"><strong>Финално Салдо: <?php echo ($vk_zarabotka - $vrabotenTrosoci); ?> ден.</strong></div>
		<?php }
		?>
		
		<?php if( !(in_array($logiran_user_id, array(5)) && $vraboten_id == 0) ) { ?>	
			<br>
			<br>
			<!-- PROMETOT STO GO BARA SMETKOVODSTVO-->
			<div  style="text-align: right;color: #007FFF;"><strong>Промет со 18% ДДВ: <?php echo $promet_18; ?> ден.</strong></div>
			<div  style="text-align: right;color: #007FFF;"><strong>Промет со 5% ДДВ: <?php echo $promet_5; ?> ден.</strong></div>
			<div  style="text-align: right;color: #007FFF;"><strong>Промет од клиенти ослободени од ДДВ согласно законот: <?php echo $promet_osloboden; ?> ден.</strong></div>
			<div  style="text-align: right;color: #007FFF;"><strong>Промет остварен во странство: <?php echo $promet_stranstvo; ?> ден.</strong></div>
			<div  style="text-align: right;color: #007FFF;"><strong>Вкупен промет: <?php echo ($promet_18 + $promet_5 + $promet_osloboden + $promet_stranstvo); ?> ден.</strong></div>
			<br/>
			<div  style="text-align: right;color: #007FFF;"><strong>Вкупен промет (само од online трансакции): <?php echo ($promet_18_with_card + $promet_5_with_card + $promet_osloboden_with_card + $promet_stranstvo_with_card); ?> ден.</strong></div>
		<?php } ?>
		<br/><br/>
		<a id="scroll_header_to_top" href="javascript: return 0;" style="font-size: 16px;">Префрли се горе</a>
  </div>
</div>