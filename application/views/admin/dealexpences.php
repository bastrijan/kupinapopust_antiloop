<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once 'menu.php' ; ?>

		<h1>Трошоци</h1>
		<br/>
		<div><?php  print $dealData->title_mk  ; ?></div>
		<br/>

<div class="container_12 homepage-billboard">
    <div class="clearfix" style="margin-bottom: 30px">

			<?php
				print "<a href='/admin/uredidealexpence/0/$dealData->id'>Внеси нов трошок</a>";
			?>

			<br/>
			<table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
				<!-- HEAD -->
				<tr>
					<td width="50%"><strong>Трошок</strong></td>
					<td ><strong>Сума</strong></td>
					<td ><strong>&nbsp;</td>
				</tr>
				<tr>
					<td  colspan="3"><hr></td>
				</tr>
				<!-- END HEAD -->
			<?php
			if ($dealExpencesData) {
				foreach ($dealExpencesData as $dealExpence) {
			?>
				<tr>
					<td width="50%" ><?php echo $dealExpence->description; ?></td>
					<td ><?php echo $dealExpence->price. " ден."; ?></td>
					<td >
						<?php
							print html::anchor("/admin/uredidealexpence/$dealExpence->id/$dealData->id", "Промени");
							print "&nbsp;&nbsp;";
							print html::anchor("/admin/izbrisidealexpence/$dealExpence->id/$dealData->id", "Избриши", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да го избришеш овој трошок?")'));
						?>
					</td>

				</tr>
				<tr>
					<td  colspan="3"><hr></td>
				</tr>
			<?php
				}
			}//if ($finreportdata) {
			?>		
			</table>
    </div>
</div>