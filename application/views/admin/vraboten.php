<?php 
defined('SYSPATH') OR die('No direct access allowed.'); 
?>

<!-- include summernote -->
<link rel="stylesheet" href="/pub/summernote/summernote.css">
<script type="text/javascript" src="/pub/summernote/summernote.js"></script>

<?php require_once 'menu.php'; ?>

  <script type="text/javascript">

    $(document).ready(function() {
      $('.notes').summernote({
			height: 300
	  });

    });
  </script>

<?php if (!isset($dealData)) { ?>
    <form method="post" action="" id="deal_save_form">
        <div class="container_12 homepage-billboard-dhtml">
            <div class="grid_12 alpha omega">
                <input type="hidden" name="id" value="">


                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Име</div>
                    <div class="grid_8 alpha omega"><input type="text" name="name" value="" ></div>
                </div>

                <div class="row clearfix" style="padding: 20px;">
					<strong>Забелешка</strong>
					<textarea class="notes" name="notes"></textarea>
                </div>

				
                <div class="row clearfix">
                    <input type="submit" value="Зачувај">

                </div>
                
            </div>
        </div>
    </form>
<?php } ?>

<?php if (isset($dealData)) { ?>

    <form method="post" action="" id="deal_save_form">
        <div class="container_12 homepage-billboard-dhtml">
            <div class="grid_12 alpha omega">
                <input type="hidden" name="id" value="<?php print $dealData[0]->id ?>">
               
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Име</div>
                    <div class="grid_8 alpha omega"><input type="text" name="name" value="<?php print $dealData[0]->name ?>" ></div>
                </div>

                <div class="row clearfix" style="padding: 20px;">
					<strong>Забелешка</strong>
					<textarea class="notes" name="notes"><?php print $dealData[0]->notes; ?></textarea>
                </div>
				
                <div class="row clearfix">
                    <input type="submit" value="Зачувај" value="save_btn">
                </div>
                
            </div>
        </div>
    </form>

<?php } ?>

