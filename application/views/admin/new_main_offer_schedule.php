<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once 'menu.php'; ?>

<h1>Внеси нов главен попуст во календар</h1>

<br/>
<div><?php  print $dealData->title_mk  ; ?></div>
<br/>
		
<form method="post" action="" id="deal_save_form">
	 
	<?php if (isset($error_msg)) { ?>
	  <div style="color: red; padding: 5px">
		<?php print $error_msg ; ?>
	  </div>
	<?php } ?>
						
    <div class="container_12 homepage-billboard-dhtml">
        <div class="grid_12 alpha omega">
			<input type="hidden" name="deal_id" value="<?php print $dealData->id;?>">
            <div class="row clearfix">
                <div class="grid_4 alpha omega">Број на Главна понуда</div>
                <div class="grid_8 alpha omega">
					<select id="main_offer" name="main_offer">
						<?php for($i=1; $i<=5; $i++) { ?>
							<option value="<?php echo $i?>" <?php print ($main_offer == $i) ? 'selected="true"' : ""  ?>><?php echo $i?></option>
						<?php }//for($i=1; $i<=5; $i++)  ?>
					</select>
				</div>
            </div>
			
            <div class="row clearfix">
                <div class="grid_4 alpha omega">Датум и час</div>
                <div class="grid_8 alpha omega">
                	  
                      <div class='input-group date' id='datetimepicker_date_time'>
                            <input type='text' class="form-control" name="date_time" value="<?php print $date_time ?>" id="date_time"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        <script type="text/javascript">
                            $(function () {
                                $('#datetimepicker_date_time').datetimepicker({
                                    format: 'YYYY-MM-DD HH',
                                    locale: 'mk'
                                })
                            });
                        </script>

				<br/>
				Забелешка: Системот е наместен да проверува на секој полн час дали постои понуда која треба да се постави за главна. Заради тоа опцијата за минути е отстранета од контролата (календарот) со кој се одбира датум и време.
				</div>
            </div>

		
            <div class="row clearfix">
                <input type="submit" value="Зачувај">
                <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/admin/';return false;">
            </div>
            
        </div>
    </div>
</form>
