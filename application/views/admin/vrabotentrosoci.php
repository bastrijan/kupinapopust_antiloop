<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once 'menu.php' ; ?>

<script type="text/javascript">

	$(document).ready(function() {
		$("#finreport_month").change(function () {
			$("#finreport_frm").submit();
		})

		$("#finreport_year").change(function () {
			$("#finreport_frm").submit();
		})
	});
</script>

		<h1>Трошоци по вработен - <?php echo $vraboten_name?></h1>
		<br/>
<div class="container_12 homepage-billboard">
    <div class="clearfix" style="margin-bottom: 30px">

		<form method="get" action="" id="finreport_frm">
			<input type="hidden" name="vraboten_id" value="<?php print $vraboten_id;?>">
			<?php print "Месец: " ?> 
			<select id="finreport_month" name="finreport_month">
				<?php for($i=1; $i<=12; $i++) { ?>
				<option value="<?php echo $i?>" <?php print ($finreport_month == $i) ? 'selected="true"' : ""  ?>><?php echo $i?></option>
				<?php }//for($i=1; $i<=12; $i++)  ?>
				
			</select>
			&nbsp;&nbsp;
			<?php print "Година: " ?> 
			<select id="finreport_year" name="finreport_year">
				<?php for($i=2011; $i<=(date("Y")+ 1); $i++) { ?>
					<option value="<?php echo $i?>" <?php print ($finreport_year == $i) ? 'selected="true"' : ""  ?>><?php echo $i?></option>
				<?php }//for($i=1; $i<=12; $i++)  ?>
			</select>
		<?php
			print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='/admin/uredivrabotentrosok?finreport_month=".$finreport_month."&finreport_year=".$finreport_year."&vraboten_id=".$vraboten_id."'>Внеси нов трошок по вработен</a>";
		?>
		</form>

		<br/>
		<table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
			<!-- HEAD -->
			<tr>
				<td width="50%"><strong>Трошок</strong></td>
				<td ><strong>Сума</strong></td>
				<td ><strong>&nbsp;</td>
			</tr>
			<tr>
				<td  colspan="3"><hr></td>
			</tr>
			<!-- END HEAD -->
		<?php
		if ($vrabotenTrosociData) {
			foreach ($vrabotenTrosociData as $vrabotenTrosoci) {
		?>
			<tr>
				<td width="50%" ><?php echo $vrabotenTrosoci->description; ?></td>
				<td ><?php echo $vrabotenTrosoci->price. " ден."; ?></td>
				<td >
					<?php
						print html::anchor("/admin/uredivrabotentrosok?finreport_month=".$finreport_month."&finreport_year=".$finreport_year."&vraboten_id=".$vraboten_id."&id=$vrabotenTrosoci->id", "Промени");
						print "&nbsp;&nbsp;";
						print html::anchor("/admin/izbrisivrabotentrosok?finreport_month=".$finreport_month."&finreport_year=".$finreport_year."&vraboten_id=".$vraboten_id."&id=$vrabotenTrosoci->id", "Избриши", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да го избришеш овој трошок по вработен?")'));
					?>
				</td>

			</tr>
			<tr>
				<td  colspan="3"><hr></td>
			</tr>
		<?php
			}
		}//if ($finreportdata) {
		?>		
		</table>
		
    </div>
</div>