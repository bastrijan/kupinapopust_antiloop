<?php
defined('SYSPATH') OR die('No direct access allowed.');
require_once 'menu.php';
?>


<div class="container_12 homepage-billboard">
    <div class="clearfix" style="margin-bottom: 30px">

        <form method="post" action="" style="float:left">
            <?php
            if(isset($ReportDate) && !empty($ReportDate)){
                $Day = $ReportDate['d'];
                $Month = $ReportDate['m'];
                $Year = $ReportDate['Y'];
            }else{
                $Day = date("j");
                $Month = date("m");
                $Year = date("Y"); 
            }
            if(strlen($Day)<2)
                $Day='0'.$Day;
            
            ?>
           
            Ден:
            <select name="day_report">

                <?php for ($i = 1; $i < 32; $i++) { ?>
                <option <?php echo $i == $Day ? 'selected' : ''; ?> > <?php echo strlen($i)<2 ? '0'.$i : $i ?> </option>
                <?php } ?>

            </select>
            Месец:
            <select name="month_report">
                <?php $monthArray = array(
                    '01' => 'Jan',
                    '02' => 'Feb',
                    '03' => 'Mar',
                    '04' => 'Apr',
                    '05' => 'May',
                    '06' => 'Jun',
                    '07' => 'Jul',
                    '08' => 'Aug',
                    '09' => 'Sep',
                    '10' => 'Oct',
                    '11' => 'Nov',
                    '12' => 'Dec',
                ); ?>
                <?php foreach ($monthArray as $monthNu => $monthStr){ ?>
                    <option value="<?php echo $monthNu; ?>" <?php echo $monthNu == $Month ? 'selected' : ''; ?> > <?php echo $monthStr; ?> </option>
                <?php } ?>
                

            </select>
            Година:
            <select name="year_report">
                <?php $CurrentYear = date("Y"); ?>
                <?php for ($i = 2010; $i <= $CurrentYear; $i++) { ?>
                    <option <?php echo $i == $Year ? 'selected' : ''; ?> > <?php echo $i; ?> </option>
                <?php } ?>

            </select>
            Играчи:
            <select name="players_report">
                <option value="all" > Сите </option>
                <option value="winners" 
                    <?php if(isset($ReportPlayers) && !empty($ReportPlayers)){
                            if($ReportPlayers == 'winners')
                                echo'selected';
                            else 
                                echo '';
                        }?>> Добитници </option>
            </select>
            
            <input type="submit" value="Филтрирај">
            </form>
            
           
            
            <br>
            <br>
            Дневен број на играчи: <strong><?php echo isset($CntDayAllPlayers) ? $CntDayAllPlayers : 0 ?></strong>
            &nbsp;&nbsp;
            Дневен број на добитници: <strong><?php echo isset($CntDayWinnerPlayers) ? $CntDayWinnerPlayers : 0 ?></strong>
            &nbsp;&nbsp;
            Месечен број на играчи: <strong><?php echo isset($CntMonthAllPlayers) ? $CntMonthAllPlayers : 0 ?></strong>
            &nbsp;&nbsp;
            Месечен број на добитници: <strong><?php echo isset($CntMonthWinnerPlayers) ? $CntMonthWinnerPlayers : 0 ?></strong>
        

        
        <div class="clear"></div>
        <br/>
        <table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
            <!-- HEAD -->
            <tr>
                <td width="50%"><strong>Играч</strong></td>

                <td><strong>Датум</strong></td>
                <td><strong>Добитник</strong></td>
                <td><strong>Добивка</strong></td>
                <td><strong>Поени подарени на</strong></td>
            </tr>
            <tr>
                <td  colspan="5"><hr></td>
            </tr>
            <!-- END HEAD -->
            
            <?php if(isset($DayReports) && !empty($DayReports) ): ?>
                <?php foreach ($DayReports as $report):?>
            
                    <tr>
                        <td width="50%" ><?php echo $report->email ?></td>
                        <td><?php echo date("d/m/Y", strtotime($report->date)) ?></td>
                        <td><?php echo $report->winner == 1 ? 'Да' : 'Не' ?></td>
                        <td ><?php echo $report->winner == 1 ? $report->prize.' Поени' : '' ?></td>
                        <td ><?php echo $report->winner == 1 ? $report->assign_prize_to_email : '' ?></td>
                    </tr>
                    <tr>
                        <td  colspan="5"><hr></td>
                    </tr>
               <?php endforeach; ?>     
            <?php endif ?>

        </table>

    </div>
</div>