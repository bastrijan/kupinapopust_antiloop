<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<!-- include summernote -->
<link rel="stylesheet" href="/pub/summernote/summernote.css">
<script type="text/javascript" src="/pub/summernote/summernote.js"></script>

<style type="text/css">

    input[type=checkbox] {
      /* All browsers except webkit*/
      transform: scale(2);

      /* Webkit browsers*/
      -webkit-transform: scale(2);
    }


    /*TAG SYSTEM*/
    .tagify--outside{
        border: 0;
        padding: 0;
        margin: 0;
        background-color: #fff;
    }

    .tagify__input--outside{
        display: block;
        width: 100%;
        border: 1px solid #DDD;
        background-color: #fff;
    }

    .tagify__input {
        min-width: 110px;
        margin: 5px 5px 5px 0px;
        padding: .3em .5em;
        position: relative;
    }

    .clear_margin_left_right
    {
        margin-left: 0px !important;
        margin-right: 0px !important;
    }

</style>

<script type="text/javascript">
    $(document).ready(function() {
        (function(){

            // alert("test");

        var input = document.querySelector('input[name=tags-outside]'),
            // init Tagify script on the above inputs
            tagify = new Tagify(input, {
                        whitelist: [<?php echo $dealAllTagsCommaDelimited; ?>],
                        callbacks: {
                            add : activateAutoSaveFromDHTML, // calls an imaginary "onAddTag" function when a tag is added
                            remove : activateAutoSaveFromDHTML
                        }
                    }
            );

            // add a class to Tagify's input element
            tagify.DOM.input.classList.add('tagify__input--outside');

            // re-place Tagify's input element outside of the <tags> element (tagify.DOM.scope), just before it
            tagify.DOM.scope.parentNode.insertBefore(tagify.DOM.input, tagify.DOM.scope);

            // $( "#deal_save_form" ).submit(function( event ) {
            //   event.preventDefault();

            //   alert( "Handler for .submit() called." );
              
            //   alert(tagify.value);

            //   alert(input.value);


            // });

        })();



    });
</script>


<?php require_once 'menu.php'; ?>


<script type="text/javascript">

    function activateAutoSaveFromDHTML() 
	{
        if ($('input[name="autosave"]').val() == 'false') {
            $('input[name="autosave"]').val('true');
            var setIntervals = setInterval(start_autosave, 30 * 1000);
        }
    };


    $(document).ready(function() 
    {

        <?php if (isset($is_clone) and $is_clone == 1) { ?>

                $().toastmessage('showToast', {
                    text     : "Успешно го креиравте клонот на избраната понуда!<br>Потребно е рачно да ги сетирате следните полиња:<br/>-(Почеток на попустот) и (Крај на попустот) кај понудата<br/>-(Може да се искористи од) и (Може да се искористи до) кај опциите<br/>-(Статус на понудата) во 'Активна понуда' кога клонираната понуда ќе биде спремна да биде активна",
                    sticky   : true,
                    position : 'top-center',
                    type     : 'success'
                });
       
        <?php } ?> 

      $('.title_mk').summernote({
			onKeyup: function(e) {
				activateAutoSaveFromDHTML();
			},
			height: 300
	  });
      $('.title_mk_clean').summernote({
			onKeyup: function(e) {
				activateAutoSaveFromDHTML();
			},
			height: 300
	  });
      
      

      $('.content_short_mk').summernote({
			onKeyup: function(e) {
				activateAutoSaveFromDHTML();
			},
			height: 300
	  });




      $('.email_content_buy').summernote({
			onKeyup: function(e) {
				activateAutoSaveFromDHTML();
			},
			height: 300
	  });


         $('#deal_save_form').bootstrapValidator({
    //        live: 'disabled',
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

                min_ammount: {
                    validators: {
                        integer: {
                            message: 'Полето <strong>Минимален број на ваучери</strong> мора да биде пополнето со нумеричка вредност'
                        }
                    }
                },
                max_per_person: {
                    validators: {

                        notEmpty: {
                            message: 'Полето <strong>Макс. број на ваучери по купувач</strong> мора да биде пополнето'
                              }, // notEmpty

                        integer: {
                            message: 'Полето <strong>Макс. број на ваучери по купувач</strong> мора да биде пополнето со нумеричка вредност'
                        }
                    }
                },
                platena_reklama: {
                    validators: {
                        /*
                        notEmpty: {
                            message: 'Полето <strong>Макс. број на ваучери по купувач</strong> мора да биде пополнето'
                              }, // notEmpty
                        */
                        integer: {
                            message: 'Полето <strong>Платена реклама - Вредност</strong> мора да биде пополнето со нумеричка вредност'
                        }
                    }
                },
                start_time: {
                    validators: {
                        notEmpty: {
                            message: 'Полето <strong>Почеток на попустот</strong> мора да биде пополнето'
                              },
                        date: {
                            format: 'YYYY-MM-DD HH:mm:ss',
                            message: 'Полето <strong>Почеток на попустот</strong> не е исправно пополнето'
                        }
                    }
                },
                end_time: {
                    validators: {
                        notEmpty: {
                            message: 'Полето <strong>Крај на попустот</strong> мора да биде пополнето'
                              },
                        date: {
                            format: 'YYYY-MM-DD HH:mm:ss',
                            message: 'Полето <strong>Крај на попустот</strong> не е исправно пополнето'
                        },
                        callback: {
                            message: '<?php print $lastValidToDate == "0000-00-00 00:00:00" ? "Полето <b>(Може да се искористи до)</b> во опциите на понудата не е внесен. Прво внесете го полето <b>(Може да се искористи до)</b> кај барем една опција од понудата и потоа вратете се тука, направете Refresh на страната, и потоа сменете го овој датум. <br/>" : "Полето <strong>Крај на попустот</strong> мора да биде пополнето со датум пред ".date("d/m/Y H:i:s", strtotime($lastValidToDate))." за да не биде поголем од датумот во полето <b>(Може да се искористи до)</b> на опцијата која последна истекува. Прво зголемето го полето <b>(Може да се искористи до)</b> кај барем една опција од понудата и потоа вратете се тука, направете Refresh на страната, и потоа сменете го овој датум."; ?>',
                            callback: function(value, validator) {
                                var m = new moment(value, 'YYYY-MM-DD HH:mm:ss', true);
                                return m.isBefore('<?php echo $lastValidToDate;?>');
                            }
                        }
                    }
                }
                // ,
                // uploadImages: {
                //     selector: '.uploadImages',
                //     validators: {
                //         file: {
                //             extension: 'png',
                //             type: 'image/png',
                //             maxSize: 500*1024,
                //             message: 'Фајлот мора да биде PNG и помал од 500 kB'
                //         }
                //     }
                // }

            }
        });

        //za COMBOBOX AUTOCOMPLETE
        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });

    });
</script>


<script type="text/javascript">
    var partnerTipDanocnaSema = new Array();
    var partnerDDVStapka = new Array();
    var firsPartnerID = <?php print $allPartners[0]->id; ?>;

<?php foreach ($allPartners as $partner) { ?>

        partnerTipDanocnaSema[<?php print $partner->id; ?>] = "<?php print $partner->tip_danocna_sema; ?>";
        partnerDDVStapka[<?php print $partner->id; ?>] = "<?php print $partner->ddv_stapka; ?>";

<?php }//foreach ($allPartners as $partner) {  ?>

    function ChangeDDVStapkaDiv()
    {

        $("#ddv_stapka_div").show();
        $("#5_posto").show();

        if ($("#tip_danocna_sema").val() == "Промет остварен во странство")
        {
            $("#ddv_stapka_div").hide();
        }
        else
        {
            if ($("#tip_danocna_sema").val() == "Ослободен согласно законот")
            {
                $('#ddv_stapka option[value="18"]').attr('selected', 'selected');
                $("#5_posto").hide();

            }
        }
    }

    function PartnerChanged(partner_id)
    {
        if (partner_id > 0)
        {
            $("#tip_danocna_sema").val(partnerTipDanocnaSema[partner_id]);
            $("#ddv_stapka").val(partnerDDVStapka[partner_id]);
            ChangeDDVStapkaDiv();
        }
    }


    $(document).ready(function () {

<?php if (!isset($dealData)) { ?>
            PartnerChanged(firsPartnerID);
<?php }//if (!isset($dealData)) {  ?>

        ChangeDDVStapkaDiv();

        $("#tip_danocna_sema").change(function () {

            ChangeDDVStapkaDiv();
        });

        $("#partner_id").change(function () {

            PartnerChanged($("#partner_id").val());
        });



        $("#email_content_buy_switch").change(function () {
            if (this.checked) {
                alert("Дали сте сигурни дека сакате да ја вклучите специјалната email содржина!");
            }
        });

    });

</script>

<div style="text-align: center">
    <strong>
        <?php echo "АДМИНИСТРИРАЊЕ НА "; isset($is_clone) AND $is_clone == 1 ? print "КЛОНИРАНА " : print ""; print "ПОНУДА"  ?>
    </strong>
    <?php 
        if (isset($dealData))
            print "<br/>".html::anchor("/admin2/dealoptions/".$dealData[0]->id, "Отвори ги во нов таб Опциите на понудата", array('target' => '_blank')); 
    ?>
</div>

<?php if (!isset($dealData)) { ?>
    <form method="post" action="" id="deal_save_form"  class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" name="autosave" value="false">

        <div class="container_12 homepage-billboard-dhtml">
            <div class="grid_12 alpha omega">
                <input type="hidden" name="id" value="">
				<input type="hidden" name="partner_notified_for_active_offer" value="0">

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Администратор</div>
                    <div class="grid_8 alpha omega">
                        <?php if (0) { //if($userID > 1)ako ne e Jovica ?>
                            <input type="hidden" name="vraboten_id" value="<?php print $userID; ?>">
                            <?php
                            foreach ($allVraboteni as $vraboten)
                                if ($vraboten->id == $userID)
                                    print $vraboten->name;
                            ?>
                            <?php }else { ?>
                            <select name="vraboten_id">
                                    <?php foreach ($allVraboteni as $vraboten) { ?>
                                    <option value="<?php print $vraboten->id; ?>" <?php if ($vraboten->active == 0) print "disabled"; ?>  >
                                    <?php print $vraboten->name; ?>
                                    </option>
                            <?php } ?>
                            </select>
    <?php } ?>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Тип на понуда</div>
                    <div class="grid_8 alpha omega">
                        <select name="tip">
                            <option value="cela_cena"><?php print "Ваучер"; ?></option>
                            <option value="cena_na_vaucer"><?php print "Купон"; ?></option>
                            <!--
                            <option value="cena_na_proizvod"><?php //print "Наша цена";  ?></option>
                            -->
                        </select>
                    </div>
                </div>  

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Партнер</div>
                    <div class="grid_8 alpha omega">
                        <select class="js-example-basic-single"  name="partner_id" id="partner_id">
                                <?php foreach ($allPartners as $partner) { ?>
                                <option value="<?php print $partner->id; ?>">
                                <?php print $partner->name; ?>
                                </option>
    <?php } ?>
                        </select>
                    </div>
                </div>                

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Тип на даночна шема</div>
                    <div class="grid_8 alpha omega" >
                        <select name="tip_danocna_sema" id="tip_danocna_sema">
                            <option value="ДДВ Обврзник"><?php print "ДДВ Обврзник"; ?></option>
                            <option value="Не ДДВ Обврзник"><?php print "Не ДДВ Обврзник"; ?></option>
                            <option value="Ослободен согласно законот"><?php print "Ослободен согласно законот"; ?></option>
                            <option value="Промет остварен во странство"><?php print "Промет остварен во странство"; ?></option>
                        </select>
                    </div>
                </div>   

                <div class="row clearfix" id="ddv_stapka_div">
                    <div class="grid_4 alpha omega">ДДВ стапка</div>
                    <div class="grid_8 alpha omega">
                        <select name="ddv_stapka" id="ddv_stapka">
                            <option value="5" id="5_posto"><?php print "5%"; ?></option>
                            <option value="18"><?php print "18%"; ?></option>
                        </select>
                    </div>
                </div>  

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Категорија</div>
                    <div class="grid_8 alpha omega">
                        <select name="category_id">
                                <?php foreach ($categoriesData as $category) { ?>
                                <option value="<?php print $category->id; ?>">
                                <?php print $category->name; ?>
                                </option>
    <?php } ?>
                        </select>
                    </div>
                </div>   
                
                  <div class="row clearfix">
                    <div class="grid_4 alpha omega">Поткатегорија</div>
                    <div class="grid_8 alpha omega">
                        <select name="subcategory_id">
                            <option value="0" >Избери Поткатегорија</option>
                          <?php foreach ($SubcategoriesData as $subcategory) { ?>
                            <option value="<?php print $subcategory->id; ?>">
                                  <?php print $subcategory->name; ?>
                            </option>
                          <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row homepage-billboard">
                    <div class="admin_error_msg" style="text-align: center;"> 
                        <strong>Овие полиња се однесуваат само за понуди кои се производ</strong>
                    </div>
                    <div class="row clearfix clear_margin_left_right">    
                        <div class="grid_4 alpha omega">Понудата е производ</div>
                        <div class="grid_8 alpha omega">
                            <input type="checkbox" name="proizvod_flag" value="1" />
                        </div>
                    </div>

                    <div class="row clearfix clear_margin_left_right"> 
                        <div class="grid_4 alpha omega">Опции за преземање на производот</div>
                        <div class="grid_8 alpha omega">
                            <select name="plakjanje_pri_prezemanje_nacin_podiganje" id="plakjanje_pri_prezemanje_nacin_podiganje">
                                <option value="0" selected="">Подигање од kupinapopust или Достава до дома</option>
                                <option value="1">Подигање од kupinapopust</option>
                                <option value="2">Достава до дома</option>
                            </select>
                        </div>
                    </div>

                    <div class="row clearfix clear_margin_left_right"> 
                        <div class="grid_4 alpha omega">Можност за плаќање при преземање <br/>на производот - ОТКУП</div>
                        <div class="grid_8 alpha omega">
                            <input type="checkbox" name="plakjanje_pri_prezemanje" value="1" />
                        </div>
                    </div>

                    <div class="row clearfix clear_margin_left_right">    
                        <div class="grid_4 alpha omega">Бесплатна достава - платежна картичка</div>
                        <div class="grid_8 alpha omega">
                            <input type="checkbox" name="proizvod_besplatna_dostava" value="1" />
                        </div>
                    </div>

                    <div class="row clearfix clear_margin_left_right">    
                        <div class="grid_4 alpha omega">Бесплатна достава - откуп</div>
                        <div class="grid_8 alpha omega">
                            <input type="checkbox" name="proizvod_besplatna_dostava_otkup" value="1" />
                        </div>
                    </div>
                </div>
                    
                <div class="row clearfix" style="padding: 20px;">
					<strong>Наслов</strong>
					<textarea class="title_mk" name="title_mk"></textarea>
                </div>
				
                 <div class="row clearfix" style="padding: 20px;">
					<strong>Наслов (за подарок)</strong>
					<textarea class="title_mk_clean" name="title_mk_clean"></textarea>
                </div>

						
                 <div class="row clearfix" style="padding: 20px;">
					<strong>Добивате (опис за сите опции)</strong>
					<textarea class="content_short_mk" name="content_short_mk"></textarea>
                </div>
				


                <div class="row clearfix">
                    <!--
    <div class="grid_4 alpha omega">Активна</div>
    <div class="grid_8 alpha omega"><input type="checkbox" name="visible" value="1" /></div>
                    -->
                    <div class="grid_4 alpha omega">Статус на понудата</div>
                    <div class="grid_8 alpha omega">
                        <select name="deal_status">
							<option value="demo" ><?php print "Демо понуда"; ?></option>
                            <option value="aktivna" ><?php print "Активна понуда"; ?></option>
                        </select>
                        <input type="hidden" name="old_deal_status" value="demo">
                    </div>

                    <div class="grid_4 alpha omega">Заклучена понуда</div>
                    <div class="grid_8 alpha omega"><input type="checkbox" name="locked" value="1" /></div>
                </div>

                <div class="row clearfix">
                    <!--
                    <div class="grid_4 alpha omega">Демо понуда</div>
    <div class="grid_8 alpha omega"><input type="checkbox" name="demo" value="1" /></div>
                    -->
                    <div class="form-group" style="padding-top: 50px">
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">Минимален број на ваучери</div>
                        <div class="col-md-8"><input type="text" name="min_ammount" class="form-control" value="" /></div>
                    </div>

                    <!--    OVA E KOD ZA POLETO 'Макс. број на ваучери на располагање' KOE VAZESE ZA PONUDATA PRED VOVEDUVANJETO NA OPCIITE 
                    <div class="grid_4 alpha omega">Макс. број на ваучери на располагање</div>
                    <div class="grid_8 alpha omega"><input type="text" name="max_ammount" value="" /></div>
                    -->

                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">Макс. број на ваучери по купувач</div>
                        <div class="col-md-8"><input type="text" name="max_per_person" class="form-control" value="" /></div>
                    </div>
                </div>    

                <div class="row clearfix">
                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">Почеток на попустот</div>
                        <div class="col-md-8 dateContainer">
                            <div class='input-group date' id='datetimepicker_start'>
                                <input type='text' class="form-control" name="start_time" value="" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>

                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('#datetimepicker_start').datetimepicker({
                            format: 'YYYY-MM-DD HH:mm:ss',
                            locale: 'mk'
                        });

                        $('#datetimepicker_start').on("dp.change dp.show", function(ev) {
                                activateAutoSaveFromDHTML();
                                $('#deal_save_form').bootstrapValidator('revalidateField', 'start_time');
                           });
                    });
                </script>



                <div class="row clearfix">
                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">Крај на попустот</div>
                        <div class="col-md-8 dateContainer">
                            <div class='input-group date' id='datetimepicker_end'>
                                <input type='text' class="form-control" name="end_time" value="" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>

                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('#datetimepicker_end').datetimepicker({
                            format: 'YYYY-MM-DD HH:mm:ss',
                            locale: 'mk'
                        });

                        $('#datetimepicker_end').on("dp.change dp.show", function(ev) {
                                activateAutoSaveFromDHTML();
                                $('#deal_save_form').bootstrapValidator('revalidateField', 'end_time');
                           });
                    });
                </script>
					
					
					
				<div class="row clearfix">	
                    <div class="grid_4 alpha omega">Прати email на партнерот<br/>на крај од попустот</div>
                    <div class="grid_8 alpha omega"><input type="checkbox" name="prati_mail_do_partner" value="1" /></div>
                </div>


                <div class="row clearfix">

                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">Платена реклама</div>
                        <div class="col-md-8"></div>
                    </div>

                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">- Вредност</div>
                        <div class="col-md-8"><input type="text" name="platena_reklama" class="form-control" value="" /></div>
                    </div>
                     <div class="form-group" >    
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">- Датум</div>
                        <div class="col-md-8">
                            <span class='input-group date' id='datetimepicker_pla_rek_date'>
                                <input type='text' class="form-control" name="platena_reklama_date" value="" id="dp_pla_rek_date" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </span>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker_pla_rek_date').datetimepicker({
                                        format: 'YYYY-MM-DD HH:mm:ss',
                                        locale: 'mk'
                                    })
                                    .on("dp.change", function(ev) {
                                            activateAutoSaveFromDHTML();
                                       });
                                });
                            </script>
                        </div>
                    </div>

					
                </div>




                <div class="row clearfix">
                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">
                            Главна слика <br/>(PNG со димензии 800px X 300px) <br/>
  
                        </div>
                        <div class="col-md-8 ">
                            <input type="file" class="form-control uploadImages" name="main_picture" />
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">
                            Странична слика <br/>(PNG со димензии 800px X 600px) <br/>
  
                        </div>
                        <div class="col-md-8 ">
                            <input type="file" class="form-control uploadImages" name="side_picture" />
                        </div>
                    </div>
                </div>



                <!-- Additional images -->
                <?php for ($br_slika = 1; $br_slika <= 5; $br_slika++) : 
                            $additional_img_column_name = 'additional_img_'.$br_slika;
                ?>

                   <div class="row clearfix">
                        <div class="form-group" >
                            <div class="grid_4 alpha omega" style="padding-left: 35px;">
                                 Дополнителна Слика <?php echo $br_slika; ?> <br/>(PNG со димензии 800px X 600px) <br/>

                            </div>
                            <div class="col-md-8 ">
                                <input type="file" class="form-control uploadImages" name="<?php echo 'picture_' . $br_slika; ?>" />
                            </div>
                        </div>
                    </div>

                <?php endfor; ?>
				



                
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Видео</div>
                    <div class="grid_8 alpha omega">
                        <input type="file" name="ponuda_video">
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Вклучи специјална email содржина</div>
                    <div class="grid_8 alpha omega"><input type="checkbox"  id="email_content_buy_switch" name="email_content_buy_switch" value="1" /></div>
                    <div class="grid_4 alpha omega">&nbsp</div>
                    <div class="grid_8 alpha omega">&nbsp</div>
                    <div style="padding: 20px;">
						<strong>
							Специјална email содржина<br>(Забелешка: Доколку внесете содржина во ова поле, опцијата "Купи како подарок" за оваа понуда ќе биде исклучена.)
						</strong>
						<textarea class="email_content_buy" name="email_content_buy"></textarea>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Тагови</div>
                    <div class="grid_8 alpha omega">
                        <input type="text" name='tags-outside' class='tagify--outside' value='' placeholder='Внеси тагови!'>
                    </div>

                </div>


                <div class="row clearfix">
                    <input type="submit" value="Зачувај и затвори" name="submit_btn">
                    &nbsp;
                    <input type="submit" value="Зачувај и премини на опции" name="submit_btn_opt">
                    &nbsp;
                    <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/admin';
                                return false;">
                </div>
                <br><br><br><br><br><br><br><br><br><br>
            </div>
        </div>
    </form>
<?php } 
      else//isset($dealData)
      { 
?>

    <form method="post" action="" id="deal_save_form" class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" name="autosave" value="false">


        <div class="container_12 homepage-billboard-dhtml">
            <div class="grid_12 alpha omega">
                <input type="hidden" name="id" value="<?php print $dealData[0]->id ?>">
				<input type="hidden" name="partner_notified_for_active_offer" value="<?php print $dealData[0]->partner_notified_for_active_offer ?>">

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Администратор</div>
                    <div class="grid_8 alpha omega">
                        <?php if (0) { //if($userID > 1)ako ne e Jovica  ?>
                            <input type="hidden" name="vraboten_id" value="<?php print $userID; ?>">
                            <?php
                            foreach ($allVraboteni as $vraboten)
                                if ($vraboten->id == $userID)
                                    print $vraboten->name;
                            ?>
                            <?php }else { ?>
                            <select name="vraboten_id">

                                    <?php foreach ($allVraboteni as $vraboten) { ?>
                                    <option <?php if ($vraboten->id == $dealData[0]->vraboten_id) echo 'selected="selected"'; ?> value="<?php print $vraboten->id; ?>" <?php if ($vraboten->active == 0) print "disabled"; ?>  >
                                    <?php print $vraboten->name; ?>
                                    </option>
                            <?php } ?>
                            </select>
    <?php } ?>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Тип на понуда</div>
                    <div class="grid_8 alpha omega">
                        <select name="tip">
                            <option value="cela_cena" <?php if ($dealData[0]->tip == "cela_cena") echo 'selected="selected"'; ?> ><?php print "Ваучер"; ?></option>
                            <option value="cena_na_vaucer" <?php if ($dealData[0]->tip == "cena_na_vaucer") echo 'selected="selected"'; ?> ><?php print "Купон"; ?></option>
                            <!--
                            <option value="cena_na_proizvod" <?php //if($dealData[0]->tip=="cena_na_proizvod") echo 'selected="selected"';  ?> ><?php //print "Наша цена";  ?></option>
                            -->
                        </select>
                    </div>
                </div>  

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Партнер</div>
                    <div class="grid_8 alpha omega">
                        <select class="js-example-basic-single"  name="partner_id" id="partner_id">

                                <?php foreach ($allPartners as $partner) { ?>
                                <option <?php if ($partner->id == $dealData[0]->partner_id) echo 'selected="selected"'; ?> value="<?php print $partner->id; ?>">
                                <?php print $partner->name; ?>
                                </option>
    <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Тип на даночна шема</div>
                    <div class="grid_8 alpha omega">
                        <select name="tip_danocna_sema"  id="tip_danocna_sema">
                            <option value="ДДВ Обврзник" <?php if ($dealData[0]->tip_danocna_sema == "ДДВ Обврзник") echo 'selected="selected"'; ?> ><?php print "ДДВ Обврзник"; ?></option>
                            <option value="Не ДДВ Обврзник" <?php if ($dealData[0]->tip_danocna_sema == "Не ДДВ Обврзник") echo 'selected="selected"'; ?> ><?php print "Не ДДВ Обврзник"; ?></option>
                            <option value="Ослободен согласно законот" <?php if ($dealData[0]->tip_danocna_sema == "Ослободен согласно законот") echo 'selected="selected"'; ?> ><?php print "Ослободен согласно законот"; ?></option>
                            <option value="Промет остварен во странство" <?php if ($dealData[0]->tip_danocna_sema == "Промет остварен во странство") echo 'selected="selected"'; ?> ><?php print "Промет остварен во странство"; ?></option>
                        </select>
                    </div>
                </div> 

                <div class="row clearfix" id="ddv_stapka_div">
                    <div class="grid_4 alpha omega">ДДВ стапка</div>
                    <div class="grid_8 alpha omega">
                        <select name="ddv_stapka" id="ddv_stapka">
                            <option value="5" <?php if ($dealData[0]->ddv_stapka == "5") echo 'selected="selected"'; ?>  id="5_posto"><?php print "5%"; ?></option>
                            <option value="18" <?php if ($dealData[0]->ddv_stapka == "18") echo 'selected="selected"'; ?> ><?php print "18%"; ?></option>
                        </select>
                    </div>
                </div> 

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Категорија</div>
                    <div class="grid_8 alpha omega">
                        <select name="category_id">

                                <?php foreach ($categoriesData as $category) { ?>
                                <option <?php if ($category->id == $dealData[0]->category_id) echo 'selected="selected"'; ?> value="<?php print $category->id; ?>">
                                <?php print $category->name; ?>
                                </option>
    <?php } ?>
                        </select>
                    </div>
                </div>
                
               <div class="row clearfix">
                    <div class="grid_4 alpha omega">Поткатегорија</div>
                    <div class="grid_8 alpha omega">
                        <select name="subcategory_id">
                            <option value="0" >Избери Поткатегорија</option>
                          <?php foreach ($SubcategoriesData as $subcategory) { ?>
                            <option <?php if($subcategory->id==$dealData[0]->subcategory_id) echo 'selected="selected"'; ?> value="<?php print $subcategory->id; ?>">
                                  <?php print $subcategory->name; ?>
                            </option>
                          <?php } ?>
                        </select>
                    </div>
                </div>  

                <div class="row homepage-billboard">
                    <div class="admin_error_msg" style="text-align: center;"> 
                        <strong>Овие полиња се однесуваат само за понуди кои се производ</strong>
                    </div>
                    <div class="row clearfix clear_margin_left_right">    
                        <div class="grid_4 alpha omega">Понудата е производ</div>
                        <div class="grid_8 alpha omega">
                            <input type="checkbox" name="proizvod_flag" <?php print ($dealData[0]->proizvod_flag) ? "checked='checked'" : ""  ?> value="1" />
                        </div>
                    </div>

                    <div class="row clearfix clear_margin_left_right"> 
                        <div class="grid_4 alpha omega">Опции за преземање на производот</div>
                        <div class="grid_8 alpha omega">
                            <select name="plakjanje_pri_prezemanje_nacin_podiganje" id="plakjanje_pri_prezemanje_nacin_podiganje">
                                <option value="0" <?php if ($dealData[0]->plakjanje_pri_prezemanje_nacin_podiganje == 0) echo 'selected="selected"'; ?> >Подигање од kupinapopust или Достава до дома</option>
                                <option value="1" <?php if ($dealData[0]->plakjanje_pri_prezemanje_nacin_podiganje == 1) echo 'selected="selected"'; ?> >Подигање од kupinapopust</option>
                                <option value="2" <?php if ($dealData[0]->plakjanje_pri_prezemanje_nacin_podiganje == 2) echo 'selected="selected"'; ?> >Достава до дома</option>
                            </select>
                        </div>
                    </div>

                    <div class="row clearfix clear_margin_left_right"> 
                        <div class="grid_4 alpha omega">Можност за плаќање при преземање <br/>на производот - ОТКУП</div>
                        <div class="grid_8 alpha omega">
                            <input type="checkbox" name="plakjanje_pri_prezemanje" <?php print ($dealData[0]->plakjanje_pri_prezemanje) ? "checked='checked'" : ""  ?> value="1" />
                        </div>
                    </div>

                    <div class="row clearfix clear_margin_left_right">    
                        <div class="grid_4 alpha omega">Бесплатна достава - платежна картичка</div>
                        <div class="grid_8 alpha omega">
                            <input type="checkbox" name="proizvod_besplatna_dostava" <?php print ($dealData[0]->proizvod_besplatna_dostava) ? "checked='checked'" : ""  ?> value="1" />
                        </div>
                    </div>

                    <div class="row clearfix clear_margin_left_right">    
                        <div class="grid_4 alpha omega">Бесплатна достава - откуп</div>
                        <div class="grid_8 alpha omega">
                            <input type="checkbox" name="proizvod_besplatna_dostava_otkup" <?php print ($dealData[0]->proizvod_besplatna_dostava_otkup) ? "checked='checked'" : ""  ?> value="1" />
                        </div>
                    </div>
                </div>

                <div class="row clearfix" style="padding: 20px;">
					<strong>Наслов</strong>
					<textarea class="title_mk" name="title_mk"><?php print $dealData[0]->title_mk ?></textarea>
                </div>
				
                <div class="row clearfix" style="padding: 20px;">
					<strong>Наслов (за подарок)</strong>
					<textarea class="title_mk_clean" name="title_mk_clean"><?php print $dealData[0]->title_mk_clean ?></textarea>
                </div>
				
                
                <div class="row clearfix" style="padding: 20px;">
					<strong>Добивате (опис за сите опции)</strong>
					<textarea class="content_short_mk" name="content_short_mk"><?php print $dealData[0]->content_short_mk ?></textarea>
                </div>
				


				

                <div class="row clearfix">
                    <!--
    <div class="grid_4 alpha omega">Активна</div>
    <div class="grid_8 alpha omega"><input type="checkbox" name="visible" <?php print ($dealData[0]->visible) ? "checked='checked'" : ""  ?> value="1" /></div>
                    -->
                    <div class="grid_4 alpha omega">Статус на понудата</div>
                    <div class="grid_8 alpha omega">
                        <select name="deal_status">
                            <option value="aktivna" <?php if ($dealData[0]->visible == 1) echo 'selected="selected"'; ?> ><?php print "Активна понуда"; ?></option>
                            <option value="demo" <?php if ($dealData[0]->demo == 1) echo 'selected="selected"'; ?> ><?php print "Демо понуда"; ?></option>
                        </select>
                        <input type="hidden" name="old_deal_status" value="<?php if ($dealData[0]->visible == 1) echo 'aktivna"';
    else echo 'demo"'; ?>">
                    </div>

                    <div class="grid_4 alpha omega">Заклучена понуда</div>
                    <div class="grid_8 alpha omega"><input type="checkbox" name="locked" <?php print ($dealData[0]->locked) ? "checked='checked'" : ""  ?> value="1" /></div>
                </div>
                
                <div class="row clearfix">
                    <!--
    <div class="grid_4 alpha omega">Демо понуда</div>
    <div class="grid_8 alpha omega"><input type="checkbox" name="demo" <?php print ($dealData[0]->demo) ? "checked='checked'" : ""  ?> value="1" /></div>
                    -->

                    <div class="form-group" style="padding-top: 50px">
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">Минимален број на ваучери</div>
                        <div class="col-md-8"><input type="text" name="min_ammount" class="form-control" value="<?php print $dealData[0]->min_ammount ?>" /></div>
                    </div>

                    <!--    OVA E KOD ZA POLETO 'Макс. број на ваучери на располагање' KOE VAZESE ZA PONUDATA PRED VOVEDUVANJETO NA OPCIITE 
                    <div class="clearfix">
                        <div class="grid_4 alpha omega">Макс. број на ваучери на располагање</div>
                        <div class="grid_8 alpha omega"><input type="text" name="max_ammount" value="<?php //print $dealData[0]->max_ammount ?>" /></div>
                    </div>
                    -->

                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">Макс. број на ваучери по купувач</div>
                        <div class="col-md-8"><input type="text" name="max_per_person" class="form-control" value="<?php print $dealData[0]->max_per_person ?>" /></div>
                    </div>

                </div>

                <div class="row clearfix">
                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">Почеток на попустот</div>
                        <div class="col-md-8 dateContainer">
                            <div class='input-group date' id='datetimepicker_start'>
                                <input type='text' class="form-control" name="start_time" value="<?php if ($dealData[0]->start_time != "0000-00-00 00:00:00" ) print $dealData[0]->start_time ?>" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>

                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('#datetimepicker_start').datetimepicker({
                            format: 'YYYY-MM-DD HH:mm:ss',
                            locale: 'mk'
                        });

                        $('#datetimepicker_start').on("dp.change dp.show", function(ev) {
                                activateAutoSaveFromDHTML();
                                $('#deal_save_form').bootstrapValidator('revalidateField', 'start_time');
                           });
                    });
                </script>

                <div class="row clearfix">
                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">Крај на попустот</div>
                        <div class="col-md-8 dateContainer">
                            <div class='input-group date' id='datetimepicker_end'>
                                <input type='text' class="form-control" name="end_time" value="<?php if ($dealData[0]->end_time != "0000-00-00 00:00:00" ) print $dealData[0]->end_time ?>" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>

                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('#datetimepicker_end').datetimepicker({
                            format: 'YYYY-MM-DD HH:mm:ss',
                            locale: 'mk'
                        });

                        $('#datetimepicker_end').on("dp.change dp.show", function(ev) {
                                activateAutoSaveFromDHTML();
                                $('#deal_save_form').bootstrapValidator('revalidateField', 'end_time');
                           });
                    });
                </script>

                <div class="row clearfix">    
                    <div class="grid_4 alpha omega">Прати email на партнерот<br/>на крај од попустот</div>
                    <div class="grid_8 alpha omega"><input type="checkbox" name="prati_mail_do_partner" <?php print ($dealData[0]->prati_mail_do_partner) ? "checked='checked'" : ""  ?> value="1" /></div>
                </div>

                <div class="row clearfix">
                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">Платена реклама</div>
                        <div class="col-md-8"></div>
                    </div>
                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">- Вредност</div>
                        <div class="col-md-8"><input type="text" name="platena_reklama" class="form-control" value="<?php print $dealData[0]->platena_reklama ?>" /></div>
                    </div>
                     <div class="form-group" >    
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">- Датум</div>
                        <div class="col-md-8">
                            <span class='input-group date' id='datetimepicker_pla_rek_date'>
                                <input type='text' class="form-control" name="platena_reklama_date" value="<?php if ($dealData[0]->platena_reklama_date != "0000-00-00 00:00:00" ) print $dealData[0]->platena_reklama_date ?>"  id="dp_pla_rek_date" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </span>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker_pla_rek_date').datetimepicker({
                                        format: 'YYYY-MM-DD HH:mm:ss',
                                        locale: 'mk'
                                    })
                                    .on("dp.change", function(ev) {
                                            activateAutoSaveFromDHTML();
                                       });
                                });
                            </script>
                        </div>
                    </div>

                </div>





                <div class="row clearfix">
                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">
                            Главна слика <br/>(PNG со димензии 800px X 300px) <br/>
                            <?php if(is_file(DOCROOT . 'pub/deals/'.$dealData[0]->main_img)) { ?> 
                                    <a href ="/pub/deals/<?php echo $dealData[0]->main_img; ?>" target="_blank">(Преглед)</a>
                  
                                    &nbsp;&nbsp;
                  
                                    <?php if(strpos($dealData[0]->main_img, $dealData[0]->id) !== false) { ?>
                                        <a onclick="return confirm('Дали си сигурен дека сакаш да ја избришеш сликата?')" href ="/admin/delete_image?deal_id=<?php echo $dealData[0]->id; ?>&image_name=<?php echo $dealData[0]->main_img; ?>&column_name=main_img" >(Избриши)</a>
                                    <?php }  ?> 
                            <?php }  ?> 
                        </div>
                        <div class="col-md-8 ">
                            <input type="file" class="form-control uploadImages" name="main_picture" />
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="form-group" >
                        <div class="grid_4 alpha omega" style="padding-left: 35px;">
                            Странична слика <br/>(PNG со димензии 800px X 600px) <br/>
                            <?php if(is_file(DOCROOT . 'pub/deals/'.$dealData[0]->side_img)) { ?> 
                                    <a href ="/pub/deals/<?php echo $dealData[0]->side_img; ?>" target="_blank">(Преглед)</a>
                  
                                    &nbsp;&nbsp;
                  
                                    <?php if(strpos($dealData[0]->side_img, $dealData[0]->id) !== false) { ?>
                                        <a onclick="return confirm('Дали си сигурен дека сакаш да ја избришеш сликата?')" href ="/admin/delete_image?deal_id=<?php echo $dealData[0]->id; ?>&image_name=<?php echo $dealData[0]->side_img; ?>&column_name=side_img" >(Избриши)</a>
                                    <?php }  ?> 
                            <?php }  ?> 
                        </div>
                        <div class="col-md-8 ">
                            <input type="file" class="form-control uploadImages" name="side_picture" />
                        </div>
                    </div>
                </div>



				<!-- Additional images -->
				<?php for ($br_slika = 1; $br_slika <= 5; $br_slika++) : 
                            $additional_img_column_name = 'additional_img_'.$br_slika;
                ?>

                   <div class="row clearfix">
                        <div class="form-group" >
                            <div class="grid_4 alpha omega" style="padding-left: 35px;">
                                 Дополнителна Слика <?php echo $br_slika; ?> <br/>(PNG со димензии 800px X 600px) <br/>
                                <?php if(is_file(DOCROOT . 'pub/deals/'.$dealData[0]->$additional_img_column_name)) { ?> 
                                        <a href ="/pub/deals/<?php echo $dealData[0]->$additional_img_column_name; ?>" target="_blank">(Преглед)</a>
                      
                                        &nbsp;&nbsp;
                      
                                        <?php if(strpos($dealData[0]->$additional_img_column_name, $dealData[0]->id) !== false) { ?>
                                            <a onclick="return confirm('Дали си сигурен дека сакаш да ја избришеш сликата?')" href ="/admin/delete_image?deal_id=<?php echo $dealData[0]->id; ?>&image_name=<?php echo $dealData[0]->$additional_img_column_name; ?>&column_name=<?php echo $additional_img_column_name;?>" >(Избриши)</a>
                                        <?php }  ?> 
                                <?php }  ?> 
                            </div>
                            <div class="col-md-8 ">
                                <input type="file" class="form-control uploadImages" name="<?php echo 'picture_' . $br_slika; ?>" />
                            </div>
                        </div>
                    </div>

				<?php endfor; ?>


                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Видео <?php if($dealData[0]->ponuda_video != "") echo html::anchor("/admin/deal_delete_video/".$dealData[0]->id, "(Избриши)", array('onclick' => 'return confirm("Дали си сигурен?")')) ?></div>
                    <div class="grid_8 alpha omega">
                        <input type="file" name="ponuda_video">
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Вклучи специјална email содржина</div>
                    <div class="grid_8 alpha omega"><input type="checkbox" id="email_content_buy_switch" name="email_content_buy_switch" <?php print ($dealData[0]->email_content_buy_switch) ? "checked='checked'" : ""  ?> value="1" /></div>
                    <div class="grid_4 alpha omega">&nbsp</div>
                    <div class="grid_8 alpha omega">&nbsp</div>
                    <div style="padding: 20px;">
						<strong>
							Специјална email содржина<br>(Забелешка: Доколку внесете содржина во ова поле, опцијата "Купи како подарок" за оваа понуда ќе биде исклучена.)
						</strong>
						<textarea class="email_content_buy" name="email_content_buy"><?php print $dealData[0]->email_content_buy ?></textarea>
                    </div>
                </div>
				
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Тагови</div>
                    <div class="grid_8 alpha omega">
                        <input type="text" name='tags-outside' class='tagify--outside' value='<?php echo $dealTagsCommaDelimited;?>' placeholder='Внеси тагови!'>
                    </div>

                </div>

                <div class="row clearfix">
                    <input type="submit" value="Зачувај и затвори" name="submit_btn">
                    &nbsp;
                    <input type="submit" value="Зачувај и премини на опции" name="submit_btn_opt">
                    &nbsp;
                    <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/admin';
                                return false;">
                </div>
                
            </div>
        </div>
    </form>




<?php } 

?>


<script type="text/javascript">
    
    $("#deal_save_form").change(function () {
         
        activateAutoSaveFromDHTML();
            
    });
	
    
    function start_autosave() {

        $('textarea[name="title_mk"]').val($('.title_mk').code());
        $('textarea[name="title_mk_clean"]').val($('.title_mk_clean').code());


        $('textarea[name="content_short_mk"]').val($('.content_short_mk').code());
        

        $('textarea[name="email_content_buy"]').val($('.email_content_buy').code());

//        var DataSer = new FormData('form');
        var DataSer = $("#deal_save_form").serializeArray();
        jQuery.ajax({
            url: '/admin/ajax_autosave',
            data: DataSer,
            enctype: 'multipart/form-data',
//            cache: false,
//            processData: false,
//            contentType: false,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data && data.status == 'success') {
                    $('input[name="id"]').val(data.id);
					$('input[name="partner_notified_for_active_offer"]').val(data.partner_notified_for_active_offer);
                } else {
                    clearInterval(setIntervals);
                }
            }// end successful POST function
        }); // end jQuery ajax call
    }
    
    
     $('select[name="category_id"]').change(function () {
        var id = "";
        $( 'select[name="category_id"] option:selected' ).each(function() {
          id = $( this ).val();
        });
        
        if(id > 0){
            get_subcategories_by_id(id);
        }
        
    });
    
    function get_subcategories_by_id(id){
        
        jQuery.ajax({
            url: '/acategories/prezemi_podkategorii',
            data: { parent_id: id },
            enctype: 'multipart/form-data',
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data && data.status == 'success') {
                    
                    $('select[name="subcategory_id"]').empty();
                    $('select[name="subcategory_id"]').append(data.html);
                    
                } else {
                    alert('Се појави грешка!');
                }
            }// end successful POST function
        }); // end jQuery ajax call
        
    }

</script>