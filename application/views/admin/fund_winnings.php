<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once 'menu.php'; ?>

<?php // var_dump($dataofWfPrizeFundModel); ?>

<h1>Фонд на добивки</h1>
<br/>
Забелешка: Сумата од количините на сите добивки мора да биде целобројно делива со 28.
<br/>
<div class="container_12 homepage-billboard">
    <div class="clearfix" style="margin-bottom: 30px">

        <a href='/fund_winnings/add'>Внеси нова добивка</a>

        <table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
            <!-- HEAD -->
            <tr>
                <td><strong>Опис</strong></td>
                <td><strong>Добивка</strong></td>
                <td ><strong>Количина</strong></td>
                <td ><strong>&nbsp;</strong></td>
            </tr>
            <tr>
                <td  colspan="5"><hr></td>
            </tr>
            <!-- END HEAD -->
            <?php if ($dataofWfPrizeFundModel) { ?>
                <?php foreach ($dataofWfPrizeFundModel as $dataofWfPrize) { ?>
                    <tr>
                        <td ><?php echo $dataofWfPrize->description ?></td>
                        <td ><?php echo $dataofWfPrize->prize ?></td>
                        <td ><?php echo $dataofWfPrize->quantity ?></td>
                        <td>
                            <?php
                            print html::anchor("/fund_winnings/edit?id=$dataofWfPrize->id", "Промени");
                            print "&nbsp;&nbsp;";
                            print html::anchor("/fund_winnings/remove?id=$dataofWfPrize->id", "Избриши", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да го избришеш овој податок?")'));
                            ?>
                        </td>    
                    </tr>
                    <tr>
                        <td  colspan="5"><hr></td>
                    </tr>
                <?php } ?>
            <?php } ?>

        </table>

    </div>
</div>  