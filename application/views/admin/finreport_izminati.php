<?php defined('SYSPATH') OR die('No direct access allowed.') ; 

//zemi go logiraniot user
$logiran_user_id = $this->session->get("user_id");
?>

<?php require_once 'menu.php' ; ?>


<div class="container_12 homepage-billboard">
  <div class="clearfix" style="margin-bottom: 30px">

		<form method="get" action="" id="finreport_frm">
				Име на понуда: <input type="text" name="search_deal_name" id="search_deal_name" value="<?php echo $search_deal_name?>">
				<input type="submit" value="Филтрирај">
		</form>
		
		
		
		<br/>
		<table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
			<!-- HEAD -->
			<tr>
				<td width="40%"><strong>Понуда</strong></td>
				<td><strong>Цена<br/>на понудата</strong></td>
				<td><strong>Продадени<br/>ваучери</strong></td>
				<td><strong>Провизија</strong></td>
				<td><strong>Трошоци</strong></td>
				<td><strong>Платена реклама</strong></td>
			</tr>
			<tr>
				<td  colspan="6"><hr></td>
			</tr>
			<!-- END HEAD -->
		<?php
		//sumarni vrednosti
		$vk_zarabotka = 0;
		$prodadeni_vauceri = 0;
		$ddv_po_ponuda = 0;
		$ponudi_zapocnati_vo_mesecot = 0;
		
		$vk_provizii_site_ponudi = 0;
		$vk_platena_reklama_site_ponudi = 0;
		$vk_trosoci_site_ponudi = 0;
		$ddv_vkupno_site_ponuda = 0;
		
		$ddv_vkupno_18 = 0;
		$ddv_vkupno_5 = 0;
		$ddv_vkupno_oslobodeni = 0;
		$ddv_vkupno_stranstvo = 0;
		
		$promet_18 = 0;
		$promet_5 = 0;
		$promet_osloboden = 0;
		$promet_stranstvo = 0;
		
		if ($finreportdata) {
			foreach ($finreportdata as $finreportitem) {

				//povikaj ja funcijata za presmetka na ddv-to
				$ddv_po_ponuda = round(calculate::ddv($finreportitem->price_discount, $finreportitem->price_voucher, $finreportitem->tip_danocna_sema, $finreportitem->ddv_stapka, $finreportitem->tip_ponuda, $finreportitem->payed_other_than_cash, $finreportitem->platena_reklama), 2);
								
				//povikaj ja funcijata za presmetka na sumarnite vrednosti za promet sto gi bara smetkovodstvo
				//calculate::promet($finreportitem->price_discount, $finreportitem->price_voucher, $finreportitem->tip_danocna_sema, $finreportitem->ddv_stapka, $finreportitem->tip_ponuda, $finreportitem->prodadeni, $promet_18, $promet_5, $promet_osloboden, $promet_stranstvo);
		?>
			<tr>
				<td width="40%" >
					<?php print strip_tags( ($finreportitem->options_cnt > 1 ? $finreportitem->title_mk_clean_deal." - ": "").$finreportitem->title_mk_clean ); ?>
					<br/><br/>
					<strong>
						Понуда ID: <?php echo html::anchor("/admin/autosave_deal/$finreportitem->deal_id", $finreportitem->deal_id, array("target" => "_blank")); ?><br/>
						Опција ID: <?php echo html::anchor("/admin2/uredi_dealoption/$finreportitem->deal_option_id/$finreportitem->deal_id", $finreportitem->deal_option_id, array("target" => "_blank")); ?><br/>
						
						Вработен админ.: <?php echo $finreportitem->vraboten_name; ?><br/>
						Почеток на попустот: <?php echo date("d/m/Y", strtotime($finreportitem->start_time)); ?><br/>
						Крај на попустот: <?php echo date("d/m/Y", strtotime($finreportitem->end_time)); ?><br/>
						Тип на понуда: <?php if($finreportitem->tip_ponuda == "cela_cena") print "Ваучер"; elseif($finreportitem->tip_ponuda == "cena_na_vaucer") print "Купон"; else  print "Наша цена";?><br>
						Тип на даночна шема: <?php echo $finreportitem->tip_danocna_sema; ?><br/>
						ДДВ стапка: <?php echo $finreportitem->ddv_stapka."%"; ?><br/>
						Категорија: <?php echo $finreportitem->category_name; ?><br/>
						Партнер: <?php echo $finreportitem->partner_name; ?>
					</strong>
				</td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp; <?php if ($finreportitem->tip_ponuda == 'cena_na_vaucer') print $finreportitem->price_voucher; else print $finreportitem->price_discount; ?></td>
				<td><?php echo $finreportitem->prodadeni; ?><br/>
						<strong>Bank/Card:</strong> <?php echo $finreportitem->payed_other_than_cash; ?><br/>
						<strong>Cash:</strong> <?php echo $finreportitem->payed_with_cash; ?><br/><br/>
						
<!-- 					<strong>Web Site:</strong> <?php //echo $finreportitem->bought_via_website; ?><br/>
						<strong>Android:</strong> <?php //echo $finreportitem->bought_via_android; ?><br/>
						<strong>iOS:</strong> <?php //echo $finreportitem->bought_via_ios; ?> -->
				</td>
				<td>
					<strong>Поед.:</strong> <?php echo $finreportitem->price_voucher; ?> ден.<br/>
					<strong>Вк.:</strong> <?php echo $finreportitem->vk_zarabotka; ?> ден.
				</td>
				<td>
					<strong>Останати:</strong> <?php echo $finreportitem->mesecni_trosoci; ?> ден.<br/>
					<strong>ДДВ:</strong> <?php echo $ddv_po_ponuda; ?> ден.<br/>
					<strong>Вк.:</strong> <?php echo ($finreportitem->mesecni_trosoci + $ddv_po_ponuda); ?> ден.
				</td>
				<td style="text-align: center;"><?php echo $finreportitem->platena_reklama; ?> ден.</td>
			</tr>
			<tr>
				<td  colspan="6"><hr></td>
			</tr>
		<?php
				//sumarni vrednosti
				//$vk_zarabotka += $finreportitem->vk_zarabotka + ((date("Y-n", strtotime($finreportitem->start_time)) == "$finreport_year-$finreport_month") ? $finreportitem->platena_reklama : 0) - (((date("Y-n", strtotime($finreportitem->start_time)) == "$finreport_year-$finreport_month") ? $finreportitem->mesecni_trosoci : 0) + $ddv_po_ponuda);
				//$prodadeni_vauceri += $finreportitem->prodadeni;
				
				//$vk_provizii_site_ponudi += $finreportitem->vk_zarabotka;
				//$vk_platena_reklama_site_ponudi += ((date("Y-n", strtotime($finreportitem->start_time)) == "$finreport_year-$finreport_month") ? $finreportitem->platena_reklama : 0);
				//$vk_trosoci_site_ponudi += ((date("Y-n", strtotime($finreportitem->start_time)) == "$finreport_year-$finreport_month") ? $finreportitem->mesecni_trosoci : 0);
				//$ddv_vkupno_site_ponuda += $ddv_po_ponuda;
				
				//sumarni ddv razbieni po stavki
				switch ($finreportitem->tip_danocna_sema) {
					case "ДДВ Обврзник":
						if($finreportitem->ddv_stapka == 18)
							$ddv_vkupno_18 += $ddv_po_ponuda;
						else
							$ddv_vkupno_5 += $ddv_po_ponuda;
						break;					
					case "Не ДДВ Обврзник":
						if($finreportitem->ddv_stapka == 18)
							$ddv_vkupno_18 += $ddv_po_ponuda;
						else
							$ddv_vkupno_5 += $ddv_po_ponuda;
						break;
					case "Ослободен согласно законот":
						$ddv_vkupno_oslobodeni += $ddv_po_ponuda;
						break;
					case "Промет остварен во странство":
						$ddv_vkupno_stranstvo += $ddv_po_ponuda;
						break;
				}
				
				//presmetka na ponudi koi se zapocnati vo mesecot
				//if(date("Y-n", strtotime($finreportitem->start_time)) == "$finreport_year-$finreport_month")
					//$ponudi_zapocnati_vo_mesecot++;
				
			}
		}//if ($finreportdata) {
		?>		
		</table>
	


  </div>
</div>