<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once 'menu.php'; ?>

<h1><?php echo (!isset($voucherData) ? "Внеси" : "Уреди"); ?> датум</h1>

<br/>
<div><?php  print $dealData->title_mk  ; ?></div>
<br/>
		

<?php if (isset($voucherData)) { 
	$customerModel = new Customer_Model() ;
?>

    <form method="post" action="" id="deal_save_form">
        <div class="container_12 homepage-billboard-dhtml">
            <div class="grid_12 alpha omega">
				<input type="hidden" name="id" value="<?php print $voucherData->id ?>">
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Е-mail адреса</div>
                    <div class="grid_8 alpha omega"><?php print $customerModel->getCustomerMail($voucherData->customer_id);  ?></div>
                </div>
				
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Код</div>
                    <div class="grid_8 alpha omega"><?php print $voucherData->code;  ?></div>
                </div>
				
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Тип</div>
                    <div class="grid_8 alpha omega"><?php print "Резервиран со ".$voucherData->type;  ?></div>
                </div>

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Активен</div>
                    <div class="grid_8 alpha omega"><?php print ($voucherData->confirmed && $voucherData->activated) ? "Да" : "Не";  ?></div>
                </div>				
				
				
				
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Креиран на</div>
                    <div class="grid_8 alpha omega" style="width: 300px;">

                        <div class='input-group date' id='datetimepicker_time'>
                            <input type='text' class="form-control" name="time"  value="<?php print $voucherData->time ?>"  id="datepicker_time"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        <script type="text/javascript">
                            $(function () {
                                $('#datetimepicker_time').datetimepicker({
                                    format: 'YYYY-MM-DD HH:mm:ss',
                                    locale: 'mk'
                                })
                            });
                        </script>

                    </div>
                </div>
                <div class="row clearfix">
                    <input type="submit" value="Зачувај" value="save_btn">
                    <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/admin/dealdetails/<?php echo $dealData->deal_id ?>';return false;">
                </div>
                
            </div>
        </div>
    </form>

<?php } ?>
