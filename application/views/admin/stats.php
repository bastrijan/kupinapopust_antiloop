<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<style type="text/css">
    .inlineedit {
        width: 50px
    }
</style>
<script type="text/javascript" charset="utf-8">

    $(function() {

        $(".dblclick").editable("/ajax/adminpoints", { 
            indicator : "<img src='/pub/img/layout/ajax-loader.gif'>",
            tooltip   : "двоен клик за промена...",
            event     : "dblclick",
            style  : "inherit",
            cssclass : 'inlineedit',
            submit : "сними",
            cancel : 'откажи',
            width: 50,
			
			onsubmit: function(settings, td) {
			  var input = $(td).find('input');
				var original = input.val();
				if (original < 0) {
					alert("Не можете да внесете негативна вредност");
					return false;
				} 
			}
        });


        <?php if (isset($customerActivated) and $customerActivated == 1) { ?>
            $(document).ready(function () {

                $().toastmessage('showToast', {
                    text     : "Успешно го активиравте корисникот.",
                    sticky   : false,
                    position : 'middle-center',
                    type     : 'success'
                });
            }) 
        <?php } ?>    


    });
</script>

<?php require_once 'menu.php'; ?>

<h1>Корисници</h1>

<div class="container_12 homepage-billboard">
    <div class="clearfix" style="margin-bottom: 30px">

        <form method="get" action="" id="finreport_frm">
                E-mail адреса: <input type="text" name="search_email" id="search_email" value="<?php echo $search_email?>">
                <input type="submit" value="Филтрирај">
        </form>

        <div style="margin-top: 20px">
             <div style="border: black 1px dotted" class="clearfix">
                    <div class="grid_3 alpha">
                        Email адреса
                    </div>
                    <div class="grid_1 alpha">
                        Ваучери
                    </div>
                    <div class="grid_2 alpha">
                        Последен логин
                    </div>
                    <div class="grid_2 alpha">
                        Последно купување
                    </div>
                    <div class="grid_3 omega">
                        Поени
                    </div>
                    <div class="grid_1 omega">
                        Статус
                    </div>
                </div>
            <?php foreach ($grid as $email => $vouchers) { ?>
                <div style="border: black 1px dotted" class="clearfix">
                    <div class="grid_3 alpha">
                        <?php print $email; ?>
                    </div>
                    <div class="grid_1 alpha">
                        <?php print $vouchers; ?>
                    </div>

                    <div class="grid_2 alpha">
                        <?php $last_login = $last_login_arr[$email]; if(!empty($last_login)) print date("d/m/Y H:i", strtotime($last_login)); else print "&nbsp;";  ?>
                    </div>
                    <div class="grid_2 alpha">
                        <?php $last_purchase = $last_purchase_arr[$email]; if(!empty($last_purchase)) print date("d/m/Y H:i", strtotime($last_purchase)); else print "&nbsp;"; ?>
                    </div>

                    <div class="grid_3 omega">
                        <span id="<?php print $email; ?>" class="dblclick" style="width: 100px"><?php $poeni = $points[$email]; print $poeni; ?></span>
                    </div>

                    <div class="grid_1 omega">
                       <?php
                            if($active[$email] == 1)
                                print "Активен"; 
                            else
                                print html::anchor("/admin/reactivate_customer/".$customerIDs[$email]."/".$email."/".$search_email, "Реактивирај", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да го реактивираш корисникот?")'));
                                
                       ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
