<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once 'menu.php' ; ?>

<script type="text/javascript">

	$(document).ready(function() {
		$("#finreport_month").change(function () {
			$("#finreport_frm").submit();
		})

		$("#finreport_year").change(function () {
			$("#finreport_frm").submit();
		})
	});
</script>

		<h1>Месечни (општи) трошоци</h1>
		<br/>
<div class="container_12 homepage-billboard">
    <div class="clearfix" style="margin-bottom: 30px">

		<form method="get" action="" id="finreport_frm">
			<?php print "Месец: " ?> 
			<select id="finreport_month" name="finreport_month">
				<?php for($i=1; $i<=12; $i++) { ?>
				<option value="<?php echo $i?>" <?php print ($finreport_month == $i) ? 'selected="true"' : ""  ?>><?php echo $i?></option>
				<?php }//for($i=1; $i<=12; $i++)  ?>
				
			</select>
			&nbsp;&nbsp;
			<?php print "Година: " ?> 
			<select id="finreport_year" name="finreport_year">
				<?php for($i=2011; $i<=(date("Y")+ 1); $i++) { ?>
					<option value="<?php echo $i?>" <?php print ($finreport_year == $i) ? 'selected="true"' : ""  ?>><?php echo $i?></option>
				<?php }//for($i=1; $i<=12; $i++)  ?>
			</select>
		<?php
			print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='/admin/uredimesecentrosok?finreport_month=".$finreport_month."&finreport_year=".$finreport_year."'>Внеси нов месечен трошок</a>";
		?>
		</form>

		<br/>
		<table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
			<!-- HEAD -->
			<tr>
				<td width="50%"><strong>Трошок</strong></td>
				<td ><strong>Сума</strong></td>
				<td ><strong>&nbsp;</td>
			</tr>
			<tr>
				<td  colspan="3"><hr></td>
			</tr>
			<!-- END HEAD -->
		<?php
		if ($mesecniTrosociData) {
			foreach ($mesecniTrosociData as $mesecniTrosoci) {
		?>
			<tr>
				<td width="50%" ><?php echo $mesecniTrosoci->description; ?></td>
				<td ><?php echo $mesecniTrosoci->price. " ден."; ?></td>
				<td >
					<?php
						print html::anchor("/admin/uredimesecentrosok?finreport_month=".$finreport_month."&finreport_year=".$finreport_year."&id=$mesecniTrosoci->id", "Промени");
						print "&nbsp;&nbsp;";
						print html::anchor("/admin/izbrisimesecentrosok?finreport_month=".$finreport_month."&finreport_year=".$finreport_year."&id=$mesecniTrosoci->id", "Избриши", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да го избришеш овој месечен трошок?")'));
					?>
				</td>

			</tr>
			<tr>
				<td  colspan="3"><hr></td>
			</tr>
		<?php
			}
		}//if ($finreportdata) {
		?>		
		</table>
		
    </div>
</div>