<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php
$userID = $this->session->get("user_id");
$adminName = $this->session->get("admin_name");
?>
<style>
    .admin_menu li {
        display: inline;
        padding: 0px 0px !important;
    }
</style>
<div class="container_12 homepage-billboard">
    <div style="width: 940px;">
        <ul class="admin_menu">
            <li style="float: left;">Логиран администратор: <?php print "<strong>$adminName</strong>"; ?></li>
            <li style="float: right;">
                <a  href="/admin/changepassword">Промени лозинка</a>
                &nbsp;|&nbsp;
                <a  href="/admin/logout/<?php print base64_encode("/admin"); ?>">Logout</a>
            </li>

        </ul>
    </div>


    <div class="grid_10 alpha omega" style="width: 950px">
        <ul class="admin_menu">
            <li><?php print html::anchor("/admin", "Понуди"); ?></li>
            <li>|&nbsp;<?php print html::anchor("/admin/autosave_deal", "Креирај Понуда"); ?></li>
            <li>|&nbsp;<?php print html::anchor("/admin2/autosave_general_deal", "Креирај Општа Понуда"); ?></li>

            <li>|&nbsp;<?php print html::anchor("/apartners/index", "Партнери"); ?></li>

            <li>|&nbsp;<?php print html::anchor("/admin2/static_pages", "Содржини"); ?></li>

            <li>|&nbsp;<?php print html::anchor("/acategories", "Категории"); ?></li>
            <li>|&nbsp;<?php print html::anchor("/admin/finreport", "Фин. извештај"); ?></li>
            <li>|&nbsp;<?php print html::anchor("/admin/main_offers_scheduler", "Гл. понуди-календар"); ?></li>
        </ul>

        <?php if (in_array($userID, array(1, 5))) { //ako se samo 1 (Jovica) i 5(Martina) ?>
            <ul class="admin_menu">
                <li><?php print html::anchor("/admin/stats", "Корисници"); ?></li>
				<li>|&nbsp;<?php print html::anchor("/admin/finreport_izminati", "Фин. извештај (изминати понуди)"); ?></li>
				
				<?php if (in_array($userID, array(1))) { //ako samo e superadmin prikazi gi linkovite ?>
					
					<li>|&nbsp;<?php print html::anchor("/admin2/download_partner", "Partner Export"); ?></li>
					
					<li>|&nbsp;<?php print html::anchor("/fund_winnings", "WоF - Фонд на добивки"); ?></li>
					<li>|&nbsp;<?php print html::anchor("/admin2/DailyReportWheel", "WоF - Извештај"); ?></li>
					
				<?php } ?>
                <!--
                <li>|&nbsp;<?php print html::anchor("/smetkovodstvo_izvestaj", "Смет. извештај"); ?></li>
                -->
            </ul>
        <?php } ?>
		
		<ul class="admin_menu">
			<?php if (in_array($userID, array(1))) { //ako samo e superadmin prikazi gi linkovite ?>
				<li><?php print html::anchor("/admin2/download", "E-mail export all"); ?></li>
				<li>|&nbsp;<?php print html::anchor("/admin2/download_by_provider/gmail_yahoo", "E-mail export yahoo and gmail"); ?></li>
				<li>|&nbsp;<?php print html::anchor("/admin2/download_by_provider/others", "E-mail export all the rest"); ?></li>
			<?php } ?>	
		</ul>

        <ul class="admin_menu">
            <li><a target="_blank" href="/all/">Сите понуди</a></li>
            <li>|&nbsp;<a target="_blank" href="/all/index/2">Најпродавани актуелни</a></li>
            <li>|&nbsp;<a target="_blank" href="/past/index/0">Најпродавани изминати</a></li>
            <?php if (in_array($userID, array(1))) { //ako samo e superadmin prikazi gi linkovite ?>
                <li>|&nbsp;<?php print html::anchor("/admin2/download_customers", "Досегашни купувачи - Export"); ?></li>
            <?php } ?>  
        </ul>
		
        <ul class="admin_menu">
            
       <!--  <li><?php print html::anchor("/acategories/header_banner", "Header банер"); ?></li> -->

        <li><?php print html::anchor("/abanners/index", "Банер систем"); ?></li>

            <?php if (in_array($userID, array(1, 36, 23))) { //ako se samo 1 (Jovica), 36(Janja) i 23(Administrator) ?>
                

                    <li>|&nbsp;<?php print html::anchor("/admin2/amazon_newsletter_scheduler", "AMAZON - Newsletter"); ?></li>
                    
                    <li>|&nbsp;<?php print html::anchor("/admin/newsletterstats", "Пријавени е-mail-ови"); ?></li>     

                    <?php if (in_array($userID, array(1))) { //ako samo e superadmin prikazi gi linkovite ?>
                        <li>|&nbsp;<?php print html::anchor("/admin2/changepassword_prijaveni_mailovi", "Пр. лозинка - Пријав. е-mail-ови"); ?></li>
                    <?php } ?> 
                    
                
            <?php } ?> 

            <li>|&nbsp;<?php print html::anchor("/admin_naracki/index", "Нарачки"); ?></li>
        </ul>

    </div>

</div>