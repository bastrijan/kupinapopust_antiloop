<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once 'menu.php'; 

if (!empty($ParentCatData)) {
    $Parent_id = $ParentCatData[0]->id;
    $Parent_name = $ParentCatData[0]->name;
} else {
    url::redirect("/acategories");
}
?>

<h1><?php echo (!isset($SubCatData) ? "Внеси" : "Уреди"); ?> поткатегорија за категоријата "<?php echo $Parent_name ?>"</h1>

	
<?php if (!isset($SubCatData)) { ?>

    <form method="post" action="" id="deal_save_form">
        <div class="container_12 homepage-billboard-dhtml">
            <div class="grid_12 alpha omega">
                <input type="hidden" name="id" value="">
                <input type="hidden" name="category_id" value="<?php echo isset($Parent_id) ? $Parent_id : 1 ?>">

                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Име</div>
                    <div class="grid_8 alpha omega"><input type="text" name="name" value="" ></div>
                </div>

			
                <div class="row clearfix">
                    <input type="submit" value="Зачувај">
                    <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/acategories/podkategorii?parent_id=<?php echo $Parent_id; ?>';return false;">
                </div>
                
            </div>
        </div>
    </form>
<?php } ?>

<?php if (isset($SubCatData)) { ?>
    <form method="post" action="" id="deal_save_form">
        <div class="container_12 homepage-billboard-dhtml">
            <div class="grid_12 alpha omega">
                <input type="hidden" name="id" value="<?php print $SubCatData[0]->id ?>">
                <input type="hidden" name="category_id" value="<?php print $SubCatData[0]->category_id ?>">
                
                <div class="row clearfix">
                    <div class="grid_4 alpha omega">Име</div>
                    <div class="grid_8 alpha omega"><input type="text" name="name"  value="<?php print $SubCatData[0]->name ?>" ></div>
                </div>
               
                <div class="row clearfix">
                    <input type="submit" value="Зачувај" value="save_btn">
                    <input type="submit" value="Откажи" value="cancel_btn" onclick="location.href = '/acategories/podkategorii?parent_id=<?php echo $SubCatData[0]->category_id; ?>';return false;">
                </div>
                
            </div>
        </div>
    </form>

<?php } ?>
