<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once 'menu.php' ; ?>

		<h1>Категории</h1>
		<br/>
<div class="container_12 homepage-billboard">
    <div class="clearfix" style="margin-bottom: 30px">
		<?php
			print "<a href='/acategories/uredi'>Внеси нова категорија</a>";
		?>
		<table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
			<!-- HEAD -->
			<tr>
				<td><strong>ID</strong><br/>(за newsletter-ot)</td>
				<td><strong>Име</strong></td>
				<td><strong>Број на понуди</strong></td>
				<td ><strong>&nbsp;</td>
			</tr>
			<tr>
				<td  colspan="4"><hr></td>
			</tr>
			<!-- END HEAD -->
		<?php
		if ($categoriesData) {
			foreach ($categoriesData as $category) {
		?>
			<tr>
				<td ><?php echo $category->id; ?></td>
				<td ><?php echo $category->name; ?></td>
				<td ><?php print html::anchor("/admin?type=all&category_id_selected=$category->id", $dealsByCategoryData[$category->id]);?></td>
				<td >
					<?php
                                                print html::anchor("/acategories/podkategorii?parent_id=$category->id", "Поткатегории");
						print "&nbsp;&nbsp;";
						print html::anchor("/acategories/uredi?id=$category->id", "Промени");
						print "&nbsp;&nbsp;";
						if($dealsByCategoryData[$category->id]> 0)
							print html::anchor("/", "Избриши", array('onclick' => 'alert("Не можете да ја избришете оваа категорија затоа што постојат понуди поврзани со неа!\nВе молиме да ги префрлите понудите поврзани со оваа категорија во друга категорија и потоа да ја избришете."); return false'));
						else
							print html::anchor("/acategories/izbrisi?id=$category->id", "Избриши", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да ја избришеш оваа категорија?")'));
					?>
				</td>

			</tr>
			<tr>
				<td  colspan="4"><hr></td>
			</tr>
		<?php
			}
		}//if ($finreportdata) {
		?>		
		</table>
		
    </div>
</div>