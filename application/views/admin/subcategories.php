<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once 'menu.php'; ?>
<?php
if (isset($ParentCatData)) {
    $Parent_id = $ParentCatData[0]->id;
    $Parent_name = $ParentCatData[0]->name;
} else {
    url::redirect("/acategories");
}
?>
<h1>Поткатегории на категоријата  "<?php echo $Parent_name ?>" </h1>
<div class="container_12 homepage-billboard">
    <div class="clearfix" style="margin-bottom: 30px">

        <?php
        print "<a href='/acategories/uredi_podkategorija?parent_id=$Parent_id'>Внеси нова поткатегорија</a><br>";
        ?>
        <table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
            <!-- HEAD -->
            <tr>
                <td><strong>ID</strong></td>
                <td><strong>Име</strong></td>
                <td><strong>Број на понуди</strong></td>
                <td ><strong>&nbsp;</td>
            </tr>
            <tr>
                <td  colspan="4"><hr></td>
            </tr>
            <!-- END HEAD -->
            <?php
            if ($SubCatData) {
                foreach ($SubCatData as $subcategory) {
                    ?>
                    <tr>
                        <td ><?php echo $subcategory->id; ?></td>
                        <td ><?php echo $subcategory->name; ?></td>
                        <td ><?php 
									//print html::anchor("/admin?type=all&category_id_selected=$subcategory->category_id", $dealsBySubcategoryData[$subcategory->id]);
									print $dealsBySubcategoryData[$subcategory->id];
							 ?>
						</td>
                        <td >
                            <?php
                            print html::anchor("/acategories/uredi_podkategorija?id=$subcategory->id&parent_id=$Parent_id", "Промени");
                            print "&nbsp;&nbsp;";
                            if($dealsBySubcategoryData[$subcategory->id]> 0)
                                    print html::anchor("/", "Избриши", array('onclick' => 'alert("Не можете да ја избришете оваа категорија затоа што постојат понуди поврзани со неа!\nВе молиме да ги префрлите понудите поврзани со оваа категорија во друга категорија и потоа да ја избришете."); return false'));
                            else
                                    print html::anchor("/acategories/izbrisi_podkategorija?id=$subcategory->id&parent_id=$Parent_id", "Избриши", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да ја избришеш оваа категорија?")'));
                            ?>
                        </td>

                    </tr>
                    <tr>
                        <td  colspan="4"><hr></td>
                    </tr>
                    <?php
                }
            }//if ($finreportdata) {
            ?>		
        </table>

    </div>
</div>