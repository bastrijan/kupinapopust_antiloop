<?php defined('SYSPATH') OR die('No direct access allowed.') ; 
require_once 'menu.php' ; 
?>

<div class="container_12 homepage-billboard">
  <div class="clearfix" style="margin-bottom: 30px">

		<?php print html::anchor("/admin2/send_newsletters", "Креирај AMAZON - Newsletter"); ?>
		
		<br/><br/>

		<table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
			<!-- HEAD -->
			<tr>
				<td><strong>Наслов(Subject)</strong></td>
				<td><strong>Одбран Newsletter</strong></td>
				<td><strong>Испрати на</strong></td>
				<td><strong>Датум и час</strong></td>
				<td><strong>Статус</strong></td>
				<td><strong>Акција</strong></td>
			</tr>
			<tr>
				<td  colspan="6"><hr></td>
			</tr>
			<!-- END HEAD -->
		<?php

		if ($reportData) {
			foreach ($reportData as $reportItem) {
					
					/*******Одбран Newsletter*********/
					$type_naslov = "";
					$type_link = "";
					
					//ako e newsletter_category
					if (strpos($reportItem->newsletter_type,'newsletter_category') !== false)
					{
						$newsletterArray = explode("/", $reportItem->newsletter_type);
						$catID = $newsletterArray[1];
						
						//$type_naslov = "Категорија - ".$categoriesData[$catID]."<br/>(http://kupinapopust.mk/index/newsletter_category/$catID)";
						$type_naslov = "Категорија - ".$categoriesData[$catID];
						$type_link = "http://kupinapopust.mk/index/newsletter_category/$catID";
					}
					else//ako e drug newsletter
					{
						if($reportItem->newsletter_type == "newsletter")
							//$type_naslov = "Сите понуди<br/>(http://kupinapopust.mk/index/newsletter)";
							$type_naslov = "Сите понуди";
						elseif($reportItem->newsletter_type == "newsletter_main_offers")
								//$type_naslov = "Главни понуди<br/>(http://kupinapopust.mk/index/newsletter_main_offers)";
								$type_naslov = "Главни понуди";
							else
								//$type_naslov = "Најпродавани понуди<br/>(http://kupinapopust.mk/index/newsletter_bestsellers)";
								$type_naslov = "Најпродавани понуди";
						
						$type_link = "http://kupinapopust.mk/index/".$reportItem->newsletter_type;
					} 
					
					
					/*******Испрати на*********/
					$isprati_na = "";
					
					if($reportItem->send_to == "")
						$isprati_na = "Сите";	
					else
					{

						if (strpos($reportItem->send_to, "email LIKE '%@gmail.%'") !== false)
							$isprati_na .= "E-mail адреси од <strong>GMAIL</strong><br/>";	
							
						if (strpos($reportItem->send_to, "email LIKE '%@yahoo.%'") !== false)
							$isprati_na .= "E-mail адреси од <strong>YAHOO</strong><br/>";	
							
						if (strpos($reportItem->send_to, "email NOT LIKE '%@gmail.%' AND email NOT LIKE '%@yahoo.%'") !== false)
							$isprati_na .= "<strong>Сите останати</strong> e-mail адреси";	

						//////////////****///////////////

						if (strpos($reportItem->send_to,  "customer_id > 0") !== false)
							$isprati_na .= "Регистрирани корисници - <strong>купувачи</strong><br/>";	

						if (strpos($reportItem->send_to,  "customer_id = 0 OR customer_id IS NULL") !== false)
							$isprati_na .= "Регистрирани корисници - <strong>примачи на newsletter</strong><br/>";  

						//////////////****///////////////
						
						if (strpos($reportItem->send_to,  "DATEDIFF(DATE(NOW()), last_visited_website) <= 90") !== false)
							$isprati_na .= "Активни корисници<br/>"; 

						if (strpos($reportItem->send_to,  "DATEDIFF(DATE(NOW()), last_visited_website) > 90") !== false)
							$isprati_na .= "Неактивни корисници<br/>"; 


					}
					
					/*******Статус*********/
					$status = "";
					
					if($reportItem->current_status == 0)
						$status = "Не е почнат";
					elseif($reportItem->current_status == 1)
							$status = "Обработен";
						elseif($reportItem->current_status == 2)
								$status = "Завршен";
		?>
					<tr>
						<td width="20%"><?php print $reportItem->name; ?></td>
						<td >
							<?php print html::anchor($type_link, $type_naslov, array('target' => '_blank')); ?>
						</td>
						<td ><?php echo $isprati_na; ?></td>
						<td><?php echo date("d/m/Y  H:i", strtotime($reportItem->date_time)); ?></td>
						<td ><?php echo $status; ?></td>
						<td >
							<?php
							print html::anchor("/admin2/izbrisi_amazon_newsletter_scheduler/$reportItem->id", "Избриши", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да го избришеш овој запис?")'));
							?>
						</td>
					</tr>
					<tr>
						<td  colspan="6"><hr></td>
					</tr>
				<?php
				
			}//foreach ($reportData as $reportItem) {
		}//if ($reportData) {
		?>		
			
		</table>
	
  </div>
</div>