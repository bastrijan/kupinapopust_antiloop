<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php require_once 'menu.php' ; ?>

		<h1>Приходи</h1>
		<br/>
		<div><?php  print $dealData->title_mk  ; ?></div>
		<br/>

<div class="container_12 homepage-billboard">
    <div class="clearfix" style="margin-bottom: 30px">

			<?php
				print "<a href='/admin/uredidealincome/0/$dealData->id'>Внеси нов приход</a>";
			?>

			<br/>
			<table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
				<!-- HEAD -->
				<tr>
					<td width="50%"><strong>Приход</strong></td>
					<td ><strong>Сума</strong></td>
					<td ><strong>&nbsp;</td>
				</tr>
				<tr>
					<td  colspan="3"><hr></td>
				</tr>
				<!-- END HEAD -->
			<?php
			if ($dealIncomesData) {
				foreach ($dealIncomesData as $dealIncome) {
			?>
				<tr>
					<td width="50%" ><?php echo $dealIncome->description; ?></td>
					<td ><?php echo $dealIncome->price. " ден."; ?></td>
					<td >
						<?php
							print html::anchor("/admin/uredidealincome/$dealIncome->id/$dealData->id", "Промени");
							print "&nbsp;&nbsp;";
		print html::anchor("/admin/izbrisidealincome/$dealIncome->id/$dealData->id", "Избриши", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да го избришеш овој приход?")'));
						?>
					</td>

				</tr>
				<tr>
					<td  colspan="3"><hr></td>
				</tr>
			<?php
				}
			}//if ($finreportdata) {
			?>		
			</table>
    </div>
</div>