<?php defined('SYSPATH') OR die('No direct access allowed.') ; ?>
<?php require_once 'menu.php' ; ?>

<style type="text/css">

    input[type=checkbox] {
      /* All browsers except webkit*/
      transform: scale(2);

      /* Webkit browsers*/
      -webkit-transform: scale(2);
    }

</style>

<?php 
	$monthArray = array(
		'01' => 'Jan',
		'02' => 'Feb',
		'03' => 'Mar',
		'04' => 'Apr',
		'05' => 'May',
		'06' => 'Jun',
		'07' => 'Jul',
		'08' => 'Aug',
		'09' => 'Sep',
		'10' => 'Oct',
		'11' => 'Nov',
		'12' => 'Dec',
      );
	
	$CurrentYear = date("Y"); 
?>

<script type="text/javascript">

    $(document).ready(function() {

		$( "#newsletter_multiple_delete" ).submit(function( event ) 
		{
			val_flag = false;
			val_txt = "";


			if($('.multiple_delete_arr:checked').length < 1)
			{
				val_txt += "Ве молиме одберете барем една email адреса која сакате да ја избришeте!";
				val_flag = true;
			}
			
			
			if(val_flag)
			{
				alert(val_txt);
				event.preventDefault();
			}
			
		});
		
		

		<?php if (isset($succes_msg) and $succes_msg == 1) { ?>
					$.noticeAdd({
						text: "Успешно ги избришавте избраните email адреси.",
						stay: false,
						type:'notification-success'
						//                stayTime: 3000
					});
				
		<?php } ?>
		

		
    });
</script>


<div class="container_12 homepage-billboard">
  <div class="clearfix" style="margin-bottom: 30px">
    <h1>Листа на пријавени e-mail адреси</h1>
    <div style="margin-top: 20px">

        <form method="get" action="/admin/newsletterstats?<?php echo "from_day=$from_day&from_month=$from_month&from_year=$from_year&to_day=$to_day&to_month=$to_month&to_year=$to_year&search_email=$search_email"; ?>" style="float:left">
            Датум од:
            <select name="from_day">
                <?php for ($i = 1; $i <= 31; $i++) { ?>
                <option <?php echo ($i == $from_day ? 'selected' : '');?>><?php echo strlen($i) < 2 ? '0'.$i : $i ?></option>
                <?php } ?>
            </select>
            <select name="from_month">
                <?php foreach ($monthArray as $monthNu => $monthStr){ ?>
                    <option value="<?php echo $monthNu; ?>" <?php echo $monthNu == $from_month ? 'selected' : ''; ?> > <?php echo $monthStr; ?> </option>
                <?php } ?>
            </select>
            <select name="from_year">
                <?php for ($i = 2010; $i <= $CurrentYear; $i++) { ?>
                    <option <?php echo $i == $from_year ? 'selected' : ''; ?> > <?php echo $i; ?> </option>
                <?php } ?>
            </select>
			
			&nbsp;&nbsp;
            Датум до:
            <select name="to_day">
                <?php for ($i = 1; $i <= 31; $i++) { ?>
                <option <?php echo ($i == $to_day ? 'selected' : '');?>><?php echo strlen($i) < 2 ? '0'.$i : $i ?></option>
                <?php } ?>
            </select>
            <select name="to_month">
                <?php foreach ($monthArray as $monthNu => $monthStr){ ?>
                    <option value="<?php echo $monthNu; ?>" <?php echo $monthNu == $to_month ? 'selected' : ''; ?> > <?php echo $monthStr; ?> </option>
                <?php } ?>
            </select>
            <select name="to_year">
                <?php for ($i = 2010; $i <= $CurrentYear; $i++) { ?>
                    <option <?php echo $i == $to_year ? 'selected' : ''; ?> > <?php echo $i; ?> </option>
                <?php } ?>
            </select>

            &nbsp;&nbsp;
            E-mail адреса: <input type="text" name="search_email" id="search_email" value="<?php echo $search_email?>">
			
            <input type="submit" value="Филтрирај">
       </form>



      <p>Вкупно има <?php print count($grid);?> пријавени адреси</p>
	  <br/>
	
	  <form method="post" action="/admin/newsletter_multiple_delete?<?php echo "from_day=$from_day&from_month=$from_month&from_year=$from_year&to_day=$to_day&to_month=$to_month&to_year=$to_year&search_email=$search_email"; ?>" name="newsletter_multiple_delete" id="newsletter_multiple_delete">
		  <table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
			<!-- HEAD -->
			<tr>
				<td ><strong>Email адреса</strong></td>
				<td><strong>Креиран</strong></td>
				<td><strong>Последна посета</strong></td>
				<td><input type="submit" name="delete_submit" id="delete_submit" value="Бриши"></td>
			</tr>
			<!-- END HEAD -->
			<tr>
				<td  colspan="3"><hr></td>
			</tr>
			
			<?php foreach ($grid as $row) { ?>
				<tr>
					<td >
						<?php print $row->email;  ?>
					</td>
					<td>&nbsp;&nbsp;<?php print date("d/m/Y H:i", strtotime($row->date_created));  ?></td>
					<td>&nbsp;&nbsp;<?php if(!empty($row->last_visited_website)) print date("d/m/Y", strtotime($row->last_visited_website));  ?></td>
					<td><input type="checkbox" name="multiple_delete_arr[]" class="multiple_delete_arr" value="<?php echo $row->id ?>"></td>
					
				</tr>
				<tr>
					<td  colspan="3"><hr></td>
				</tr>
			<?php } ?>
		</table>
	</form>	

    </div>
  </div>
</div>
