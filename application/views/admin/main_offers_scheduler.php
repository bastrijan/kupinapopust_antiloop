<?php defined('SYSPATH') OR die('No direct access allowed.') ; 
require_once 'menu.php' ; 
?>

<script type="text/javascript">

	$(document).ready(function() {
		$("#main_offer").change(function () {
			$("#mo_scheduler_frm").submit();
		});
	});
</script>

<div class="container_12 homepage-billboard">
  <div class="clearfix" style="margin-bottom: 30px">

		<form method="get" action="" id="mo_scheduler_frm">
			<?php print "Главна понуда: " ?> 
			<select id="main_offer" name="main_offer">
				  <option <?php if(0==$main_offer) echo 'selected="selected"'; ?> value="0">
						  <?php print "Сите" ?>
				  </option>
				<?php for($i=1; $i<=5; $i++) { ?>
				<option value="<?php echo $i?>" <?php print ($main_offer == $i) ? 'selected="true"' : ""  ?>><?php echo $i?></option>
				<?php }//for($i=1; $i<=3; $i++)  ?>
				
			</select>
		</form>
		
		<br/>
		<table border='0' cellspacing='1' cellpadding='5' align="center" width="100%">
			<!-- HEAD -->
			<tr>
				<td width="50%"><strong>Понуда</strong></td>
				<td><strong>Број на Главна понуда</strong></td>
				<td><strong>Датум и час</strong></td>
				<td><strong>Акција</strong></td>
			</tr>
			<tr>
				<td  colspan="4"><hr></td>
			</tr>
			<!-- END HEAD -->
		<?php

		if ($reportData) {
			foreach ($reportData as $reportItem) {
				
		?>
					<tr>
						<td width="50%" >
							<?php print html::anchor("/admin/autosave_deal/$reportItem->deal_id", strip_tags($reportItem->deal_title_mk), array("target" => "_blank")); ?>
						</td>
						<td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $reportItem->main_offer; ?></td>
						<td><?php echo date("d/m/Y  H:i", strtotime($reportItem->date_time)); ?></td>
						<td >
							<?php
							print html::anchor("/admin/izbrisi_main_offer_schedule/$reportItem->id", "Избриши", array('onclick' => 'return confirm("Дали си сигурен дека сакаш да го избришеш овој запис?")'));
							?>
						</td>
					</tr>
					<tr>
						<td  colspan="4"><hr></td>
					</tr>
				<?php
				
			}//foreach ($reportData as $reportItem) {
		}//if ($reportData) {
		?>		
			
		</table>
	
  </div>
</div>