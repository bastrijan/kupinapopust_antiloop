<?php defined('SYSPATH') or die('No direct script access.');

class Static_Model extends Default_Model {

    protected $_tableName = 'static_pages';

    public function __construct() {
        parent::__construct();
    }

}