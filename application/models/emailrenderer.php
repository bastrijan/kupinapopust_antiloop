<?php
if (! defined('SYSPATH')) {
    die('No direct script access.');
}

/* usage:

$emailrenderer = new Emailrenderer_Model();
$maildata    = array(
                'variable1'=>"value1",
                'variable2'=>"value2",
                'variable3'=>"value3"
);

$content     = $emailrenderer->render("voucher", $maildata);
$nrSent      = email::send($email, "Sender Name <".$senderemail.">", $subject, $content, true);
 * 
*/

class Emailrenderer_Model extends Default_Model {

    /**
     * Defines default table name
     * @var string
     */
    protected $_tableName       = '';

    protected $templatePath     = '';
    
    public function __construct () {
        parent::__construct();
        $this->templatePath = 'emails/'.$this->lang.'/';
    }

    public function render($viewName, $data){
        $view = new View($this->templatePath.$viewName);
        foreach ($data as $key => $value) {
            $view->$key = $value;
        }
        return $view->render();
    }



}

