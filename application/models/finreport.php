<?php defined('SYSPATH') or die('No direct script access.');

class FinReport_Model extends Default_Model {

	protected $_tableName = 'finizvestaj';
	
	public function __construct() {
		parent::__construct();
	}
	
	public function getFinIzvestaj($finreport_year, $finreport_month, $vraboten_id = 0, $category_id = 0, $partner_id = 0) {
		
		$firstDayOfMonth = "$finreport_year-$finreport_month-1";
		
		$numDaysOfMonth = date('t', strtotime("$finreport_year-$finreport_month-1"));
		$lastDayOfMonth = "$finreport_year-$finreport_month-$numDaysOfMonth";
		
		$sql =  "SELECT d.id AS deal_id, do.id AS deal_option_id, do.title_mk AS title_mk_clean, do.price_discount, d.tip AS tip_ponuda, d.tip_danocna_sema, d.ddv_stapka, IF(f.prodadeni IS NULL, 0, f.prodadeni) AS prodadeni, do.price_voucher, (do.price_voucher * IF(f.prodadeni IS NULL, 0, f.prodadeni)) AS vk_zarabotka, ";
		$sql .= "(SELECT SUM(IF(e.price IS NULL OR do.default_option = 0, 0, e.price)) FROM deal_expences AS e WHERE e.deal_id = d.id) AS mesecni_trosoci, IF(do.default_option = 0, 0, d.platena_reklama) AS platena_reklama, d.platena_reklama_date, v.name AS vraboten_name, d.start_time, d.end_time, ";
		$sql .= "cat.name AS category_name, par.name AS partner_name, ";
		$sql .= "IF(f.with_cash IS NULL, 0, f.with_cash) AS payed_with_cash, ";
		$sql .= "IF(f.with_card IS NULL, 0, f.with_card) AS payed_with_card, ";
		$sql .= "IF(f.with_bank IS NULL, 0, f.with_bank) AS payed_with_bank, ";
		$sql .= "IF(f.other_than_cash IS NULL, 0, f.other_than_cash) AS payed_other_than_cash, ";
		$sql .= "IF(f.app_type_website IS NULL, 0, f.app_type_website) AS bought_via_website, IF(f.app_type_android IS NULL, 0, f.app_type_android) AS bought_via_android, IF(f.app_type_ios IS NULL, 0, f.app_type_ios) AS bought_via_ios, ";
		$sql .= "d.title_mk AS title_mk_deal, d.title_mk_clean AS title_mk_clean_deal, 	(SELECT COUNT(doc.id) FROM deal_options AS doc WHERE doc.deal_id = d.id) AS options_cnt ";
		$sql .= "FROM deals AS d ";
		$sql .= "LEFT JOIN deal_options AS do ON do.deal_id = d.id ";
		$sql .= "LEFT JOIN $this->_tableName AS f ON f.deal_id = d.id AND f.deal_option_id = do.id AND f.godina = $finreport_year AND f.mesec = $finreport_month ";
		$sql .= "LEFT JOIN vraboteni AS v ON v.id = d.vraboten_id ";
		$sql .= "LEFT JOIN partners AS par ON par.id = d.partner_id ";
		$sql .= "LEFT JOIN category AS cat ON cat.id = d.category_id ";
		$sql .= "WHERE d.visible = 1 AND (d.vraboten_id = $vraboten_id OR 0 = $vraboten_id) AND (d.category_id = $category_id OR 0 = $category_id) AND (d.partner_id = $partner_id OR 0 = $partner_id) AND DATE(d.start_time) <= '$lastDayOfMonth' AND DATE(d.end_time) >= '$firstDayOfMonth' ";
		$sql .= "ORDER BY d.start_time DESC, d.id, do.id ";
		//die($sql);
		
		//uslov za intersection between date periods
		//(StartA <= EndB) and (EndA >= StartB)
		
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
	
	public function getMesecniTrosoci($finreport_year, $finreport_month) {
		
		$sql = "SELECT IF(SUM(price) IS NULL, 0, SUM(price)) AS vk_mesecni_trosoci FROM monthly_expences WHERE year = $finreport_year AND month = $finreport_month ";
		
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
	
	public function getMesecniPrihodi($finreport_year, $finreport_month) {
		
		$sql = "SELECT IF(SUM(price) IS NULL, 0, SUM(price)) AS vk_mesecni_prihodi FROM monthly_incomes WHERE year = $finreport_year AND month = $finreport_month ";
		
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
	
	public function getVrabotenTrosoci($vraboten_id, $finreport_year, $finreport_month) {
		
		$sql = "SELECT IF(SUM(price) IS NULL, 0, SUM(price)) AS vk_vraboten_trosoci FROM vraboten_expences WHERE vraboten_id = $vraboten_id AND year = $finreport_year AND month = $finreport_month ";
		
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
	
	public function getCntVouchersByVraboten($vraboten_id = 0) {
		
		$sql = "SELECT COUNT(v.id) AS cnt_vouchers FROM voucher AS v INNER JOIN deals AS d ON d.id = v.deal_id AND (d.vraboten_id = $vraboten_id OR 0 = $vraboten_id) ";
		
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
	
	public function getCntDealsByVraboten($vraboten_id = 0) {
		
		$sql = "SELECT COUNT(id) AS cnt_ponudi FROM deals WHERE demo = 0 AND visible = 1 AND (vraboten_id = $vraboten_id OR 0 = $vraboten_id) ";
		
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
	
	///izminati ponudi
	public function getFinIzvestajIzminati($sk = "") {
		
		$result = array();
		
		$sk = trim($sk);
		
		if($sk != "")
		{
			/*
			$parts = explode(" ", $sk);
			$clauses=array();
			foreach ($parts as $part){
				//function_description in my case ,  replace it with whatever u want in ur table
				$clauses[]="d.title_mk LIKE '%" . $this -> db ->escape_str($part) . "%'";
			}
			$clause=implode(' OR ' ,$clauses);
			*/
			$clause = "do.title_mk LIKE '%" . $sk . "%'";

			$sql =  "SELECT d.id AS deal_id, do.id AS deal_option_id, do.title_mk AS title_mk_clean, do.price_discount, d.tip AS tip_ponuda, d.tip_danocna_sema, d.ddv_stapka, IF(f.prodadeni IS NULL, 0, f.prodadeni) AS prodadeni, do.price_voucher, (do.price_voucher * IF(f.prodadeni IS NULL, 0, f.prodadeni)) AS vk_zarabotka, ";
			$sql .= "(SELECT SUM(IF(e.price IS NULL OR do.default_option = 0, 0, e.price)) FROM deal_expences AS e WHERE e.deal_id = d.id) AS mesecni_trosoci, IF(do.default_option = 0, 0, d.platena_reklama) AS platena_reklama, d.platena_reklama_date, v.name AS vraboten_name, d.start_time, d.end_time, ";
			$sql .= "cat.name AS category_name, par.name AS partner_name, ";
			$sql .= "IF(f.with_cash IS NULL, 0, f.with_cash) AS payed_with_cash, ";
			$sql .= "IF(f.other_than_cash IS NULL, 0, f.other_than_cash) AS payed_other_than_cash, ";
			$sql .= "IF(f.app_type_website IS NULL, 0, f.app_type_website) AS bought_via_website, IF(f.app_type_android IS NULL, 0, f.app_type_android) AS bought_via_android, IF(f.app_type_ios IS NULL, 0, f.app_type_ios) AS bought_via_ios, ";
			$sql .= "d.title_mk AS title_mk_deal, d.title_mk_clean AS title_mk_clean_deal, 	(SELECT COUNT(doc.id) FROM deal_options AS doc WHERE doc.deal_id = d.id) AS options_cnt ";
			$sql .= "FROM deals AS d ";
			$sql .= "LEFT JOIN deal_options AS do ON do.deal_id = d.id ";
			$sql .= "LEFT JOIN finizvestaj_izminati AS f ON f.deal_id = d.id AND f.deal_option_id = do.id ";
			$sql .= "LEFT JOIN vraboteni AS v ON v.id = d.vraboten_id ";
			$sql .= "LEFT JOIN partners AS par ON par.id = d.partner_id ";
			$sql .= "LEFT JOIN category AS cat ON cat.id = d.category_id ";
			$sql .= "WHERE d.visible = 1 AND d.end_time < '".date("Y-m-d H:i:s")."' AND ($clause) ";
			$sql .= "ORDER BY d.start_time DESC, d.id, do.id ";
			//die($sql);
			
			//uslov za intersection between date periods
			//(StartA <= EndB) and (EndA >= StartB)
			
			$result = $this -> db -> query($sql)->result_array();
			
			
		}
		
		return $result;
	}
	
}