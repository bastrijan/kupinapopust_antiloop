<?php

if (!defined('SYSPATH')) {
    die('No direct script access.');
}

class Mailqueue_Model extends Default_Model {

    /**
     * Defines default table name
     * @var string
     */
    protected $_tableName = 'mail_queue';

    /**
     * Constructor
     *
     */
    public function __construct() {
        parent::__construct();
    }

    public function addMail($fromMail, $fromName, $recipientMail, $recipientName, $title, $messageContent) {
        $message = array();

        $message['from_mail'] = $fromMail;
        $message['from_name'] = $fromName;
        $message['recipient_mail'] = $recipientMail;
        $message['recipient_name'] = $recipientName;
        $message['message_title'] = $title;
        $message['message_content'] = $messageContent;
        $message['mail_type'] = "html";

        $this->db->insert($this->_tableName, $message);
    }
    
    public function addMailBulk(array $recipientMails, $title, $messageContent) {
		$email_config = Kohana::config('email');
		
		$fromName = $email_config['from']['name'];
		$fromMail = $email_config['from']['email'];
        
        $sql = 'INSERT INTO mail_queue ';
        $sql.= '(id, from_mail, from_name, recipient_mail, recipient_name, message_title, message_content, mail_type, message_in_ts, message_sent_ts, sent) ';
        foreach ($recipientMails as $mail) {
            $values[] = "VALUES (NULL, $fromMail, $fromName, $mail->email, '', $title, $messageContent, 'html', CURRENT_TIMESTAMP, NULL, '0')";
        }
        $sql.= implode(",", $values);
        
        $this->db->query($sql);
            
        
    }

    public function sendMails($numberAtOnce = 30) {
//        exit;
        $time_start = microtime(true);

        // send the messages
        $queue = $this->readMails($numberAtOnce);
        $sentMails = array();

        foreach ($queue as $mail) {
            $sentMails[] = $mail->id;
        }
        $this->updateMails($sentMails);
        
        foreach ($queue as $mail) {
            $mail = (array)$mail;
            try {
                $result = email::send($mail['recipient_mail'], array($mail['from_mail'],$mail['from_name']), strip_tags($mail['message_title']), $mail['message_content'], true);
            } catch (Exception $e) {
                Kohana::log("info", $mail['recipient_mail']);
                $result = true;
            }
//            if ($result) {
//              $sentMails[] = $mail['id'];
//            } else {
//              Kohana::log("info", $mail['recipient_mail']);
//            }
            
        }

        
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        if (count ($queue) > 0 ) {
            echo count($sentMails)." mails sent in $time seconds\n";
        } else {
            echo "There was no mail to send\n";
        }
    }

    public function updateMails($mails) {
        if($mails and is_array($mails)) {
            $where = join(',', $mails);
            $sql = "DELETE from $this->_tableName where id in ($where)";
            $this->db->query($sql);
        }
    }

    public function readMails($numberAtOnce) {
        $messages = array();
        $messages = $this->db->select()->from($this->_tableName)->orderby("id", "ASC")->limit($numberAtOnce)->get()->result_array();
        return $messages;
    }

}