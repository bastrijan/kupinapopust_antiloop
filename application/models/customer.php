<?php

defined('SYSPATH') or die('No direct script access.');

class Customer_Model extends Default_Model {

    protected $_tableName = 'customer';

    public function __construct() {
        parent::__construct();
    }
	
	//returns 0 if doesn't exist
	//returns 1 if exists
	public function checkCustomerExist($email) {
		$records = $this->getData(array("email" => $email));
		return $records;
	}

    public function getCustomerID($email, $kp_aff_program_id = 0, $ap_earnings_transaction_id = 0) {
        if (!$email) {
            return false;
        }

        $records = $this->getData(array("email" => $email));

        //ako se raboti za nov kupuvac => 
        if (count($records) == 0) 
        {

            //ako treba da se proveri dali da se dodadat poeni na affiliate-ot za nov korisnik
            if($kp_aff_program_id)
            {


                $ref_id = intval($kp_aff_program_id);

                $switch_poeni_nov_kupuvac = 0;
                $poeni_nov_kupuvac = 0;

                if((int) $ref_id > 0 )
                {
                    //GET fixed prize for new client
                    $sql_settings = "SELECT new_cpc, (SELECT cpc_on FROM ap_other_commissions WHERE id=1) AS cpc_on FROM ap_members WHERE id= $ref_id ";
                    $res_settings = $this->db->query($sql_settings)->result_array();
                    
                    if(isset($res_settings))
                    {
                        $poeni_nov_kupuvac = $res_settings[0]->new_cpc;
                        $switch_poeni_nov_kupuvac = $res_settings[0]->cpc_on;                            
                    }

                }


                //IF e ovozmozena funcionalnosta za dobivanje na poeni za nov kupuvac 
                //dodadi mu poeni
                if($switch_poeni_nov_kupuvac)
                {
                    //UPDATE BALANCE RECORD
                    $sql_update = "UPDATE ap_members SET balance = (balance + $poeni_nov_kupuvac) WHERE id= $ref_id";
                    $this->db->query($sql_update);

                    //UPDATE ap_earnings so poenite sto se dodeleni za nov kupuvac
                    if($ap_earnings_transaction_id)
                    {
                        $sql_update = "UPDATE ap_earnings SET points_new_buyer = $poeni_nov_kupuvac WHERE id= $ap_earnings_transaction_id";
                        $this->db->query($sql_update);                            
                    }


                }


            }//if($kp_aff_program)



            return $this->createCustomer($email);
        }
        else //ako e postoecki korisnik
        {
            // PROVERI DALI KORISNIKOT E DEAKTIVIRAN. AKO E DEAKTIVIRAN AKTIVIRAJ GO
            if(!$records[0]->active)
                $this->customerReactivate($records[0]->id, $records[0]->email);

        }

        /* OLD CODE
        $data = array('id' => $records[0]->id, 'last_purchase' => date("Y-m-d H:i:s"));
        $this->saveData($data);
        */

        return $records[0]->id;
    }


    public function customerReactivate($customerID = 0, $customerEmail = "")
    {

        if($customerID == 0 || $customerEmail == "")
            return false;

        $newsletterModel = new Newsletter_Model();

        //Kreiranje na zapis vo tabelata newsletter
        $where_insert = array("email" => $customerEmail, "customer_id" => $customerID);
        $newsletterModel->saveSubscription($where_insert);


        //Setiranje na poleto customer.active = 1
        $newPassword = substr(md5(uniqid(rand(), true)), 0, 6);
        $where = array('id' => $customerID, 'active' => 1, 'password' => md5($newPassword));
        $this->saveData($where);
        
        //isprati soodveten email do korisnikot deka account-ot mu e reaktiviran i isprati mu nov password
        $mailContentModel = new Emailrenderer_Model();
        $maildata = array(
                'email' => $customerEmail,
                'password' => $newPassword,
                );
        $mailContent = $mailContentModel->render("customer_reactivation", $maildata);
        $email_config = Kohana::config('email');
        $from = array($email_config['from']['email'], $email_config['from']['name']);
        email::send($customerEmail, $from, "Kupinapopust.mk реактивација на кориснички профил!", $mailContent, true);


        return true;
    }

    public function getCustomerPoints($id) {
        if (!$id) {
            return false;
        }

        $pointsModel = new Points_Model();
        return $pointsModel->getCustomerPoints($id);
    }

    public function getCustomerMail($id) {
        if (!$id) {
            return false;
        }

        $record = $this->getData(array("id" => $id));
        if ($record) {
            return $record[0]->email;
        } else {
            return false;
        }
    }


    public function createFacebookCustomer(array $userData) {

        //stavi last_login
        $userData['last_login'] = date("Y-m-d H:i:s");
		
		//prilagodi go birthday za vo baza
		if ($userData['birthday']) {
			$birthday = explode("/", $userData['birthday']);
			$userData['birthday'] = $birthday[2] . "-" . $birthday[0] . "-" . $birthday[1];
		}
		
        $email = $userData['email'];
        $records = $this->getData(array("email" => $email));
        if (count($records) > 0) {
            $userData['login_type'] = 'both';
            $this->db->update($this->_tableName, $userData, array('id' => $records[0]->id));
            return $records[0]->id;
        } else {
            $userData['created'] = date("Y-m-d H:i:s");
            $res = $this->db->insert($this->_tableName, $userData);
            $newUserID = $res->insert_id();
            $this->signupNewsletter($email, $newUserID);
            return $newUserID;
        }
    }

    public function setLastLogin($customerID) {

        if ($customerID > 0) 
        {
            $userData['last_login'] = date("Y-m-d H:i:s");
            $this->db->update($this->_tableName, $userData, array('id' => $customerID));
        } 
    }

    public function setLastPurchase($customerID) {

        if ($customerID > 0) 
        {
            $userData['last_purchase'] = date("Y-m-d H:i:s");
            $this->db->update($this->_tableName, $userData, array('id' => $customerID));
        } 
    }
	
	
	//metoda za creiranje na customer
	//parametarot $registracija	pokazuva dali customerot se registrira sam ($registracija = true) bez kupuvanje na ponuda ili starite slucai na creiranje na customer
	public function createCustomer($email, $registracija = false, $birthday = null, $password = "") {

        $records = $this->getData(array("email" => $email, 'login_type' => 'default'));
        if (count($records) > 0) {
            $userData = array();
            $userData['login_type'] = 'both';
            $userData['last_login'] = date("Y-m-d H:i:s");

            $this->db->update($this->_tableName, $userData, array('id' => $records[0]->id));
            return $records[0]->id;
        }

        $set = array();
        $set['email'] = $email;
		
		if($password == "")
		{
			$pass = md5(uniqid(rand(), true));
			$pass = substr($pass, 0, 6);
		}
		else
			$pass = $password;	
		
		$set['password'] = md5($pass);
		
        /* OLD CODE
		if(!$registracija)
			$set['last_purchase'] = date("Y-m-d H:i:s");
        */
        
        $set['created'] = date("Y-m-d H:i:s");

		$set['last_login'] = date("Y-m-d H:i:s");
		
		if($registracija)
			$set['birthday'] = $birthday;

        $res = $this->db->insert($this->_tableName, $set);
		
		$mailContentModel = new Emailrenderer_Model();
		$maildata = array(
				'email' => $email,
				'password' => $pass
				);
		$mailContent = $mailContentModel->render("new_customer", $maildata);
		$email_config = Kohana::config('email');
		$from = array($email_config['from']['email'], $email_config['from']['name']);
		email::send($email, $from, "Kupinapopust.mk кориснички профил!", $mailContent, true);
		
        $newUserID = $res->insert_id();
        $this->signupNewsletter($email, $newUserID);

        //cookie::set("newsletter_popup", "true", 15552000);

        return $newUserID;
    }

    public function signupNewsletter($email, $userID) {
        $newsletterModel = new Newsletter_Model();

        $where_search = array("email" => $email);
        $row = $newsletterModel->getData($where_search);
        if (!$row) {
            $where_insert = array("email" => $email, "customer_id" => $userID);
            $newID = $newsletterModel->saveSubscription($where_insert);
        }
    }

    public function getCustomerGrid($search_email = "") 
    {

        $search_email = trim($search_email);

        if($search_email != "")
        {
            $sql = "SELECT customer.email as email, count(voucher.id) as count, customer.id as id, customer.last_login, customer.last_purchase, customer.active
                FROM customer 
                LEFT JOIN voucher  ON voucher.customer_id = customer.id 
                WHERE customer.email LIKE '%" . $search_email . "%'
                GROUP BY customer.id
                ORDER BY customer.id ";

            $res = $this->db->query($sql)->result_array();
            $resArray = array();
            $pointsArray = array();

            $last_loginArray = array();
            $last_purchaseArray = array();
            $activeArray = array();
            $customerIDArray = array();

            $pointsModel = new Points_Model();
            foreach ($res as $entry) {
                $resArray[$entry->email] = $entry->count;

                $last_loginArray[$entry->email] = $entry->last_login;

                $last_purchaseArray[$entry->email] = $entry->last_purchase;

                $activeArray[$entry->email] = $entry->active;

                $pointsArray[$entry->email] = $pointsModel->getCustomerPoints($entry->id);

                $customerIDArray[$entry->email] = $entry->id;
            }

            $result = array('count' => $resArray, 'points' => $pointsArray, 'last_login_arr' => $last_loginArray, 
                            'last_purchase_arr' => $last_purchaseArray, 'active' => $activeArray, 'customerIDs' => $customerIDArray);

        }
        else
            $result = array('count' => array(), 'points' => array(), 'last_login_arr' => array(), 'last_purchase_arr' => array(), 'active' => array(), 'customerIDs' => array());          

        return $result;
    }

    public function getCustumersBirthdaysToday() {
		$sql = 'SELECT * FROM customer 
				WHERE MONTH(birthday)='.date("m").' 
                    AND DAYOFMONTH(birthday)='.date("j").'
                    AND id NOT IN (SELECT customer_id FROM points_birthday_log WHERE YEAR(CAST(created AS DATE)) = '.date("Y").')';
        $res = $this->db->query($sql)->result_array();
        return $res;
    }
    
    public function isInBirthdayLog($customerID) {
        $sql = 'SELECT customer_id FROM points_birthday_log WHERE YEAR(CAST(created AS DATE)) = '.date("Y").' AND customer_id = '.$customerID;
        $res = $this->db->query($sql)->result_array();
//        print count($res);exit;
        return count($res);
    }

}