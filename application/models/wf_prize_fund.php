<?php defined('SYSPATH') or die('No direct script access.');

class Wf_Prize_Fund_Model extends Default_Model {

	protected $_tableName = 'wf_prize_fund';
	
	public function __construct() {
		parent::__construct();
	}
	
	public function getPrizeFundOrderBy($order) {
		
		return $this->db->select()->from($this->_tableName)
		->orderby($order, "DESC")->get()->result_array();
		
		
	}
        
	public function get_art_by_id($id) {
            
		return $this->db->select()->from($this->_tableName)->where('id', $id)
                ->get()->result_array();
		
	}
        
    public function delete($id) {
		return $this->db->delete($this->_tableName, array('id' => $id));
	}
	
	public function getSumPrizeQuantity() {
		
		$sql = "SELECT SUM(quantity) AS sum_prize_quntity FROM $this->_tableName";
		$res = $this->db->query($sql)->result_array();
		
		return $res[0]->sum_prize_quntity;
	}
	
	public function getDailyPrizeFund() {
		
		$getSumPrizeQuantity = $this->getSumPrizeQuantity();
		
		$dailyPrizeFund = 0;
		
		//ja delime so 28 zatoa sto sme dogovoreni samo prvite 28 dena vo mesecot da ima nagradi.
		//Ostatokot od mesecot ne delime nagradi.
		if($getSumPrizeQuantity > 0)
			$dailyPrizeFund = intval($getSumPrizeQuantity / 28);
		
		return $dailyPrizeFund;
	}
}