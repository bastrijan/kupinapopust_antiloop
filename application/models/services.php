<?php
if (! defined('SYSPATH')) {
    die('No direct script access.');
}

class Services_Model extends Default_Model {

    /**
     * The default table name
     */
    public $_identifier = '';
    public $_signature  = '';
    public $requestUrl = '';
    public $headers = '';

    public function  __construct() {
        parent::__construct();
        $this->_signature = $this->getSignature();
        $this->headers = array("HTTP_ACCEPT: {$_SERVER['HTTP_ACCEPT']}");
    }

    public function getSignature() {
        $this->_identifier = 'IjVMMSpgZgidDAfmRwcx';
        return md5($this->_identifier.date("Ymd"));
    }
    public function setRequestUrl($url) {
        $this->requestUrl = $url;
    }

    public function sendRequest($post, $associativeArray = true) {

        $post['identifier'] = $this->_signature;
        $c = curl_init ($this->requestUrl);
        if(is_array($post)) {
            $post = http_build_query($post);
        }
        curl_setopt ($c, CURLOPT_HTTPHEADER, $this->headers);
        curl_setopt ($c, CURLOPT_POST, true);
        curl_setopt ($c, CURLOPT_POSTFIELDS, $post);
        curl_setopt ($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($c, CURLOPT_COOKIESESSION, false);

        $resp = curl_exec ($c);
        if(curl_errno($c)) {
            echo 'Curl error 2: ' . curl_error($ch);
//            Event::run('system.404');
        }
        //var_dump($resp);
        curl_close ($c);
        $response = json_decode($resp, $associativeArray);

        return $response;
    }
}
