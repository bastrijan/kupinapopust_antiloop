<?php defined('SYSPATH') or die('No direct script access.');

class Wf_Players_Model extends Default_Model {

	protected $_tableName = 'wf_players';
	
	public function __construct() {
		parent::__construct();
	}

	public function getCntPlayersToday() {
		
		$sql = "SELECT COUNT(id) AS count FROM $this->_tableName WHERE date = '".date("Y-m-d")."' ";
		$res = $this->db->query($sql)->result_array();
		
		return $res[0]->count;
	}
	
	public function getCntWinnersToday() {
            
		$sql = "SELECT COUNT(id) AS count FROM $this->_tableName WHERE date = '".date("Y-m-d")."' AND winner = 1 ";
		$res = $this->db->query($sql)->result_array();
		
		return $res[0]->count;
	}
	
	public function getPreviousDayWinners() {
		
		$previous_day = date('Y-m-d', strtotime(' -1 day'));
		
		$sql = "SELECT c.login_value AS facebook_id FROM $this->_tableName AS p INNER JOIN customer AS c ON c.id = p.customer_id WHERE p.date = '".$previous_day."' AND p.winner = 1 ";
		
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
	
	public function hasUserPlayedToday($userID = 0) {
		
		$queryUserID = (int) $userID;
		
		$sql = "SELECT IF(COUNT(id) > 0, true, false) AS hasPlayed FROM $this->_tableName WHERE date = '".date("Y-m-d")."' AND customer_id = $queryUserID ";

		$res = $this->db->query($sql)->result_array();
		
		return $res[0]->hasPlayed;
	}
	
	public function savePlayer($userID = 0) {
		
		if((int)$userID > 0)
		{
			$data = array();
			$data['customer_id'] = $userID;
			$data['date'] = date("Y-m-d");
			
			//zacuvaj
			$this->saveData($data);
		}
			
	}
        
    public function saveWinnerPlayer($userID = 0,  $prize = 0, $assign_prize_to_email = '', $win_info = 1) {
		
		if((int)$userID > 0)
		{   
                    $where = array('customer_id'=>$userID, 'date' => date("Y-m-d"));
                    $set = array('winner' => $win_info, 'prize' => $prize, 'assign_prize_to_email' => $assign_prize_to_email);
                    $this->db->update($this->_tableName, $set, $where);
		}
			
	}
        
        public function hasUserwinnToday($userID = 0){
                $today = date("Y-m-d");
                return $this->db->select()->from($this->_tableName)->where(array('customer_id'=>$userID, 'date'=>$today, 'winner'=>'1'))->get()->result_array();
        }
		
		
	public function getCntWinnerByPriceCurrentMonth() {
		
		
		$current_month = date("Y-m");
		
		$sql = "SELECT prize, COUNT(id) AS cnt FROM $this->_tableName WHERE  DATE_FORMAT(date, '%Y-%m') = '".$current_month."' GROUP BY prize ";
		
		$res = $this->db->query($sql)->result_array();
		
		$result = array();
		foreach ($res as $record) {
			$result[$record->prize] = $record->cnt;
		}
		
		return $result;
	}
        
        public function getDayReport($DayDate = false, $players = 'all') {
		
                if($DayDate == false){
                    $DayDate = date("Y-m-d");
                }
                
                if(empty($players) || $players == 'all' || ($players != 'all' && $players != 'winners' ))
                    $playersClouse = '';
                elseif($players == 'winners')
                    $playersClouse = " and $this->_tableName.winner = '1' ";
                
                
                $sql = "SELECT customer.email, $this->_tableName.*
                        FROM $this->_tableName 
                        INNER JOIN customer
                        ON customer.id=$this->_tableName.customer_id
                        WHERE DATE_FORMAT($this->_tableName.date, '%Y-%m-%d') = '".$DayDate."'$playersClouse";
                
		$res = $this->db->query($sql)->result_array();
		
		$result = array();
		foreach ($res as $record) {
			$result[] = $record;
		}
		
		return $result;
	}
	
        public function getCountMonthPlayers($MonthDate = false, $players = 'all') {
                
                if($MonthDate == false){
                    $MonthDate = date("Y-m");
                }
                if(empty($players) || $players == 'all' || ($players != 'all' && $players != 'winners' ))
                    $playersClouse = '';
                elseif($players == 'winners')
                    $playersClouse = " and $this->_tableName.winner = '1' ";
                
                $sql = "SELECT COUNT(id) AS count
                        FROM $this->_tableName
                        WHERE DATE_FORMAT(date, '%Y-%m') = '".$MonthDate."' $playersClouse ";
		$res = $this->db->query($sql)->result_array();
		
		return $res[0]->count;
                
        }

	public function hasUserWonDaysAgo($userID = 0, $days = 30) {
		
		$queryUserID = (int) $userID;
		
		$sql = "SELECT IF(COUNT(id) > 0, true, false) AS hasWon FROM $this->_tableName WHERE date >= DATE_SUB(DATE(NOW()), INTERVAL $days DAY) AND customer_id = $queryUserID AND winner = 1 ";

		$res = $this->db->query($sql)->result_array();
		
		return $res[0]->hasWon;
	}
}