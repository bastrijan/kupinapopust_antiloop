<?php defined('SYSPATH') or die('No direct script access.');

class City_Model extends Default_Model {

    protected $_tableName = 'cities';

    public function __construct() {
        parent::__construct();
    }

	public function getCities() {
		
		$sql = "SELECT id, name_mk FROM $this->_tableName";
		$res = $this->db->query($sql)->result_array();
		
		$result = array();
		foreach ($res as $value) {
			$result[$value->id] = $value->name_mk;
		}
		
		return $result;
	}

}