<?php defined('SYSPATH') or die('No direct script access.');

class Default_Model extends Model_Core {

    public $lang = "mk";
    
    public function __construct() {
        parent::__construct();

        $this->session = Session::instance();
        
        if ($this->session->get("lang")) {
            $this->lang = $this->session->get("lang");
        }
        
    }

    /**
     * Insert/updates record in the table $this->_tableName
     * If id is set in $data updates, otherwise inserts new record
     *
     * @param array $data
     * @return int/bool
     */
     public function saveData(array $data){

         if(isset($data['id']) and $data['id']){
             $this->db->update($this->_tableName, $data, array('id'=>$data['id']));
             return $data['id'];
         }
         $result = $this->db->insert($this->_tableName, $data);
         return $result->insert_id();

    }//saveData

    public function detectIE() {
        if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false))
            return true;
        else
            return false;
    }

    /**
     *
     * @param Array $where
     * @return StdClass object
     */
	public function getData($where = 1, $orderBy = 1){
		
		if($orderBy == 1)
			$result = $this->db->select()->where($where)->get($this->_tableName)->result_array();
		else
			$result = $this->db->select()->where($where)->orderby($orderBy)->get($this->_tableName)->result_array();

        return $result;
    }//getData


}