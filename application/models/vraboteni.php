<?php defined('SYSPATH') or die('No direct script access.');

class Vraboteni_Model extends Default_Model {

    protected $_tableName = 'vraboteni';

    public function __construct() {
        parent::__construct();
    }

	public function getVraboteni() {
		
		$sql = "SELECT id, name FROM $this->_tableName";
		$res = $this->db->query($sql)->result_array();
		
		$result = array();
		foreach ($res as $value) {
			$result[$value->id] = $value->name;
		}
		
		return $result;
	}
}