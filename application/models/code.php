<?php defined('SYSPATH') or die('No direct script access.');

class Code_Model extends Default_Model {
    /**
     * The default table name
     */
    protected $_tableName = 'voucher';

    /**
     * Generates voucher code
     *
     * @param string $code_length max is 10 according to the db
     * @return string
     */
    public function generateCode($code_length=10) {

        $codeExists=true;

        while($codeExists) {
            $code = md5(uniqid(rand(), true));
            $code= substr($code, 0, $code_length);

            $codeExists=false;

            $dbCode = $this->db->select()->from($this->_tableName)->where("code", $code)->get();
            if(!$dbCode) {
                $codeExists = true;
            }
            else {
                $codeExists = false;
                break;
            }
        }

        return $code;
    }

    public function checkCode($code) {
        return $this->db->select()->from($this->_tableName)->where("code", $code)->get();
    }

    public function disableCode($code) {
        $where = array('code'=>$code);
        $set = array('used'=>1);
        $this->db->update($this->_tableName, $set, $where);
    }

}

