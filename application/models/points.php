<?php

defined('SYSPATH') or die('No direct script access.');

class Points_Model extends Default_Model {

    protected $_tableName = 'points';
    protected $_tableNameHistory = 'points_history';
    protected $_tableNameLogHistory = 'points_birthday_log';
    protected $_tableActivation = 'points_activation';

    public function __construct() {
        parent::__construct();
    }
    
    public function getHistoryRecords($customerID) {
        return $this->db->select()->from($this->_tableNameHistory)->where('customer_id', $customerID)->get()->result_array();
    }

    public function savePoints($customerID, $points, $reason, $saveInLog = false) {
        $data = array();
        $data['customer_id'] = $customerID;
        $data['total'] = $points;
        $data['remaining'] = $points;
        $data['created'] = date("Y-m-d H:i:s");
        $data['ends'] = date("Y-m-d 23:59:59", strtotime("+2 months"));

		if($points >=0)
		{
			//zacuvaj vo tabelata POINTS
			$this->saveData($data);
			
			//zacuvaj vo tabelata POINTS_HISTORY
			$this->saveHistory($customerID, $points, $reason, "+");
		}
		else
		{
			//odzemi soodvetno poeni od tabelata POINTS, dokolku ima vo pozitiva
			//dokolku ima pomalku otkolku sto treba, odzemi mu kolku sto ima.
			$points_odzemeni = $this->usePoints($customerID, abs($points), $reason);
		}
        
		if ($saveInLog) {
            $this->saveBirthdayLog($customerID, $data['created']);
        }
    }

    public function usePoints($customerID, $points, $reason) {
        //$where = array("customer_id" => $customerID, 'ends >=' => date("Y-m-d H:i:s", strtotime("-2 months")));
		$where = array("customer_id" => $customerID, 'ends >=' => date("Y-m-d H:i:s"));
        $records = $this->getData($where);
		$currentCustomerPoints = $this->getCustomerPoints($customerID);
		
		//kolku da mu se odzemat		
		$total = ($currentCustomerPoints > $points ? $points: $currentCustomerPoints);
		$usedPoints_return = $total;
		
        while ($total > 0) {
            $record = array_shift($records);
            $left = $record->remaining;
            if ($left > $total) {
                $setValue = $left - $total;
                $total = 0;
            } else {
                $setValue = 0;
                $total = $total - $left;
            }
            $this->saveData(array("id" => $record->id, 'remaining' => $setValue));
        }
		
		if($usedPoints_return > 0)
			$this->saveHistory($customerID, $usedPoints_return, $reason, "-");
		
		
		return $usedPoints_return;
    }

    public function saveBirthdayLog($customerID, $timestamp) {
        $set = array();
        $set['customer_id'] = $customerID;
        $set['created'] = $timestamp;
        $this->db->insert($this->_tableNameLogHistory, $set);
    }
    
    public function saveHistory($customerID, $points, $reason, $plusOrMinus) {
        $set = array();
        $set['customer_id'] = $customerID;
        $set['created'] = date("Y-m-d H:i:s");
        $set['reason'] = $reason;
        $set['points'] = $plusOrMinus . $points;
        $this->db->insert($this->_tableNameHistory, $set);
    }

    public function getCustomerPoints($customerID) {
        $where = array("customer_id" => $customerID, 'ends >=' => date("Y-m-d H:i:s"));
        $records = $this->getData($where);
        $sum = 0;
        if ($records) {
            foreach ($records as $record) {
                $sum = $sum + $record->remaining;
            }
        }

        return $sum;
    }
    
    public function activateGiftCard($email,$pointsData) {
        $customerModel = new Customer_Model();
        $customerID = $customerModel->getCustomerID($email);
        if (!$customerID) {
            $customerID = $customerModel->createCustomer($email);
        }
		
		$pointsGift = $pointsData[0]->points;
		if($pointsGift > 0)
			$this->savePoints($customerID, $pointsGift, "- Подарок поени со електронска „kupinapopust“ картичка");
        
		$set = array('status'=>'inactive');
        $where = array('id'=>$pointsData[0]->id);
        $this->db->update($this->_tableActivation, $set, $where);
        
    }
    public function getGiftCard($where) {
        return $this->db->select()->from($this->_tableActivation)->where($where)->get()->result_array();
    }
    public function sendGiftCard($email,$points,$code) {
        $set = array();
        $set['email'] = $email;
        $set['status'] = "active";
        $set['points'] = $points;
        $set['code'] = $code;
        $this->db->insert($this->_tableActivation, $set);
        
    }
    
    public function getExpiringPoints($days = 5) {
        $points = array();
        $points = $this->db->select()->from($this->_tableName)->like("ends", date("Y-m-d",strtotime("+$days days")))->get()->result_array();
        return $points;
    }
	
	public function getCustomerPointsGrouped($userID) {
		
		$sql = "SELECT customer_id, SUM(remaining) AS remaining,DATE(ends) AS ends FROM points WHERE customer_id = $userID group by DATE(ends)";
		return $this->db->query($sql)->result_array();
	}
        
    public function getExpiringDateOfJustAddedPoints($customerID, $TodayDate){
        $sql = "select ends from points where customer_id = '$customerID' and created LIKE '$TodayDate%'";
        return $this->db->query($sql)->result_array();
    }

}