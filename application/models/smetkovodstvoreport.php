<?php defined('SYSPATH') or die('No direct script access.');

class SmetkovodstvoReport_Model extends Default_Model {

	protected $_tableName = 'smetkovodstvoizvestaj';
	
	public function __construct() {
		parent::__construct();
	}
	
	
	public function getSmetkovodstvoIzvestaj($smetkovodstvoreport_day, $smetkovodstvoreport_month, $smetkovodstvoreport_year, $tip_plakjanje = "card") {
		
		//$previous_day = date('Y-n-j', strtotime('-1 day', strtotime("$smetkovodstvoreport_year-$smetkovodstvoreport_month-$smetkovodstvoreport_day")));
		$today = "$smetkovodstvoreport_year-$smetkovodstvoreport_month-$smetkovodstvoreport_day";
		
		$sql =  "SELECT v.deal_id, p.name AS partner_name, p.ddv_obvrznik, d.title_mk, COUNT(v.id) AS cnt_vouchers, ";
		$sql .= "IF(d.tip = 'cena_na_vaucer',d.price_voucher, d.price_discount) AS deal_cena,IF(d.tip = 'cena_na_vaucer',d.price_voucher, d.price_discount)*COUNT(v.id) AS vkupen_prihod,d.price_voucher AS deal_provizija, d.price_voucher*COUNT(v.id) AS vkupna_provizija ";
		$sql .= "FROM voucher AS v ";
		$sql .= "INNER JOIN deals AS d ON d.id = v.deal_id ";
		$sql .= "INNER JOIN partners AS p ON p.id = d.partner_id ";
		$sql .= "WHERE v.activated = 1 AND v.confirmed = 1 AND v.type = '$tip_plakjanje' ";
		//$sql .= "AND v.time between '$previous_day 19:00:01' and '$today 19:00:00' ";
		$sql .= "AND DATE(v.time) = '$today' ";
		$sql .= "GROUP BY v.deal_id ";
		
		//die($sql);
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
	
	public function getSmetkovodstvoIzvestajMesecen($smetkovodstvoreport_month, $smetkovodstvoreport_year) {
		
		//$theDayBeforeFirstDayOfMonth = date('Y-n-j', strtotime('-1 day', strtotime("$smetkovodstvoreport_year-$smetkovodstvoreport_month-1")));
		$firstDayOfMonth = "$smetkovodstvoreport_year-$smetkovodstvoreport_month-1";
		$numDaysOfMonth = date('t', strtotime("$smetkovodstvoreport_year-$smetkovodstvoreport_month-1"));
		$lastDayOfMonth = "$smetkovodstvoreport_year-$smetkovodstvoreport_month-$numDaysOfMonth";
		
		$sql =  "SELECT GROUP_CONCAT(v.id) AS group_concat_ids, IFNULL(COUNT(v.id),0) AS cnt_vouchers, IFNULL(SUM(IF(d.tip = 'cena_na_vaucer',d.price_voucher, d.price_discount)),0) AS vkupen_prihod, IFNULL(SUM(d.price_voucher),0) AS vkupna_provizija ";
		$sql .= "FROM voucher AS v ";
		$sql .= "INNER JOIN deals AS d ON d.id = v.deal_id ";
		$sql .= "INNER JOIN partners AS p ON p.id = d.partner_id AND p.ddv_obvrznik = 'Ослободен' ";
		$sql .= "WHERE v.activated = 1 AND v.confirmed = 1 AND v.type <> 'cache' ";
		//$sql .= "AND v.time between '$theDayBeforeFirstDayOfMonth 19:00:01' and '$lastDayOfMonth 19:00:00' ";
		$sql .= "AND DATE(v.time) between '$firstDayOfMonth' and '$lastDayOfMonth' ";

		
		//die($sql);
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
}