<?php defined('SYSPATH') or die('No direct script access.');

class Deal_Tags_Model extends Default_Model {

    protected $_tableName = 'deal_tags';

    public function __construct() {
        parent::__construct();
    }

	public function delete_selected_tags($deal_id = 0) 
	{
		$deal_id = (int) $deal_id;

		return $this->db->delete($this->_tableName, array('deal_id' => $deal_id));
	}

	public function saveTag($deal_id = 0, $tag_name = "") 
	{
		$deal_id = (int) $deal_id;
		
		$sqlStm = "REPLACE INTO $this->_tableName (deal_id, name) VALUES ($deal_id, '$tag_name') ";

		// die($sqlStm);
		$retValue = $this->db->query($sqlStm);

		return $retValue;
	}

	 public function getPonudiByTagPaginationCnt($tag_name = "", $dateTimeUnique ){

	     
	     $query_str = "SELECT COUNT(dt.id) AS cnt_deals
			FROM deal_tags AS dt
			INNER JOIN deals AS d ON dt.deal_id = d.id
			WHERE d.visible = 1
			AND d.demo = 0
			AND d.end_time > '".$dateTimeUnique."'
			AND (dt.name = '".mysql_real_escape_string($tag_name, $this->db->return_link())."')
			";


		$cnt_offers = $this->db->query($query_str)->result_array();
		if ($cnt_offers)
			return $cnt_offers[0]->cnt_deals;
	}

	 public function getDealTagsCommaDelimited($deal_id = 0)
	 {
	 	$deal_id = (int) $deal_id;  

	 	$dealTagsArray = $this->getDealTagsData($deal_id);

	 	$dealTagsStr = "";
		
		if ($dealTagsArray)
		{
			// die(print_r($dealTagsArray));

			foreach ($dealTagsArray as $dealTags) 
			{
				if($dealTagsStr != "")
					$dealTagsStr .= ", ";
				
				if($deal_id == 0)
					$dealTagsStr .= "'".$dealTags->name."'";
				else		
					$dealTagsStr .= $dealTags->name;
			}

		}

		// die($dealTagsStr);

		return $dealTagsStr;
	}

	 private function getDealTagsData($deal_id = 0)
	 {
	 	$deal_id = (int) $deal_id;

	 	$dealTagsArray = array();  

	 	if($deal_id > -1) // Vo admin se vadat site DISTINCT tagovi 
	 	{
		 	if($deal_id > 0)
		 		$whereArr = array('deal_id' => $deal_id);
		 	else
		 		$whereArr = array();

	 		$dealTagsArray = $this->db->select("DISTINCT name")->from($this->_tableName)->where($whereArr)->get()->result_array();
	 	}
	 	else // za public se vadat samo onie tagovi sto se povrzani so AKTIVNI ponudi
	 	{
		 	//kreiraj go query string-ot
			$query_str = "SELECT dt.name, COUNT(d.id) AS cnt_deals
						  FROM deal_tags AS dt 
						  INNER JOIN deals AS d ON dt.deal_id = d.id AND d.visible = 1 AND d.demo = 0 AND d.end_time > '".date("Y-m-d H:i:s")."'
						  GROUP BY dt.name
						  ORDER BY dt.name ASC
						";
			$dealTagsArray = $this->db->query($query_str)->result_array();			

	 	}


		return $dealTagsArray;
		
	}


	 public function getDealTagsPublicRender($deal_id = 0)
	 {
	 	$deal_id = (int) $deal_id;  

	 	$dealTagsArray = $this->getDealTagsData($deal_id);

	 	$dealTagsStr = "";
		
		if ($dealTagsArray)
		{
			// die(print_r($dealTagsArray));

			foreach ($dealTagsArray as $dealTags) 
			{
				$dealTagsStr .= '<a href ="/deal/tag/'.$dealTags->name.'" target="_blank" class="tag-custom tag-cloud-link">'.$dealTags->name.' ('.$dealTags->cnt_deals.')</a>'."\r\n";
			}

		}

		// die($dealTagsStr);

		return $dealTagsStr;
	}

	public function clonDealTags($existingDealID = 0, $cloneDealID = 0) 
	{
		if((int)$existingDealID == 0 || (int)$cloneDealID == 0)
			return -1;
		
		//default value
		// $insert_id = 0;

		//gradenje na query
		$sql =  "INSERT INTO $this->_tableName (deal_id,  name) ";
		
		$sql .= "SELECT     ".$cloneDealID." AS deal_id,  name  ";
		$sql .= "FROM $this->_tableName ";
		$sql .= "WHERE deal_id = ".$existingDealID." ";
		// die($sql);
		
		//izvrsuvanje na query
		$result = $this->db->query($sql);

		//ako e uspesno izvrseno query-to da se zeme ID-to na noviot zapis (INSERT_ID )
		// if($result)
		// 	$insert_id = $result->insert_id();
		
		//vrati go nazat INSERT_ID
		return $result;
	}

}