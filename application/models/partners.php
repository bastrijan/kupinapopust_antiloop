<?php defined('SYSPATH') or die('No direct script access.');

class Partners_Model extends Default_Model {

    protected $_tableName = 'partners';

    public function __construct() {
        parent::__construct();
    }
	
	public function getPartners() {
		
		$sql = "SELECT id, name, tip_danocna_sema, ddv_stapka FROM $this->_tableName ORDER BY REPLACE(REPLACE(REPLACE(TRIM(name), '„', ''), '“', ''),'\"','') ASC  ";
		$res = $this->db->query($sql)->result_array();
		
		/*
		$result = array();
		foreach ($res as $value) {
			$result[$value->id] = $value->name;
		}
		*/
		
		//return $result;
		
		return $res;
	}

	public function checkDuplicatePartnerUsername($username = '', $partner_id_check = 0) 
	{
        $partner_id_check = (int)$partner_id_check;

		$sql = "SELECT SUM(id) AS sum_partners FROM $this->_tableName WHERE username = '".$username."' and id <> $partner_id_check ";
		$res = $this->db->query($sql)->result_array();
		
		return $res[0]->sum_partners;
	}



	public function getInfoZaIsplata($partnerId = 0) {

		$partnerId = (int) $partnerId;
            
		$sql = "SELECT
					IFNULL(SUM(IF(d.tip = 'cena_na_vaucer', 0, do.price_discount - do.price_voucher)), 0) AS suma_za_isplata
				FROM deals AS d
				INNER JOIN deal_options AS do ON do.deal_id = d.id
				INNER JOIN voucher AS v ON v.deal_id = do.deal_id AND v.deal_option_id = do.id AND v.used = 1 AND v.paid_off = 0
				WHERE d.partner_id = ".$partnerId." ";
		
		// die($sql);

		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}


	public function getInfoZaPotvrdaNaIsplata($partnerId = 0) {

		$partnerId = (int) $partnerId;
            
		$sql = "SELECT
					IFNULL(SUM(IF(d.tip = 'cena_na_vaucer', 0, do.price_discount - do.price_voucher)), 0) AS suma_za_isplata
				FROM deals AS d
				INNER JOIN deal_options AS do ON do.deal_id = d.id
				INNER JOIN voucher AS v ON v.deal_id = do.deal_id AND v.deal_option_id = do.id AND v.used = 1 AND v.paid_off = 1 AND v.confirm_paid_off = 0
				WHERE d.partner_id = ".$partnerId." ";
		
		// die($sql);

		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}

}