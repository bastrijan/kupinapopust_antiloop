<?php defined('SYSPATH') or die('No direct script access.');

class Amazon_Newsletter_Scheduler_Model extends Default_Model {

	protected $_tableName = 'amazon_newsletter_scheduler';
	
	public function __construct() {
		parent::__construct();
	}
	
	public function getNewsletterToSend($curr_date_time = "") {
		
		if($curr_date_time == "")
			$curr_date_time = date('Y-m-d H:10:00');
		
		//current_status
		//0 - ne e pocnat
		//1 - vo progres
		//2 - zavrsen
		
		$query_str = "SELECT id, name, newsletter_type, send_to
					  FROM $this->_tableName
					  WHERE current_status = 0
				      AND date_time <= '".$curr_date_time."'
				      ORDER BY date_time DESC
					  LIMIT 1
					 ";
		
		
		return $this -> db -> query($query_str)->result_array();
	}
	
	public function getNewsletterSchedulerReport() {
		
		$sql =  "SELECT id, name, newsletter_type, send_to, current_status, date_time ";
		$sql .= "FROM $this->_tableName ";
		$sql .= "ORDER BY date_time ASC ";
		//die($sql);
		
		
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
	
	
	public function delete($id) {
		return $this->db->delete($this->_tableName, array('id' => $id));
	}
	
	public function deleteOldNewsletters($date) {
		return $this->db->delete($this->_tableName, array('date_time <= ' => $date));
	}
}