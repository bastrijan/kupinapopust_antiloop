<?php defined('SYSPATH') or die('No direct script access.');

class Deal_Options_Model extends Default_Model {

    protected $_tableName = 'deal_options';

    public function __construct() {
        parent::__construct();
    }

	public function isThereDefaultOption($dealID) {
            
		$sql = "SELECT COUNT(id) AS count FROM $this->_tableName WHERE deal_id = ".$dealID." AND default_option = 1 ";
		$res = $this->db->query($sql)->result_array();
		
		return $res[0]->count;
	}

	public function getFiktivniVauceriByDeal($dealID) {
            
		$sql = "SELECT SUM(fiktivni_kuponi_num) AS sum_fiktivni_vauceri FROM $this->_tableName WHERE deal_id = ".$dealID." ";
		$res = $this->db->query($sql)->result_array();
		
		return $res[0]->sum_fiktivni_vauceri;
	}

	public function getLastValidToDate($dealID = null) {

		if(!$dealID)
			$dealID = 0;
            
		$sql = "SELECT valid_to FROM $this->_tableName WHERE deal_id = ".$dealID." AND active = 1 ORDER BY valid_to DESC LIMIT 1 ";
		$res = $this->db->query($sql)->result_array();
		
		$returnVal = '2030-12-31 00:00:00';

		if(count($res) > 0 && $res[0]->valid_to != '0000-00-00 00:00:00')
			$returnVal = $res[0]->valid_to;

		return $returnVal;
	}

	public function multiple_valid_to($end_time, $where_in_ids) 
	{
		//izvrsi go query-to
		$status = $this->db->from($this->_tableName)->set(array('valid_to' => $end_time))->in('deal_id', $where_in_ids)->update();
		
		return $rows = count($status);
	}
	

	public function clonDealOptions($existingDealID = 0, $cloneDealID = 0) 
	{
		if((int)$existingDealID == 0 || (int)$cloneDealID == 0)
			return -1;
		
		//default value
		// $insert_id = 0;

		//gradenje na query
		$sql =  "INSERT INTO $this->_tableName (deal_id,  default_option,  active,  title_mk,  title_mk_clean,  content_short_mk,  max_ammount,  price,  price_discount,  price_voucher,  fiktivni_kuponi_num,  instrukcii_za_iskoristuvanje ) ";
		
		$sql .= "SELECT     ".$cloneDealID." AS deal_id,  default_option,  active,  title_mk,  title_mk_clean,  content_short_mk,  max_ammount,  price,  price_discount,  price_voucher,  fiktivni_kuponi_num,  instrukcii_za_iskoristuvanje   ";
		$sql .= "FROM $this->_tableName ";
		$sql .= "WHERE deal_id = ".$existingDealID." ";
		// die($sql);
		
		//izvrsuvanje na query
		$result = $this->db->query($sql);

		//ako e uspesno izvrseno query-to da se zeme ID-to na noviot zapis (INSERT_ID )
		// if($result)
		// 	$insert_id = $result->insert_id();
		
		//vrati go nazat INSERT_ID
		return $result;
	}

}