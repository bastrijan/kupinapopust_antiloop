<?php defined('SYSPATH') or die('No direct script access.');

class General_Offer_Deals_Model extends Default_Model {

    protected $_tableName = 'general_offer_deals';

    public function __construct() {
        parent::__construct();
    }

	public function delete_selected_deals($id) {
		return $this->db->delete($this->_tableName, array('general_offer_id' => $id));
	}

	 public function getPonudiByGeneralOfferPaginationCnt($general_offer_id = 0, $dateTimeUnique ){

	     
	     $query_str = "SELECT COUNT(god.id) AS cnt_deals
			FROM general_offer_deals AS god
			INNER JOIN deals AS d ON d.id = god.deal_id
			WHERE d.visible = 1
			AND d.demo = 0
			AND d.end_time > '".$dateTimeUnique."'
			AND (god.general_offer_id = ".$general_offer_id.")
			";


		$cnt_offers = $this->db->query($query_str)->result_array();
		if ($cnt_offers)
			return $cnt_offers[0]->cnt_deals;
	}

}