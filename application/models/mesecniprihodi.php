﻿<?php defined('SYSPATH') or die('No direct script access.');

class Mesecniprihodi_Model extends Default_Model {

	protected $_tableName = 'monthly_incomes';
	
	public function __construct() {
		parent::__construct();
	}
	
	public function delete($id) {
		return $this->db->delete($this->_tableName, array('id' => $id));
	}
}