<?php

defined('SYSPATH') or die('No direct script access.');

class Deals_Model extends Default_Model {

    protected $_tableName = 'deals';
	protected $_categoryTable ='category';
	protected $_mainOffersSchedulerName = 'main_offers_scheduler';
	protected $_customer = 'customer';
	protected $_current_ts = 0;

    public function __construct() {
        parent::__construct();
		
		$this->_current_ts = time();
    }

	public function setPrimary($id, $reden_broj = 1, $sendMails = false, $main_offers_scheduler_id = 0) {
        
		$unset = array('primary'.$reden_broj => 0);
		
		$where = array('id' => $id);
        $set = array('primary'.$reden_broj => 1, 'visible' => 1);
		
		//zapocni transakcija
		$checkTransaction = true;
		$this->db->query("BEGIN");

		$checkTransaction = $this->db->update($this->_tableName, $unset, 1);
		//$checkTransaction = $this->db->query("UPDATE deals SET `primary` = 0");
		
		if($checkTransaction)
			$checkTransaction = $this->db->update($this->_tableName, $set, $where);
			//$checkTransaction = $this->db->query("UPDATE deals SET `primary` = 1, visible = 1 WHERE id=".$id."");

		//ako postoi ID od main_offers_scheduler togas izbrisi go zapisot
		if($checkTransaction && $main_offers_scheduler_id)
			$checkTransaction = $this->db->delete($this->_mainOffersSchedulerName, array('id' => $main_offers_scheduler_id));

		if($checkTransaction)
		{
			$this->db->query("COMMIT");
			
		}
		else
		{
			$this->db->query("ROLLBACK");
		}
		
		return $checkTransaction;
	}

	public function removePrimary($id, $reden_broj = 2) {
        
		
		$where = array('id' => $id);
        $set = array('primary'.$reden_broj => 0);
		
		$this->db->update($this->_tableName, $set, $where);

	}
	
    public function getPrimary() {

        $deal = $this->db->select()->from($this->_tableName)->where('primary1', 1)->get()->result_array();
        if ($deal)
            return $deal[0];
        return false;
    }



    public function delete($id) {
        return $this->db->delete($this->_tableName, array('id' => $id));
    }
	
	public function getAllCategory() {
        return $this->db->select()->from($this->_categoryTable)
                        ->get()->result_array();
    }
	
	public function getAllCategoriesWithOffers() {
		
		$query_str = "SELECT c.id, c.name, c.icon_html, c.render_bold, c.custom_order, COUNT(d.id) AS cnt_deals
					  FROM category AS c 
					  INNER JOIN deals AS d ON d.category_id = c.id AND d.is_general_deal = 0 AND d.visible = 1 AND d.demo = 0 AND d.end_time > '".date("Y-m-d H:i:s")."'
					  GROUP BY c.id
					  ORDER BY c.custom_order DESC, c.id ASC
					";
		return $this->db->query($query_str)->result_array();
	}

	public function getCntAllActiveOffers() {
		
		$query_str = "SELECT COUNT(d.id) AS cnt_deals
					  FROM deals AS d 
					  WHERE d.visible = 1 AND d.demo = 0 AND d.end_time > '".date("Y-m-d H:i:s")."'
					";
		
		$cnt_offers = $this->db->query($query_str)->result_array();
		if ($cnt_offers)
			return $cnt_offers[0]->cnt_deals;
	}

	public function getOffersOrderBySubCat() {
		$res = $this->db->select()->from($this->_tableName)
									->where('category_id', 0)
									->where('visible', 1)
									->where('demo', 0)
									->where('end_time > ', date("Y-m-d H:i:s"))
									->orderby(array( 'category_id' => 'ASC', 'subcategory_id' => 'ASC', 'start_time' => 'DESC' ))
									->get()->result_array();
		
		$result = array();
		foreach ($res as $value) {
			$result[$value->category_id][$value->subcategory_id][] = $value;
		}
		
		return $result;
	}

    public function getActiveOffers() {
						
        return $this->db->select()->from($this->_tableName)
                        ->where('end_time > ', date("Y-m-d H:i:s"))
                        ->where('visible', 1)
						->where('demo', 0)
                        ->orderby("start_time", "DESC")->get()->result_array();
						

    }

	public function getExpiredOffers($category_id_selected = 0, $vraboten_id = 0, $partner_id_selected = 0, $deal_name_filter = "", $sorting = 0) {

		// ini_set('memory_limit', '1024M'); // or you could use 1G
		
		//zemi go ID-to na logiraniot administrator
		$userID = $this->session->get("user_id");
		
		//inicijaliziraj array za where vo query-to
		$where = array();
		
		//ako ne e Jovica
		//if($userID > 1)
		//	$where["vraboten_id"] = $userID;
		
		//ako e odbrana kategorija	
		if($category_id_selected > 0)
			$where["category_id"] = $category_id_selected;

		//ako e selectiran vraboten
		if($vraboten_id > 0)
			$where["vraboten_id"] = $vraboten_id;
		
		//ako e selectiran partner
		if($partner_id_selected > 0)
			$where["partner_id"] = $partner_id_selected;

		//ako e selectiran IME NA PONUDA
		if($deal_name_filter != "")
			$deal_name_where_like = trim($deal_name_filter);
		else
			$deal_name_where_like = "";

		//die(print_r($where));

		//setiranje na sortiranjeto
		$sort_array = array("created" => "DESC");

		if($sorting == 1)
			$sort_array = array("primary1" => "DESC", "primary2" => "DESC", "primary3" => "DESC", "primary4" => "DESC", "primary5" => "DESC", "created" => "DESC");
		

		
        return $this->db->select()->from($this->_tableName)
						->where($where)
                        ->where('end_time < ', date("Y-m-d H:i:s"))
                        ->where('visible', 1)
						->where('demo', 0)
						->where('is_general_deal', 0)
						->like('title_mk', $deal_name_where_like)
                        ->orderby($sort_array)->get()->result_array();
    }

	public function getCurrentOffers($category_id_selected = 0, $vraboten_id = 0, $partner_id_selected = 0, $deal_name_filter = "", $sorting = 0) {

		//zemi go ID-to na logiraniot administrator
		$userID = $this->session->get("user_id");
		
		//inicijaliziraj array za where vo query-to
		$where = array();
		
		//ako ne e Jovica
		//if($userID > 1)
		//	$where["vraboten_id"] = $userID;
		
		//ako e odbrana kategorija	
		if($category_id_selected > 0)
			$where["category_id"] = $category_id_selected;

		//ako e selectiran vraboten
		if($vraboten_id > 0)
			$where["vraboten_id"] = $vraboten_id;
		
		//ako e selectiran partner
		if($partner_id_selected > 0)
			$where["partner_id"] = $partner_id_selected;

		//ako e selectiran IME NA PONUDA
		if($deal_name_filter != "")
			$deal_name_where_like = trim($deal_name_filter);
		else
			$deal_name_where_like = "";

		//die(print_r($where));

		//setiranje na sortiranjeto
		$sort_array = array("created" => "DESC");

		if($sorting == 1)
			$sort_array = array("primary1" => "DESC", "primary2" => "DESC", "primary3" => "DESC", "primary4" => "DESC", "primary5" => "DESC", "created" => "DESC");
		
		//die(print_r($sort_array));

		//za prikazuvanje na DIE na query ===== get('', NULL, NULL, true)
        return $this->db->select()->from($this->_tableName)
						->where($where)
                        ->where('end_time > ', date("Y-m-d H:i:s"))
                        ->where('visible', 1)
						->where('demo', 0)
						->where('is_general_deal', 0)
						->like('title_mk', $deal_name_where_like)
                        ->orderby($sort_array)->get()->result_array();
	
    }
	

	public function getGeneralOffers($category_id_selected = 0, $vraboten_id = 0, $partner_id_selected = 0, $deal_name_filter = "", $sorting = 0) {

		$category_id_selected = (int)$category_id_selected;
		$vraboten_id = (int)$vraboten_id;
		$partner_id_selected = (int)$partner_id_selected;
		$deal_name_where_like = trim($deal_name_filter);

		$sort_str = "created DESC";
		if($sorting == 1)
			$sort_str = "primary1 DESC, primary2 DESC, primary3 DESC, primary4 DESC, primary5 DESC, created DESC";

		
		$query_str = "SELECT *
					  FROM (".$this->_tableName.")
					  WHERE is_general_deal = 1
					  AND (category_id = '".$category_id_selected."' OR 0 = '".$category_id_selected."')
					  AND (vraboten_id = '".$vraboten_id."' OR 0 = '".$vraboten_id."')
					  AND (partner_id = '".$partner_id_selected."' OR 0 = '".$partner_id_selected."')
					  AND title_mk LIKE '%".$deal_name_where_like."%' 
					  AND (end_time > '".date("Y-m-d H:i:s")."' OR end_time = '0000-00-00 00:00:00')
				      ORDER BY $sort_str";
		
		
		return $this -> db -> query($query_str)->result_array();

    }

	public function getPastGeneralOffers($category_id_selected = 0, $vraboten_id = 0, $partner_id_selected = 0, $deal_name_filter = "", $sorting = 0) {
		
		//zemi go ID-to na logiraniot administrator
		$userID = $this->session->get("user_id");
		
		//inicijaliziraj array za where vo query-to
		$where = array();
		
		//ako e odbrana kategorija	
		if($category_id_selected > 0)
			$where["category_id"] = $category_id_selected;

		//ako e selectiran vraboten
		if($vraboten_id > 0)
			$where["vraboten_id"] = $vraboten_id;
		
		//ako e selectiran partner
		if($partner_id_selected > 0)
			$where["partner_id"] = $partner_id_selected;

		//ako e selectiran IME NA PONUDA
		if($deal_name_filter != "")
			$deal_name_where_like = trim($deal_name_filter);
		else
			$deal_name_where_like = "";

		//die(print_r($where));

		//setiranje na sortiranjeto
		$sort_array = array("created" => "DESC");

		if($sorting == 1)
			$sort_array = array("primary1" => "DESC", "primary2" => "DESC", "primary3" => "DESC", "primary4" => "DESC", "primary5" => "DESC", "created" => "DESC");
		

        return $this->db->select()->from($this->_tableName)
						->where($where)
						->where('is_general_deal', 1)
						->where('end_time <> ', '0000-00-00 00:00:00')
						->where('end_time < ', date("Y-m-d H:i:s"))
						->like('title_mk', $deal_name_where_like)
                        ->orderby($sort_array)->get()->result_array();
	
    }

	public function getDemoOffers($category_id_selected = 0, $vraboten_id = 0, $partner_id_selected = 0, $deal_name_filter = "", $sorting = 0) {
		
		
		//zemi go ID-to na logiraniot administrator
		$userID = $this->session->get("user_id");
		
		//inicijaliziraj array za where vo query-to
		$where = array();
		
		//ako ne e Jovica
		//if($userID > 1)
		//	$where["vraboten_id"] = $userID;
		
		if($category_id_selected > 0)
			$where["category_id"] = $category_id_selected;

		//ako e selectiran vraboten
		if($vraboten_id > 0)
			$where["vraboten_id"] = $vraboten_id;
		
		//ako e selectiran partner
		if($partner_id_selected > 0)
			$where["partner_id"] = $partner_id_selected;

		//ako e selectiran IME NA PONUDA
		if($deal_name_filter != "")
			$deal_name_where_like = trim($deal_name_filter);
		else
			$deal_name_where_like = "";

		//die(print_r($where));

		//setiranje na sortiranjeto
		$sort_array = array("created" => "DESC");

		if($sorting == 1)
			$sort_array = array("primary1" => "DESC", "primary2" => "DESC", "primary3" => "DESC", "primary4" => "DESC", "primary5" => "DESC", "created" => "DESC");
		
		
        return $this->db->select()->from($this->_tableName)
                        ->where($where)
						->where('demo', 1)
						->where('is_general_deal', 0)
						->like('title_mk', $deal_name_where_like)
	                    ->orderby($sort_array)->get()->result_array();
    }

	public function getAllOffers($category_id_selected = 0, $vraboten_id = 0, $partner_id_selected = 0, $deal_name_filter = "", $sorting = 0) {
		
		//zemi go ID-to na logiraniot administrator
		$userID = $this->session->get("user_id");
		
		//inicijaliziraj array za where vo query-to
		$where = array();
		
		//ako ne e Jovica
		//if($userID > 1)
		//	$where["vraboten_id"] = $userID;
		
		//ako e odbrana kategorija	
		if($category_id_selected > 0)
			$where["category_id"] = $category_id_selected;

		//ako e selectiran vraboten
		if($vraboten_id > 0)
			$where["vraboten_id"] = $vraboten_id;
		
		//ako e selectiran partner
		if($partner_id_selected > 0)
			$where["partner_id"] = $partner_id_selected;

		//ako e selectiran IME NA PONUDA
		if($deal_name_filter != "")
			$deal_name_where_like = trim($deal_name_filter);
		else
			$deal_name_where_like = "";

		//die(print_r($where));

		//setiranje na sortiranjeto
		$sort_array = array("created" => "DESC");

		if($sorting == 1)
			$sort_array = array("primary1" => "DESC", "primary2" => "DESC", "primary3" => "DESC", "primary4" => "DESC", "primary5" => "DESC", "created" => "DESC");
		
		
		//za prikazuvanje na DIE na query ===== get('', NULL, NULL, true)
        return $this->db->select()->from($this->_tableName)
                        ->where($where)
                        ->where('is_general_deal', 0)
                        ->like('title_mk', $deal_name_where_like)
                        ->orderby($sort_array)->get()->result_array();	

    }


        
         public function getPonudiByCategoryPagination($category_id = 0, $offset = 0, $sql_limit = 0, $dateTimeUnique )
         {
			
			$where = "d.is_general_deal = 0 AND d.visible = 1 AND d.demo = 0 AND d.end_time > '".$dateTimeUnique."' AND do.active = 1 AND (d.category_id = ".$category_id." OR ".$category_id." = 0)";
	        $sql_limit_str = "LIMIT $offset, $sql_limit"; 
	        $sort_str = "";


             $query_str = "SELECT dwov.*, 
             					  p.name, p.adress_mk, p.terms_mk, p.google_link, p.google_url, 
             					  SUM(dwov.voucher_count) AS voucher_count,
					    	 	  COUNT(dwov.deal_option_id) AS options_cnt
				FROM (".$this->getDealWithOptionsView($where, $sql_limit_str, $sort_str).") AS dwov
				LEFT JOIN partners AS p ON dwov.partner_id = p.id 
			    GROUP BY dwov.deal_id
				ORDER BY dwov.start_time DESC
				$sql_limit_str ";

		
			return $this -> db -> query($query_str)->result_array();
        }

         public function getPonudiByCategoryPaginationCnt($category_id = 0, $dateTimeUnique ){

             
             $query_str = "SELECT COUNT(d.id) AS cnt_deals
				FROM deals AS d
				WHERE d.is_general_deal = 0 
				AND d.visible = 1
				AND d.demo = 0
				AND d.end_time > '".$dateTimeUnique."'
				AND (d.category_id = ".$category_id." OR ".$category_id." = 0)
				";
		
		
			$cnt_offers = $this->db->query($query_str)->result_array();
			if ($cnt_offers)
				return $cnt_offers[0]->cnt_deals;
        }
        
        public function getPonudiBySubCategoryPagination($subcategory_id = 0, $offset = 0, $sql_limit = 0, $dateTimeUnique)
        {

	        $where = "d.is_general_deal = 0 AND d.visible = 1 AND d.demo = 0 AND d.end_time > '".$dateTimeUnique."' AND do.active = 1 AND (d.subcategory_id = ".$subcategory_id." OR ".$subcategory_id." = 0)";
	        $sql_limit_str = "LIMIT $offset, $sql_limit"; 
	        $sort_str = "";
            
            $query_str = "SELECT dwov.*, 
								 p.name, p.adress_mk, p.terms_mk, p.google_link, p.google_url, 
								 SUM(dwov.voucher_count) AS voucher_count,
						    	 COUNT(dwov.deal_option_id) AS options_cnt
					  FROM (".$this->getDealWithOptionsView($where, $sql_limit_str, $sort_str).") AS dwov
					  LEFT JOIN partners AS p ON dwov.partner_id = p.id 
					  GROUP BY dwov.deal_id
					  ORDER BY dwov.start_time DESC
					  $sql_limit_str 
                ";
		
			//die($query_str);
			return $this -> db -> query($query_str)->result_array();
        }

         public function getPonudiBySubCategoryPaginationCnt($subcategory_id = 0, $dateTimeUnique ){

             
             $query_str = "SELECT COUNT(d.id) AS cnt_deals
				FROM deals AS d
				WHERE d.is_general_deal = 0 
				AND d.visible = 1
				AND d.demo = 0
				AND d.end_time > '".$dateTimeUnique."'
				AND (d.subcategory_id = ".$subcategory_id." OR ".$subcategory_id." = 0)
				";
		
		
			$cnt_offers = $this->db->query($query_str)->result_array();
			if ($cnt_offers)
				return $cnt_offers[0]->cnt_deals;
        }


         public function getPonudiByGeneralOfferPagination($general_offer_id = 0, $offset = 0, $sql_limit = 0, $dateTimeUnique )
         {

         	if( (int)$general_offer_id == 0)
         		exit;

         	//najdi gi id-ata od ponudite koi se staveni pod generalnata ponuda
		    $query_str = "SELECT god.deal_id
							FROM general_offer_deals AS god
							INNER JOIN deals AS d ON d.id = god.deal_id
							WHERE d.visible = 1
							AND d.demo = 0
							AND d.end_time > '".$dateTimeUnique."'
							AND (god.general_offer_id = ".$general_offer_id.")
							";


			$deal_ids_arr = $this->db->query($query_str)->result_array();

			$deal_ids_str = "0";

			// die(print_r($deal_ids_arr));

			foreach ($deal_ids_arr as $deal_id_obj) 
			{
				$deal_ids_str .= ", ".$deal_id_obj->deal_id;					
			}

			// die($deal_ids_str);

	        $where = "d.visible = 1 AND d.demo = 0 AND d.end_time > '".$dateTimeUnique."' AND do.active = 1 AND (d.id IN (".$deal_ids_str.") )";
	        $sql_limit_str = "LIMIT $offset, $sql_limit"; 
	        $sort_str = "";

             
             $query_str = "SELECT dwov.*, 
             					  p.name, p.adress_mk, p.terms_mk, p.google_link, p.google_url, 
             					  SUM(dwov.voucher_count) AS voucher_count,
					    	 	  COUNT(dwov.deal_option_id) AS options_cnt
				FROM (".$this->getDealWithOptionsView($where, $sql_limit_str, $sort_str).") AS dwov
				LEFT JOIN partners AS p ON dwov.partner_id = p.id 
                GROUP BY dwov.deal_id
				ORDER BY dwov.start_time DESC
				$sql_limit_str ";

			// die($query_str);
		
		
			return $this -> db -> query($query_str)->result_array();
        }


         public function getPonudiByTagPagination($tag_name = "", $offset = 0, $sql_limit = 0, $dateTimeUnique )
         {

         	if($tag_name == "")
         		exit;

         	//najdi gi id-ata od ponudite koi se staveni pod generalnata ponuda
		    $query_str = "SELECT dt.deal_id
							FROM deal_tags AS dt
							INNER JOIN deals AS d ON dt.deal_id = d.id
							WHERE d.visible = 1
							AND d.demo = 0
							AND d.end_time > '".$dateTimeUnique."'
							AND (dt.name = '".mysql_real_escape_string($tag_name, $this->db->return_link())."')
							";


			$deal_ids_arr = $this->db->query($query_str)->result_array();

			$deal_ids_str = "0";

			// die(print_r($deal_ids_arr));

			foreach ($deal_ids_arr as $deal_id_obj) 
			{
				$deal_ids_str .= ", ".$deal_id_obj->deal_id;					
			}

			// die($deal_ids_str);

	        $where = "d.visible = 1 AND d.demo = 0 AND d.end_time > '".$dateTimeUnique."' AND do.active = 1 AND (d.id IN (".$deal_ids_str.") )";
	        $sql_limit_str = "LIMIT $offset, $sql_limit"; 
	        $sort_str = "";

             
             $query_str = "SELECT dwov.*, 
             					  p.name, p.adress_mk, p.terms_mk, p.google_link, p.google_url, 
             					  SUM(dwov.voucher_count) AS voucher_count,
					    	 	  COUNT(dwov.deal_option_id) AS options_cnt
				FROM (".$this->getDealWithOptionsView($where, $sql_limit_str, $sort_str).") AS dwov
				LEFT JOIN partners AS p ON dwov.partner_id = p.id 
                GROUP BY dwov.deal_id
				ORDER BY dwov.start_time DESC
				$sql_limit_str ";

			// die($query_str);
		
		
			return $this -> db -> query($query_str)->result_array();
        }


	
	public function getSitePonudi($mode = 0, $limitRecords = 0) {
		
        //mode = 0 (odnosno koga ne se prosleduva mode) => gi dava SITE ponudi
        //mode = 1 => gi dava NAJNOVITE ponudi
        //mode = 2 => gi dava NAJPRODAVANITE ponudi
        //mode = 3 => gi dava ponudite koi se PRI KRAJ
        //mode = 4 => gi dava ponudite po NAJNISKA CENA
        //mode = 5 => gi dava ponudite po NaJVISOKA CENA
        //mode = 6 => gi dava ponudite po BESPLATNI KUPONI
        //mode = 7 => gi dava ponudite po KUPON + DOPLATA
        //mode = 8 => gi dava ponudite po VAUCERI

		//limitRecords = 0 => bez limit
		
		
		if($mode == 0)//mode = 0 (odnosno koga ne se prosleduva mode) => gi dava SITE ponudi
		{
			$filterstr = "";
			$orderstr = "d.start_time DESC";
			$limitStr = "";
		}
		elseif($mode == 1)//mode = 1 => gi dava NAJNOVITE ponudi
		{
			$filterstr = " AND NOT(".$this->_current_ts." > UNIX_TIMESTAMP(d.start_time) + 345600)";
			$orderstr = "d.start_time DESC";
			$limitStr = "";
		}
		elseif($mode == 2)//mode = 2 => gi dava NAJPRODAVANITE ponudi
		{
			$filterstr = "";
			$orderstr = "voucher_count DESC";
			$limitStr = "";
		}
		elseif($mode == 3)//mode = 3 => gi dava ponudite koi se PRI KRAJ
		{
			$filterstr = " AND (".$this->_current_ts." > UNIX_TIMESTAMP(d.end_time) - 86400)";
			$orderstr = "d.start_time DESC";
			$limitStr = "";
		}
		elseif($mode == 4)//mode = 4 => gi dava ponudite po NAJNISKA CENA
		{
			$filterstr = "";
			$orderstr = "finalna_cena ASC";
			$limitStr = "";
		}
		elseif($mode == 5)//mode = 5 => gi dava ponudite po NAJVISOKA CENA
		{
			$filterstr = "";
			$orderstr = "finalna_cena DESC";
			$limitStr = "";
		}
		elseif($mode == 6)//mode = 6 => gi dava ponudite po BESPLATNI KUPONI
		{
			$filterstr = "";
			$orderstr = "finalna_cena DESC";
			$limitStr = "";
		}
		elseif($mode == 7)//mode = 7 => gi dava ponudite po KUPON + DOPLATA
		{
			$filterstr = "";
			$orderstr = "finalna_cena DESC";
			$limitStr = "";
		}
		elseif($mode == 8)//mode = 8 => gi dava ponudite po VAUCERI
		{
			$filterstr = "";
			$orderstr = "finalna_cena DESC";
			$limitStr = "";
		}

		///OVA SE KORISTI KAJ NEWSLETTERITE (newsletter_bestsellers)
		if($limitRecords > 0)
			$limitStr = "LIMIT $limitRecords ";

		
        $where = "d.visible = 1 AND d.demo = 0 AND d.end_time > '".date("Y-m-d H:i:s")."' $filterstr AND do.active = 1";
        $sql_limit_str = "$limitStr"; 
        $sort_str = "$orderstr,";

		$query_str = "SELECT dwov.*, 
							 p.name, p.adress_mk, p.terms_mk, p.google_link, p.google_url, 
							 SUM(dwov.voucher_count) AS voucher_count,
					    	 COUNT(dwov.deal_option_id) AS options_cnt
					  FROM (".$this->getDealWithOptionsView($where, $sql_limit_str, "").") AS dwov
					  LEFT JOIN partners AS p ON dwov.partner_id = p.id 
					  LEFT JOIN general_offer_deals AS god ON god.deal_id = dwov.deal_id 
					  GROUP BY dwov.deal_id
					  HAVING COUNT(god.id) = 0
				      ORDER BY ".str_replace ("d.", "dwov.", $orderstr)."
				      $sql_limit_str ";
		
		//die($query_str);
		return $this -> db -> query($query_str)->result_array();
	}
	
	
	public function getFullOfersInfo()
	{
		$where = "(d.primary1 = 1 OR d.primary2 = 1 OR d.primary3 = 1 OR d.primary4 = 1 OR d.primary5 = 1) AND d.demo = 0 and d.end_time > '".date("Y-m-d H:i:s")."' AND do.active = 1";
		$sql_limit_str = ""; 
        $sort_str = "";

		$query_str = "SELECT dwov.*, 
							p.name, p.adress_mk, p.terms_mk, p.google_link, p.google_url, 
							SUM(dwov.voucher_count) AS voucher_count,
					  		COUNT(dwov.deal_option_id) AS options_cnt
					  FROM (".$this->getDealWithOptionsView($where, $sql_limit_str, $sort_str).") AS dwov
					  LEFT JOIN partners AS p ON dwov.partner_id = p.id
					  LEFT JOIN general_offer_deals AS god ON god.deal_id = dwov.deal_id 
					  GROUP BY dwov.deal_id
					  HAVING COUNT(god.id) = 0
					  ORDER BY dwov.primary1 DESC, dwov.primary2 DESC, dwov.primary3 DESC, dwov.primary4 DESC, dwov.primary5 DESC	
					  ";
		
		//die($query_str);
		return $this -> db -> query($query_str)->result_array();
	}
	
	public function getSitePominatiPonudi($order = -1, $sql_limit) {
		
		//najprodavani DEFAULT
		$orderstr = "d.end_time";
		
		//najnovi
		if($order == 0){
			$orderstr = "voucher_count";
		}
		
        $where = "d.visible = 1 AND d.demo = 0 AND d.end_time < '".date("Y-m-d H:i:s")."' AND do.active = 1";
        $sql_limit_str = "$sql_limit"; 
        $sort_str = "$orderstr DESC,";

		$query_str = "SELECT dwov.*, 
							 p.name, p.adress_mk, p.terms_mk, p.google_link, p.google_url, 
							 SUM(dwov.voucher_count) AS voucher_count,
					    	 COUNT(dwov.deal_option_id) AS options_cnt
					  FROM (".$this->getDealWithOptionsView($where, $sql_limit_str, $sort_str).") AS dwov
					  LEFT JOIN partners AS p ON dwov.partner_id = p.id 
					  LEFT JOIN general_offer_deals AS god ON god.deal_id = dwov.deal_id 
					  GROUP BY dwov.deal_id
					  HAVING COUNT(god.id) = 0
				      ORDER BY ".str_replace ("d.", "dwov.", $orderstr)." DESC
				      $sql_limit_str ";
		return $this -> db -> query($query_str)->result_array();
		
	}
	
	public function getCntSitePominatiPonudi() {
		
		$query_str = "SELECT COUNT(d.id) AS cnt_deals
					  FROM deals AS d
					  WHERE d.visible = 1
					  AND d.demo = 0
				      AND d.end_time < '".date("Y-m-d H:i:s")."'";
		
		
		$cnt_offers = $this->db->query($query_str)->result_array();
		if ($cnt_offers)
			return $cnt_offers[0]->cnt_deals;
		
	}

        
    public function getBottomOffersFullInfoPagination($offset = 0, $sql_limit = 0, $dateTimeUnique)
	{
            
        $where = "(d.primary1 = 0 AND d.primary2 = 0 AND d.primary3 = 0 AND d.primary4 = 0 AND d.primary5 = 0) AND d.demo = 0 AND d.end_time > '".$dateTimeUnique."' AND do.active = 1";
        $sql_limit_str = "LIMIT $offset,$sql_limit"; 
        $sort_str = "";

		$query_str = "SELECT dwov.*, 
							 p.name, p.adress_mk, p.terms_mk, p.google_link, p.google_url, 
							 SUM(dwov.voucher_count) AS voucher_count,
					    	 COUNT(dwov.deal_option_id) AS options_cnt
					  FROM (".$this->getDealWithOptionsView($where, $sql_limit_str, $sort_str).") AS dwov
					  LEFT JOIN partners AS p ON dwov.partner_id = p.id 
					  LEFT JOIN general_offer_deals AS god ON god.deal_id = dwov.deal_id 
					  GROUP BY dwov.deal_id
					  HAVING COUNT(god.id) = 0
                      ORDER BY dwov.start_time DESC 
                      $sql_limit_str ";
		
		//die($query_str);
		return $this->db->query($query_str)->result_array();
		
	}
	
	public function getBottomOffersFullInfoPaginationCnt($dateTimeUnique)
	{
		$query_str = "SELECT COUNT(d.id) AS cnt_deals
					  FROM deals AS d
					  WHERE (d.primary1 = 0 AND d.primary2 = 0 AND d.primary3 = 0 AND d.primary4 = 0 AND d.primary5 = 0) AND d.demo = 0 AND d.end_time > '".$dateTimeUnique."' ";
		
		$cnt_offers = $this->db->query($query_str)->result_array();
		if ($cnt_offers)
			return $cnt_offers[0]->cnt_deals;
		
	}

	
	//////////
	public function getBottomOffersFullInfoSinglePagination($primaryDeal, $offset = 0, $sql_limit = 0, $dateTimeUnique)
	{

        $where = "d.id <> ".$primaryDeal." AND d.demo = 0 AND d.end_time > '".$dateTimeUnique."' AND do.active = 1";
        $sql_limit_str = "LIMIT $offset,$sql_limit"; 
        $sort_str = "";
                        
		$query_str = "SELECT dwov.*, 
							 p.name, p.adress_mk, p.terms_mk, p.google_link, p.google_url, 
							 SUM(dwov.voucher_count) AS voucher_count,
					    	 COUNT(dwov.deal_option_id) AS options_cnt
					  FROM (".$this->getDealWithOptionsView($where, $sql_limit_str, $sort_str).") AS dwov
					  LEFT JOIN partners AS p ON dwov.partner_id = p.id
					  LEFT JOIN general_offer_deals AS god ON god.deal_id = dwov.deal_id 
					  GROUP BY dwov.deal_id
					  HAVING COUNT(god.id) = 0
                      ORDER BY dwov.start_time DESC 
                      $sql_limit_str ";
		
		//die($query_str);
		return $this->db->query($query_str)->result_array();
		
	}
	
	public function getBottomOffersFullInfoSinglePaginationCnt($primaryDeal, $dateTimeUnique)
	{
		$query_str = "SELECT COUNT(d.id) AS cnt_deals
					  FROM deals AS d
					  WHERE d.id <> ".$primaryDeal." AND d.demo = 0 AND d.end_time > '".$dateTimeUnique."' ";
		
		$cnt_offers = $this->db->query($query_str)->result_array();
		if ($cnt_offers)
			return $cnt_offers[0]->cnt_deals;
		
	}
	//////////
	
	
	
	public function getSearchResults($sk = "", $controller_name = "index", $sql_limit)
	{
		$result = array();
		
		$sk = trim($sk);
		
		if($sk != "")
		{
			$parts = explode(" ", $sk);
			$clauses=array();
			foreach ($parts as $part){
				//function_description in my case ,  replace it with whatever u want in ur table
				$clauses[]="d.title_mk LIKE '%" . $this -> db ->escape_str($part) . "%'";
				$clauses[]="do.title_mk LIKE '%" . $this -> db ->escape_str($part) . "%'";
			}
			$clause=implode(' OR ' ,$clauses);
			
			//filter po koi ponudi da prebaruva
			$deals_filter = " d.end_time > '".date("Y-m-d H:i:s")."' ";
			if($controller_name == "past")
				$deals_filter = " d.end_time < '".date("Y-m-d H:i:s")."' ";
			

	        $where = "d.demo = 0 AND $deals_filter AND do.active = 1 AND ($clause)";
	        $sql_limit_str = "$sql_limit"; 
	        $sort_str = "";

			$query_str = "SELECT dwov.*, 
								 p.name, p.adress_mk, p.terms_mk, p.google_link, p.google_url, 
								 SUM(dwov.voucher_count) AS voucher_count,
						    	 COUNT(dwov.deal_option_id) AS options_cnt
						  FROM (".$this->getDealWithOptionsView($where, $sql_limit_str, $sort_str).") AS dwov
						  LEFT JOIN partners AS p ON dwov.partner_id = p.id 
	                      GROUP BY dwov.deal_id
				      	  ORDER BY dwov.start_time DESC
				      	  $sql_limit_str ";
			//die($query_str);
			$result = $this -> db -> query($query_str)->result_array();
		}
		
		return $result;
		
	}
        
        public function getSearchResultsPagination($sk = "", $controller_name = "index", $offset = 0, $sql_limit = 0, $dateTimeUnique )
	{
		$result = array();
		
		$sk = trim($sk);
		
		if($sk != "")
		{
			$parts = explode(" ", $sk);
			$clauses=array();
			foreach ($parts as $part){
				//function_description in my case ,  replace it with whatever u want in ur table
				$clauses[]="d.title_mk LIKE '%" . $this -> db ->escape_str($part) . "%'";
			}
			$clause=implode(' OR ' ,$clauses);
			
			//filter po koi ponudi da prebaruva
			$deals_filter = " d.end_time > '".$dateTimeUnique."' ";
		
			
			$query_str = "SELECT d.*, 
						  (COUNT(v.id) + SUM(do.fiktivni_kuponi_num)) AS voucher_count
							FROM deals AS d
							LEFT JOIN deal_options AS do ON do.deal_id = d.id
							LEFT JOIN voucher AS v ON v.deal_id = d.id  AND v.confirmed = 1
							WHERE d.demo = 0 AND $deals_filter
							AND ($clause)
							GROUP BY d.id 
							ORDER BY d.start_time DESC
							LIMIT $offset,$sql_limit";
//                        if($offset>10)
//			die($query_str);
                        
			$result = $this -> db -> query($query_str)->result_array();
		}
		
		return $result;
		
	}	
	
	public function getCntSearchResults($sk = "", $controller_name = "index")
	{
		$result = 0;
		
		$sk = trim($sk);
		
		if($sk != "")
		{
			$parts = explode(" ", $sk);
			$clauses=array();
			foreach ($parts as $part){
				//function_description in my case ,  replace it with whatever u want in ur table
				$clauses[]="d.title_mk LIKE '%" . $this -> db ->escape_str($part) . "%'";
			}
			$clause=implode(' OR ' ,$clauses);
			
			//filter po koi ponudi da prebaruva
			$deals_filter = " d.end_time > '".date("Y-m-d H:i:s")."' ";
			if($controller_name == "past")
				$deals_filter = " d.end_time < '".date("Y-m-d H:i:s")."' ";
			
			$query_str = "SELECT COUNT(d.id) AS cnt_deals
					FROM deals AS d
					WHERE d.demo = 0 AND $deals_filter
					AND ($clause)
					";
			//die($query_str);
			$cnt_offers = $this->db->query($query_str)->result_array();
			if ($cnt_offers)
				$result = $cnt_offers[0]->cnt_deals;
		}
		
		return $result;
		
	}	
	
	public function multiple_end_time($end_time, $where_in_ids) 
	{
		//izvrsi go query-to
		$status = $this->db->from($this->_tableName)->set(array('end_time' => $end_time))->in('id', $where_in_ids)->update();
		
		return $rows = count($status);
	}
	
	public function getMainOffersScheduled($curr_date_time) {
		
		$deal = $this->db->select()->from($this->_mainOffersSchedulerName)->where(array('date_time <= ' => $curr_date_time))->orderby('date_time', 'ASC')->get()->result_array();
		
		if ($deal)
			return $deal;
		return false;
	}
	
	
	public function setMainOffers($curr_date_time) {
		
		$ret_val = true;
		
		//proveri dali ima glavni ponudi za setiranje
		$result = $this->getMainOffersScheduled($curr_date_time);
		
		//ako ima togas povikaj soodvetni fukcii
		if($result)	
		{
			//za sekoj zapis
			foreach($result as $record)
			{
				//setiraj gi novite glavni ponudi
				$ret_val = $this->setPrimary($record->deal_id, $record->main_offer, false, $record->id);
				
			}
		}
		else
			$ret_val = false;
		
		//vrati nazad boolean rezultat
		return $ret_val;
		
	}
	
	public function getMainOffersSchedulerReport($main_offer = 0) {
		
		$sql =  "SELECT s.id, s.deal_id, d.title_mk AS deal_title_mk, s.main_offer, s.date_time ";
		$sql .= "FROM $this->_mainOffersSchedulerName AS s ";
		$sql .= "LEFT JOIN $this->_tableName AS d ON s.deal_id = d.id ";
		$sql .= "WHERE (s.main_offer = $main_offer OR 0 = $main_offer) ";
		$sql .= "ORDER BY s.date_time ASC ";
		//die($sql);
		

		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
	
	public function saveMainOffersScheduler($post) {
		
		$sql =  "INSERT INTO $this->_mainOffersSchedulerName SET ";
		$sql .= "deal_id = ".$post["deal_id"].", ";
		$sql .= "main_offer = ".$post["main_offer"].", ";
		$sql .= "date_time = '".$post["date_time"]."'";
		//die($sql);
		
		
		$res = $this->db->query($sql);
		
		return $res;
	}
	
	public function checkExistMainOffersScheduler($post) {
		
		$sql =  "SELECT * FROM $this->_mainOffersSchedulerName WHERE ";
		$sql .= "main_offer = ".$post["main_offer"]." AND ";
		$sql .= "date_time = '".$post["date_time"]."'";
		//die($sql);
		
		
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
	
	public function delete_main_offer_schedule($id) {
		return $this->db->delete($this->_mainOffersSchedulerName, array('id' => $id));
	}
	
	public function getOffersByCat($category_id = 0) 
	{
        $where = "d.is_general_deal = 0 AND d.visible = 1 AND d.demo = 0 AND d.end_time > '".date("Y-m-d H:i:s")."' AND do.active = 1 ";
        
        if((int)$category_id > 0)
			$where .= "AND (d.category_id = ".$category_id.")";


        $sql_limit_str = ""; 
        $sort_str = "";

		$query_str = "SELECT dwov.*, 
							 p.name, p.adress_mk, p.terms_mk, p.google_link, p.google_url, 
							 SUM(dwov.voucher_count) AS voucher_count,
					    	 COUNT(dwov.deal_option_id) AS options_cnt
					  FROM (".$this->getDealWithOptionsView($where, $sql_limit_str, $sort_str).") AS dwov
					  LEFT JOIN partners AS p ON dwov.partner_id = p.id 
					  GROUP BY dwov.deal_id
				      ORDER BY dwov.category_id ASC, dwov.subcategory_id ASC, dwov.start_time DESC";

		return $this -> db -> query($query_str)->result_array();
	}
        
    public function getFavouriteOffers($deal_ids)
	{	
		if ( ! empty($deal_ids) && is_array($deal_ids))
		{
			$result = array();
			
			foreach ($deal_ids as $deal_id)
			{

		        $where = "d.demo = 0 AND do.active = 1 AND d.id = $deal_id";
		        $sql_limit_str = ""; 
		        $sort_str = "";

				$query_str = "SELECT dwov.*, 
									 p.name, p.adress_mk, p.terms_mk, p.google_link, p.google_url, 
									 SUM(dwov.voucher_count) AS voucher_count,
							    	 COUNT(dwov.deal_option_id) AS options_cnt
							  FROM (".$this->getDealWithOptionsView($where, $sql_limit_str, $sort_str).") AS dwov
							  LEFT JOIN partners AS p ON dwov.partner_id = p.id 
		                      GROUP BY dwov.deal_id";

				$res = $this->db->query($query_str)->result_array();
				
				if ( ! empty($res) && count($res) > 0)
				{
					$result[] = $res[0];
				}
			}
			return $result;
		}
		else
		{
			return NULL;
		}
	}

	/////////////////////////ZA SITE OPCII OD ALL-CONTROLLEROT
	public function getAllController($mode = 0, $offset = 0, $sql_limit = 0, $dateTimeUnique) 
	{
        //filter po koi ponudi da prebaruva
        $deals_filter = " d.end_time > '".$dateTimeUnique."' ";
		
        //mode = 0 (odnosno koga ne se prosleduva mode) => gi dava SITE ponudi
        //mode = 1 => gi dava NAJNOVITE ponudi
        //mode = 2 => gi dava NAJPRODAVANITE ponudi
        //mode = 3 => gi dava ponudite koi se PRI KRAJ
        //mode = 4 => gi dava ponudite po NAJNISKA CENA
        //mode = 5 => gi dava ponudite po NAJVISOKA CENA
        //mode = 6 => gi dava ponudite po BESPLATNI KUPONI
        //mode = 7 => gi dava ponudite po KUPON + DOPLATA
        //mode = 8 => gi dava ponudite po VAUCERI

		//limitRecords = 0 => bez limit
		
		
		if($mode == 0)//mode = 0 (odnosno koga ne se prosleduva mode) => gi dava SITE ponudi
		{
			$filterstr = "";
			$orderstr = "d.start_time DESC";

		}
		elseif($mode == 1)//mode = 1 => gi dava NAJNOVITE ponudi
		{
			$filterstr = " AND NOT(".$this->_current_ts." > UNIX_TIMESTAMP(d.start_time) + 345600)";
			$orderstr = "d.start_time DESC";

		}
		elseif($mode == 2)//mode = 2 => gi dava NAJPRODAVANITE ponudi
		{
			$filterstr = "";
			$orderstr = "voucher_count DESC";

		}
		elseif($mode == 3)//mode = 3 => gi dava ponudite koi se PRI KRAJ
		{
			$filterstr = " AND (".$this->_current_ts." > UNIX_TIMESTAMP(d.end_time) - 86400)";
			$orderstr = "d.start_time DESC";

		}
		elseif($mode == 4)//mode = 4 => gi dava ponudite po NAJNISKA CENA
		{
			$filterstr = "";
			$orderstr = "finalna_cena ASC";
	
		}
		elseif($mode == 5)//mode = 5 => gi dava ponudite po NAJVISOKA CENA
		{
			$filterstr = "";
			$orderstr = "finalna_cena DESC";

		}
		elseif($mode == 6)//mode = 6 => gi dava ponudite po Бесплатни купони
		{
			$filterstr = " AND d.tip = 'cena_na_vaucer' AND do.price_voucher = 0";
			$orderstr = "d.start_time DESC";

		}
		elseif($mode == 7)//mode = 7 => gi dava ponudite po Купон + доплата
		{
			$filterstr = " AND d.tip = 'cena_na_vaucer' AND do.price_voucher > 0";
			$orderstr = "d.start_time DESC";

		}
		elseif($mode == 8)//mode = 8 => gi dava ponudite po Ваучери
		{
			$filterstr = " AND d.tip = 'cela_cena'";
			$orderstr = "d.start_time DESC";

		}

        $where = "d.visible = 1 AND d.demo = 0 AND do.active = 1 AND $deals_filter $filterstr";
        $sql_limit_str = "LIMIT $offset,$sql_limit"; 
        $sort_str = "$orderstr,";

		$query_str = "SELECT dwov.*, 
							 p.name, p.adress_mk, p.terms_mk, p.google_link, p.google_url, 
							 SUM(dwov.voucher_count) AS voucher_count,
					    	 COUNT(dwov.deal_option_id) AS options_cnt
					  FROM (".$this->getDealWithOptionsView($where, $sql_limit_str, "").") AS dwov
					  LEFT JOIN partners AS p ON dwov.partner_id = p.id 
  					  LEFT JOIN general_offer_deals AS god ON god.deal_id = dwov.deal_id 
					  GROUP BY dwov.deal_id
					  HAVING COUNT(god.id) = 0
				      ORDER BY ".str_replace ("d.", "dwov.", $orderstr)."
				      $sql_limit_str ";
		
		//die($query_str);
		return $this -> db -> query($query_str)->result_array();
	}

	public function getAllControllerCnt($mode = 0, $dateTimeUnique)
	{
		//mode = 0 (odnosno koga ne se prosleduva mode) => gi dava SITE ponudi
        //mode = 1 => gi dava NAJNOVITE ponudi
        //mode = 2 => gi dava NAJPRODAVANITE ponudi
        //mode = 3 => gi dava ponudite koi se PRI KRAJ
        //mode = 4 => gi dava ponudite po NAJNISKA CENA
        //mode = 5 => gi dava ponudite po POJVISOKA CENA
		//mode = 6 => gi dava ponudite po BESPLATNI KUPONI
        //mode = 7 => gi dava ponudite po KUPON + DOPLATA
        //mode = 8 => gi dava ponudite po VAUCERI
		
		
		if($mode == 0)//mode = 0 (odnosno koga ne se prosleduva mode) => gi dava SITE ponudi
		{
			$filterstr = "";
		}
		elseif($mode == 1)//mode = 1 => gi dava NAJNOVITE ponudi
		{
			$filterstr = " AND NOT(".$this->_current_ts." > UNIX_TIMESTAMP(d.start_time) + 345600)";
		}
		elseif($mode == 2)//mode = 2 => gi dava NAJPRODAVANITE ponudi
		{
			$filterstr = "";
		}
		elseif($mode == 3)//mode = 3 => gi dava ponudite koi se PRI KRAJ
		{
			$filterstr = " AND (".$this->_current_ts." > UNIX_TIMESTAMP(d.end_time) - 86400)";
		}
		elseif($mode == 4)//mode = 4 => gi dava ponudite po NAJNISKA CENA
		{
			$filterstr = "";
		}
		elseif($mode == 5)//mode = 5 => gi dava ponudite po POJVISOKA CENA
		{
			$filterstr = "";
		}
		elseif($mode == 6)//mode = 6 => gi dava ponudite po Бесплатни купони
		{
			$filterstr = " AND d.tip = 'cena_na_vaucer' AND do.price_voucher = 0";
		}
		elseif($mode == 7)//mode = 7 => gi dava ponudite po Купон + доплата
		{
			$filterstr = " AND d.tip = 'cena_na_vaucer' AND do.price_voucher > 0";
		}
		elseif($mode == 8)//mode = 8 => gi dava ponudite po Ваучери
		{
			$filterstr = " AND d.tip = 'cela_cena'";
		}

		//staro query
		// $query_str = "SELECT COUNT(d.id) AS cnt_deals
		// 			  FROM deals AS d
		// 			  WHERE d.visible = 1
		// 			  AND d.demo = 0 
		// 		      $filterstr
		// 			  AND d.end_time > '".$dateTimeUnique."' ";
		
					  
		$query_str = "SELECT COUNT(DISTINCT d.id) AS cnt_deals
					  FROM deals AS d
					  INNER JOIN deal_options AS do ON do.deal_id = d.id
					  WHERE d.visible = 1
					  AND d.demo = 0 
				      $filterstr
					  AND d.end_time > '".$dateTimeUnique."' ";
		
		// die($query_str);
		$cnt_offers = $this->db->query($query_str)->result_array();
		if ($cnt_offers)
			return $cnt_offers[0]->cnt_deals;
		
	}


	public function getDealOptionsData($where = "", $sql_limit_str = "", $sort_str = "")
	{
		$query_str = $this->getDealWithOptionsView($where, $sql_limit_str, $sort_str);
		return $this -> db -> query($query_str)->result_array();
	}

	public function getAllOffersPartner($category_id_selected = 0, $partner_id_selected = 0) {

		
		// //inicijaliziraj array za where vo query-to
		// $where = array();

		
		// //ako e odbrana kategorija	
		// if($category_id_selected > 0)
		// 	$where["category_id"] = $category_id_selected;
			
		// if($partner_id_selected > 0)
		// 	$where["partner_id"] = $partner_id_selected;
			
		// //die(print_r($where));
		

		//    return $this->db->select()->from($this->_tableName)
		//                   ->where($where)
		//                   ->orderby(array( "created" => "DESC", 'deal_id' => 'ASC', "finalna_cena" => 'ASC'))->get()->result_array();	

		$where = "(d.category_id = $category_id_selected OR 0 = $category_id_selected) AND (d.partner_id = $partner_id_selected OR 0 = $partner_id_selected)";
		$sql_limit_str = ""; 
        $sort_str = "d.created DESC,";

		$query_str = $this->getDealWithOptionsView($where, $sql_limit_str, $sort_str);
		 
		
		return $this -> db -> query($query_str)->result_array();
    }


	public function getCurrentOffersPartner($category_id_selected = 0, $partner_id_selected = 0) {

		
		//inicijaliziraj array za where vo query-to
		$where = array();

		
		// //ako e odbrana kategorija	
		// if($category_id_selected > 0)
		// 	$where["category_id"] = $category_id_selected;

		// if($partner_id_selected > 0)
		// 	$where["partner_id"] = $partner_id_selected;
		
		// //die(print_r($where));

  //       return $this->db->select()->from($this->_tableName)
		// 				->where($where)
  //                       ->where('end_time > ', date("Y-m-d H:i:s"))
  //                       ->where('visible', 1)
		// 				->where('demo', 0)
  //                       ->orderby(array('deal_id' => 'DESC', 'active' => 'DESC', 'disabled_sort_flag' => 'ASC', "finalna_cena" => 'ASC'))->get()->result_array();
	
		$where = "(d.category_id = $category_id_selected OR 0 = $category_id_selected) AND (d.partner_id = $partner_id_selected OR 0 = $partner_id_selected) AND d.end_time > '".date("Y-m-d H:i:s")."' AND d.visible = 1 AND d.demo = 0 ";
		$sql_limit_str = ""; 
        $sort_str = "d.created DESC,";

		$query_str = $this->getDealWithOptionsView($where, $sql_limit_str, $sort_str);
		 
		//die($query_str);
		return $this -> db -> query($query_str)->result_array();
    }

	public function getExpiredOffersPartner($category_id_selected = 0, $partner_id_selected = 0) {

		// //inicijaliziraj array za where vo query-to
		// $where = array();

		// //ako e odbrana kategorija	
		// if($category_id_selected > 0)
		// 	$where["category_id"] = $category_id_selected;
		
		// if($partner_id_selected > 0)
		// 	$where["partner_id"] = $partner_id_selected;
		// //die(print_r($where));
		
  //       return $this->db->select()->from($this->_tableName)
		// 				->where($where)
  //                       ->where('end_time < ', date("Y-m-d H:i:s"))
  //                       ->where('visible', 1)
		// 				->where('demo', 0)
  //                       ->orderby(array( "created" =>  "DESC", 'deal_id' => 'ASC', "finalna_cena" => 'ASC'))->get()->result_array();

		$where = "(d.category_id = $category_id_selected OR 0 = $category_id_selected) AND (d.partner_id = $partner_id_selected OR 0 = $partner_id_selected) AND d.end_time < '".date("Y-m-d H:i:s")."' AND d.visible = 1 AND d.demo = 0 ";
		$sql_limit_str = ""; 
        $sort_str = "d.created DESC,";

		$query_str = $this->getDealWithOptionsView($where, $sql_limit_str, $sort_str);
		 
		
		return $this -> db -> query($query_str)->result_array();
    }

	public function getDealWithOptionsView($where, $sql_limit_str = "", $sort_str = "") {
		
			$retStr = "SELECT
			d.is_general_deal,
			d.id AS deal_id,
			d.category_id,
			d.subcategory_id,
			d.vraboten_id,
			d.partner_id,
			do.title_mk,
			do.title_mk_clean,
			do.content_short_mk,
			d.visible,
			d.primary1,
			d.primary2,
			d.primary3,
			d.primary4,
			d.primary5,
			d.start_time,
			d.end_time,
			d.min_ammount,
			do.max_ammount,
			d.max_per_person,
			do.price,
			do.price_discount,
			do.valid_from,
			do.valid_to,
			d.locked,
			d.created,
			d.edited,
			d.main_img,
			d.side_img,
			d.card,
			d.demo,
			d.tip,
			do.price_voucher,
			d.prati_mail_do_partner,
			d.ponuda_video,
			d.tip_danocna_sema,
			d.ddv_stapka,
			d.email_content_buy,
			d.email_content_buy_switch,
			d.platena_reklama,
			d.platena_reklama_date,
			do.fiktivni_kuponi_num,
			d.partner_notified_for_active_offer,
			d.additional_img_1,
			d.additional_img_2,
			d.additional_img_3,
			d.additional_img_4,
			d.additional_img_5,
			do.id AS deal_option_id,
			do.default_option,
			do.active,
			IF(d.tip = 'cena_na_vaucer', do.price_voucher, do.price_discount) AS finalna_cena,
			(COUNT(v.id) +do.fiktivni_kuponi_num) AS voucher_count,
			IF( (do.valid_to <> '0000-00-00 00:00:00' AND do.valid_to < NOW() ) OR (do.max_ammount > 0 AND (COUNT(v.id) +do.fiktivni_kuponi_num) >= do.max_ammount) , 1, 0) AS disabled_sort_flag,
			d.title_mk AS title_mk_deal,
			d.title_mk_clean AS title_mk_clean_deal,
			d.content_short_mk AS content_short_mk_deal,
			(SELECT COUNT(doc.id) FROM deal_options AS doc WHERE doc.deal_id = d.id) AS options_cnt,
			COUNT(v.id) AS soldVoucherCount,
			d.plakjanje_pri_prezemanje,
			d.plakjanje_pri_prezemanje_nacin_podiganje,
			d.proizvod_flag,
			d.proizvod_besplatna_dostava,
			d.proizvod_besplatna_dostava_otkup
		FROM deals AS d
		INNER JOIN deal_options AS do ON do.deal_id = d.id
		LEFT JOIN voucher AS v ON v.deal_id = do.deal_id AND v.deal_option_id = do.id AND v.confirmed = 1
		WHERE $where
		group by d.id, do.id
		ORDER BY $sort_str d.id ASC, do.active DESC, disabled_sort_flag ASC, finalna_cena ASC 
		 ";
		
		#STARO
		#$sql_limit_str ";


		//echo $retStr."<br/><br/>";

		return $retStr;	

	}

    public function checkCustomer($customerMail)
    {

        $deal = $this->db->select()->from($this->_customer)->where('email', $customerMail)->get()->result_array();
        if ($deal)
            return $deal[0];
        return false;
    }


	public function clonDeal($existingDealID = 0, $userID = 0) 
	{
		if((int)$existingDealID == 0 || (int)$userID == 0)
			return -1;
		
		//default value
		$insert_id = 0;

		//gradenje na query
		$sql =  "INSERT INTO $this->_tableName (category_id,  subcategory_id,  vraboten_id,  partner_id,  title_mk,  title_mk_clean,  content_short_mk,  visible,  primary1,  primary2,  primary3,  primary4,  primary5,  min_ammount,  max_ammount,  max_per_person,  price,  price_discount,  valid_from,  valid_to,  locked,  created,  edited,  main_img,  side_img,  card,  demo,  tip,  price_voucher,  prati_mail_do_partner,  ponuda_video,  tip_danocna_sema,  ddv_stapka,  email_content_buy,  email_content_buy_switch,  platena_reklama,  platena_reklama_date,  partner_notified_for_active_offer,  additional_img_1,  additional_img_2,  additional_img_3,  additional_img_4,  additional_img_5,  is_general_deal, plakjanje_pri_prezemanje, plakjanje_pri_prezemanje_nacin_podiganje, proizvod_flag, proizvod_besplatna_dostava, proizvod_besplatna_dostava_otkup ) ";
		
		$sql .= "SELECT                         category_id,  subcategory_id,  vraboten_id,  partner_id,  title_mk,  title_mk_clean,  content_short_mk,  	   0,         0,  	     0,  		0,    	   0,  		  0,  min_ammount,  max_ammount,  max_per_person,  price,  price_discount,  valid_from,  valid_to,  locked,    NOW(),   NOW(),  main_img,  side_img,  card,  1   ,  tip,  price_voucher,  prati_mail_do_partner,  ponuda_video,  tip_danocna_sema,  ddv_stapka,  email_content_buy,  email_content_buy_switch,  platena_reklama,  platena_reklama_date,                                  0,  additional_img_1,  additional_img_2,  additional_img_3,  additional_img_4,  additional_img_5,  is_general_deal, plakjanje_pri_prezemanje, plakjanje_pri_prezemanje_nacin_podiganje, proizvod_flag, proizvod_besplatna_dostava, proizvod_besplatna_dostava_otkup  ";
		$sql .= "FROM $this->_tableName ";
		$sql .= "WHERE id = ".$existingDealID." ";
		// die($sql);
		
		//izvrsuvanje na query
		$result = $this->db->query($sql);

		//ako e uspesno izvrseno query-to da se zeme ID-to na noviot zapis (INSERT_ID )
		if($result)
			$insert_id = $result->insert_id();
		
		//vrati go nazat INSERT_ID
		return $insert_id;
	}

}