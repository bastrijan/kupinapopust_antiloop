<?php defined('SYSPATH') or die('No direct script access.');

class Categories_Model extends Default_Model {

	protected $_tableName = 'category';
	
	public function __construct() {
		parent::__construct();
	}
	
	public function getCategories() {
		
		$sql = "SELECT id, name FROM $this->_tableName";
		$res = $this->db->query($sql)->result_array();
		
		$result = array();
		foreach ($res as $value) {
			$result[$value->id] = $value->name;
		}
		
		return $result;
	}

	public function getCategoryAllInfoByID() {
		
		$sql = "SELECT id, name, service_desc FROM $this->_tableName";
		$res = $this->db->query($sql)->result_array();
		
		$result = array();
		foreach ($res as $value) {
			$result[$value->id]["name"] = $value->name;
			$result[$value->id]["service_desc"] = $value->service_desc;
		}
		
		return $result;
	}
	
	public function getNumDealsByCategories() {
		
		$sql = "SELECT c.id, COUNT(d.id) AS cnt FROM category AS c LEFT JOIN deals AS d ON c.id = d.category_id GROUP BY c.id ";
		$res = $this->db->query($sql)->result_array();
		
		$result = array();
		foreach ($res as $value) {
			$result[$value->id] = $value->cnt;
		}
		
		return $result;
	}
	
	public function delete($id) {
		return $this->db->delete($this->_tableName, array('id' => $id));
	}
}