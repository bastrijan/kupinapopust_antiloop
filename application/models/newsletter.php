<?php

defined('SYSPATH') or die('No direct script access.') ;

class Newsletter_Model extends Default_Model {
  
	  protected $_tableName = 'newsletter' ;	



	  public function __construct() {
		parent::__construct() ;
	  }

	public function saveSubscription($data) 
	{
		$data['last_visited_website'] = date("Y-m-d");
		
		$result = $this->db->insert($this->_tableName, $data);
		
		return $result;
	}
	


  public function unsubscribe($hash) 
  {
	
    $sql = "DELETE FROM $this->_tableName WHERE md5(CONCAT('kupi_', email )) = '$hash'" ;
	
	$retValue = $this->db->query($sql);
	

	
    return $retValue ;
  }

  	//samo aktivnite spored last_visited_website
	public function getCustomersEmails($queryWhereAppend = "", $limit = 0, $offset = 0) {

		$sql = "SELECT id, email FROM $this->_tableName WHERE 1=1 ";

		//dopolnitelen WHERE
		$sql .= $queryWhereAppend." ";
		
		//za sortiranje
		$sql .= "ORDER BY id ";
		
		//za limitiranje 
		if($limit != 0)
			$sql .= "LIMIT $limit OFFSET $offset ";
		
		//die($sql);
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}


  	//samo NEaktivnite spored last_visited_website
	public function getInActiveCustomersEmails() {

		$sql = "SELECT id, email FROM $this->_tableName WHERE welcome_again_status IN (0, 2) AND DATEDIFF(DATE(NOW()), last_visited_website) > 90 ";

		//za sortiranje
		$sql .= "ORDER BY id ";
		
		
		//die($sql);
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}

    public function updateInActiveCustomersWelcomeAgainStatus() {

	    $sql = "UPDATE $this->_tableName SET welcome_again_status = welcome_again_status + 1 WHERE welcome_again_status IN (0, 2) AND DATEDIFF(DATE(NOW()), last_visited_website) > 90 " ;
			
		$this->db->query($sql);
    }
	
	public function delete_multiple($ids) 
	{
		return $this->db->from($this->_tableName)->in('id', $ids)->delete();
	}

	public function insert_temp_table($data) 
	{
		$resultTester = $this->db->insert($this->_tableName, $data);
	}



 	public function delete_by_email($email) 
  	{
	
	    $sql = "DELETE FROM $this->_tableName WHERE LOWER(TRIM(email)) = LOWER(TRIM('$email')) " ;
		
		$retValue = $this->db->query($sql);

		return $retValue;
	
  	}

	public function getGrid($from, $to, $search_email = "") {

		$search_email = trim($search_email);

		$res = array();

		if($search_email != "")
        {

			$sql = "SELECT id, email, date_created, last_visited_website FROM $this->_tableName ";
			
			$sql .= "WHERE email LIKE '%" . $search_email . "%' AND (DATE(date_created) between '$from' AND '$to') ";
			
			$sql .= "ORDER BY date_created DESC, email ASC ";
			
			//die($sql);
			$res = $this->db->query($sql)->result_array();
		}
		
		return $res;
	}

    public function setLastVisitedWebsite($email) {

    	if ($email != "") 
        {
		    $sql = "UPDATE $this->_tableName SET last_visited_website = '".date("Y-m-d")."' WHERE LOWER(TRIM(email)) = LOWER(TRIM('$email')) " ;
			
			$this->db->query($sql);
		}
    }

}