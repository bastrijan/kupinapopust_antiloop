<?php defined('SYSPATH') OR die('No direct script access.');

class Subcategories_Model extends Default_Model {

	protected $_tableName = 'subcategories';
	
	public function __construct() {
		parent::__construct();
	}
	
	public function getSubcategoriesbyParentid($parent_id) {
		
		$sql = "SELECT id, name FROM $this->_tableName where category_id = '$parent_id' OR '0' = '$parent_id' ";
		$res = $this->db->query($sql)->result_array();
		
		$result = array();
		foreach ($res as $value) {
			$result[$value->id] = $value->name;
		}
		
		return $result;
	}
	
	public function getNumDealsBySubcategories($Parent_id) {
		
		$sql = "SELECT sc.category_id, sc.id, COUNT(d.id) AS cnt 
                            FROM subcategories AS sc 
                            LEFT JOIN deals AS d 
                            ON sc.id = d.subcategory_id 
                            where sc.category_id = '$Parent_id'
                            GROUP BY sc.id ";
                
		$res = $this->db->query($sql)->result_array();
		
		$result = array();
		foreach ($res as $value) {
			$result[$value->id] = $value->cnt;
		}
		
		return $result;
	}
	
	public function delete($id) {
		return $this->db->delete($this->_tableName, array('id' => $id));
	}
	
	public function getAllSubCategoriesWithOffers($category_id = 0) {
		
		$query_str = "SELECT sc.id, sc.category_id, sc.name, c.name as category_name, COUNT(d.id) AS cnt_deals
					  FROM subcategories AS sc 
					  INNER JOIN category AS c ON c.id = sc.category_id
					  INNER JOIN deals AS d ON d.category_id = sc.category_id AND d.subcategory_id = sc.id AND d.visible = 1 AND d.demo = 0 AND d.end_time > '".date("Y-m-d H:i:s")."'
					  WHERE sc.category_id = '$category_id'
					  GROUP BY sc.id
					  ORDER BY sc.id ASC
					";
		return $this->db->query($query_str)->result_array();
	}
	

}