<?php

defined('SYSPATH') or die('No direct script access.');

class Vouchers_Model extends Default_Model {

    protected $_tableName = 'voucher';
    protected $_tableName2 = 'voucher_attempt';
    protected $_tableNameAffiliateEarning = 'ap_earnings';

    public function __construct() {
        parent::__construct();
    }
	
	/*
	public function getVoucherAttemptsToActivate($dealID) {
		
		$sql = "SELECT v.* FROM $this->_tableName AS v WHERE v.deal_id = $dealID AND confirmed = 1 AND activated = 0 GROUP BY v.attempt_id ";
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
	*/
	
	public function getPlatenaNekreiranaNaracka() {
		
		$sql = "SELECT att.* FROM  voucher_attempt AS att LEFT JOIN voucher AS v ON v.attempt_id = att.id WHERE att.ref <> '' AND att.type = 'card'  AND  DATE(att.ts) > DATE(  '2013-03-06' )  AND (TIME_TO_SEC(TIMEDIFF('".date("Y-m-d h:i:s")."', att.ts))/60 ) > 15   GROUP BY att.id HAVING COUNT(v.id) = 0 LIMIT 1 ";

		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
	
	public function getZavrsenaPonudaAktivniPlateniVauceri($dealID) {
		
		$sql = "SELECT v.*, att.email FROM $this->_tableName AS v INNER JOIN $this->_tableName2 AS att ON v.attempt_id = att.id WHERE v.deal_id = $dealID AND v.activated = 1 AND v.confirmed = 1 ORDER BY att.email ASC";
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
	
	public function getCntVouchersByDealAndByCustomer($dealID, $customerEmail) {
		
		$sql = "SELECT COUNT(v.id) AS count FROM $this->_tableName2 AS va INNER JOIN $this->_tableName AS v ON va.id = v.attempt_id WHERE va.deal_id = $dealID AND va.email = '$customerEmail' ";
		$res = $this->db->query($sql)->result_array();
		
		return $res[0]->count;
	}

	public function getCntVouchersByDealOptionAndByCustomer($dealID, $dealOptionID, $customerEmail) {
		
		$sql = "SELECT COUNT(v.id) AS count FROM $this->_tableName2 AS va INNER JOIN $this->_tableName AS v ON va.id = v.attempt_id WHERE va.deal_id = $dealID AND va.deal_option_id = $dealOptionID AND va.email = '$customerEmail' ";
		$res = $this->db->query($sql)->result_array();
		
		return $res[0]->count;
	}
	
	public function getCntPlateniVauceri($dealID = 0, $dealOptionID = 0) {
		
		$sql = "SELECT COUNT(v.id) AS count FROM $this->_tableName AS v  WHERE v.confirmed = 1 AND v.deal_id = $dealID AND (v.deal_option_id = $dealOptionID || 0 = $dealOptionID) ";
		$res = $this->db->query($sql)->result_array();
		
		return $res[0]->count;
	}
	
	public function getCustomerVouchersGrouped($userID, $limit_records = 0, $where_add = "") {
		
		$sql = "SELECT v.attempt_id, v.deal_id, v.deal_option_id, v.activated, v.confirmed, SUM(v.used) AS cnt_used, GROUP_CONCAT(v.used SEPARATOR '|') AS list_used, GROUP_CONCAT(do.valid_to SEPARATOR '|') AS list_valid_to, count(v.id) AS cnt, v.customer_id, v.id AS first_voucher_id, ";
		$sql .= "GROUP_CONCAT(v.code SEPARATOR '; ') AS list_voucher_code, v.time, va.email, va.copy_email "; 
		$sql .= "FROM $this->_tableName AS v INNER JOIN $this->_tableName2 AS va ON va.id = v.attempt_id INNER JOIN deals AS d ON d.id = v.deal_id AND d.card = 0 INNER JOIN deal_options AS do ON do.id = v.deal_option_id WHERE v.customer_id = $userID ".$where_add." GROUP BY v.attempt_id, v.confirmed, v.activated ORDER BY v.attempt_id DESC, v.id ASC ".($limit_records > 0 ? "LIMIT $limit_records" : "");

		// die($sql);
	
		$res = $this->db->query($sql)->result_array();

		return $res;
	}
	
	public function getCustomerGiftCards($userID, $limit_records = 0) {
		
		$sql = "SELECT v.attempt_id, v.deal_id, v.deal_option_id, v.activated, v.confirmed, SUM(v.used) AS cnt_used, GROUP_CONCAT(v.used SEPARATOR '|') AS list_used, GROUP_CONCAT(do.valid_to SEPARATOR '|') AS list_valid_to, count(v.id) AS cnt, v.customer_id, v.id AS first_voucher_id, ";
		$sql .= "GROUP_CONCAT(v.code SEPARATOR '; ') AS list_voucher_code, v.time, va.email, va.copy_email "; 
		$sql .= "FROM $this->_tableName AS v INNER JOIN $this->_tableName2 AS va ON va.id = v.attempt_id INNER JOIN deals AS d ON d.id = v.deal_id AND d.card = 1 INNER JOIN deal_options AS do ON do.id = v.deal_option_id WHERE v.customer_id = $userID GROUP BY v.attempt_id, v.confirmed, v.activated ORDER BY v.attempt_id DESC, v.id ASC ".($limit_records > 0 ? "LIMIT $limit_records" : "");
		$res = $this->db->query($sql)->result_array();


		// die($sql);
		
		return $res;
	}
	

    public function getExpiredData($ids) {
        if (is_array($ids)) {
            $ids = implode(',', $ids);
        }
        $sql = "SELECT count(*) as count, deal_id FROM $this->_tableName WHERE deal_id in ($ids) group by deal_id";
        $res = $this->db->query($sql)->result_array();

        $result = array();
        foreach ($res as $value) {
            $result[$value->deal_id] = $value->count;
        }

        return $result;
    }

    public function createVoucher($data, $payAttemptID, $type = 'card', $kp_aff_program_id = 0) {
		
		ini_set('max_execution_time', 300); //300 seconds = 5 minutes=

        $codeModel = new Code_Model();
        $customerModel = new Customer_Model();
        $mailContentModel = new Emailrenderer_Model();
        $dealsModel = new Deals_Model();
        $dealsOptionsModel = new Deal_Options_Model() ;

        $payAttemptData = $this->getPayAttempt($payAttemptID);

        $deal_option_id = $payAttemptData[0]->deal_option_id;

        $deal = $dealsModel->getData(array("id" => $data['discount_id']));
        $dealOption = $dealsOptionsModel->getData(array("id" => $deal_option_id));
        
        $ap_earnings_transaction_id = 0;

        //ako treba se presmeta provizija za affiliate programata na kupinapopust
        if($kp_aff_program_id)
        {
			/***********TRACKING NA PRODAZBA OD NASIOT AFFILIATE******************/

		    $sale_amount = ($deal[0]->tip == 'cena_na_vaucer' ? $dealOption[0]->price_voucher : $dealOption[0]->price_discount) * $payAttemptData[0]->quantity;//provizijata sto ja zema kupinapopust * brojot na vauceri


		    $kupinapopust_provizija = $dealOption[0]->price_voucher * $payAttemptData[0]->quantity;//provizijata sto ja zema kupinapopust * brojot na vauceri
		    $product = strip_tags($deal[0]->title_mk_clean);
		    $email = $payAttemptData[0]->email;
		    $voucher_attempt_id = $payAttemptID;
			
		    include(DOCROOT.'affiliate/controller/record-sale.php');
        }

        $customerID = $customerModel->getCustomerID($payAttemptData[0]->email, $kp_aff_program_id, $ap_earnings_transaction_id);
        
        $countVouchers = $data["amount"];
		
		$time_voucher_created = 0;
		for ($i = 1; $i <= $countVouchers; $i++) {

	        /*****DOPOLNITELNA PROVERKA DALI VEKJE SE ISKREIRANI ONOLKU VAUCERI KOLKU STO TREBA*****/
	        $vouchersCreatedUntilNowArray = $this->getData(array("attempt_id" =>  $payAttemptID));

	        // Kohana::log("error", "naracka ID: ".$payAttemptID." |||broj na vauceri: ".count($vouchersCreatedUntilNowArray));

	        if(count($vouchersCreatedUntilNowArray) >= $payAttemptData[0]->quantity)
	        	break;
	        /****************************************************************************************/
			
			$set = array();
			$set['deal_id'] = $data['discount_id'];
			$set['deal_option_id'] = $deal_option_id;
			$set['customer_id'] = $customerID;
			$set['type'] = $type;
			
			if(array_key_exists('used', $data))
				$set['used'] = $data['used'];

			$set['time'] = date('Y-m-d H:i:s');
			$time_voucher_created = $set['time'];
			$set['attempt_id'] = $payAttemptID;
			
			$code = $codeModel->generateCode();
			$set['code'] = $code;
			$set['activated'] = 0;
			$set['confirmed'] = 0;

			if (in_array($type, array("card", "otkup"))) {
				$set['confirmed'] = 1;
			}

			if(array_key_exists('app_type', $data))
				$set['app_type'] = $data['app_type'];
			
			$this->saveData($set);
		}

		//update-iraj go poleto last_purchase vo Customers
		$customerModel->setLastPurchase($customerID);
        
        //izvadi broj na plateni vauceri
        $createdVouchers = $this->getData(array("deal_id" => $data['discount_id'], 'confirmed' => 1));
       	
       	//izvadi broj na fiktivni vauceri
		$fiktivniVauceriByDeal = $dealsOptionsModel->getFiktivniVauceriByDeal($data['discount_id']) ;

		//vkupen broj na vauceri = realni + fiktivni
		$vkBrojNaVauceri = ( count($createdVouchers) + $fiktivniVauceriByDeal );

        if(in_array($type, array('card', 'bank', 'cache')))
        {
			/********CREIRANJE NA EMAIL-ITE*******/
			//osnovni podatoci    
	        $nameLang = "title_" . $this->lang;
	        $nameCleanLang = "title_" . $this->lang . "_clean";
	        $dealTitle = $deal[0]->$nameLang;
	        $dealCleanTitle = $deal[0]->$nameCleanLang;
			$suma = ($deal[0]->card) ? $dealOption[0]->price_discount : ($dealOption[0]->price - $dealOption[0]->price_discount);
			
			//formiranje na array-ite za popolnuvanje na email template-ite
	        if ($payAttemptData[0]->gift) {
	            $maildata = array(
	                'email' => $payAttemptData[0]->email,
					'copy_email' => $payAttemptData[0]->copy_email,
					'time' => $time_voucher_created,
	                'cleanTitle' => $dealCleanTitle,
	                'suma' => $suma,
					'cnt' => $countVouchers
	            );
	        } else {
	            $maildata = array(
	                'email' => $payAttemptData[0]->email,
					'time' => $time_voucher_created,
	                'offerTitle' => $dealTitle,
	                'suma' => $suma,
					'cnt' => $countVouchers
	            );
	        }
			$email_config = Kohana::config('email');
			$from = array($email_config['from']['email'], $email_config['from']['name']);

			//ako plakja so platezna karticka
	        if ($type == 'card') {
	        	//ako e podarok
	            if ($payAttemptData[0]->gift) {
	            	//ako e Podarok 'kupinapopust' elektronska karticka
	                if ($deal[0]->card) {
						//if($payAttemptData[0]->copy_email == "")
						$mailContent = $mailContentModel->render("payment_success_gift_card", $maildata);
	                } else {
	                    $mailContent = $mailContentModel->render("payment_success_gift", $maildata);
	                }
	            } else {
	                $mailContent = $mailContentModel->render("payment_success", $maildata);
	            }
	        }

			//ako plakja KES (vo gotovo)
	        if ($type == 'cache') {
	        	//ako e podarok
	            if ($payAttemptData[0]->gift) {
	            	//ako e Podarok 'kupinapopust' elektronska karticka
	                if ($deal[0]->card) {
	                    $mailContent = $mailContentModel->render("payment_success_cache_gift_card", $maildata);
	                } else {
	                    $mailContent = $mailContentModel->render("payment_success_cache_gift", $maildata);
	                }
	            } else {
	                $mailContent = $mailContentModel->render("payment_success_cache", $maildata);
	            }
	        }
			
			//ako plakja so uplatnica
	        if ($type == 'bank') {
	        	//ako e podarok
	            if ($payAttemptData[0]->gift) {
	            	//ako e Podarok 'kupinapopust' elektronska karticka
	                if ($deal[0]->card) {
	                    $mailContent = $mailContentModel->render("payment_success_bank_gift_card", $maildata);
	                } else {
	                    $mailContent = $mailContentModel->render("payment_success_bank_gift", $maildata);
	                }
	            } else {
	                $mailContent = $mailContentModel->render("payment_success_bank", $maildata);
	            }
	        }

	        if ($deal[0]->card) 
	        {
	            $titleCard = 'Резервација на електронска "kupinapopust" картичка';
	            
	            if ($type == 'card') {
	                $titleCard = 'Потврда за извршено плаќање.';
	            }
				
				//if($payAttemptData[0]->copy_email == "")
				email::send($payAttemptData[0]->email, $from, $titleCard, $mailContent, true);
	        } else {
				if (($vkBrojNaVauceri < $deal[0]->min_ammount) && !in_array($type, array('bank', 'cache'))) {
	                email::send($payAttemptData[0]->email, $from, kohana::lang("email.payment_success"), $mailContent, true);
	            }
	        }

        }// if(in_array($type, array('card', 'bank', 'cache'))

        /*********AKTIVIRANJE NA NEAKTIVNI VAUCERI************/
        if ($vkBrojNaVauceri >= $deal[0]->min_ammount) {
            $notSentVouchers = $this->getData(array("deal_id" => $data['discount_id'], 'activated' => 0, 'confirmed' => 1));
            if ($notSentVouchers) {
                foreach ($notSentVouchers as $notSentVoucher) {
                    $this->activateVoucher($notSentVoucher);
                }
            }
        }

    }

    public function savePayAttempt($data) {
		$loggedUserID = $this->session->get("user_id");
		
        $set = array();
        $set['deal_id'] = $data['discount_id'];
        $set['deal_option_id'] = $data['deal_option_id'];
        $set['email'] = $data['Email'];
        $set['gift'] = $data['gift'];
        if ($set['gift']) {
            $set['posveta'] = $data['posveta'];
        }

        if (isset($data['email_friend'])) {
        	$set['copy_email'] = $data['email_friend'];
        }

        $set['email'] = $data['Email'];

        $set['quantity'] = $data['amount'];
        $set['type'] = $data['payment'];
        $set['ts'] = date('Y-m-d H:i:s');
        if (!isset($data['points_used'])) {
            $data['points_used'] = 0;
        }
        $set['points_used'] = $data['points_used'];
		
		$set['take_points_from_customer_id'] = $loggedUserID > 0 ? $loggedUserID : 0;
		
        if (isset($data['kp_aff_program_id'])) {
            $set['kp_aff_program_id'] = $data['kp_aff_program_id'];
        }

        if (isset($data['shop_cart_id'])) {
            $set['shop_cart_id'] = $data['shop_cart_id'];
        }

        if (isset($data['installment'])) {
            $set['installments'] = $data['installment'];
        }      

        if (isset($data['otkup_ime_prezime'])) 
            $set['otkup_ime_prezime'] = $data['otkup_ime_prezime'];

        if (isset($data['otkup_adresa'])) 
            $set['otkup_adresa'] = $data['otkup_adresa'];

        if (isset($data['otkup_telefon'])) 
            $set['otkup_telefon'] = $data['otkup_telefon'];

        if (isset($data['otkup_prevzemanje'])) 
            $set['otkup_prevzemanje'] = $data['otkup_prevzemanje'];  
		
        $res = $this->db->insert($this->_tableName2, $set);
        return $res->insert_id();
    }

    public function getPayAttempt($payAttemptID, $column = 'id', $order_by = array()) {

        $res = $this->db->select()->from($this->_tableName2)
        						  ->where($column, $payAttemptID)
        						  ->orderby($order_by)
        						  // ->orderby(array( 'category_id' => 'ASC', 'subcategory_id' => 'ASC', 'start_time' => 'DESC' ))
        						  ->get()->result_array();

        return $res;
    }


	public function getNaracki($naracki_tip = '', $naracka_datum = "", $naracka_email = "", $naracka_ref = "", $otkup_administracija_status = -1, $otkup_ime_prezime = "", $otkup_adresnica = "", $preselectedDealID = "", $preselectedDealOptionID = "", $preselectedShoppingCartID = "") 
	{
		//inicijalizacija
		$dbRef = $this->db;
		$filterChain = $dbRef->select()->from($this->_tableName2);

		//WHERE chain
		if($naracki_tip != "")
			$filterChain->where("type", $naracki_tip);		
		
		if($naracka_datum != "" && $naracka_datum != "0000-00-00")
			$filterChain->where("DATE(ts) = ".$dbRef->escape($naracka_datum));

		if(trim($naracka_email) != "")
			$filterChain->like("email", trim($naracka_email));

		if(trim($naracka_ref) != "")
			$filterChain->where("ref", trim($naracka_ref));

		if($otkup_administracija_status != -1)
			$filterChain->where("otkup_administracija_status", $otkup_administracija_status);

		if(trim($otkup_ime_prezime) != "")
			$filterChain->like("otkup_ime_prezime", trim($otkup_ime_prezime));

		if(trim($otkup_adresnica) != "")
			$filterChain->where("otkup_adresnica", trim($otkup_adresnica));

		if($preselectedDealID != "")
			$filterChain->where("deal_id", $preselectedDealID);	

		if($preselectedDealOptionID != "")
			$filterChain->where("deal_option_id", $preselectedDealOptionID);	

		if($preselectedShoppingCartID != "")
			$filterChain->where("shop_cart_id", $preselectedShoppingCartID);	

		// die(print_r($filterChain));
		//sorting
		$sort_array = array("id" => "ASC");

		//izvrsuvanje na query i vrakjanje na podatoci
        return 	$filterChain
                ->orderby($sort_array)
                ->get()
                ->result_array();
	
    }



	public function getVoucher($voucher_id) {
		
		$res = $this->db->select()->from($this->_tableName)->where('id', $voucher_id)->get()->result_array();
		
		return $res;
	}
	
	public function getVouchersByAttemptID($customer_id, $attempt_id) {
		
		$attempt_id = (int) $attempt_id;
		
		$where = array('customer_id' => $customer_id, 'attempt_id' => $attempt_id);
		
		$res = $this->db->select()->from($this->_tableName)->where($where)->get()->result_array();
		
		return $res;
	}
	
    public function savePaymentRef($payAttemptID, $ref) {
        $set = array('ref' => $ref);
        $where = array('id' => $payAttemptID);
        $res = $this->db->update($this->_tableName2, $set, $where);
        return $res;
    }

    public function savePaymentRefShopCart($shop_cart_id, $ref) {
        $set = array('ref' => $ref);
        $where = array('shop_cart_id' => $shop_cart_id);
        $res = $this->db->update($this->_tableName2, $set, $where);
        return $res;
    }

    public function updateVoucherAttempt($voucherAttemptUpdateColumnArray, $voucherAttemptWhereArray) {
        // $set = array('ref' => $ref);
        // $where = array('id' => $payAttemptID);
        // $res = $this->db->update($this->_tableName2, $set, $where);
        $res = $this->db->update($this->_tableName2, $voucherAttemptUpdateColumnArray, $voucherAttemptWhereArray);
        return $res;
    }

	public function zastedivme() {
		ini_set('max_execution_time', 600); //600 seconds = 10 minutes=
		$vouchersModel = new Vouchers_Model();
		$dealsModel = new Deals_Model();
		$where = array();

		$myVouchers = $vouchersModel->getData($where);
		
		$totalSpent = 0;
		foreach ($myVouchers as $voucher) {
			
			$dealData = $dealsModel->getData(array("id" => $voucher->deal_id));
			if (isset($dealData[0])) {
				
					$totalSpent+= ($dealData[0]->price - $dealData[0]->price_discount);    
			}
			
		}
		
		return $totalSpent;
	}

    public function confirmVoucher($id) {

        $where = array('id' => $id);
        $set = array('confirmed' => 1);

        return $this->db->update($this->_tableName, $set, $where);
    }
    
    public function usedVoucher($checked_ids, $unchecked_ids) {
        //izvrsi go query-to
		if(count($checked_ids) > 0)
			$status_checked = $this->db->from($this->_tableName)->set(array('used' => 1))->in('id', $checked_ids)->update();
		
		if(count($unchecked_ids) > 0)	
	        $status_unchecked = $this->db->from($this->_tableName)->set(array('used' => 0))->in('id', $unchecked_ids)->update();
    }

    public function paidOffVoucher($partnerId = 0) {

    	$partnerId = (int) $partnerId;

	    $sql = "UPDATE $this->_tableName AS v ";
	    $sql .= "INNER JOIN deals AS d ON v.deal_id = d.id " ;
	    $sql .= "SET v.paid_off = 1 ";
	    $sql .= "WHERE d.partner_id = ".$partnerId." AND v.used = 1 " ;
		
		$status = $this->db->query($sql);		



		return $status;
    }


    public function confirmPaidOffVoucher($partnerId = 0) {

    	$partnerId = (int) $partnerId;

        //izvrsi go query-to
	    $sql = "UPDATE $this->_tableName AS v ";
	    $sql .= "INNER JOIN deals AS d ON v.deal_id = d.id " ;
	    $sql .= "SET v.confirm_paid_off = 1 ";
	    $sql .= "WHERE d.partner_id = ".$partnerId." AND v.paid_off = 1 AND v.used = 1 " ;
		
		$status = $this->db->query($sql);

		return $status;
    }

	public function deleteVoucher($voucher_id, $namaliKolicinaKajNarackata = 1) {
		
		//zemi go zapisot za voucher
		$voucher_arr = $this->getVoucher($voucher_id);
		
		//izbrisi go zapisot za vaucerot vo tabelata voucher
		$this->cancelVoucher($voucher_id);
		
		if($namaliKolicinaKajNarackata)
		{
			/****proveri kolku e quantity vo voucher_attmpts za toj voucher_attmpt****/
			//zemi go zapisot za voucher_attempts
			$voucher_attempt_arr = $this->getPayAttempt($voucher_arr[0]->attempt_id);
			
			if($voucher_attempt_arr[0]->quantity > 1)
			{
				//presmetaj kolku e noviot quantity (odnosto stariot namali go za 1)
				$quantity = $voucher_attempt_arr[0]->quantity - 1;
				
				//zacuvaj go noviot quantity
				$this->voucherAttemptNamaliQuantity($voucher_arr[0]->attempt_id, $quantity);
			}
			else // znaci ako quantity bil 1 => brisi go zapisot 
			{
				//izbrisi go zapisot vo voucher_attmpts
				$this->deleteVoucherAttempt($voucher_arr[0]->attempt_id);
			}

		}
		 

		/****odzemi poeni****/
		//proveri dali e aktiviran voucher-ot. 
		if($voucher_arr[0]->activated)
		{
			$dealModel = new Deals_Model();	
			$dealData = $dealModel->getData(array("id" => $voucher_arr[0]->deal_id));
			
			$points_osnova = calculate::points_osnova($dealData[0]->price_discount, $dealData[0]->price_voucher, $dealData[0]->tip_danocna_sema, $dealData[0]->ddv_stapka, $dealData[0]->tip);
			if($points_osnova > 9)
			{
				$pointsModel = new Points_Model();
				
				$points = calculate::points($points_osnova, 1);
				$customer_id = $voucher_arr[0]->customer_id;
				$pointsModel->savePoints($customer_id, -$points, "- Поени одземени од Kupinapopust.mk заради избришан ваучер");
			}
		}
	}

	public function deleteVoucherAttempt($id) {
		
		$where = array('id' => $id);
		return $this->db->delete($this->_tableName2, $where);
	}

	public function deleteAffiliateEarning($voucher_attempt_id) {
		
		$where = array('voucher_attempt_id' => $voucher_attempt_id);
		return $this->db->delete($this->_tableNameAffiliateEarning, $where);
	}
	
    public function cancelVoucher($id) {

        $where = array('id' => $id);
        return $this->db->delete($this->_tableName, $where);
    }
	
    public function cancelAllVouchersInOrder($order_id) {

        $where = array('attempt_id' => $order_id);
        return $this->db->delete($this->_tableName, $where);
    }

	public function voucherAttemptNamaliQuantity($payAttemptID, $quantity) {
		$set = array('quantity' => $quantity);
		$where = array('id' => $payAttemptID);
		$res = $this->db->update($this->_tableName2, $set, $where);
		return $res;
	}

    public function activateVoucher($voucher, $oznaciKakoIskoristen = 0) {
		ini_set('max_execution_time', 300); //300 seconds = 5 minutes=

        $payAttemptData = $this->getPayAttempt($voucher->attempt_id);

        // mark as activated
        $data = array('id' => $voucher->id, 'activated' => 1);
        
        //dali da se oznaci kako iskoristen
        if($oznaciKakoIskoristen)
        	$data["used"] = 1;

        $this->saveData($data);

        $voucherData = $this->getData(array('id' => $voucher->id));

        $pointsModel = new Points_Model();
		if (!$payAttemptData[0]->points_used_calculated && $payAttemptData[0]->points_used) {
			
			//iskoristi gi poenite i odzemi gi od profilot
			$pointsModel->usePoints($payAttemptData[0]->take_points_from_customer_id, $payAttemptData[0]->points_used, "Употребени за купување на понуда");
			
			//zapisi vo baza (vo voucher_attempt) deka vekje se odzemeni
			$setPointCalculated = array('points_used_calculated' => 1);
			$wherePointCalculated = array('id' => $payAttemptData[0]->id);
			$this->db->update($this->_tableName2, $setPointCalculated, $wherePointCalculated);
		}
		
		//ako e "kupinapopust" elektronska karticka, stavi go kodot na kartickata vo tabelata - points_activation
        $dealModel = new Deals_Model();

        //old
        //$dealData = $dealModel->getData(array("id" => $payAttemptData[0]->deal_id));

	    $where_options_str = "d.id = ".$payAttemptData[0]->deal_id." AND do.id = ".$payAttemptData[0]->deal_option_id;
	    $dealData = $dealModel->getDealOptionsData($where_options_str);
	    

        if ($dealData[0]->card) {
            $giftEmail = $payAttemptData[0]->email;
            if ($payAttemptData[0]->copy_email) {
              $giftEmail =  $payAttemptData[0]->copy_email; 
            }
			$pointsModel->sendGiftCard($giftEmail, ($dealData[0]->tip == 'cena_na_vaucer' ? $dealData[0]->price_voucher : $dealData[0]->price_discount),$voucher->code);
        }
		
		//flag dali da se prakja email ili ne. Samo koga doaga prviot vaucer od narackata se prakja mail. vo ostanatite slucai ne se prakja.
		$sendEmails = false;
        //add new points. The points are added only once for all quantity
		if (!$payAttemptData[0]->new_points_calculated) {
			
			$points_osnova = 0;
			$pointsVoucher = 0;

			//poeni se dodavaat samo ako NE E lyoness korisnik
			if(!commonshow::isLyonessUser())
			{
				$points_osnova = calculate::points_osnova($dealData[0]->price_discount, $dealData[0]->price_voucher, $dealData[0]->tip_danocna_sema, $dealData[0]->ddv_stapka, $dealData[0]->tip);
				$pointsVoucher = calculate::points($points_osnova, $payAttemptData[0]->quantity);
			}
			
			//dokolku ponudata nosi poeni i plakjanjeto e ednokratno (ne e na rati)
			if ($pointsVoucher > 0 && $payAttemptData[0]->installments == 1)
				$pointsModel->savePoints($voucherData[0]->customer_id, $pointsVoucher, "од купени ваучери");

			//zapisi vo baza (vo voucher_attempt) deka vekje se odzemeni
			$setNewPointCalculated = array('new_points_calculated' => 1);
			$whereNewPointCalculated = array('id' => $payAttemptData[0]->id);
			$this->db->update($this->_tableName2, $setNewPointCalculated, $whereNewPointCalculated);
			
			$sendEmails = true;
		}
		
		if($sendEmails)
		{
			//send vauchers
			$mailContent = $this->renderVoucher($voucher->id);
			
			$email_config = Kohana::config('email');
			$from = array($email_config['from']['email'], $email_config['from']['name']);
			$from_email_string = $email_config['from']['email'];
			$from_name_string = $email_config['from']['name'];
			
			//se generira subject-ot na mail-ot
			if ($payAttemptData[0]->gift) {
				if ($dealData[0]->card) {
					$title = 'Некој мисли на вас:) Добивте подарок '.$payAttemptData[0]->quantity.' "kupinapopust" картичк'.($payAttemptData[0]->quantity > 1 ? "и" : "а").'.';
				} else {
					$title = kohana::lang("email.gift_voucher");//." Нарачка бр. ".$payAttemptData[0]->id;
				}
			} else {
				$title = kohana::lang("email.send_voucher");//." Нарачка бр. ".$payAttemptData[0]->id;
			}
			
			//tuka se generira body-to na mail-ot
			//ako e kupuvanje na - podarok elektronska karticka
			if ($dealData[0]->card) {
				if ($payAttemptData[0]->copy_email) {
					
					$pdf_filename = "kupinapopust_naracka_br_".$payAttemptData[0]->id.".pdf";
					$pdf_data = $this->CreatePDF($payAttemptData[0]->id);

					email::sendAttachment($payAttemptData[0]->copy_email, $from_email_string, $from_name_string, $title, $mailContent, $pdf_data, $pdf_filename);
				} 
			} 
			else //ako e kupuvanje na - ponuda
			{
				
				//generiranje na podatoci za PDF-ot
				$pdf_filename = "kupinapopust_naracka_br_".$payAttemptData[0]->id.".pdf";
				$pdf_data = $this->CreatePDF($payAttemptData[0]->id);

				//ona sto ke go dobie kupuvacot
				$pdf_data_kupuvac = "";
				$pdf_filename_kupuvac = "";
				if (!$payAttemptData[0]->gift)
				{
					$pdf_data_kupuvac = $pdf_data;
					$pdf_filename_kupuvac = $pdf_filename;
				}

				//email::send($payAttemptData[0]->email, $from, $title, $mailContent, true);
				//ZA KUPUVACOT
				email::sendAttachment($payAttemptData[0]->email, $from_email_string, $from_name_string, $title, $mailContent, $pdf_data_kupuvac, $pdf_filename_kupuvac);
				
				//ako vaucerot se podaruva - ZA ONOJ NA KOGO MU SE PODARUVA
				if ($payAttemptData[0]->copy_email) {

					$maildata = array(
							'posveta' => strip_tags($payAttemptData[0]->posveta),
							'cnt' => $payAttemptData[0]->quantity,
							'voucherTitle' => $dealData[0]->title_mk_clean,
							'time' => $voucherData[0]->time,
							'otkup_prevzemanje' => $payAttemptData[0]->otkup_prevzemanje
					);
					$mailContentModel = new Emailrenderer_Model();
					$mailContent = $mailContentModel->render("voucher_gift", $maildata);
					//email::send($payAttemptData[0]->copy_email, $from, $title, $mailContent, true);
					email::sendAttachment($payAttemptData[0]->copy_email, $from_email_string, $from_name_string, $title, $mailContent, $pdf_data, $pdf_filename);
				}
		    }
		}//if($sendEmails)
    }

    public function renderVoucher($voucher_id) {
        $vouchersModel = new Vouchers_Model();
        $dealsModel = new Deals_Model();
        $partnersModel = new Partners_Model();

        if ($voucher_id) {
            $voucherData = $vouchersModel->getData(array('id' => $voucher_id));
            $payAttempt = $vouchersModel->getPayAttempt($voucherData[0]->attempt_id);


            $deal = $dealsModel->getData(array("id" => $voucherData[0]->deal_id));
            $addressLang = "adress_" . $this->lang;
            $termsLang = "terms_" . $this->lang;
            $nameLang = "title_" . $this->lang;
            $nameCleanLang = "title_" . $this->lang . "_clean";
            $dealTitle = $deal[0]->$nameLang;
            $dealCleanTitle = $deal[0]->$nameCleanLang;
			$zasteda = $deal[0]->price - $deal[0]->price_discount;

            $partnerData = $partnersModel->getData(array('id' => $deal[0]->partner_id));

            $mailContentModel = new Emailrenderer_Model();
			
			
            if ($payAttempt[0]->gift) {
                if ($deal[0]->card) {
                    $viewName = 'card_gift';
                    $maildata = array(
                        'posveta' => strip_tags($payAttempt[0]->posveta),
						'cnt' => $payAttempt[0]->quantity,
                        'code' => $voucherData[0]->code,
                        'time' => $voucherData[0]->time,
                        'email' => ($payAttempt[0]->copy_email) ? $payAttempt[0]->copy_email : $payAttempt[0]->email,
                        'otkup_prevzemanje' => $payAttempt[0]->otkup_prevzemanje
                    );
                } else {
					$viewName = 'voucher_gift_clean';
                    $maildata = array(
						'ponuda_tip' => $deal[0]->tip,
						'provizija' => $deal[0]->price_voucher,
						'zasteda' => $zasteda,
						'copy_email' => $payAttempt[0]->copy_email,
						'cnt' => $payAttempt[0]->quantity,
                        'posveta' => strip_tags($payAttempt[0]->posveta),
                        'voucherTitle' => $dealCleanTitle,
                        'price' => $deal[0]->price,
                        'code' => $voucherData[0]->code,
                        'time' => $voucherData[0]->time,
                        'partnerName' => strip_tags($partnerData[0]->name),
                        'partnerAddress' => $this->PartnerInfoCustom($partnerData[0]->$addressLang),
                        'from' => date("d/m/Y", strtotime($deal[0]->valid_from)),
                        'to' => date("d/m/Y", strtotime($deal[0]->valid_to)),
                        'info' => strip_tags($partnerData[0]->$termsLang),
                    	'otkup_prevzemanje' => $payAttempt[0]->otkup_prevzemanje
                    );
                }
            } else {
                $viewName = 'voucher';
                $maildata = array(
					'ponuda_tip' => $deal[0]->tip,
					'provizija' => $deal[0]->price_voucher,
                    'zasteda' => $zasteda,
					'cnt' => $payAttempt[0]->quantity,
                    'voucherTitle' => $dealTitle,
                    'code' => $voucherData[0]->code,
                    'time' => $voucherData[0]->time,
                    'partnerName' => strip_tags($partnerData[0]->name),
                    'partnerAddress' => $this->PartnerInfoCustom($partnerData[0]->$addressLang),
                    'from' => date("d/m/Y", strtotime($deal[0]->valid_from)),
                    'to' => date("d/m/Y", strtotime($deal[0]->valid_to)),
					'email_content_buy' => $deal[0]->email_content_buy,
					'email_content_buy_switch' => $deal[0]->email_content_buy_switch,
                    'info' => strip_tags($partnerData[0]->$termsLang),
                    'otkup_prevzemanje' => $payAttempt[0]->otkup_prevzemanje
                );
            }


            $mailContent = $mailContentModel->render($viewName, $maildata);
			//die($mailContent);
            return $mailContent;
        }
    }


	private function NewLine2Br($str_in) {
		$str_out = "";
		
		//za zamena na new line so zapirka
		$order   = array("\r\n", "\n", "\r", "\n\r");
		$replace = array("<br/>", "<br/>", '<br/>', '<br/>');
		
		$str_out = str_replace($order, $replace, trim($str_in));
						
		return $str_out;
	}	
	
	private function PartnerInfoCustom($str_in) {
		$str_out = "";
		
		//za zamena na new line so zapirka
		$order   = array(":<br />",",<br />","<br>", "<br/>", "<br />");
		$replace = array(": ","<br />",', ', ', ', ', ');
		
		$str_out = str_replace($order, $replace, trim($str_in));
						
		return $str_out;
	}	
	
	private function PdfCustomReplace($str_in) {
		$str_out = "";
		$str_out = strip_tags($str_in);
		//za zamena na new line so zapirka
		$order   = array("&bull;", "•");
		$replace = array("", "");
		
		$str_out = str_replace($order, $replace, $str_out);
		
		return trim($str_out);
	}	
	
	public function getVouchers($ids) {
		
		$sql = "SELECT * FROM voucher WHERE id IN ($ids) ";
		
		$res = $this->db->query($sql)->result_array();
		
		return $res;
	}
	
	public function CreatePDF($voucher_attempt_id, $type = "S", $deal_id = 0, $smetkovodstvoreport_type = '', $smetkovodstvoreport_year = 2013 , $smetkovodstvoreport_month = 1 , $smetkovodstvoreport_day = 1) 
	{
		ini_set('max_execution_time', 600); //600 seconds = 10 minutes=
		
		//inicijalizacija na array-ot sto se polni so vauceri
		$vouchersArr = array();
		
		//zemi podatoci od baza sto treba
		
		//ako se raboti za zemanje na site vauceri od eden attempt
		if($voucher_attempt_id > 0)
		{
			$vouchersArr = $this->getData(array("attempt_id" => $voucher_attempt_id));
			
		}
		else
		{
			$previous_day = date('Y-n-j', strtotime('-1 day', strtotime("$smetkovodstvoreport_year-$smetkovodstvoreport_month-$smetkovodstvoreport_day")));
			$today = "$smetkovodstvoreport_year-$smetkovodstvoreport_month-$smetkovodstvoreport_day";
			
			//ako se raboti za zemanje na site vauceri od eden deal i konkreten tip na plakanje na vauceri
			if($deal_id > 0 && $smetkovodstvoreport_type != "")
			{
				//$where = array('deal_id' => $deal_id, 'activated' => 1, 'confirmed' => 1, 'type' => $smetkovodstvoreport_type, 'time >=' => "$previous_day 19:00:01", 'time <=' => "$today 19:00:00") ;
				//$where = array('deal_id' => $deal_id, 'activated' => 1, 'confirmed' => 1, 'type' => $smetkovodstvoreport_type, 'time' => "$today") ;
				//$vouchersArr = $this->getData($where);

				$sql = "SELECT * FROM voucher WHERE deal_id = $deal_id AND activated = 1 AND confirmed = 1 AND type = '$smetkovodstvoreport_type' AND DATE(time) = '$today' ";
				$vouchersArr = $this->db->query($sql)->result_array();
			}
			elseif($deal_id == 0 && $smetkovodstvoreport_type != "")
			{
				//$where = array('activated' => 1, 'confirmed' => 1, 'type' => $smetkovodstvoreport_type, 'time >=' => "$previous_day 19:00:01", 'time <=' => "$today 19:00:00") ;
				//$where = array('activated' => 1, 'confirmed' => 1, 'type' => $smetkovodstvoreport_type, 'time' => "$today") ;
				//$vouchersArr = $this->getData($where);
				
				$sql = "SELECT * FROM voucher WHERE activated = 1 AND confirmed = 1 AND type = '$smetkovodstvoreport_type' AND DATE(time) = '$today' ";
				$vouchersArr = $this->db->query($sql)->result_array();
			}
			elseif($deal_id == 0 && $smetkovodstvoreport_type == "" && $smetkovodstvoreport_day != "")
			{
				//$where = array('activated' => 1, 'confirmed' => 1,  'time >=' => "$previous_day 19:00:01", 'time <=' => "$today 19:00:00") ;	
				//$where = array('activated' => 1, 'confirmed' => 1,  'time' => "$today") ;	
				//$vouchersArr = $this->getData($where);
				
				$sql = "SELECT * FROM voucher WHERE type <> 'cache' AND activated = 1 AND confirmed = 1 AND DATE(time) = '$today' ";
				$vouchersArr = $this->db->query($sql)->result_array();
			}
			elseif($deal_id == 0 && $smetkovodstvoreport_type == "" && $smetkovodstvoreport_day == "")
			{
				$SmetkovodstvoReportModel = new SmetkovodstvoReport_Model() ;
				$mesecenOslobodeniOdDanok = $SmetkovodstvoReportModel->getSmetkovodstvoIzvestajMesecen($smetkovodstvoreport_month, $smetkovodstvoreport_year) ;
				$group_concat_ids = $mesecenOslobodeniOdDanok[0]->group_concat_ids;
				
				$vouchersArr = $this->getVouchers($group_concat_ids);	
			}
			else
				die("GRESKA");
		}

		//zemi podatoci za voucher_attempt (narackata)
		$voucherAttemptArr = $this->db->select()->from($this->_tableName2)->where('id', $voucher_attempt_id)->get()->result_array();

		//zemi ja ponudata		
		$dealsModel = new Deals_Model();

		//kreiraj objekt za opcijata		
		$dealOptionModel = new Deal_Options_Model();
		
		//zemi go partnerot
		$partnersModel = new Partners_Model();
		
		//vkluci gi potrebnite fajlovi
		require_once APPPATH . 'tcpdf/config/lang/mkd.php';
		require_once APPPATH . 'tcpdf/tcpdf.php';
		
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		// set document information
		//$pdf->SetCreator(PDF_CREATOR);
		//$pdf->SetAuthor('Nicola Asuni');
		//$pdf->SetTitle('TCPDF Example 001');
		//$pdf->SetSubject('TCPDF Tutorial');
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		
		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(255,99,0), array(255,99,0));
		$pdf->setFooterData($tc=array(0,0,0), $lc=array(255,99,0));
		
		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, 'I', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		
		//set some language-dependent strings
		//$pdf->setLanguageArray($l);
		
		// set default font subsetting mode
		$pdf->setFontSubsetting(false);

		//line style

		// ---------------------------------------------------------
		$cnt = 0;	
		$cnt_vouchers = count($vouchersArr);
		foreach($vouchersArr as $voucher)
		{
			//zemi go pay_attempt record-ot za sekoj vaucer
			$payAttemptDataPerVoucher = $this->getPayAttempt($voucher->attempt_id);
			
			//zemi ja ponudata		
			$deal = $dealsModel->getData(array("id" => $voucher->deal_id));
			
			//zemi ja opcijata		
			$dealOption = $dealOptionModel->getData(array("id" => $voucher->deal_option_id));

			//zemi go partnerot
			$partnerData = $partnersModel->getData(array('id' => $deal[0]->partner_id));
			
			//zgolemi go brojacot
			$cnt++;
			
			
			//SETIRAJ GLAVEN X I Y PROMENLIVI
			if($cnt%2)
			{
				$main_X = PDF_MARGIN_LEFT + 0;
				$main_Y = PDF_MARGIN_TOP + 0;
			}
			/******************************* Add a page ******************/
			
			// ---------------------------------------------------------
			// This method has several options, check the source code documentation for more information.
			if($cnt%2)
				$pdf->AddPage();
			
			// set text shadow effect
			//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
			
			/******************************* Благодариме на довербата! ******************/
			if($cnt%2)
			{
				$curr_X = $main_X;
				$curr_Y = $main_Y;
				
				if($cnt == 1)
					$html = "Ви Благодариме на довербата!";
				else
					$html = "";
				$pdf->SetFont('dejavusans', 'B', 16, '', true);
				$pdf->SetFillColor(0,0,0);
				$pdf->SetTextColor(0,0,0);
				$pdf->writeHTMLCell(0, 0, $curr_X, $curr_Y, $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='C', $autopadding=true);
			}
			
			
			/******************************* Dodadi round rectangle - ramka ******************/
			//Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
			$curr_X = $pdf->GetX() ;
			$curr_Y = $pdf->GetY() + 5 + ($cnt%2 == 0 ? 5: 0);
			//$pdf->Image(K_PATH_IMAGES.'voucher_bg.png', $curr_X, $curr_Y, 0, 0, 'PNG');
			//$pdf->setPageMark();
			//$pdf->Rect($curr_X, $curr_Y, 120, 120);	
			$pdf->RoundedRect($curr_X, $curr_Y, 180, 117, 3.50, '1111', "", array('width' => 0.3 , 'cap' => 'round', 'join' => 'miter', 'dash' => 0, 'color' => array(0,0,0)));	

			/******************************* TEXT  - glaven tekst of ponudata******************/
			$curr_X = $pdf->GetX() + 3;
			$curr_Y = $pdf->GetY() + 5 + ($cnt%2 == 0 ? 5: 0);
			$logo_coor_Y = $curr_Y;
			/* STARO
			if ($payAttemptDataPerVoucher[0]->gift)
				$title_mk = $deal[0]->title_mk_clean;
			else
				$title_mk = $deal[0]->title_mk;
			*/

			// $title_mk = $dealOption[0]->title_mk_clean;	
				
			$title_mk = trim(strip_tags($dealOption[0]->title_mk_clean));

			// $title_mk = $title_mk.$title_mk.$title_mk.$title_mk;
			
			if(mb_strlen ($title_mk) > 260)
			{
				$mb_srt_pos = mb_strpos($title_mk, ' ', 240);
				
				$html = mb_substr($title_mk, 0 , $mb_srt_pos)."...";
			}
			else
				$html = $title_mk;
			
			if($deal[0]->card)
				$html = 'Подарок "kupinapopust" електронска картичка во вредност од '.$html;	
				
			$pdf->SetFont('dejavusans', 'B', 11, '', true);
			$pdf->SetFillColor(0,0,0);
			$pdf->SetTextColor(0,0,0);
			$pdf->writeHTMLCell(135, 0, $curr_X, $curr_Y, $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=true);
			
			/******************************* Dodadi LOGO slika ******************/
			
			$curr_X = $pdf->GetX() + 139;
			$curr_Y = $logo_coor_Y + 1;
			$pdf->Image(K_PATH_IMAGES.'logo_kupinapopust.png', $curr_X, $curr_Y, 0, 0, 'PNG');
			
			
			/******************************* TEXT  - Ваучерот е наменет за:******************/
			$curr_X = $pdf->GetX() + 5;
			$curr_Y = $pdf->GetY() + 5;
			$html = "Ваучерот е наменет за:";
				
			$pdf->SetFont('dejavusans', '', 10, '', true);
			$pdf->SetFillColor(0,0,0);
			$pdf->SetTextColor(255,255,255);
			$pdf->writeHTMLCell(50, 0, $curr_X, $curr_Y, $html, $border=0, $ln=0, $fill=1, $reseth=true, $align='J', $autopadding=true);
			
			
			/******************************* TEXT  - Потребно е да се знае дека:******************/
			$curr_X = $pdf->GetX() + 23;
			$curr_Y = $pdf->GetY();
			$html = "Потребно е да се знае дека:";
			
			$pdf->SetFont('dejavusans', '', 10, '', true);
			$pdf->SetFillColor(0,0,0);
			$pdf->SetTextColor(255,255,255);
			$pdf->writeHTMLCell(60, 0, $curr_X, $curr_Y, $html, $border=0, $ln=1, $fill=1, $reseth=true, $align='J', $autopadding=true);
			
			
			/******************************* TEXT  - info za partnerot ******************/
			$curr_X = $pdf->GetX() + 5;
			$curr_Y = $pdf->GetY() + 2;

			$partnerNameFull = trim(strip_tags($partnerData[0]->name));
			$partnerNameFinal = "";

			if(mb_strlen ($partnerNameFull) > 80)
			{
				$mb_srt_pos = mb_strpos($partnerNameFull, ' ', 60);
				
				$partnerNameFinal = mb_substr($partnerNameFull, 0 , $mb_srt_pos)."...";
			}
			else
				$partnerNameFinal = $partnerNameFull;

			$html = '<span style="font-size: 10;"><b>'.$partnerNameFinal.'</b></span>';
			
			//if(substr($partnerData[0]->adress_mk, 0, 3) != "<p>")
			$html .= "<br>";
			
			$infoZaPartnerFull = trim(strip_tags($partnerData[0]->adress_mk,"<br/><br><BR/><BR><a><A>"));
			$infoZaPartnerFinal = "";

			if(mb_strlen ($infoZaPartnerFull) > 440)
			{
				$mb_srt_pos = mb_strpos($infoZaPartnerFull, ' ', 400);
				
				$infoZaPartnerFinal = mb_substr($infoZaPartnerFull, 0 , $mb_srt_pos)."...";
			}
			else
				$infoZaPartnerFinal = $infoZaPartnerFull;


			$html .= '<span style="font-size: 8;">'.$infoZaPartnerFinal.'</span>';
			
			$pdf->SetFont('dejavusans', '', 8, '', true);
			$pdf->SetFillColor(0,0,0);
			$pdf->SetTextColor(0,0,0);
			$pdf->writeHTMLCell(65, 0, $curr_X, $curr_Y, $html, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
			

			/******************************* NAJDI GO Y-ot za Broj na vaucer  ******************/
			$curr_Y_BrojNaVaucer = $pdf->GetY() + 59;

			/******************************* TEXT  - site ostanati detali detali  ******************/
			$curr_X = $pdf->GetX();
			$curr_Y = $pdf->GetY();
			$html = '<ul>';
			if(!$deal[0]->card)
			{
				$html .= '<li>';
				$html .= ''."Ваучерот може да се искористи од ".date("d/m/Y", strtotime($dealOption[0]->valid_from))." до ".date("d/m/Y", strtotime($dealOption[0]->valid_to)).'';
				$html .= '</li>';
			}
			
			$partner_terms = $this->PdfCustomReplace($partnerData[0]->terms_mk);
			if($partner_terms!="")
			{
				$html .= '<li>';
				$html .= ''.$partner_terms.'';
				$html .= '</li>';
			}
			
			if(!$deal[0]->card)
			{
				$html .= '<li>';
				$html .= 'Ваучерот станува валиден '.($payAttemptDataPerVoucher[0]->gift ? "по добивањето на овој подарок" : "по активирање на попустот").'/не се заменува за пари/може да биде употребен еднократно.';
				$html .= '</li>';
				$html .= '<li>';
				$html .= 'Инструкции за искористување на ваучерот:<br/>';
				

				// $html .= '1. Испечатете го ваучерот (доколку немате принтер доволно е само да го знаете вашиот <b>Единствен код</b>. Запишете си го во мобилен или на парче хартија, но внимавајте при препишувањето).<br>';
				// $html .= '2. Предадете го Ваучерот кај давателот на услугата или кажете го вашиот <b>Единствен код</b>. Без испечатен ваучер давателот на услугата ќе ви побара лична карта.<br>';
				// $html .= '3. Уживајте :) !!!';
				// $html .= '';

				$html .= $this->NewLine2Br($dealOption[0]->instrukcii_za_iskoristuvanje);


				$html .= '</li>';
			}
			else
			{
				$html .= '<li>';
				$html .= 'Како да го активирате подарениот кредит?<br/>';
				$html .= '- Внесете го  <b>Единствениот код</b> што го добивте на вашиот E-mail и кликнете <b>Активирај</b>. По успешната верификација поените во вашиот профил ќе се зголемат за подарената сума.<br>';
				$html .= '- Доколку внесете е-mail на пријател поените му ги подарувате нему и тие ќе се препишат на неговиот профил.<br>';
				$html .= '';
				$html .= '</li>';
			}
			
			
			$html .= '</ul>';
			
			$pdf->SetFont('dejavusans', '', 8, '', true);
			$pdf->SetFillColor(0,0,0);
			$pdf->SetTextColor(0,0,0);
			$pdf->writeHTMLCell(110, 0, $curr_X, $curr_Y, $html, $border=0, $ln=1, $fill=0, $reseth=true, $align='L', $autopadding=true);
			
			
			/******************************* TEXT  - Broj na vaucer, opis na usluga i tn,. ******************/
			$curr_X = $pdf->GetX() + 5;
			$curr_Y = $curr_Y_BrojNaVaucer + 5;

				///broj na vaucer
			$html = '<span ><b>Број на ваучер/фактура:</b> '.$voucher->id.'</span><br/>';

				///cena
			$cenaNaVaucer = ($deal[0]->tip == 'cena_na_vaucer' ? $dealOption[0]->price_voucher : $dealOption[0]->price_discount);
			// $platena_cena = $cenaNaVaucer - round($voucherAttemptArr[0]->points_used / $voucherAttemptArr[0]->quantity);
			// if($platena_cena < 0)
			// 	$platena_cena = 0;

			$html .= '<span ><b>Цена:</b> '.$cenaNaVaucer.' ден.</span><br/>';

			//iskoristeni poeni
			$iskoristeni_poeni = round($voucherAttemptArr[0]->points_used / $voucherAttemptArr[0]->quantity);
			$html .= '<span ><b>Искористени поени:</b> '.$iskoristeni_poeni.' поени (ден.)</span><br/>';

				///opis na usluga
			$categoriesModel = new Categories_Model() ;
			$categoryData = $categoriesModel->getData(array("id"=> $deal[0]->category_id));

			$opisNaUslugaTxt = "";

			if(count($categoryData) > 0)
				$opisNaUslugaTxt = $categoryData[0]->service_desc;
			
			if($deal[0]->card) 
					$opisNaUslugaTxt = "Kupinapopust картичка";

			if($opisNaUslugaTxt != "")	
				$html .= '<span ><b>Опис на услуга:</b> '.$opisNaUslugaTxt.'</span><br/>';
			
				///datum na transakcija
				//date("d/m/Y H:i", strtotime($voucher->time))
			$html .= '<span ><b>Датум на трансакција:</b> '.date("d/m/Y", strtotime($voucher->time)).'</span>';
						
			$pdf->SetFont('dejavusans', '', 7, '', true);
			$pdf->SetFillColor(0,0,0);
			$pdf->SetTextColor(0,0,0);
			$pdf->writeHTMLCell(65, 0, $curr_X, $curr_Y, $html, $border=1, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
			


			/******************************* Setiraj koordinati za Email ******************/

			if($cnt%2)
			{
				$curr_Y_Email = 125;
				//$curr_Y = 125;
			}
			else
			{
				$curr_Y_Email = 251;
				//$curr_Y = 251;
			}	
			
			//$html = '';
			
			// $pdf->SetFont('dejavusans', '', 6, '', true);
			// $pdf->SetFillColor(0,0,0);
			// $pdf->SetTextColor(0,0,0);
			// $pdf->writeHTMLCell(70, 0, $curr_X, $curr_Y, $html, $border=1, $ln=0, $fill=0, $reseth=true, $align='J', $autopadding=true);
			
			/******************************* TEXT  - email******************/
			$curr_X = $pdf->GetX() + 10;
			$curr_Y = $curr_Y_Email;
			// $display_email = ($payAttemptDataPerVoucher[0]->copy_email) ? $payAttemptDataPerVoucher[0]->copy_email : $payAttemptDataPerVoucher[0]->email;
			$display_email = $payAttemptDataPerVoucher[0]->email;
			$html = "<b>Email адреса</b><br>".(strlen($display_email) > 38 ? substr($display_email,0,37)."..." : $display_email );
			
			$pdf->SetFont('dejavusans', '', 10, '', true);
			$pdf->SetFillColor(255,255,255);
			$pdf->writeHTMLCell(98, 0, $curr_X, $curr_Y, $html, $border=1, $ln=1, $fill=1, $reseth=true, $align='C', $autopadding=true);
			
			/******************************* TEXT  - info za kupinapopust ******************/
			$curr_X = $pdf->GetX() + 5;
			$curr_Y = $pdf->GetY() + 1;	
			
			$html = '<br/><br/><br/><br/><br/><b>Имате прашање?</b> Пишете на contact@kupinapopust.mk или јавете се: Фиксен тел. 02 3 256 027, 2 3 255 918, моб. 078 439 829 (секој раб. ден од 09:00 до 17:00 часот)';
			
			$pdf->SetFont('dejavusans', '', 6, '', true);
			$pdf->SetFillColor(0,0,0);
			$pdf->SetTextColor(0,0,0);
			$pdf->writeHTMLCell(70, 0, $curr_X, $curr_Y, $html, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
			
			/******************************* TEXT  - procent******************/
			$edinstvenCodeWidth = 71;
			if($dealOption[0]->price > 0 && !$payAttemptDataPerVoucher[0]->gift)
			{
				$curr_X = $pdf->GetX() + 5;
				$curr_Y = $pdf->GetY();
				$html = "<b>Попуст</b><br>".(int) round(100 - ($dealOption[0]->price_discount / $dealOption[0]->price) * 100)."%";
				
				$pdf->SetFont('dejavusans', '', 10, '', true);
				$pdf->SetFillColor(255,255,255);
				$pdf->writeHTMLCell(17, 0, $curr_X, $curr_Y, $html, $border=1, $ln=0, $fill=1, $reseth=true, $align='C', $autopadding=true);
				
				$edinstvenCodeWidth = 49;
			}
			
			//proveri dali e iskoristen
			$iskoristen_ili_izminat = "";

			$iskoristen_ili_izminat = ($voucher->used == 1) ? '  <span style="color: red; font-weight: bold;">Искористен</span>' : "";

			if($iskoristen_ili_izminat == "")
				$iskoristen_ili_izminat = (time() > strtotime($dealOption[0]->valid_to) ) ? '  <span style="color: red; font-weight: bold;">Истечен</span>' : "";

			
			$curr_X = $pdf->GetX() + 5;
			$curr_Y = $pdf->GetY();
			$html = "<b>Единствен Код</b><br>".$voucher->code.$iskoristen_ili_izminat;
			
			$pdf->SetFont('dejavusans', '', 10, '', true);
			$pdf->SetFillColor(255,255,255);
			$pdf->writeHTMLCell($edinstvenCodeWidth, 0, $curr_X, $curr_Y, $html, $border=1, $ln=1, $fill=1, $reseth=true, $align='C', $autopadding=true);
			
			/******************************* TEXT  - copyright ******************/
			$curr_X = $pdf->GetX() + 79;
			$curr_Y = $pdf->GetY() + 1.5;
			$html = '<br/><br/>Copyright © Kupinapopust.mk, Груп Поинт дооел, ул.Никола Вапцаров бр.3/1, реон 8, нас. Центар, 1000, Скопје, Македонија';
			
			$pdf->SetFont('dejavusans', '', 6, '', true);
			$pdf->SetTextColor(0,0,0);
			$pdf->writeHTMLCell(75, 0, $curr_X, $curr_Y, $html, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
			
			/******************************* 2D BarCode  ******************/
			$curr_X = $pdf->GetX() + 1;
			$curr_Y = $pdf->GetY()- 10.5;
			// set style for barcode
			$barcode_style = array(
					'border' => false,
					'padding' => 0,
					'fgcolor' => array(255,99,0),
					'bgcolor' => array(255,255,255)
					);

			//star kod 
			// $code = (($payAttemptDataPerVoucher[0]->copy_email) ? $payAttemptDataPerVoucher[0]->copy_email : $payAttemptDataPerVoucher[0]->email)." ".$voucher->code;

			//nov kod
			$code = Kohana::config('facebook.facebookSiteProtocol').'://' . Kohana::config('facebook.facebookSiteURL')."/validate/voucher/".$voucher->code;

			//iscrtaj go QR kodot
			$pdf->write2DBarcode($code, 'QRCODE,L', $curr_X, $curr_Y , 20, 20, $barcode_style, 'N');
			
			
			/******************************* Dodadi CUT slika ******************/
			if($cnt_vouchers > $cnt && $cnt%2)
			{
				$curr_X_scissors = $pdf->GetX();
				$curr_Y_scissors = $pdf->GetY() + 4.5;
				$curr_X_dotline = $curr_X_scissors + 5;
				$curr_Y_dotline = $curr_Y_scissors + 1;
				
				$pdf->Image(K_PATH_IMAGES.'cut_here.png', $curr_X_scissors, $curr_Y_scissors, 0, 0, 'PNG');
				$pdf->Line($curr_X_dotline, $curr_Y_dotline, $curr_X_dotline + 175, $curr_Y_dotline, array('width' => 0.3 , 'cap' => 'round', 'join' => 'miter', 'dash' => 2, 'color' => array(0,0,0)));
			}
		}
	
		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
		//$pdf->Output(DOCROOT . '/'.$voucher_attempt_id.'.pdf', ($type == "email" ? "F" : "D"));
		$data = $pdf->Output("kupinapopust.pdf", $type);
		
		return $data;
	}


}