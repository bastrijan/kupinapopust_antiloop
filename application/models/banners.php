<?php defined('SYSPATH') or die('No direct script access.');

class Banners_Model extends Default_Model {

	protected $_tableName = 'banners';
	
	public function __construct() {
		parent::__construct();
	}
	
	public function delete($id) {
		return $this->db->delete($this->_tableName, array('id' => $id));
	}

	public function getBanner($banner_location = 1) {
		
		$sql = "SELECT id, banner_ext, banner_link, impressions_cnt FROM $this->_tableName WHERE active = 1 && banner_location = $banner_location AND (impressions_cnt < impressions_limit OR impressions_limit = 0) ORDER BY RAND() LIMIT 1";
		$res = $this->db->query($sql)->result_array();
		
        if ($res)
            return $res[0];
        return false;
	}
}