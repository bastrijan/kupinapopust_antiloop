<?php defined('SYSPATH') OR die('No direct access allowed.');

$lang = array
(
        'newsletter_subscribe' => 'You have successfully subscribed your e-mail address!',
        'user_account' => 'User account!',
        'payment_success' => 'Voucher reservation!',
        'send_voucher' => 'Payment confirmation!',
        'gift_voucher' => 'Gift Voucher!'
);