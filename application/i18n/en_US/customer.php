<?php defined('SYSPATH') OR die('No direct access allowed.');

$lang = array
(
        'Кориснички профил' => 'User account',
        'Ваучер' => 'Voucher',
        'по цена од' => 'priced',
        'Кодот на ваучерот е' => 'The code is',
        'Поени' => 'Points',
        'Вкупно имате' => 'You have',
        'Kupinapopust поени' => 'Kupinapopust.mk points',
        'Вашиот Е-mail' => 'Your Е-mail',
        'Вашата лозинка' => 'Your password',
        'Логирај се' => 'Login',
        'Кориснички профил - најава' => 'User account access'
);