<?php defined('SYSPATH') OR die('No direct access allowed.');

$lang = array
(
        'newsletter_subscribe' => 'Успешно го пријавивте вашиот e-mail!',
        'user_account' => 'Кориснички профил!',
        'payment_success' => 'Резервација на ваучер!',
        'send_voucher' => 'Потврда за извршено плаќање!',
        'gift_voucher' => 'Подарок ваучер!'
);