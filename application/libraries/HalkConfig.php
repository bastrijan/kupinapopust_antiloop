<?php

class HalkConfig {

    public static $clientId           = "180000015";    
    public static $storeId            = "123456";
            
    public static $okUrl                = "/pay/response";
    public static $errorUrl             = "/pay/error?exitif=true";
    
    public static $encoding             = "UTF8";
    
    /*
		There are seven type of transactions : Auth, Void, Credit, PreAuth, PostAuth, OrderStatus, OrderHistory
		A unique transastion id returns after each transaction, it is used for reference purposes for some sort of transactions, explained below.
		Auth : Sale
		Void : Canceling sale, it must be done in same day that sale was done.  Sale's order id must be provided.
		Credit : Canceling sale and refunding provisioned amount  during sale process.  It can be done after settlement. Transaction id must be provided.
		PreAuth : Pre Authorization, it starts a sale request but it doesn't end process.
		PostAuth : Post Authorization, it ends sale process started before by Pre Authorization, transaction id must be provided.
		OrderStatus : Reporting request for order's status.
		OrderHistory : Reporting request for order's history.	
	*/
    public static $transacationType     = "Auth";

    //public static $preauthorizationUrl  = "https://epay.halkbank.mk/servlet/est3Dgate";
	public static $preauthorizationUrl  = "/pay/localtest";

    public static $gatewayIP            = "80.77.147.43";          
}