<?php

require_once("CPayConfig.php") ;

class Dms2 {

  public function completeTransaction($transactionId, $amount) {

    $amount = round($amount, 2) * 100 ;

    $md5String = CPayConfig::$merchantId . "{$transactionId}{$amount}" . CPayConfig::$merchatnPassword ;
    $md5String = md5($md5String) ;

    $params["MerchantID"] = CPayConfig::$merchantId ;
    $params["cPayPaymentRef"] = $transactionId ;
    $params["Amount"] = $amount ;
    $params["MD5"] = $md5String ;


    $responseString = $this->getResponse($params) ;

    return $this->parseResponse($responseString) ;
  }

// completeTransaction()

  private function getResponse(array $completeAuthorizationRq) {

    $path = dirname(__FILE__) ;

    //ini_set("soap.wsdl_cache_enabled", 0);

    $client = new SoapClient("$path/PaymentCompletionWS_20110328.wsdl",
                    array(
                    //"trace"=> 1,
                    //'exceptions' => true
            )) ;


    $response = $client->__soapCall("completeAuthorization", array("parameters" => array("rq" => $completeAuthorizationRq))) ;

    // $client->__getLastRequest();
    // $client->__getLastResponse();
    // za tipovite koi se dobieni od wsdl ot:
    // $client->__getTypes
    // za funkciite
    // $client->__getFunctions

    return $response ;
  }

// getResponse()

  public function parseResponse($responseObject) {

    $result = array() ;

    // true, false
    $success = $responseObject->completeAuthorizationResult->Success ;

    // 1 - transaction is denied by the issuing bank
    // 2 - transaction is denied by cpay due to spedific validations
    // 3 - techincal problems
    $reasonOfDecline = $responseObject->completeAuthorizationResult->ReasonOfDecline ;

    // if $reasonOfDecline
    if ($reasonOfDecline) {
      $errorDescription = $responseObject->completeAuthorizationResult->ErrorDecription ;
    }
    
    $responseObject = (array) $responseObject->completeAuthorizationResult ;
    $responseString = print_r($responseObject, true) ;


    if ($success) {
      $result["result"] = "OK" ;
    }
    else {
      $result["result"] = "FAILED" ;
    }

    $result["plain_response"] = $responseString ;

    return $result ;
  }

// parseResponse()
}

// class