<?php

class CPayConfig {
	
	public static $merchantId           = "1000000393"; //OLD "1000000132";
	public static $merchantName         = "kupinapopust.mk";//OLD "DR.BELOVSKI KOMPANIJA";
	//public static $merchatnPassword     = "J0v1c@Kup1N@P0pusT2012";//OLD = "tJ?97fA=";
	public static $merchatnPassword     = "TEST_PASS";//TEST
	
	public static $okUrl                = "/pay_casys/response";
	public static $errorUrl             = "/pay_casys/error?exitif=true";
	public static $currency             = "MKD";

	//shopping cart
	public static $okUrlShopCart          = "/pay_casys_shop_cart/response";
	public static $errorUrlShopCart       = "/pay_casys_shop_cart/error?exitif=true";
	
	//mobile variables
	public static $okUrlMobile          = "/pay_casys_mobile/response";
	public static $errorUrlMobile       = "/pay_casys_mobile/error?exitif=true";
	
	
	//public static $preauthorizationUrl  = "https://www.cpay.com.mk/client/Page/default.aspx?xml_id=/mk-MK/.loginToPay/.simple/";//ZA IFRAME
	//public static $preauthorizationUrl  = "https://www.cpay.com.mk/client/Page/default.aspx?xml_id=/mk-MK/.loginToPay/";
	//public static $preauthorizationUrl  = "https://www.cpay.com.mk/Client/page/default.aspx?xml_id=/mk-MK/.TestLoginToPay/";//TEST
	public static $preauthorizationUrl  = "/pay_casys/localtest";
	//public static $gatewayIP            = "80.77.147.43"; // Ova e IP adresa na CASYS
	public static $gatewayIP            = "127.0.0.1"; //TEST    
	
	public static $dms2Url             = "https://www.cpay.com.mk/service/PaymentCompletionWS.asmx";
	
}
