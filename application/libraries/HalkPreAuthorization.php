<?php
require_once "HalkConfig.php";

class HalkPreAuthorization {


    private $isPreauthorization     = 0;
    private $ammuntToPay            = 0;

    private $orderId                = "";
    private $clientId               = "";
    private $storeId                = "";
    private $paymentOkUrl           = "";
    private $paymentErrorUrl        = "";
    private $installment            = "";
    private $transacationType       = "";
    private $rnd                    = 0;    

//    private $originalAmmount        = "";
//    private $orriginalCurrency      = "";

//    private $firstName              = "";
//    private $lastName               = "";
//    private $address                = "";
//    private $city                   = "";
//    private $zip                    = "";
//    private $country                = "";
//    private $tel                    = "";
//    private $email                  = "";

    private $lanuage                = "mk";

    public $hasHttp                  = false;

    /**
     * Names that should be sent to cpay:
     *
     * isPreauthorization
     * AmountToPay
     * AmountCurrency
     * Details1
     * Details2
     * PayToMerchant
     * MerchantName
     * PaymentOKURL
     * PaymentFailURL
     * OriginalAmount
     * OriginalCurrency
     * FirstName
     * LastName
     * Address
     * City
     * Zip
     * Country
     * Telephone
     * Email
     * CheckSumHeader
     * CheckSum
     */

    function __construct($ammount, $orderId) {

        $this->setAmmountToPay($ammount);

        $s = "";
        if ($this->hasHttp) {
            $s = "s";
        }
        
        $this->clientId         = HalkConfig::$clientId;
        $this->storeId          = HalkConfig::$storeId;
        $this->transacationType = HalkConfig::$transacationType;
        
        $this->paymentOkUrl     = "http{$s}://" . $_SERVER["HTTP_HOST"] . HalkConfig::$okUrl;
        $this->paymentErrorUrl  = "http{$s}://" . $_SERVER["HTTP_HOST"] . HalkConfig::$errorUrl;

        $this->orderId          = $orderId;
        $this->rnd              = microtime();
    }// constructor


    public function getUrl() {
        return HalkConfig::$preauthorizationUrl;
    }

    public function getInputStrings() {

        $urlParams = $this->getPostParams();
        $str = '';

        foreach ($urlParams as $key => $value) {
            $str .= ' <input type="hidden" name="' . $key .'" value="' . $value . '">';            
        }
        
        return $str;
    }


    public function getPostParams() {

        $urlParams = array();

//        if ($this->isPreauthorization) {
//            $urlParams["isPreauthorization"] = $this->isPreauthorization;
//        }
        if ($this->clientId) {
            $urlParams["clientid"] = $this->clientId;
        }
        
        if ($this->ammuntToPay) {
            $urlParams["amount"] = $this->ammuntToPay;
        }
        if ($this->orderId) {
            $urlParams["oid"] = $this->orderId;
        }
        
        if ($this->paymentOkUrl) {
            $urlParams["okUrl"] = $this->paymentOkUrl;
        }
        if ($this->paymentErrorUrl) {
            $urlParams["failUrl"] = $this->paymentErrorUrl;
        }
        if ($this->installment) {
            $urlParams['taksit'] = $this->installment;
        }
        if ($this->transacationType) {
            $urlParams['islemtipi'] = $this->transacationType;
        }
        if ($this->rnd) {
            $urlParams['rnd'] = $this->rnd;
        }
        if ($this->storeId) {
            $urlParams['storeid'] = $this->storeId;
        }
        

        $hashstr = $this->clientId . $this->orderId . $this->ammuntToPay 
                 . $this->paymentOkUrl . $this->paymentErrorUrl 
                 . $this->transacationType . $this->installment . $this->rnd 
                 . $this->storeId;
        
        $urlParams['hash'] = base64_encode(pack('H*',sha1($hashstr)));

//
//        if ($this->originalAmmount) {
//            $urlParams["OriginalAmount"] = $this->originalAmmount;
//        }
//        if ($this->orriginalCurrency) {
//            $urlParams["OriginalCurrency"] = $this->orriginalCurrency;
//        }
//        if ($this->firstName) {
//            $urlParams["FirstName"] = $this->firstName;
//        }
//        if ($this->lastName) {
//            $urlParams["LastName"] = $this->lastName;
//        }
//        if ($this->address) {
//            $urlParams["Address"] = $this->address;
//        }
//        if ($this->city) {
//            $urlParams["City"] = $this->city;
//        }
//        if ($this->zip) {
//            $urlParams["Zip"] = $this->zip;
//        }
//        if ($this->country) {
//            $urlParams["Country"] = $this->country;
//        }
//        if ($this->tel) {
//            $urlParams["Telephone"] = $this->tel;
//        }
//        if ($this->email) {
//            $urlParams["Email"] = $this->email;
//        }
//
//        $urlParams["CheckSumHeader"]    = $this->getHeader();
//        $urlParams["CheckSum"]          = $this->getCheckSum();

        return $urlParams;

    }// getPostParams()


//    public function getHeader() {
//
//        $count = 0;
//
//        $headerString = "";
//        $suffixString = "";
//
//        if ($this->isPreauthorization) {
//            $count++;
//            $headerString .= "isPreauthorization,";
//            $suffixString .= $this->getFormattedLength($this->isPreauthorization);
//        }
//        if ($this->ammuntToPay) {
//            $count++;
//            $headerString .= "AmountToPay,";
//            $suffixString .= $this->getFormattedLength($this->ammuntToPay);
//        }
//        if ($this->ammountCurrency) {
//            $count++;
//            $headerString .= "AmountCurrency,";
//            $suffixString .= $this->getFormattedLength($this->ammountCurrency);
//        }
//        if ($this->details1) {
//            $count++;
//            $headerString .= "Details1,";
//            $suffixString .= $this->getFormattedLength($this->details1);
//        }
//        if ($this->details2) {
//            $count++;
//            $headerString .= "Details2,";
//            $suffixString .= $this->getFormattedLength($this->details2);
//        }
//        if ($this->merchantId) {
//            $count++;
//            $headerString .= "PayToMerchant,";
//            $suffixString .= $this->getFormattedLength($this->merchantId);
//        }
//        if ($this->merchantName) {
//            $count++;
//            $headerString .= "MerchantName,";
//            $suffixString .= $this->getFormattedLength($this->merchantName);
//        }
//        if ($this->paymentOkUrl) {
//            $count++;
//            $headerString .= "PaymentOKURL,";
//            $suffixString .= $this->getFormattedLength($this->paymentOkUrl);
//        }
//        if ($this->paymentErrorUrl) {
//            $count++;
//            $headerString .= "PaymentFailURL,";
//            $suffixString .= $this->getFormattedLength($this->paymentErrorUrl);
//        }
//        if ($this->originalAmmount) {
//            $count++;
//            $headerString .= "OriginalAmount,";
//            $suffixString .= $this->getFormattedLength($this->originalAmmount);
//        }
//        if ($this->orriginalCurrency) {
//            $count++;
//            $headerString .= "OriginalCurrency,";
//            $suffixString .= $this->getFormattedLength($this->orriginalCurrency);
//        }
//        if ($this->firstName) {
//            $count++;
//            $headerString .= "FirstName,";
//            $suffixString .= $this->getFormattedLength($this->firstName);
//        }
//        if ($this->lastName) {
//            $count++;
//            $headerString .= "LastName,";
//            $suffixString .= $this->getFormattedLength($this->lastName);
//        }
//        if ($this->address) {
//            $count++;
//            $headerString .= "Address,";
//            $suffixString .= $this->getFormattedLength($this->address);
//        }
//        if ($this->city) {
//            $count++;
//            $headerString .= "City,";
//            $suffixString .= $this->getFormattedLength($this->city);
//        }
//        if ($this->zip) {
//            $count++;
//            $headerString .= "Zip,";
//            $suffixString .= $this->getFormattedLength($this->zip);
//        }
//        if ($this->country) {
//            $count++;
//            $headerString .= "Country,";
//            $suffixString .= $this->getFormattedLength($this->country);
//        }
//        if ($this->tel) {
//            $count++;
//            $headerString .= "Telephone,";
//            $suffixString .= $this->getFormattedLength($this->tel);
//        }
//        if ($this->email) {
//            $count++;
//            $headerString .= "Email,";
//            $suffixString .= $this->getFormattedLength($this->email);
//        }
//
//        $resp = str_pad($count, 2, "0", STR_PAD_LEFT) . "{$headerString}{$suffixString}";
//
//        return $resp;
//
//    }// getHeader
//
//
//    private function getFormattedLength($string) {
//        return str_pad( mb_strlen($string), 3, "0", STR_PAD_LEFT);
//    }
//
//    public function getHeaderMd5() {
//        return md5($this->getHeader());
//    }// getHeaderMd5()
//
//
//
//
//    public function getCheckSum() {
//
//        $checkSum = "";
//
//        if ($this->isPreauthorization) {
//            $checkSum .= ($this->isPreauthorization);
//        }
//        if ($this->ammuntToPay) {
//            $checkSum .= ($this->ammuntToPay);
//        }
//        if ($this->ammountCurrency) {
//            $checkSum .= ($this->ammountCurrency);
//        }
//        if ($this->details1) {
//            $checkSum .= ($this->details1);
//        }
//        if ($this->details2) {
//            $checkSum .= ($this->details2);
//        }
//        if ($this->merchantId) {
//            $checkSum .= ($this->merchantId);
//        }
//        if ($this->merchantName) {
//            $checkSum .= ($this->merchantName);
//        }
//        if ($this->paymentOkUrl) {
//            $checkSum .= ($this->paymentOkUrl);
//        }
//        if ($this->paymentErrorUrl) {
//            $checkSum .= ($this->paymentErrorUrl);
//        }
//        if ($this->originalAmmount) {
//            $checkSum .= ($this->originalAmmount);
//        }
//        if ($this->orriginalCurrency) {
//            $checkSum .= ($this->orriginalCurrency);
//        }
//        if ($this->firstName) {
//            $checkSum .= ($this->firstName);
//        }
//        if ($this->lastName) {
//            $checkSum .= ($this->lastName);
//        }
//        if ($this->address) {
//            $checkSum .= ($this->address);
//        }
//        if ($this->city) {
//            $checkSum .= ($this->city);
//        }
//        if ($this->zip) {
//            $checkSum .= ($this->zip);
//        }
//        if ($this->country) {
//            $checkSum .= ($this->country);
//        }
//        if ($this->tel) {
//            $checkSum .= ($this->tel);
//        }
//        if ($this->email) {
//            $checkSum .= ($this->email);
//        }
//
//        $checkSum = $this->getHeader() .  $checkSum . CPayConfig::$merchatnPassword;
//
//        $checkSum = md5($checkSum);
//
//        return $checkSum;
//
//    }// getCheckSum


    public function getIsPreauthorization() {
        return $this->isPreauthorization;
    }

    public function setIsPreauthorization($isPreauthorization) {
        $this->isPreauthorization = $isPreauthorization;
    }

    public function getAmmuntToPay() {
        return $this->ammuntToPay;
    }

    public function setAmmountToPay($ammuntToPay) {

//        $ammuntToPay = round($ammuntToPay, 2) * 100;

        $this->ammuntToPay = $ammuntToPay;
    }
//
//    public function getAmmountCurrency() {
//        return $this->ammountCurrency;
//    }
//
//    public function setAmmountCurrency($ammountCurrency) {
//        $this->ammountCurrency = $ammountCurrency;
//    }

//    public function getDetails1() {
//        return $this->details1;
//    }
//
//    public function setDetails1($details1) {
//        $this->details1 = $details1;
//    }
//
//    public function getDetails2() {
//        return $this->details2;
//    }
//
//    public function setDetails2($details2) {
//        $this->details2 = $details2;
//    }

    public function getMerchantId() {
        return $this->clientId;
    }

    public function setMerchantId($merchantId) {
        $this->clientId = $merchantId;
    }

    public function getPaymentOkUrl() {
        return $this->paymentOkUrl;
    }

    public function setPaymentOkUrl($paymentOkUrl) {
        $this->paymentOkUrl = $paymentOkUrl;
    }

    public function getPaymentErrorUrl() {
        return $this->paymentErrorUrl;
    }

    public function setPaymentErrorUrl($paymentErrorUrl) {
        $this->paymentErrorUrl = $paymentErrorUrl;
    }
   
    public function getLanuage() {
        return $this->lanuage;
    }

    public function setLanuage($lanuage) {

        if ($lanuage!="mk" && $lanuage!="en") {
            throw "Unsupported language $lanuage. only mk, en are supported";
        }

        $this->lanuage = $lanuage;
    }
}
