<?php
require_once "CPayConfig.php";

class Preauthorization {


    private $isPreauthorization     = 0;
    private $ammuntToPay            = 0;
    private $ammountCurrency        = "MKD";
    private $details1               = "";
    private $details2               = "";
    private $merchantId             = "";
    private $merchantName           = "";
    private $paymentOkUrl           = "";
    private $paymentErrorUrl        = "";

    private $originalAmmount        = "";
    private $orriginalCurrency      = "";

    private $firstName              = "";
    private $lastName               = "";
    private $address                = "";
    private $city                   = "";
    private $zip                    = "";
    private $country                = "";
    private $tel                    = "";
    private $email                  = "";
    private $lanuage                = "mk";
    public $hasHttp                  = true;
    private $LoadNewIFrameLook            = 1;
    private $installment            = "01";
    private $cRef                  = "";

    /**
     * Names that should be sent to cpay:
     *
     * isPreauthorization
     * AmountToPay
     * AmountCurrency
     * Details1
     * Details2
     * PayToMerchant
     * MerchantName
     * PaymentOKURL
     * PaymentFailURL
     * OriginalAmount
     * OriginalCurrency
     * FirstName
     * LastName
     * Address
     * City
     * Zip
     * Country
     * Telephone
     * Email
     * LoadNewIFrameLook
     * Installment
     * CRef
     * CheckSumHeader
     * CheckSum
     */

    // $contextType = 1 - regular
    // $contextType = 2 - shopping cart
    // $contextType = 3 - mobile

    function __construct($ammount, $bankStatemenDetails, $extraDetails, $contextType = 1) {

        $this->setAmmountToPay($ammount);

        $s = "";
        if ($this->hasHttp) {
            $s = "s";
        }

        $this->ammountCurrency  = CPayConfig::$currency;
        $this->merchantId       = CPayConfig::$merchantId;
        $this->merchantName     = CPayConfig::$merchantName;

        //this is for $contextType == 1 (default)
        $okUrlFinal = CPayConfig::$okUrl;
        $errorUrlFinal = CPayConfig::$errorUrl; 

        if($contextType == 2)
        {
            $okUrlFinal = CPayConfig::$okUrlShopCart;
            $errorUrlFinal = CPayConfig::$errorUrlShopCart; 
        }
        elseif($contextType == 3)
        {
            $okUrlFinal = CPayConfig::$okUrlMobile;
            $errorUrlFinal = CPayConfig::$errorUrlMobile; 
        }


        $this->paymentOkUrl     = "http{$s}://" . $_SERVER["HTTP_HOST"] . $okUrlFinal ;
        $this->paymentErrorUrl  = "http{$s}://" . $_SERVER["HTTP_HOST"] . $errorUrlFinal ;

        $this->details1         = $bankStatemenDetails;
        $this->details2         = $extraDetails;

    }// constructor


    public function getUrl() {
        return CPayConfig::$preauthorizationUrl;
    }

    public function getIframeUrl () {

        $urlParams = $this->getPostParams();
        $escapedParams = array();

        foreach ($urlParams as $key => $value) {
            $escapedParams[] = $key . '=' . urlencode($value);
        }

        $queryString = implode("&", $escapedParams);
        $url = CPayConfig::$preauthorizationUrl . "&" . $queryString;

        return $url;
    }


    public function getPostParams() {

        $urlParams = array();

        if ($this->isPreauthorization) {
            $urlParams["isPreauthorization"] = $this->isPreauthorization;
        }
        if ($this->ammuntToPay) {
            $urlParams["AmountToPay"] = $this->ammuntToPay;
        }
        if ($this->ammountCurrency) {
            $urlParams["AmountCurrency"] = $this->ammountCurrency;
        }
        if ($this->details1) {
            $urlParams["Details1"] = $this->details1;
        }
        if ($this->details2) {
            $urlParams["Details2"] = $this->details2;
        }
        if ($this->merchantId) {
            $urlParams["PayToMerchant"] = $this->merchantId;
        }
        if ($this->merchantName) {
            $urlParams["MerchantName"] = $this->merchantName;
        }
        if ($this->paymentOkUrl) {
            $urlParams["PaymentOKURL"] = $this->paymentOkUrl;
        }
        if ($this->paymentErrorUrl) {
            $urlParams["PaymentFailURL"] = $this->paymentErrorUrl;
        }
        if ($this->originalAmmount) {
            $urlParams["OriginalAmount"] = $this->originalAmmount;
        }
        if ($this->orriginalCurrency) {
            $urlParams["OriginalCurrency"] = $this->orriginalCurrency;
        }
        if ($this->firstName) {
            $urlParams["FirstName"] = $this->firstName;
        }
        if ($this->lastName) {
            $urlParams["LastName"] = $this->lastName;
        }
        if ($this->address) {
            $urlParams["Address"] = $this->address;
        }
        if ($this->city) {
            $urlParams["City"] = $this->city;
        }
        if ($this->zip) {
            $urlParams["Zip"] = $this->zip;
        }
        if ($this->country) {
            $urlParams["Country"] = $this->country;
        }
        if ($this->tel) {
            $urlParams["Telephone"] = $this->tel;
        }
        if ($this->email) {
            $urlParams["Email"] = $this->email;
        }

        if ($this->LoadNewIFrameLook) {
            $urlParams["LoadNewIFrameLook"] = $this->LoadNewIFrameLook;
        }

        if ($this->installment && $this->installment != "01") {
            $urlParams["Installment"] = $this->installment;
        }

        if ($this->cRef && $this->cRef != "") {
            $urlParams["CRef"] = $this->cRef;
        }

        $urlParams["CheckSumHeader"]    = $this->getHeader();
        $urlParams["CheckSum"]          = $this->getCheckSum();
        
        return $urlParams;

    }// getPostParams()


    public function getHeader() {

        $count = 0;

        $headerString = "";
        $suffixString = "";

        if ($this->isPreauthorization) {
            $count++;
            $headerString .= "isPreauthorization,";
            $suffixString .= $this->getFormattedLength($this->isPreauthorization);
        }
        if ($this->ammuntToPay) {
            $count++;
            $headerString .= "AmountToPay,";
            $suffixString .= $this->getFormattedLength($this->ammuntToPay);
        }
        if ($this->ammountCurrency) {
            $count++;
            $headerString .= "AmountCurrency,";
            $suffixString .= $this->getFormattedLength($this->ammountCurrency);
        }
        if ($this->details1) {
            $count++;
            $headerString .= "Details1,";
            $suffixString .= $this->getFormattedLength($this->details1);
        }
        if ($this->details2) {
            $count++;
            $headerString .= "Details2,";
            $suffixString .= $this->getFormattedLength($this->details2);
        }
        if ($this->merchantId) {
            $count++;
            $headerString .= "PayToMerchant,";
            $suffixString .= $this->getFormattedLength($this->merchantId);
        }
        if ($this->merchantName) {
            $count++;
            $headerString .= "MerchantName,";
            $suffixString .= $this->getFormattedLength($this->merchantName);
        }
        if ($this->paymentOkUrl) {
            $count++;
            $headerString .= "PaymentOKURL,";
            $suffixString .= $this->getFormattedLength($this->paymentOkUrl);
        }
        if ($this->paymentErrorUrl) {
            $count++;
            $headerString .= "PaymentFailURL,";
            $suffixString .= $this->getFormattedLength($this->paymentErrorUrl);
        }
        if ($this->originalAmmount) {
            $count++;
            $headerString .= "OriginalAmount,";
            $suffixString .= $this->getFormattedLength($this->originalAmmount);
        }
        if ($this->orriginalCurrency) {
            $count++;
            $headerString .= "OriginalCurrency,";
            $suffixString .= $this->getFormattedLength($this->orriginalCurrency);
        }
        if ($this->firstName) {
            $count++;
            $headerString .= "FirstName,";
            $suffixString .= $this->getFormattedLength($this->firstName);
        }
        if ($this->lastName) {
            $count++;
            $headerString .= "LastName,";
            $suffixString .= $this->getFormattedLength($this->lastName);
        }
        if ($this->address) {
            $count++;
            $headerString .= "Address,";
            $suffixString .= $this->getFormattedLength($this->address);
        }
        if ($this->city) {
            $count++;
            $headerString .= "City,";
            $suffixString .= $this->getFormattedLength($this->city);
        }
        if ($this->zip) {
            $count++;
            $headerString .= "Zip,";
            $suffixString .= $this->getFormattedLength($this->zip);
        }
        if ($this->country) {
            $count++;
            $headerString .= "Country,";
            $suffixString .= $this->getFormattedLength($this->country);
        }
        if ($this->tel) {
            $count++;
            $headerString .= "Telephone,";
            $suffixString .= $this->getFormattedLength($this->tel);
        }
        if ($this->email) {
            $count++;
            $headerString .= "Email,";
            $suffixString .= $this->getFormattedLength($this->email);
        }

        if ($this->LoadNewIFrameLook) {
            $count++;
            $headerString .= "LoadNewIFrameLook,";
            $suffixString .= $this->getFormattedLength($this->LoadNewIFrameLook);
        }

        if ($this->installment && $this->installment != "01") {
            $count++;
            $headerString .= "Installment,";
            $suffixString .= $this->getFormattedLength($this->installment);
        }

        if ($this->cRef && $this->cRef != "") {
            $count++;
            $headerString .= "CRef,";
            $suffixString .= $this->getFormattedLength($this->cRef);
        }

        $resp = str_pad($count, 2, "0", STR_PAD_LEFT) . "{$headerString}{$suffixString}";

        return $resp;

    }// getHeader


    private function getFormattedLength($string) {
        return str_pad( mb_strlen($string), 3, "0", STR_PAD_LEFT);
    }

    public function getHeaderMd5() {
        return md5($this->getHeader());
    }// getHeaderMd5()




    public function getCheckSum() {

        $checkSum = "";

        if ($this->isPreauthorization) {
            $checkSum .= ($this->isPreauthorization);
        }
        if ($this->ammuntToPay) {
            $checkSum .= ($this->ammuntToPay);
        }
        if ($this->ammountCurrency) {
            $checkSum .= ($this->ammountCurrency);
        }
        if ($this->details1) {
            $checkSum .= ($this->details1);
        }
        if ($this->details2) {
            $checkSum .= ($this->details2);
        }
        if ($this->merchantId) {
            $checkSum .= ($this->merchantId);
        }
        if ($this->merchantName) {
            $checkSum .= ($this->merchantName);
        }
        if ($this->paymentOkUrl) {
            $checkSum .= ($this->paymentOkUrl);
        }
        if ($this->paymentErrorUrl) {
            $checkSum .= ($this->paymentErrorUrl);
        }
        if ($this->originalAmmount) {
            $checkSum .= ($this->originalAmmount);
        }
        if ($this->orriginalCurrency) {
            $checkSum .= ($this->orriginalCurrency);
        }
        if ($this->firstName) {
            $checkSum .= ($this->firstName);
        }
        if ($this->lastName) {
            $checkSum .= ($this->lastName);
        }
        if ($this->address) {
            $checkSum .= ($this->address);
        }
        if ($this->city) {
            $checkSum .= ($this->city);
        }
        if ($this->zip) {
            $checkSum .= ($this->zip);
        }
        if ($this->country) {
            $checkSum .= ($this->country);
        }
        if ($this->tel) {
            $checkSum .= ($this->tel);
        }
        if ($this->email) {
            $checkSum .= ($this->email);
        }

        if ($this->LoadNewIFrameLook) {
            $checkSum .= ($this->LoadNewIFrameLook);
        }

        if ($this->installment && $this->installment != "01") {
            $checkSum .= ($this->installment);
        }

        if ($this->cRef && $this->cRef != "") {
            $checkSum .= ($this->cRef);
        }

        $checkSum = $this->getHeader() .  $checkSum . CPayConfig::$merchatnPassword;

        $checkSum = md5($checkSum);

        return $checkSum;

    }// getCheckSum


    public function getIsPreauthorization() {
        return $this->isPreauthorization;
    }

    public function setIsPreauthorization($isPreauthorization) {
        $this->isPreauthorization = $isPreauthorization;
    }

    public function getAmmuntToPay() {
        return $this->ammuntToPay;
    }

    public function setAmmountToPay($ammuntToPay) {

        $ammuntToPay = round($ammuntToPay, 2) * 100;

        $this->ammuntToPay = $ammuntToPay;
    }

    public function getAmmountCurrency() {
        return $this->ammountCurrency;
    }

    public function setAmmountCurrency($ammountCurrency) {
        $this->ammountCurrency = $ammountCurrency;
    }

    public function getDetails1() {
        return $this->details1;
    }

    public function setDetails1($details1) {
        $this->details1 = $details1;
    }

    public function getDetails2() {
        return $this->details2;
    }

    public function setDetails2($details2) {
        $this->details2 = $details2;
    }

    public function getMerchantId() {
        return $this->merchantId;
    }

    public function setMerchantId($merchantId) {
        $this->merchantId = $merchantId;
    }

    public function getMerchantName() {
        return $this->merchantName;
    }

    public function setMerchantName($merchantName) {
        $this->merchantName = $merchantName;
    }

    public function getPaymentOkUrl() {
        return $this->paymentOkUrl;
    }

    public function setPaymentOkUrl($paymentOkUrl) {
        $this->paymentOkUrl = $paymentOkUrl;
    }

    public function getPaymentErrorUrl() {
        return $this->paymentErrorUrl;
    }

    public function setPaymentErrorUrl($paymentErrorUrl) {
        $this->paymentErrorUrl = $paymentErrorUrl;
    }

    public function getOriginalAmmount() {
        return $this->originalAmmount;
    }

    public function setOriginalAmmount($originalAmmount) {
        $this->originalAmmount = $originalAmmount;
    }

    public function getOrriginalCurrency() {
        return $this->orriginalCurrency;
    }

    public function setOrriginalCurrency($orriginalCurrency) {
        $this->orriginalCurrency = $orriginalCurrency;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function getCity() {
        return $this->city;
    }

    public function setCity($city) {
        $this->city = $city;
    }

    public function getZip() {
        return $this->zip;
    }

    public function setZip($zip) {
        $this->zip = $zip;
    }

    public function getCountry() {
        return $this->country;
    }

    public function setCountry($country) {
        $this->country = $country;
    }

    public function getTel() {
        return $this->tel;
    }

    public function setTel($tel) {
        $this->tel = $tel;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getLanuage() {
        return $this->lanuage;
    }

    public function setLanuage($lanuage) {

        if ($lanuage!="mk" && $lanuage!="en") {
            throw "Unsupported language $lanuage. only mk, en are supported";
        }

        $this->lanuage = $lanuage;
    }

    //Installment
    public function getInstallment() {
        return $this->installment;
    }


    public function setInstallment($installmentSetValue) {
        $this->installment = str_pad($installmentSetValue, 2, "0", STR_PAD_LEFT);
    }


    /*    
    public function setInstallment($installmentSetValue) {
        $this->installment = $installmentSetValue;
    }
    */    


    // CRef
    public function getCRef() {
        return $this->cRef;
    }

    public function setCRef($saved_cc = "0", $save_credit_card = 0) {

        $cRefFinalVal = "";
        
        if($saved_cc != "0")
            $cRefFinalVal = $saved_cc;
        elseif($saved_cc == 0 && $save_credit_card == 1)
            $cRefFinalVal = "-2";

        $this->cRef = $cRefFinalVal;
    }

}//class