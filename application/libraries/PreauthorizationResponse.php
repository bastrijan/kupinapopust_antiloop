<?php
require_once "CPayConfig.php";

/**
 * Description of PreauthorizationResponse
 *
 */
class PreauthorizationResponse {

    private $cPayPaymentRef         = "";


    // za uspesna
    private $isPayToMerchant        = false;



    // za neuspesna
    private $reasonOfDecline        = 0;

    private $cardNumber             = "";
    private $_3dSecure              = "";
    
    private $returnCheckSumHeader   = "";
    private $returnCheckSum         = "";
    private $details2               = "";
    private $allParameters          = array();

    private $cRef                 = ""; 


    function __construct() {
        if (!$_POST || count($_POST)==0) {
            throw new Exception("Empty Response!");
        }

        /*
$parameters = Array ( 
            "PaymentOKURL" => "https://kupinapopust.mk/pay_casys/response", 
            "PaymentFailURL" => "https://kupinapopust.mk/pay_casys/error?exitif=true", 
            "PayToMerchant" => "1000000393", 
            "AmountToPay" => 100, 
            "AmountCurrency" => "MKD", 
            "Details1" => "Kupinapopust.mk naracka br. 69548 za bastrijan@gmail.com", 
            "Details2" => "69548", 
            "MerchantName" => "kupinapopust.mk",
            "ServiceID" => "",
            "Details3" => "",
            "iseCollect" => "",
            "CheckSum" => "d10f1c7935ec9292f27b066c25589db4",
            "CheckSumHeader" => "10AmountToPay,AmountCurrency,Details1,Details2,PayToMerchant,MerchantName,PaymentOKURL,PaymentFailURL,Email,LoadNewIFrameLook,003003056005010015042051019001",
            "FirstName" => "",
            "LastName" => "",
            "Telephone" => "",
            "Email" => "bastrijan@gmail.com", 
            "Zip" => "",
            "Address" => "",
            "City" => "",
            "Country" => "",
            "OriginalAmount" => "",
            "OriginalCurrency" => "",
            "LoadNewIFrameLook" => "1",
            "ReturnCheckSum" => "93B180A8CAA2FF68A4662A082A31DCD1",
            "ReturnCheckSumHeader" => "11AmountCurrency,AmountToPay,Details1,Details2,PayToMerchant,MerchantName,PaymentOKURL,PaymentFailURL,Email,LoadNewIFrameLook,cPayPaymentRef,003003056005010015042051019001007",
            "isPreauthorization" => "",
            "cPayPaymentRef" => "2728287" 
            );
        */

        foreach ($_POST as $key=>$value) {

            $this->allParameters[$key] = $value;

            if ($key=="cPayPaymentRef")         $this->cPayPaymentRef       = $value;
            if ($key=="CardNumber")             $this->cardNumber           = $value;
            if ($key=="3DSecure")               $this->_3dSecure            = $value;
            if ($key=="ReasonOfDecline")        $this->reasonOfDecline      = $value;
            if ($key=="Details2")               $this->details2             = $value;

            if ($key=="ReturnCheckSumHeader")   $this->returnCheckSumHeader = $value;
            if ($key=="ReturnCheckSum")         $this->returnCheckSum       = $value;

            if ($key=="isPayToMerchant")        $this->isPayToMerchant       = (bool) $value;

            if ($key=="CRef")               $this->cRef             = $value;
            
        }// foreach

    }// __construct()




    public function isRequestConsistent() {

        if (!$this->isCheckSumHeaderValid()) {
            return false;
        }


        $calculatedChecksum = $this->calculateCheckSum();
        if ($calculatedChecksum==$this->returnCheckSum) {
            return true;
        }

        return false;

    }// isRequestConsistent()


    /**
     * Dali requestot doaga od kasis server ili od redirekt
     * Dokolku e od kasis vraka true
     *
     * @return bool
     */
    public function isSenderAuthenticated() {

        Kohana::log("error", "sporedba {$_SERVER["REMOTE_ADDR"]}==". CPayConfig::$gatewayIP);
                
        if ($_SERVER["REMOTE_ADDR"]==CPayConfig::$gatewayIP) {

            return true;
        }

        return false;

    }// isSenderAuthenticated()



    /**
     * Dali headerot e validen t.e dali e izmenet od tret subjekt
     * Proveruva sledno:
     * - Dali prvata brojka vo headerot odgovara na brojot na parametrite.
     * - dali parametrite navedeni vo headerot gi ima vo urlot
     * - dali dozlinata na parametrite navedeni vo headerot odgorava so dolzinata na vistinskite vrednosti
     * - dali brojot na parametrite vo headerot i realnite parametri e ist.
     *
     * @return bool
     */
    private function isCheckSumHeaderValid() {

        try {

            $checksuHeaderStructure = $this->parseCheckSumHeader();


            
            // proveri gi site parametri od headerot so vistinskite
            foreach ($checksuHeaderStructure as $paramData) {

                $paramName      = $paramData["parameter_name"];
                $paramLength    = $paramData["parameter_length"];

                if (array_key_exists("$paramName", $this->allParameters)) {

                    if (strlen($this->allParameters[$paramName])!=$paramLength) {
                        // dolzinata na praametarot e razlicna ot taa navedena vo headerot
                    }

                }else {
                    // parametarot od headerot ne posoi kako parametar vo urlot
                    return false;
                }

            }// foreach

            // all the parameters should be in the checksumheader except two ReturnCheckSumHeader, ReturnCheckSum
            //if (count($checksuHeaderStructure)!=(count($this->allParameters)-2) ) {
            //    return false;
            //}
            

        }catch (Exception $err) {
            // brojot na parametri ne odgovara so prvite dve cifri
            return false;
        }

        return true;

    }// isCheckSumHeaderValid()


    
    private function calculateCheckSum() {

        $baseString = "";

        $checksumHeaderStructure = $this->parseCheckSumHeader();
        foreach ($checksumHeaderStructure as $paramData) {

            $paramName      = $paramData["parameter_name"];
            $parameterValue = $this->allParameters[$paramName];

            $baseString .= $parameterValue;
            
            
        }// foreach
        
        $baseString = $this->returnCheckSumHeader . $baseString . CPayConfig::$merchatnPassword;
        $baseString = md5($baseString);
        
        return strtoupper($baseString);

    }// calculateChecksum


    /**
     * Parses the returnChecksumheader
     *
     * 09isPreauthorization,AmountToPay,AmountCurrency,Details1,Details2,PayToMerchant,MerchantName,PaymentOKURL,PaymentFailURL,001005003020007007009014017
     *
     * [0] => Array
     *       [parameter_name] => isPreauthorization
     *       [parameter_length] => 1
     * [1] => Array
     *       [parameter_name] => AmountToPay
     *       [parameter_length] => 5
     *  ...
     *
     * @return
     */
    private function parseCheckSumHeader() {

        $headerStructure = array();

        //

        // ne ja modificiraj instance variablata.
        $checkSumHeader = $this->returnCheckSumHeader;

        // kolku parametri se prateni
        $nrParameters = (int) substr($checkSumHeader, 0, 2);

        // otstarni gi prvite dva karakteri
        $checkSumHeader = substr_replace($checkSumHeader, "", 0, 2);

        //
        $parameterNameList = explode(",", $checkSumHeader);
        $numericPart = $parameterNameList[count($parameterNameList)-1];
        unset($parameterNameList[count($parameterNameList)-1]);

        if ($nrParameters != count($parameterNameList)) {
            throw new Exception("Unconsistent checksum header");
        }


        foreach ($parameterNameList as $paramName) {

            $paramValueLength = substr($numericPart, 0, 3);
            $numericPart = substr_replace($numericPart, "", 0, 3);

            $row["parameter_name"]      = $paramName;
            $row["parameter_length"]    = $paramValueLength;
            $headerStructure[] = $row;

        }//

        return $headerStructure;

    }// parseCheckSumHeader()




    public function getCPayPaymentRef() {
        return $this->cPayPaymentRef;
    }

    public function getCardNumber() {
        return $this->cardNumber;
    }

    public function get_3dSecure() {
        return $this->_3dSecure;
    }

    public function getReasonOfDecline() {
        return $this->reasonOfDecline;
    }


    public function getDetails2() {
        return $this->details2;
    }

    public function getCRef() {
        return $this->cRef;
    }

    public function getAllParameters() {
        return $this->allParameters;
    }

    public function getIsPayToMerchant() {
        return $this->isPayToMerchant;
    }
    
    public function setIsPayToMerchant($isPayToMerchant) {
        $this->isPayToMerchant = $isPayToMerchant;
    }

}// class