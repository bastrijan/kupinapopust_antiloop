<?php defined('SYSPATH') OR die('No direct access allowed.');

class Index_Controller extends Default_Controller {
	
	private $pagination_settings;
    
    public function __construct()
	{
		$this->template = 'layouts/public' ;
		
		$this->pagination_settings = Kohana::config('pagination.default');
	
		if(strpos(Router::$method, "_android") !== false) {
			$this->template = 'layouts/android' ;
		}

	    if(in_array(Router::$method, array('google_static_map'))) {
	      	$this->template = 'layouts/empty' ;
	    }
			
		parent::__construct();
	}

    public function index()
	{	
        $user_agent = true;  
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        
        // ova e za android verzija
        if (0 && stripos($user_agent, 'android') !== false && ($_SESSION['total_hits'] == 1)) {
            $this->template = new View('layouts/mobile_application');
        } else {
        	//ova e za web verzija
			//  new Profiler;
			$this->template->content = new View('index/index');

			//zemanje na serversi variabli
			$message = $this->input->get('message', '', true);
			
			//kreiranje na modeli
			$dealsModel = new Deals_Model();
			// $staticPagesModel = new Static_Model();
			$categoriesModel = new Categories_Model() ;
			$subCategoriesModel = new Subcategories_Model();
			
			// KOD KOJ STO VADI PODATOCI OD BAZA ZA PRIKAZ NA PONUDI
			$primaryOffers = $dealsModel->getFullOfersInfo();
			
			//momentalen datum i vreme
			$dateTimeUnique = date("Y-m-d H:i:s");

			// KOD KOJ STO VADI PODATOCI OD BAZA ZA PRIKAZ NA PONUDI
			$bottomOffersPagesCnt = ceil($dealsModel->getBottomOffersFullInfoPaginationCnt($dateTimeUnique) / $this->pagination_settings['items_per_page']);      
			$bottomOffers = $dealsModel->getBottomOffersFullInfoPagination(0, $this->pagination_settings['items_per_page'], $dateTimeUnique);
    
			$categoriesData = $categoriesModel->getCategories();
			$subCategoriesData = $subCategoriesModel->getSubcategoriesbyParentid(0);
			
			//kreiranje na output promenlivi
			$this->template->content->primaryDeals = $primaryOffers;
			$this->template->content->bottomOffers = $bottomOffers;
			$this->template->content->message = $message;
			$this->template->title = kohana::lang("prevod.website_title")." | Попусти до дури -90% на различни услуги и производи";
			$this->template->content->categoriesData = $categoriesData ;
			$this->template->content->subCategoriesData = $subCategoriesData ;
			$this->template->content->dateTimeUnique = $dateTimeUnique;
			$this->template->content->bottomOffersPagesCnt = $bottomOffersPagesCnt;
			
			// OMILENI
			
			// zemanje na 'cookie'
			$cookie_value = cookie::get("kupinapopust_fav");
			
			if ( ! empty($cookie_value) ) {
				$favDeals = json_decode($cookie_value, true);
			} else {
				$favDeals = array();
			}
			$this->template->content->favDeals = $favDeals;
			
			// END - OMILENI


			// SHOPPING CART
			// zemanje na 'cookie'
			$cookie_value_shop_cart = cookie::get("kupinapopust_shop_cart");

			if ( ! empty($cookie_value_shop_cart)) {
				$shopCartDeals = json_decode($cookie_value_shop_cart, true);
			} else {
				$shopCartDeals = array();
			}
			$this->template->content->shopCartDeals = $shopCartDeals;
			// END - SHOPPING CART

        }
    }
	
	public function index_ajax_paging()
	{		
		//Ajax request za paginacija na ponudite;
	
		if (request::method() == 'post')
		{
			//kreiranje na modeli
			$dealsModel = new Deals_Model();
			
			$post = $this->input->post();
			$dateTimeUnique = $post['dateTimeUnique'];
			$pagination_cnt = $post['pagination_cnt'];
			$type_post = $post['type'];
			$controller = $post['controller'];
			$method = $post['method'];
			$deal_id = $post['deal_id'];
			$general_offer_id = $post['general_offer_id'];
			$mode = $post['mode'];
			$category_id = $post['category_id'];
			$subcategory_id = $post['subcategory_id'];
			$tag_name = $post['tag_name'];
			
			if($type_post == "ajax" )
			{	
				$page = $pagination_cnt + 1;
				$offset = $this->pagination_settings['items_per_page'] * ($page - 1) ;
				
				// KOD KOJ STO VADI PODATOCI OD BAZA ZA PRIKAZ NA PONUDI
				if($controller == "index") {
					$bottomOffers = $dealsModel->getBottomOffersFullInfoPagination($offset, $this->pagination_settings['items_per_page'], $dateTimeUnique);
					$div_class = "col-md-4";
				} elseif($controller == "deal" && $method == "index") {
					$bottomOffers = $dealsModel->getBottomOffersFullInfoSinglePagination($deal_id, $offset, $this->pagination_settings['items_per_page'], $dateTimeUnique);
					$div_class = "col-md-4";
				} elseif($controller == "deal" && $method == "general_deal") {
					$bottomOffers = $dealsModel->getPonudiByGeneralOfferPagination($general_offer_id, $offset, $this->pagination_settings['items_per_page'], $dateTimeUnique);
					$div_class = "col-md-4";
				} elseif($controller == "deal" && $method == "tag") {
					$bottomOffers = $dealsModel->getPonudiByTagPagination($tag_name, $offset, $this->pagination_settings['items_per_page'], $dateTimeUnique);
					$div_class = "col-md-4";
				} elseif($controller == "all" && $method == "category") {
					$bottomOffers = $dealsModel->getPonudiByCategoryPagination($category_id, $offset, $this->pagination_settings['items_per_page'], $dateTimeUnique);
					$div_class = "col-md-4";
				} elseif($controller == "all" && $method == "subcategory") {
					$bottomOffers = $dealsModel->getPonudiBySubCategoryPagination($subcategory_id, $offset, $this->pagination_settings['items_per_page'], $dateTimeUnique);
					$div_class = "col-md-4";
				} else {
					$bottomOffers = $dealsModel->getAllController($mode, $offset, $this->pagination_settings['items_per_page'], $dateTimeUnique);
					$div_class = "col-md-4";
				}
				
				$html = "";
				
				foreach ($bottomOffers as $bottomDeal) 
				{

					$title_mk_clean = "";
					$title_mk = "";
					$max_ammount = 0;
					$ceni_od_txt = "";

					if($bottomDeal->options_cnt > 1 || $bottomDeal->is_general_deal)
					{
						$title_mk_clean =  $bottomDeal->title_mk_clean_deal;
						$title_mk =  $bottomDeal->title_mk_deal;
						$max_ammount = 0;

						if($bottomDeal->is_general_deal)
							$ceni_od_txt = "Прегледај понуди";
						else	
							$ceni_od_txt = ($bottomDeal->finalna_cena > 0 ? "од ".$bottomDeal->finalna_cena." ден." : "Превземи купон");
					}
					else
					{
						$title_mk_clean =  $bottomDeal->title_mk_clean;
						$title_mk =  $bottomDeal->title_mk;
						$max_ammount = $bottomDeal->max_ammount;
						$ceni_od_txt = ($bottomDeal->finalna_cena > 0 ? $bottomDeal->finalna_cena." ден." : "Превземи купон");
					}


					if($bottomDeal->is_general_deal)
						$detali_za_ponudata = "/deal/general_deal/" . seo::DealPermaLink($bottomDeal->deal_id, $title_mk_clean, $bottomDeal->category_id, $bottomDeal->subcategory_id, $categoriesData, $subCategoriesData);
					else
						$detali_za_ponudata = "/deal/index/" . seo::DealPermaLink($bottomDeal->deal_id, $title_mk_clean, $bottomDeal->category_id, $bottomDeal->subcategory_id, $categoriesData, $subCategoriesData);
							
					$html .= '<div class="' . $div_class . '"><div class="product-thumb"><header class="product-header">';
					$html .= '<a target="_blank" href="' . $detali_za_ponudata . '">';
					$html .= '<img src="/pub/deals/' . $bottomDeal->side_img . '" alt="' . trim(str_replace("\"", "'", strip_tags($title_mk_clean))) . '" /></a>';
					
					if($bottomDeal->price > 0  && !$bottomDeal->is_general_deal)
						$html .= '<span class="product-save popust-na-slika">'."- " . (int)round(100 - ($bottomDeal->price_discount / $bottomDeal->price) * 100) . "%".'</span>';
					

					///////////////////////favorites START
					$cookie_value = cookie::get("kupinapopust_fav");
					$favDeals = array();
					
					// Kreiranje na niza od indeksi na omileni ponudi
					if(!empty($cookie_value)) 
						$favDeals = json_decode($cookie_value, true);

					$html .= '<span class="product-save omileni-na-slika">';
					$html .= '<a href ="#" class="btn btn-sm fav-button'.(in_array($bottomDeal->deal_id, $favDeals) ? ' fav-active' : '').'" data-toggle="tooltip" data-placement="top" data-title="'.(in_array($bottomDeal->deal_id, $favDeals) ? 'Одземи од омилени' : 'Додади во омилени').'">';
					$html .= '<i class="fa fa-star"></i>';
					$html .= '<i class="infoDealID" style="display: none;">'.$bottomDeal->deal_id.'</i>';
					$html .= '</a>';
					$html .= '</span>';
					///////////////////////favorites END


					///////////////////////shpping cart START
					$cookie_value_shop_cart = cookie::get("kupinapopust_shop_cart");
					$shopCartDeals = array();
					
					// Kreiranje na niza od indeksi na omileni ponudi
					if ( ! empty($cookie_value_shop_cart))  
						$shopCartDeals = json_decode($cookie_value_shop_cart, true);

					if (!$bottomDeal->is_general_deal) { 
						$html .= '<span class="product-save shop-cart-na-slika" '.( strtotime($bottomDeal->start_time) > time() ? 'style="display: none;"' : '').'>';
						$html .= '<a href ="#" class="btn btn-sm '.($bottomDeal->options_cnt > 1 ? ' shop-cart-many-options' : ' shop-cart-button').(in_array($bottomDeal->deal_option_id, $shopCartDeals) ? ' shop-cart-active' : '').'" data-toggle="tooltip" data-placement="top" data-title="'.(in_array($bottomDeal->deal_option_id, $shopCartDeals) ? 'Одземи од кошничка' : 'Додади во кошничка').'">';
						$html .= '<i class="fa fa-shopping-cart"></i>';
						$html .= '<i class="infoDealOptionID" style="display: none;">'.$bottomDeal->deal_option_id.'</i>';
						$html .= '</a>';
						$html .= '</span>';
					} //if (!$bottomDeal->is_general_deal) { 
					///////////////////////shpping cart END



					$html .= '</header>';
					$html .= '<div class="product-inner" >';

					if(!$bottomDeal->is_general_deal)
					{
						$html .= '<h5 style="height: '.Kohana::config('settings.so_partner_height').'px">';
						$html .= commonshow::Truncate(strip_tags($bottomDeal->name), Kohana::config('settings.so_partner_letters'));;
						$html .= '</h5>';
						$html .= '<div class="gap-small"></div>';
					}
					
					
					$product_title_height = Kohana::config('settings.so_title_height');
					$product_title_letters = Kohana::config('settings.so_title_letters');
					//dokolku e OPSTA PONUDA
					if($bottomDeal->is_general_deal)
					{
						$product_title_height = 15 + Kohana::config('settings.so_title_height') + Kohana::config('settings.so_partner_height');
						$product_title_letters = 10 + Kohana::config('settings.so_title_letters') + Kohana::config('settings.so_partner_letters');
					}

					$html .= '<a target="_blank" href="' . $detali_za_ponudata . '">';
					$html .= '<h5 class="product-title" style="height: '.$product_title_height.'px">' . trim(commonshow::Truncate(strip_tags($title_mk_clean), $product_title_letters)) . '</h5>';
					$html .= '</a><div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i>&nbsp;';
					$html .= commonshow::staticCountDown(strtotime($bottomDeal->start_time), strtotime($bottomDeal->end_time));
					$html .= '</span>';
					
					$count  = $bottomDeal->voucher_count;
					$bottomDealsTooltipTxt = commonshow::tooltip($bottomDeal->voucher_count, $bottomDeal->min_ammount, $max_ammount);

					$html .= '<h6 class="text-green"><strong class="buyers-count" title="' . $bottomDealsTooltipTxt . '">';
					if($count > 0)
					{
						if(commonshow::isSoldOut($count, $max_ammount)) {
							$html .= '<span style="color: red">';
							$kupi_style = "btn-danger";
							$kupi_tooltip = "Распродадено";
							$onclick = 'sold_out(); return false;';
						} else {
							$kupi_style = "green";
							$kupi_tooltip = "Купи";
							$onclick = '';
						}
						
						$html .= '<i class="fa fa-level-up"></i>&nbsp;';
						$html .= commonshow::number($count, $bottomDeal->min_ammount, $max_ammount);
						$html .= commonshow::text($count);
						
						if(commonshow::isSoldOut($count, $max_ammount))
							$html .= '</span>';
					} else {
						$html .= '&nbsp;';
					}
					$html .= '</strong></h6>';

					$html .= '<ul class="product-price-list"><li><a target="_blank" href ="'.$detali_za_ponudata.'"><span class="product-price">';
					$html .= $ceni_od_txt;
					$html .= '</span></a></li><li>';
					
					if($bottomDeal->options_cnt <= 1 && $bottomDeal->price > 0)
						$html .= '<span class="product-old-price">'.$bottomDeal->price . " " . kohana::lang("prevod.ден").'</span>';
					
					$html .= '</li></ul>';



					$html .= '</div></div></div></div>';
					
					// END - OMILENI
				}
				
				$arr = array('status' => 'success', 'pagination_cnt' => $page, 'html' => $html);
				die(json_encode($arr));
				
			} // if($type_post == "ajax" )
			
		} // if(request::method() == 'post')
		
	}

	public function demo()
	{
		exit;
		$cityModel = new City_Model();
		$cities = $cityModel->getData();
		var_dump($cities);
		exit;
	}
	
	//za kreiranje na pdf - test
	public function pdf($attempt_id = null)
	{
		if ($attempt_id) {
			$vouchersModel = new Vouchers_Model() ;
			die($vouchersModel->CreatePDF($attempt_id, "I"));
		}
	}
	
	//za web adresa na opstiot newsletter
	public function newsletter($return_as_string = false)
	{
		//proveri dali doaga so REF id od nasata affiliate programa
		$ref = $this->input->get("ref", 0);

		//kreiraj modeli		
		$dealsModel = new Deals_Model() ;
		$mailContentModel = new Emailrenderer_Model();
		$categoriesModel = new Categories_Model() ;
		$subCategoriesModel = new Subcategories_Model();

		$allPrimary = $dealsModel->getFullOfersInfo();
		
		//kreiraj array so primarniot i stranicnite ponudi
		$maildata = array(
				'allPrimary' => $allPrimary,
				'newestOffers' => $dealsModel->getSitePonudi(1),
				'sideOffers' => $dealsModel->getOffersByCat(),
				'allCategories' => $categoriesModel->getCategories(),
				'aktivni_ponudi_cnt' => $dealsModel->getCntAllActiveOffers(),
				'subCategories' => $subCategoriesModel->getSubcategoriesbyParentid(0),
				'site_nasi_ponudi_on_top' => "yes",
				'return_as_string' => $return_as_string,
				'ref' => $ref
				);

		

		if(count($allPrimary) == 1)
			$maildata['newsletter_name_on_top'] = '<a style="color: #111;font-weight: bold;font-size: 20px;line-height: 24px;text-decoration: none;text-align: center;" href="'.Kohana::config('facebook.facebookSiteProtocol').'://' . Kohana::config('facebook.facebookSiteURL') . '/index" target="_blank" >Понуда на денот</a>';
		
		// die(print_r($maildata));

		//render-iraj go newsletter contenot
		$mailContent = $mailContentModel->render("newsletter_mailerlite", $maildata);
		
		//vrati go kako string
		if($return_as_string)
			return $mailContent;
		else//prikazi go
			die($mailContent);
	}
        
    //email so pottik za povtorna aktivnost na sajtot so dodeluvanje na poeni
	public function incentive_to_activate_email($return_as_string = false)
	{        
		//kreiraj modeli		
		$mailContentModel = new Emailrenderer_Model();
		
		//dodatni parametri
		$maildata = array();
		
		//render-iraj go newsletter contenot
		$mailContent = $mailContentModel->render("incentive_to_activate_email", $maildata);
		
		//vrati go kako string
		if($return_as_string)
			return $mailContent;
		else//prikazi go
			die($mailContent);
	}
	
	//za web adresa na newsletter-ot koj gi sodrzi glavnite ponudi, kategoriite i najnovite ponudi
	public function newsletter_main_offers($return_as_string = false)
	{
		//proveri dali doaga so REF id od nasata affiliate programa
		$ref = $this->input->get("ref", 0);

		//kreiraj modeli		
		$dealsModel = new Deals_Model() ;
		$mailContentModel = new Emailrenderer_Model();
		
		$allPrimary = $dealsModel->getFullOfersInfo();
		
		//kreiraj array so primarniot i stranicnite ponudi
		$maildata = array(
			'allPrimary' => $allPrimary,
			'newestOffers' => $dealsModel->getSitePonudi(1),
			'aktivni_ponudi_cnt' => $dealsModel->getCntAllActiveOffers(),
			'display_categories_on_top' => "no",
			'site_nasi_ponudi_on_top' => "yes",
			'return_as_string' => $return_as_string,
			'ref' => $ref
		);

		if(count($allPrimary) == 1)
			$maildata['newsletter_name_on_top'] = '<a style="color: #111;font-weight: bold;font-size: 20px;line-height: 24px;text-decoration: none;text-align: center;" href="'.Kohana::config('facebook.facebookSiteProtocol').'://' . Kohana::config('facebook.facebookSiteURL') . '/index" target="_blank" >Понуда на денот</a>';	
		
		//render-iraj go newsletter contenot
		$mailContent = $mailContentModel->render("newsletter_main_offers", $maildata);
		
		//vrati go kako string
		if($return_as_string)
			return $mailContent;
		else//prikazi go
			die($mailContent);
	}
	
	
	//za web adresa na newsletter-ot sto gi sodrzi ponudite od sekoja kategorija poedinecno
	public function newsletter_category($category_id = 0, $return_as_string = false)
	{
		//kreiraj modeli		
		$dealsModel = new Deals_Model() ;
		$mailContentModel = new Emailrenderer_Model();
		$aCategory = new Categories_Model();
		$subCategoriesModel = new Subcategories_Model();
		
		$cat_obj = $aCategory->getData(array("id" => $category_id));
			
		//kreiraj array 
		$maildata = array(
			'allOffersByCategory' => $dealsModel->getOffersByCat($category_id),
			'aktivni_ponudi_cnt' => $dealsModel->getCntAllActiveOffers(),
			'subCategories' => $subCategoriesModel->getSubcategoriesbyParentid($category_id),
			'site_nasi_ponudi_on_top' => "yes",
			'show_banner' => true,
			'newsletter_name_on_top' =>  '<a style="color: #111;font-weight: bold;font-size: 20px;line-height: 24px;text-decoration: none;text-align: center;" href="'.Kohana::config('facebook.facebookSiteProtocol').'://' . Kohana::config('facebook.facebookSiteURL') . '/all/category/'.$cat_obj[0]->id.'" target="_blank" >'.$cat_obj[0]->name.'</a>',
			'category_name' => $cat_obj[0]->name,
			'return_as_string' => $return_as_string
		);
		
		//render-iraj go newsletter contenot
		$mailContent = $mailContentModel->render("newsletter_category", $maildata);
		
		//vrati go kako string
		if($return_as_string)
			return $mailContent;
		else//prikazi go
			die($mailContent);
	}
	
	//za web adresa na newsletter-ot sto gi sodrzi 20te najprodavani ponudi
	public function newsletter_bestsellers($return_as_string = false)
	{
		//proveri dali doaga so REF id od nasata affiliate programa
		$ref = $this->input->get("ref", 0);
		$ref_add = isset($ref) && $ref > 0 ? "?ref=".$ref : "";

		//kreiraj modeli		
		$dealsModel = new Deals_Model() ;
		$mailContentModel = new Emailrenderer_Model();
		
		//kreiraj array 
		$limitRecords = 20;
		$maildata = array(
			'allOffers' => $dealsModel->getSitePonudi(2, $limitRecords),
			'aktivni_ponudi_cnt' => $dealsModel->getCntAllActiveOffers(),
			'site_nasi_ponudi_on_top' => "yes",
			'show_banner' => true,
			'newsletter_name_on_top' =>  '<a style="color: #111;font-weight: bold;font-size: 20px;line-height: 24px;text-decoration: none;text-align: center;" href="'.Kohana::config('facebook.facebookSiteProtocol').'://' . Kohana::config('facebook.facebookSiteURL') . '/all/index/2'.$ref_add.'" target="_blank" >Најпродавани понуди</a>',
			'return_as_string' => $return_as_string,
			'ref' => $ref
		);
		
		//render-iraj go newsletter contenot
		$mailContent = $mailContentModel->render("newsletter_bestsellers", $maildata);
		
		//vrati go kako string
		if($return_as_string)
			return $mailContent;
		else//prikazi go
			die($mailContent);
	}
        
        
		
	public function pazar3()
	{	
		//kreiraj modeli		
		$dealsModel = new Deals_Model() ;
		$mailContentModel = new Emailrenderer_Model();
		
		//inicijalizacija na promenlivi
		$data_string = "";
		
		//kreiraj array so potrebnite podatoci podatoci
		$activeOffers = $dealsModel->getActiveOffers();
		
		//svrti go array-ot so podatoci za deal-ot i gradi go 
		if ($activeOffers) {
			foreach ($activeOffers as $item) {
				$title = $this->stripStr($item->title_mk);
				$sku = $item->id;
				$cena = ($item->tip == 'cena_na_vaucer' ? $item->price_voucher : $item->price_discount)." ден."; 
				$link_proizvod = Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/deal/index/$item->id";
				$opis = $title; 
				$link_slika = Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/pub/deals/$item->main_img"; 
				$stock_level = ($item->max_ammount == 0 ? 10000 : $item->max_ammount); 
				$manufacturer_sku = $item->id;
				
				$data_string .= "\"Специјал > Ваучери за попуст\", \"".commonshow::Truncate($title, 60)."\", \"$sku\", \"$cena\", \"0 ден\", \"$link_proizvod\", \"$manufacturer_sku\", \"\", \"\", \"$opis\", \"$link_slika\", \"На залиха\", \"Веднаш\", \"\", \"\", \"\", \"\", \"$stock_level\"\r\n";	
			}
		}
		
		if($data_string != "")
			$data_string = substr($data_string, 0, strlen($data_string) - 2);
		
		//stavi go vo soodveten array za da se ispise na fajlot
		$maildata = array('data_string' => $data_string);
		
		//render-iraj go newsletter contenot
		$mailContent = $mailContentModel->render("pazar3", $maildata);
		
		//prikazi go
		die($mailContent);
		
	}
	
	private function stripStr($str_in)
	{
		$str_out = "";
		
		//za zamena na new line so zapirka
		$order   = array("\r","\n","\n\r", "\r\n", "'", '"', ",");
		$replace = array(" "," "," ", " ", "", "", "");
		
		$str_out = str_replace($order, $replace, trim(strip_tags($str_in)));
		
		return $str_out;
	}	
	
    public function search_android()
	{
        $this->search("android");
    }
	
	public function search($type = "")
	{
		commonshow::search("index", $this, $type); 
	}
	
	public function __call($method, $arguments)
	{
		$this->auto_render = FALSE;
		echo Router::$current_uri;
	}
	
	public function newsletter_insert($pass) 
	{
		$this->auto_render = FALSE;
		
		if ($pass == "baki123") 
		{
			$newsletterModel = new Newsletter_Model();
			
			$email_list = "";
					
			$email_list_array = explode("\r\n", $email_list);
			
			foreach($email_list_array as $email)
			{
				if($email != "")
				{
					$email = trim($email);
					
					////////////		
					$where = array("email" => $email);
					$row = $newsletterModel->getData($where);
					if (!$row) {
						$newID = $newsletterModel->saveData($where);
						$newsletterModel->saveData(array('id' => $newID, 'hash' => md5("kupi_". $email)));
					} 
					///////////////
				}
			}

			print "finished!";
		}
		else
		{
			print "access denied!";
		}
	}

	
	public function favourites()
	{
		// definiranje na 'view'
		$this->template->content = new View('index/favourites');
		
		// kreiranje na modelot
		$dealsModel = new Deals_Model();
		
		// zemanje na 'cookie'
		$cookie_value = cookie::get("kupinapopust_fav");
		
		if ( ! empty($cookie_value)) {
			$deal_ids = json_decode($cookie_value, true);
			if (count($deal_ids) == 0) {
				$allOffers = array();
			} else {
				$allOffers = $dealsModel->getFavouriteOffers($deal_ids);
			}
		} else {
			$allOffers = array();
		}


		// SHOPPING CART
		// zemanje na 'cookie'
		$cookie_value_shop_cart = cookie::get("kupinapopust_shop_cart");

		if ( ! empty($cookie_value_shop_cart)) {
			$shopCartDeals = json_decode($cookie_value_shop_cart, true);
		} else {
			$shopCartDeals = array();
		}
		$this->template->content->shopCartDeals = $shopCartDeals;
		// END - SHOPPING CART
			

		//kreiranje na output promenlivi
		$this->template->content->allOffers = $allOffers;
		$this->template->title = kohana::lang("prevod.website_title")." | Попусти до дури -90% на различни услуги и производи";
	}
	

	public function shop_cart($user_login = 0, $error_status = 0)
	{
		// definiranje na 'view'
		$this->template->content = new View('index/shop_cart');
		
		// kreiranje na modelot
		$dealsModel = new Deals_Model();
		$categoriesModel = new Categories_Model() ;
		$customerModel = new Customer_Model();

		// zemanje na 'cookie'
		$cookie_value = cookie::get("kupinapopust_shop_cart");

		$categoriesData = $categoriesModel->getCategoryAllInfoByID();
		
		if ( ! empty($cookie_value)) {
			$deal_option_ids = json_decode($cookie_value, true);
			if (count($deal_option_ids) == 0) {
				$allOffers = array();
			} else {
				//$where_options_str = "d.id = $deal_id AND do.active = 1";
				
				// die(implode(",", $deal_option_ids));

				$deal_option_ids = implode(",", $deal_option_ids);
				$where_options_str = "do.id IN (".$deal_option_ids.") and d.end_time > '".date("Y-m-d H:i:s")."' ";
				$allOffers = $dealsModel->getDealOptionsData($where_options_str);
				// $allOffers = $dealsModel->getFavouriteOffers($deal_ids);
			}
		} else {
			$allOffers = array();
		}

		//zemanje na poenite na customerot
        $customerPoints = 0;
        $user_id = $this->session->get("user_id");
        $user_type = $this->session->get("user_type");
        if ($user_id and $user_type == 'customer') 
            $customerPoints = $customerModel->getCustomerPoints($user_id);

        if ($customerPoints < 10) 
        	$customerPoints = 0;

		//kreiranje na output promenlivi
		$this->template->content->allOffers = $allOffers;
		$this->template->content->categoriesData = $categoriesData ;
		$this->template->content->customerPoints = (int)$customerPoints;

		$this->template->content->user_login = $user_login;
		$this->template->content->error_status = $error_status;

		$this->template->title = kohana::lang("prevod.website_title")." | Попусти до дури -90% на различни услуги и производи";
	}

	public function google_static_map() {
		$this->template->title = "Детали за попуст" ;
		$this->template->content = new View("index/google_static_map") ;
	}
}