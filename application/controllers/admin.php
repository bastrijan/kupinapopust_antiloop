<?php
defined('SYSPATH') OR die('No direct access allowed.') ;

class Admin_Controller extends Default_Controller {

  public function __construct() 
  {

  	ini_set('max_execution_time', 600); //600 seconds = 10 minutes=

    $this->template = 'layouts/admin' ;

    if(in_array(Router::$method, array('printvoucherlist', 'finreportprint'))) {
      $this->template = 'layouts/publicprint' ;
    }

    parent::__construct() ;
  }

  public $session = '' ;

  public function index() 
  {

  	//sistemski setiranja
    $this->template->title = "admin welcome" ;
    $this->template->content = new View("admin_deals/index") ;

	//zemi go logiraniot user
	$userID = $this->session->get("user_id");
	
	//zemi gi get variablite od formata
    $type = $this->input->get("type", 'current');
	$category_id_selected = $this->input->get("category_id_selected", 0);
	$partner_id_selected = $this->input->get("partner_id_selected", 0);
	$deal_name_filter = $this->input->get("deal_name_filter", '');
	$vraboten_id = in_array($userID, array(1, 23)) ? $this->input->get("vraboten_id", 0) : 0;

	//zemi gi get variablite od drugite mesta
	$succes_msg = $this->input->get("succes_msg", 0);
	$invalid_deals_ids = $this->input->get("invalid_deals_ids", "");
	$partOfGeneralOffer = $this->input->get("partOfGeneralOffer", 0);


	//kreiranje na modelite
    $dealsModel = new Deals_Model() ;
	$vraboteniModel = new Vraboteni_Model() ;
	$categoriesModel = new Categories_Model() ;
	$subCategoriesModel = new Subcategories_Model();
	$partnersModel = new Partners_Model() ;

	//inicijalizacija na lokalni promenlivi
	$function = '';

	//setiranje na lokalni promenlivi zavisno od odbraniot filter: TIP NA PONUDA
    if ($type == 'all') 
    {
        $function = 'getAllOffers';
    } 
    elseif ($type == 'current')
    {
        $function = 'getCurrentOffers';
    } 
    elseif ($type == 'past')
    {
         $function = 'getExpiredOffers';
    } 
    elseif ($type == 'general_offers')
    {
        $function = 'getGeneralOffers';
    } 
    elseif ($type == 'past_general_offers')
    {
        $function = 'getPastGeneralOffers';
    }
    else 
    {
		$function = 'getDemoOffers';
    }

    //sortiranje
    $sorting = 1;

    //povikuvanje na metodi
    $allVraboteni = $vraboteniModel->getData() ;
	$vraboteniData = $vraboteniModel->getVraboteni();
	$categoriesData = $categoriesModel->getCategories();
	$subCategoriesData = $subCategoriesModel->getSubcategoriesbyParentid(0);
	$partnersData = $partnersModel->getPartners() ;

    $dealsData = $dealsModel->$function($category_id_selected, $vraboten_id, $partner_id_selected, $deal_name_filter, $sorting);


    /**********************************************OUTPUT********************************************************/
    //output variabli od formata
    $this->template->content->selected = $type ;
    $this->template->content->category_id_selected = $category_id_selected ;
    $this->template->content->partner_id_selected = $partner_id_selected ;
	$this->template->content->deal_name_filter = $deal_name_filter;
	$this->template->content->vraboten_id = $vraboten_id;

    //output variabli od drugi mesta
    $this->template->content->dealsData = $dealsData ;
	$this->template->content->vraboteniData = $vraboteniData ;
	$this->template->content->categoriesData = $categoriesData ;
	$this->template->content->subCategoriesData = $subCategoriesData ;
	$this->template->content->succes_msg = $succes_msg;
	$this->template->content->invalid_deals_ids = $invalid_deals_ids;
	$this->template->content->partOfGeneralOffer = $partOfGeneralOffer;
	$this->template->content->allVraboteni = $allVraboteni;	
	$this->template->content->partnersData = $partnersData ;
  }

  public function login() {
  
    $this->template->title = "admin welcome" ;
    $this->template->content = new View("/admin/login") ;

    if (request::method() == 'post') {
      $post = $this->input->post() ;
      $username = $post['username'] ;
      $password = $post['password'] ;

      $adminModel = new Admin_Model() ;
      $where = array('username' => $username, 'password' => md5($password)) ;
      $admin = $adminModel->getData($where) ;
      if (count($admin) > 0) 
	 {
		if($admin[0]->active)
		{
			$this->session = Session::instance() ;
			$this->session->set("user_id", $admin[0]->id) ;
			$this->session->set("user_type", 'admin') ;
			$this->session->set("admin_name", $admin[0]->name) ;
			url::redirect("/admin/index") ;
		}
		else
			$this->template->content->error = 'The user is disabled! Please contact your administrator.' ;
      }
      else {
        $this->template->content->error = 'Username and password combination is not valid' ;
      }
    }
  }

 
  public function delete_image($deal_id = null, $imageName = "")
  {
  	$deal_id = $this->input->get("deal_id", 0);
  	$imageName = $this->input->get("image_name", '');
  	$column_name = $this->input->get("column_name", '');
  	$deal_type = $this->input->get("deal_type", 'real');
  	

  	if((int) $deal_id > 0)
  	{
  		

  		//izbrisi ja slikata
  		if($imageName != "" && is_file(DOCROOT . 'pub/deals/'.$imageName))
  			unlink(DOCROOT . 'pub/deals/'.$imageName);

  		//izbrisi od baza
  		if($column_name != "")
  		{
  			$dealsModel = new Deals_Model() ;

			$data = array("id" => $deal_id, $column_name => "") ;

			// die(print_r($data));

			$dealsModel->saveData($data) ;
  		}


  		//redirect na edit stranata
  		if($deal_type == "real")
			url::redirect("/admin/autosave_deal/$deal_id");  		
		else
			url::redirect("/admin2/autosave_general_deal/$deal_id");  		
  	}

  }

  //ova e koga ke klikne na SUBMIT kopceto
  public function autosave_deal($deal_id = null, $is_clone = 0) {
	
	$deal_id = (int) $deal_id;    

    //za GD library da se koristi od driver-ite
	$img_config = array('driver' => 'GD');
		
    Kohana::config_set('config.global_xss_filtering', false);
    $this->template->title = "Добар попуст, подесување" ;
    $this->template->content = new View("/admin/autosave_deal") ;

    $dealsModel = new Deals_Model() ;

    $partnersModel = new Partners_Model() ;
    $allPartners = $partnersModel->getPartners() ;
    $this->template->content->allPartners = $allPartners ;
	
	$categoriesModel = new Categories_Model() ;
	$categoriesData = $categoriesModel->getData() ;
	$this->template->content->categoriesData = $categoriesData ;
	
    $vraboteniModel = new Vraboteni_Model() ;
    $allVraboteni = $vraboteniModel->getData() ;
    $this->template->content->allVraboteni = $allVraboteni;

    $dealOptionsModel = new Deal_Options_Model() ;
    $lastValidToDate = $dealOptionsModel->getLastValidToDate($deal_id) ;
    $this->template->content->lastValidToDate = $lastValidToDate;

    $SubcategoriesModel = new Subcategories_Model();
    $dealTagsModel = new Deal_Tags_Model() ;


    // AKO E POST
    if (request::method() == 'post') 
    {

        //should save then
        $post = $this->input->post() ;
        unset($post['autosave']);

        //prefrli gi tagovite vo temp promenliva i izbrisi ja promenlivata od $post
        $dealTagsInput = $post['tags-outside'];
        unset($post['tags-outside']);

        $redirect_to_str = "";
        if(isset($post['submit_btn_opt']))
        {
        	$redirect_to_str = "option";
        	unset($post['submit_btn_opt']);
        }
        else
        {
        	$redirect_to_str = "dashboard";
        	unset($post['submit_btn']);
        }

        $files = $this->input->files() ;
		
		//PROVERKA DALI TREBA DA SE PUSTI EMAIL DO PARTNER ZA NOVA KREIRANA PONUDA
		//ako se kreira nova ponuda ili od DEMO e smenet statusot vo AKTIVNA => isprati email do partnerot
		$ispratiEmailZaNovaPonuda = false;

		if ( $post['deal_status'] == "aktivna" && (strtotime($post['end_time']) > time()) && $post['partner_notified_for_active_offer'] == 0) 
		{
			$ispratiEmailZaNovaPonuda = true;
			$post['partner_notified_for_active_offer'] = 1;
		}
  	
		if (isset($post['id']) && $post['id'] =="") 
		{
			$post['created'] = date("Y-m-d H:i:s");
		}
		  
		if ($post['deal_status'] == "aktivna")
			$post['visible'] = 1;
		else
			$post['visible'] = 0;
				
		if ($post['deal_status'] == "demo")
			$post['demo'] = 1;
		else
			$post['demo'] = 0;
				
		unset($post['deal_status']);
		unset($post['old_deal_status']);		

		if (!isset($post['plakjanje_pri_prezemanje'])) {
			$post['plakjanje_pri_prezemanje'] = 0 ;
		}

		if (!isset($post['proizvod_flag'])) {
			$post['proizvod_flag'] = 0 ;
		}

		if (!isset($post['proizvod_besplatna_dostava'])) {
			$post['proizvod_besplatna_dostava'] = 0 ;
		}

		if (!isset($post['proizvod_besplatna_dostava_otkup'])) {
			$post['proizvod_besplatna_dostava_otkup'] = 0 ;
		}

		if (!isset($post['locked'])) {
			$post['locked'] = 0 ;
		}

		if (!isset($post['prati_mail_do_partner'])) {
			$post['prati_mail_do_partner'] = 0 ;
		}

		if (!isset($post['email_content_buy_switch'])) {
			$post['email_content_buy_switch'] = 0 ;
		}

		//ako e odbrano tip_danocna_sema za ddv da bide "Промет остварен во странство" => stavi go ddv-to da bide 0%
		if ($post['tip_danocna_sema'] == "Промет остварен во странство")
			$post['ddv_stapka'] = 0;

        //UNSET
        if($post['platena_reklama_date'] == "")
        	unset($post['platena_reklama_date']);

        if($post['min_ammount'] == "")
        	unset($post['min_ammount']);

        if($post['platena_reklama'] == "")
        	unset($post['platena_reklama']);

		//povikaj fukcija za zacuvuvanje na podatocite vo baza
		$insertID = $dealsModel->saveData($post) ;
		if (isset($post['id']) and $post['id']) {
			$insertID = $post['id'] ;
		}

		//kreiraj objekt za optimizacija na slikata
		$optimizeImageWebServiceObject = new Optimize_Image_Webservice_Controller(Kohana::config('settings.tiny_png')['api_key']);


		if (isset($files["main_picture"]) and $files["main_picture"]['name']) 
		{
			//zacuvaj ja upload-iranata slika vo temp lokacija
			$filename = upload::save($files["main_picture"]) ;

			//zemi info za upload-iranata slika
			$imgInfo = pathinfo($filename) ;

			//info na novata slika
			$new_db_image_name = Kohana::config('config.offers_image_subfolder')."/deal_" . $insertID . "." . $imgInfo['extension'];
			$new_image_full_path = DOCROOT . 'pub/deals/'.$new_db_image_name;



			//ako ne moze da se kompresira slikata togas zacuvaj ja na klasicen nacin
			if(!$optimizeImageWebServiceObject->compress($filename, $new_image_full_path))
			{
				Image::factory($filename, $img_config)
				//  ->resize(100, 100, Image::WIDTH)
				//  ->quality(50)
					->save($new_image_full_path) ;

			}	

			// Remove the temporary file
			unlink($filename) ;
			
			//zacuvaj vo baza
			$data = array('id' => $insertID, 'main_img' => $new_db_image_name) ;
			$dealsModel->saveData($data) ;
		}
		
		if (isset($files["side_picture"]) and $files["side_picture"]['name']) {

			//zacuvaj ja upload-iranata slika vo temp lokacija
			$filename = upload::save($files["side_picture"]) ;

			//zemi info za upload-iranata slika
			$imgInfo = pathinfo($filename) ;

			//info na novata slika
			$new_db_image_name = Kohana::config('config.offers_image_subfolder')."/side_deal_" . $insertID . "." . $imgInfo['extension'];
			$new_image_full_path = DOCROOT . 'pub/deals/'.$new_db_image_name;



			//ako ne moze da se kompresira slikata togas zacuvaj ja na klasicen nacin
			if(!$optimizeImageWebServiceObject->compress($filename, $new_image_full_path))
			{
				Image::factory($filename, $img_config)
				//  ->resize(100, 100, Image::WIDTH)
				//  ->quality(50)
					->save($new_image_full_path) ;

			}	

			// Remove the temporary file
			unlink($filename) ;
			
			//zacuvaj vo baza
			$data = array('id' => $insertID, 'side_img' => $new_db_image_name) ;
			$dealsModel->saveData($data) ;



		}
		
		// Additional images
		for ($i = 1; $i <= 5; $i++)
		{
			if (isset($files["picture_" . $i]) and $files["picture_" . $i]['name'])
			{

				//zacuvaj ja upload-iranata slika vo temp lokacija
				$filename = upload::save($files["picture_" . $i]) ;

				//zemi info za upload-iranata slika
				$imgInfo = pathinfo($filename) ;

				//info na novata slika
				$new_db_image_name = Kohana::config('config.offers_image_subfolder')."/deal_p" . $i . "_" . $insertID . "." . $imgInfo['extension'];
				$new_image_full_path = DOCROOT . 'pub/deals/'.$new_db_image_name;



				//ako ne moze da se kompresira slikata togas zacuvaj ja na klasicen nacin
				if(!$optimizeImageWebServiceObject->compress($filename, $new_image_full_path))
				{
					Image::factory($filename, $img_config)
					//  ->resize(100, 100, Image::WIDTH)
					//  ->quality(50)
						->save($new_image_full_path) ;

				}	

				// Remove the temporary file
				unlink($filename) ;
				
				//zacuvaj vo baza
				$data = array('id' => $insertID, "additional_img_" . $i => $new_db_image_name) ;
				$dealsModel->saveData($data) ;

			}
		}

		//ponuda_video	
		if (isset($files["ponuda_video"]) and $files["ponuda_video"]['name']) {

			$uploaddir = DOCROOT . 'pub/videos/';
			$video_ext = pathinfo($_FILES['ponuda_video']['name'], PATHINFO_EXTENSION);
			
			$uploadfile = $uploaddir . $insertID . "." . $video_ext;

			// $uploadfile = $uploaddir . basename($_FILES['ponuda_video']['name']);
			
			if (move_uploaded_file($_FILES['ponuda_video']['tmp_name'], $uploadfile)) {

				

				//update the record with the file name for the video
				$data = array('id' => $insertID, 'ponuda_video' => $insertID . "." . $video_ext) ;
				$dealsModel->saveData($data) ;
			} 
		}

		//vnesuvanje na default opcija dokolku ne postoi
		$this->vnesiDefaultOpcija($insertID, $post);

		//vnesuvanje na tagovite
		$this->tagDBEntry($insertID, $dealTagsInput);		

		//ako treba da se isprati email do partner za novo otvorena ponuda
		if ($ispratiEmailZaNovaPonuda)
			$this->newOfferSendEmailToPartner($post);

		//vo slucaj da e del od OPSTI PONUDI da se update-ira soodvetno end_time na tie OPSTI PONUDI
		// $this->generalOffersSetEndTime($insertID, $post['end_time']);
		$this->generalOffersSetEndTime($insertID);


		Kohana::config_set('config.global_xss_filtering', true);

		//die(print_r($post));

		if($redirect_to_str == "option")
			url::redirect("/admin2/dealoptions/$insertID");
		else
			url::redirect("/admin");

    }//if (request::method() == 'post')
    
    
    if ($deal_id) 
    {
		//edit
		$where = array('id' => $deal_id) ;
		$dealData = $dealsModel->getData($where) ;

		$whereCategory = array('category_id'=>$dealData[0]->category_id);

		$this->template->content->dealData = $dealData ;
    }
    else
	{
		// new, just render the form
		$whereCategory = array('category_id'=>$categoriesData[0]->id);
    }
    
	$SubcategoriesData = $SubcategoriesModel->getData($whereCategory) ;
	$this->template->content->SubcategoriesData = $SubcategoriesData ;


    //tag system
    $dealTagsCommaDelimited = "";
    $dealAllTagsCommaDelimited = "";
    
    if($deal_id > 0)
    	$dealTagsCommaDelimited = $dealTagsModel->getDealTagsCommaDelimited($deal_id);

    $this->template->content->dealTagsCommaDelimited = $dealTagsCommaDelimited;


    $dealAllTagsCommaDelimited = $dealTagsModel->getDealTagsCommaDelimited();
    $this->template->content->dealAllTagsCommaDelimited = $dealAllTagsCommaDelimited;

    $this->template->content->is_clone = $is_clone;

}



//ova e za avtomatskiot ajax na odredeno vreme sto se povikuva	
public function ajax_autosave()
{

    $this->template->title = "Добар попуст, подесување" ;
    $this->template->content = "";

    if (request::method() == 'post') {
        
         $dealsModel = new Deals_Model() ;
        
        //should save then
        $post = $this->input->post();
        unset($post['autosave']);
        unset($post['search_keyword']);
        unset($post['mail']);
        
//        die();

        if (!isset($post['id']) || $post['id'] =="") 
        {
        	$deal_id = 0 ;
        	unset($post['id']);
        }
        else
        	$deal_id = $post['id'] ;

        //prefrli gi tagovite vo temp promenliva i izbrisi ja promenlivata od $post
        $dealTagsInput = $post['tags-outside'];
        unset($post['tags-outside']);

        //PROVERKA end_time mora da bide pomal od $lastValidToDate
	    $dealOptionsModel = new Deal_Options_Model() ;
	    $lastValidToDate = $dealOptionsModel->getLastValidToDate($deal_id) ;

	    if(strtotime($post['end_time']) > strtotime($lastValidToDate))
	    {
	    	die("greska so datum lastValidToDate!!");
	    	// die($post['end_time']."--a--".strtotime($post['end_time'])."--b--".strtotime($lastValidToDate)."--c--lastValidToDate: ".$lastValidToDate);
	    }

		//PROVERKA DALI TREBA DA SE PUSTI EMAIL DO PARTNER ZA NOVA KREIRANA PONUDA
		//ako se kreira nova ponuda ili od DEMO e smenet statusot vo AKTIVNA => isprati email do partnerot
		$ispratiEmailZaNovaPonuda = false;
		
		
		if ( $post['deal_status'] == "aktivna" && (strtotime($post['end_time']) > time()) && $post['partner_notified_for_active_offer'] == 0) 
		{
			$ispratiEmailZaNovaPonuda = true;
			$post['partner_notified_for_active_offer'] = 1;
		}
		
		if (isset($post['id']) && $post['id'] =="") 
		{
			$post['created'] = date("Y-m-d H:i:s") ;
		}
		
        if ($post['deal_status'] == "aktivna")
            $post['visible'] = 1;
        else
            $post['visible'] = 0; //BIDEJKI E VO AUTOSAVE MODE 'visible' NA POCETOK IMA VREDNOST 0

       
        
        if ($post['deal_status'] == "demo")
            $post['demo'] = 1;
        else
            $post['demo'] = 0;
			
        unset($post['deal_status']);
        unset($post['old_deal_status']);		

		if (!isset($post['plakjanje_pri_prezemanje'])) {
			$post['plakjanje_pri_prezemanje'] = 0 ;
		}

		if (!isset($post['proizvod_flag'])) {
			$post['proizvod_flag'] = 0 ;
		}

		if (!isset($post['proizvod_besplatna_dostava'])) {
			$post['proizvod_besplatna_dostava'] = 0 ;
		}

		if (!isset($post['proizvod_besplatna_dostava_otkup'])) {
			$post['proizvod_besplatna_dostava_otkup'] = 0 ;
		}
	
        if (!isset($post['locked'])) {
          $post['locked'] = 0 ;
        }

        if (!isset($post['prati_mail_do_partner'])) {
          $post['prati_mail_do_partner'] = 0 ;
        }
	
        if (!isset($post['email_content_buy_switch'])) {
          $post['email_content_buy_switch'] = 0 ;
        }
	
        //ako e odbrano tip_danocna_sema za ddv da bide "Промет остварен во странство" => stavi go ddv-to da bide 0%
        if ($post['tip_danocna_sema'] == "Промет остварен во странство")
            $post['ddv_stapka'] = 0;


        //UNSET
        if($post['platena_reklama_date'] == "")
        	unset($post['platena_reklama_date']);

        if($post['min_ammount'] == "")
        	unset($post['min_ammount']);

        if($post['platena_reklama'] == "")
        	unset($post['platena_reklama']);


        //povikaj fukcija za zacuvuvanje na podatocite vo baza
        $insertID = $dealsModel->saveData($post);
        if (isset($post['id']) and $post['id']) {
            $insertID = $post['id'] ;
        }

		//vnesuvanje na default opcija dokolku ne postoi
		$this->vnesiDefaultOpcija($insertID, $post);

		//vnesuvanje na tagovite
		$this->tagDBEntry($insertID, $dealTagsInput);	

        //ako treba da se isprati email do partner za novo otvorena ponuda
		if ($ispratiEmailZaNovaPonuda)
			$this->newOfferSendEmailToPartner($post);

		//vo slucaj da e del od OPSTI PONUDI da se update-ira soodvetno end_time na tie OPSTI PONUDI
		// $this->generalOffersSetEndTime($insertID, $post['end_time']);
		$this->generalOffersSetEndTime($insertID);
              
      Kohana::config_set('config.global_xss_filtering', true);
      $arr = array('status' => 'success', 'id' => $insertID, 'partner_notified_for_active_offer' => $post['partner_notified_for_active_offer']);
      die(json_encode($arr));
    }

}


private function tagDBEntry($deal_id = 0, $dealTagsInput = "")
{
 	$deal_id = (int) $deal_id;

 	if($deal_id < 1 || $dealTagsInput == "")
 		return -1;

 	$dealTagsModel = new Deal_Tags_Model() ;

    //napravi array od string-ot so tagovi
    eval('$dealTagsArray = '.$dealTagsInput.';');

    //izbrisi gi site segasni tagovi
    $dealTagsModel->delete_selected_tags($deal_id);

    //svrti gi site tagovi
    $sava_array = array();
	foreach ($dealTagsArray as $tag) 
	{
        //vnesi gi odbranite DEALS vo general_offer_deals
        $sava_array = null;
        $sava_array["deal_id"] = $deal_id;
        $sava_array["name"] = $tag;

		$dealTagsModel->saveData($sava_array) ;

	}

	return 0;
}



  // private function generalOffersSetEndTime($deal_id = null, $end_time)
  private function generalOffersSetEndTime($deal_id = null) 
  {

		$this->auto_render = false ;

		$deal_id = (int) $deal_id;

		if ($deal_id) 
		{

			//setiraj modeli
			$dealsModel = new Deals_Model();
			$generalOfferDealsModel = new General_Offer_Deals_Model();

		    //izvadi gi ID-ata na site OPSTI ponudi vo koi sto ovaa ponuda e izbrana
			$where = array('deal_id' => $deal_id);
			$generalOfferDealsRelation = $generalOfferDealsModel->getData($where) ;

			// die(print_r($generalOfferDealsRelation));

		    //svrti gi site selektirani OPSTI ponudi
			foreach ($generalOfferDealsRelation as $generalOfferDealsRelationData) 
			{

				//povikaj funcija sto ke gi postavi soodvetno datumite na opstata ponuda
				$this->generalOfferSetDates($generalOfferDealsRelationData->general_offer_id, $dealsModel, $generalOfferDealsModel);

			}//foreach ($generalOfferDealsRelation as $generalOfferDealsRelationData) 
		    
		    
		}//if ($deal_id)

 }


 private function generalOfferSetDates($general_offer_id = null, $dealsModel, $generalOfferDealsModel) 
 {
 	$this->auto_render = false ;

 	$general_offer_id = (int) $general_offer_id;

	if ($general_offer_id) 
	{
		// die("---".$general_offer_id);

        //setiraj default vrednosti za datumite
        $start_date_final = "0000-00-00 00:00:00";
        $end_date_final = "0000-00-00 00:00:00";

		//izvadi gi site REALNI ponudi koi se del od ovaa OPSTA PONUDA
		$where = array('general_offer_id' => $general_offer_id);
		$realOfferDealsRelation = $generalOfferDealsModel->getData($where) ;

		// die("count=".count($realOfferDealsRelation));

        //svrti gi REALNITE PONUDI za da gi odredis datumite
		foreach ($realOfferDealsRelation as $realOfferDealsRelationData) 
		{

	        //zemi go date_start i date_end za REALNATA PONUDA
			$where = array('id' => $realOfferDealsRelationData->deal_id);
			$selectedDealData = $dealsModel->getData($where) ;


			// if(!isset($selectedDealData[0]->start_time) || !isset($selectedDealData[0]->end_time))
			// 	echo("--".print_r($realOfferDealsRelationData));

			if(count($selectedDealData) > 0)
			{			

				if($start_date_final == "0000-00-00 00:00:00")
					$start_date_final = $selectedDealData[0]->start_time;
				elseif($start_date_final > $selectedDealData[0]->start_time )
						$start_date_final = $selectedDealData[0]->start_time;

				if($end_date_final < $selectedDealData[0]->end_time )
					$end_date_final = $selectedDealData[0]->end_time;
			}


		}


		//da se update-ira date_start i date_end vo GENERAL OFFER
        $sava_array = array();
        $sava_array["id"] = $general_offer_id;
        $sava_array["start_time"] = $start_date_final;
        $sava_array["end_time"] = $end_date_final;

		$dealsModel->saveData($sava_array) ;

	}
}

  
  public function setprimary($deal_id = null, $reden_broj = 1, $sendMails = false, $return_type = "current") {

		$this->auto_render = false ;

		$qsAdd = "";

		//dokolku ne e del od generalna ponuda togas setiraj
		if(!$this->checkIfPartOfGeneralOffers($deal_id))
		{
			$dealsModel = new Deals_Model() ;

			if ($deal_id) {
			    //edit
			    $dealsModel->setPrimary($deal_id, $reden_broj, $sendMails) ;
			
				//kreiraj go fajlot za banner-ot
				$pass_create_file = "nlonlo321";
				// require_once Kohana::config('config.banner_generator_filepath');
			}

		}	
		else // dokolku e del od generalna ponuda togas vrati vo query string soodvetna poraka
		{
			  $qsAdd = "&partOfGeneralOffer=1";
		}
		


		url::redirect("/admin?type=".$return_type.$qsAdd) ;
  }




  public function dealdetails($deal_id = null, $deal_option_id = 0) {

    $this->template->title = "Детали за попуст" ;
    $this->template->content = new View("/admin/dealdetails") ;

    $dealsModel = new Deals_Model();

    $deal_option_id = (int) $deal_option_id;

    if ($deal_id) {

	  //Get deal by deal id
	  $where_deal = array('id' => $deal_id);
	  $dealDataInfo = $dealsModel->getData($where_deal) ;

      //edit
	  $where_options_str = "d.id = $deal_id ";

	  if($deal_option_id > 0)
	  		$where_options_str .= "AND do.id = $deal_option_id ";		  	


	  $dealData = $dealsModel->getDealOptionsData($where_options_str);


      $this->template->content->dealData = $dealData ;
      $this->template->content->dealDataInfo = $dealDataInfo ;
      $this->template->content->deal_option_id = $deal_option_id ;
	
    }
    else {
      // new, just render the form
    }
  }

  public function confirmvoucher($voucher_id = null) {

    $this->auto_render = false ;

    $vouchersModel = new Vouchers_Model() ;

    if ($voucher_id) {
      $vouchersModel->confirmVoucher($voucher_id) ;
    }


    $voucherData = $vouchersModel->getData(array('id' => $voucher_id)) ;

    $createdVouchers = $vouchersModel->getData(array("deal_id" => $voucherData[0]->deal_id, 'confirmed' => 1)) ;
    
    $dealsModel = new Deals_Model() ;
    $dealData = $dealsModel->getData(array("id" => $voucherData[0]->deal_id)) ;

    //izvadi broj na fiktivni vauceri
	$dealsOptionsModel = new Deal_Options_Model() ;
	$fiktivniVauceriByDeal = $dealsOptionsModel->getFiktivniVauceriByDeal($voucherData[0]->deal_id) ;

	//vkupen broj na vauceri = realni + fiktivni
	$vkBrojNaVauceri = ( count($createdVouchers) + $fiktivniVauceriByDeal );

    if ( $vkBrojNaVauceri >= $dealData[0]->min_ammount) {
      $notSentVouchers = $vouchersModel->getData(array("deal_id" => $voucherData[0]->deal_id, 'activated' => 0, 'confirmed' => 1)) ;

      if ($notSentVouchers) {
        foreach ($notSentVouchers as $notSentVoucher) {
          $vouchersModel->activateVoucher($notSentVoucher) ;
        }
      }
    }

    url::redirect("/admin/dealdetails/{$voucherData[0]->deal_id}") ;
  }

  public function deletevoucher($voucher_id = null) {

    $this->auto_render = false ;

    $vouchersModel = new Vouchers_Model() ;

    if ($voucher_id) {
	  //zemi od baza zapis povrzan so ovoj vaucer od tabelata vouchers za da mozes
	  //da go najdes deal_id pa posle da go prefrlis admin-ot na soodvetnata strana
      $voucherData = $vouchersModel->getData(array('id' => $voucher_id)) ;
      
	  //povikaj ja funkcijata za brisenje na vaucer
	  $vouchersModel->deleteVoucher($voucher_id) ;

	  //povikaj ja funkcijata za brisenje na affiliate earning
	  $vouchersModel->deleteAffiliateEarning($voucherData[0]->attempt_id) ;
      
	  //prefrli go admin-ot na soodvetnata strana otkako ke zavrsis
	  url::redirect("/admin/dealdetails/{$voucherData[0]->deal_id}") ;
    }

    url::redirect("/admin") ;
  }
  public function cancelvoucher($voucher_id = null) {

    $this->auto_render = false ;

    $vouchersModel = new Vouchers_Model() ;

    if ($voucher_id) {
      $voucherData = $vouchersModel->getData(array('id' => $voucher_id)) ;
      $vouchersModel->cancelVoucher($voucher_id) ;
      url::redirect("/admin/dealdetails/{$voucherData[0]->deal_id}") ;
    }

    url::redirect("/admin") ;
  }

  public function printvoucher($voucher_id = null) {
    if ($voucher_id) {
		
      
	  $vouchersModel = new Vouchers_Model() ;
			$voucherData = $vouchersModel->getVoucher($voucher_id);
		
			$vouchersModel->CreatePDF($voucherData[0]->attempt_id, $type = "D") ;
	  
	  //ZA TEST NA MAILOVI
	   //star kod za obicno printanje
      //$mailContent = $vouchersModel->renderVoucher($voucher_id) ;
      //print $mailContent ;
      
	
	  exit ;
	
    }
    else {
      url::redirect("/admin") ;
    }
  }
  
  
  public function printvoucherlist($deal_id = null) {
    
    $this->template->title = "Детали за попуст" ;
    $this->template->content = new View("/admin/dealdetailslist") ;
    
    $dealsModel = new Deals_Model();
    
    if ($deal_id) 
    {
	    //Get deal by deal id
	    $where_deal = array('id' => $deal_id);
	    $dealDataInfo = $dealsModel->getData($where_deal) ;

      	//print
	    $where_options_str = "d.id = $deal_id";
	    $dealData = $dealsModel->getDealOptionsData($where_options_str);

		$this->template->content->dealData = $dealData ;
		$this->template->content->dealDataInfo = $dealDataInfo ;

    }
    else {
      url::redirect("/admin") ;
    }
  }
  
  
  public function stats() {

	//ako se samo 1 (Jovica) i 5(Martina)
	$this->session = Session::instance();
	$userID = $this->session->get("user_id");
	if (!in_array($userID, array(1, 5)))//the id for superadmin
		url::redirect("/admin");
	
	//get variabli
	$search_email = $this->input->get("search_email", "");
	$customerActivated = $this->input->get("customerActivated", 0);
	
    $this->template->title = "Статистика" ;
    $this->template->content = new View("admin/stats") ;

    $customerModel = new Customer_Model();
    $grid = $customerModel->getCustomerGrid($search_email);
    
    $this->template->content->grid = $grid['count'] ;    
    $this->template->content->points = $grid['points'] ;    

    $this->template->content->last_login_arr = $grid['last_login_arr'] ;
    $this->template->content->last_purchase_arr = $grid['last_purchase_arr'] ;
    $this->template->content->active = $grid['active'] ;
    $this->template->content->customerIDs = $grid['customerIDs'] ;

    $this->template->content->search_email = $search_email;
    $this->template->content->customerActivated = $customerActivated;
  }

  public function newsletterstats() 
  {

  	//dokolku ne e setirana sesiskata variabla sto oznacuva deka vnesol password => 
  	//prefrli go na stranata kade ke mora da se logira
	$prijaveni_mailovi_logged = $this->session->get("prijaveni_mailovi_logged");

    if (!$prijaveni_mailovi_logged ) 
        url::redirect("/admin2/prijaveni_mailovi_login");

    $this->template->title = "Статистика - пријавени e-mail адреси" ;
    $this->template->content = new View("admin/newsletterstats") ;

	//get variabli
	$succes_msg = $this->input->get("succes_msg", 0);
	
	$from_day = $this->input->get("from_day", 1);
	$from_month = $this->input->get("from_month", 01);
	$from_year = $this->input->get("from_year", 2010);
	
	$to_day = $this->input->get("to_day", date("d"));
	$to_month = $this->input->get("to_month", date("m"));
	$to_year = $this->input->get("to_year", date("Y"));

	$search_email = $this->input->get("search_email", "");


	
    $newsletterModel = new Newsletter_Model();
	$grid = $newsletterModel->getGrid($from_year."-".$from_month."-".$from_day, $to_year."-".$to_month."-".$to_day, $search_email);
    
	//foreach ($grid as $value) {
    //  $gridArray[] = strtolower($value->email);
    //}
    //asort($gridArray);
	//$this->template->content->grid = $gridArray ;    
	
    $this->template->content->grid = $grid ;
	
	
	$this->template->content->succes_msg = $succes_msg ;
	
	$this->template->content->from_day = $from_day ;
	$this->template->content->from_month = $from_month ;
	$this->template->content->from_year = $from_year ;

	$this->template->content->to_day = $to_day ;
	$this->template->content->to_month = $to_month ;
	$this->template->content->to_year = $to_year ;

	$this->template->content->search_email = $search_email;     
  }

  public function sortByTitle($a, $b){
    return strcmp($a->title, $b->title);
  }

	public function finreport() {
		
		//zemi go logiraniot user
		$userID = $this->session->get("user_id");
		
		//get variabli
		$finreport_month = $this->input->get("finreport_month", date("n"));
		$finreport_year = $this->input->get("finreport_year", date("Y"));
		$vraboten_id = in_array($userID, array(1, 5)) ? $this->input->get("vraboten_id", 0) : $userID;
		$category_id = $this->input->get("category_id", 0);
		$partner_id = $this->input->get("partner_id", 0);
		
		//template
		$this->template->title = "Финансиски извештај" ;
		$this->template->content = new View("/admin/finreport") ;
		
		//zemi gi site vraboteni
		$vraboteniModel = new Vraboteni_Model() ;
		$allVraboteni = $vraboteniModel->getData() ;
		$this->template->content->allVraboteni = $allVraboteni;
		
		//zemi gi site kategorii
		$categoryModel = new Categories_Model() ;
		$allCategories = $categoryModel->getData() ;
		$this->template->content->allCategories = $allCategories;

		//zemi gi site partneri
		$partnerModel = new Partners_Model() ;
		$allPartners = $partnerModel->getData() ;
		$this->template->content->allPartners = $allPartners;

		
		//model
		$FinReportModel = new FinReport_Model() ;
		$finreportdata = $FinReportModel->getFinIzvestaj($finreport_year, $finreport_month, $vraboten_id, $category_id, $partner_id) ;
		$mesecniTrosociArray = $FinReportModel->getMesecniTrosoci($finreport_year, $finreport_month) ;
		$mesecniOpstiTrosoci = $mesecniTrosociArray[0]->vk_mesecni_trosoci;
		$vrabotenTrosociArray = $FinReportModel->getVrabotenTrosoci($vraboten_id, $finreport_year, $finreport_month) ;
		$vrabotenTrosoci = $vrabotenTrosociArray[0]->vk_vraboten_trosoci;
		$cntVouchersArray = $FinReportModel->getCntVouchersByVraboten($vraboten_id) ;
		$vkupen_broj_prodadeni_vauceri = $cntVouchersArray[0]->cnt_vouchers;
		$cntDealsArray = $FinReportModel->getCntDealsByVraboten($vraboten_id) ;
		$vkupen_broj_ponudi = $cntDealsArray[0]->cnt_ponudi;
		$mesecniPrihodiArray = $FinReportModel->getMesecniPrihodi($finreport_year, $finreport_month) ;
		$mesecniOpstiPrihodi = $mesecniPrihodiArray[0]->vk_mesecni_prihodi;
		
				
		$this->template->content->finreport_month = $finreport_month;
		$this->template->content->finreport_year = $finreport_year;
		$this->template->content->vraboten_id = $vraboten_id;
		$this->template->content->category_id = $category_id;
		$this->template->content->partner_id = $partner_id;
		$this->template->content->finreportdata = $finreportdata;
		$this->template->content->mesecniOpstiTrosoci = $mesecniOpstiTrosoci;
		$this->template->content->vrabotenTrosoci = $vrabotenTrosoci;
		$this->template->content->vkupen_broj_prodadeni_vauceri = $vkupen_broj_prodadeni_vauceri;
		$this->template->content->vkupen_broj_ponudi = $vkupen_broj_ponudi;
		$this->template->content->mesecniOpstiPrihodi = $mesecniOpstiPrihodi;
		$this->template->content->isPrintVersion = false;
	}
	
	public function finreportprint() {
		
		//zemi go logiraniot user
		$userID = $this->session->get("user_id");
		
		//get variabli
		$finreport_month = $this->input->get("finreport_month", date("n"));
		$finreport_year = $this->input->get("finreport_year", date("Y"));
		$vraboten_id = in_array($userID, array(1, 5)) ? $this->input->get("vraboten_id", 0) : $userID;
		$category_id = $this->input->get("category_id", 0);
		$partner_id = $this->input->get("partner_id", 0);
		
		//template
		$this->template->title = "Финансиски извештај" ;
		$this->template->content = new View("/admin/finreport") ;
		
		//zemi gi site vraboteni
		$vraboteniModel = new Vraboteni_Model() ;
		$allVraboteni = $vraboteniModel->getData() ;
		$this->template->content->allVraboteni = $allVraboteni;
		
		//zemi gi site kategorii
		$categoryModel = new Categories_Model() ;
		$allCategories = $categoryModel->getData() ;
		$this->template->content->allCategories = $allCategories;
		
		//zemi gi site partneri
		$partnerModel = new Partners_Model() ;
		$allPartners = $partnerModel->getData() ;
		$this->template->content->allPartners = $allPartners;
		
		//model
		$FinReportModel = new FinReport_Model() ;
		$finreportdata = $FinReportModel->getFinIzvestaj($finreport_year, $finreport_month, $vraboten_id, $category_id, $partner_id) ;
		$mesecniTrosociArray = $FinReportModel->getMesecniTrosoci($finreport_year, $finreport_month) ;
		$mesecniOpstiTrosoci = $mesecniTrosociArray[0]->vk_mesecni_trosoci;
		$vrabotenTrosociArray = $FinReportModel->getVrabotenTrosoci($vraboten_id, $finreport_year, $finreport_month) ;
		$vrabotenTrosoci = $vrabotenTrosociArray[0]->vk_vraboten_trosoci;
		$cntVouchersArray = $FinReportModel->getCntVouchersByVraboten($vraboten_id) ;
		$vkupen_broj_prodadeni_vauceri = $cntVouchersArray[0]->cnt_vouchers;
		$cntDealsArray = $FinReportModel->getCntDealsByVraboten($vraboten_id) ;
		$vkupen_broj_ponudi = $cntDealsArray[0]->cnt_ponudi;
		$mesecniPrihodiArray = $FinReportModel->getMesecniPrihodi($finreport_year, $finreport_month) ;
		$mesecniOpstiPrihodi = $mesecniPrihodiArray[0]->vk_mesecni_prihodi;
		
				
		$this->template->content->finreport_month = $finreport_month;
		$this->template->content->finreport_year = $finreport_year;
		$this->template->content->vraboten_id = $vraboten_id;
		$this->template->content->category_id = $category_id;
		$this->template->content->partner_id = $partner_id;
		$this->template->content->finreportdata = $finreportdata;
		$this->template->content->mesecniOpstiTrosoci = $mesecniOpstiTrosoci;
		$this->template->content->vrabotenTrosoci = $vrabotenTrosoci;
		$this->template->content->vkupen_broj_prodadeni_vauceri = $vkupen_broj_prodadeni_vauceri;
		$this->template->content->vkupen_broj_ponudi = $vkupen_broj_ponudi;
		$this->template->content->mesecniOpstiPrihodi = $mesecniOpstiPrihodi;
		$this->template->content->isPrintVersion = true;	
	}
	

	public function mesecnitrosoci() {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		//get variabli
		$finreport_month = $this->input->get("finreport_month", date("n"));
		$finreport_year = $this->input->get("finreport_year", date("Y"));
		
		//template
		$this->template->title = "Mесечни трошоци" ;
		$this->template->content = new View("/admin/mesecnitrosoci") ;
		
		$mesecnitrosociModel = new Mesecnitrosoci_Model() ;
		$where = array('year' => $finreport_year, 'month' => $finreport_month) ;
		$mesecniTrosociData = $mesecnitrosociModel->getData($where) ;
		
		$this->template->content->mesecniTrosociData = $mesecniTrosociData;
		$this->template->content->finreport_month = $finreport_month;
		$this->template->content->finreport_year = $finreport_year;
	}
	
	public function izbrisimesecentrosok() {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		//nema output
		$this->auto_render = false ;
		
		//get variabli
		$finreport_month = $this->input->get("finreport_month", date("n"));
		$finreport_year = $this->input->get("finreport_year", date("Y"));
		$id = $this->input->get("id", 0);
		
		$mesecnitrosociModel = new Mesecnitrosoci_Model() ;
		
		if ($id)
			$mesecnitrosociModel->delete($id);
		
		url::redirect("/admin/mesecnitrosoci?finreport_month=".$finreport_month."&finreport_year=".$finreport_year) ;
	}

	public function uredimesecentrosok() {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		$this->template->title = "Добар попуст, подесување" ;
		$this->template->content = new View("/admin/uredimesecentrosok") ;
		
		//get variabli
		$finreport_month = $this->input->get("finreport_month", date("n"));
		$finreport_year = $this->input->get("finreport_year", date("Y"));
		$id = $this->input->get("id", 0);
		
		$mesecnitrosociModel = new Mesecnitrosoci_Model() ;
		
		if (request::method() == 'post') {
			//should save then
			$post = $this->input->post() ;
			
			$mesecnitrosociModel->saveData($post) ;
			url::redirect("/admin/mesecnitrosoci?finreport_month=".$finreport_month."&finreport_year=".$finreport_year) ;
		}
		if ($id) {
			//edit
			$where = array('id' => $id) ;
			$mesecniTrosociData = $mesecnitrosociModel->getData($where) ;
			$this->template->content->mesecniTrosociData = $mesecniTrosociData;
		}
		else {
			// new, just render the form
		}
		
		$this->template->content->finreport_month = $finreport_month;
		$this->template->content->finreport_year = $finreport_year;
	}
	
	
	public function dealexpences($deal_id = null) {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		//if ($userID != '1')//the id for superadmin
		//	url::redirect("/admin") ;
		
					
		//template
		$this->template->title = "Трошоци" ;
		$this->template->content = new View("/admin/dealexpences") ;
		
		$dealsModel = new Deals_Model() ;
		$where = array('id' => $deal_id) ;
		$dealData = $dealsModel->getData($where) ;
		$this->template->content->dealData = $dealData[0] ;
		
		if ($deal_id) {
			//print
			$dealexpencesModel = new Dealexpences_Model() ;
			$where = array('deal_id' => $deal_id) ;
			$dealexpencesData = $dealexpencesModel->getData($where) ;
			
			$this->template->content->dealExpencesData = $dealexpencesData;
		}
		else {
			url::redirect("/admin") ;
		}
	}

	public function izbrisidealexpence($id = null, $deal_id = null) {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		//if ($userID != '1')//the id for superadmin
		//	url::redirect("/admin") ;
			
		$this->auto_render = false ;
		
		if ($id && $deal_id) {
			$dealexpencesModel = new Dealexpences_Model() ;
			$dealexpencesModel->delete($id);
			
			url::redirect("/admin/dealexpences/$deal_id") ;
		}
		
		url::redirect("/admin") ;
	}	

	public function uredidealexpence($id = null, $deal_id = null) {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		//if ($userID != '1')//the id for superadmin
		//	url::redirect("/admin") ;
			
		$this->template->title = "Добар попуст, подесување" ;
		$this->template->content = new View("/admin/uredidealexpence") ;
		
		$dealsModel = new Deals_Model() ;
		$where = array('id' => $deal_id) ;
		$dealData = $dealsModel->getData($where) ;
		$this->template->content->dealData = $dealData[0] ;
		
		$dealexpencesModel = new Dealexpences_Model() ;
		
		if (request::method() == 'post') {
			//should save then
			$post = $this->input->post() ;
			
			$dealexpencesModel->saveData($post) ;
			url::redirect("/admin/dealexpences/$deal_id") ;
		}
		
		if ($id) {
			//edit
			$where = array('id' => $id) ;
			$dealexpencesData = $dealexpencesModel->getData($where) ;
			$this->template->content->dealExpencesData = $dealexpencesData ;
		}
		else {
			// new, just render the form
		}
	}
	
	//TROSOCI PO VRABOTEN
	public function vrabotentrosoci() {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		//get variabli
		$finreport_month = $this->input->get("finreport_month", date("n"));
		$finreport_year = $this->input->get("finreport_year", date("Y"));
		$vraboten_id = $this->input->get("vraboten_id", 0);
		
		//template
		$this->template->title = "Tрошоци по вработен" ;
		$this->template->content = new View("/admin/vrabotentrosoci") ;
		
		$vrabotentrosociModel = new Vrabotentrosoci_Model() ;
		$vraboteniModel = new Vraboteni_Model() ;
		
		$where = array('vraboten_id' => $vraboten_id, 'year' => $finreport_year, 'month' => $finreport_month) ;
		$vrabotenTrosociData = $vrabotentrosociModel->getData($where) ;
		
		$where = array('id' => $vraboten_id) ;
		$vrabotenData = $vraboteniModel->getData($where);
		
		$this->template->content->vrabotenTrosociData = $vrabotenTrosociData;
		$this->template->content->finreport_month = $finreport_month;
		$this->template->content->finreport_year = $finreport_year;
		$this->template->content->vraboten_id = $vraboten_id;
		$this->template->content->vraboten_name = $vrabotenData[0]->name;
	}
	
	public function izbrisivrabotentrosok() {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		//nema output
		$this->auto_render = false ;
		
		//get variabli
		$finreport_month = $this->input->get("finreport_month", date("n"));
		$finreport_year = $this->input->get("finreport_year", date("Y"));
		$vraboten_id = $this->input->get("vraboten_id", 0);
		$id = $this->input->get("id", 0);
		
		$vrabotentrosociModel = new Vrabotentrosoci_Model() ;
		
		if ($id)
			$vrabotentrosociModel->delete($id);
		
		url::redirect("/admin/vrabotentrosoci?finreport_month=".$finreport_month."&finreport_year=".$finreport_year."&vraboten_id=".$vraboten_id) ;
	}
	
	public function uredivrabotentrosok() {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		$this->template->title = "Добар попуст, подесување" ;
		$this->template->content = new View("/admin/uredivrabotentrosok") ;
		
		//get variabli
		$finreport_month = $this->input->get("finreport_month", date("n"));
		$finreport_year = $this->input->get("finreport_year", date("Y"));
		$vraboten_id = $this->input->get("vraboten_id", 0);
		$id = $this->input->get("id", 0);
		
		$vrabotentrosociModel = new Vrabotentrosoci_Model() ;
		
		if (request::method() == 'post') {
			//should save then
			$post = $this->input->post() ;
			
			$vrabotentrosociModel->saveData($post) ;
			url::redirect("/admin/vrabotentrosoci?finreport_month=".$finreport_month."&finreport_year=".$finreport_year."&vraboten_id=".$vraboten_id) ;
		}
		if ($id) {
			//edit
			$where = array('id' => $id) ;
			$vrabotenTrosociData = $vrabotentrosociModel->getData($where) ;
			$this->template->content->vrabotenTrosociData = $vrabotenTrosociData;
		}
		else {
			// new, just render the form
		}
		
		$vraboteniModel = new Vraboteni_Model() ;
		$where = array('id' => $vraboten_id) ;
		$vrabotenData = $vraboteniModel->getData($where);
		$this->template->content->vraboten_name = $vrabotenData[0]->name;
		
		$this->template->content->finreport_month = $finreport_month;
		$this->template->content->finreport_year = $finreport_year;
		$this->template->content->vraboten_id = $vraboten_id;
	}
	

	public function changepassword() {
		
		//zemi go ID-to na logiraniot administrator
		$userID = $this->session->get("user_id");
		
		$this->template->title = kohana::lang("prevod.website_title");
		$this->template->content = new View("admin/changepassword");
		
		if (request::method() == 'post') {
			$post = $this->input->post();
			
			$password = $post['password'];
			$password1 = $post['password1'];
			$password2 = $post['password2'];
			
			$adminModel = new Admin_Model();
			
			$where = array('id' => $userID, 'password' => md5($password));
			$admin = $adminModel->getData($where);
			
			if (count($admin) == 1 and ($password1 == $password2)) {
				
				$where = array('id' => $userID, 'password' => md5($password1));
				$adminModel->saveData($where);
				$this->template->content->success = 'Лозинката е успешно сменета.';
			} else {
				if (count($admin) == 0) {
					$this->template->content->error = 'Ја згрешивте старата лозинка.';
				}
				if ($password1 != $password2) {
					$this->template->content->error = 'Лозинките не се совпаѓаат.';
				}
			}
		}
	}	

	//deal incomes
	public function dealincomes($deal_id = null) {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		
		//template
		$this->template->title = "Приходи" ;
		$this->template->content = new View("/admin/dealincomes") ;
		
		$dealsModel = new Deals_Model() ;
		$where = array('id' => $deal_id) ;
		$dealData = $dealsModel->getData($where) ;
		$this->template->content->dealData = $dealData[0] ;
		
		if ($deal_id) {
			//print
			$dealincomesModel = new Dealincomes_Model() ;
			$where = array('deal_id' => $deal_id) ;
			$dealincomesData = $dealincomesModel->getData($where) ;
			
			$this->template->content->dealIncomesData = $dealincomesData;
		}
		else {
			url::redirect("/admin") ;
		}
	}
	
	public function izbrisidealincome($id = null, $deal_id = null) {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		$this->auto_render = false ;
		
		if ($id && $deal_id) {
			$dealincomesModel = new Dealincomes_Model() ;
			$dealincomesModel->delete($id);
			
			url::redirect("/admin/dealincomes/$deal_id") ;
		}
		
		url::redirect("/admin") ;
	}	
	
	public function uredidealincome($id = null, $deal_id = null) {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		$this->template->title = "Добар попуст, подесување" ;
		$this->template->content = new View("/admin/uredidealincome") ;
		
		$dealsModel = new Deals_Model() ;
		$where = array('id' => $deal_id) ;
		$dealData = $dealsModel->getData($where) ;
		$this->template->content->dealData = $dealData[0] ;
		
		$dealincomesModel = new Dealincomes_Model() ;
		
		if (request::method() == 'post') {
			//should save then
			$post = $this->input->post() ;
			
			$dealincomesModel->saveData($post) ;
			url::redirect("/admin/dealincomes/$deal_id") ;
		}
		
		if ($id) {
			//edit
			$where = array('id' => $id) ;
			$dealincomesData = $dealincomesModel->getData($where) ;
			$this->template->content->dealIncomesData = $dealincomesData ;
		}
		else {
			// new, just render the form
		}
	}	
	//end deal incomes
	
	
	//mesecni prihodi
	public function mesecniprihodi() {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		//get variabli
		$finreport_month = $this->input->get("finreport_month", date("n"));
		$finreport_year = $this->input->get("finreport_year", date("Y"));
		
		//template
		$this->template->title = "Mесечни приходи" ;
		$this->template->content = new View("/admin/mesecniprihodi") ;
		
		$mesecniprihodiModel = new Mesecniprihodi_Model() ;
		$where = array('year' => $finreport_year, 'month' => $finreport_month) ;
		$mesecniPrihodiData = $mesecniprihodiModel->getData($where) ;
		
		$this->template->content->mesecniPrihodiData = $mesecniPrihodiData;
		$this->template->content->finreport_month = $finreport_month;
		$this->template->content->finreport_year = $finreport_year;
	}
	
	public function izbrisimesecenprihod() {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		//nema output
		$this->auto_render = false ;
		
		//get variabli
		$finreport_month = $this->input->get("finreport_month", date("n"));
		$finreport_year = $this->input->get("finreport_year", date("Y"));
		$id = $this->input->get("id", 0);
		
		$mesecniprihodiModel = new Mesecniprihodi_Model() ;
		
		if ($id)
			$mesecniprihodiModel->delete($id);
		
		url::redirect("/admin/mesecniprihodi?finreport_month=".$finreport_month."&finreport_year=".$finreport_year) ;
	}
	
	public function uredimesecenprihod() {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		$this->template->title = "Добар попуст, подесување" ;
		$this->template->content = new View("/admin/uredimesecenprihod") ;
		
		//get variabli
		$finreport_month = $this->input->get("finreport_month", date("n"));
		$finreport_year = $this->input->get("finreport_year", date("Y"));
		$id = $this->input->get("id", 0);
		
		$mesecniprihodiModel = new Mesecniprihodi_Model() ;
		
		if (request::method() == 'post') {
			//should save then
			$post = $this->input->post() ;
			
			$mesecniprihodiModel->saveData($post) ;
			url::redirect("/admin/mesecniprihodi?finreport_month=".$finreport_month."&finreport_year=".$finreport_year) ;
		}
		if ($id) {
			//edit
			$where = array('id' => $id) ;
			$mesecniPrihodiData = $mesecniprihodiModel->getData($where) ;
			$this->template->content->mesecniPrihodiData = $mesecniPrihodiData;
		}
		else {
			// new, just render the form
		}
		
		$this->template->content->finreport_month = $finreport_month;
		$this->template->content->finreport_year = $finreport_year;
	}
	//end mesecni prihodi


	public function editvoucherdate($id = null, $deal_option_id = null) {
		
		$this->template->title = "Добар попуст, подесување" ;
		$this->template->content = new View("/admin/editvoucherdate") ;
		
		//izvadi gi informaciite za ponudata
		$dealsModel = new Deal_Options_Model() ;
		$where = array('id' => $deal_option_id) ;
		$dealData = $dealsModel->getData($where) ;
		$this->template->content->dealData = $dealData[0] ;
		
		//kreiraj model za vaucer-ot
		$vouchersModel = new Vouchers_Model();		
				
		if (request::method() == 'post') {
			//should save then
			$post = $this->input->post() ;
			
			$vouchersModel->saveData($post) ;
			url::redirect("/admin/dealdetails/".$dealData[0]->deal_id) ;
		}
		
		if ($id) {
			//edit
			$where = array('id' => $id) ;
			$voucherData = $vouchersModel->getData($where) ;
			$this->template->content->voucherData = $voucherData[0] ;
		}
		else {
			// new, just render the form
			url::redirect("/admin/dealdetails/".$dealData[0]->deal_id) ;
		}
	}
	
	public function zastedivme() {
		// Disable auto-rendering
		$this->auto_render = FALSE ;
		
		$vouchersModel = new Vouchers_Model();
		
		echo 'zastedivme: '.$vouchersModel->zastedivme() ;
	}


	public function multiple_end_time_method() {
		// Disable auto-rendering
		$this->auto_render = FALSE ;
		
		$post = $this->input->post() ;
		//die(print_r($post));
		
		$get_str = "?type=".$post["type"]."&category_id_selected=".$post["category_id_selected"]."&vraboten_id=".$post["vraboten_id"]."&deal_name_filter=".$post["deal_name_filter"]."&partner_id_selected=".$post["partner_id_selected"];
		
		//ako formata e submit-irana so klikanje na kopceto so id = group_end_time_submit  (za vnesuvanje na Kraj na ponudi)
		if(isset($post["group_end_time_submit"]))
		{

			$invalid_deals_ids = "";
			$dealsModel = new Deals_Model() ;
			$dealOptionsModel = new Deal_Options_Model() ;


			//za sekoj slucaj se proveruva dali e odbrana barem edna ponuda za menuvanje da datumot za istekuvanje
			if(isset($post["multiple_valid_to_arr"]) and count($post["multiple_valid_to_arr"]) > 0)
			{
				//die(print_r($post["multiple_valid_to_arr"]));

				//napravi kopija od arra-ot so deal_ids
				$final_arr = $post["multiple_valid_to_arr"];

				$dealOptionsModel->multiple_valid_to($post['datepicker_valid_to'], $final_arr);

			}//if(isset($post["multiple_valid_to_arr"]) and count($post["multiple_valid_to_arr"]) > 0)


			//za sekoj slucaj se proveruva dali e odbrana barem edna ponuda za menuvanje da datumot za kupuvanje
			if(isset($post["multiple_end_time_arr"]) and count($post["multiple_end_time_arr"]) > 0)
			{
				//die(print_r($post["multiple_end_time_arr"]));

				//napravi kopija od arra-ot so deal_ids
				$final_arr = $post["multiple_end_time_arr"];
				
				foreach ($post["multiple_end_time_arr"] as $arr_key => $arr_deal_id) 
				{
					//zemi go posledniot validen datum od opciite na soodvennata ponuda
					$lastValidToDate = $dealOptionsModel->getLastValidToDate($arr_deal_id) ;

					//ako datumot sto saka da go stavi e pogolem od posledniot VALID DATE na opciite na ponudata
					if(strtotime($post['datepicker_end']) > strtotime($lastValidToDate))
					{
						//trgni go toj element od array-ot za update-iranje
						unset($final_arr[$arr_key]);

						//ne update-iraj tuku stavi vo soodvetna promenliva za da izvestis
						$invalid_deals_ids .= $arr_deal_id.",";
					}


				}


				//dokolku ostana barem eden validen deal_id => update-iraj
				if(count($final_arr) > 0)
				{
					$dealsModel->multiple_end_time($post['datepicker_end'], $final_arr);
					

					//postavi go datumot za kraj na Opstite ponudi vo koi ovie ponudi vleguvaat
					foreach ($final_arr as $final_deal_id) 
					{	
						$this->generalOffersSetEndTime($final_deal_id);
					}
				}

				if($invalid_deals_ids != "")
				{
					$invalid_deals_ids = substr($invalid_deals_ids, 0, strlen($invalid_deals_ids) - 1 );
				}

			}//if(isset($post["multiple_end_time_arr"]) and count($post["multiple_end_time_arr"]) > 0)



			//vrati na index so soodvetna poraka
			url::redirect("/admin/".$get_str."&succes_msg=1&invalid_deals_ids=".$invalid_deals_ids);
		}



		url::redirect("/admin/".$get_str);
		
	}
	
	public function main_offers_scheduler() {
	
		//get variabli
		$main_offer = $this->input->get("main_offer", 0);
		
		//template
		$this->template->title = "Гл. понуди-календар" ;
		$this->template->content = new View("/admin/main_offers_scheduler") ;
		
		//model
		$dealsModel = new Deals_Model() ;
		$reportData = $dealsModel->getMainOffersSchedulerReport($main_offer) ;
		
		$this->template->content->main_offer = $main_offer;
		$this->template->content->reportData = $reportData;
	}
	

	public function new_main_offer_schedule($deal_id = null) {
		
		$this->template->title = "Добар попуст, подесување" ;
		$this->template->content = new View("/admin/new_main_offer_schedule") ;
		
		$error_msg = "";
		$dealsModel = new Deals_Model() ;
		
		if (request::method() == 'post') {

			$post = $this->input->post() ;
			
			if(!$dealsModel->checkExistMainOffersScheduler($post))
			{
				$dealsModel->saveMainOffersScheduler($post);
				
				url::redirect("/admin/index/") ;
			}
			else
				$error_msg = "Веќе постои понуда сетирана за избраниот Број на Главна понуда и избраниот Датум и час!<br/> Ве молиме изберете друга комбинација Број на Главна понуда - Датум и час.";
			
			$this->template->content->main_offer = $post["main_offer"];
			$this->template->content->date_time = $post["date_time"];
		}
		else
		{
			
			//dokolku e del od Opsta ponuda togas vrati go nazad so soodvetna poraka
			if($this->checkIfPartOfGeneralOffers($deal_id))
				url::redirect("/admin?partOfGeneralOffer=1") ;

			//setiraj default-ni podatoci		
			$this->template->content->main_offer = 1;
			$this->template->content->date_time = "";			
		}

		
		$dealData = $dealsModel->getData(array('id' => $deal_id)) ;
		$this->template->content->dealData = $dealData[0] ;

		
		$this->template->content->error_msg = $error_msg;
	}
	

	public function izbrisi_main_offer_schedule($id = null) {
		
		$this->auto_render = false ;
		
		if ($id) {
			$dealsModel = new Deals_Model() ;
			$dealsModel->delete_main_offer_schedule($id);
			
			url::redirect("/admin/main_offers_scheduler") ;
		}
		
		url::redirect("/admin") ;
	}


	public function finreport_izminati() {

		
		//dokolku ne e jovica ili martina togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if (!in_array($userID, array(1, 5)))
			url::redirect("/admin") ;
		
		//get variabli
		$search_deal_name = $this->input->get("search_deal_name", "");
		
		//template
		$this->template->title = "Фин. извештај (изминати понуди)" ;
		$this->template->content = new View("/admin/finreport_izminati") ;
		
		//model
		$FinReportModel = new FinReport_Model() ;
		$finreportdata = $FinReportModel->getFinIzvestajIzminati($search_deal_name) ;
		
		
		$this->template->content->finreportdata = $finreportdata;
		$this->template->content->search_deal_name = $search_deal_name;
		
	}

	public function vraboten($vraboten_id = null) {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		$this->template->title = "Добар попуст, подесување" ;
		$this->template->content = new View("/admin/vraboten") ;
		
		$vrabotenModel = new Vraboteni_Model() ;
		
		if (request::method() == 'post') {
			//should save then
			$post = $this->input->post() ;
			unset($post['files']);
			
			$vrabotenModel->saveData($post) ;
			url::redirect("/admin/vraboten/".$post["id"]) ;
		}
		
		if ($vraboten_id) {
			//edit
			$where = array('id' => $vraboten_id) ;
			$vrabotenData = $vrabotenModel->getData($where) ;
			$this->template->content->dealData = $vrabotenData ;
		}
		else {
			// new, just render the form
		}
	}
	
  public function vnesiDefaultOpcija($dealID, &$post)
  {
  	$dealOptionsModel = new Deal_Options_Model() ;

  	//proveri dali vekje postoi default opcija za soodvetnata opcija
  	$defaultOptionCnt = $dealOptionsModel->isThereDefaultOption($dealID);

  	//ako ne postoi togas vnesi ja.
  	if(!$defaultOptionCnt)
  	{
  		$insertArr = array(
  			'deal_id' => $dealID, 
  			'default_option' => 1,
  			'active' => 0,
  			'title_mk' => $post['title_mk'],
  			'title_mk_clean' => $post['title_mk_clean']
  		); 
			
		$dealOptionsModel->saveData($insertArr) ;
  	}
  }

  public function newOfferSendEmailToPartner(&$post)
  {
		
		$partnersModel = new Partners_Model() ;
		
		$getPartner = $partnersModel->getData(array("id"=> $post['partner_id'])) ;
		
		
		//ako e vnesen email za partner-ot
		if($getPartner[0]->email != "")
		{
			
			$mailContentModel = new Emailrenderer_Model();
			
			//napravi soodvetna sodrzina na mail-ot sto ke go pratis
			$maildata = array(
					'deal_name' => $post['title_mk'],
					'partner_username' => $getPartner[0]->username,
					'partner_password' => $getPartner[0]->password	
					);
			$mailContent = $mailContentModel->render("partner_nova_ponuda", $maildata);
			//die($mailContent);
			
			//zemi go email-ot na partnerot
			
			$recipient_arr = array("to"=>$getPartner[0]->email);
			// "cc"=> Kohana::config('config.kupinapopust_contact_email')
			
			//die(print_r($recipient_arr));		
			
			//kreiraj FROM info	
			$email_config = Kohana::config('email');
			$from = array($email_config['from']['email'], $email_config['from']['name']);
			
			//isprati email
			$result = email::send($recipient_arr, $from, 'Договорената понуда е активна на kupinapopust.mk', $mailContent, true); 
		}

  }


	public function newsletter_multiple_delete() {
		// Disable auto-rendering
		$this->auto_render = FALSE ;

	  	//dokolku ne e setirana sesiskata variabla sto oznacuva deka vnesol password => 
	  	//prefrli go na stranata kade ke mora da se logira
		$prijaveni_mailovi_logged = $this->session->get("prijaveni_mailovi_logged");

	    if (!$prijaveni_mailovi_logged ) 
	        url::redirect("/admin2/prijaveni_mailovi_login");
		
		//get variabli
		$from_day = $this->input->get("from_day", date("d"));
		$from_month = $this->input->get("from_month", date("m"));
		$from_year = $this->input->get("from_year", date("Y"));
		
		$to_day = $this->input->get("to_day", date("d"));
		$to_month = $this->input->get("to_month", date("m"));
		$to_year = $this->input->get("to_year", date("Y"));

		$search_email = $this->input->get("search_email", "");
		
		$post = $this->input->post() ;
		
		
		//ako formata e submit-irana
		if(isset($post["delete_submit"]))
		{
			//za sekoj slucaj se proveruva dali e odbrana barem edna ponuda
			if(isset($post["multiple_delete_arr"]) and count($post["multiple_delete_arr"]) > 0)
			{
				$newsletterModel = new Newsletter_Model();
				
				//povikaj ja soodvetnata f-ja od db klasata
				$rcd_aff = $newsletterModel->delete_multiple($post["multiple_delete_arr"]);
				
				//dokolku pomina uspesno
				if($rcd_aff)
				{
					//vrati na index so soodvetna poraka
					url::redirect("/admin/newsletterstats?succes_msg=1&from_day=$from_day&from_month=$from_month&from_year=$from_year&to_day=$to_day&to_month=$to_month&to_year=$to_year&search_email=$search_email");
				}
				
			}
		}

		
		url::redirect("/admin/newsletterstats");
		
	}

	public function deal_delete_video($deal_id = null) {
		
		$this->auto_render = FALSE ;
		
		$dealsModel = new Deals_Model() ;
				
		$update_arr = array('id' => $deal_id, 'ponuda_video' => ''); 
			
		$dealsModel->saveData($update_arr) ;
		
		url::redirect("/admin");
		
	}

	private function checkIfPartOfGeneralOffers($deal_id = 0)
	{
		$retValue = 0;

		if((int) $deal_id > 0)
		{	
			$generalOfferDealsModel = new General_Offer_Deals_Model();

		    //izvadi gi site opsti ponudi vo koi sto ovaa ponuda e izbrana
			$where = array('deal_id' => $deal_id);
			$generalOfferDealsRelation = $generalOfferDealsModel->getData($where) ;
	
			$retValue = count($generalOfferDealsRelation);
		}

		return $retValue;
	}


	public function reactivate_customer($customerID = 0, $customerEmail = "", $search_email = "") 
	{

		if((int)$customerID == 0 || $customerEmail == "")
			url::redirect("/admin");

		$customerModel = new Customer_Model();

    	//REAKTIVIRAJ GO KORISNIKOT
    	$checkFlag = $customerModel->customerReactivate($customerID, $customerEmail);


    	//rediret so poraka za uspesna reaktivacija
    	if($checkFlag)
    		url::redirect("/admin/stats?customerActivated=1&search_email=".$search_email);

        url::redirect("/admin");
    }

  public function __call($method, $arguments) {
    // Disable auto-rendering
    $this->auto_render = FALSE ;
    echo 'page not found' ;
  }

}