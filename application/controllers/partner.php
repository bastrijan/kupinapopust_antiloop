<?php defined('SYSPATH') OR die('No direct access allowed.');

class Partner_Controller extends Default_Controller {
	
	public function __construct() {
		//$this->template = 'layouts/admin';
		$this->template = 'layouts/partner';
		
		if (in_array(Router::$method, array('printvoucherlist'))) {
			$this->template = 'layouts/publicprint' ;
		}
		
		parent::__construct() ;
	}
	
	public $session = '' ;
	
	public function index() {
		$this->template->title = "Partner welcome" ;
		$this->template->content = new View("partner/index") ;

		//zemi go ID-to na logiraniot partner
		$userID = $this->session->get("user_id");
		
		//KREIRAJ GI POTREBNITE MODELI
		$dealsModel = new Deals_Model() ;
		$vouchersModel = new Vouchers_Model();
		$partnersModel = new Partners_Model();

		//IZVADI INFO ZA OPCIITE OD PONUDATA
		$dealsData = '' ;
		
		$type = $this->input->get("type", 'current');
		
		if ($type == 'all') {
			$function = 'getAllOffersPartner';
		} elseif ($type == 'current'){
			$function = 'getCurrentOffersPartner';
		} else{
			$function = 'getExpiredOffersPartner';
		} 
		
		$this->template->content->selected = $type ;
		
		$dealsData = $dealsModel->$function(0, $userID);

		$vouchers_count = array();

		foreach ($dealsData as $dealData) {
			$vouchers_count[$dealData->deal_id][$dealData->deal_option_id] = $vouchersModel->getCntPlateniVauceri($dealData->deal_id, $dealData->deal_option_id); 
		}

		//izvadi informacija za SUMA ZA NAPLATA
		$infoZaPotvrdaNaIsplata = $partnersModel->getInfoZaPotvrdaNaIsplata($userID); 

		//OUTPUT INFORMACIITE STO ODAT VO VIEW-TO
		$this->template->content->vouchers_count = $vouchers_count;
		$this->template->content->dealsData = $dealsData ;
		$this->template->content->sumaZaPotvrdaNaIsplata = $infoZaPotvrdaNaIsplata[0]->suma_za_isplata;
	}




	public function dealdetails($deal_id = null, $deal_option_id = null) {
		
		$this->template->title = "Детали за попуст" ;
		$this->template->content = new View("/partner/dealdetails") ;
		
		//zemi go ID-to na logiraniot partner
		$userID = $this->session->get("user_id");
		
		$dealsModel = new Deals_Model() ;
		$vouchersModel = new Vouchers_Model() ;
		
		if ($deal_id && $deal_option_id) {
                        
            if(request::method() == 'post'){
                $post = $this->input->post();
                
                $unchecked_arr = array();
                if($post['unchecked'] != ''){
                    $post['unchecked'] = rtrim($post['unchecked'], ',');
                    
                    $unchecked_arr = explode(',', $post['unchecked']);
                }
                
                $checked_arr = array();
                if(isset($post['voucherUsed'])){
                    $checked_arr = $post['voucherUsed'];
                    
                }
                
                $updateVauchers = $vouchersModel->usedVoucher($checked_arr, $unchecked_arr);
                $this->template->content->type = 1 ;
            }
                    
			//edit
		    $where_options_str = "d.id = $deal_id AND do.id = $deal_option_id AND d.partner_id = $userID";
		    $dealData = $dealsModel->getDealOptionsData($where_options_str);

			
			if($dealData)
			{
				$where2 = array('deal_id' => $deal_id, 'deal_option_id' => $deal_option_id, 'confirmed' => 1) ;
				$orderBy2 = array('time' => 'DESC') ;
				$allVouchers = $vouchersModel->getData($where2, $orderBy2) ;
				
				$this->template->content->dealData = $dealData[0] ;
				$this->template->content->vouchers = $allVouchers ;
			}
			else
				url::redirect("/partner") ;
		}
		else {
			// new, just render the form
			url::redirect("/partner") ;
		}
	}


	//ova e za avtomatskiot ajax na odredeno vreme sto se povikuva	
	 public function ajax_autosave_dealdetails(){

	    $this->template->title = "Добар попуст, подесување" ;
	    $this->template->content = "";

	    if (request::method() == 'post') {
	        
			//zemi go ID-to na logiraniot partner
			$userID = $this->session->get("user_id");

			$dealsModel = new Deals_Model() ;
			$vouchersModel = new Vouchers_Model() ;
	        
	        //should save then
	        $post = $this->input->post();

	        // die(print_r($post));


	        
          	$unchecked_arr = array();
          	$checked_arr = array();

            if($post['voucher_status'])
            {
                $checked_arr[] = $post['voucher_id'];
            }
            else
            {
            	$unchecked_arr[] = $post['voucher_id'];	
            }
            
            $updateVauchers = $vouchersModel->usedVoucher($checked_arr, $unchecked_arr);
		

			//izvadi info za soodvetniot voucer
			$where2 = array('id' => $post['voucher_id']) ;
			$voucherResult = $vouchersModel->getData($where2) ;
			$voucher = $voucherResult[0];

			$deal_id = $voucher->deal_id;
			$deal_option_id = $voucher->deal_option_id;

			$where_options_str = "d.id = $deal_id AND do.id = $deal_option_id AND d.partner_id = $userID";	
		    $dealDataResult = $dealsModel->getDealOptionsData($where_options_str);
		    $dealData = $dealDataResult[0];

		    $retStr = "";
   			if($voucher->confirm_paid_off)
   				$retStr = "<strong>Потврденo од ваша страна како исплатен</strong>";
   			elseif($voucher->paid_off)
   				$retStr = "<strong>Маркиранo од kupinapopust.mk како исплатен</strong>";
   			elseif($voucher->used)
   				$retStr = "<strong>Искористен</strong>";
   			elseif(time() > strtotime($dealData->valid_to))
   					$retStr = "<strong style='color: red;'>Истечен</strong>";
   				else
   					$retStr = "<strong>Неискористен</strong>";

	              
		      Kohana::config_set('config.global_xss_filtering', true);
		      $arr = array('status' => 'success', 'retStr' => $retStr);
		      die(json_encode($arr));
	    }

	}
	
	public function printvoucherlist($deal_id = null, $deal_option_id = null) {
		
		$this->template->title = "Детали за попуст" ;
		$this->template->content = new View("/partner/dealdetailslist") ;
		
		//zemi go ID-to na logiraniot partner
		$userID = $this->session->get("user_id");
		
		$dealsModel = new Deals_Model() ;
		$vouchersModel = new Vouchers_Model() ;
		
		if ($deal_id && $deal_option_id) {
			//edit
		    $where_options_str = "d.id = $deal_id AND do.id = $deal_option_id AND d.partner_id = $userID";
		    $dealData = $dealsModel->getDealOptionsData($where_options_str);
			
			if($dealData)
			{
				$where2 = array('deal_id' => $deal_id, 'deal_option_id' => $deal_option_id, 'confirmed' => 1) ;
				$allVouchers = $vouchersModel->getData($where2) ;
				
				$this->template->content->dealData = $dealData[0] ;
				$this->template->content->vouchers = $allVouchers ;
			}
			else
				url::redirect("/partner") ;
		}
		else {
			// new, just render the form
			url::redirect("/partner") ;
		}
	}
	

	public function login() {
		
		
		$this->template->title = "partnet welcome" ;
		$this->template->content = new View("/partner/login") ;
		
		if (request::method() == 'post') {
			$post = $this->input->post() ;
			$username = $post['username'] ;
			$password = $post['password'] ;
			
			$partnerModel = new Partners_Model() ;
			$where = array('username' => $username, 'password' => $password) ;
			$partner = $partnerModel->getData($where) ;
			if (count($partner) > 0) {
				
				$this->session = Session::instance() ;
				$this->session->set("user_id", $partner[0]->id) ;
				$this->session->set("user_type", 'partner') ;
				$this->session->set("partner_name", $partner[0]->name) ;
				url::redirect("/partner") ;
			}
			else {
				$this->template->content->error = 'Настаната е грешка! Не е точно Корисничкото име или Лозинката.<br/>(Error Occurred! The Username or Password is wrong)' ;
			}
		}
	}
	
	public function changepassword() {
		
		//zemi go ID-to na logiraniot partner
		$userID = $this->session->get("user_id");
		
		$this->template->title = kohana::lang("prevod.website_title");
		$this->template->content = new View("partner/changepassword");
		
		if (request::method() == 'post') {
			$post = $this->input->post();
			
			$password = $post['password'];
			$password1 = $post['password1'];
			$password2 = $post['password2'];
			
			$partnerModel = new Partners_Model() ;
			
			$where = array('id' => $userID, 'password' => $password);
			$partner = $partnerModel->getData($where);
			
			if (count($partner) == 1 and ($password1 == $password2)) {
				
				$where = array('id' => $userID, 'password' => $password1);
				$partnerModel->saveData($where);
				$this->template->content->success = 'Лозинката е успешно сменета (Password successfully changed).';
			} else {
				if (count($partner) == 0) {
					$this->template->content->error = 'Ја згрешивте старата лозинка (The old password is wrong).';
				}
				if ($password1 != $password2) {
					$this->template->content->error = 'Лозинките не се совпаѓаат (Password mismatched).';
				}
			}
		}
	}


	  public function confirm_as_paid($updateSuccess = 0) 
	  {
		    $this->template->title = kohana::lang("prevod.website_title");
		    $this->template->content = new View("/partner/confirm_as_paid") ;

		    $userID = $this->session->get("user_id");

		    $updateSuccess = (int) $updateSuccess;

		    //ako ne e vnesena brojka kako partner ID
		    if($userID == 0)
		    	url::redirect("/partner") ;

			//KREIRAJ GI POTREBNITE MODELI
			$vouchersModel = new Vouchers_Model();
			$partnersModel = new Partners_Model();

			//AKO E KLIKNATO KOPCETO - OZNACI KAKO ISPLATENO (POST)
	        if(request::method() == 'post')
	        {
	            $post = $this->input->post();
	            
	            $vouchersModel->confirmPaidOffVoucher($userID);

	            url::redirect("/partner/confirm_as_paid/1") ;

	        }

	    	$where = array('id' => $userID) ;
			$partnerData = $partnersModel->getData($where) ;
			$this->template->content->partnerData = $partnerData ;


	    	$this->template->content->updateSuccess = $updateSuccess ;


			//izvadi informacija za SUMA ZA NAPLATA
			$infoZaPotvrdaNaIsplata = $partnersModel->getInfoZaPotvrdaNaIsplata($userID); 

			//OUTPUT INFORMACIITE STO ODAT VO VIEW-TO
			$this->template->content->sumaZaPotvrdaNaIsplata = $infoZaPotvrdaNaIsplata[0]->suma_za_isplata;

	  }	


	  public function paid_off_notes() 
	  {
	  		$this->auto_render = FALSE ;

		    $userID = $this->session->get("user_id");

		    //ako ne e vnesena brojka kako partner ID
		    if($userID == 0)
		    	url::redirect("/partner") ;

			//KREIRAJ GI POTREBNITE MODELI
			$partnersModel = new Partners_Model();

			//AKO E KLIKNATO KOPCETO - OZNACI KAKO ISPLATENO (POST)
	        if(request::method() == 'post')
	        {
	            $post = $this->input->post();
	            
	        	unset($notes_post_arr);
	            $notes_post_arr["id"] = $userID;
	            $notes_post_arr["paid_off_notes"] = $post["paid_off_notes"];
	            $partnersModel->saveData($notes_post_arr) ;

	            url::redirect("/partner/confirm_as_paid/1") ;

	        }
	        else
	        	url::redirect("/partner/confirm_as_paid/") ;

	  }


  //ova e za avtomatskiot ajax za zacuvuvanje na NOTES (ZACUVAJ ZABELESKA)
 	 public function ajax_autosave_notes(){

	    $this->template->title = "Добар попуст, подесување" ;
	    $this->template->content = "";
		
		//zemi go ID-to na logiraniot partner
		$userID = $this->session->get("user_id");


	    if (request::method() == 'post') {
	        
	        $partnersModel = new Partners_Model();
	        
	        //should save then
	        $post = $this->input->post();
	        
	        unset($notes_post_arr);
            $notes_post_arr["id"] = $userID;
            $notes_post_arr["paid_off_notes"] = $post["paid_off_notes"];
            $partnersModel->saveData($notes_post_arr) ;
	              
		    Kohana::config_set('config.global_xss_filtering', true);
		    $arr = array('status' => 'success');
		    die(json_encode($arr));
	    }

	}



    public function __call($method, $arguments) {
        $this->auto_render = FALSE;
        echo 'This text is generated by __call. If you expected the index page, you need to use: welcome/index/'.substr(Router::$current_uri, 8);
    }

}