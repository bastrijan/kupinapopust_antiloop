<?php

defined('SYSPATH') OR die('No direct access allowed.') ;

class Admin2_Controller extends Default_Controller {
	
	public function __construct() 
	{
		ini_set('max_execution_time', 600); //600 seconds = 10 minutes=

		$this->template = 'layouts/admin' ;
		
		parent::__construct() ;
	}
	
	public function download($from_id = 1)
	{
		// Keep extra page output from being added to your file
		$this->auto_render = false;
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		//kreiraj podatoci
		$newsletterModel = new Newsletter_Model();
		$emailSubscribers = $newsletterModel->getData(array("id >= " => $from_id));
		
		$data = "";
		$num_rows = 0;
		$num_rows = count($emailSubscribers);
		$cnt = 0;
		
		
		foreach ($emailSubscribers as $subscriber) 
		{
			$cnt++;
			
			$data .= $subscriber->email;
			
			if($cnt < $num_rows)
				$data .= "\r\n";
			
		}
		// Don't forget to 'return' the result otherwise nothing happens
		return download::force(null, $data, "pretplatnici.csv");
	}
	
	public function download_by_provider($provider)
	{
		// Keep extra page output from being added to your file
		$this->auto_render = false;
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		//kreiraj podatoci
		$sendNewslettersModel = new Newsletter_Model();
		
		$queryAppend = "";
		
		if($provider != "")
			$queryAppend .= "WHERE ";
		
		if($provider == "gmail_yahoo")
		{ 
			$queryAppend .= "email LIKE '%@gmail.%' OR email LIKE '%@yahoo.%' ";
		}
		elseif($provider == "others")
		{
			$queryAppend .= "email NOT LIKE '%@gmail.%' AND email NOT LIKE '%@yahoo.%' ";
		}

		if($queryAppend != "")
		{
			$emailSubscribers = $sendNewslettersModel->getCustomersEmails($queryAppend, 0, 0);
			
			$data = "";
			$num_rows = 0;
			$num_rows = count($emailSubscribers);
			$cnt = 0;
			
			
			foreach ($emailSubscribers as $subscriber) 
			{
				$cnt++;
				
				$data .= $subscriber->email;
				
				if($cnt < $num_rows)
					$data .= "\r\n";
				
			}
			
			// Don't forget to 'return' the result otherwise nothing happens
			return download::force(null, $data, "pretplatnici.csv");
		}
		else
			die("greska!!!!");
	}
	
	
	public function download_partner()
	{
		// Keep extra page output from being added to your file
		$this->auto_render = false;
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		//kreiraj podatoci
		$partnerModel = new Partners_Model();
		$emailPartners = $partnerModel->getData();
		
		$data = "";
		$cnt = 0;
		foreach ($emailPartners as $partner) 
		{
			$cnt++;
			
			if($partner->email != "")
			{
				if($cnt > 1 && $data !="")
					$data .= "\r\n";
					
				$data .= $partner->email;
			}
			
		}
		// Don't forget to 'return' the result otherwise nothing happens
		return download::force(null, $data, "partneri.csv");
	}


	public function download_customers()
	{
		// Keep extra page output from being added to your file
		$this->auto_render = false;
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		//kreiraj podatoci
		$customerModel = new Customer_Model();
		$emailCustomers = $customerModel->getData();
		
		$data = "";
		$cnt = 0;
		foreach ($emailCustomers as $customer) 
		{
			$cnt++;
			
			if($customer->email != "")
			{
				if($cnt > 1 && $data !="")
					$data .= "\r\n";
					
				$data .= $customer->email;
			}
			
		}
		// Don't forget to 'return' the result otherwise nothing happens
		return download::force(null, $data, "dosegasni_kupuvaci.csv");
	}
        
        public function DailyReportWheel(){
            
            //dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
            $this->session = Session::instance();
            $userID = $this->session->get("user_id");
            //if ($userID != '1')//the id for superadmin
            //        url::redirect("/admin");
            
            //template
            $this->template->title = "WoF - Извештај" ;
            $this->template->content = new View("/admin/DailyReportWheel") ;
            
            //Vklucuvanje model
            $wfPlayers = new Wf_Players_Model();
            
            if (request::method() == 'post'){
                
                $day = $_POST['day_report'];
                $month = $_POST['month_report'];
                $year = $_POST['year_report'];
                $players = $_POST['players_report'];
                
                 //Filtriranje spored datumot
                 $dateByDay = $year.'-'.$month.'-'.$day;
                 
                 $ReportDate = array('Y' => $year, 'm' => $month, 'd' => $day);
                 $this->template->content->ReportDate = $ReportDate;
                 $this->template->content->ReportPlayers = $players;
                 
            }else{
                //Filtriranje spored datumot
                $dateByDay = date('Y-m-d');
                $players = 'all';
            }
            
            //odreduvanje na mesecot vo zavisnost od datumot kooj e odbran
            $dateByMonthFromDay = explode('-',$dateByDay);
            $dateByMonthFromDayString = $dateByMonthFromDay[0].'-'.$dateByMonthFromDay[1];
            
            
            
            //Prezemanje informacii za celosen dneven izvestaj
            $DayReports = $wfPlayers->getDayReport($dateByDay,$players);
            
            //Dneven broj na site igraci
            $CntDayAllPlayers = count($wfPlayers->getDayReport($dateByDay,'all'));
            //Dneven broj na dobitnite igraci
            $CntDayWinnerPlayers = count($wfPlayers->getDayReport($dateByDay,'winners'));
            //Mesecen broj na site igraci
            $CntMonthAllPlayers = $wfPlayers->getCountMonthPlayers($dateByMonthFromDayString,'all');
            //Mesecen broj na site igraci
            $CntMonthWinnerPlayers = $wfPlayers->getCountMonthPlayers($dateByMonthFromDayString,'winners');
            
            //Pass variables to view
            $this->template->content->DayReports = $DayReports;
            $this->template->content->CntDayAllPlayers = $CntDayAllPlayers;
            $this->template->content->CntDayWinnerPlayers = $CntDayWinnerPlayers;
            $this->template->content->CntMonthAllPlayers = $CntMonthAllPlayers;
            $this->template->content->CntMonthWinnerPlayers = $CntMonthWinnerPlayers;
        }
        
	public function amazon_newsletter_scheduler() {
		
		//template
		$this->template->title = "Amazon - Newsletter" ;
		$this->template->content = new View("/admin/amazon_newsletter_scheduler") ;
		
		//model
		$amazonNewsletterSchedulerModel = new Amazon_Newsletter_Scheduler_Model();		
		$categoriesModel = new Categories_Model();		
		
		$reportData = $amazonNewsletterSchedulerModel->getNewsletterSchedulerReport() ;
		$categoriesData = $categoriesModel->getCategories() ;
		
		$this->template->content->reportData = $reportData;
		$this->template->content->categoriesData = $categoriesData;
	}
		
	public function izbrisi_amazon_newsletter_scheduler($id = null) {
		
		$this->auto_render = false ;
		
		if ($id) {
			$amazonNewsletterSchedulerModel = new Amazon_Newsletter_Scheduler_Model();	
			$amazonNewsletterSchedulerModel->delete($id);
			
			url::redirect("/admin2/amazon_newsletter_scheduler") ;
		}
		
		url::redirect("/admin") ;
	}
		
  //------------------   Send newsletters through Amazon SES API   ------------------
	public function import_emails() {
		// Keep extra page output from being added to your file
		$this->auto_render = false;

		//se postavuva max_execution_time za da moze skriptata da vrti podolgo vreme bez da ja skine serverot.
		ini_set('max_execution_time', 12000); //12000 seconds = 200 minutes=
			
		$all_emails = file('final_lista_mejlovi_za_amazon.csv');
		//$invalid = file('Mailerlite e-mejli.csv');
		//$correctEmails=array_udiff($all, $invalid, 'strcasecmp');
		//$data = array (print_r($correctEmails)."\n");
		
		
		$newsletterModel = new Newsletter_Model();
		
		foreach($all_emails as $email)
		{
			//trgni prazni mesta od levo i desno
			$email = trim($email);
			
			//ako e dobro formiran email togas vnesi go vo temp tabelata
			if (filter_var($email, FILTER_VALIDATE_EMAIL)) 
			{
				$where = array("email" => $email);
				$newID = $newsletterModel->insert_temp_table($where);
				//echo $email."\r\n";
			}
			
		}

	}


  public function send_newsletters() {
	
	
						
		$error_msg = "";
						
		if (request::method() == 'post') {
				
				$post = $this->input->post();
				
				//ako ne e vnesen subject
				if($post['name'] == "")
					$error_msg .= "- Ве молам внесете Наслов(Subject)!<br/>";
					
				
				$queryAppend = "";
				
				//PRVA GRUPA
				$is_first_group_checked = false;
				if(isset($post['gmail']) && isset($post['yahoo']) && isset($post['others']))//ako site se odbrani
				{
					$queryAppend = "";
					$is_first_group_checked = true;
				}	
				else//ako ne se odbrani site
				{
					if(isset($post['gmail']))
					{ 
						$queryAppend .= "(email LIKE '%@gmail.%') ";
						
						$is_first_group_checked = true;
					}
					
					if(isset($post['yahoo']))
					{
						if($queryAppend != "")
							$queryAppend .= "OR ";
						
						$queryAppend .= "(email LIKE '%@yahoo.%') ";

						$is_first_group_checked = true;
					}
					
					if (isset($post['others']))
					{
						if($queryAppend != "")
							$queryAppend .= "OR ";
						
						$queryAppend .= "(email NOT LIKE '%@gmail.%' AND email NOT LIKE '%@yahoo.%') ";

						$is_first_group_checked = true;
					}
					
					if($queryAppend != "")
						$queryAppend = "AND (".$queryAppend.") ";
				}

				//VTORA GRUPA
				$is_second_group_checked = false;
				if(!$is_first_group_checked)
				{
					if(isset($post['reg_korisnici_kupuvaci']) && isset($post['reg_korisnici_primaci_newsletter']) )//ako site se odbrani
					{
						$queryAppend = "";
						$is_second_group_checked = true;
					}	
					else//ako ne se odbrani site
					{
						if(isset($post['reg_korisnici_kupuvaci']))
						{ 
							$queryAppend .= "(customer_id > 0)  ";

							$is_second_group_checked = true;
						}
						
						if(isset($post['reg_korisnici_primaci_newsletter']))
						{
							if($queryAppend != "")
								$queryAppend .= "OR ";
							
							$queryAppend .= "(customer_id = 0 OR customer_id IS NULL) ";

							$is_second_group_checked = true;
						}
						
						if($queryAppend != "")
							$queryAppend = "AND (".$queryAppend.") ";
					}
				}


				//SPORED AKTIVNOST
				$is_aktivnost_checked = false;

				if(isset($post['aktivni_korisnici']) && isset($post['neaktivni_korisnici']) )//ako site se odbrani
				{
					$is_aktivnost_checked = true;
				}	
				else//ako ne se odbrani site
				{
					if(isset($post['aktivni_korisnici']))
					{ 
						$queryAppend .= "AND DATEDIFF(DATE(NOW()), last_visited_website) <= 90  ";

						$is_aktivnost_checked = true;
					}
					elseif(isset($post['neaktivni_korisnici']))
					{
						
						$queryAppend .= "AND DATEDIFF(DATE(NOW()), last_visited_website) > 90 ";

						$is_aktivnost_checked = true;
					}
				}


				//ako ne e checkiran nieden checkbox
				if(!$is_first_group_checked && !$is_second_group_checked)
					$error_msg .= "- Ве молам одберете барем една група на email адреси!<br/>";

				if(!$is_aktivnost_checked)
					$error_msg .= "- Ве молам одберете корисници Според активност!<br/>";

				//ako nema greska
				if($error_msg == "")
				{
				
					//zacuvaj vo baza
					$post["send_to"] = $queryAppend;
					$post["current_status"] = 0;
					
					if(isset($post['gmail']))
						unset($post['gmail']);
						
					if(isset($post['yahoo']))
						unset($post['yahoo']);
					
					if(isset($post['others']))
						unset($post['others']);

					if(isset($post['reg_korisnici_kupuvaci']))
						unset($post['reg_korisnici_kupuvaci']);

					if(isset($post['reg_korisnici_primaci_newsletter']))
						unset($post['reg_korisnici_primaci_newsletter']);
					
					if(isset($post['aktivni_korisnici']))
						unset($post['aktivni_korisnici']);

					if(isset($post['neaktivni_korisnici']))
						unset($post['neaktivni_korisnici']);

					$amazonNewsletterSchedulerModel = new Amazon_Newsletter_Scheduler_Model();
					$amazonNewsletterSchedulerModel->saveData($post) ;
					
					//prefli na stranata kade sto se listaat site zapisi
					url::redirect("/admin2/amazon_newsletter_scheduler") ;
					
				}//if($error_msg == "")
				
		}//if (request::method() == 'post') {
	
		//setiranje na osnovni parametri
		$this->template->title = "Праќање на Amazon - Newsletter" ;
		$this->template->content = new View("/admin/send_newsletters") ;
		
		$categories = new Categories_Model();

		$this->template->content->categories = $categories->getCategories();
		
		$this->template->content->error_msg = $error_msg;
  }


	//CRONJOB NA SEKOJ POLN SAAT//
	public function send()
	{
		//se postavuva max_execution_time za da moze skriptata da vrti podolgo vreme bez da ja skine serverot.
		ini_set('max_execution_time', 18000); //18000 seconds = 300 minutes=
		

			
			//izvadi gi site odednas
			$cntAcctualSent = 0;
			$custNumber = 0;
			$sendNewslettersModel = new Newsletter_Model();
			$customersObjArr = $sendNewslettersModel->getCustomersEmails("", 0, 0);				
			
			//izbroj kolku gi ima
			//$custNumber = count($customersObjArr);
			
			foreach($customersObjArr as $customer_obj)
			{
						//if (filter_var($customer_obj->email, FILTER_VALIDATE_EMAIL)) 
						//{
							//zgolemi count na broj na email-i koi vistinski se prateni
							$cntAcctualSent++;
							echo $customer_obj->email."\r\n";
						//}
			}
					

		//die("Испратени се $cntAcctualSent (od baza se izvadeni $custNumber) e-mail пораки со Newsletter од типот: ");

		die();

		
	}


	public function newsletter_izbrisi_nepotrebni() {
		// Keep extra page output from being added to your file
		$this->auto_render = false;

		//se postavuva max_execution_time za da moze skriptata da vrti podolgo vreme bez da ja skine serverot.
		ini_set('max_execution_time', 12000); //12000 seconds = 200 minutes=
			
		$all_emails = file('nepotrebni.csv');

		
		
		$newsletterModel = new Newsletter_Model();
		
		foreach($all_emails as $email)
		{
			//trgni prazni mesta od levo i desno
			$email = trim($email);
			
			//ako e dobro formiran email togas vnesi go vo temp tabelata
			if ($email != "" && filter_var($email, FILTER_VALIDATE_EMAIL)) 
			{
					$newsletterModel->delete_by_email($email);
				
			}
			
		}

		die("END!");

	}


    public function dealoptions($deal_id = null) 
    {
		
		//ako postoi deal_id
		if ($deal_id) 
		{
			$this->template->title = "Понуда - Опции, подесување" ;
			$this->template->content = new View("/admin/dealoptions") ;
			
		
			$dealsModel = new Deals_Model();
            

			//Get deal by deal id
			$where = array('id' => $deal_id);
			$dealData = $dealsModel->getData($where) ;
            
            //get options            
			  $where_options_str = "d.id = $deal_id";
			  $dealOptionsData = $dealsModel->getDealOptionsData($where_options_str);
                        
                        
            $this->template->content->dealData = $dealData;
			$this->template->content->dealOptionsData = $dealOptionsData;
                        
                        
		}
		else //ako ne postoi deal_id
		{
			// isfrli go od tuka
			url::redirect("/admin");
		}
		
	}

    public function uredi_dealoption($id = null, $deal_id = null) 
    {
		
		if (!$deal_id) 
			url::redirect("/admin");

		$this->template->title = "Понуда - Опции, подесување" ;
		$this->template->content = new View("/admin/uredi_dealoption") ;

		$dealsModel = new Deals_Model();
        $dealOptionsModel = new Deal_Options_Model();

		if (request::method() == 'post') {
			//should save then
			$post = $this->input->post() ;
			unset($post['autosave']);

			if (!isset($post['active'])) {
				$post['active'] = 0 ;
			}
   
			$dealOptionsModel->saveData($post) ;
			url::redirect("/admin2/dealoptions/$deal_id") ;
		}

		//Get deal by deal id
		$where = array('id' => $deal_id);
		$dealData = $dealsModel->getData($where) ;
        $this->template->content->dealData = $dealData;

		if ($id) {
			//edit
			$where_opt = array('id' => $id) ;
			$dealOptionData = $dealOptionsModel->getData($where_opt) ;
			$this->template->content->dealOptionData = $dealOptionData;
		}

	}

    public function ajax_autosave_option() 
    {
	    $this->template->title = "Понуда - Опции, подесување" ;
	    $this->template->content = "";

	    if (request::method() == 'post') 
	    {
	        
	        $dealOptionsModel = new Deal_Options_Model();
	        
	        //should save then
	        $post = $this->input->post();
	        unset($post['autosave']);

			if (!isset($post['active'])) {
				$post['active'] = 0 ;
			}
	        
	        $insertID = $dealOptionsModel->saveData($post);
	        if (isset($post['id']) and $post['id']) {
	            $insertID = $post['id'] ;
	        }

	              
	      Kohana::config_set('config.global_xss_filtering', true);
	      $arr = array('status' => 'success', 'id' => $insertID);
	      die(json_encode($arr));
	    }
    }

	public function changepassword_prijaveni_mailovi() {
		
		//dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
		$userID = $this->session->get("user_id");
		if ($userID != '1')//the id for superadmin
			url::redirect("/admin") ;
		
		$this->template->title = kohana::lang("prevod.website_title");
		$this->template->content = new View("admin/changepassword_prijaveni_mailovi");
		
		if (request::method() == 'post') 
		{
			$post = $this->input->post();
			
			$password = $post['password'];
			$password1 = $post['password1'];
			$password2 = $post['password2'];
			
			$adminModel = new Common_Settings_Model();
			
			$where = array('password_prijaveni_mailovi' => md5($password));
			$admin = $adminModel->getData($where);
			//die(print_r($admin));
			
			if (count($admin) == 1 and ($password1 == $password2)) {
				
				$adminModel->savePassword($password, $password1);
				$this->template->content->success = 'Лозинката е успешно сменета.';
			} else {
				if (count($admin) == 0) {
					$this->template->content->error = 'Ја згрешивте старата лозинка.';
				}
				if ($password1 != $password2) {
					$this->template->content->error = 'Лозинките не се совпаѓаат.';
				}
			}
		}
	}

	public function prijaveni_mailovi_login() 
	{
  
    $this->template->title = "admin welcome" ;
    $this->template->content = new View("/admin/prijaveni_mailovi_login") ;

    if (request::method() == 'post') 
    {
      $post = $this->input->post() ;
      $password = $post['password'] ;

      $adminModel = new Common_Settings_Model() ;
      $where = array('password_prijaveni_mailovi' => md5($password)) ;
      $admin = $adminModel->getData($where) ;

      if (count($admin) > 0) 
	  {
			$this->session->set("prijaveni_mailovi_logged", "yes") ;
			url::redirect("/admin/newsletterstats") ;
      }
      else 
        $this->template->content->error = 'Password is not valid' ;

    }
  }

  public function remove_as_primary($deal_id = null, $reden_broj = 2, $return_type = "current") {

		$this->auto_render = false ;

		$dealsModel = new Deals_Model() ;

		if ($deal_id) {
		    //edit
		    $dealsModel->removePrimary($deal_id, $reden_broj) ;
		
			//kreiraj go fajlot za banner-ot
			$pass_create_file = "nlonlo321";
			// require_once Kohana::config('config.banner_generator_filepath');
		}
		else {
		  // new, just render the form
		}
		url::redirect("/admin?type=".$return_type) ;
  }


  //ova e koga ke klikne na SUBMIT kopceto
  public function autosave_general_deal($deal_id = null) {
    
    ini_set('max_execution_time', 300); //300 seconds = 5 minutes

    //za GD library da se koristi od driver-ite
	$img_config = array('driver' => 'GD');
		
    Kohana::config_set('config.global_xss_filtering', false);
    $this->template->title = "Добар попуст, подесување" ;
    $this->template->content = new View("/admin/autosave_general_deal") ;

    $dealsModel = new Deals_Model() ;
	
	$categoriesModel = new Categories_Model() ;
	$categoriesData = $categoriesModel->getData() ;
	$this->template->content->categoriesData = $categoriesData ;
	
    $vraboteniModel = new Vraboteni_Model() ;
    $allVraboteni = $vraboteniModel->getData() ;
    $this->template->content->allVraboteni = $allVraboteni;


    if (request::method() == 'post') 
    {

        //should save then
        $post = $this->input->post() ;
        unset($post['autosave']);
       	unset($post['submit_btn']);


        $files = $this->input->files() ;

        $post['visible'] = 1;
		
		if (isset($post['id']) && $post['id'] =="") 
		{
			$post['created'] = date("Y-m-d H:i:s");
		}
				
		unset($post['deal_status']);
		unset($post['old_deal_status']);		

		if (!isset($post['locked'])) {
			$post['locked'] = 0 ;
		}

		if (!isset($post['prati_mail_do_partner'])) {
			$post['prati_mail_do_partner'] = 0 ;
		}

		if (!isset($post['email_content_buy_switch'])) {
			$post['email_content_buy_switch'] = 0 ;
		}


		//stavi flag 1 vo poleto sto oznacuva deka e GENERAL DEAL
		$post['is_general_deal'] = 1 ;
		

		//povikaj fukcija za zacuvuvanje na podatocite vo baza
		$insertID = $dealsModel->saveData($post) ;

		if (isset($post['id']) and $post['id']) {
			$insertID = $post['id'] ;
		}


		//kreiraj objekt za optimizacija na slikata
		$optimizeImageWebServiceObject = new Optimize_Image_Webservice_Controller(Kohana::config('settings.tiny_png')['api_key']);


		if (isset($files["main_picture"]) and $files["main_picture"]['name']) 
		{
			//zacuvaj ja upload-iranata slika vo temp lokacija
			$filename = upload::save($files["main_picture"]) ;

			//zemi info za upload-iranata slika
			$imgInfo = pathinfo($filename) ;

			//info na novata slika
			$new_db_image_name = Kohana::config('config.offers_image_subfolder')."/deal_" . $insertID . "." . $imgInfo['extension'];
			$new_image_full_path = DOCROOT . 'pub/deals/'.$new_db_image_name;



			//ako ne moze da se kompresira slikata togas zacuvaj ja na klasicen nacin
			if(!$optimizeImageWebServiceObject->compress($filename, $new_image_full_path))
			{
				Image::factory($filename, $img_config)
				//  ->resize(100, 100, Image::WIDTH)
				//  ->quality(50)
					->save($new_image_full_path) ;

			}	

			// Remove the temporary file
			unlink($filename) ;
			
			//zacuvaj vo baza
			$data = array('id' => $insertID, 'main_img' => $new_db_image_name) ;
			$dealsModel->saveData($data) ;
		}
		
		if (isset($files["side_picture"]) and $files["side_picture"]['name']) {

			//zacuvaj ja upload-iranata slika vo temp lokacija
			$filename = upload::save($files["side_picture"]) ;

			//zemi info za upload-iranata slika
			$imgInfo = pathinfo($filename) ;

			//info na novata slika
			$new_db_image_name = Kohana::config('config.offers_image_subfolder')."/side_deal_" . $insertID . "." . $imgInfo['extension'];
			$new_image_full_path = DOCROOT . 'pub/deals/'.$new_db_image_name;



			//ako ne moze da se kompresira slikata togas zacuvaj ja na klasicen nacin
			if(!$optimizeImageWebServiceObject->compress($filename, $new_image_full_path))
			{
				Image::factory($filename, $img_config)
				//  ->resize(100, 100, Image::WIDTH)
				//  ->quality(50)
					->save($new_image_full_path) ;

			}	

			// Remove the temporary file
			unlink($filename) ;
			
			//zacuvaj vo baza
			$data = array('id' => $insertID, 'side_img' => $new_db_image_name) ;
			$dealsModel->saveData($data) ;



		}
		
		
		//vnesuvanje na default opcija dokolku ne postoi   
		$this->vnesiGeneralOfferDefaultOpcija($insertID, $post);



		Kohana::config_set('config.global_xss_filtering', true);

		//die(print_r($post));

		url::redirect("/admin?type=general_offers");

    }//if (request::method() == 'post')
    
    
    $SubcategoriesModel = new Subcategories_Model();
    if ($deal_id) {
		//edit
		$where = array('id' => $deal_id) ;
		$dealData = $dealsModel->getData($where) ;

		$where = array('category_id'=>$dealData[0]->category_id);

		$this->template->content->dealData = $dealData ;
    }
    else
	{
		// new, just render the form
		$where = array('category_id'=>$categoriesData[0]->id);
    }

    
	$SubcategoriesData = $SubcategoriesModel->getData($where) ;
	$this->template->content->SubcategoriesData = $SubcategoriesData ;
	
}


  //ova e za avtomatskiot ajax na odredeno vreme sto se povikuva	
  public function ajax_autosave_general_deal(){

    $this->template->title = "Добар попуст, подесување" ;
    $this->template->content = "";

    if (request::method() == 'post') {
        
         $dealsModel = new Deals_Model() ;
        
        //should save then
        $post = $this->input->post();
        unset($post['autosave']);
        unset($post['search_keyword']);
        unset($post['mail']);
        
//        die();

        if (!isset($post['id']) || $post['id'] =="") 
        {
        	$deal_id = 0 ;
        	unset($post['id']);
        }
        else
        	$deal_id = $post['id'] ;

        //PROVERKA end_time mora da bide pomal od $lastValidToDate
	    $dealOptionsModel = new Deal_Options_Model() ;

		
		if (isset($post['id']) && $post['id'] =="") 
		{
			$post['created'] = date("Y-m-d H:i:s") ;
		}

		$post['visible'] = 1;
		
        unset($post['deal_status']);
        unset($post['old_deal_status']);		
	
	
		//stavi flag 1 vo poleto sto oznacuva deka e GENERAL DEAL
		$post['is_general_deal'] = 1 ;
	
        //povikaj fukcija za zacuvuvanje na podatocite vo baza
        $insertID = $dealsModel->saveData($post);
    
        if (isset($post['id']) and $post['id']) {
            $insertID = $post['id'] ;
        }

		//vnesuvanje na default opcija dokolku ne postoi    //////TODO?????
		$this->vnesiGeneralOfferDefaultOpcija($insertID, $post);

              
      Kohana::config_set('config.global_xss_filtering', true);
      $arr = array('status' => 'success', 'id' => $insertID);
      die(json_encode($arr));
    }

}


	 public function general_offer_deals($general_offer_id = null) 
	 {
		$dealsModel = new Deals_Model();
		$generalOfferDealsModel = new General_Offer_Deals_Model();


	    if (request::method() == 'post' && $general_offer_id) 
	    {
	        //should save then
	        $post = $this->input->post() ;
	        unset($post['general_offer_deals_submit']);

	        //izbrisi gi site prethodno odbrani DEALS vo general_offer_deals
	        $generalOfferDealsModel->delete_selected_deals($general_offer_id);

	        //svrti gi site izbrani DEALS
	        $sava_array = array();

	        $start_date_final = "0000-00-00 00:00:00";
	        $end_date_final = "0000-00-00 00:00:00";

	        if(isset($post['general_offer_selected_deals']))
	        {
				foreach ($post['general_offer_selected_deals'] as $selected_deal_id) 
				{
			        //vnesi gi odbranite DEALS vo general_offer_deals
			        $sava_array = null;
			        $sava_array["general_offer_id"] = $general_offer_id;
			        $sava_array["deal_id"] = $selected_deal_id;

					$generalOfferDealsModel->saveData($sava_array) ;

			        //da se odredi date_start i date_end za GENERAL OFFER
					$where = array('id' => $selected_deal_id);
					$selectedDealData = $dealsModel->getData($where) ;

					if($start_date_final == "0000-00-00 00:00:00")
						$start_date_final = $selectedDealData[0]->start_time;
					elseif($start_date_final > $selectedDealData[0]->start_time )
							$start_date_final = $selectedDealData[0]->start_time;

					if($end_date_final < $selectedDealData[0]->end_time )
						$end_date_final = $selectedDealData[0]->end_time;


				}
	        }	

			//da se update-ira date_start i date_end vo GENERAL OFFER
			list($date_tmp, $time_tmp) = explode(' ',  $start_date_final);
			list($year_tmp, $month_tmp, $day_tmp) = explode('-', $date_tmp);


	        $sava_array = null;
	        $sava_array["id"] = $general_offer_id;
	        $sava_array["start_time"] = $start_date_final;
	        $sava_array["end_time"] = $end_date_final;

			$dealsModel->saveData($sava_array) ;



			url::redirect("/admin?type=general_offers");

	    }
		



		//ako postoi general_offer_id
		if ($general_offer_id) 
		{
			$this->template->title = "Понуда - Опции, подесување" ;
			$this->template->content = new View("/admin/general_offer_deals") ;
			
		
			//Get deal by general_offer_id
			$where = array('id' => $general_offer_id);
			$generalOfferData = $dealsModel->getData($where) ;
            
            //get all active deals            
			$allCurrentOffersData = $dealsModel->getCurrentOffers();
            
            //get selected deals
			$where = array('general_offer_id' => $general_offer_id);
			$selectedOffersData = $generalOfferDealsModel->getData($where) ;

			// die(print_r($selectedOffersData));
			$selectedOfferArray = array();
			foreach ($selectedOffersData as $selectedOffer) 
			{
				$selectedOfferArray[] = $selectedOffer->deal_id;
			}

			$noIncludedRealOffers = count($selectedOfferArray);
                        
            $this->template->content->generalOfferData = $generalOfferData;
			$this->template->content->allCurrentOffersData = $allCurrentOffersData;
            $this->template->content->selectedOfferArray = $selectedOfferArray;
            $this->template->content->noIncludedRealOffers = $noIncludedRealOffers;
                        
		}
		else //ako ne postoi deal_id
		{
			// isfrli go od tuka
			url::redirect("/admin");
		}
		
	}


  public function vnesiGeneralOfferDefaultOpcija($dealID, &$post)
  {
  	$dealOptionsModel = new Deal_Options_Model() ;

  	//proveri dali vekje postoi default opcija za soodvetnata opcija
  	$defaultOptionCnt = $dealOptionsModel->isThereDefaultOption($dealID);

  	//ako ne postoi togas vnesi ja.
  	if(!$defaultOptionCnt)
  	{
  		$insertArr = array(
  			'deal_id' => $dealID, 
  			'default_option' => 1,
  			'active' => 1,
  			'title_mk' => $post['title_mk'],
  			'title_mk_clean' => $post['title_mk_clean']
  		); 
			
		$dealOptionsModel->saveData($insertArr) ;
  	}
  }


   public function static_pages() {
		
		//template
		$this->template->title = "Содржини" ;
		$this->template->content = new View("/admin_static_pages/index") ;
		
		$staticModel = new Static_Model() ;
		
		$staticPagesData = $staticModel->getData() ;
		
		$this->template->content->staticPagesData = $staticPagesData;
	}

  public function manage_staticpages($page_identifier = null) 
  {

	    $this->template->title = "Добра зделка, подесување" ;
	    $this->template->content = new View("/admin_static_pages/manage") ;

	    $staticPagesModel = new Static_Model() ;

	    if (request::method() == 'post') {
		      //should save then
		      $post = $this->input->post() ;
			  unset($post['files']);
			   	
		      $staticPagesModel->saveData($post) ;

		      url::redirect("/admin2/static_pages") ;
	    }

	    if ($page_identifier) 
	    {
		      //edit
		      $where = array('identifier' => $page_identifier) ;
		      $staticPage = $staticPagesModel->getData($where) ;

		      if(count($staticPage) == 0)
		      		url::redirect("/admin2/static_pages") ;

		      $this->template->content->staticPage = $staticPage[0] ;
	    }

  }


	public function __call($method, $arguments) {
		// Disable auto-rendering
		$this->auto_render = FALSE ;
		echo 'page not found' ;
	}
	
}