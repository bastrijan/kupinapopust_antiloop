<?php

defined('SYSPATH') OR die('No direct access allowed.') ;

class Pay_Controller extends Default_Controller {
	
	public function index() {
		
		if (request::method() != 'post') {
			url::redirect("/") ;
		}
		
		$post = $this->input->post() ;
		
		$vouchersModel = new Vouchers_Model() ;
		
		$payAttemptID = $vouchersModel->savePayAttempt($post) ;
		//var_dump($post['payment']);
		//exit();
		
		if ($post['payment'] == 'cache') {
			$vouchersModel->createVoucher($post, $payAttemptID, 'cache') ;
			url::redirect("/pay/cache") ;
		}
		if ($post['payment'] == 'bank') {
			$vouchersModel->createVoucher($post, $payAttemptID, 'bank') ;
			$dealModel = new Deals_Model();
			$deal = $dealModel->getData(array("id"=>$post['discount_id']));
			if ($deal[0]->card) {
				url::redirect("/pay/bankcard") ;
			}   
			url::redirect("/pay/bank") ;
		}
		
		//goran
		//    $post["AmountToPay"] = 1 ;
		$ammount = $post["AmountToPay"] * $post["amount"];
		if (isset($post['points_used'])) {
			$ammount = $ammount - $post['points_used'] ;
		}
		
		$description = $post["Details1"] ;
		
		if (strpos($_SERVER["HTTP_REFERER"], "/buy/deal") === false
				and strpos($_SERVER["HTTP_REFERER"], "/buy/gift") === false) {
			print "Access forbidden" ;
			//print $_SERVER["HTTP_REFERER"];
			exit ;
		}
		
		if (!$payAttemptID) {
			print "Access forbidden payment" ;
			exit ;
		}        
		
		//    $bankStatemenDetails = "Kupinapopust.mk ������� ��. $payAttemptID" ;
		$preauthorization = new HalkPreAuthorization($ammount, $payAttemptID) ;
		$preauthorization->setLanuage($this->lang) ;
		//    $preauthorization->setEmail($post["Email"]) ;
		
		$params = $preauthorization->getInputStrings() ;
		
		$this->template->content = new View("pay/index") ;
		$this->template->title = kohana::lang("prevod.website_title") ;
		$this->template->content->params = $params ;    
		$this->template->content->halkUrl = $preauthorization->getUrl() ;
		$this->template->content->email = $post['Email'];
		$this->template->content->payAttemptId = $payAttemptID;
	}
	
	// index
	
	public function response() {
		
		$this->session->set("payed", false) ;
		$post = $this->input->post();
		
		//     $nesto = var_export($post, true);
		//    
		//    $mailBody = $nesto ;    
		//    $mailBody .= "post parameters = " . print_r($_POST, true) . "<br />" ;
		//
		//    $to = "dimitar.velkov@gmail.com" ;
		//    email::send($to, "error@kupinapopust.mk", "CPay bad request", $mailBody, true) ;
		
		$mdStatus = $_POST["mdStatus"];
		$ErrMsg = $_POST["ErrMsg"];
		
		if($mdStatus == 1 || $mdStatus == 2 || $mdStatus == 3 || $mdStatus == 4 || $mdStatus == 7)
		{
			$response = $post["Response"];
			if($response == "Approved")
			{
				$this->responseHalk($post);
			}
			else
			{
				url::redirect("/pay/error/$payAttemptID?exitif=true") ;
				// echo "Payment is NOT Successful. Error Message : ".$ErrMsg;
			} 
		}
		else
		{
			url::redirect("/pay/error/$payAttemptID?exitif=true") ;
			//echo "<h5>3D Authentication is NOT Successful !</h5>";
		}
		
		// if (!$response->isRequestConsistent()) {
		//   Kohana::log("error", "bad request") ;
		//   $this->badRequest($response) ;
		// }
		
		// if ($response->isSenderAuthenticated()) {
		//   $this->responseFromCpay($response) ;
		// }
		// else {
		//   $this->responseFromRedirect($response) ;
		// }
	}
	
	// index
	
	private function responseFromRedirect(PreauthorizationResponse $response) {
		
		if ($response->getReasonOfDecline()) {
			$payAttemptID = $response->getDetails2() ;
			url::redirect("/pay/error/$payAttemptID?exitif=true") ;
		}
		else {
			$this->session->set("payed", true) ;
			url::redirect("/pay/success?exitif=true") ;
		}
	}
	
	// responseFromRedirect
	
	private function responseHalk($post) {
		
		//    $post = $this->input->post();
		
		$payAttemptID = $post['payAttemptId'] ;
		$refID = $post['TransId'];
		$vouchersModel = new Vouchers_Model() ;
		
		$vouchersModel->savePaymentRef($payAttemptID, $refID);
		
		$payData = $vouchersModel->getPayAttempt($payAttemptID) ;
		
		$data = array("discount_id" => $payData[0]->deal_id, "amount" => $payData[0]->quantity) ;
		$vouchersModel->createVoucher($data, $payAttemptID, "card") ;
		
		
		$this->session->set("payed", true) ;
		
		url::redirect("pay/success/$payAttemptID?exitif=true") ;
		
		
		//        $orderPreauthorization = new OrderPreauthorizationResonse();
		//        $orderPreauthorization->savePreauthorizationCpay($response);
		exit ;
	}
	
	// responseFromCpay
	
	private function badRequest(PreauthorizationResponse $response) {
		// prati mail
		$mailBody = "" ;
		$mailBody .= "orders_id = " . $response->getDetails2() . "<br />" ;
		$mailBody .= "ip = " . $_SERVER["REMOTE_ADDR"] . "<br />" ;
		$mailBody .= "get parameters = " . print_r($_GET, true) . "<br />" ;
		$mailBody .= "post parameters = " . print_r($_POST, true) . "<br />" ;
		
		$to = "bastrijan@gmail.com" ;
		email::send($to, "error@kupinapopust.mk", "CPay bad request", $mailBody, true) ;
		
		// show error to the user
		url::redirect("/pay/error") ;
		
		exit ;
	}
	
	// badRequest
	
	public function success($id = '') {
		$this->template->content = new View('pay/success') ;
		$this->template->title = kohana::lang("prevod.website_title") ;
	}
	
	public function error($id = '') {
		$this->template->content = new View('pay/fail') ;
		$this->template->title = kohana::lang("prevod.website_title") ;
	}
	
	public function cache() {
		$this->template->content = new View('pay/cache') ;
		$this->template->title = kohana::lang("prevod.website_title") ;
	}
	
	public function bank() {
		$this->template->content = new View('pay/bank') ;
		$this->template->title = kohana::lang("prevod.website_title") ;
	}
	public function bankcard() {
		$this->template->content = new View('pay/card_bank') ;
		$this->template->title = kohana::lang("prevod.website_title") ;
	}
	
	public function __call($method, $arguments) {
		$this->auto_render = FALSE ;
		echo 'This text is generated by __call. If you expected the index page, you need to use: welcome/index/' . substr(Router::$current_uri, 8) ;
	}
	
	public function localtest() {
		$this->template->content = new View('pay/localtest') ;
		$this->template->title = kohana::lang("prevod.website_title") ;
		
		$post = $this->input->post() ;
		$this->template->content->payAttemptId = $post['payAttemptId'];
		
	}
	
}
