<?php

defined('SYSPATH') OR die('No direct access allowed.');

/****** LOGIKA ZA POLETO - welcome_again_status ******/
// Ako welcome_again_status = (1 ili 3) togas dodeli poeni
// Ako welcome_again_status = (0 ili 2) + (pominati se 90 dena neaktivno) togas prati mail za pottik
// Koga ke se prakja email za pottik ili koga ke mu se dodelat poeni INCREMENT + 1

// Vrednosti za welcome_again_status:
// 0 - ne pravi nisto (ne mu e isprateno do sega nieden mail za pottik)
// 1 - prv pat mu e ispraten mail za pottik
// 2 - prv pat dobil 50 poeni
// 3 - vtor pat mu e ispraten mail za pottik
// 4 - vtor pat dobil 50 poeni


/**
 * Default Kohana controller. This controller should NOT be used in production.
 * It is for demonstration purposes only!
 *
 * @package    Core
 * @author     Kohana Team
 * @copyright  (c) 2007-2008 Kohana Team
 * @license    http://kohanaphp.com/license.html
 */
class Customer_Controller extends Default_Controller {
	
	public function __construct() {
		$this->template = 'layouts/public' ;
		
		if(strpos(Router::$method, "_android") !== false) {
			$this->template = 'layouts/android' ;
		}
		
		parent::__construct();
	}

	public function index($typemobile = "") {
		
        $this->template->title = kohana::lang("prevod.website_title");
		$this->template->content = new View("customer/index".($typemobile == "android" ? "_android" : ""));

        $userID = $this->session->get("user_id");
        $type = $this->input->get("type", 0);
        $flagGiftPointsWelcome = $this->input->get("flagGiftPointsWelcome", 0);

        $vouchersModel = new Vouchers_Model();
        $customerModel = new Customer_Model();
		
		$limit_records = ($typemobile == "android" ? 1 : 0);
		
		$vouchers = $vouchersModel->getCustomerVouchersGrouped($userID, $limit_records);
		$cards = $vouchersModel->getCustomerGiftCards($userID, $limit_records);
		
		$latestVouchers = array();
		$latestCards = array();
	        
		if ($vouchers)     
			$latestVouchers =  array_shift($vouchers);

		if ($cards)
			$latestCards =  array_shift($cards);

		$points = $customerModel->getCustomerPoints($userID);
	  
		$this->template->content->latestVouchers = $latestVouchers;
		$this->template->content->vouchers = $vouchers;
		$this->template->content->latestCards = $latestCards;
		$this->template->content->cards = $cards;
		$this->template->content->points = $points;
		$this->template->content->type = $type;
		$this->template->content->flagGiftPointsWelcome = $flagGiftPointsWelcome;
		$this->template->content->user = $customerModel->getData(array("id" => $userID));
	    
		$pointsModel = new Points_Model();

		$records = $pointsModel->getHistoryRecords($userID);
		//zemi gi site poeni na customer-ot pogolemi od 0 a grupirani po den
		$recordsPoints = $pointsModel->getCustomerPointsGrouped($userID);

		$this->template->content->records = $records;
		$this->template->content->recordsPoints = $recordsPoints;
    }
	
	public function index_android() {
		$this->index("android");
	}  

	public function vouchers($typemobile = "") {
        $this->template->title = kohana::lang("prevod.website_title");
		$this->template->content = new View("customer/vouchers".($typemobile == "android" ? "_android" : ""));

        $userID = $this->session->get("user_id");
        $type = $this->input->get("type", 0);

        $vouchersModel = new Vouchers_Model();
        $customerModel = new Customer_Model();
        //$myVouchers = $vouchersModel->getData(array("customer_id" => $userID));
		$myVouchers = $vouchersModel->getCustomerVouchersGrouped($userID, 0, " AND va.gift = 0 ");
        $points = $customerModel->getCustomerPoints($userID);

        $this->template->content->vouchers = $myVouchers;
        $this->template->content->points = $points;
        $this->template->content->type = $type;
        $this->template->content->user = $customerModel->getData(array("id" => $userID));
    }


	public function vouchers_gift($typemobile = "") {
        $this->template->title = kohana::lang("prevod.website_title");
		$this->template->content = new View("customer/vouchers_gift");

        $userID = $this->session->get("user_id");
        $type = $this->input->get("type", 0);

        $vouchersModel = new Vouchers_Model();
        $customerModel = new Customer_Model();
        //$myVouchers = $vouchersModel->getData(array("customer_id" => $userID));
		$myVouchers = $vouchersModel->getCustomerVouchersGrouped($userID, 0, " AND va.gift = 1 ");
        $points = $customerModel->getCustomerPoints($userID);

        $this->template->content->vouchers = $myVouchers;
        $this->template->content->points = $points;
        $this->template->content->type = $type;
        $this->template->content->user = $customerModel->getData(array("id" => $userID));
    }
	
	public function vouchers_android() {
		$this->vouchers("android");
	}  

	public function cards($typemobile = "") {
        $this->template->title = kohana::lang("prevod.website_title");
		$this->template->content = new View("customer/cards".($typemobile == "android" ? "_android" : ""));

        $userID = $this->session->get("user_id");
        $type = $this->input->get("type", 0);

        $vouchersModel = new Vouchers_Model();
        $customerModel = new Customer_Model();
        //$myVouchers = $vouchersModel->getData(array("customer_id" => $userID));
		$myVouchers = $vouchersModel->getCustomerGiftCards($userID);
        //$points = $customerModel->getCustomerPoints($userID);

        $this->template->content->vouchers = $myVouchers;
        //$this->template->content->points = $points;
        $this->template->content->type = $type;
        $this->template->content->user = $customerModel->getData(array("id" => $userID));
    }
	
	public function cards_android() {
		$this->cards("android");
	}  

	public function personaldata($typemobile = "") {
        $this->template->title = kohana::lang("prevod.website_title");
		$this->template->content = new View("customer/personaldata".($typemobile == "android" ? "_android" : ""));

        $userID = $this->session->get("user_id");
        $this->template->content->displayForm = true;

        $customerModel = new Customer_Model();
        $user = $customerModel->getData(array('id' => $userID));


        if (request::method() == 'post') {
            $post = $this->input->post();
            $date = $post['date_selected'];

            if (!$date) {
                $date = date("Y-m-d");
            }
            $customerModel->saveData(array("id" => $userID, 'birthday' => $date));
            $message = 'Ви благодариме. Успешно го внесовте вашиот роденден. ';
            if ($customerModel->isInBirthdayLog($userID)) {
                $message = 'Почитувани,<br> Вие сте веќе наградени со 50 поена (50 ден.) за вашиот роденден.<br> Доколку сте го погрешиле датумот, истиот ќе имате прилика да го промените наредната година.<br> Со почит,<br> Kupinapopust.mk';
            }
            $this->template->content->success = $message;
            $this->template->content->displayForm = false;
        }
        $this->template->content->user = $user[0];
    }
	
	public function personaldata_android() {
		$this->personaldata("android");
	}  
	
	//$typemobile opisuva dali se koristi za android ili za web prikaz
	public function login($retQueryStr = "", $typemobile = "") {
        $userID = $this->session->get("user_id");
        $userType = $this->session->get("user_type");

		if ($userType == 'customer' and $userID) 
		{
			if($typemobile == "android")
			{
				//url::redirect("/customer/menu_android");
			}
			else
				url::redirect("/customer");
		}

        $this->template->title = kohana::lang("prevod.website_title");
		$this->template->content = new View("customer/login".($typemobile == "android" ? "_android" : ""));

        $type = $this->input->get("type", 0);
        $this->template->content->type = $type;

        if (request::method() == 'post') {
            $post = $this->input->post();
            $username = $post['Email'];
            $password = $post['password'];

            $adminModel = new Customer_Model();
            
            // ova e vistinskiot kod
            $where = array('email' => $username, 'password' => md5($password));


            $customers = $adminModel->getData($where);
            
            if (count($customers) > 0) 
            {

            	//ako profilot e AKTIVEN
            	if($customers[0]->active == 1)
            	{

					$this->session = Session::instance();
	                $this->session->set("user_id", $customers[0]->id);
	                $this->session->set("user_type", 'customer');
	                $this->session->set("user_email", $username);
	                $this->session->set("login_type", $customers[0]->login_type);
	                $this->session->set("login_value", $customers[0]->login_value);
					$this->session->set("current_login_type", "default");

					//setiraj ja variablata (last_login) za soodvetniot customer
					$adminModel->setLastLogin($customers[0]->id);

					//setiraj trackiranje na email vo cookie
					trackvisitor::setEmailTracking($username);

					//zacuvaj vo baza, vo tabelata newsletter => last_visited_website
					$newsletterModel = new Newsletter_Model();
					$newsletterModel->setLastVisitedWebsite($username);

					
                    //vidi dali treba da mu dodelis poeni deka povtorno stanal aktiven
                    $flagGiftPointsWelcome = $this->dodeliPoeniWelcomeBack($customers[0]->id);


					//zemi od sesija
					$retQueryStrSession = $this->session->get("retQueryStrSession");
					
					if($retQueryStrSession == "")
					{
						if($typemobile == "android")
						{
							$jsHandlerOutput = '
									<script type="text/javascript">
									window.MyHandlerLogin.ClientLoggedIn();
									</script>
									';
							die($jsHandlerOutput);
						}
						else
							url::redirect("customer/?type=1&flagGiftPointsWelcome=".$flagGiftPointsWelcome);
						

						//url::redirect("customer".($typemobile == "android" ? "/menu_android" : "")."/?type=1");
					}
					else
					{
						if($retQueryStrSession == "shop_cart")
						{
							url::redirect("index/shop_cart/1/0");
						}
						else
						{	
							$retQueryStrArray = explode("_", $retQueryStrSession);
							
							url::redirect("buy/".$retQueryStrArray[0]."/".$retQueryStrArray[1]."/".$retQueryStrArray[2]."/1/0");
						}
					}

            	}
            	else //ako prifilot e DEAKTIVIRAN
            	{
            		$this->template->content->error = 'Профилот е деактивиран! Ве молиме контактирајте нè доколку сметате дека е настаната грешка.';		
            	}
				
            } else 
            {
                $this->template->content->error = 'Комбинација на email адреса и лозинка која ја внесовте не е валидна.';
            }
        }
		else  //ako ne e post
		{
			$this->session = Session::instance();
			$this->session->set("retQueryStrSession", $retQueryStr);
		}
    }


    private function dodeliPoeniWelcomeBack($customer_id = 0)
    {
    	$customer_id = (int) $customer_id;

        //ako treba da mu se dodelat poeni
        $flagGiftPointsWelcome = "0";

        //kreiraj potrebni objekti
        $newsletterModel = new Newsletter_Model();

        //izvadi go zapisot od newsletter tabelata vo baza za soodvetniot customer
        $registeredNewsletterUser = $newsletterModel->getData(array("customer_id" => $customer_id));

        //AKO POSTOI ZAPIS VO NEWSLETTER TABELATA I AKO TREBA DA MU DODELIS POENI
        if(isset($registeredNewsletterUser[0]->welcome_again_status) && in_array($registeredNewsletterUser[0]->welcome_again_status, array(1, 3)))
        {
        	//kreiraj doponitelni objekti
        	$pointsModel = new Points_Model();

        	//dodeli mu poeni
        	$pointsModel->savePoints($customer_id, 50, "- Поени додадени од Kupinapopust.mk за повторно добредојде");

        	//inkrementiraj go welcome_again_status
			$where = array('id' => $registeredNewsletterUser[0]->id, 'welcome_again_status' => ($registeredNewsletterUser[0]->welcome_again_status + 1));
            $newsletterModel->saveData($where);

        	//setiraj go flagot za da moze da se izvesti
        	$flagGiftPointsWelcome = "1";
        }

    	return $flagGiftPointsWelcome;
    }

    
	public function login_android() {
		$this->login("", "android");
	}  
	
	public function registracija($typemobile = "") {
		$userID = $this->session->get("user_id");
		$userType = $this->session->get("user_type");
		
		if ($userType == 'customer' and $userID) {
			
			if($typemobile == "android")
			{
				//url::redirect("/customer/index_android");
			}
			else
				url::redirect("/customer");
		}
		
		$this->template->title = kohana::lang("prevod.website_title");
		$this->template->content = new View("customer/registracija".($typemobile == "android" ? "_android" : ""));
		$this->template->content->displayForm = true;
		
		$type = $this->input->get("type", 0);
		$this->template->content->type = $type;
		$this->template->content->post = array();
		
		if (request::method() == 'post') {
			$post = $this->input->post();
			$email = $post['Email'];
			$password = $post['password'];
			$repeat_password = $post['repeat_password'];
			$date_selected = $post['date_selected'];
			if (!$date_selected) {
				$date_selected = date("Y-m-d");
			}
			$opsti_odredbi_soglasnost = isset($post['opsti_odredbi_soglasnost']) ? $post['opsti_odredbi_soglasnost'] : "";
			
			//proverka dali se dobri POST vrednostite
			$this->template->content->post = $post;
			if($opsti_odredbi_soglasnost == "")
			{
				$this->template->content->error =   'Почитувани, морате да се согласите со Општите одредби на веб страната';
				$this->template->content->displayForm = true;
				
				//izlezi
				return 0;
			}	
			
			if($email == "")
			{
				$this->template->content->error =   'Ве молиме внесете Email адреса';
				$this->template->content->displayForm = true;
				
				//izlezi
				return 0;
			}	
			
			
			if($password == "")
			{
				$this->template->content->error =   'Ве молиме внесете лозинка';
				$this->template->content->displayForm = true;
				
				//izlezi
				return 0;
			}	
			
			if($password != $repeat_password)
			{
				$this->template->content->error =   'Полињата "Вашата лозинка" и "Повторете лозинка" мора да бидат исти';
				$this->template->content->displayForm = true;
				
				//izlezi
				return 0;
			}	
			
			//proveri dali postoi korisnikot
			$customerModel = new Customer_Model();
			$customerArr = $customerModel->checkCustomerExist($email);
			
			if(count($customerArr))//ako vekje postoi
			{
				if($customerArr[0]->active)
				{
					$this->template->content->success =   'Почитувани,<br/>
							Вие сте веќе регистрирани и имате свој "Кориснички профил".<br/> 
							Доколку сте ја заборавиле лозинката Ве молиме кликнете <a href="/customer/resetpassword">овде</a> за да креирате нова.<br/><br/>
							Со почит,<br/>
							Од тимот на "kupinapopust.mk"<br/>
							<img border="0" alt="Kupinapopust.mk" src="'.Kohana::config('facebook.facebookSiteProtocol').'://' . Kohana::config('facebook.facebookSiteURL') . '/pub/img/logo5.png">';

				}	
				else
				{
					$this->template->content->error =  'Почитувани, email адресата со која сакате да се регистрирате е веќе поврзана со профил што е деактивиран!<br/>Ве молиме контактирајте нè доколку сметате дека е настаната грешка.';
				}


				$this->template->content->displayForm = false;
			}
			else//ako ne postoi togas kreiraj
			{
				$customerID = $customerModel->createCustomer($email, true, $date_selected, $password);
				if($customerID)//ako e dobro kreiran
				{
					//setiraj trackiranje na email vo cookie
					trackvisitor::setEmailTracking($email);

					//kreiraj doponitelni objekti
		        	$pointsModel = new Points_Model();

					//dodeli mu poeni
        			$pointsModel->savePoints($customerID, 50, "- Поени додадени од Kupinapopust.mk за добредојде");

					//logiraj go userot
					$this->session = Session::instance();
					$this->session->set("user_id", $customerID);
					$this->session->set("user_type", 'customer');
					$this->session->set("user_email", $email);
					$this->session->set("login_type", "default");
					$this->session->set("login_value", "");
					$this->session->set("current_login_type", "default");
					
					//prikazi poraka za uspesno registriranje
					$this->template->content->success =    'Почитувани,<br/>							
							Успешно се регистриравте на една од најдобрите веб страни за групни попусти.<br/>
							Исто така ви доделивме <strong>50 поени како подарок за добредојде</strong> кои може да ги искористите при вашето купување.<br/>
							Ви посакуваме многу да заштедите.<br/><br/>
							Со почит,<br/>
							Kupinapopust.mk<br/>
							<img border="0" alt="Kupinapopust.mk" src="'.Kohana::config('facebook.facebookSiteProtocol').'://' . Kohana::config('facebook.facebookSiteURL') . '/pub/img/logo5.png"><br/><br/>
															';
															/*
															<form  name="okForm" id="okForm" method="get" action="/customer">
															<input type="submit" value="OK" class="submit" >
															</form> ';
															*/
					
					$this->template->content->displayForm = false;
				}
				else//dokolku e nastanata nekoja greska
				{
					$this->template->content->success =  'Почитувани,<br/>
							Настаната е грешка при креирање на Вашиот "kupinapopust" кориснички профил.<br/> 
							Ве молиме кликнете <a href="/customer/registracija">овде</a> за да се обидете повторно.<br/><br/>
							Со почит,<br/>
							Од тимот на "kupinapopust.mk"<br/>
							<img border="0" alt="Kupinapopust.mk" src="'.Kohana::config('facebook.facebookSiteProtocol').'://' . Kohana::config('facebook.facebookSiteURL') . '/pub/img/logo5.png">';
					$this->template->content->displayForm = false;
				}
			}
		}
	}
        public function registracija_android() {
                    $this->registracija("android");
                }  
                
    public function unsubscribe($hash) {
//       $this->auto_render = FALSE;
        $newsletterModel = new Newsletter_Model();
        $result = $newsletterModel->unsubscribe($hash);

        if ($result) {
            url::redirect("/?message=success");
        }


        $where = array("email" => $post['mail']);
        $row = $newsletterModel->getData($where);
        if (!$row) {
			$newsletterModel->saveSubscription($where);
            echo json_encode(array("resp" => "success"));
            exit;
        } else {
            echo json_encode(array("resp" => "duplicate"));
            exit;
        }
    }

	public function changepassword($typemobile = "") {
        $userID = $this->session->get("user_id");
        $userType = $this->session->get("user_type");

        if (!($userType == 'customer' and $userID)) {
           
			
			if($typemobile == "android")
				url::redirect("/customer/login_android");
			else
				url::redirect("/customer/login");
        }

        $this->template->title = kohana::lang("prevod.website_title");
        $this->template->content = new View("customer/changepassword".($typemobile == "android" ? "_android" : ""));

        if (request::method() == 'post') {
            $post = $this->input->post();

            $password = $post['password'];
            $password1 = $post['password1'];
            $password2 = $post['password2'];

            $customerModel = new Customer_Model();

            $where = array('id' => $userID, 'password' => md5($password));
            $customers = $customerModel->getData($where);

            if (count($customers) == 1 and ($password1 == $password2)) {

                $where = array('id' => $userID, 'password' => md5($password1));
                $customerModel->saveData($where);
                $this->template->content->success = 'Лозинката е успешно сменета.';
            } else {
                if (count($customers) == 0) {
                    $this->template->content->error = 'Ја згрешивте старата лозинка.';
                }
                if ($password1 != $password2) {
                    $this->template->content->error = 'Лозинките не се совпаѓаат.';
                }
            }
        }
    }
	
	public function changepassword_android() {
		$this->changepassword("android");
	} 


	public function deactivate_profile() 
	{
        $userID = $this->session->get("user_id");
        $user_email = $this->session->get("user_email");

        $this->template->title = kohana::lang("prevod.website_title");
        $this->template->content = new View("customer/deactivate_profile");

        //ako e pritisnato kopceto SUBMIT na formata
        if (request::method() == 'post') 
        {
            $post = $this->input->post();

            $customerModel = new Customer_Model();
            $newsletterModel = new Newsletter_Model();

            //Brisenje na zapis od tabelata newsletter
            $checkFlag = $newsletterModel->delete_by_email($user_email);


            //Ako brisenjeto na zapis od tabelata newsletter e USPESNO
            if($checkFlag)
            {

            	//Setiranje na poleto customer.active = 0
            	$where = array('id' => $userID, 'active' => 0);
            	$customerModel->saveData($where);

            	//isprati soodveten email do korisnikot deka account-ot mu e deaktiviran
				$mailContentModel = new Emailrenderer_Model();
				$maildata = array();
				$mailContent = $mailContentModel->render("customer_deactivation", $maildata);
				$email_config = Kohana::config('email');
				$from = array($email_config['from']['email'], $email_config['from']['name']);
				email::send($user_email, $from, "Kupinapopust.mk деактивација на кориснички профил!", $mailContent, true);
            			


            	//odlogiraj i vrati na strana a logiranje so info deka profilot e deaktiviran
            	url::redirect("/customer/logout/".base64_encode("/customer/login?type=3"));

            }

        }
    }


    public function resetpassword($typemobile = "") 
    {
        $userID = $this->session->get("user_id");
        $userType = $this->session->get("user_type");
        
        if ($userType == 'customer' and $userID) {
            if($typemobile == "android")
			{
				//url::redirect("/customer/index_android");
			}
			else
				url::redirect("/customer");
        }
        
        $this->template->title = kohana::lang("prevod.website_title");
        $this->template->content = new View("customer/resetpassword".($typemobile == "android" ? "_android" : ""));

        if (request::method() == 'post') {
            $post = $this->input->post();
            $username = $post['Email'];

            $adminModel = new Customer_Model();
            $where = array('email' => $username);
            $customers = $adminModel->getData($where);
            if (count($customers) > 0) 
            {
            	//ako e aktiven
            	if($customers[0]->active)
            	{

	                $password = $customers[0]->password;
	                $id = $customers[0]->id;
	                $salt = "kp.mk";

	                $hash = md5($id . $salt . $password);

	                $mailContentModel = new Emailrenderer_Model();

	                $maildata = array(
	                    'hash' => $hash,
	                    'username' => $username
	                );

	                $mailContent = $mailContentModel->render("passwordreset", $maildata);
					$email_config = Kohana::config('email');
					$from = array($email_config['from']['email'], $email_config['from']['name']);
	                email::send($username, $from, "Промена на лозинка", $mailContent, true);


	                $this->template->content->success = 'Проверете ја вашата e-mail адреса каде има инструкции како да ја смените вашата лозинка.';


            	}
            	else //ako ne e aktiven
            	{
            		$this->template->content->error = 'Профилот е деактивиран! Ве молиме контактирајте нè доколку сметате дека е настаната грешка.';
            	}

            } 
            else 
            {
                $this->template->content->error = 'Внесената е-mail адреса не постои во нашиот систем.';
            }
        }
    }
    
    public function resetpassword_android(){
        $this->resetpassword("android");
    }

    public function choosepassword($username, $hash) {
        $this->template->title = kohana::lang("prevod.website_title");
        $this->template->content = new View("customer/choosepassword");
        $this->template->content->displayForm = true;

        $adminModel = new Customer_Model();
        $where = array('email' => $username);
        $customers = $adminModel->getData($where);
        
        if (count($customers) > 0) {
            $password = $customers[0]->password;
            $id = $customers[0]->id;
            $salt = "kp.mk";

            $buildHash = md5($id . $salt . $password);
        }

        if ($buildHash != $hash) 
        {
            $this->template->content->error = 'Линкот за промена на лозинка веќе не е активен. Побарајте нов на <a href="/customer/resetpassword">страната за заборавена лозинка</a>';
            $this->template->content->displayForm = false;
        } 
        else 
        {
            if (request::method() == 'post') 
            {
                $post = $this->input->post();
                $password1 = $post['password1'];
                $password2 = $post['password2'];
				
				if ($password1 != "")
				{
					if ($password1 == $password2) 
					{
						//snimi ja novata lozinka vo baza
						$data = array('id' => $id, "password" => md5($password1));
						$customers = $adminModel->saveData($data);

						//logiraj go userot
						$this->session = Session::instance();
						$this->session->set("user_id", $id);
						$this->session->set("user_type", 'customer');
						$this->session->set("user_email", $username);
						$this->session->set("login_type", "default");
						$this->session->set("login_value", "");
						$this->session->set("current_login_type", "default");

						//prefrli go na index stranata od customer sekcijata
						url::redirect("customer/?type=1");

						// $this->template->content->success = 'Успешно ја сменивте вашата лозинка!<br/>Истовремено успешно сте логирани на вашиот профил.</a>';
						// $this->template->content->displayForm = false;

					} else {
						$this->template->content->error = 'Внесените лозинки не се совпаѓаат';
					}
				} else {
					$this->template->content->error = 'Полињата за лозинки не смеат да бидат празни';
				}
            }
        }
    }

	public function points($typemobile = "") {
        $this->template->title = kohana::lang("prevod.website_title");
		$this->template->content = new View("customer/points".($typemobile == "android" ? "_android" : ""));

        $userID = $this->session->get("user_id");
        $customerModel = new Customer_Model();
        $pointsModel = new Points_Model();

        $points = $pointsModel->getCustomerPoints($userID);
        $records = $pointsModel->getHistoryRecords($userID);
		
		//zemi gi site poeni na customer-ot pogolemi od 0 a grupirani po den
		$recordsPoints = $pointsModel->getCustomerPointsGrouped($userID);

        $this->template->content->records = $records;
        $this->template->content->recordsPoints = $recordsPoints;
        $this->template->content->points = $points;
        $this->template->content->user = $customerModel->getData(array("id" => $userID));
        
        
        $staticPagesModel = new Static_Model();
        $where = array('identifier'=>"kakodopoeni");
        $pageData = $staticPagesModel->getData($where);

        $propertyTitle = 'title_'.$this->lang;
        $propertyContent = 'content_'.$this->lang;
        $this->template->content->pageTitle = $pageData[0]->$propertyTitle;
        $this->template->content->pageContent = $pageData[0]->$propertyContent;
        
    }
	
	public function points_android() {
		$this->points("android");
	}  

    public function printvoucher($voucher_id = null) {
        if ($voucher_id) {
            $vouchersModel = new Vouchers_Model();
            $voucherData = $vouchersModel->getData(array('id' => $voucher_id));
            $userID = $this->session->get("user_id");
            if ($voucherData[0]->customer_id != $userID) {
                url::redirect("/customer");
            }
			
			if ($voucherData[0]->activated == 0) {
				url::redirect("/customer");
			}
			//die(print_r($voucherData));
			$vouchersModel->CreatePDF($voucherData[0]->attempt_id, "D") ;
			
			//star kod za obicno printanje
            //$mailContent = $vouchersModel->renderVoucher($voucher_id);
            //print $mailContent;
			
            exit;
        } else {
            url::redirect("/customer");
        }
    }
	
	public function fblogin($final_url_redirect_type = "regular") {

		// Kohana::log("error", "facebook== test 1");
		
		$post = $this->input->post();

	
		//check our post variable from index.php, just to insure user isn't accessing this page directly.
		if(isset($post["connect"]) && $post["connect"]==1) 
		{
			$appId = Kohana::config('facebook.facebookAppID');
			$appSecret = Kohana::config('facebook.facebookAppSecret');
			
			$facebook = new Facebook(array(
						'appId' => $appId,
						'secret' => $appSecret,
						));
			
			$fbuser = $facebook->getUser();

			// Kohana::log("error", "facebook== ".$fbuser);
			
			if ($fbuser) {
				try {
					// Proceed knowing you have a logged in user who's authenticated.
					$user_profile = $facebook->api('/'.$fbuser, array('fields' => 'id,name,email')); //user
					//$uid = $facebook->getUser();
				}
				catch (FacebookApiException $e) 
				{
					// Kohana::log("error", "facebook==". $e);

					echo error_log($e);
					$fbuser = null;
				}
			}
			
			// redirect user to facebook login page if empty data or fresh login requires
			if (!$fbuser){
				$loginUrl = $facebook->getLoginUrl(array('redirect_uri'=>Kohana::config('facebook.facebookSiteUrlLogin'), false), 'v2.11');
				header('Location: '.$loginUrl);
			}
			
			// die(print_r($user_profile));

			if (isset($user_profile)) {
				$loginID = $user_profile['id'];
				
				
				$loginType = "facebook";

				$customerModel = new Customer_Model();
				$where = array('login_value' => $loginID);
				$customers = $customerModel->getData($where);

				$flagGiftPointsWelcome = '0';
				
				if (count($customers) == 0) // ako ne postoi takov korisnik
				{
					$userEmail = $user_profile['email'];
					$userBirthday = isset($user_profile['birthday']) ? $user_profile['birthday'] : "";
					
					$userData = array(
						'login_value' => $loginID,
						'login_type' => $loginType,
						'birthday' => $userBirthday,
						'email' => $userEmail
						);

					$newID = $customerModel->createFacebookCustomer($userData);
					$where = array('id' => $newID);
					$customers = $customerModel->getData($where);

					//kreiraj doponitelni objekti
		        	$pointsModel = new Points_Model();

					//dodeli mu poeni
        			$pointsModel->savePoints($newID, 50, "- Поени додадени од Kupinapopust.mk за добредојде");
        			$flagGiftPointsWelcome = '2';
				}
				else //ako vekje postoi takov korisnik
				{
					//ako profilot e DEAKTIVIRAN
					if($customers[0]->active == 0)
					{
						// url::redirect("/customer/login?type=4");
						echo "/customer/login?type=4";
						exit;
					}

					//setiraj ja variablata (last_login) za soodvetniot customer
					$customerModel->setLastLogin($customers[0]->id);

					//zacuvaj vo baza last_visited_website
					$newsletterModel = new Newsletter_Model();
					$newsletterModel->setLastVisitedWebsite($customers[0]->email);

					//vidi dali treba da mu dodelis poeni deka povtorno stanal aktiven
                    $flagGiftPointsWelcome = $this->dodeliPoeniWelcomeBack($customers[0]->id);
				}

				//setiraj trackiranje na email vo cookie
				trackvisitor::setEmailTracking($customers[0]->email);


				$this->session = Session::instance();
				$this->session->set("user_id", $customers[0]->id);
				$this->session->set("user_type", 'customer');
				$this->session->set("user_email", $customers[0]->email);
				$this->session->set("login_type", $customers[0]->login_type);
				$this->session->set("login_value", $customers[0]->login_value);
				$this->session->set("current_login_type", "facebook");
				//url::redirect("customer/?type=1");
				
				//zemi od sesija
				$retQueryStrSession = $this->session->get("retQueryStrSession");
				
				if($retQueryStrSession == "")
				{
						echo "/customer/?type=1&flagGiftPointsWelcome=".$flagGiftPointsWelcome;
				}
				else
				{
					
					if($retQueryStrSession == "shop_cart")
					{
						echo "/index/shop_cart/1/0";
					}
					else
					{	
						$retQueryStrArray = explode("_", $retQueryStrSession);
						
						echo "/buy/".$retQueryStrArray[0]."/".$retQueryStrArray[1]."/".$retQueryStrArray[2]."/1/0";
					}
				}
				
			}
			
			
			
			exit;
			$this->template->title = kohana::lang("prevod.website_title");
			$this->template->content = new View("customer/fblogin");
			
			
		}
		else
			url::redirect("/customer");
	}
	
	
	public function fblogin_mobile($type = "android") {
		
		// Kohana::log("error", "cekor 1 ");
		$post = $this->input->post();
		// Kohana::log("error", "cekor 2 - ". $post['id']);
		
		$loginID = $post['id'];
		$userEmail = $post['email'];
		$userBirthday = isset($post['birthday']) ? $post['birthday'] : "";
		
		$loginType = "facebook";
		$userData = array(
				'login_value' => $loginID,
				'login_type' => $loginType,
				'birthday' => $userBirthday,
				'email' => $userEmail
				);
		$customerModel = new Customer_Model();
		$where = array('login_value' => $loginID);
		$customers = $customerModel->getData($where);
		if (count($customers) == 0) {
			$newID = $customerModel->createFacebookCustomer($userData);
			$where = array('id' => $newID);
			$customers = $customerModel->getData($where);
		}
		
		$this->session = Session::instance();
		$this->session->set("user_id", $customers[0]->id);
		$this->session->set("user_type", 'customer');
		$this->session->set("user_email", $customers[0]->email);
		$this->session->set("login_type", $customers[0]->login_type);
		$this->session->set("login_value", $customers[0]->login_value);
		$this->session->set("current_login_type", "facebook");
		//url::redirect("customer/?type=1");
		
		//zemi od sesija
		$retQueryStrSession = $this->session->get("retQueryStrSession");
		
		// Kohana::log("error", "cekor 3 ");	
		if($retQueryStrSession == "") //ako ne doaga od nekoja ponuda
		{
			// Kohana::log("error", "cekor 4 ");
			$jsHandlerOutput = '
					<script type="text/javascript">
					window.MyHandlerLogin.ClientLoggedIn();
					</script>
					';
			die($jsHandlerOutput);
		}
		else//ako doaga od ponuda pa posle logiranje treba da se vrati na ponudata.
		{
			$retQueryStrArray = explode("_", $retQueryStrSession);
			echo "/buy/".$retQueryStrArray[0]."/".$retQueryStrArray[1]."/1";
		}
		// Kohana::log("error", "cekor 5 ");			
			

			
		exit;
		$this->template->title = kohana::lang("prevod.website_title");
		$this->template->content = new View("customer/fblogin");

	}
	
	
	public function menu_android() {
		$this->template->title = kohana::lang("prevod.website_title");
		$this->template->content = new View("customer/menu_android");
		
		$type = $this->input->get("type", 0);
		$this->template->content->type = $type;
		
	}

	public function listcodes_android($attempt_id, $deal_id) {
		$this->template->title = kohana::lang("prevod.website_title");
		$this->template->content = new View("customer/listcodes_android");
		
		$userID = $this->session->get("user_id");
		
		$vouchersModel = new Vouchers_Model();
		$dealsModel = new Deals_Model();
		
		$myVouchers = $vouchersModel->getVouchersByAttemptID($userID, $attempt_id);
		$dealData = $dealsModel->getData(array("id" => $deal_id));
		//die(print_r($myVouchers));
		$this->template->content->deal_id = $deal_id;
		$this->template->content->vouchers = $myVouchers;
		$this->template->content->dealData = $dealData;
	}
        
        public function qrcode_android($voucherCode, $deal_id) {
		$this->template->title = kohana::lang("prevod.website_title");
		$this->template->content = new View("customer/qrcode_android");
		
		$userID = $this->session->get("user_id");
                $userEMAIL = $this->session->get("user_email");
                
		$dealsModel = new Deals_Model();
		
		$dealData = $dealsModel->getData(array("id" => $deal_id));
                
		//die(print_r($myVouchers));
		$this->template->content->userEmail = $userEMAIL;
		$this->template->content->voucherCode = $voucherCode;
		$this->template->content->dealData = $dealData;
	}

	public function logout($nextPage = null, $typemobile = "") {
		
		
		if($typemobile == "android")
		{
			$session = Session::instance();
			$session->destroy();
			
			$jsHandlerOutput = '
					<script type="text/javascript">
					window.MyHandlerLogged.ClientLogout();
					</script>
					';
			die($jsHandlerOutput);
		}
		else
		{
			
			parent::logout($nextPage);
		}
	}
	
	public function logout_android() {
		$this->logout(null ,"android");
	} 


	public function qrCodeGenerator($code) {
 		require_once APPPATH . 'phpqrcode/qrlib.php';
     	
     	$link = Kohana::config('facebook.facebookSiteProtocol').'://' . Kohana::config('facebook.facebookSiteURL')."/validate/voucher/".$code;


	    // outputs image directly into browser, as PNG stream 
	    QRcode::png($link);
	}  

}