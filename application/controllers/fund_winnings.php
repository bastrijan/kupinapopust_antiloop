<?php

defined('SYSPATH') OR die('No direct access allowed.') ;

class Fund_Winnings_Controller extends Default_Controller {

  public function __construct() {
    $this->template = 'layouts/admin' ;

      //dokolku ne e superadmin togas ne mu ja prikazuvaj stranata tuku prenasoci go na index strana od admin sekcijata
        $this->session = Session::instance();
        $userID = $this->session->get("user_id");
        if ($userID != '1')//the id for superadmin
                url::redirect("/admin");
                
    parent::__construct() ;
  }

   public function index() {
		
		//template
		$this->template->title = "Фонд на добивки" ;
		$this->template->content = new View("/admin/fund_winnings") ;
		
		
                
                $WfPrizeFundModel = new Wf_Prize_Fund_Model();
                $order = 'id';
                $dataofWfPrizeFundModel = $WfPrizeFundModel->getPrizeFundOrderBy($order);
		
		$this->template->content->dataofWfPrizeFundModel = $dataofWfPrizeFundModel;
	}
        
        
        public function add() {
                //instance Model
                $WfPrizeFundModel = new Wf_Prize_Fund_Model();
		//template
		$this->template->title = "Фонд на добивки" ;
		$this->template->content = new View("/admin/uredi_fund_winnings") ;
		
                $this->template->content->action = 'Додади';
                
                if (request::method() == 'post') {
			//should save then
			$post = $this->input->post();
			
			$WfPrizeFundModel->saveData($post) ;
			url::redirect("/fund_winnings") ;
		}
                
	}
        
         public function edit() {
             
                $article_id = $this->input->get("id", 0);
                //instance Model
		$WfPrizeFundModel = new Wf_Prize_Fund_Model();
                
                if(isset($article_id) && !empty($article_id)){
                    
                    $get_article_action = array();
                
                    //template
                    $this->template->title = "Фонд на добивки" ;
                    $this->template->content = new View("/admin/uredi_fund_winnings") ;
                    
                    $get_article_action = $WfPrizeFundModel->get_art_by_id($article_id);

                    $this->template->content->action = 'Промени';
                    $this->template->content->article = $get_article_action[0];
                     
                }
                
                if (request::method() == 'post') {
			//should save then
			$post = $this->input->post() ;
			
			$WfPrizeFundModel->saveData($post) ;
			url::redirect("/fund_winnings") ;
		}
                
                
	}
         public function remove() {
		
                 //instance Model
		$WfPrizeFundModel = new Wf_Prize_Fund_Model();
             
		//nema output
		$this->auto_render = false;
                $article_id = $this->input->get("id", 0);
                
                if(isset($article_id) && !empty($article_id)){
                    $WfPrizeFundModel->delete($article_id);
                }
                
                url::redirect("/fund_winnings");
	}  
	
	
  public function __call($method, $arguments) {
    // Disable auto-rendering
    $this->auto_render = FALSE ;
    echo 'page not found' ;
  }



}