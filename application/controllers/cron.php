<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Cron_Controller extends Controller {

	public function __construct() {
		parent::__construct();
		
		//Kohana::log("error", "set_main_offers step 1: ");
		
		$get = $this->input->get();
		//die(print_r($get));
		
		//Kohana::log("error", "set_main_offers step 2: ".implode(",",$get));
		
		if (!(isset($get['password']) and $get['password'] == 'qqqqqq10cronsend')) {
			exit;
		}
		
		$this->auto_render = FALSE;
	}
	
	//NE SE KORISTI//
	//CRONJOB NA SEKOJA MINUTA//
    public function sendmessages() {
        // $messageQueue = new Mailqueue_Model();
        // $messageQueue->sendMails();
    }

    public function customnewsletter() {
        $mailContentModel = new Emailrenderer_Model();
        $newsletterModel = new Newsletter_Model();

        $maildata = array(
            'content' => "",
        );

        $mailContent = $mailContentModel->render("newsletter_custom", $maildata);
		
		$email_config = Kohana::config('email');
		$from = array($email_config['from']['email'], $email_config['from']['name']);
        //email::send("gvelkov@gmail.com", $from, 'Ви посакуваме Среќни и Весели празници!', $mailContent, true) ;

        $emailSubscribers = $newsletterModel->getData();
		
	
        foreach ($emailSubscribers as $subscriber) {

            email::send($subscriber->email, $from, 'Ви посакуваме Среќни и Весели празници!', str_replace("hash_string_replace", md5("kupi_" . $subscriber->email), $mailContent), true);
        }
    }

    /**
     * Point for birthday script - run once a day
     */
	//CRONJOB - Se aktivira sekoj den vo 00:45h //
    public function birthdaypoints() {
        $customerModel = new Customer_Model();
        $pointsModel = new Points_Model();

        $email_config = Kohana::config('email');
        $from = array($email_config['from']['email'], $email_config['from']['name']);

//        $today = date("m-d");
        $customers = $customerModel->getCustumersBirthdaysToday();
//        var_dump($today);exit;
        if ($customers) {
            foreach ($customers as $customer) {
                $emails[] = $customer->email;
                $pointsModel->savePoints($customer->id, 50, "- Поени додадени од Kupinapopust.mk како подарок за роденден", true);

            }

            $mailContentModel = new Emailrenderer_Model();
			
			$email_config = Kohana::config('email');
			
            foreach ($emails as $email) {
                $maildata = array(
                    'email' => $email,
                );

                $mailContent = $mailContentModel->render("birthday_points", $maildata);

                email::send($email, $from, 'Подарок „kupinapopust“ поени', str_replace("hash_string_replace", md5("kupi_". $customer->email), $mailContent), true);

            }
            print count($emails). " greets sent!";
        } else {
            print "no birthday today!";
        }
    }
    
    /**
     * Expiration points - run once a day
     */
	
	//CRONJOB - Se aktivira sekoj den vo 3:45h //
    public function pointsreminder() {
        $customerModel = new Customer_Model();
        $pointsModel = new Points_Model();

        $email_config = Kohana::config('email');
        $from = array($email_config['from']['email'], $email_config['from']['name']);

        $expiringPoints = $pointsModel->getExpiringPoints();
        $customers = array();
        foreach ($expiringPoints as $points) {
            $customers[$points->customer_id] = $customerModel->getCustomerMail($points->customer_id);
        }
        
        if ($customers) {
            $mailContentModel = new Emailrenderer_Model();
			
            foreach ($customers as $id =>$email) {
                $maildata = array(
                    'email' => $email,
                );

                $mailContent = $mailContentModel->render("points_reminder", $maildata);


                email::send($email, $from, 'Искористете ги вашите „kupinapopust“ поени', str_replace("hash_string_replace", md5("kupi_". $email), $mailContent), true);
            }
            print count($customers). " greets sent!";
        } else {
            print "no points expire today!";
        }
    }
	
	//koga e zavrsena nekoja ponuda se prakja mail do partnerot so aktivnite voucher-i
	//vo sekoj ciklus se obrabutuva samo po edna zavrsena ponuda za da bide poefikasno prakjanjeto.
	
	//CRONJOB NA sekoja 10ta minuta i sekoja 40ta minuta od casot ()//
	public function ponudazavrsena() 
	{
			
		$voucherModel = new Vouchers_Model();
		$dealsModel = new Deals_Model();
		$partnersModel = new Partners_Model() ;
		$mailContentModel = new Emailrenderer_Model();
		
		//zemi gi site ponudi koi zavrsile a za koi treba da se isprati mail do partnerot so aktivnite voucher-i
		//ke se izvrsi samo edna vo sekoe izvrsuvanje na cronjob-ot (zaradi efikasnost)
		//povekje ne treba
		
		//TODO: dali mozebi da se dodade uste eden uslov samo od poslednata godina da se zemat deals. Zasto vaka se zemaat i starite koi nemaa aktivni vauceri i odnovo i odnovo se proveruvaat.
		// Isto mozebi podolu da se zapise flag-ot prati_mail_do_partner na nula iako nema aktivni vauceri za da ne se pominuva pak istiot deal povtorno sledniot pat.


		//$dealData = $dealsModel->getData(array("prati_mail_do_partner"=>1, "end_time < "=> date('Y-m-d H:i:s')));
		$dealData = array();
		
		//svrti go array-ot
		foreach ($dealData as $deal) 
		{
			
			//zemi go email-ot na partnerot cija sto e ponudata
			$partnerData = $partnersModel->getData(array("id"=>$deal->partner_id)) ;
			
			//ako e vnesena informacija za mail na partnerot
			if($partnerData[0]->email != "")
			{
				//zemi gi site aktivni i plateni voucher-i za soodvetnata ponuda
				$vouchersData = $voucherModel->getZavrsenaPonudaAktivniPlateniVauceri($deal->id);
				
				//ako ima barem eden aktiven i platen voucher
				if(count($vouchersData) > 0)
				{
					//napravi soodvetna sodrzina na mail-ot sto ke go pratis
					$maildata = array(
							'deal_name' => $deal->title_mk,
							'vouchers' => $vouchersData
							);
					$mailContent = $mailContentModel->render("partner_aktivni_vouceri", $maildata);
					//die($mailContent);
					//isprati go mail-ot.
					$recipient_arr = array("to"=>$partnerData[0]->email,
										   "cc"=>"contact@kupinapopust.mk"
									 );

					//die(print_r($recipient_arr));			
					$email_config = Kohana::config('email');
					$from = array($email_config['from']['email'], $email_config['from']['name']);
					$result = email::send($recipient_arr, $from, 'Листа на ваучери за понудата: '.trim(strip_tags($deal->title_mk)), $mailContent, true); 
					
					//Ako e uspestno ispraten mail-ot, togas setiraj ja vrednosta (prati_mail_do_partner) = 0 za soodvetnata ponuda
					if($result == true)
					{
						$data_save["id"] = $deal->id;
						$data_save["prati_mail_do_partner"] = 0;
						$dealsModel->saveData($data_save) ;
						
						//izvrsi samo za edna
						break;
					}	
				}
			}
		}
	}


	//CRONJOB NA SEKOJ POLN SAAT//
	public function set_main_offers() {
		// Kohana::log("error", "set_main_offers , datum = ".date('Y-m-d H:i:s'));
		
		//kreiraj object od Deals Class-ata
		$dealsModel = new Deals_Model() ;
		
		//zemi go momentalniot datum, cas i minuti
		$curr_date_time = date('Y-m-d H:i:s');
		
		//povikaj ja soodvetnata f-ja
		$ret_val = $dealsModel->setMainOffers($curr_date_time);
		
		//ako se pomina kako sto treba => kreiraj go banner-ot so novite glavni ponudi
		if($ret_val)
		{
			print "MAIN OFFERS se setirani! ";

			$pass_create_file = "nlonlo321";
			// require_once Kohana::config('config.banner_generator_filepath');
		}
		
	}
	

    public function __call($method, $arguments) {
        // Disable auto-rendering
        $this->auto_render = FALSE;

        echo Router::$current_uri;
    }

}