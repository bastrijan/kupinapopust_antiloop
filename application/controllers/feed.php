<?php defined('SYSPATH') OR die('No direct access allowed.');

class SimpleXMLElementExtended extends SimpleXMLElement {
  public function addCData($cdata_text) {
    $node = dom_import_simplexml($this); 
    $no   = $node->ownerDocument; 
    $node->appendChild($no->createCDATASection($cdata_text)); 
  } 
}

class Feed_Controller extends Default_Controller {
	
    
    public function __construct()
	{
		$this->auto_render = FALSE;
		
		parent::__construct();
	}

	public function sitepopusti()
	{	
		//kreiraj modeli		
		$dealsModel = new Deals_Model() ;
		$cityModel = new City_Model();
		$categoryModel = new Categories_Model();
		$subCategoriesModel = new Subcategories_Model();

		//inicijalizacija na promenlivi
		$xml_basic = '<?xml version="1.0" encoding="utf-8"?>
<xml>
<portal>
	<name>Kupinapopust.mk</name>
	<url>https://kupinapopust.mk</url>
</portal>
<offers>
</offers>
</xml>';
		
		//kreiraj array so potrebnite podatoci podatoci
		$activeOffers = $dealsModel->getSitePonudi();
		$cities = $cityModel->getCities();
		$categories = $categoryModel->getCategories();
		$subCategoriesData = $subCategoriesModel->getSubcategoriesbyParentid(0);

		//kreiraj object od osnovniot xml 
		$xml_obj = new SimpleXMLElementExtended($xml_basic);
		
		//svrti go array-ot so podatoci za deal-ot i gradi go 

		if ($activeOffers) {
			foreach ($activeOffers as $offer) 
			{
				// dodadi nov tag  <offer></offer>
				$item_obj = $xml_obj->offers->addChild('offer');


				//<image></image>
				$item_obj->addChild("image", Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/pub/deals/$offer->main_img");

				//<text></text>
				$element_name = "text";
				$item_obj->$element_name = NULL; // VERY IMPORTANT! We need a node where to append
				$item_obj->$element_name->addCData(trim(strip_tags($offer->title_mk_clean)));

				//<description></description>
				$element_name = "description";
				$item_obj->$element_name = NULL; // VERY IMPORTANT! We need a node where to append
				$item_obj->$element_name->addCData(trim(strip_tags($offer->title_mk)));


				//<city></city>
				$item_obj->addChild("city", "Скопје");

				//<value></value>
				$item_obj->addChild("value", $offer->price);

				// <price></price>
				$price = "";
				if ($offer->tip == 'cela_cena')
					$price = $offer->price_discount;

				$item_obj->addChild("price", $price);

				// <coupon_price></coupon_price>
				$coupon_price = "";
				if ($offer->tip == 'cena_na_vaucer')
					$coupon_price = $offer->price_voucher;

				$item_obj->addChild("coupon_price", $coupon_price);

				// <offer_price></offer_price>
				$offer_price = "";
				if ($offer->tip == 'cena_na_vaucer')
				{
					$cena_na_lice_mesto = $offer->price_discount - $offer->price_voucher;

					if($cena_na_lice_mesto > -1)
						$offer_price = $cena_na_lice_mesto;
				}

				$item_obj->addChild("offer_price", $offer_price);

				// <url></url>
				$item_obj->addChild("url", Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/deal/index/".seo::DealPermaLink($offer->deal_id, $offer->title_mk_clean, $offer->category_id, $offer->subcategory_id, $categories, $subCategoriesData));
				

				// <main></main>
				$item_obj->addChild("main", $this->mainOffer($offer->primary1, $offer->primary2, $offer->primary3, $offer->primary4, $offer->primary5));

				// <sold_count></sold_count>
				$item_obj->addChild("sold_count", $offer->voucher_count);

				// <sold_out></sold_out>
				$item_obj->addChild("sold_out", $this->isSoldOut($offer->voucher_count, $offer->max_ammount));

				// <category></category>
				$item_obj->addChild("category", $categories[$offer->category_id]);

				// <lat></lat>
				$item_obj->addChild("lat", "");

				// <lng></lng>
				$item_obj->addChild("lng", "");

			}
		}
		
		
		
		//isprintaj go xml-ot
		Header('Content-type: text/xml;  charset:UTF-8; ');
		echo $xml_obj->asXML();

	}

    private function isSoldOut($count = 0, $max) {

        $ret_value = 0;

        if ($max > 0 && $count >= $max)
            $ret_value = 1;


        return $ret_value;
    }

	private function mainOffer($primary1, $primary2, $primary3, $primary4, $primary5)
	{
		$ret_value = 0;
		
		//if($primary1 == 1 || $primary2 == 1 || $primary3 == 1 || $primary4 == 1 || $primary5 == 1)
		if($primary1 == 1)
			$ret_value = 1;
				
		return $ret_value;
	}
	
	private function stripStr($str_in)
	{
		$str_out = "";
		
		//za zamena na new line so zapirka
		$order   = array("\r","\n","\n\r", "\r\n", "'", '"', ",");
		$replace = array(" "," "," ", " ", "", "", "");
		
		$str_out = str_replace($order, $replace, trim(strip_tags($str_in)));
		
		return $str_out;
	}	
	
   
}