<?php defined('SYSPATH') OR die('No direct access allowed.');

class Card_Android_Controller extends Default_Controller {
    
     public function __construct() {
            $this->template = 'layouts/android' ;
            parent::__construct();
	}
    
    public function index($deal_id = null) {
        $this->template->content = new View('card/index_android');
        $this->template->title = kohana::lang("prevod.website_title");
    }
    public function activate($deal_id = null) {
        $this->template->content = new View('card/activate_android');
        $this->template->title = kohana::lang("prevod.website_title");
        $type = 1;
        
        if (request::method() == 'post') {
			//privremeno
			/*
			die("				<strong>
						Почитувани корисници,<br/>
						Се извинуваме за привремената неможност да го искористите подарениот кредит. Ве молиме обидете се повторно за 2 до 3 часа, со рефреширање на веб страната.<br/><br/>
						Ви благодаримe на разбирањето и трпеливоста.<br/><br/>
						
						
						<p>Со почит,<br>   
						'Kupinapopust.mk' тим
						</p>
						
						</strong>");
			*/
			
            $post = $this->input->post();
            $username = $post['Email'];
            $usernameGift = $post['Email-gift'];
            
            $code = $post['code'];
			
			if($username == "" || $code == "")
			{
				$this->template->content->error = 'Почитувани,<br>полињата Вашиот Е-mail и Единствен код мора да бидат внесени!';
			}
			else
			{
				
				$adminModel = new Points_Model();
				$where = array('email' => $username, 'code' => $code,'status'=>'active');
				$pointsData = $adminModel->getGiftCard($where);
				if (count($pointsData) > 0) {
					if ($usernameGift) {
						$username = $usernameGift;
						$type = 2;
					}
					$adminModel->activateGiftCard($username, $pointsData);
					url::redirect("/card_android/success/$type");
				} else {
					$this->template->content->error = 'Почитувани,<br>настаната е една од следните грешки:<br>- E-mail адресата што ја внесовте не соодејствува со кодот<br>- Кодот што го внесовте е веќе искористен<br>- Кодот што го внесовте е погрешен<br>';
				}
			}
        }
    }
    
    public function success($type = 1) {
		$this->template->content = new View('card/success_android') ;
        $this->template->title = kohana::lang("prevod.website_title") ;
        $this->template->content->type = $type ;
    }
    

    public function __call($method, $arguments) {
        $this->auto_render = FALSE;
        echo Router::$current_uri;
    }

}