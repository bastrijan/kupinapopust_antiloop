<?php
defined('SYSPATH') OR die('No direct access allowed.') ;

require_once DOCROOT.'vendor/autoload.php';

class Optimize_Image_Webservice_Controller
{
	public function __construct($api_key)
	{
		\Tinify\setKey($api_key);

	}

	public function compress($filename = "", $new_image_full_path = "")
	{
		$returnVal = false;


		////////////
		try 
		{
			//zemi kolku e momentalniot broj na compresii za da ze sporedi so limitot
			$compressionsThisMonth = \Tinify\compressionCount();

			// die("--".$compressionsThisMonth."///ddd");

			//ako ne e nadminat limitot 
			if($compressionsThisMonth < Kohana::config('settings.tiny_png')['limit'] && $filename !="" && $new_image_full_path !="")
			{
				//komresiraj slikata so web servisot
				$source = \Tinify\fromFile($filename);
				$source->toFile($new_image_full_path);

				$returnVal = true;
			}

		} catch(Exception $e) {
			$returnVal = false;
		    // die("The error message is: " . $e->getMessage());
		}
		////////////

		return $returnVal;
	}

}