<?php defined('SYSPATH') OR die('No direct access allowed.');

class Banner_Controller extends Default_Controller {
	
    public function __construct() {
        parent::__construct();
    }
 
    public function index()
	{	
		$this->auto_render = FALSE;

		//zemi gi variablite od GET-ot
		$banner_id = $this->input->get("banner_id", 0);
		$ref = $this->input->get("ref", 0);

		/*************dokolku banner_id i ref se validni => izvadi od baza info za bannerot********/
		if($banner_id && $ref)
		{
			$affiliateBannerModel = new Affiliatebanners_Model() ;
			$where = array('id' => $banner_id) ;
			$affiliateBannerData = $affiliateBannerModel->getData($where) ;

			$url = $affiliateBannerData[0]->banner_link.'?ref='.$ref;			
		}


		header("Location: " . $url);

    }
	
    public function __call($method, $arguments) {
        // Disable auto-rendering
        $this->auto_render = FALSE;
        echo 'page not found';
    }
}