<?php
defined('SYSPATH') OR die('No direct access allowed.') ;

class Admin_Deals_Controller extends Default_Controller 
{

	public function __construct() 
	{

		ini_set('max_execution_time', 600); //600 seconds = 10 minutes=

		$this->template = 'layouts/admin' ;

		parent::__construct() ;
	}

	public function clone_deal($existingDealID = 0) 
	{
		$this->auto_render = false ;

		//vo slucaj ako ne se prilozi integer vrednost za ID na ponuda
		if((int)$existingDealID == 0)
			url::redirect("/admin") ;

		$userID = $this->session->get("user_id");

		// die("--".$userID);

		//kreiranje na modeli
		$dealsModel = new Deals_Model() ;
		$dealTagsModel = new Deal_Tags_Model() ;
		$dealOptionsModel = new Deal_Options_Model() ;

		//insert na novata ponuda i zemi ID na novata ponuda
		$cloneDealID = $dealsModel->clonDeal($existingDealID, $userID);

		//ako e uspesno vnesen klonot
		if($cloneDealID > 0)
		{
			// tagovite
			$dealTagsModel->clonDealTags($existingDealID, $cloneDealID);

			//opciite
			$dealOptionsModel->clonDealOptions($existingDealID, $cloneDealID);

			//slikite da se kompresiraat
			$this->compressImages($cloneDealID);

			//Na kraj da se napravi redirekt na EDIT NA PONUDA
			url::redirect("/admin/autosave_deal/".$cloneDealID."/1") ;

		}//ako ne e uspesno vnesen
		else
			url::redirect("/admin") ;

	}

	protected function compressImages($deal_id = 0)
	{
		if((int)$deal_id == 0)
			return -1;

		//kreiranje na modeli
		$dealsModel = new Deals_Model() ;

		// izvadi info za ponudata	
		$where = array('id' => $deal_id) ;
		$dealData = $dealsModel->getData($where) ;

		//main_img
		if($dealData[0]->main_img != "")
		{
			$image_full_path = DOCROOT . 'pub/deals/'.$dealData[0]->main_img;	
			$this->exeCompressImage($image_full_path);
		}

		//side_img
		if($dealData[0]->side_img != "")
		{
			$image_full_path = DOCROOT . 'pub/deals/'.$dealData[0]->side_img;	
			$this->exeCompressImage($image_full_path);
		}


		//ostanati 5
		for ($i = 1; $i <= 5; $i++)
		{
			$additional_img_column_name = 'additional_img_'.$i;

			$image_full_path = DOCROOT . 'pub/deals/'.$dealData[0]->$additional_img_column_name;	
			$this->exeCompressImage($image_full_path);
		}
	}


	protected function exeCompressImage($image_full_path = "")
	{

		if($image_full_path != "" && is_file($image_full_path))
		{
			//zemi ja goleminata na fajlot
			$file_size = filesize($image_full_path);

			$file_size_reper = 204800; //vo bajti = 200 kB
			
			if($file_size > $file_size_reper)
			{
				//kreiraj objekt za optimizacija na slikata
				$optimizeImageWebServiceObject = new Optimize_Image_Webservice_Controller(Kohana::config('settings.tiny_png')['api_key']);

				$optimizeImageWebServiceObject->compress($image_full_path, $image_full_path);
			}
		}

	}

	public function __call($method, $arguments) 
	{
		// Disable auto-rendering
		$this->auto_render = FALSE ;
		echo 'page not found' ;
	}

}