<?php
defined('SYSPATH') OR die('No direct access allowed.') ;

class Pay_Casys_Controller extends Default_Controller {

  public function index() {
	
	//polnenje na POST variable-ata
	$post = $this->input->post() ;
	
	//dokolku ne doaga od POST ili pak ne postoi ID za ponudata => isfrli go na index strana
    if (request::method() != 'post' || (int)$post['discount_id'] == 0 || (int)$post['deal_option_id'] == 0) {
      url::redirect("/") ;
    }
	
    // if (strpos($_SERVER["HTTP_REFERER"], "/buy/deal") === false and strpos($_SERVER["HTTP_REFERER"], "/buy/gift") === false) {
    //   print "Access forbidden" ;
    //   //print $_SERVER["HTTP_REFERER"];
    //   exit ;
    // }
	
	//povikuvanje na sesiski variabli (vo slucaj da e logiran)
	$sessionUserID = $this->session->get("user_id");
	$sessionUserType = $this->session->get("user_type");
	
	//povikuvanje na potrebnite objekti
    $vouchersModel = new Vouchers_Model() ;
	$dealModel = new Deals_Model();
	$customerModel = new Customer_Model();
	$newsletterModel = new Newsletter_Model();
	
	//izvadi gi informaciite za ponudata (deal)
    $where_options_str = "d.id = ".$post['discount_id']." AND do.id = ".$post['deal_option_id'];
    $dealData = $dealModel->getDealOptionsData($where_options_str);
	
	//dokolku ne postoi taa ponudata - opcija => isfrli go na index strana
	if(count($dealData) == 0)
		url::redirect("/") ;

	//proveri dali e validna email adresa
	if (!filter_var($post["Email"], FILTER_VALIDATE_EMAIL))
	{
		if(isset($_SERVER["HTTP_REFERER"]))
			url::redirect($_SERVER["HTTP_REFERER"]."/0/3");
		else
			url::redirect("/buy/deal/".$post['discount_id']."/".$post['deal_option_id']."/0/3");
	}


	//zacuvaj vo baza last_visited_website
	$newsletterModel->setLastVisitedWebsite($post["Email"]);

	//setiraj trackiranje na email vo cookie
	trackvisitor::setEmailTracking($post["Email"]);


	/*****proveri dali ima dovolno slobodni vauuceri*****/
	//treba da se proveri dali brojot na kupeni vauceri od post["Email"] go nadminuva limitot
	if($dealData[0]->max_per_person > 0 && ($vouchersModel->getCntVouchersByDealOptionAndByCustomer($post['discount_id'], $post['deal_option_id'], $post["Email"]) + $post["amount"]) > $dealData[0]->max_per_person)
	{

		if(isset($_SERVER["HTTP_REFERER"]))
			url::redirect($_SERVER["HTTP_REFERER"]."/0/2");
		else
			url::redirect("/buy/deal/".$post['discount_id']."/".$post['deal_option_id']."/0/2");
	}
	
	//ako postoi ogranicuvanje na broj na vauceri
	if($dealData[0]->max_ammount > 0)
	{
		$soldVoucherCount = $vouchersModel->getCntPlateniVauceri($post['discount_id'], $post['deal_option_id']);
		
		if (!$dealData[0]->max_per_person) {
			$dealData[0]->max_per_person = 50;
		}
		
		$currentMaxPerPerson = $dealData[0]->max_per_person;
		
		if (($dealData[0]->max_ammount - $soldVoucherCount) < $dealData[0]->max_per_person )
			$currentMaxPerPerson = ($dealData[0]->max_ammount - $soldVoucherCount);
		
		if ($currentMaxPerPerson <= 0) 
			url::redirect("/deal/index/".$post['discount_id']."/1") ;
		elseif ($post["amount"] > $currentMaxPerPerson) 
			{
				if(isset($_SERVER["HTTP_REFERER"]))
					url::redirect($_SERVER["HTTP_REFERER"]."/0/1");
				else
					url::redirect("/buy/deal/".$post['discount_id']."/".$post['deal_option_id']."/0/1");
			}
			
	}

	//zacuvaj go order-ot vo voucher_attempt tabelata
	$ap_tracking = 'ap_ref_tracking';
	if(isset($_COOKIE[$ap_tracking]))
	{
		$kp_aff_program_id = intval($_COOKIE[$ap_tracking]);
		$post['kp_aff_program_id'] = $kp_aff_program_id;
	}

    $payAttemptID = $vouchersModel->savePayAttempt($post) ;
	
    if (!$payAttemptID) {
      print "Access forbidden payment" ;
      exit ;
    }


	//dokolku se plakja so OTKUP => prefrli go korisnikot na thankyou stranata za OTKUP
    if ($post['payment'] == 'otkup') {
      url::redirect("/buy/otkup_thankyou/".$payAttemptID) ;
    }
	
	//dokolku se plakja so cache ili so uplatnica
    if ($post['payment'] == 'cache' || $post['payment'] == 'bank') 
	{
		//ako ne e logiran ili e logiran so account razlicno od 297 (jovica.belovnski@gmail.com) => isfrli go nazad
		if($sessionUserType != 'customer' || $sessionUserID != 297)	
				url::redirect("/") ;
	}

	//dokolku se plakja so cache => kreiraj go vaucerot i prefrli go korisnikot na thankyou stranata za cache
    if ($post['payment'] == 'cache') {
      $vouchersModel->createVoucher($post, $payAttemptID, 'cache') ;
      url::redirect("/pay_casys/cache") ;
    }
	
	//dokolku se plakja so bank => kreiraj go vaucerot i prefrli go korisnikot na thankyou stranata za bank
    if ($post['payment'] == 'bank') {
      $vouchersModel->createVoucher($post, $payAttemptID, 'bank') ;
      if ($dealData[0]->card) {
          url::redirect("/pay_casys/bankcard") ;
      }
      url::redirect("/pay_casys/bank") ;
    }
	

    //dokolku se plakja so karticka (card)
	$voucher_price = ($dealData[0]->tip == 'cena_na_vaucer' ? $dealData[0]->price_voucher : $dealData[0]->price_discount);
    $ammount = $voucher_price * $post["amount"];


	//PLAKJANJE NA RATI
	$installmentFinalValue = 1;

	//dokolku se naoga vo POST-ot togas setiraj go
	if(isset($post["installment"])) 
		$installmentFinalValue = (int) $post["installment"];

	//mora da bide edna od slednite vrednosti = "1", "2", "4", "6", "10", "12"
	if(!in_array($installmentFinalValue, array(1, 2, 4, 6, 10, 12))) 
		$installmentFinalValue = 1;

	if($ammount < 2000)
		$installmentFinalValue = 1;

	if($ammount >= 2000 && $ammount < 6000 && $installmentFinalValue > 6)
		$installmentFinalValue = 1;	

	//moze da koristi poeni samo ako ne plakja na rati
    if ($installmentFinalValue == 1 && isset($post['points_used']) && (int)$post['points_used'] > 0) {
		
		//stom koristi poeni togas mora da e logiran
		//ako e logiran => proveri dali navistina korisnikot ima tolku poeni.
		if($sessionUserType == 'customer' && $sessionUserID > 0)
		{
			$currentCustomerPoints = $customerModel->getCustomerPoints($sessionUserID);
	
		   //ako ima togas namalija cenata sto treba da ja plati
		   if($currentCustomerPoints >= $post['points_used'])
				$ammount = $ammount - $post['points_used'] ;
		   else//dokolku nema tolku poeni => isfrli go nazad
				url::redirect("/") ;
		}
		else//ako ne e logrin => isfrli go nazad
			url::redirect("/") ;
    }

	//dokolku sumata za plakjanje e 0
	if($ammount == 0)
	{
		//stavi sesiska varijabla payed na true
		$this->session->set("payed", true) ;
		
		//kreiraj vaucer
		$vouchersModel->createVoucher($post, $payAttemptID, 'card') ;
		
		//redirekt na success - thankyou stranata
		url::redirect("pay_casys/success/$payAttemptID?exitif=true") ;
		exit ;
	}
	
	//setiranje na vrednosta za zacuvuvanje na platezna karticka
	$saved_cc = $post["saved_cc"];
	$save_credit_card = $post["save_credit_card"];

	//dokolku treba da plati preku payment gateway
	$bankStatemenDetails = substr("Kupinapopust.mk naracka br. $payAttemptID za ".$post["Email"], 0, 299);//ogranicuvanje na Casys na 300 karakteri
	
    $preauthorization = new Preauthorization($ammount, $bankStatemenDetails, $payAttemptID) ;
    $preauthorization->setLanuage($this->lang) ;
    $preauthorization->setEmail($post["Email"]) ;
    $preauthorization->setInstallment($installmentFinalValue);
    $preauthorization->setCRef($saved_cc, $save_credit_card);

    // $params = $preauthorization->getPostParams() ;

    $this->template->content = new View("pay/index_casys") ;
    $this->template->title = kohana::lang("prevod.website_title") ;
    // $this->template->content->params = $params ;

	if(CPayConfig::$preauthorizationUrl == "/pay_casys/localtest")
		$this->template->content->iframeURL = $preauthorization->getUrl()."?Details2=".$payAttemptID."&ammount=".$ammount ;
	else
		$this->template->content->iframeURL = $preauthorization->getIframeUrl() ;
    
  }

// index

  public function response() {

    $this->session->set("payed", true) ;

    try {
      $response = new PreauthorizationResponse() ;
    } catch (Exception $err) {
      Kohana::log("error", $err->getTraceAsString()) ;
      exit ;
    }

    if (!$response->isRequestConsistent()) {
      Kohana::log("error", "bad request") ;
      $this->badRequest($response) ;
    }

    if ($response->isSenderAuthenticated()) {
      $this->responseFromCpay($response) ;
    }
    else {
      $this->responseFromRedirect($response) ;
    }
  }

	public function response_localtest() {
		
		$this->session->set("payed", false) ;
		
		$post = $this->input->post() ;
		
		$payAttemptID = $post['payAttemptId'] ;
		$refID = "1" ;

		//dali da se cuva plateznata karticka
		$cRefID = "DG1234";


		$this->successfullTransactionToDo($payAttemptID, $refID, $cRefID);

		
		url::redirect("pay_casys/success/$payAttemptID?exitif=true") ;
		
		//        $orderPreauthorization = new OrderPreauthorizationResonse();
		//        $orderPreauthorization->savePreauthorizationCpay($response);
		exit ;
	}


// responseFromRedirect
  private function responseFromRedirect(PreauthorizationResponse $response) {
	$payAttemptID = $response->getDetails2() ;
	
    if ($response->getReasonOfDecline()) {  
      url::redirect("/pay_casys/error/$payAttemptID?exitif=true") ;
    }
    else {
      $this->session->set("payed", true) ;
	  url::redirect("pay_casys/success/$payAttemptID?exitif=true") ;
    }
  }


// responseFromCpay
  private function responseFromCpay(PreauthorizationResponse $response) {

    $payAttemptID = $response->getDetails2() ;
    $refID = $response->getCPayPaymentRef() ;

    $cRefID = $response->getCRef() ;

	$this->successfullTransactionToDo($payAttemptID, $refID, $cRefID);

	
    url::redirect("pay_casys/success/$payAttemptID?exitif=true") ;


//        $orderPreauthorization = new OrderPreauthorizationResonse();
//        $orderPreauthorization->savePreauthorizationCpay($response);
    exit ;
  }



  private function successfullTransactionToDo($payAttemptID = "", $refID = "", $cRefID = "") 
  {

    $vouchersModel = new Vouchers_Model() ;
	
	//proveri dali vekje ednas si ja primil push porakata od casys
	//i si napravil se sto treba so kreiranje na vaucer-ot
	$payData = $vouchersModel->getPayAttempt($payAttemptID) ;
	
	//ako e prv pat togas napravi sto treba za uspesna transakcija
	if($payData[0]->ref == "")
	{

  		//zacuvaj go REFERENCE brojot 
		$vouchersModel->savePaymentRef($payAttemptID, $refID);

		//kreiraj vaucer
		$data = array("discount_id" => $payData[0]->deal_id, "amount" => $payData[0]->quantity) ;
		$vouchersModel->createVoucher($data, $payAttemptID, "card", $payData[0]->kp_aff_program_id) ;

		//dokolku e odbrano da se zacuva plateznata karticka togas zacuvaj ja
		if($cRefID != "")
		{
			$cardOnFileModel = new Card_On_File_Model() ;
			$cardOnFileModel->savePaymentCard($payData[0]->email, $cRefID);
		}	

	}
  }

	public function racno_kreiranje_vauceri($password = "", $payAttemptID = "")
	{
		$this->auto_render = FALSE ;


		if($password == "10Kupinap0pust10")
		{
			if($payAttemptID != "")
			{
				$this->successfullTransactionToDo($payAttemptID, "racno");

				die("Uspesno! Ve molam proverete vo sistemot dali se kreirani vaucerite");
			}
			else
				die("prazen ID na naracka");
		}
		else
			die("ne validen password");
	}

// badRequest
  private function badRequest(PreauthorizationResponse $response) {
    // prati mail
    $mailBody = "" ;
    $mailBody .= "orders_id = " . $response->getDetails2() . "<br />" ;
    $mailBody .= "ip = " . $_SERVER["REMOTE_ADDR"] . "<br />" ;
    $mailBody .= "get parameters = " . print_r($_GET, true) . "<br />" ;
    $mailBody .= "post parameters = " . print_r($_POST, true) . "<br />" ;

    $to = "bastrijan@gmail.com" ;
    email::send($to, "error@kupinapopust.mk", "CPay bad request", $mailBody, true) ;

    // show error to the user
    url::redirect("/pay_casys/error") ;

    exit ;
  }



  public function success($payAttemptID = '') 
  {

	//zemi gi variablite od GET-ot
	$get = $this->input->get();

	//ZEMI GI INFORMACIITE OD BAZA
	$vouchersModel = new Vouchers_Model() ;
	$dealsModel = new Deals_Model();
	$categoryModel = new Categories_Model();
	$payAttemptData = $vouchersModel->getPayAttempt($payAttemptID) ;
	
    //$this->template->title = kohana::lang("prevod.website_title") ;
	$this->template->content = new View('pay/success_casys') ;
	
	$where_options_str = "d.id = ".$payAttemptData[0]->deal_id." AND do.id = ".$payAttemptData[0]->deal_option_id;
    $primary = $dealsModel->getDealOptionsData($where_options_str);


	$where_cat = array("id"=>$primary[0]->category_id);
	$categoryObj = $categoryModel->getData($where_cat);
	
	//proverka dali e nadminat limitot
	$soldVoucherCount = $vouchersModel->getCntPlateniVauceri($primary[0]->deal_id, $primary[0]->deal_option_id);
	$vaucer_nadminat_limit = ($primary[0]->max_ammount > 0 && ($soldVoucherCount > $primary[0]->max_ammount) ) ? 1 : 0;

	//LYONESS confirmation - kreiranje na pixel
	//samo koga se povikuva bez exitif za da samo ednas se izvrsi
	//isto taka mora da bide LYONESS korisnik
	$lyonessConfirmationImgPixel = "";
	if (  ( !isset($get["exitif"]) || $get["exitif"] == "" )  && commonshow::isLyonessUser()  )
	{
		//povikaj ja funcijata za presmetka na ddv-to
		$vk_ddv = calculate::ddv($primary[0]->price_discount, $primary[0]->price_voucher, $primary[0]->tip_danocna_sema, $primary[0]->ddv_stapka, $primary[0]->tip, $payAttemptData[0]->quantity, ((date("Y-n", strtotime($primary[0]->platena_reklama_date)) == date("Y-n")) ? $primary[0]->platena_reklama : 0));

		$total_order_price = round((($primary[0]->finalna_cena * $payAttemptData[0]->quantity) - $payAttemptData[0]->points_used - $vk_ddv));

		$lyonessConfirmationImgPixel = $this->LyonessConfirmationImgPixel($payAttemptID, $total_order_price);
	} 
		
	
	//output variablite se generiraat
	$this->template->content->payAttemptData = $payAttemptData[0];
	$this->template->content->dealData = $primary[0];
	$this->template->content->categoryData = $categoryObj[0];
	

	$this->template->set_global("fb_image", Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/pub/deals/".$primary[0]->main_img);
	$this->template->set_global("fb_title", strip_tags($primary[0]->title_mk));
	$this->template->set_global("fb_link",  Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/deal/index/".$primary[0]->deal_id);
	$this->template->set_global("fb_description", strip_tags($primary[0]->content_short_mk));
	
	$this->template->title = ($primary[0]->card ? 'Подарок "kupinapopust" електронска картичка во вредност од ' : "").trim(strip_tags($primary[0]->title_mk));
	$this->template->content->vaucer_nadminat_limit = $vaucer_nadminat_limit;

	$this->template->content->lyoness_confirmation_img_pixel = $lyonessConfirmationImgPixel;
	
  }

  private function LyonessConfirmationImgPixel($payAttemptID = 0, $total_order_price = 0) 
  {

  		//array za lyoness settings
  		$lyoness_settings_arr = Kohana::config('settings.lyoness');
  		
		// Your organization ID; provided by Lyoness
		//
		$organization = $lyoness_settings_arr["organization"];
		
		// Your checksum code; provided by Lyoness
		//
		$checksumCode = $lyoness_settings_arr["checksumCode"];
		
		// Value of the sale; sale specific value has to
		// be assigned dynamically out of the shop database
		//
		$orderValue = $total_order_price;
		
		// Currency of the sale.
		// For example "USD" for US $, or "EUR" for Euro
		//
		$currency = $lyoness_settings_arr["currency"];
		
		// Event ID; provided by Lyoness
		//
		$event = $lyoness_settings_arr["event"];

		// Event type:
		// in most cases $isSale should be left as "true" unless
		// you arranged a lead commission with Lyoness
		// true = Sale
		// false = Lead
		//
		$isSale = true;

		// Encrypted connection on this page:
		// true = Yes (https)
		// false = No (http)
		//
		$isSecure = true;

		// Here you must specify a unique identifier for the transaction.
		// You should assign your internal shop order number dynamically
		// out of your shop database
		//
		$orderNumber = $payAttemptID;

		// If you do not use the built-in session functionality in PHP, modify
		// the following expressions to work with your session handling routines.
		//
		$tduid = "";
		if (!empty($_SESSION["TRADEDOUBLER"]))
			$tduid = $_SESSION["TRADEDOUBLER"];


		// OPTIONAL: You may transmit a list of items ordered in the reportInfo
		// parameter. See the chapter reportInfo for details.
		//
		$reportInfo = ""; 
		$reportInfo = urlencode($reportInfo);



		/***** IMPORTANT: *****/
		/***** In most cases, you should not edit anything below this line. *****/
		if (!empty($_COOKIE["TRADEDOUBLER"]))
			$tduid = $_COOKIE["TRADEDOUBLER"];

		if ($isSale)
		{
			$domain = "tbs.tradedoubler.com";
			$checkNumberName = "orderNumber";
		}
		else
		{
			$domain = "tbl.tradedoubler.com";
			$checkNumberName = "leadNumber";
			$orderValue = "1";
		}
		
		$checksum = "v04" . md5($checksumCode . $orderNumber . $orderValue);
		
		if ($isSecure) 
			$scheme = "https";
		else 
			$scheme = "http";
		
		$trackBackUrl = $scheme . "://" . $domain . "/report"
		. "?organization=" . $organization
		. "&amp;event=" . $event
		. "&amp;" . $checkNumberName . "=" . $orderNumber
		. "&amp;checksum=" . $checksum
		. "&amp;tduid=" . $tduid
		. "&amp;reportInfo=" . $reportInfo;
		
		if ($isSale)
		{
			$trackBackUrl
			.= "&amp;orderValue=" .$orderValue
			.= "&amp;currency=" .$currency;
		}

		return "<img src=\"" . $trackBackUrl . "\" alt=\"\" style=\"border: none\" />";
  }

  public function error($id = '') {
    $this->template->content = new View('pay/fail_casys') ;
    $this->template->title = kohana::lang("prevod.website_title") ;
  }

  public function cache() {
    $this->template->content = new View('pay/cache_casys') ;
    $this->template->title = kohana::lang("prevod.website_title") ;
  }

  public function bank() {
    $this->template->content = new View('pay/bank_casys') ;
    $this->template->title = kohana::lang("prevod.website_title") ;
  }
  public function bankcard() {
    $this->template->content = new View('pay/card_bank_casys') ;
    $this->template->title = kohana::lang("prevod.website_title") ;
  }

  public function __call($method, $arguments) {
    $this->auto_render = FALSE ;
    echo 'This text is generated by __call. If you expected the index page, you need to use: welcome/index/' . substr(Router::$current_uri, 8) ;
  }

	public function localtest() {
		$this->template->content = new View('pay/localtest_casys') ;
		$this->template->title = kohana::lang("prevod.website_title") ;

		$this->template->content->payAttemptId = $this->input->get("Details2");
		$this->template->content->ammount = $this->input->get("ammount");
	}
	
}