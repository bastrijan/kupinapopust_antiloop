<?php

defined('SYSPATH') OR die('No direct access allowed.');

abstract class Default_Controller extends Controller {

    // Template view name
    public $template = 'layouts/public';
    // Default to do auto-rendering
    public $auto_render = TRUE;
    public $session = "";
    public $lang = "mk";
    public $user_id = "";
    public $user_type = "";
    protected $admin_controllers = array("admin", "admin2", "acategories", "abanners", "apartners", "admin_deals", "admin_naracki");
    protected $partner_controllers = array("partner");
    protected $customer_controllers = array("customer");

    /**
     * Template loading and setup routine.
     */
    public function __construct() {

        $this->session = Session::instance();
        if ($this->session->get("lang")) {
            $this->lang = $this->session->get("lang");
        } else {
            $this->lang = "mk";
        }

        if ($this->lang == "mk") {
            Kohana::config_set("locale.language", "mk_MK");
        } else {
            Kohana::config_set("locale.language", "en_US");
        }

        $this->user_id = $this->session->get("user_id");
        $this->user_type = $this->session->get("user_type");
        
        if ((!$this->user_id or $this->user_type != 'admin') and in_array(Router::$controller, $this->admin_controllers) and Router::$method != 'login') {
            url::redirect("/admin/login");
        }
        if ((!$this->user_id or $this->user_type != 'partner') and in_array(Router::$controller, $this->partner_controllers) and Router::$method != 'login') {
            url::redirect("/partner/login");
        }
        if ((!$this->user_id or $this->user_type != 'customer') and in_array(Router::$controller, $this->customer_controllers) and Router::$method != 'login' and Router::$method != 'login_android'and Router::$method != 'resetpassword' and Router::$method != 'resetpassword_android' and Router::$method != 'choosepassword' and Router::$method != 'fblogin'  and Router::$method != 'fblogin_mobile' and Router::$method != 'registracija'  and Router::$method != 'registracija_android' ) {
//            print "nema logiran customer";
//            exit;
            url::redirect("/customer/login");
        }
		


        parent::__construct();
		
        // Load the template
        $this->template = new View($this->template);
        $this->template->set_global("lang", $this->lang);
		
        $this->template->set_global("fb_image", false);
        $this->template->set_global("fb_title", false);
        $this->template->set_global("fb_link", false);
		$this->template->set_global("fb_description", false);

		
        /*******TRACKING NA KORISNIK*******/
        if(!in_array(Router::$controller, $this->admin_controllers) && !in_array(Router::$controller, $this->partner_controllers) )
            trackvisitor::checkToTrack($this->template);


        /*******TRACKING NA NASIOT AFFILIATE*******/
        $affiliateRefID = $this->input->get("ref", 0);

        // if( 
        //     (Router::$controller == "index" && Router::$method == "index")  ||
        //     (Router::$controller == "deal" && Router::$method == "index")  ||
        //     (Router::$controller == "all" && Router::$method == "index")  ||
        //     (Router::$controller == "all" && Router::$method == "category")  ||
        //     (Router::$controller == "all" && Router::$method == "subcategory")
        // )
        if($affiliateRefID)
        {
            include(DOCROOT.'affiliate/controller/affiliate-tracking.php');
        }


        if ($this->auto_render == TRUE) {
            // Render the template immediately after the controller method
            Event::add('system.post_controller', array($this, '_render'));
        }
    }

    public function logout($nextPage = null) {
        $session = Session::instance();
        $session->destroy();
        if ($nextPage) {
            url::redirect(base64_decode($nextPage));
        }
        url::redirect('/?type=2');
    }



    /**
     * Render the loaded template.
     */
    public function _render() {
        if ($this->auto_render == TRUE) {
            // Render the template when the class is destroyed
            $this->template->render(TRUE);
        }
    }

}

// End default_Controller