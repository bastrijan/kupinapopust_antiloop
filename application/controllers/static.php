<?php defined('SYSPATH') OR die('No direct access allowed.');

class Static_Controller extends Default_Controller {

	public function __construct() {
		$this->template = 'layouts/public' ;
		
		if(in_array(Router::$method, array('page_android'))) {
			$this->template = 'layouts/android' ;
		}
		parent::__construct() ;
	}

    public function index() {
        url::redirect("/");
    }
	
	
	/**
	 * This is method page
	 *
	 * @param mixed $typemobile opisuva dali se koristi za android ili za web prikaz
	 *
	 */
	public function page($identifier, $typemobile = "") {
        $staticPagesModel = new Static_Model();
        if (!$identifier) {
			if($typemobile == "android")
				die("");
			else
				url::redirect("/");
        }

        $where = array('identifier'=>$identifier);
        $pageData = $staticPagesModel->getData($where);
        if (!$pageData) {
			if($typemobile == "android")
				die("");
			else
				url::redirect("/");
        }


        $propertyTitle = 'title_'.$this->lang;
        $propertyContent = 'content_'.$this->lang;

        $this->template->title = $pageData[0]->$propertyTitle;
        $this->template->content = new View("static/page".($typemobile == "android" ? "_android" : ""));
        $this->template->content->pageData = $pageData[0];
        $this->template->content->pageTitle = $pageData[0]->$propertyTitle;
        $this->template->content->pageContent = $pageData[0]->$propertyContent;

    }
 
	public function page_android($identifier) {
		$this->page($identifier, "android");
	}  
    
    public function __call($method, $arguments) {
        // Disable auto-rendering
        $this->auto_render = FALSE;
        echo 'page not found';
    }
}