<?php
defined('SYSPATH') OR die('No direct access allowed.') ;

class Apartners_Controller extends Default_Controller 
{

  public function __construct() {
    $this->template = 'layouts/admin' ;
   
    parent::__construct() ;
  }






   public function index() {
		
		//template
		$this->template->title = "Партнери" ;
		$this->template->content = new View("/admin_partners/index") ;
		
		$partnersModel = new Partners_Model() ;
		
		$partnersData = $partnersModel->getData() ;
		
		$this->template->content->partnersData = $partnersData;
	}





  public function manage($partner_id = null) 
  {

	    $this->template->title = "Добар попуст, подесување" ;
	    $this->template->content = new View("/admin_partners/manage") ;
	    $this->template->content->errorFlag = 0 ;

	    $partnersModel = new Partners_Model() ;

	    if (request::method() == 'post') 
	    {
		      //should save then
		      $post = $this->input->post() ;
			  unset($post['files']);

			  //proveri dali username-ot e duplikat dokolku se vnesuva nov partner
			  $sum_partners = 0;

			  if($post['id'] == '')
			  	$partner_id_check = 0;
			  else
			  	$partner_id_check = $post['id'];
			  
			  $sum_partners = $partnersModel->checkDuplicatePartnerUsername($post['username'], $partner_id_check);

			  if($sum_partners > 0)
			  	$this->template->content->errorFlag = 1 ;
			  

			
			  //ako e odbrano tip_danocna_sema za ddv da bide "Промет остварен во странство" => stavi go ddv-to da bide 0%
			  if ($post['tip_danocna_sema'] == "Промет остварен во странство")
					$post['ddv_stapka'] = 0;

			  if($sum_partners == 0)	
			  {
			      $partnersModel->saveData($post) ;
			      url::redirect("/apartners/") ;	  	
			  }

	    }

	    if ($partner_id) 
	    {
			  //edit
			  $where = array('id' => $partner_id) ;
			  $partnerData = $partnersModel->getData($where) ;
			  $this->template->content->dealData = $partnerData ;
	    }
	    else {
	      // new, just render the form
	    }
  }






  public function mark_as_paid($partner_id = 0, $updateSuccess = 0) 
  {
	    $this->template->title = "admin welcome" ;
	    $this->template->content = new View("/admin_partners/mark_as_paid") ;

	    $partner_id = (int) $partner_id;

	    $updateSuccess = (int) $updateSuccess;

	    //ako ne e vnesena brojka kako partner ID
	    if($partner_id == 0)
	    	url::redirect("/apartners") ;

		//KREIRAJ GI POTREBNITE MODELI
		$vouchersModel = new Vouchers_Model();
		$partnersModel = new Partners_Model();

		//AKO E KLIKNATO KOPCETO - OZNACI KAKO ISPLATENO (POST)
        if(request::method() == 'post')
        {
            $post = $this->input->post();
            
            $vouchersModel->paidOffVoucher($partner_id);

            url::redirect("/apartners/mark_as_paid/".$partner_id."/1") ;

        }

    	$where = array('id' => $partner_id) ;
		$partnerData = $partnersModel->getData($where) ;
		$this->template->content->partnerData = $partnerData ;


    	$this->template->content->updateSuccess = $updateSuccess ;


		//izvadi informacija za SUMA ZA NAPLATA
		$infoZaNaplata = $partnersModel->getInfoZaIsplata($partner_id); 

		//OUTPUT INFORMACIITE STO ODAT VO VIEW-TO
		$this->template->content->sumaZaNaplata = $infoZaNaplata[0]->suma_za_isplata;

  }


  public function paid_off_notes_admin($partner_id = 0) 
  {
  		$this->auto_render = FALSE ;

	    $partner_id = (int) $partner_id;

	    //ako ne e vnesena brojka kako partner ID
	    if($partner_id == 0)
	    	url::redirect("/apartners") ;

		//KREIRAJ GI POTREBNITE MODELI
		$partnersModel = new Partners_Model();

		//AKO E KLIKNATO KOPCETO - OZNACI KAKO ISPLATENO (POST)
        if(request::method() == 'post')
        {
            $post = $this->input->post();
            
        	unset($notes_post_arr);
            $notes_post_arr["id"] = $partner_id;
            $notes_post_arr["paid_off_notes_admin"] = $post["paid_off_notes_admin"];
            $partnersModel->saveData($notes_post_arr) ;

            url::redirect("/apartners/mark_as_paid/".$partner_id."/1") ;

        }
        else
        	url::redirect("/apartners/mark_as_paid/".$partner_id."/") ;

  }

	//ova e za avtomatskiot ajax za zacuvuvanje na ADMIN NOTES ZA ODREDEN PARTNER (ZACUVAJ ZABELESKA)
	 public function ajax_autosave_paid_off_notes_admin(){

	    $this->template->title = "Добар попуст, подесување" ;
	    $this->template->content = "";
		

	    if (request::method() == 'post') {
	        
	        $partnersModel = new Partners_Model();
	        
	        //should save then
	        $post = $this->input->post();
	        
	        unset($notes_post_arr);
	        $notes_post_arr["id"] = $post["partner_id"];
	        $notes_post_arr["paid_off_notes_admin"] = $post["paid_off_notes_admin"];
	        $partnersModel->saveData($notes_post_arr) ;
	              
		    Kohana::config_set('config.global_xss_filtering', true);
		    $arr = array('status' => 'success');
		    die(json_encode($arr));
	    }

	}
	
	
  public function __call($method, $arguments) {
    // Disable auto-rendering
    $this->auto_render = FALSE ;
    echo 'page not found' ;
  }



}