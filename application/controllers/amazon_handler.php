<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Amazon_Handler_Controller extends Default_Controller {

    
    public function bounces () 
    {
        //se postavuva max_execution_time za da moze skriptata da vrti podolgo vreme bez da ja skine serverot.
        ini_set('max_execution_time', 300); //300 = 5 min

        $this->auto_render = FALSE;
        
        //zemi gi heade-ite
        $headers_arr = getallheaders();

        /*****ZA TEST NA ZEMANJE NA HEADER-ITE *****/
        // foreach ($headers_arr as $name => $value) {
        //     echo "$name: $value\n";
        //     Kohana::log("error", "AMAZON header - $name: $value");
        // }


        //proveri vo HTTPS Headers dali navistina doagja od AMAZON
        //spored slednata dukumentacija: https://docs.aws.amazon.com/sns/latest/dg/sns-message-and-json-formats.html#http-header
        if(isset($headers_arr["X-Amz-Sns-Topic-Arn"]) && $headers_arr["X-Amz-Sns-Topic-Arn"] == Kohana::config('config.amazon_X_Amz_Sns_Topic_Arn'))
        {

            //proveri vo HTTPS Headers dali x-amz-sns-message-type e: Notification
            //spored slednata dukumentacija: https://docs.aws.amazon.com/sns/latest/dg/sns-message-and-json-formats.html#http-header
            if(isset($headers_arr["X-Amz-Sns-Message-Type"]) && $headers_arr["X-Amz-Sns-Message-Type"] == "Notification") 
            {
                //zemi go content-ot
                $postdata = file_get_contents("php://input");

                /*****ZA TEST NA ZEMANJE NA CONTENT-OT *****/
                // die($postdata);
                // Kohana::log("error", "AMAZON content:". $postdata);


                //napravi decode na JSON
                $jsonAllObj = json_decode($postdata);

                // die(print_r($jsonAllObj));

                $jsonMessageObj = json_decode($jsonAllObj->Message);

                // die(print_r($jsonMessageObj));


                //proveri dali navistina e BOUNCE notifikacija
                if(isset($jsonMessageObj->notificationType) && in_array($jsonMessageObj->notificationType, array("Bounce", "Complaint")) )
                {
                    $emailToDelete = "";

                    //najdi ja email adresata sto treba da se brise

                    if($jsonMessageObj->notificationType == "Bounce")
                        $emailToDelete = $jsonMessageObj->bounce->bouncedRecipients[0]->emailAddress;
                    elseif($jsonMessageObj->notificationType == "Complaint")
                            $emailToDelete = $jsonMessageObj->complaint->complainedRecipients[0]->emailAddress;

                    //izbrisi ja email adresata  
                    $newslettersModel = new Newsletter_Model();

                    $newslettersModel->delete_by_email($emailToDelete);  

                    //zapisi vo log
                    Kohana::log("error", "AMAZON - ".$jsonMessageObj->notificationType." izbrisan email:". $emailToDelete);

                }

            }    
        }

    }

    public function __call($method, $arguments) {
        // Disable auto-rendering
        $this->auto_render = FALSE;
        echo 'page not found';
    }

}