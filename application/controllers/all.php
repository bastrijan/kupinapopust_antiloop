<?php defined('SYSPATH') OR die('No direct access allowed.');

class All_Controller extends Default_Controller {
	
    private $pagination_settings;

    public function __construct() 
    {
		$this->template = 'layouts/public' ;

        $this->pagination_settings = Kohana::config('pagination.default');
		
	
        if(strpos(Router::$method, "_android") !== false) {
            $this->template = 'layouts/android' ;
        }
		parent::__construct();
	}
        
    public function category_android($category_id = 0) {
        $this ->category($category_id, "android");
    }

	public function category($category_id = 0, $type = "") {
		
                //proveri dali se povikuva so permalink.
                //dokolku e permalink togas najdi go ID-to i stavi go vo $category_id
                $category_id = seo::PermaLinkConversion($category_id);
                
                //new Profiler;
                if($type == "android"){
                    //kreiraj nov template - view
                     $this -> template -> content = new View('alloffers/index_android');
                }else{
                    $this -> template -> content = new View('alloffers/index');
                }
                $generalenTitle = kohana::lang("prevod.website_title");
                
                 //inicijalizacija na modeli
                $dealsModel = new Deals_Model();
                $categoriesModel = new Categories_Model() ;
                $subCategoriesModel = new Subcategories_Model();
                
                //Ajax request za paginacija na ponudite
                if(request::method() == 'post' && $type == "android"){
                    
                    $post = $this->input->post() ;
                    $dateTimeUnique = $post['dateTimeUnique'] ;
                    $pagination_cnt = $post['pagination_cnt'] ;
                    $type_post = $post['type'] ;
                    $deals_limit = 10;
                    
                    if($type_post == "ajax" ){
                        
                        $page = $pagination_cnt + 1;
                        $offset = $deals_limit * $page ;
                        
                        $allPaginationOffers = $dealsModel -> getPonudiByCategoryPagination($category_id, $offset, $deals_limit, $dateTimeUnique);
                        $all_num = count($allPaginationOffers);
                        $html = $this->buildAjaxHtml($allPaginationOffers);
                        
                        $arr = array('status' => 'success', 'pagination_cnt' => $page, 'html' => $html, 'ajax_btn' => $all_num, 'TEST' => $dateTimeUnique );
                        die(json_encode($arr));
                        
                    }
                    
                }else{
                    
                    $dateTimeUnique = date("Y-m-d H:i:s");

                    //povikuvanje na funcii

                    $allOffers = array();
                    $bottomOffers = array();
                    $bottomOffersPagesCnt = 0;
                    
                     if($type == "android"){
                        $allOffers = $dealsModel -> getPonudiByCategoryPagination($category_id, 0, 10, $dateTimeUnique);
                    }else{

                        // KOD KOJ STO VADI PODATOCI OD BAZA ZA PRIKAZ NA PONUDI
                        $bottomOffersPagesCnt = ceil($dealsModel->getPonudiByCategoryPaginationCnt($category_id, $dateTimeUnique) / $this->pagination_settings['items_per_page']);      
                        $bottomOffers = $dealsModel->getPonudiByCategoryPagination($category_id, 0, $this->pagination_settings['items_per_page'], $dateTimeUnique);
                    }
                    
                    $categoriesData = $categoriesModel->getCategories();
                    $subCategoriesData = $subCategoriesModel->getSubcategoriesbyParentid(0);
                    $categoryObjectData = $categoriesModel->getData(array('id' => $category_id));

                    //kreiranje na variabli
                    $this -> template -> content -> selectedCategory = $category_id;
                    $this->template->content->allOffers = $allOffers;
                    $this->template->content->bottomOffers = $bottomOffers;
                    $this->template->content->bottomOffersPagesCnt = $bottomOffersPagesCnt;

                    $this->template->content->categoriesData = $categoriesData ;
                    $this->template->content->subCategoriesData = $subCategoriesData ;
                    $this->template->content->scrollPageTitle = $categoryObjectData[0]->name ;
                    $this->template->content->categoryObjectData = $categoryObjectData[0];

                    //$generalenTitle .= (!empty($categoryObjectData) ? "-".seo::url_slug($categoryObjectData[0]->name) : "");
					$generalenTitle = (!empty($categoryObjectData) ? $categoryObjectData[0]->name : "");
					
					if ($generalenTitle == "")
					{
						$generalenTitle = "kupinapopust.mk";
					}
					else
					{
						$generalenTitle .= " | kupinapopust.mk";
					}	

                    //vo slucaj da e odbrana nekoja kategorija
                    if($category_id > 0 && $type == "")
                    {
                            //inicijalizacija na modeli
                            $subCategoriesModel = new Subcategories_Model();

                            $subCategoriesArray = $subCategoriesModel->getAllSubCategoriesWithOffers($category_id);

                            $this->template->content->subCategoriesArray = $subCategoriesArray;
                    }
                }
                $this -> template -> content -> dateTimeUnique = $dateTimeUnique;
                $this -> template -> title = $generalenTitle;
				$this -> template -> share_dialog_show = true;
				$this -> template -> bridgeName = "CategoriesHandlerDeal";
				
				// OMILENI
				// zemanje na 'cookie'
				$cookie_value = cookie::get("kupinapopust_fav");
				if ( ! empty($cookie_value)) {
					$favDeals = json_decode($cookie_value, true);
				} else {
					$favDeals = array();
				}
				$this->template->content->favDeals = $favDeals;
				// END - OMILENI

                // SHOPPING CART
                // zemanje na 'cookie'
                $cookie_value_shop_cart = cookie::get("kupinapopust_shop_cart");

                if ( ! empty($cookie_value_shop_cart)) {
                    $shopCartDeals = json_decode($cookie_value_shop_cart, true);
                } else {
                    $shopCartDeals = array();
                }
                $this->template->content->shopCartDeals = $shopCartDeals;
                // END - SHOPPING CART

		
	}
        
	public function subcategory_android($category_id = 0, $subcategory_id = 0) {
            $this ->subcategory($category_id, $subcategory_id, "android");
        }
        
	public function subcategory($category_id = 0, $subcategory_id = 0, $type = "") {
		
		//proveri dali se povikuva so permalink.
		//dokolku e permalink togas najdi go ID-to i stavi go vo $category_id
		$category_id = seo::PermaLinkConversion($category_id);
		
		//proveri dali se povikuva so permalink.
		//dokolku e permalink togas najdi go ID-to i stavi go vo $subcategory_id
		$subcategory_id = seo::PermaLinkConversion($subcategory_id);
		
		//new Profiler;
                if($type == "android"){
                    //kreiraj nov template - view
                     $this -> template -> content = new View('alloffers/index_android');
                }else{
                    $this -> template -> content = new View('alloffers/index');
                }
		$generalenTitle = kohana::lang("prevod.website_title");
		
		//inicijalizacija na modeli
		$dealsModel = new Deals_Model();
		$categoriesModel = new Categories_Model() ;
		$subCategoriesModel = new Subcategories_Model();
		
                  //Ajax request za paginacija na ponudite
                if(request::method() == 'post' && $type == "android"){
                    
                    $post = $this->input->post() ;
                    $pagination_cnt = $post['pagination_cnt'] ;
                    $dateTimeUnique = $post['dateTimeUnique'] ;
                    $type_post = $post['type'] ;
                    $deals_limit = 10;
                    
                    if($type_post == "ajax" ){
                        
                        $page = $pagination_cnt + 1;
                        $offset = $deals_limit * $page ;
                        
                        $allPaginationOffers = $dealsModel -> getPonudiBySubCategoryPagination($subcategory_id, $offset, $deals_limit, $dateTimeUnique );
                        $all_num = count($allPaginationOffers);
                        $html = $this->buildAjaxHtml($allPaginationOffers);
                        
                        $arr = array('status' => 'success', 'pagination_cnt' => $page, 'html' => $html, 'ajax_btn' => $all_num );
                        die(json_encode($arr));
                        
                    }
                    
                }else{
                
                        $dateTimeUnique = date("Y-m-d H:i:s");

                        //povikuvanje na funcii

                        $allOffers = array();
                        $bottomOffers = array();
                        $bottomOffersPagesCnt = 0;
						
						if($type == "android"){
							$allOffers = $dealsModel -> getPonudiBySubCategoryPagination($subcategory_id, 0, 10, $dateTimeUnique);
                        }else{

                            // KOD KOJ STO VADI PODATOCI OD BAZA ZA PRIKAZ NA PONUDI
                            $bottomOffersPagesCnt = ceil($dealsModel->getPonudiBySubCategoryPaginationCnt($subcategory_id, $dateTimeUnique) / $this->pagination_settings['items_per_page']);      
                            $bottomOffers = $dealsModel->getPonudiBySubCategoryPagination($subcategory_id, 0, $this->pagination_settings['items_per_page'], $dateTimeUnique);
                        }
                        						
                        $categoriesData = $categoriesModel->getCategories();
                        $subCategoriesData = $subCategoriesModel->getSubcategoriesbyParentid(0);
                        $categoryObjectData = $categoriesModel->getData(array('id' => $category_id));
                        $subCategoryObjectData = $subCategoriesModel->getData(array('id' => $subcategory_id));

                        //kreiranje na variabli
                        $this->template->content->allOffers = $allOffers;
                        $this->template->content->bottomOffers = $bottomOffers;
                        $this->template->content->bottomOffersPagesCnt = $bottomOffersPagesCnt;
                        $this -> template -> content -> selectedCategory = $category_id;
                        $this -> template -> content -> selectedSubCategory = $subcategory_id;

                        $this->template->content->categoriesData = $categoriesData ;
                        $this->template->content->subCategoriesData = $subCategoriesData ;
                        $this->template->content->subCategoriesName = $subCategoryObjectData[0]->name ;
                        $this->template->content->scrollPageTitle = $categoryObjectData[0]->name."/".$subCategoryObjectData[0]->name ;
                        $this->template->content->categoryObjectData = $categoryObjectData[0];

                        //$generalenTitle .= (!empty($categoryObjectData) ? "-".seo::url_slug($categoryObjectData[0]->name) : "").(!empty($subCategoryObjectData) ? "-".seo::url_slug($subCategoryObjectData[0]->name) : "");

						$generalenTitle = (!empty($categoryObjectData) ? $categoryObjectData[0]->name : "").
										  (!empty($subCategoryObjectData) ? " | ".$subCategoryObjectData[0]->name : "");
						
						if ($generalenTitle == "")
						{
							$generalenTitle = "kupinapopust.mk";
						}
						else
						{
							$generalenTitle .= " | kupinapopust.mk";
						}
					
                        //vo slucaj da e odbrana nekoja kategorija
                        if($category_id > 0 && $type == "")
                        {
                                //inicijalizacija na modeli
                                $subCategoriesModel = new Subcategories_Model();

                                $subCategoriesArray = $subCategoriesModel->getAllSubCategoriesWithOffers($category_id);

                                $this->template->content->subCategoriesArray = $subCategoriesArray;
                        }
                }
                $this -> template -> content -> dateTimeUnique = $dateTimeUnique;
		$this -> template -> title = $generalenTitle;
		
		$this -> template -> share_dialog_show = true;
		$this -> template -> bridgeName = "CategoriesHandlerDeal";
		
		// OMILENI
		// zemanje na 'cookie'
		$cookie_value = cookie::get("kupinapopust_fav");
		if ( ! empty($cookie_value)) {
			$favDeals = json_decode($cookie_value, true);
		} else {
			$favDeals = array();
		}
		$this->template->content->favDeals = $favDeals;
		// END - OMILENI

        // SHOPPING CART
        // zemanje na 'cookie'
        $cookie_value_shop_cart = cookie::get("kupinapopust_shop_cart");

        if ( ! empty($cookie_value_shop_cart)) {
            $shopCartDeals = json_decode($cookie_value_shop_cart, true);
        } else {
            $shopCartDeals = array();
        }
        $this->template->content->shopCartDeals = $shopCartDeals;
        // END - SHOPPING CART

	}

	public function index($mode = 0) {
		//new Profiler;
		$this -> template -> content = new View('alloffers/filter');
		$this -> template -> title = kohana::lang("prevod.website_title");

		//inicijalizacija na modeli
		$dealsModel = new Deals_Model();
		$categoriesModel = new Categories_Model() ;
		$subCategoriesModel = new Subcategories_Model();
		

        if($mode == 0)//mode = 0 (odnosno koga ne se prosleduva mode) => gi dava SITE ponudi
        {
            $scrollPageTitle = "Сите понуди" ;
        }
        elseif($mode == 1)//mode = 1 => gi dava NAJNOVITE ponudi
        {
            $scrollPageTitle = "Најнови" ;
        }
        elseif($mode == 2)//mode = 2 => gi dava NAJPRODAVANITE ponudi
        {
            $scrollPageTitle = "Најпродавани" ;
        }
        elseif($mode == 3)//mode = 3 => gi dava ponudite koi se PRI KRAJ
        {
            $scrollPageTitle = "При крај" ;
        }
        elseif($mode == 4)//mode = 4 => gi dava ponudite po NAJNISKA CENA
        {
            $scrollPageTitle = "Најниска цена" ;
        }
        elseif($mode == 5)//mode = 5 => gi dava ponudite po POJVISOKA CENA
        {
            $scrollPageTitle = "Највисока цена" ;
        }
        elseif($mode == 6)//mode = 6 => gi dava ponudite po Бесплатни купони
        {
            $scrollPageTitle = "Бесплатни купони" ;
        }
        elseif($mode == 7)//mode = 6 => gi dava ponudite po Купон + доплата
        {
            $scrollPageTitle = "Купон + доплата" ;
        }
        elseif($mode == 8)//mode = 6 => gi dava ponudite po Ваучери
        {
            $scrollPageTitle = "Ваучери" ;
        }

        $dateTimeUnique = date("Y-m-d H:i:s");

        // KOD KOJ STO VADI PODATOCI OD BAZA ZA PRIKAZ NA PONUDI
        $bottomOffersPagesCnt = ceil($dealsModel->getAllControllerCnt($mode, $dateTimeUnique) / $this->pagination_settings['items_per_page']);      
        $bottomOffers = $dealsModel->getAllController($mode, 0, $this->pagination_settings['items_per_page'], $dateTimeUnique);
		
		$categoriesData = $categoriesModel->getCategories();
		$subCategoriesData = $subCategoriesModel->getSubcategoriesbyParentid(0);
		
		//kreiranje na variabli
		$this->template->content->bottomOffers = $bottomOffers;
		$this -> template -> content -> mode = $mode;

		$this->template->content->categoriesData = $categoriesData ;
		$this->template->content->subCategoriesData = $subCategoriesData ;
        $this->template->content->scrollPageTitle = $scrollPageTitle;

        $this->template->content->dateTimeUnique = $dateTimeUnique;
        $this->template->content->bottomOffersPagesCnt = $bottomOffersPagesCnt;
		
		// OMILENI
		// zemanje na 'cookie'
		$cookie_value = cookie::get("kupinapopust_fav");
		if ( ! empty($cookie_value)) {
			$favDeals = json_decode($cookie_value, true);
		} else {
			$favDeals = array();
		}
		$this->template->content->favDeals = $favDeals;
		// END - OMILENI

        // SHOPPING CART
        // zemanje na 'cookie'
        $cookie_value_shop_cart = cookie::get("kupinapopust_shop_cart");

        if ( ! empty($cookie_value_shop_cart)) {
            $shopCartDeals = json_decode($cookie_value_shop_cart, true);
        } else {
            $shopCartDeals = array();
        }
        $this->template->content->shopCartDeals = $shopCartDeals;
        // END - SHOPPING CART
            
	}
        
    public function buildAjaxHtml($allPaginationOffers = ""){
        $html = "";
                    
        $all_num = 0;
        foreach ($allPaginationOffers as $singleDeal) {
            $all_num++;

            //se pravi logika za vremeto da se prikaze
            $timeLeft = strtotime($singleDeal->end_time) - time();
            $timeStart = strtotime($singleDeal->start_time);
            $timeEnd = strtotime($singleDeal->end_time);
            $timeNow = time();

            $html .= '<div class="deal-item" id="'. $singleDeal->id .'">';
            $html .=   '<img src="/pub/deals/'. $singleDeal->side_img .'" alt="'. strip_tags($singleDeal->title_mk_clean) .'"/>
                            <h3>'. commonshow::Truncate(strip_tags($singleDeal->title_mk_clean)) .'</h3>';

            $html .= '<div class="item-description">';
            $html .= '<span class="price">';

            if ($singleDeal->tip == 'cena_na_vaucer')
                $html .= $singleDeal->price_voucher;
            else
                $html .= $singleDeal->price_discount;

            $html .= ' ден.</span>';
            $html .= '<small class="today">'. commonshow::staticCountDown(strtotime($singleDeal->start_time), strtotime($singleDeal->end_time)) .'</small>'; 
            $html .= '<span class="icon customers" style="color:  #333; font-weight: bold">';

            $count = 0;
            if (isset($voucherCount[$singleDeal->id]))
                $count = $voucherCount[$singleDeal->id];

            $allDealsTooltipTxt = commonshow::tooltip($count, $singleDeal->min_ammount, $singleDeal->max_ammount);

            $html .= '<span class="buyers-count" title="'. $allDealsTooltipTxt .'">';
			if($count > 0)
			{
				
				$html .= commonshow::image($count, $singleDeal->max_ammount);

				if (commonshow::isSoldOut($count, $singleDeal->max_ammount))
						$html .= "<span style='color: red'>";

				$html .= commonshow::number($count, $singleDeal->min_ammount, $singleDeal->max_ammount);
//                            $html .= commonshow::text($count);

					if (commonshow::isSoldOut($count, $singleDeal->max_ammount))
						$html .= "</span>";
			}
            $html .= '</span>
                    </span>';

            if ($singleDeal->price > 0) {
                $html .= '<span class="discount">-';
                $html .= '<strong>'. (int) round(100 - ($singleDeal->price_discount / $singleDeal->price) * 100) .'</strong>%'; 
                $html .= '</span>';
            }

             if (commonshow::newoffer(strtotime($singleDeal->start_time))) {
                $html .= '<span class="newoffer">нова</span>';
            }

            $html .= ' <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>';
        }
        
        return $html;
    }
	
	public function __call($method, $arguments) {
		$this -> auto_render = FALSE;
		echo 'Ne postoi vakva strana!!!';
	}



}
