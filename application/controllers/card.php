<?php defined('SYSPATH') OR die('No direct access allowed.');

class Card_Controller extends Default_Controller {

    public function index($deal_id = null) {
        $this->template->content = new View('card/index');
        $this->template->title = kohana::lang("prevod.website_title");
    }
    public function activate($deal_id = null) {
        $this->template->content = new View('card/activate');
        $this->template->title = kohana::lang("prevod.website_title");
        $type = 1;
        
        if (request::method() == 'post') {
            $post = $this->input->post();
            $username = $post['Email'];
            $usernameGift = $post['Email-gift'];
            
            $code = $post['code'];
			
			if($username == "" || $code == "")
			{
				$this->template->content->error = 'Почитувани,<br>полињата Вашиот Е-mail и Единствен код мора да бидат внесени!';
			}
			else
			{
				
				$adminModel = new Points_Model();
				$where = array('email' => $username, 'code' => $code,'status'=>'active');
				$pointsData = $adminModel->getGiftCard($where);
				if (count($pointsData) > 0) {
					if ($usernameGift) {
						$username = $usernameGift;
						$type = 2;
					}
					$adminModel->activateGiftCard($username, $pointsData);
					url::redirect("/card/success/$type");
				} else {
					$this->template->content->error = 'Почитувани,<br>настаната е една од следните грешки:<br>- E-mail адресата што ја внесовте не соодејствува со кодот<br>- Кодот што го внесовте е веќе искористен<br>- Кодот што го внесовте е погрешен<br>';
				}
			}
        }
    }
    
    public function success($type = 1) {
        $this->template->content = new View('card/success') ;
        $this->template->title = kohana::lang("prevod.website_title") ;
        $this->template->content->type = $type ;
    }
    
    public function instructions($type = 1) {
        $this->template->content = new View('card/instructions') ;
        $this->template->title = kohana::lang("prevod.website_title") ;
    }

    public function __call($method, $arguments) {
        $this->auto_render = FALSE;
        echo Router::$current_uri;
    }

}