<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Wheel_Of_Fortune_Controller extends Default_Controller {

    public $kakoDoPovekjePoeniText = '';

    public function __construct() {

        return 0;

        $this->kakoDoPovekjePoeniText = '
				<strong>Како до повеќе "kupinapopust" поени?</strong>
				<p>
				<span style="color:#ffa500;">• </span>При секое купување <strong>единствено</strong> преку "kupinapopust" добивате поeни кои можете да ги искористите за купување на некоја од нашите понуди.</p>
				<p>
				<span style="color:#ffa500;">• </span>Внесете го датумот на Вашиот следен роденден во "Корисничкиот профил" и ние ќе ви подариме <strong>50 поени (50 ден.)</strong> на тој ден.</p>
				<p>
				<span style="color:#ffa500;">• </span>Сликајте се при искористување на понудата и Ние ќе ви подариме <strong>50 поени (50 ден.) кредит</strong>. 
				</p>
				<p>
				<span style="color:#ffa500;">• </span>Завртете го тркалото на среќата и освојте вредни "kupinapopust" поени.
				</p>
		';

        parent::__construct();
    }

    public function index() {
        return 0;

        //osnovni setiranja za stranata
        $this->template->title = kohana::lang("prevod.website_title");
        $this->template->content = new View('wheel_of_fortune/index');

        //kreiranje na modeli
        $wfPriceFundModel = new Wf_Prize_Fund_Model();
        $wfPlayers = new Wf_Players_Model();

        //povikuvanje na funcii
        $order = 'prize';
        $prizeFund = $wfPriceFundModel->getPrizeFundOrderBy($order);
        $cntPlayersToday = $wfPlayers->getCntPlayersToday();
        $previousDayWinners = $wfPlayers->getPreviousDayWinners();
        $cntWinnersToday = $wfPlayers->getCntWinnersToday();
        $dailyPrizeFund = $wfPriceFundModel->getDailyPrizeFund();
        $cntWinnerspreviousDay = count($previousDayWinners);

        //facebook social tools
		$this->template->set_global("fb_image", Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL') . "/pub/img/trkalo.jpg");
        $this->template->set_global("fb_title", "Тркало на среќата");
		$this->template->set_global("fb_link",  Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL') . "/wheel_of_fortune");
        $this->template->set_global("fb_description", "Завртете и освојте 'kupinapopust' поени");
        $this->template->set_global("fb_redirect_addition", "wheel_of_fortune");

        //proveri dali e logiran user-ot i dali izigral denes
        $userID = $this->session->get("user_id");
        $userType = $this->session->get("user_type");
        $facebook_id = $this->session->get("login_value");
		$current_login_type = $this->session->get("current_login_type");

        $showGame = false;
        $hasUserPlayedToday = false;

		if ($userType == 'customer' && $userID > 0 && $facebook_id != "" && $current_login_type == "facebook") {
            $hasUserPlayedToday = $wfPlayers->hasUserPlayedToday($userID);

            if (!$hasUserPlayedToday) {


                //zapisi vo baza deka izigral
                $wfPlayers->savePlayer($userID);

                //zapisi na Facebook deka ja igral igrata
                //$this->FacebookPostTodayPlayed();
            }

            //prikazi ja igrata
            $showGame = true;
        }

        //variable-i okolu datum-ot za da moze da se setira dali da bide RANDOM ili nedobitno
        $currentDayOfMonth = date("j");
        $ssm = 0;

        //dali dobil vo poslednite X denovi
        //ako dobil togas podolu setirame da ne dobie
        $hasUserWonDaysAgo = false;
        $hasUserWonDaysAgo = $wfPlayers->hasUserWonDaysAgo($userID, 15);

        //dokolku dobil pred X devovi
        //isto taka, dokolku momentalniot den od mesecot e pogolem od 28 => stavi brzina (ssm) sto ke napravi da zastane nanedobitno ili "svrti uste ednas" pole
        //isto taka, dokolku brojot na dobitnici vo denot stignal do brojkata na dnevniot fond na nagradi 
        //=> setiraj da ne zastane na dobitno pole
        if ($hasUserWonDaysAgo || $currentDayOfMonth > 28 || $this->hasReachedTodayWinnersLimit($cntWinnerspreviousDay, $cntWinnersToday, $dailyPrizeFund, $currentDayOfMonth))
            $ssm = rand(3, 6);
        

        // if($userID == 8383)
        //     $ssm = 1;
        // else
        //     $ssm = rand(3, 6);

        //odberi domain za flash-ot
        $domain_protocol = Kohana::config('facebook.facebookSiteProtocol');
        $domain_name = Kohana::config('facebook.facebookSiteURL');

        //kreiranje na query string za flash-ot
        $wf_qs = "domain_protocol=".$domain_protocol."&domain_name=" . $domain_name;

        if ($ssm != 0)
            $wf_qs .= "&ssm=" . $ssm;


        //kreiranje na output promenlivi
        $this->template->content->prizeFund = $prizeFund;
        $this->template->content->wf_qs = $wf_qs;
        $this->template->content->cntPlayersToday = $cntPlayersToday;
        $this->template->content->previousDayWinners = $previousDayWinners;
        $this->template->content->showGame = $showGame;
        $this->template->content->hasUserPlayedToday = $hasUserPlayedToday;
        $this->template->content->kakoDoPovekjePoeniText = $this->kakoDoPovekjePoeniText;
    }

    //metoda koja se povikuva koga trkaloto ke zastane na DOBITNO pole
    private function hasReachedTodayWinnersLimit($previousDayWinnersCnt = 0, $todayWinnersCnt = 0, $dailyPrizeFund = 0, $dayOfMonth = 0) {

        return 0;

        $retVal = false;
        $todayWinnersCntCalibration = $todayWinnersCnt;

        //ako e 28 den od denot ke simulirame deka sme dali edna nagrada plus prethodniot den za da imame edna nagrada za sekoj slucaj.
        if ($dayOfMonth == 28)
            $previousDayWinnersCnt++;

        //ako prethodniot den sme dali povekje nagradi od predvidenoto
        if ($previousDayWinnersCnt > $dailyPrizeFund) {
            //kolku pobednici imame povekje od dnevniot fond na nagradi
            $cntOverWinners = $previousDayWinnersCnt - $dailyPrizeFund;

            $todayWinnersCntCalibration += $cntOverWinners;
        }

        //proverka dali da ne davame vekje nagradi
        if ($todayWinnersCntCalibration >= $dailyPrizeFund)
            $retVal = true;

        return $retVal;
    }

    //metoda koja se povikuva koga trkaloto ke zastane na DOBITNO pole
    public function win() {

        //PRIVREMENO DODEKA SE NAPRAVI NOVA VERZIJA OD IGRATA
        return 0;

        //proverka dali e post request I dali requestot doagja od tocniot referrer
        if (request::method() == 'post' && strpos(request::referrer(), Kohana::config('facebook.facebookSiteURL') . "/wheel_of_fortune")) {

            //proveri dali e tocna bezbednosata promenliva. 
            //Toa pokazuva deka go koristi swf-to koe e na nasata strana
            //Ako e se vo red, pusti go nas stranata
            if ($_POST['userwin'] == 'KupiNaPopustWinner2014!') {

                //prezemi user podatoci
                $userID = $this->session->get("user_id");
                $userType = $this->session->get("user_type");
                $facebook_id = $this->session->get("login_value");
                $userFBmail = $this->session->get("user_email");

                //proveri dali e logiran user-ot i dali izigral denes
                if ($userType == 'customer' && $userID > 0 && $facebook_id != "") {

                    //osnovni setiranja za stranata
                    $this->template->title = kohana::lang("prevod.website_title");
                    $this->template->content = new View('wheel_of_fortune/win');

                        //oderduvanje dobivka
                        $prize = $this->getPrize();

                        $this->template->content->prize = $prize;
                        $this->template->content->userFBmail = $userFBmail;

                        $this->session->set('userwinPrepis', 'KupiNaPopustWinner2014!PrepisNaPoeni');
                        $this->session->set('prize', $prize);

                    //kreiranje na output promenlivi
                    $this->template->content->kakoDoPovekjePoeniText = $this->kakoDoPovekjePoeniText;
                } else {
                    url::redirect("/customer/login");
                }//ako ne e logiran
            } else {
                url::redirect("/wheel_of_fortune");
            }//ako bezbednosnata promenliva ne e tocna
        } else {
            url::redirect("/wheel_of_fortune");
        }//ako requestite ne se tocni
    }

    public function point_assigned() {

        return 0;
        
        //proverka dali e post request I dali requestot doagja od tocniot referrer
        if (request::method() == 'post' && strpos(request::referrer(), Kohana::config('facebook.facebookSiteURL') . "/wheel_of_fortune/win")) {

            //prezemi user podatoci
            $userID = $this->session->get("user_id");
            $userType = $this->session->get("user_type");
            $userwinPrepis = $this->session->get("userwinPrepis");
            $prize = $this->session->get("prize");
            //prezemi go email-ot od megucekorot
            $user_email = $_POST['user_email'];
            
            //proveri dali e tocna bezbednosata promenliva. 
            //Toa pokazuva deka go koristi swf-to koe e na nasata strana
            //Ako e se vo red, pusti go nas stranata
            if ($userwinPrepis == 'KupiNaPopustWinner2014!PrepisNaPoeni') {

                //kreiranje na modeli
                $wfPlayers = new Wf_Players_Model();
                $Points = new Points_Model();
                $customerModel = new Customer_Model();

                //proveri dali e logiran user-ot i dali izigral denes
                if ($userType == 'customer' && $userID > 0) {

                    //osnovni setiranja za stranata
                    $this->template->title = kohana::lang("prevod.website_title");
                    $this->template->content = new View('wheel_of_fortune/point_assigned');

                   //proverka dali denes user-ot vekje dobil nagrada
                    $CheckPlayerForToday = $wfPlayers->hasUserwinnToday($userID);
                    if (count($CheckPlayerForToday) > 0) {//proveruvame dali vekje dobil vo tekonvniot den
                        //na refresh na stranata dokolku veke igral
                        url::redirect("/wheel_of_fortune");
                    } else {

                        //proverka dali mailot veke postoi
                        $customerID = $customerModel->getCustomerID($user_email);
                        if (!$customerID) {//ako ne postoi se kreira nov customer
                            $customerID = $customerModel->createCustomer($user_email);
                        }
                       
                        //zapisi vo baza (wf_players) kolku dobil i deka e dobitnik
                        $wfPlayers->saveWinnerPlayer($userID, $prize, $user_email);
                        
                        //zapisi vo baza poenite na korisnikot i pricinata od koja gi dobil;
                        $reason = '- Подарок поени од играта "тркало на среќа"';
                        $Points->savePoints($customerID, $prize, $reason);
                        
                        //prezemi info od BAZA za toa do koga mozat da se iskoristat dobienite poeni
                        $TodayDate = date("Y-m-d");
                        $Get_endDatePoints = $Points->getExpiringDateOfJustAddedPoints($customerID, $TodayDate);
                        list($date_tmp, $time_tmp) = explode(' ', $Get_endDatePoints[0]->ends);
			            list($year_tmp, $month_tmp, $day_tmp) = explode('-', $date_tmp);
                        $endDatePoints = "$day_tmp-$month_tmp-$year_tmp";
                                
                        //prati email na dobitnikot
                        $this->SendEmailWinner($user_email, $prize, $endDatePoints);

                        //izbrisi gi soodvetnite sesiski promenlivi za da ne napravi pak RELOAD pa pak da mu se zapisat poeni
                        $this->session->delete("userwinPrepis");
                        $this->session->delete("prize");
                    }
                    
                    //kreiranje na output promenlivi
                    $this->template->content->prize = $prize;
                    $this->template->content->user_email = $user_email;
                    $this->template->content->kakoDoPovekjePoeniText = $this->kakoDoPovekjePoeniText;

                } else {
                    url::redirect("/customer/login");
                }//ako ne e logiran
            } else {
                url::redirect("/wheel_of_fortune");
            }//ako bezbednosnata promenliva ne e tocna
        } else {
            url::redirect("/wheel_of_fortune");
        }//ako requestite ne se tocni
    }

    //metoda koja se povikuva za da odredime koja nagrada da mu se dodeli na dobitnikot
    private function getPrize() {

        return 0;


        //lokalni variabli
        $retVal = 0;
        $order = 'prize';

        //kreiranje na modeli
        $wfPlayers = new Wf_Players_Model();
        $wfPriceFundModel = new Wf_Prize_Fund_Model();

        //povikuvanje na f-cii
        $allPrizes = $wfPriceFundModel->getPrizeFundOrderBy($order);
        $arrCntWinnerByPriceCurrentMonth = $wfPlayers->getCntWinnerByPriceCurrentMonth();

        //odreduvanje nagrada
        $allPrizesIDs = array();

        $countallPrizes = count($allPrizes);
        //var_dump($allPrizes);

        $last_elemet_id = -1;
        for ($i = 0; ($i) < $countallPrizes; $i++) {
            //ako do sega nema dobitnici od odredena nagrada, togas ke nema zapis za nea vo array-ot. zatoa za taa nagrada setirame vrednost 0
            if (!isset($arrCntWinnerByPriceCurrentMonth[$allPrizes[$i]->prize]))
                $arrCntWinnerByPriceCurrentMonth[$allPrizes[$i]->prize] = 0;

            if ($allPrizes[$i]->quantity > $arrCntWinnerByPriceCurrentMonth[$allPrizes[$i]->prize]) {
                $prizesArray[] = $allPrizes[$i]->prize;
                $last_elemet_id = $i;
            }
        }

        if ($last_elemet_id > -1) {
            $addLastElement = $allPrizes[$last_elemet_id]->prize;
            array_push($prizesArray, $addLastElement, $addLastElement, $addLastElement);
        }

        $countprizesArray = count($prizesArray);
        $randNUmber = rand(0, ($countprizesArray - 1));

        //odreduvanje dobivka
        $retVal = $prizesArray[$randNUmber];

        return $retVal;
    }

   //  private function FacebookPostTodayPlayed() {
   //      $appId = Kohana::config('facebook.facebookAppID');
   //      $appSecret = Kohana::config('facebook.facebookAppSecret');

   //      $facebook = new Facebook(array(
   //          'appId' => $appId,
   //          'secret' => $appSecret,
   //      ));

   //      $user_id = $facebook->getUser();

   //      if ($user_id) {

   //          // We have a user ID, so probably a logged in user.
   //          // If not, we'll get an exception, which we handle below.
   //          try {
   //              $ret_obj = $facebook->api('/me/feed', 'POST', array(
   //                  'link' => 'www.kupinapopust.mk',
   //                  'message' => 'Ја игравте нашата игра - Тркало на среќата'
   //              ));
   //              //echo '<pre>Post ID: ' . $ret_obj['id'] . '</pre>';
   //              // Give the user a logout link 
   //              //echo '<br /><a href="' . $facebook->getLogoutUrl() . '">logout</a>';
   //          } catch (FacebookApiException $e) {
   //              // If the user is logged out, you can have a 
   //              // user ID even though the access token is invalid.
   //              // In this case, we'll get an exception, so we'll
   //              // just ask the user to login again here.
			// 	$login_url = $facebook->getLoginUrl(array('scope' => 'publish_actions'), 'v2.3');
   //              //echo 'Please <a href="' . $login_url . '">login.</a>';
   //              //error_log($e->getType());
   //              //error_log($e->getMessage());
   //          }
   //      } else {

   //          // No user, so print a link for the user to login
   //          // To post to a user's wall, we need publish_actions permission
   //          // We'll use the current URL as the redirect_uri, so we don't
   //          // need to specify it here.
			// $login_url = $facebook->getLoginUrl(array('scope' => 'publish_actions'), 'v2.3');
   //          //echo 'Please <a href="' . $login_url . '">login.</a>';
   //      }

   //      //die();
   //  }

    private function SendEmailWinner($to = "", $prize = "", $endDatePoints = "") {

        return 0;
        
        $messageSend = false;
        
        //kreiraj modeli		
        $dealsModel = new Deals_Model();
        $mailContentModel = new Emailrenderer_Model();

        //za sega ne se koristi
        //$allPrimary = $dealsModel->getAllPrimary();
        
        $allPrimary = array();

        //kreiraj array so primarniot i stranicnite ponudi
        $maildata = array(
            'allPrimary' => $allPrimary,
            'prize' => $prize,
            'endDatePoints' => $endDatePoints
            );

        $mailContent = $mailContentModel->render("wheel_of_fortune_winner", $maildata);
        $recipient_arr = array("to" => $to);
        $email_config = Kohana::config('email');

        $from = array($email_config['from']['email'], $email_config['from']['name']);
        $result = email::send($recipient_arr, $from, 'Честитаме! Добивте ' . $prize . ' поени.', $mailContent, true);
        if ($result == true) {
            $messageSend = true;
        }
    }

    public function __call($method, $arguments) {
        return 0;
        $this->auto_render = FALSE;
        echo 'This text is generated by __call. If you expected the index page, you need to use: welcome/index/' . substr(Router::$current_uri, 8);
    }

}
