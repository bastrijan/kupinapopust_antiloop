<?php defined('SYSPATH') OR die('No direct access allowed.');

class Lyoness_Redirect_Controller extends Default_Controller {
	
    public function __construct() {
        parent::__construct();
    }
 
    public function index()
	{	
		$this->auto_render = FALSE;

		//zemi gi variablite od GET-ot
		$get = $this->input->get();

		/*************KODOT OD LYONESS********/
		
		// Default landing page
		//
		$defaultUrl = Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/";
		
		// The domain under which this script is installed
		//
		$domain = Kohana::config('facebook.facebookSiteURL');

		if (!empty($get["tduid"]))
		{
			$cookieDomain = "." . $domain;
			setcookie("TRADEDOUBLER", $get["tduid"], (time() + 3600 * 24 * 365), "/", $cookieDomain);
			
			// If you do not use the built-in session functionality in PHP, modify
			// the following expression to work with your session handling routines.
			//
			$_SESSION["TRADEDOUBLER"] = $get["tduid"];
		}

		if (empty($get["url"]))
			$url = $defaultUrl;
		else
			$url = urldecode(substr(strstr($_SERVER["QUERY_STRING"], "url"), 4));

		header("Location: " . $url);

    }
	
    public function __call($method, $arguments) {
        // Disable auto-rendering
        $this->auto_render = FALSE;
        echo 'page not found';
    }
}