<?php

defined('SYSPATH') OR die('No direct access allowed.') ;

class Acategories_Controller extends Default_Controller {

  public function __construct() {
    $this->template = 'layouts/admin' ;
   
    parent::__construct() ;
  }

   public function index() {
		
		//template
		$this->template->title = "Категории" ;
		$this->template->content = new View("/admin/acategories") ;
		
		$categoriesModel = new Categories_Model() ;
		
		$categoriesData = $categoriesModel->getData() ;
		$dealsByCategoryData = $categoriesModel->getNumDealsByCategories() ;
		
		$this->template->content->categoriesData = $categoriesData;
		$this->template->content->dealsByCategoryData = $dealsByCategoryData;
	}
	
	public function izbrisi() {
		
		//nema output
		$this->auto_render = false ;
		
		//get variabli
		$id = $this->input->get("id", 0);
		
		$categoriesModel = new Categories_Model() ;
		
		if ($id)
			$categoriesModel->delete($id);
		
		url::redirect("/acategories") ;
	}
	
	public function uredi() {
		
		$this->template->title = "Категории, подесување" ;
		$this->template->content = new View("/admin/urediacategories") ;
		
		//get variabli
		$id = $this->input->get("id", 0);
		
		$categoriesModel = new Categories_Model() ;
		
		if (request::method() == 'post') {
			//should save then
			$post = $this->input->post() ;
			$files = $this->input->files();
			
			$insertID = $categoriesModel->saveData($post);
			
			if (isset($post['id']) && $post['id']) {
				$insertID = $post['id'] ;
			}



			
			url::redirect("/acategories");
		}
		
		if ($id) {
			//edit
			$where = array('id' => $id) ;
			$categoriesData = $categoriesModel->getData($where) ;
			$this->template->content->categoriesData = $categoriesData;
		}
		else {
			// new, just render the form
		}
		
	}
	
    public function podkategorii() {
		
		$this->template->title = "Поткатегории, подесување" ;
		$this->template->content = new View("/admin/subcategories") ;
		
		//get variabli
		$Parent_id = $this->input->get("parent_id", 0);
		
		$categoriesModel = new Categories_Model();
                $subcategoriesModel = new Subcategories_Model();
                
                $dealsBySubcategoryData = $subcategoriesModel->getNumDealsBySubcategories($Parent_id);
//                var_dump($dealsBySubcategoryData);
                
		if ($Parent_id) {
			//Get subcategories by parent id
			$where = array('id' => $Parent_id);
			$ParentCatData = $categoriesModel->getData($where) ;
                        
                        $where_sub = array('category_id' => $Parent_id);
                        $SubCatData = $subcategoriesModel->getData($where_sub);
                        
                        
                        $this->template->content->ParentCatData = $ParentCatData;
			$this->template->content->SubCatData = $SubCatData;
                        $this->template->content->dealsBySubcategoryData = $dealsBySubcategoryData;
                        
                        
		}
		else {
			// new, just render the form
		}
		
	}
        
        
    public function uredi_podkategorija() 
    {
		
		$this->template->title = "Поткатегории, подесување" ;
		$this->template->content = new View("/admin/uredisubcategories") ;
		
		//get variabli
		$Sub_id = $this->input->get("id", 0);
                $Parent_id = $this->input->get("parent_id", 0);
		
		$categoriesModel = new Categories_Model() ;
		$subcategoriesModel = new Subcategories_Model();

                $where = array('id' => $Parent_id);
                $ParentCatData = $categoriesModel->getData($where) ;
                $this->template->content->ParentCatData = $ParentCatData;
                
		if (request::method() == 'post') {
			//should save then
			$post = $this->input->post() ;
                        
			$subcategoriesModel->saveData($post) ;
			url::redirect("/acategories/podkategorii?parent_id=$Parent_id") ;
		}
		
		if ($Sub_id) {
			//edit
			$where = array('id' => $Sub_id) ;
			$SubCatData = $subcategoriesModel->getData($where) ;
			$this->template->content->SubCatData = $SubCatData;
		}
		else {
			// new, just render the form
                        if($Parent_id){
                            $this->template->content->Parent_id = $Parent_id;
                        }
		}
		
	}
        
        public function izbrisi_podkategorija() {
		
		//nema output
		$this->auto_render = false ;
		
		//get variabli
		$id = $this->input->get("id", 0);
		$Parent_id = $this->input->get("parent_id", 0);
		$subcategoriesModel = new Subcategories_Model();
		
		if ($id)
			$subcategoriesModel->delete($id);
		
		url::redirect("/acategories/podkategorii?parent_id=$Parent_id") ;
	}
        
        //ajax
    public function prezemi_podkategorii(){
        
        //nema output
        $this->auto_render = false ;
        
        if (request::method() == 'post') {
            
            $post = $this->input->post();
            $parent_id = $post['parent_id'];
            
            $subcategoriesModel = new Subcategories_Model();
            $where = array('category_id' => $parent_id) ;
            $SubCatData = $subcategoriesModel->getData($where) ;
            
            $html = "<option>Избери поткатегорија</option>";
            
            if(!empty($SubCatData)){
                foreach ($SubCatData as $subcategory){
                    $html .= "<option value='$subcategory->id' >$subcategory->name</option>";
                }
                    
                
            }
            $arr = array('status' => 'success', 'html'=> $html);
            die(json_encode($arr));
        }
        
        $arr = array('status' => 'fail', 'html'=> "");
        die(json_encode($arr));
        
    }
	



	
  public function __call($method, $arguments) {
    // Disable auto-rendering
    $this->auto_render = FALSE ;
    echo 'page not found' ;
  }



}