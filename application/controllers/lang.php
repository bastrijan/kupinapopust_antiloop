<?php defined('SYSPATH') OR die('No direct access allowed.');

class Lang_Controller extends Default_Controller {

    public function mk() {
        $session = Session::instance();
        $session->set("lang", "mk");
        Kohana::config_set("locale.language", "mk_MK");
        if ( request::referrer())
        url::redirect( request::referrer());
        url::redirect("/");
    }
    public function en() {
        $session = Session::instance();
//        $session->set("lang", "en");
        $session->set("lang", "mk");
//        Kohana::config_set("locale.language", "en_US");
        Kohana::config_set("locale.language", "mk_MK");
        if ( request::referrer())
        url::redirect( request::referrer());
        url::redirect("/");
    }


    public function __call($method, $arguments) {
        $this->auto_render = FALSE;
        echo 'page not found';
    }

}