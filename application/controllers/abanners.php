<?php

defined('SYSPATH') OR die('No direct access allowed.') ;

class Abanners_Controller extends Default_Controller {

  public function __construct() {
    $this->template = 'layouts/admin' ;
   
    parent::__construct() ;
  }

   public function index() {
		
		//template
		$this->template->title = "Банер систем" ;
		$this->template->content = new View("/admin_banners/index") ;
		
		$bannersModel = new Banners_Model() ;
		
		$where = array('id >= ' => 0) ;
		$orderBy = array('banner_location' => 'ASC') ;
		$bannersData = $bannersModel->getData($where, $orderBy) ;
		
		$this->template->content->bannersData = $bannersData;
	}

	public function izbrisi() {
		
		//nema output
		$this->auto_render = false ;
		
		//get variabli
		$id = $this->input->get("id", 0);
		
		
		
		if ($id)
		{
			$bannersModel = new Banners_Model() ;

			//zemi go zapisot od baza
			$where = array('id' => $id) ;
			$bannersData = $bannersModel->getData($where) ;

			//izbrisi ja slikata
			$banner_img = $bannersData[0]->id.'.'.$bannersData[0]->banner_ext;
			unlink(DOCROOT . 'pub/img/header_banner/'.$banner_img);

			//izbrisi go zapisot od baza
			$bannersModel->delete($id);

		}
		
		url::redirect("/abanners") ;
	}
	
	public function uredi() {
		
		$this->template->title = "Банер систем, подесување" ;
		$this->template->content = new View("/admin_banners/uredi") ;
		
		//get variabli
		$id = $this->input->get("id", 0);
		
		$bannersModel = new Banners_Model() ;
		
		if (request::method() == 'post') {
			//should save then
			$post = $this->input->post() ;
			$files = $this->input->files() ;

			//ZEMI JA EXTENZIJATA NA SLIKATA
	        $banner_ext = "";

			if (isset($files["main_picture"]) and $files["main_picture"]['name']) 
			{
				$imgInfo = pathinfo($files["main_picture"]['name']) ;

				$banner_ext = $imgInfo['extension'];
			}

			//ZACUVAJ GI SITE INFORMACII OD FORMATA OSVEN SLIKATA
			if($banner_ext != "")
				$post["banner_ext"] = $banner_ext;
			
			$insertID = $bannersModel->saveData($post);
			
			//dokolku e EDIT togas $insertID se polni preku POST variablata
			if (isset($post['id']) && $post['id']) {
				$insertID = $post['id'] ;
			}



			//ZACUVAJ JA SLIKATA
	        $banner_img = "";
			
			if (isset($files["main_picture"]) and $files["main_picture"]['name']) 
			{

				$banner_img = $insertID.'.'.$banner_ext;

				$filename = upload::save($files["main_picture"], $banner_img, DOCROOT . 'pub/img/header_banner/') ;

			}

			

			
			url::redirect("/abanners");
		}
		
		if ($id) {
			//edit
			$where = array('id' => $id) ;
			$bannersData = $bannersModel->getData($where) ;
			$this->template->content->bannersData = $bannersData;
		}
		else {
			// new, just render the form
		}
		
	}
	

    
     
  public function __call($method, $arguments) {
    // Disable auto-rendering
    $this->auto_render = FALSE ;
    echo 'page not found' ;
  }



}