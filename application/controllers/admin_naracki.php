<?php
defined('SYSPATH') OR die('No direct access allowed.') ;

class Admin_Naracki_Controller extends Default_Controller 
{
	private static	$otkupStatusiArr = array(
			0 => "Нова",
			1 => "Во процес на подготовка",
			2 => "Испратено по пошта / Корисникот информиран да ја подигне",
			3 => "Комплетирана",
			10 => "Откажана"
		);

	private static 	$narackaTipArr = array(
			"" => "Сите",
			"otkup" => "Откуп",
			"card" => "Плат. картичка",
			"cache" => "Во готово",
			"bank" => "Уплатница"
		);

	private static 	$otkupPrevzemanjeArr = array(
			1 => "Подигање од канцелариите на kupinapopust",
			2 => "Достава до наведена адреса"
		);

	public function __construct() 
	{

		ini_set('max_execution_time', 600); //600 seconds = 10 minutes=

		$this->template = 'layouts/admin' ;

		parent::__construct() ;
	}

	public function index() 
	{

		//sistemski setiranja
		$this->template->title = "Admin - Нарачки" ;
		$this->template->content = new View("admin_naracki/index") ;

		//zemi gi get variablite od formata
		$deal_filter_submit = $this->input->get("deal_filter_submit", '');
		$naracki_tip = $this->input->get("naracki_tip", '');
		$naracka_datum = $this->input->get("naracka_datum", date('Y-m-d'));

		$naracka_email = $this->input->get("naracka_email", '');
		$naracka_ref = $this->input->get("naracka_ref", '');

		$otkup_administracija_status = $this->input->get("otkup_administracija_status", -1);
		$otkup_ime_prezime = $this->input->get("otkup_ime_prezime", '');
		$otkup_adresnica = $this->input->get("otkup_adresnica", '');

		$preselectedDealID = $this->input->get("preselectedDealID", '');
		$preselectedDealOptionID = $this->input->get("preselectedDealOptionID", '');
		$preselectedShoppingCartID = $this->input->get("preselectedShoppingCartID", '');

		if($deal_filter_submit == "" && ($preselectedDealID != "" || $preselectedDealOptionID != "" || $preselectedShoppingCartID != "") )
			$naracka_datum = "";

		//kreiranje na modelite
		$narackiModel = new Vouchers_Model() ;

		//povikuvanje na metodi
		$narackiData = $narackiModel->getNaracki($naracki_tip, $naracka_datum, $naracka_email, $naracka_ref, $otkup_administracija_status, $otkup_ime_prezime, $otkup_adresnica, $preselectedDealID, $preselectedDealOptionID, $preselectedShoppingCartID);

		// die(print_r($narackiData));	
		/**********************************************OUTPUT********************************************************/
		//output variabli
		$this->template->content->naracki_tip = $naracki_tip ;
		$this->template->content->naracka_datum = $naracka_datum ;
		$this->template->content->narackiData = $narackiData ;
		$this->template->content->narackaTipArr = self::$narackaTipArr ;
		$this->template->content->otkupPrevzemanjeArr = self::$otkupPrevzemanjeArr ;
		$this->template->content->naracka_email = $naracka_email;
		$this->template->content->naracka_ref = $naracka_ref;
		$this->template->content->otkupStatusiArr = self::$otkupStatusiArr;
		$this->template->content->otkup_administracija_status = $otkup_administracija_status;
		$this->template->content->otkup_ime_prezime = $otkup_ime_prezime;
		$this->template->content->otkup_adresnica = $otkup_adresnica;
		$this->template->content->preselectedDealID = $preselectedDealID;
		$this->template->content->preselectedDealOptionID = $preselectedDealOptionID;
		$this->template->content->preselectedShoppingCartID = $preselectedShoppingCartID;
	}

	
  public function manage($naracka_id = 0, $update_success = 0) 
  {

	  	if((int) $naracka_id == 0)
	  		url::redirect("/admin") ;

	    $this->template->title = "Добар попуст, подесување" ;
	    $this->template->content = new View("/admin_naracki/manage") ;

	    $narackiModel = new Vouchers_Model() ;
	    

	    if (request::method() == 'post') 
	    {
		    //should save then
		    $post = $this->input->post() ;
		    unset($post['files']);

		    //UPDATE-IRAJ JA  NARACKATA
			// die(print_r($post));
			$where = array('id' => $naracka_id);
			$updateFlag = $narackiModel->updateVoucherAttempt($post, $where);

			//ako pominal uspesno UPDATE-ot na narackata i se raboti za OTKUP
			if($updateFlag && $post["type"] == 'otkup')
			{
				$createdVouchers = $narackiModel->getData(array("attempt_id" => $naracka_id)) ;

				//otkup_administracija_status == 3 ("Комплетирана")
				if(count($createdVouchers) == 0 && $post["otkup_administracija_status"] == 3)
				{
					//kreiraj gi potrebnite vauceri (dokolku do sega ne se kreirani)
					$data = array("discount_id" => $post["deal_id"], "amount" => $post["quantity"], "used" => 1) ;
					$narackiModel->createVoucher($data, $naracka_id, "otkup") ;
				}
				elseif(count($createdVouchers) > 0 && $post["otkup_administracija_status"] == 10) //Откажана
				{
					$narackiModel->cancelAllVouchersInOrder($naracka_id);
				}

				// url::redirect("/admin") ;
			}

		    // url::redirect("/admin_naracki/index/") ;	 
		    $update_success = 1;

	    }

	    $narackaData = $narackiModel->getPayAttempt($naracka_id) ;

		// die(print_r($narackaData));
		$this->template->content->narackaData = $narackaData ;
		$this->template->content->update_success = $update_success ;
		$this->template->content->otkupStatusiArr = self::$otkupStatusiArr;
		$this->template->content->narackaTipArr = self::$narackaTipArr;
		$this->template->content->otkupPrevzemanjeArr = self::$otkupPrevzemanjeArr ;
  }

	

	public function __call($method, $arguments) 
	{
		// Disable auto-rendering
		$this->auto_render = FALSE ;
		echo 'page not found' ;
	}

}