<?php defined('SYSPATH') OR die('No direct access allowed.');

class Rss_Controller extends Default_Controller {

    public function index() {

        
        $dealsModel = new Deals_Model();
        $itemsRss = $dealsModel->getActiveOffers();
        //$itemsRss = array_slice($itemsRss, 0, 20);
        $partnersModel = new Partners_Model();
        
        $items = array();
        if ($itemsRss) {
            foreach ($itemsRss as $item) {
                $title = trim(strip_tags($item->title_mk));
                $titleClean = trim(strip_tags($item->title_mk_clean));
                $pubDate = date("r", strtotime($item->start_time));
                
                $partnerData = $partnersModel->getData(array('id'=>$item->partner_id));
                $googleLink = $partnerData[0]->google_link;
                $matchesarray = array();
				$location = array(0,0);
				if($googleLink !="")
				{
					preg_match_all("/center=(.*?)&/", $googleLink, $matchesarray);
					
					//echo($item->id."-------".$googleLink);
					if (count($matchesarray)) {
						$location = explode(",", $matchesarray[1][0]);
					}
				}                
                
                $items[] = array(
                    "title" => $title,
                    "link" => Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/deal/index/$item->id",
                    "url" =>  Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/deal/index/$item->id",
                    "imageUrl" => Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/pub/deals/$item->main_img",
                    "city" => "skopje",
                    "longitude" => (isset($location[0]) ? $location[0] : ""),
                    "latitude" => (isset($location[1]) ? $location[1] : "") ,
                    "timeLeft" => date("Y-m-d H:i:s", strtotime($item->end_time)),
                    "price" => (int)($item->tip == 'cena_na_vaucer' ? $item->price_voucher : $item->price_discount),
                    "discount" => (int)($item->price > 0 ? round(100 - ($item->price_discount / $item->price) * 100) : 0),
                    "actualPrice" => (int)$item->price,
						"description" => $titleClean. "<p>"."<img src='".Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/pub/deals/".$item->main_img."' /></p>",
                    "author" => "contact@kupinapopust.mk",
                    "pubDate" => $pubDate,
                );
            }
        }
        
        
        $info = array(
            'title' => 'Kupinapopust.mk - многу заштедуваш!',
            'link' => Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL'),
            "description" => "Kupinapopust.mk - многу заштедуваш!",
            'copyright' => 'Kupinapopust.mk All Rights Reserved');
        echo feed::create($info, $items);
        exit;
    }

    public function __call($method, $arguments) {
        $this->auto_render = FALSE;
        echo Router::$current_uri;
    }

}