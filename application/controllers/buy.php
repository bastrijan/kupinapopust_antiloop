<?php defined('SYSPATH') OR die('No direct access allowed.');

class Buy_Controller extends Default_Controller {
    
    public function __construct() {
		$this->template = 'layouts/public' ;
		
	
                if(strpos(Router::$method, "_android") !== false) {
                    $this->template = 'layouts/android' ;
                }
		parent::__construct();
	}
        
    public function index() {
        url::redirect("/");
    }

    public function deal($deal_id = null, $deal_option_id = null, $user_login = 0, $error_status = 0) {

    	if($deal_id == null)
    		url::redirect("/");

    	if($deal_option_id == null)
    		url::redirect("/deal/index/".$deal_id);



        $this->template->content = new View('index/buy');
        $this->template->title = kohana::lang("prevod.website_title");

        $dealsModel = new Deals_Model();
        $partnersModel = new Partners_Model();
        $customerModel = new Customer_Model();
		// $staticPagesModel = new Static_Model();
		$voucherModel = new Vouchers_Model();
		$categoriesModel = new Categories_Model() ;


		$soldVouchersData = $voucherModel->getData(array("deal_id"=>$deal_id, "deal_option_id"=>$deal_option_id, "activated"=>1));

		$this->template->content->soldVoucherCount = count($soldVouchersData);
		
	    $where_options_str = "d.id = $deal_id AND do.id = $deal_option_id";
	    $dealData = $dealsModel->getDealOptionsData($where_options_str);
        
        $this->template->content->dealData = $dealData[0];
        $partnerData = $partnersModel->getData(array("id"=>$dealData[0]->partner_id));

        $this->template->content->partnerData = $partnerData;
        
        $categoryData = $categoriesModel->getData(array("id"=>$dealData[0]->category_id));
        $this->template->content->categoryData = $categoryData;

        $customerPoints = 0;
        $user_id = $this->session->get("user_id");
        $user_type = $this->session->get("user_type");
        if ($user_id and $user_type == 'customer') :
            $customerPoints = $customerModel->getCustomerPoints($user_id);
        endif;
        
        if ($customerPoints < 10) {
            $customerPoints = 0;
        }
        $this->template->content->customerPoints = (int)$customerPoints;
		

		$this->template->content->user_login = $user_login;
		$this->template->content->error_status = $error_status;

    }
    
	public function gift($deal_id = null, $deal_option_id = null, $user_login = 0, $error_status = 0) {

    	if($deal_id == null)
    		url::redirect("/");

    	if($deal_option_id == null)
    		url::redirect("/deal/index/".$deal_id);
    		

        $this->template->content = new View('index/gift');
        $this->template->title = kohana::lang("prevod.website_title");

        $dealsModel = new Deals_Model();
        $partnersModel = new Partners_Model();
        $customerModel = new Customer_Model();
		// $staticPagesModel = new Static_Model();
		$voucherModel = new Vouchers_Model();
		$categoriesModel = new Categories_Model() ;

		$soldVouchersData = $voucherModel->getData(array("deal_id"=>$deal_id, "deal_option_id"=>$deal_option_id, "activated"=>1));
		
		$this->template->content->soldVoucherCount = count($soldVouchersData);
		
	    $where_options_str = "d.id = $deal_id AND do.id = $deal_option_id";
	    $dealData = $dealsModel->getDealOptionsData($where_options_str);

        $this->template->content->dealData = $dealData[0];
        $partnerData = $partnersModel->getData(array("id"=>$dealData[0]->partner_id));

        $this->template->content->partnerData = $partnerData;

        $categoryData = $categoriesModel->getData(array("id"=>$dealData[0]->category_id));
        $this->template->content->categoryData = $categoryData;

        $customerPoints = 0;
        $user_id = $this->session->get("user_id");
        $user_type = $this->session->get("user_type");
        if ($user_id and $user_type == 'customer') :
            $customerPoints = $customerModel->getCustomerPoints($user_id);
        endif;

        if ($customerPoints < 10) {
            $customerPoints = 0;
        }
        $this->template->content->customerPoints = (int)$customerPoints;
		

		$this->template->content->user_login = $user_login;
		$this->template->content->error_status = $error_status;

    }
    
	
    //ova e za avtomatskiot ajax na odredeno vreme sto se povikuva  
     public function ajax_card_on_file(){

        $this->template->title = "Добар попуст, подесување" ;
        $this->template->content = "";

        if (request::method() == 'post') {

            //map array za tipot na platezna karticka
            $typeCreditCardMapArr = array(
                "0" => "Master Card",
                "1" => "Maestro",
                "2" => "VISA",
                "3" => "MK Domestic Card",
                "4" => "Diners Club"
            );

            
            $cardOnFileModel = new Card_On_File_Model() ;
            
            //should save then
            $post = $this->input->post();
            $email = filter_var($post['email'], FILTER_VALIDATE_EMAIL);

            //izvadi gi zapomnetite platezni karticki
            $cardTokensDropDownStr = "";
            
            if($email)
            {
                $cardOnFileData = $cardOnFileModel->getData(array("email"=>$email));
                
                

                foreach ($cardOnFileData as $carTokenObj) 
                {   
                    //parsiraj go CREF ID-to
                    // $cardTokenID = substr($carTokenObj->cref_id, 0, 32);
                    $cardTokenID = $carTokenObj->cref_id;

                    $last4PanDigits = substr($carTokenObj->cref_id, 32, 4);

                    $cardTypeCode = substr($carTokenObj->cref_id, 36, 1);

                    $cardExpiryDate = substr($carTokenObj->cref_id, 37, 4);

                    //ona sto ke mu se prikaze na klientot vo drop down-ot
                    $showToClientStr = $typeCreditCardMapArr[$cardTypeCode]." XXXXXXXXXXXX".$last4PanDigits." истекува(мм/гг): ".substr($cardExpiryDate, 2, 2)."/".substr($cardExpiryDate, 0, 2);

                    //kreiraj go drop down option
                    $cardTokensDropDownStr .= '<option value="'.$cardTokenID.'">'.$showToClientStr.'</option>';

                }          
            }
            
   
            Kohana::config_set('config.global_xss_filtering', true);
            $arr = array('status' => 'success', 'cardTokensDropDownStr' => $cardTokensDropDownStr);
            die(json_encode($arr));
        }

    }


    public function otkup_thankyou($payAttemptID = 0, $shop_cart = 0) {

        // die($payAttemptID."--".$shop_cart);

        //ako ne postoi payAttemptID togas vrati go na index stranata
        if((int) $payAttemptID == 0)
            url::redirect("/") ;

        //KREIRAJ OBJEKTI
        $vouchersModel = new Vouchers_Model() ;
        $dealsModel = new Deals_Model();

        //IZVADI INFORMACII OD BAZA
        $payAttemptSortBy = array( 'deal_id' => 'ASC', 'deal_option_id' => 'ASC');

        if($shop_cart)
        {
            $payAttemptDataArr = $vouchersModel->getPayAttempt($payAttemptID, "shop_cart_id", $payAttemptSortBy) ;    

            //da se izbrisat zdelkite od cookie-to
            cookie::delete("kupinapopust_shop_cart");  
        }
        else
            $payAttemptDataArr = $vouchersModel->getPayAttempt($payAttemptID, "id", $payAttemptSortBy) ;

        //ako ne postoi taa naracka
        if(count($payAttemptDataArr) == 0)
            url::redirect("/") ;

        $where_options_str = "";

        foreach ($payAttemptDataArr as $payAttemptItem) 
        {
            if($where_options_str != "")
                $where_options_str .= "OR ";
                    
            $where_options_str .= "(d.id = ".$payAttemptItem->deal_id." AND do.id = ".$payAttemptItem->deal_option_id.") ";
           
        }

        $sort_str = "d.id ASC, do.id ASC, ";

        $dealDataArr = $dealsModel->getDealOptionsData($where_options_str, "", $sort_str);

        
        // die(print_r($dealDataArr));

        //URL za social media
        if ($dealDataArr[0]->card)
            $url = Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/card";    
        else    
            $url = Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/deal/index/" . $dealDataArr[0]->deal_id;


        //ISPRATI EMAIL SO CONFIRM SODRZINA
        $this->otkupConfirmationEmail($payAttemptDataArr, $dealDataArr);


        //DEFINIRAJ OUTPUT VARIABLI
        //$this->template->title = kohana::lang("prevod.website_title") ;
        $this->template->content = new View('otkup/thankyou') ;
        

        //output variablite se generiraat
        $this->template->content->payAttemptDataArr = $payAttemptDataArr;
        $this->template->content->cntNaracki = count($payAttemptDataArr);
        
        $this->template->content->dealDataArr = $dealDataArr;
        $this->template->content->url = $url;

        $this->template->set_global("fb_image", Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/pub/deals/".$dealDataArr[0]->main_img);
        $this->template->set_global("fb_title", strip_tags($dealDataArr[0]->title_mk_clean));
        $this->template->set_global("fb_link",  Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/deal/index/".$dealDataArr[0]->deal_id);
        $this->template->set_global("fb_description", strip_tags($dealDataArr[0]->content_short_mk));
        
        $this->template->title = ($dealDataArr[0]->card ? 'Подарок "kupinapopust" електронска картичка во вредност од ' : "").strip_tags($dealDataArr[0]->title_mk_clean);
                
    }

    private function otkupConfirmationEmail(&$payAttemptDataArr, &$dealDataArr)
    {

        $user_email = $payAttemptDataArr[0]->email; 

        $mailContentModel = new Emailrenderer_Model();

        $maildata = array(
            "payAttemptDataArr" => $payAttemptDataArr,
            "dealDataArr" => $dealDataArr,
            "cntNaracki"  => count($payAttemptDataArr)
        );


        $mailContent = $mailContentModel->render("otkup_confirm", $maildata);
        $email_config = Kohana::config('email');
        $from = array($email_config['from']['email'], $email_config['from']['name']);
        
        // die($mailContent);

        email::send($user_email, $from, "Потврда за извршена нарачка - Откуп!", $mailContent, true);
    }


    public function __call($method, $arguments) {
        $this->auto_render = FALSE;
        echo 'This text is generated by __call. If you expected the index page, you need to use: welcome/index/'.substr(Router::$current_uri, 8);
    }

}