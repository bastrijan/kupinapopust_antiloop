<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Newsletter_Controller extends Default_Controller {

    public function __construct() {
        parent::__construct();
    }

    
    public function unsubscribe($hash = '') {
      $newsletterModel = new Newsletter_Model();
      $res = $newsletterModel->unsubscribe($hash);
      if (count($res)) {
        url::redirect("/newsletter/unsubscribed");
      } else {
        url::redirect("/");
      }
    }
    
    public function unsubscribed () {
        $this->template->content = new View('newsletter/unsubscribed') ;
        $this->template->title = kohana::lang("prevod.website_title") ;
    }

    public function __call($method, $arguments) {
        // Disable auto-rendering
        $this->auto_render = FALSE;
        echo 'page not found';
    }

}