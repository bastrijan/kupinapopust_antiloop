<?php

defined('SYSPATH') OR die('No direct access allowed.') ;

class Smetkovodstvo_izvestaj_Controller extends Default_Controller {

  public function __construct() {
		
    $this->template = 'layouts/admin' ;
    if (Router::$method == 'indexprint') {
      $this->template = 'layouts/publicprint' ;
    }
    parent::__construct() ;
  }

  public $session = '' ;


	public function index() {
		ini_set('max_execution_time', 300); //300 seconds = 5 minutes=
		
		//get variabli
		$smetkovodstvoreport_day = $this->input->get("smetkovodstvoreport_day", date("j"));
		$smetkovodstvoreport_month = $this->input->get("smetkovodstvoreport_month", date("n"));
		$smetkovodstvoreport_year = $this->input->get("smetkovodstvoreport_year", date("Y"));

		//template
		$this->template->title = "Сметководство извештај" ;
		$this->template->content = new View("/smetkovodstvo/izvestaj") ;
		
		//models
		$SmetkovodstvoReportModel = new SmetkovodstvoReport_Model() ;
		
		//get data
		$smetkovodstvoreportdata = $SmetkovodstvoReportModel->getSmetkovodstvoIzvestaj($smetkovodstvoreport_day, $smetkovodstvoreport_month, $smetkovodstvoreport_year) ;
		$smetkovodstvoreportdata_bank = $SmetkovodstvoReportModel->getSmetkovodstvoIzvestaj($smetkovodstvoreport_day, $smetkovodstvoreport_month, $smetkovodstvoreport_year, "bank") ;
		$mesecenOslobodeniOdDanok = $SmetkovodstvoReportModel->getSmetkovodstvoIzvestajMesecen($smetkovodstvoreport_month, $smetkovodstvoreport_year) ;
		
		//put to output data format
		$this->template->content->smetkovodstvoreport_day = $smetkovodstvoreport_day;		
		$this->template->content->smetkovodstvoreport_month = $smetkovodstvoreport_month;
		$this->template->content->smetkovodstvoreport_year = $smetkovodstvoreport_year;
		$this->template->content->smetkovodstvoreportdata = $smetkovodstvoreportdata;
		$this->template->content->smetkovodstvoreportdata_bank = $smetkovodstvoreportdata_bank;
		$this->template->content->mesecenOslobodeniOdDanok = $mesecenOslobodeniOdDanok;
		$this->template->content->isPrintVersion = false;
	}
	
	public function indexprint() {
		ini_set('max_execution_time', 300); //300 seconds = 5 minutes=
		
		//get variabli
		$smetkovodstvoreport_day = $this->input->get("smetkovodstvoreport_day", date("j"));
		$smetkovodstvoreport_month = $this->input->get("smetkovodstvoreport_month", date("n"));
		$smetkovodstvoreport_year = $this->input->get("smetkovodstvoreport_year", date("Y"));
		
		//template
		$this->template->title = "Сметководство извештај" ;
		$this->template->content = new View("/smetkovodstvo/izvestaj") ;
		
		//models
		$SmetkovodstvoReportModel = new SmetkovodstvoReport_Model() ;
		
		//get data
		$smetkovodstvoreportdata = $SmetkovodstvoReportModel->getSmetkovodstvoIzvestaj($smetkovodstvoreport_day, $smetkovodstvoreport_month, $smetkovodstvoreport_year) ;
		$smetkovodstvoreportdata_bank = $SmetkovodstvoReportModel->getSmetkovodstvoIzvestaj($smetkovodstvoreport_day, $smetkovodstvoreport_month, $smetkovodstvoreport_year, "bank") ;
		$mesecenOslobodeniOdDanok = $SmetkovodstvoReportModel->getSmetkovodstvoIzvestajMesecen($smetkovodstvoreport_month, $smetkovodstvoreport_year) ;
		
		//put to output data format
		$this->template->content->smetkovodstvoreport_day = $smetkovodstvoreport_day;		
		$this->template->content->smetkovodstvoreport_month = $smetkovodstvoreport_month;
		$this->template->content->smetkovodstvoreport_year = $smetkovodstvoreport_year;
		$this->template->content->smetkovodstvoreportdata = $smetkovodstvoreportdata;
		$this->template->content->smetkovodstvoreportdata_bank = $smetkovodstvoreportdata_bank;
		$this->template->content->mesecenOslobodeniOdDanok = $mesecenOslobodeniOdDanok;
		$this->template->content->isPrintVersion = true;
	}
	
	public function pdf() {
		
		//get variabli
		//$report_deal_id = null, $report_type, $report_year , $report_month , $report_day 
		
		$report_deal_id = $this->input->get("report_deal_id", 0);
		$report_type = $this->input->get("report_type", '');
		$report_year = $this->input->get("report_year", date("Y"));
		$report_month = $this->input->get("report_month", date("n"));
		$report_day = $this->input->get("report_day", '');
		
		$vouchersModel = new Vouchers_Model() ;
		$vouchersModel->CreatePDF(0, "D", $report_deal_id, $report_type, $report_year , $report_month , $report_day) ;	
		
		exit ;
	}

	
  public function __call($method, $arguments) {
    // Disable auto-rendering
    $this->auto_render = FALSE ;
    echo 'page not found' ;
  }



}