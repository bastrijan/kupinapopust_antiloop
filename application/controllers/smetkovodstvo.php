<?php

defined('SYSPATH') OR die('No direct access allowed.') ;

class Smetkovodstvo_Controller extends Default_Controller {

  public function __construct() {
    $this->template = 'layouts/admin' ;
    parent::__construct() ;
  }

  public $session = '' ;


  public function login() {
 
	
    $this->template->title = "smetkovodstvo welcome" ;
    $this->template->content = new View("/smetkovodstvo/login") ;

    if (request::method() == 'post') {
      $post = $this->input->post() ;
      $username = $post['username'] ;
      $password = $post['password'] ;

      $smetkovodstvoModel = new Smetkovodstvo_Model() ;
      $where = array('username' => $username, 'password' => md5($password)) ;
      $smetkovodstvo = $smetkovodstvoModel->getData($where) ;
      if (count($smetkovodstvo) > 0) {

        $this->session = Session::instance() ;
        $this->session->set("user_id", $smetkovodstvo[0]->id) ;
        $this->session->set("user_type", 'smetkovodstvo') ;
        url::redirect("/smetkovodstvo_izvestaj") ;
      }
      else {
        $this->template->content->error = 'Настаната е грешка! Не е точно Корисничкото име или Лозинката.' ;
      }
    }
  }

 
	
  /*
	public function index() {
		url::redirect("/smetkovodstvo_izvestaj") ;
	}
*/

	
	
  public function __call($method, $arguments) {
    // Disable auto-rendering
    $this->auto_render = FALSE ;
    echo 'page not found' ;
  }



}