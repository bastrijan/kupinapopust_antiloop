<?php defined('SYSPATH') OR die('No direct access allowed.');

class Deal_Controller extends Default_Controller {
	
		private $pagination_settings;
	
        public function __construct() {
		$this->template = 'layouts/public' ;
		
		$this->pagination_settings = Kohana::config('pagination.default');
	
                if(strpos(Router::$method, "_android") !== false) {
                    $this->template = 'layouts/android' ;
                }
		parent::__construct();
	}
        
    
	public function index($deal_id = null, $vaucer_rasprodadeno = 0) {
		
		//proveri dali se povikuva so permalink.
		//dokolku e permalink togas najdi go ID-to i stavi go vo $category_id
		$deal_id = seo::PermaLinkConversion($deal_id);
		
		//ako se doaga na stranata bez deal_id => odnesi go na index stranata
		if (!$deal_id || (int)$deal_id < 1) {
			url::redirect("/");
		}
		
		//  new Profiler;
		$this->template->content = new View('index/deal');
		
		//kreiranje na modeli
		$dealsModel = new Deals_Model();
		// $staticPagesModel = new Static_Model();
		$categoriesModel = new Categories_Model() ;
		$subCategoriesModel = new Subcategories_Model();
		$partnersModel = new Partners_Model() ;
		
		//povikuvanje na funcii
		$sideOffers = array();
		
		$dateTimeUnique = date("Y-m-d H:i:s");
		$bottomOffersPagesCnt = ceil($dealsModel->getBottomOffersFullInfoSinglePaginationCnt($deal_id, $dateTimeUnique) / $this->pagination_settings['items_per_page']);      
		$bottomOffers = $dealsModel->getBottomOffersFullInfoSinglePagination($deal_id, 0, $this->pagination_settings['items_per_page'], $dateTimeUnique);
		
		//zemi gi aktivnite opcii na ponudata
		$where_options_str = "d.id = $deal_id AND do.active = 1";
		$optionsData = $dealsModel->getDealOptionsData($where_options_str);

		
		$categoriesData = $categoriesModel->getCategories();
		$subCategoriesData = $subCategoriesModel->getSubcategoriesbyParentid(0);
		
		$categoryObjectData = $categoriesModel->getData(array('id' => $optionsData[0]->category_id));
		$subCategoryObjectData = $subCategoriesModel->getData(array('id' => $optionsData[0]->subcategory_id));

		$partnerData = $partnersModel->getData(array('id' => $optionsData[0]->partner_id)) ;
		
		//zemi od static pages sodrzina za specificnite upastva sto stojat na top od stranata
		// if((int)$optionsData[0]->platena_reklama > 0) //ako e platena reklama
		// { 
		// 	$query_upastva_identifier = "instrukcii_za_platena_rek";
		// }
		// elseif($optionsData[0]->tip == "cela_cena") //ako e vaucer
		// {
		// 	$query_upastva_identifier = "instrukcii_za_vaucer";
		// }
		// else //ako e kupon
		// {
		// 	$query_upastva_identifier = "instrukcii_za_kupon";
		// }	
		// $where = array('identifier'=> $query_upastva_identifier);
		// $pageData = $staticPagesModel->getData($where);
		
		//ako ne postoi ponuda so takvo ID => odnesi go na index stranata		
		if (!$optionsData[0]) {
			url::redirect("/");
		}
		
		//kreiranje na output promenlivi
		$this->template->content->singleOffer = true;
		$this->template->content->sideOffers = $sideOffers;
		$this->template->content->bottomOffers = $bottomOffers;
		$this->template->content->optionsData = $optionsData;
		/*
		$this->template->title = kohana::lang("prevod.website_title").
			(!empty($categoryObjectData) ? "-".seo::url_slug($categoryObjectData[0]->name) : "").
			(!empty($subCategoryObjectData) ? "-".seo::url_slug($subCategoryObjectData[0]->name) : "").
			"-".seo::url_slug(strip_tags($optionsData[0]->title_mk_deal));
		*/
		$this->template->title =
			(!empty($categoryObjectData) ? $categoryObjectData[0]->name : "").
			(!empty($subCategoryObjectData) ? " | ".$subCategoryObjectData[0]->name : "");
		if($this->template->title == "")
		{
			$this->template->title =
			strip_tags(count($optionsData) > 1 ? $optionsData[0]->title_mk_clean_deal : $optionsData[0]->title_mk_clean_deal).
			" | kupinapopust.mk";
		}
		else
		{
			$this->template->title .=
			" | ".strip_tags(count($optionsData) > 1 ? $optionsData[0]->title_mk_clean_deal : $optionsData[0]->title_mk_clean_deal).
			" | kupinapopust.mk";
		}
		// $this->template->content->instrukciiCenaVoucherContent = "";
		// $this->template->content->instrukciiCenaVoucherContent_vaucer = $pageData[0]->content_mk;
		// $this->template->content->instrukciiCenaVoucherContent_kupon = $pageData[0]->content_mk;
		// $this->template->content->instrukciiCenaVoucherContent_platena_rek = $pageData[0]->content_mk;
		//$this -> template -> content -> dealsCategory = $allCategory;
		
		$this->template->set_global("fb_image", Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/pub/deals/".$optionsData[0]->main_img);
		$this->template->set_global("fb_title", strip_tags(count($optionsData) > 1 ? $optionsData[0]->title_mk_deal : $optionsData[0]->title_mk_deal));
		$this->template->set_global("fb_link",  Kohana::config('facebook.facebookSiteProtocol')."://".Kohana::config('facebook.facebookSiteURL')."/deal/index/".$optionsData[0]->deal_id);
		$this->template->set_global("fb_description", strip_tags(count($optionsData) > 1 ? $optionsData[0]->content_short_mk : $optionsData[0]->content_short_mk));
		$this->template->content->vaucer_rasprodadeno = $vaucer_rasprodadeno;
		$this->template->content->categoriesData = $categoriesData ;
		$this->template->content->subCategoriesData = $subCategoriesData ;

		$this->template->content->partnerData = $partnerData ;
		
		$this -> template -> content -> dateTimeUnique = $dateTimeUnique;
		$this -> template -> content -> bottomOffersPagesCnt = $bottomOffersPagesCnt;
		$this -> template -> content -> deal_id = $deal_id;
		
		// OMILENI
		// zemanje na 'cookie'
		$cookie_value = cookie::get("kupinapopust_fav");

		if ( ! empty($cookie_value)) {
			$favDeals = json_decode($cookie_value, true);
		} else {
			$favDeals = array();
		}
		$this->template->content->favDeals = $favDeals;
		// END - OMILENI

		// SHOPPING CART
		// zemanje na 'cookie'
		$cookie_value_shop_cart = cookie::get("kupinapopust_shop_cart");

		if ( ! empty($cookie_value_shop_cart)) {
			$shopCartDeals = json_decode($cookie_value_shop_cart, true);
		} else {
			$shopCartDeals = array();
		}
		$this->template->content->shopCartDeals = $shopCartDeals;
		// END - SHOPPING CART
	}
        
        
	
    public function past($deal_id) {
        $this->index($deal_id);
    }


    public function demo($deal_id) {
            $this->index($deal_id);
    }
    
	public function general_deal($permalink = 0) 
	{
		
        //proveri dali se povikuva so permalink.
        //dokolku e permalink togas najdi go ID-to i stavi go vo $category_id
        $general_offer_id = seo::PermaLinkConversion($permalink);
        
        //new Profiler;
        // $this -> template -> content = new View('alloffers/index');
        $this -> template -> content = new View('alloffers/general_deal');

         //inicijalizacija na modeli
        $dealsModel = new Deals_Model();
        $generalOfferDealsModel = new General_Offer_Deals_Model();
		$categoriesModel = new Categories_Model() ;
		$subCategoriesModel = new Subcategories_Model();

		$where = array('id' => $general_offer_id);
		$generalOfferData = $dealsModel->getData($where) ;
                
        //Ajax request za paginacija na ponudite
        $dateTimeUnique = date("Y-m-d H:i:s");

        //povikuvanje na funcii
        $bottomOffers = array();

        $bottomOffersPagesCnt = ceil($generalOfferDealsModel->getPonudiByGeneralOfferPaginationCnt($general_offer_id, $dateTimeUnique) / $this->pagination_settings['items_per_page']);      
        $bottomOffers = $dealsModel->getPonudiByGeneralOfferPagination($general_offer_id, 0, $this->pagination_settings['items_per_page'], $dateTimeUnique);

        //kreiranje na variabli
        $this->template->content->bottomOffers = $bottomOffers;
        $this->template->content->bottomOffersPagesCnt = $bottomOffersPagesCnt;


		$categoryObjectData = $categoriesModel->getData(array('id' => $generalOfferData[0]->category_id));
		$subCategoryObjectData = $subCategoriesModel->getData(array('id' => $generalOfferData[0]->subcategory_id));

		$this->template->title =
			(!empty($categoryObjectData) ? $categoryObjectData[0]->name : "").
			(!empty($subCategoryObjectData) ? " | ".$subCategoryObjectData[0]->name : "");
		if($this->template->title == "")
		{
			$this->template->title =
			strip_tags(count($generalOfferData) > 1 ? $generalOfferData[0]->title_mk_clean : $generalOfferData[0]->title_mk_clean).
			" | kupinapopust.mk";
		}
		else
		{
			$this->template->title .=
			" | ".strip_tags(count($generalOfferData) > 1 ? $generalOfferData[0]->title_mk_clean : $generalOfferData[0]->title_mk_clean).
			" | kupinapopust.mk";
		}


        $this -> template -> content -> dateTimeUnique = $dateTimeUnique;

				
		// OMILENI
		// zemanje na 'cookie'
		$cookie_value = cookie::get("kupinapopust_fav");
		if ( ! empty($cookie_value)) {
			$favDeals = json_decode($cookie_value, true);
		} else {
			$favDeals = array();
		}
		$this->template->content->favDeals = $favDeals;
		// END - OMILENI

		// SHOPPING CART
		// zemanje na 'cookie'
		$cookie_value_shop_cart = cookie::get("kupinapopust_shop_cart");

		if ( ! empty($cookie_value_shop_cart)) {
			$shopCartDeals = json_decode($cookie_value_shop_cart, true);
		} else {
			$shopCartDeals = array();
		}
		$this->template->content->shopCartDeals = $shopCartDeals;
		// END - SHOPPING CART

		$this->template->content->general_offer_id = $general_offer_id;
		$this->template->content->title_mk_clean = $generalOfferData[0]->title_mk_clean;
	}


	public function tag($tag_name = "") 
	{
		
        //new Profiler;
        $this -> template -> content = new View('tags/deals');

        //inicijalizacija na modeli
        $dealsModel = new Deals_Model();
        $dealTagsModel = new Deal_Tags_Model() ;
                
        //Ajax request za paginacija na ponudite
        $dateTimeUnique = date("Y-m-d H:i:s");

        //povikuvanje na funcii
        $bottomOffers = array();

        $bottomOffersPagesCnt = ceil($dealTagsModel->getPonudiByTagPaginationCnt($tag_name, $dateTimeUnique) / $this->pagination_settings['items_per_page']);      
        $bottomOffers = $dealsModel->getPonudiByTagPagination($tag_name, 0, $this->pagination_settings['items_per_page'], $dateTimeUnique);

        //kreiranje na variabli
        $this->template->content->bottomOffers = $bottomOffers;
        $this->template->content->bottomOffersPagesCnt = $bottomOffersPagesCnt;

		$this->template->title = $tag_name." | kupinapopust.mk";
        $this -> template -> content -> dateTimeUnique = $dateTimeUnique;
				
		// OMILENI
		// zemanje na 'cookie'
		$cookie_value = cookie::get("kupinapopust_fav");
		if ( ! empty($cookie_value)) {
			$favDeals = json_decode($cookie_value, true);
		} else {
			$favDeals = array();
		}
		$this->template->content->favDeals = $favDeals;
		// END - OMILENI

		// SHOPPING CART
		// zemanje na 'cookie'
		$cookie_value_shop_cart = cookie::get("kupinapopust_shop_cart");

		if ( ! empty($cookie_value_shop_cart)) {
			$shopCartDeals = json_decode($cookie_value_shop_cart, true);
		} else {
			$shopCartDeals = array();
		}
		$this->template->content->shopCartDeals = $shopCartDeals;
		// END - SHOPPING CART

		$this->template->content->tag_name = $tag_name;
	}

    
    public function index_android($deal_id = null, $vaucer_rasprodadeno = 0) {
            
		//ako se doaga na stranata bez deal_id => odnesi go na index stranata
		if (!$deal_id || (int)$deal_id < 1) {
			url::redirect("/");
		}
		
		//  new Profiler;
//                $this->template = 'layouts/android' ;
                $this -> template -> content = new View('index/index_android');
                $this -> template -> title = kohana::lang("prevod.website_title");
		
		//kreiranje na modeli
		$dealsModel = new Deals_Model();
		
		//povikuvanje na funcii
		$primary = $dealsModel->getFullOfersSingleInfo($deal_id);
		
		//ako ne postoi ponuda so takvo ID => odnesi go na index stranata		
		if (!$primary) {
			url::redirect("/");
		}
		
		//kreiranje na output promenlivi
		$this->template->content->primaryDeals = $primary;
		$this->template->content->singleOffer = true;
		$this->template->content->instrukciiCenaVoucherContent = "";
		
		$this->template->content->vaucer_rasprodadeno = $vaucer_rasprodadeno;
		
		$this -> template -> share_dialog_show = true;
		$this -> template -> bridgeName = "DealHandlerDealBuy";
		
	}
        
        
        
    public function __call($method, $arguments) {
        $this->auto_render = FALSE;
        echo Router::$current_uri;
    }

}