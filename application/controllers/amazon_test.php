<?php

defined('SYSPATH') OR die('No direct access allowed.');

// samo za test
use Aws\Ses\SesClient;
require_once DOCROOT.'vendor/autoload.php';


class Amazon_Test_Controller extends Default_Controller {

    
    public function index()
    {
        $this->auto_render = FALSE;


        //kreiraj objekt od AMAZON
        $client = SesClient::factory(array(
                    'key' => Kohana::config('config.amazon_ses_key'),
                    'secret' => Kohana::config('config.amazon_ses_secret'),
                    'region' => Kohana::config('config.amazon_ses_region')
                    ));

        $toAddresses = array("suppressionlist@simulator.amazonses.com");

        $subject_name = "email za proverka na bauns";

        $emailContentCustom = "<b>Ova e sodrzinata</b>";

        $emailSentId = $client->sendEmail(array(
                    'Source' => Kohana::config('config.amazon_ses_email_from'),
                    'Destination' => array(
                        'ToAddresses' => $toAddresses
                        ),
                    'Message' => array(
                        'Subject' => array(
                            'Data' => $subject_name,
                            'Charset' => 'UTF-8',
                            ),
                        'Body' => array(
                            'Text' => array(
                                'Data' => 'КУПИНАПОПУСТ.МК - Многу заштедуваш!!!',
                                'Charset' => 'UTF-8',
                                ),
                            'Html' => array(
                                'Data' => $emailContentCustom,
                                'Charset' => 'UTF-8',
                                ),
                            ),
                        ),
                    'ReplyToAddresses' => array(Kohana::config('config.amazon_ses_email_from')),
                    'ReturnPath' => Kohana::config('config.amazon_ses_email_from')
                    ));
    }


    public function __call($method, $arguments) {
        // Disable auto-rendering
        $this->auto_render = FALSE;
        echo 'page not found';
    }

}