<?php
defined('SYSPATH') OR die('No direct access allowed.');

use Aws\Ses\SesClient;
require_once DOCROOT.'vendor/autoload.php';


class Cron_Amazon_Controller extends Default_Controller {

	public function __construct() {

		$this->auto_render = FALSE;

		parent::__construct();
		
	}
	

	//CRONJOB NA SEKOJ POLN SAAT//
	public function send()
	{
		// Kohana::log("error", "test 1");

		if(PHP_SAPI != "cli")
			exit;

		// Kohana::log("error", "test 2");


		$this->auto_render = FALSE;
		
		//se postavuva max_execution_time za da moze skriptata da vrti podolgo vreme bez da ja skine serverot.
		ini_set('max_execution_time', 0); //0 - never timeout
		
		//model za amazon_newsletter_scheduler
		$amazonNewsletterSchedulerModel = new Amazon_Newsletter_Scheduler_Model();		
		
		//na pocetok od mesecot izbri go newsletter-ite od prethodniot mesec
		if(date('Y-m-d') == date('Y-m-01'))
			$amazonNewsletterSchedulerModel->deleteOldNewsletters(date('Y-m-01 00:00:00'));
		
		//zemi go momentalniot datum, cas i stavi 10 minuti povekje za da bideme sigurni deka navistina ke se zeme zapisot sto e staven so soodveten cas
		$curr_date_time = date('Y-m-d H:10:00');
		
		//proveri dali ima newsletter za prakjanje
		$result = $amazonNewsletterSchedulerModel->getNewsletterToSend($curr_date_time);
		

		// Kohana::log("error", "test 3:".count($result));

		//ako ima newsletter za prakjanje togas povikaj soodvetni fukcii
		if(count($result))	
		{
			// Kohana::log("error", "test 4:");
			
			//inicijalizacija
			$method_name = "";
			$arrayParameters = NULL;
			$schedulerID = $result[0]->id;
			$subject_name = $result[0]->name;
			$newsletterType = $result[0]->newsletter_type;
			$send_to = $result[0]->send_to;
			
			//smeni go statusot vo baza deka e vo progres
			$where = array('id' => $schedulerID, 'current_status' => 1);
			$amazonNewsletterSchedulerModel->saveData($where);

			///////////PRIVREMENO
			$recipient_arr = array("to"=> "bastrijan@gmail.com");
			
			$email_config = Kohana::config('email');
			$from = array($email_config['from']['email'], $email_config['from']['name']);
			email::send($recipient_arr, $from, 'Amazon newsletter started at '.date("Y-m-d H:i:s"), "Samo sto pocna prakjanje na Newsletter од типот: $newsletterType", true); 
			/////////////////////////
			
			
			//ako e newsletter_category
			if (strpos($newsletterType,'newsletter_category') !== false)
			{
				$newsletterArray = explode("/", $newsletterType);
				$method_name = $newsletterArray[0];
				$arrayParameters = array($newsletterArray[1], true);
			}
			else//ako e drug newsletter
			{
				$method_name = $newsletterType;
				$arrayParameters = array(true);
			} 
			
			//drugi modeli
			$sendNewslettersModel = new Newsletter_Model();
			$indexController = new Index_Controller();
			
			$emailContent = call_user_func_array(array($indexController, $method_name), $arrayParameters);
			
			
			//kreiraj objekt od AMAZON
			$client = SesClient::factory(array(
						'key' => Kohana::config('config.amazon_ses_key'),
						'secret' => Kohana::config('config.amazon_ses_secret'),
						'region' => Kohana::config('config.amazon_ses_region')
						));
			
			
			$toAddresses = array();
			$customersObjArr = null;
			$custCount = 0;
			$current = 0;
			$cntAcctualSent = 0;
			$invalidIdsStr = "";
			//$emailArrTmp = array();
			$sendPerStep = Kohana::config('config.amazon_ses_sendPerStep'); //kolku email-i da prakja vo eden cekor
			
			//izvadi gi site odednas
			$customersObjArr = $sendNewslettersModel->getCustomersEmails($send_to, 0, 0);				
			
			//izbroj kolku gi ima
			$custCount = count($customersObjArr);
			
			// Kohana::log("error", "test 5:".$custCount);

			if ($custCount > 0) {
				while ($current < $custCount) {

					// Kohana::log("error", "test 6:");
					
					//da se iscisti array-ot sto ke gi sodrzi email adresite kon koi ke se prakja vo sekoj cekor
					unset($toAddresses);
					$toAddresses = array();
					
					
					for ($ij = 1; $ij <= $sendPerStep; $ij++) 
					{
						if (filter_var($customersObjArr[$current]->email, FILTER_VALIDATE_EMAIL)) //ako e validna email adresata
						{
							$toAddresses[] = $customersObjArr[$current]->email;
							
							$emailContentCustom = str_replace("hash_string_replace", md5("kupi_".$customersObjArr[$current]->email), $emailContent);
							
							//zgolemi count na broj na email-i koi vistinski se prateni
							$cntAcctualSent++;
						}
						else // izbrisi ja email adresata
						{
							//ako modelot vekje ne postoi togas kreiraj go povtorno
							// if(!is_object($sendNewslettersModel))
								// $sendNewslettersModel = new Newsletter_Model();

		                    //zapisi vo log
		                    // Kohana::log("error", "AMAZON - FILTER_VALIDATE_EMAIL izbrisan email:". $customersObjArr[$current]->email);
		                    // Kohana::log("error", "AMAZON - FILTER_VALIDATE_EMAIL da se izbrise email:". $customersObjArr[$current]->email);

							$invalidIdsStr .= $customersObjArr[$current]->id.", ";
							//izbrisi ja email adresata
							// $sendNewslettersModel->delete_multiple($customersObjArr[$current]->id); 
						}


						//zgolemi go brojacot sto broi do koj element sme stignale od array-ot so objekti
						$current++;
						
						//ako vekje stignal do posledniot element od array-ot so objekti => da izleze od ovoj FOR
						if($current == $custCount)
							break;
					}
					
					
					
					if (count($toAddresses) > 0) 
					{
						//polnenje na AMAZON objektot za da prati email-i
						$emailSentId = $client->sendEmail(array(
									'Source' => Kohana::config('config.amazon_ses_email_from'),
									'Destination' => array(
										'ToAddresses' => $toAddresses
										),
									'Message' => array(
										'Subject' => array(
											'Data' => $subject_name,
											'Charset' => 'UTF-8',
											),
										'Body' => array(
											'Text' => array(
												'Data' => 'КУПИНАПОПУСТ.МК - Многу заштедуваш!!!',
												'Charset' => 'UTF-8',
												),
											'Html' => array(
												'Data' => $emailContentCustom,
												'Charset' => 'UTF-8',
												),
											),
										),
									'ReplyToAddresses' => array(Kohana::config('config.amazon_ses_email_from')),
									'ReturnPath' => Kohana::config('config.amazon_ses_email_from')
									));
						
					}//if (count($toAddresses) > 0) 			
					
					
				}//while ($current < $custCount) {
				
			}//if ($custCount > 0) {
			
			
			///////////PRIVREMENO//////////////////////////////////////////
			//isprati email deka e zavrseno
			$recipient_arr = array("to"=> "jovica.belovski@gmail.com"
							,"cc"=>"bastrijan@gmail.com"
					);
			
			// Kohana::log("error", "test 8:");

			$email_config = Kohana::config('email');
			$from = array($email_config['from']['email'], $email_config['from']['name']);
			$confirm_email_content = "Испратени се $cntAcctualSent (od baza se izvadeni $custCount) e-mail пораки со Newsletter од типот: $newsletterType <br/><br/><br/>(DEBUG INFO) Невалидни IDs: ".$invalidIdsStr;
			email::send($recipient_arr, $from, 'Amazon newsletter finished ', $confirm_email_content, true); 
			/////////////////////////



			/////////*******************************/
			//koga ke zavrsi prakjanjeto smeni go statusot vo baza deka e zavrsen
			//ako modelot vekje ne postoi togas kreiraj go povtorno
			// Kohana::log("error", "AMAZON - sega ke upisam vo baza deka e zavrseno prakjanjeto");
			
			//izbrisi go objektot $amazonNewsletterSchedulerModel
			// unset($amazonNewsletterSchedulerModel);
			// $amazonNewsletterSchedulerModel = null;

			//kreiraj go povtorno objektot $amazonNewsletterSchedulerModel
			// $amazonNewsletterSchedulerModel_UPDATE = new Amazon_Newsletter_Scheduler_Model();			

			// Kohana::log("error", "AMAZON - UPDATE NA schedulerID=".$schedulerID);

			// $where = array('id' => $schedulerID, 'current_status' => 2);
			// $amazonNewsletterSchedulerModel_UPDATE->saveData($where);

			
			die("USPESNO ISPRAKJANJE!!!");

		}//if(count($result))	
		else
			die("NEMA NEWSLETTER ZA ISPRAKJANJE");
		
	}
	




	// /usr/local/bin/php -q /home/kupi/public_html/cron.php cron_amazon incentive_to_activate

			
	//CRONJOB EDNAS NA DEN
	public function incentive_to_activate()
	{
		// Kohana::log("error", "test 1");

		if(PHP_SAPI != "cli")
			exit;

		// Kohana::log("error", "test 2");


		$this->auto_render = FALSE;
		
		//se postavuva max_execution_time za da moze skriptata da vrti podolgo vreme bez da ja skine serverot.
		ini_set('max_execution_time', 0); //0 - never timeout
		
			
		//inicijalizacija
		$method_name = "incentive_to_activate_email";
		$arrayParameters = array(true);
		$subject_name = "Ве сакаме повторно назад!";

		//drugi modeli
		$sendNewslettersModel = new Newsletter_Model();
		$indexController = new Index_Controller();
		
		$emailContent = call_user_func_array(array($indexController, $method_name), $arrayParameters);
			
			
		//kreiraj objekt od AMAZON
		$client = SesClient::factory(array(
					'key' => Kohana::config('config.amazon_ses_key'),
					'secret' => Kohana::config('config.amazon_ses_secret'),
					'region' => Kohana::config('config.amazon_ses_region')
					));
			
			
		$toAddresses = array();
		$customersObjArr = null;
		$custCount = 0;
		$current = 0;
		$cntAcctualSent = 0;
		$invalidIdsStr = "";
		//$emailArrTmp = array();
		$sendPerStep = Kohana::config('config.amazon_ses_sendPerStep'); //kolku email-i da prakja vo eden cekor
			
		//izvadi gi site odednas
		$customersObjArr = $sendNewslettersModel->getInActiveCustomersEmails();				
			
		//izbroj kolku gi ima
		$custCount = count($customersObjArr);
			
		// Kohana::log("error", "test 5:".$custCount);	

		if ($custCount > 0) 
		{
			//update-iraj gi zapisite vo newsletter tabelata: welcome_again_status = welcome_again_status + 1
			$sendNewslettersModel->updateInActiveCustomersWelcomeAgainStatus();


			///////////PRIVREMENO
			$recipient_arr = array("to"=> "bastrijan@gmail.com");
			
			$email_config = Kohana::config('email');
			$from = array($email_config['from']['email'], $email_config['from']['name']);
			
			email::send($recipient_arr, $from, 'Prakjanje na pottiknuvacki emails na den: '.date("Y-m-d H:i:s"), "Samo sto pocna prakjanjeto na pottiknuvacki emails", true); 
			/////////////////////////


			while ($current < $custCount) 
			{

				// Kohana::log("error", "test 6:");
				
				//da se iscisti array-ot sto ke gi sodrzi email adresite kon koi ke se prakja vo sekoj cekor
				unset($toAddresses);
				$toAddresses = array();
				
				
				for ($ij = 1; $ij <= $sendPerStep; $ij++) 
				{
					if (filter_var($customersObjArr[$current]->email, FILTER_VALIDATE_EMAIL)) //ako e validna email adresata
					{
						$toAddresses[] = $customersObjArr[$current]->email;
						
						$emailContentCustom = str_replace("hash_string_replace", md5("kupi_".$customersObjArr[$current]->email), $emailContent);
						
						//zgolemi count na broj na email-i koi vistinski se prateni
						$cntAcctualSent++;
					}
					else // izbrisi ja email adresata
					{
						//ako modelot vekje ne postoi togas kreiraj go povtorno
						// if(!is_object($sendNewslettersModel))
							// $sendNewslettersModel = new Newsletter_Model();

	                    //zapisi vo log
	                    // Kohana::log("error", "AMAZON - FILTER_VALIDATE_EMAIL izbrisan email:". $customersObjArr[$current]->email);
	                    // Kohana::log("error", "AMAZON - FILTER_VALIDATE_EMAIL da se izbrise email:". $customersObjArr[$current]->email);

						$invalidIdsStr .= $customersObjArr[$current]->id.", ";
						//izbrisi ja email adresata
						// $sendNewslettersModel->delete_multiple($customersObjArr[$current]->id); 
					}


					//zgolemi go brojacot sto broi do koj element sme stignale od array-ot so objekti
					$current++;
					
					//ako vekje stignal do posledniot element od array-ot so objekti => da izleze od ovoj FOR
					if($current == $custCount)
						break;
				}
				
				
				
				if (count($toAddresses) > 0) 
				{
					//polnenje na AMAZON objektot za da prati email-i
					$emailSentId = $client->sendEmail(array(
								'Source' => Kohana::config('config.amazon_ses_email_from'),
								'Destination' => array(
									'ToAddresses' => $toAddresses
									),
								'Message' => array(
									'Subject' => array(
										'Data' => $subject_name,
										'Charset' => 'UTF-8',
										),
									'Body' => array(
										'Text' => array(
											'Data' => 'КУПИНАПОПУСТ.МК - Многу заштедуваш!!!',
											'Charset' => 'UTF-8',
											),
										'Html' => array(
											'Data' => $emailContentCustom,
											'Charset' => 'UTF-8',
											),
										),
									),
								'ReplyToAddresses' => array(Kohana::config('config.amazon_ses_email_from')),
								'ReturnPath' => Kohana::config('config.amazon_ses_email_from')
								));
					
				}//if (count($toAddresses) > 0) 			
				
				
			}//while ($current < $custCount) {

			
			///////////PRIVREMENO//////////////////////////////////////////
			//isprati email deka e zavrseno
			$recipient_arr = array("to"=> "jovica.belovski@gmail.com"
							,"cc"=>"bastrijan@gmail.com"
					);
			
			// Kohana::log("error", "test 8:");

			$email_config = Kohana::config('email');
			$from = array($email_config['from']['email'], $email_config['from']['name']);
			$confirm_email_content = "Испратени се $cntAcctualSent (od baza se izvadeni $custCount) e-mail пораки <br/><br/><br/>(DEBUG INFO) Невалидни IDs: ".$invalidIdsStr;
			email::send($recipient_arr, $from, 'Prakjanje na INCENTIVE emails za povtorna aktivnost - finished ', $confirm_email_content, true); 
			/////////////////////////

			die("USPESNO ISPRAKJANJE!!!");	
			
		}//if ($custCount > 0) {
		else
			die("NEMA INCENTIVE EMAIL-i ZA ISPRAKJANJE");	
			


	}




    public function __call($method, $arguments) {
        // Disable auto-rendering
        $this->auto_render = FALSE;

        echo Router::$current_uri;
    }

}