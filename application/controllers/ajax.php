<?php

defined('SYSPATH') OR die('No direct access allowed.');

class Ajax_Controller extends Default_Controller {

    public function __construct() {
        parent::__construct();
    }
	
	public function tellafriend() {
		$this->auto_render = FALSE;
		$post = $this->input->post();
		
		$your_email = $post['your_email_tellafriend_popup'];
		$email = $post['email_tellafriend_popup'];
		$content = $post['content_tellafriend_popup'];
		
        email::send($email, $your_email, 'Провери ја оваа понуда на kupinapopust.mk!', $content, true);
		
		echo json_encode(array("resp" => "success"));
	}

    public function newsletter() {
        $this->auto_render = FALSE;
        
        header("Access-Control-Allow-Origin: *");

        $newsletterModel = new Newsletter_Model();
        $post = $this->input->post();

        //setiraj trackiranje na email vo cookie
        trackvisitor::setEmailTracking($post['mail']);

        $where = array("email" => $post['mail']);
        $row = $newsletterModel->getData($where);
        if (!$row) {
			$newID = $newsletterModel->saveSubscription($where);
            echo json_encode(array("resp" => "success"));
            //cookie::set("newsletter_popup", "true", 15552000);
            exit;
        } else {
            echo json_encode(array("resp" => "duplicate"));
            //cookie::set("newsletter_popup", "true", 15552000);
            exit;
        }
    }

    public function adminpoints() {
        $this->auto_render = FALSE;
        $customerModel = new Customer_Model();
        $pointsModel = new Points_Model();
        $post = $this->input->post();

        $where = array("email" => $post['id']);
        $row = $customerModel->getData($where);

        if ($row) {
            $id = $row[0]->id;
            $points = $post['value'];
            $pointsAdded = $points - $pointsModel->getCustomerPoints($id);
			if($pointsAdded > 0)
				$pointsModel->savePoints($id, $pointsAdded, "- Поени додадени од Kupinapopust.mk");
			elseif($pointsAdded < 0)
				$pointsModel->savePoints($id, $pointsAdded, "- Поени одземени од Kupinapopust.mk");
            
			echo $points;
            exit;
        } else {

            exit;
        }
    }

    public function __call($method, $arguments) {
        // Disable auto-rendering
        $this->auto_render = FALSE;
        echo 'page not found';
    }

}