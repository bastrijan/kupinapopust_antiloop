<?php

defined('SYSPATH') OR die('No direct access allowed.') ;

/**
 * trackvisitor helper class.
 *
 * 
 */
class bannersystem_Core {


	public static function getBanner($banner_location = 1) 
	{
        $ret_link = "";

        //inicijalizacija na modeli
        $bannersModel = new Banners_Model() ;

        //treba da go zeme soodvetniot banner sto ke se prikazuva
        //1. da se zemat vo predvid samo banerite sto se od soodvetnata pozicija
        //2. banerite cij sto impressions_limit ne e nadminat
        $banner = $bannersModel->getBanner($banner_location);

        if($banner)
        {
            //treba da go update-ira poleto impressions_cnt
            $update_arr = array();
            $update_arr["id"] = $banner->id;
            $update_arr["impressions_cnt"] = $banner->impressions_cnt + 1;
            $bannersModel->saveData($update_arr);


            //treba da kreira linkot so slikata sto treba da se prikazuva
            $banner_img = $banner->id.'.'.$banner->banner_ext;

            if($banner->banner_link != "")
            {
                $banner_link_str = $banner->banner_link;
                
                if(strpos($banner_link_str, "kupinapopust.") !== false)
                {
                    if(strpos($banner_link_str, "?") !== false)
                        $banner_link_str .= "&banner";
                    else
                        $banner_link_str .= "/banner";
                }


                $ret_link .= '<a target="_blank" href="'.$banner_link_str.'">';    
            }

            if($banner_img != "")
                $ret_link .= '<img class="img_top_banner" src="/pub/img/header_banner/'.$banner_img.'?x='.time().'">';    

            if($banner->banner_link != "")
                $ret_link .= '</a>';
        }


        //treba da vrati linkot
        return $ret_link;
    }


}

// End arr
