<?php

defined('SYSPATH') OR die('No direct access allowed.') ;

/**
 * trackvisitor helper class.
 *
 * 
 */
class trackvisitor_Core {

	//how long a cookie to live 
	private static $secondsToLive = 157680000; // five year (3600 * 24 * 365 * 5)

	//osnovna f-cija koja proveruva dali treba da se trackira
	public static function checkToTrack($template_obj = null) 
	{
		//current date
		$currentDate = date("Y-m-d");
		
        // zemanje na 'cookie' sto cuva info za denot koga go posetil web sajtot
        $cookie_day_value = cookie::get("knp_day");
        
        //porano ne postoese cookie vo browser-ot = ne sme go trackirale porano
        if(empty($cookie_day_value)) 
        {
            //setiraj den koga sme go trackirale
            self::setDateTracking($currentDate);

        } 
        else //postoese cookie = sme go trackirale porano
        {

            //ovoj den prv pat doaga na web sajtot
            if($cookie_day_value != $currentDate)
            {
                // zemanje na 'cookie' sto cuva info za email-ot na korisnikot
                $cookie_email_value = cookie::get("knp_email");

                //dokolku ne e prazno cookie-to za email => update-iraj ja vrednosta za last_visited_website vo baza
                if(!empty($cookie_email_value))
                {
                    //kreiraj modeli
                    $newsletterModel = new Newsletter_Model();

                    //update-iraj ja vrednosta za last_visited_website vo baza	
                    $newsletterModel->setLastVisitedWebsite($cookie_email_value);

                    //zemi go zapisot od baza za soodvetniot email
                    $registeredNewsletterUser = $newsletterModel->getData(array("email" => $cookie_email_value));
                    
                    //vnesi global varijabli
                    if(isset($registeredNewsletterUser[0]->welcome_again_status))
                        $template_obj->set_global("welcome_again_status", $registeredNewsletterUser[0]->welcome_again_status);
                }

                //update-iraj go cookie-to za denot za da zebelezime deka ovoj den sme go trackirale
                self::setDateTracking($currentDate);
            }
        }

    }

	//f-cija zapisuva vo cookie info za datum.
	public static function setDateTracking($date) 
	{
        //setiraj den koga sme go trackirale
        cookie::set("knp_day", $date, self::$secondsToLive);

    }

	//f-cija zapisuva vo cookie info za email.
	public static function setEmailTracking($email) 
	{
        //setiraj email koj sme go trackirale
        cookie::set("knp_email", $email, self::$secondsToLive);

    }

}

// End arr
