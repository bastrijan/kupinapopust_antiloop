<?php

defined('SYSPATH') OR die('No direct access allowed.') ;

/**
 * Calculate helper class.
 *
 * 
 */
class calculate_Core {

   //osnova za poenit za EDEN vaucer
  public static function points_osnova($price_discount, $provizija, $tip_danocna_sema, $ddv_stapka, $tip_ponuda) {
		
		$osnova = 0;
		
		//presmetaj ddv za eden vaucer
		$ddv = calculate::ddv($price_discount, $provizija, $tip_danocna_sema, $ddv_stapka, $tip_ponuda, 1);
		
		//presmetaj poeni
		//if($tip_danocna_sema == "Не ДДВ Обврзник" && $tip_ponuda == 'cela_cena')
		//	$osnova = 0;
		//else
			$osnova = $provizija - $ddv;
		
		return $osnova ;
  }
  
   
   //presmetaka na poenite zavisno kolku e brojot na vauceri
   public static function points($osnova, $broj_vauceri = 1) {
		
		$points_return = 0;
		$points = $osnova*0.1;
		
		$points_return = floor($points)*$broj_vauceri;
		
		return $points_return ;
		
		//old code
		//return floor($price*0.1) ;
	}
	


  //go vrakja presmetaniot DDV danok izrazeno vo denari spored kriteriumite. 
  //Ako se vnese broj na vauceri => go dava vkupniot danok za site vauceri. Ako ne se vnese broj na vauceri => go dava DDV-to za poedinecen vaucer
	public static function ddv($price_discount, $provizija, $tip_danocna_sema, $ddv_stapka, $tip_ponuda, $broj_vauceri = 1, $platena_reklama = 0) {
		
		$ddv = 0;
		$ddv_platena_reklama = 0;
		
		//$cena_na_ponudata = ($tip_ponuda == 'cena_na_vaucer') ? $provizija : $price_discount;
		
		switch ($tip_danocna_sema) {
			case "ДДВ Обврзник":
				
				//presmetaj ddv
				$ddv = (($provizija*$ddv_stapka)/(100 + $ddv_stapka))*$broj_vauceri;
					
				break;

			case "Не ДДВ Обврзник":
				
				//presmetaj ddv
				$ddv = (($provizija*$ddv_stapka)/(100 + $ddv_stapka))*$broj_vauceri;

				break;
			case "Ослободен согласно законот":
				
				//presmetaj ddv
				$ddv = (($provizija*$ddv_stapka)/(100 + $ddv_stapka))*$broj_vauceri;
				
				break;
			case "Промет остварен во странство":
				
				//presmetaj ddv
				//tuka ne se presmetuva ddv. ddv-to e nula
				
				break;
		}
		
		if($platena_reklama > 0)
		{
			$ddv_platena_reklama = 0;
			// $ddv_platena_reklama = (($platena_reklama*18)/(100 + 18));
		}
		
	
      return ($ddv + $ddv_platena_reklama) ;
  }


//go vrakja presmetaniot PROMET izrazeno vo denari spored kriteriumite	
	public static function promet($price_discount, $provizija, $tip_danocna_sema, $ddv_stapka, $tip_ponuda, $broj_vauceri, &$promet_18, &$promet_5, &$promet_osloboden, &$promet_stranstvo, $payed_with_card, &$promet_18_with_card, &$promet_5_with_card, &$promet_osloboden_with_card, &$promet_stranstvo_with_card) {
		
		$cena_na_ponudata = ($tip_ponuda == 'cena_na_vaucer') ? $provizija : $price_discount;
		
		switch ($tip_danocna_sema) {
			case "ДДВ Обврзник":
				
				//presmetaj promet
				if($ddv_stapka == 18)
				{
					$promet_18 += $cena_na_ponudata*$broj_vauceri;
					$promet_18_with_card += $cena_na_ponudata*$payed_with_card;
				}
				else
				{
					$promet_5 += $cena_na_ponudata*$broj_vauceri;
					$promet_5_with_card += $cena_na_ponudata*$payed_with_card;
				}
					
				break;

			case "Не ДДВ Обврзник":
				
				//presmetaj promet	
				if($ddv_stapka == 18)
				{
					$promet_18 += $cena_na_ponudata*$broj_vauceri;
					$promet_18_with_card += $cena_na_ponudata*$payed_with_card;
				}
				else
				{
					$promet_5 += $cena_na_ponudata*$broj_vauceri;
					$promet_5_with_card += $cena_na_ponudata*$payed_with_card;
				}

				break;
			case "Ослободен согласно законот":
				
				//presmetaj promet
				$promet_osloboden += $cena_na_ponudata*$broj_vauceri;
				$promet_osloboden_with_card += $cena_na_ponudata*$payed_with_card;
				
				break;
			case "Промет остварен во странство":
				
				//presmetaj promet
				$promet_stranstvo += $cena_na_ponudata*$broj_vauceri;
				$promet_stranstvo_with_card += $cena_na_ponudata*$payed_with_card;
				
				break;
		}
		
  }


}

// End arr
