<?php

defined('SYSPATH') OR die('No direct access allowed.');

/**
 * commonshow class.
 *
 * 
 */
class commonshow_Core {

    public static function image($count = 0, $max) {
        $return_string = "";

        return $return_string;

        if ($max > 0 && $count >= $max)
            $return_string = '<span class="red_kupuvaci"></span>';
        else
            $return_string = '<span class="green_kupuvaci"></span>';


        return $return_string;
    }

    public static function imageNewsletter($count = 0, $max) {
        $return_string = "";

		if ($max > 0 && $count >= $max)
			$return_string = '<span style="background: url('.Kohana::config('facebook.facebookSiteProtocol').'://' . Kohana::config('facebook.facebookSiteURL') . '/pub/img/layout/layout-sprite_popust.png) 0 -823px no-repeat; padding-left: 25px;"></span>';
		else
			$return_string = '<span style="background: url('.Kohana::config('facebook.facebookSiteProtocol').'://' . Kohana::config('facebook.facebookSiteURL') . '/pub/img/layout/layout-sprite_popust.png) 0 -217px no-repeat; padding-left: 25px;"></span>';


        return $return_string;
    }

    public static function isSoldOut($count = 0, $max) {

        $return_bool = false;

        if ($max > 0 && $count >= $max)
            $return_bool = true;


        return $return_bool;
    }

    public static function number($count = 0, $min, $max) {

        $return_string = $count;

        if (($min > 0 && $count < $min) || ($max > 0))
            $return_string .= '/';

        if (($min > 0 && $count < $min))
            $return_string .= $min;
        elseif ($max > 0 && $count >= $min)
            $return_string .= $max;

        return $return_string;
    }

    public static function text($count = 0) {
        $return_string = "";

        if ($count == 1)
            $return_string = "&nbsp;" . kohana::lang("prevod.Купувач");
        else
            $return_string = "&nbsp;" . kohana::lang("prevod.Купувачи");

        return $return_string;
    }

    public static function newoffer($time_stamp = 0) {
        $return_var = true;
        $period_sec = 345600; //96 casa (4dena)

        if (time() > ($time_stamp + $period_sec))
            $return_var = false;

        return $return_var;
    }

    public static function tooltip($count = 0, $min, $max) {

        $return_string = "Купен" . ($count == 1 ? "" : "и") . " " . ($count == 1 ? "" : "с") . "e $count ваучер" . ($count == 1 ? "" : "и") . " од понудата. ";

        if (($min > 0 && $count < $min))
            $return_string .= "Потреб" . ($min == 1 ? "ен" : "ни") . " " . ($min == 1 ? "" : "с") . "е минимум $min ваучер" . ($min == 1 ? "" : "и") . " за активирање на понудата.";
        elseif ($max > 0 && $count >= $min)
            $return_string .= "Понудата е лимитирана на $max ваучер" . ($max == 1 ? "" : "и") . ".";

        return $return_string;
    }

    public static function highlight($text, $keyword) {

        $return_string = "";

        /*
          preg_match_all('~\w+~', $words, $m);
          if(!$m)
          $return_string = $text;
          $re = '~\\b(' . implode('|', $m[0]) . ')\\b~i';
          $return_string =  preg_replace($re, '<strong>$0</strong>', $text);
         */
        //$return_string =  preg_replace("/\b($words)\b/i", '<span class="hilitestyle">\1</span>', $text);

        $keywords = explode(' ', trim($keyword));

        // $return_string = preg_replace("/" . implode('|', $keywords) . "/iu", "<span class=\"highlight_text\">$0</span>", $text);
		
		$return_string = preg_replace("/" . implode('|', $keywords) . "/iu", "<span style='background-color: #FF6600; color: #FFFFFF'>$0</span>", $text);
        
		return $return_string;
    }

    public static function Truncate($string, $length = 77) {

        //truncates a string to a certain char length, stopping on a word if not specified otherwise.
        if (mb_strlen($string) > $length) {
            //limit hit!
            $string = mb_substr($string, 0, ($length));

            /*
              $cnt_spaces = mb_substr_count($string, " ");

              if($cnt_spaces < 10)
              {
              $length -= 5;
              $string = mb_substr ($string,0,$length);
              }
             */

            $string .= '...';
        }
        return $string;
    }

    public static function search($controller_name = "index", $obj_ref, $type = "") {
        
         //new Profiler;
        if ($type == "android") {
			
			$obj_ref -> template -> share_dialog_show = true;
			$obj_ref -> template -> bridgeName = "CategoriesHandlerDeal";
                    
            $obj_ref->template->content = new View('alloffers/search_android');

        } else {
            $obj_ref->template->content = new View('alloffers/search');
        }

        $obj_ref->template->title = kohana::lang("prevod.website_title");
        
         //inicijalizacija na modeli
        $dealsModel = new Deals_Model();
        $search_keyword = $obj_ref->input->get("search_keyword", '');
        
        //Ajax request za paginacija na ponudite
        if (request::method() == 'post' && $type == "android") {
            
            $controller_name = 'index';
            $post = $obj_ref->input->post();
//            $search_keyword = $post['search_keyword'];
            $pagination_search_cnt = $post['pagination_search_cnt'];
            $dateTimeUnique = $post['dateTimeUnique'] ;
            $type_post = $post['type'];
            $deals_limit = 10;

            if ($type_post == "ajax") {

                $page = $pagination_search_cnt + 1;
                $offset = $deals_limit * $page;

                $allPaginationOffers = $dealsModel->getSearchResultsPagination($search_keyword, $controller_name, $offset, $deals_limit, $dateTimeUnique);
                $counter = count($allPaginationOffers);
                $html = "";

                $all_num = 0;
                foreach ($allPaginationOffers as $singleDeal) {
                    $all_num++;

                    //se pravi logika za vremeto da se prikaze
                    $timeLeft = strtotime($singleDeal->end_time) - time();
                    $timeStart = strtotime($singleDeal->start_time);
                    $timeEnd = strtotime($singleDeal->end_time);
                    $timeNow = time();

                    $html .= '<div class="deal-item" id="' . $singleDeal->id . '">';
                    $html .= '<img src="/pub/deals/' . $singleDeal->side_img . '" alt="' . strip_tags($singleDeal->title_mk_clean) . '"/>
                                                    <h3>' . commonshow::Truncate(strip_tags($singleDeal->title_mk_clean)) . '</h3>';

                    $html .= '<div class="item-description">';
                    $html .= '<span class="price">';

                    if ($singleDeal->tip == 'cena_na_vaucer')
                        $html .= $singleDeal->price_voucher;
                    else
                        $html .= $singleDeal->price_discount;

                    $html .= ' ден.</span>';
                    $html .= '<small class="today">' . commonshow::staticCountDown(strtotime($singleDeal->start_time), strtotime($singleDeal->end_time)) . '</small>';
                    $html .= '<span class="icon customers" style="color:  #333; font-weight: bold">';

                    $count = 0;
                    if (isset($voucherCount[$singleDeal->id]))
                        $count = $voucherCount[$singleDeal->id];

                    $allDealsTooltipTxt = commonshow::tooltip($count, $singleDeal->min_ammount, $singleDeal->max_ammount);

                    $html .= '<span title="' . $allDealsTooltipTxt . '">';
					
					if($count > 0)
					{
						$html .= commonshow::image($count, $singleDeal->max_ammount);
						
						if (commonshow::isSoldOut($count, $singleDeal->max_ammount))
							$html .= "<span style='color: red'>";
						
						$html .= commonshow::number($count, $singleDeal->min_ammount, $singleDeal->max_ammount);
						//                    $html .= commonshow::text($count);
						
						if (commonshow::isSoldOut($count, $singleDeal->max_ammount))
							$html .= "</span>";
					}
					
                    $html .= '</span>
                    </span>';

                    if ($singleDeal->price > 0) {
                        $html .= '<span class="discount">-';
                        $html .= '<strong>' . (int) round(100 - ($singleDeal->price_discount / $singleDeal->price) * 100) . '</strong>%';
                        $html .= '</span>';
                    }

                    if (commonshow::newoffer(strtotime($singleDeal->start_time))) {
                        $html .= '<span class="newoffer">нова</span>';
                    }

                    $html .= ' <div class="clear"></div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>';
                }

                $arr = array('status' => 'success', 'pagination_search_cnt' => $page, 'html' => $html, 'ajax_btn' => $counter);
                die(json_encode($arr));
            }
        } else {
            
            $dateTimeUnique = date("Y-m-d H:i:s");
            //variables		
            if ($search_keyword != "") {
                $sql_limit = "";

                //paging - kreiranje na objekt
                if ($controller_name == "past") {
                    $obj_ref->pagination = new Pagination(array(
                        'total_items' => $dealsModel->getCntSearchResults($search_keyword, $controller_name),
                        'uri_segment' => 4,
                        'query_string' => 'pageNo'
                    ));

                    $sql_limit = $obj_ref->pagination->sql_limit;
                }

                //povikuvanje na funcii
                if ($type == "android") {
                    $allOffers = $dealsModel->getSearchResultsPagination($search_keyword, $controller_name, 0, 10, $dateTimeUnique);
                } else {
                    $allOffers = $dealsModel->getSearchResults($search_keyword, $controller_name, $sql_limit);
                }


                //kreiranje na variabli
                $obj_ref->template->content->allOffers = $allOffers;
                $obj_ref->template->content->controller_name = $controller_name;
            } else {
                url::redirect("/" . $controller_name);
            }

            //die($search_keyword);
        }
        ///////// OMILENI /////////
		$cookie_value = cookie::get("kupinapopust_fav");
		
		if ( ! empty($cookie_value) ) {
			$favDeals = json_decode($cookie_value, true);
		} else {
			$favDeals = array();
		}
		$obj_ref->template->content->favDeals = $favDeals;
		/////// END - OMILENI /////


        // SHOPPING CART
        // zemanje na 'cookie'
        $cookie_value_shop_cart = cookie::get("kupinapopust_shop_cart");

        if ( ! empty($cookie_value_shop_cart)) {
            $shopCartDeals = json_decode($cookie_value_shop_cart, true);
        } else {
            $shopCartDeals = array();
        }
        $obj_ref->template->content->shopCartDeals = $shopCartDeals;
        // END - SHOPPING CART

            
		
        $obj_ref->template->content->dateTimeUnique = $dateTimeUnique;
    }

    public static function staticCountDown($timeStampStart, $timeStampEnd) {

        $return_string = "";

        // set 'ETC' date & time for event -- currently set for 12:01 am Feb.27th 2008 C.S.T. -- !!allow for server time!! ...
        //Adjust the event $integer to set time   
        $startStart = $timeStampStart;
        $eventEnd = $timeStampEnd;

        //Get Current Time from Server --- VERY IMPORTANT --- THIS MAY OR MAY NOT BE YOUR SITE'S LOCAL TIME.  
        $currentTime = strtotime("NOW");

        // variables are pretty self explainatory ....60 seconds in a minute 60 minutues in an hour 24 hours in a day, etc+++
        $totalsec = (($eventEnd - $currentTime));
        $totalmin = (($eventEnd - $currentTime) / 60);
        $totalhr = (($eventEnd - $currentTime) / 3600);
        $totalday = intval(($eventEnd - $currentTime) / 86400);
        $hr = intval($totalhr - ($totalday * 24));
        $mn = intval($totalmin - (($hr * 60) + ($totalday * 1440)));
        // seconds can be displayed ( int created but not displayed in page here)
        $sec = intval($totalsec - (($hr * 3600) + ($mn * 60) + (86400 * $totalday)));

        if ($currentTime < $startStart) {
            //$return_string .= '<span style="color:#1c8119"><strong>Старт на ' . date("H:i d.m.Y", $startStart) . '</strong></span>';
            $return_string .= '<span ><strong>Старт на ' . date("H:i d.m.Y", $startStart) . '</strong></span>';
        } else {
            if ($totalday > 0)
                //$return_string .= '<span style="color:#1c8119"><strong>';
                $return_string .= '<span ><strong>';
            else
                $return_string .= '<span style="color:#ff0000"><strong>';


            $return_string .= kohana::lang("prevod.Можете да купувате уште") . " ";


            //check if exists 
            if ($totalday > 0) {

                $return_string .= $totalday;

                if ($totalday > 1)
                    $return_string .= " дена";
                else
                    $return_string .= " ден";
            }

            if ($return_string != "")
                $return_string .= " ";

            //check if exists 
            if ($hr > 0) {

                if ($totalday > 0)
                    $return_string .= "и ";

                //echo "$totalday";
                $return_string .= $hr . " ч.";
            }

            if ($return_string != "")
                $return_string .= " ";

            //check if exists 
            if ($totalday == 0 && $mn > 0) {
                //echo "$totalday";
                $return_string .= $mn . " мин.";
            }

            $return_string .= "</strong></span>";
        }

        return $return_string;
    }

	public static function textRezerviranSo($voucher_type = "card", $confirmed = 0) {
		$return_string = "";
		
		if ($confirmed == 0)
			$return_string .= "Резер. ";
		else
			$return_string .= "Купен ";
			
		if ($voucher_type == "card")
			$return_string .= "со платежна картичка";
		elseif ($voucher_type == "cache")
			$return_string .= "во кеш";
			else
				$return_string .= "со уплатница";
				
		return $return_string;
	}

    public static function disableDealOption($voucher_count, $max_ammount, $valid_to, &$disabledTxt) {

        $return_bool = 0;

        //proverka za broj na vauceri
        if ($voucher_count >= $max_ammount and $max_ammount)
        {
            $return_bool = 1;
            $disabledTxt = "Распродадено!!!";
        }
            

        //proverka za broj na datumot
        if($return_bool == 0)
        {
            IF($valid_to <> '0000-00-00 00:00:00' && strtotime($valid_to) < time()  )
            {
                $return_bool = 1;
                $disabledTxt = "Истечено!!!";
            }
        }

        //formatiranje na tekstot
         if($return_bool == 1)
            $disabledTxt = "<span class='error_msg'>".$disabledTxt."</span>";

        return $return_bool;
    }

    public static function isLyonessUser() {

        $return_bool = false;

        if ( !empty($_SESSION["TRADEDOUBLER"]) || !empty($_COOKIE["TRADEDOUBLER"])  )
            $return_bool = true;


        return $return_bool;
    }

}

// End arr
