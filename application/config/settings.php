<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * Setiranja na prikaz na stranicni ponudi (side offers)
 */
$config['so_partner_height'] = 30;
$config['so_partner_letters'] = 60;
$config['so_title_height'] = 50;
$config['so_title_letters'] = 65;

/**
 * Google API browser key (Google Static Maps API, Google Maps JavaScript API )
 */
$config['google_api_browser_key'] = "AIzaSyD2XtsGMBz4swyetRXN9HITcHMCEg5riLw";

/**
 * LYONESS implementation settings
 */
$config['lyoness'] = array
	(
		'organization'      => '2074163',
		'checksumCode'          => '909718541',
		'currency'          => 'MKD',
		'event'          => '352146'
		);


/**
 * TinyPNG settings
 */
$config['tiny_png'] = array(
	'api_key' => '9B6xKspx9XrJKTYg5YldgPkqdG3WY0Ml',
	'limit' => 500 // na broj na kompresii vo eden mesec
);