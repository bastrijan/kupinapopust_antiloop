﻿
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Dansk Google Static Maps v2 generator</title>
  <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" />
<meta name="DC.language" content="da" />
<meta name="DC.date" content="2009-08-26" />
<meta name="DC.format" content="text/html" />
<meta name="DC.creator" content="Søren Johannessen" />
<meta name="author" content="Søren Johannessen " />
<meta http-equiv="imagetoolbar" content="false" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<meta http-equiv="Content-Language" content="da" />
<!--
 Copyright 2008 Google Inc. 
 Licensed under the Apache License, Version 2.0: 
 http://www.apache.org/licenses/LICENSE-2.0
 Dansk version af Søren Johannessen
 http://www.microformats.dk/ 
 -->


<script type="text/javascript" src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=ABQIAAAA8Fc6l2BNxv7FyOGXzH6kWBRWjAiP6fPANHgHYi4vH4QSd7Xl7hQJEI42nUKEtbVL5vKTDD51j6pazQ" ></script>
  

<style type="text/css">
td {
  border: 0px;
  padding: 0px;
  margin: 0px;
  height: 40px;
  font-size: 12px;
}
body {
 font-family: "Trebuchet MS", tahoma, verdana, helvetica, sans-serif;
 font-size: 12px;
}


	

</style>
<script>

var map;
var polys = [];
var markers = [];
var editingNow = false;

function updateImage() {
   var baseUrl = "http://maps.google.com/maps/api/staticmap?";

   var params = [];

   if (!document.getElementById("mapFitCHECKBOX").checked) {
     params.push("center=" + map.getCenter().lat().toFixed(6) + "," + map.getCenter().lng().toFixed(6));
     params.push("zoom=" + map.getZoom());
   }

   var markerSize = document.getElementById("markerSizeSELECT").value;
   var markerColor = document.getElementById("markerColorSELECT").value;
   var markerLetter = document.getElementById("markerLetterSELECT").value;
   var markerParams = [];
   if (markerSize != "") markerParams.push(markerSize);
   if (markerColor != "") markerParams.push(markerColor);
   if (markerLetter != "") markerParams.push(markerLetter);

   var markersArray = [];
   for (var i = 0; i < markers.length; i++) {
     if (document.getElementById("markerTypeCHECKBOX").checked) {
       markersArray.push(markers[i].getTitle().replace(" ", "+", "g"));
     } else {
       markersArray.push(markers[i].getLatLng().lat().toFixed(6) + "," + markers[i].getLatLng().lng().toFixed(6));
     }
   }
   if (markersArray.length) {
     var markersString = markerParams.join("|");
     if (markerParams.length) markersString += "|";
     markersString += markersArray.join("|");
     params.push("markers=" + markersString);
   }

   var polyColor = "color:0x" + document.getElementById("polyColorSELECT").value + document.getElementById("polyAlphaSELECT").value;
   var polyWeight = "weight:" + document.getElementById("polyWeightSELECT").value;
   var polyParams = polyColor + "|" + polyWeight;
   for (var i = 0; i < polys.length; i++) {
     var poly = polys[i];
     var polyLatLngs = [];
     for (var j = 0; j < poly.getVertexCount(); j++) {
       polyLatLngs.push(poly.getVertex(j).lat().toFixed(5) + "," + poly.getVertex(j).lng().toFixed(5));
     }
     params.push("path=" + polyParams + "|" + polyLatLngs.join("|"));
   }
   if (map.getCurrentMapType() == G_SATELLITE_MAP) {
     params.push("maptype=satellite");
   }
   if (map.getCurrentMapType() == G_HYBRID_MAP) {
     params.push("maptype=hybrid");
   }
   if (map.getCurrentMapType() == G_PHYSICAL_MAP) {
     params.push("maptype=terrain");
   }

   params.push("size=" + document.getElementById("mapWidthTEXT").value + "x" + document.getElementById("mapHeightTEXT").value);
   var img = document.createElement("img");
   //img.src = baseUrl + params.join("&") + "&sensor=false&key=ABQIAAAA-O3c-Om9OcvXMOJXreXHAxSsTL4WIgxhMZ0ZK_kHjwHeQuOD4xSbZqVZW2U_OWOxMp3YPfzZl2GavQ";
   img.src = baseUrl + params.join("&") + "&sensor=false&key=ABQIAAAA8Fc6l2BNxv7FyOGXzH6kWBRWjAiP6fPANHgHYi4vH4QSd7Xl7hQJEI42nUKEtbVL5vKTDD51j6pazQ";
   document.getElementById("staticMapIMG").innerHTML = "";
   document.getElementById("staticMapIMG").appendChild(img);

   document.getElementById("staticMapURL").innerHTML = baseUrl + params.join("&") + "&sensor=TRUE_OR_FALSE&key=DINGOOGLE_API_NØGLE_HER";
}

function load() {
  if (GBrowserIsCompatible()) {
    map = new GMap2(document.getElementById("map"));
    map.setCenter(new GLatLng(55.6679813, 12.5445557), 16);
    map.addMapType(G_PHYSICAL_MAP);
    map.addControl(new GSmallMapControl());
    map.addControl(new GMapTypeControl());
    GEvent.addListener(map, "moveend", function(marker, point) {
      updateImage();
    });
    GEvent.addListener(map, "maptypechanged", function(marker, point) {
      updateImage();
    });
    GEvent.addListener(map, "click", function(overlay, latlng) {
      if (latlng && !editingNow) {
        createPolyAt(latlng);
      }
      updateImage();
     });
    geocoder = new GClientGeocoder();
    updateImage();
  }
}


function createPolyAt(latlng) {
  var poly = new GPolyline([latlng]);
  map.addOverlay(poly);
  poly.enableDrawing();
  editingNow = true;
  GEvent.addListener(poly, "mouseover", function() {
    poly.enableEditing();
  });
  GEvent.addListener(poly, "mouseout", function() {
    poly.disableEditing();
  });
  GEvent.addListener(poly, "lineupdated", function() {
    updateImage();
  });
  GEvent.addListener(poly, "endline", function() {
    editingNow = false;
  });
  polys.push(poly);
}

function createMarkerAt(latlng, address) {
  var marker = new GMarker(latlng, {draggable:true, title: address});
  GEvent.addListener(marker, 'dragend', function() {
    updateImage();
  });
  map.addOverlay(marker);
  markers.push(marker);
}

function clearMarkers() {
  for (var i = 0; i < markers.length; i++) {
    map.removeOverlay(markers[i]);
  }
  markers = [];
  updateImage();
}

function clearPolys() {
  for (var i = 0; i < polys.length; i++) {
    map.removeOverlay(polys[i]);
  }
  polys = [];
  updateImage();
}

function showAddress() {
  var address = document.getElementById("addressTEXT").value;
  geocoder.getLatLng(
    address,
    function(latlng) {
      if (!latlng) {
        alert(address + " not found");
      } else {
        map.setCenter(latlng, 13);
        createMarkerAt(latlng, address);
        updateImage();
      }
    }
  );
}

function disableSelects() {
  document.getElementById("markerSizeSELECT").disabled = false;
  document.getElementById("markerColorSELECT").disabled = false;
  document.getElementById("markerLetterSELECT").disabled = false;

  var markerColor = document.getElementById("markerColorSELECT").value;
  var markerSize = document.getElementById("markerSizeSELECT").value;
  if (markerSize == "small" || markerSize == "tiny") { 
    document.getElementById("markerLetterSELECT").selectedIndex = 0;
    document.getElementById("markerLetterSELECT").disabled = true;
  } 
}

 
</script>
<link rel="stylesheet" type="text/css" href="http://code.google.com/css/dev_docs.css">
</head>
<body onload="load()">
<h2>Lav et statisk Google Maps (fast billedeformat) med stedmarkør og evt. med en linje på.</h2>

  <p>
  Denne generator gør brug af <a href="http://code.google.com/apis/maps/documentation/staticmaps">Google Static Maps API v2</a>. I stedet for at indsætte det sædvanlige interaktive Google Maps, hvor du kan zoome ind/ud og flytte rundt, så gør Google Static Maps API brug af et fast billede (kortudsnittet), som kaldes op via en URL. Dennne URL indsættes i HTML elementet &lt;img&gt; . Google Static Maps API v2 indlæses hurtigere (nyttigt til mobiltelefoner), og er velegnet til, hvis du kun har et enkelt punkt at vise på et kort.
  </p>

<h3>1) Indsæt stedmarkør lokalitet, kortets udseende samt evt. linje</h3>
<br/>
  
   <b>Sådan indsættes en stedmarkør:</b>
  <p>Søg efter den ønskede adresse i søgefeltet nedenfor og klik på "Indsæt stedmarkør her" knappen. Hvis markøren ikke passer helt korrekt, så kan du med musen trække den hen til det ønskede sted.
  <br/>
  <input type="text" size="38" id="addressTEXT" value="Enghavevej 40, 1674 København V" />
  <input type="button" value="Indsæt stedmarkør her" onclick="showAddress()"/>
  </p>
<br/>
<b>Ændring af kortets udseende</b>
  <p>Klik på den type af kort du vil bruge oppe i højre hjørne af Google Maps (kortet nedenfor). Brug zoom ind og ud kontrolknapperne i venstre side for det ønskede område.
  </p>
<br/>
  <b>Tilføj evt. en linje:</b>
  <p>Du kan evt også klikke på kortet for at angive et startpunkt for en linje. Klik derefter flere gange for at opbygge en linje.</p>
  <br/>
  <div id="map" style="width: 500px; margin-left: 20px; height: 300px"></div>
  <p>
  <input type="button" value="Nulstil markør" onclick="clearMarkers()"/>
  <input type="button" value="Nulstil sti" onclick="clearPolys();"/>

  </p>
<br clear="all"/>

<h3>2) Opsætning af størrelse samt forhåndsvisning af Google Maps</h3>
  <p>
  <table>
  <tr>
  <td><b>Kort</b></td>
  <td>Bredde (max. 640 px):
  <input type="text" size="3" id="mapWidthTEXT" value="500" onkeyup="updateImage()" onkeypress="updateImage()" />

  </td>
  <td>Højde (max. 640 px):
  <input type="text" size="3" id="mapHeightTEXT" value="300" onkeyup="updateImage()" onkeypress="updateImage()" />
  </td>
  </tr> 
  <tr>
  <td><b>Stedmarkør</b></td>
  <td>Størrelse: 
  <select id="markerSizeSELECT" onchange="disableSelects(); updateImage();">

    <option value="" SELECTED>normal</option>
    <option value="size:mid">medium</option>
    <option value="size:small">smal</option>
    <option value="size:tiny">tynd</option>
  </select>

  </td>
  <td>Farve
  <select id="markerColorSELECT" onchange="updateImage();">

    <option value="" SELECTED>normal</option>
    <option value="color:black">sort</option>
    <option value="color:brown">brun</option>
    <option value="color:green">grønn</option>
    <option value="color:purple">lilla</option>
    <option value="color:yellow">gul</option>

    <option value="color:blue">blå</option>
    <option value="color:gray">grå</option>
    <option value="color:orange">orange</option>
    <option value="color:red">rød</option>
    <option value="color:white">hvid</option>
  </select>

  </td>
  <td>Bogstav og tal:
   <select id="markerLetterSELECT" onchange="updateImage();">
    <option value="" SELECTED>Ingen</option>
    <option value="label:A">A</option>
    <option value="label:B">B</option>
    <option value="label:C">C</option>

    <option value="label:D">D</option>
    <option value="label:E">E</option>
    <option value="label:F">F</option>
    <option value="label:G">G</option>
    <option value="label:H">H</option>
    <option value="label:I">I</option>

    <option value="label:J">J</option>
    <option value="label:K">K</option>
    <option value="label:L">L</option>
    <option value="label:M">M</option>
    <option value="label:N">N</option>
    <option value="label:O">O</option>

    <option value="label:P">P</option>
    <option value="label:Q">Q</option>
    <option value="label:R">R</option>
    <option value="label:S">S</option>
    <option value="label:T">T</option>
    <option value="label:U">U</option>

    <option value="label:V">V</option>
    <option value="label:W">W</option>
    <option value="label:X">X</option>
    <option value="label:Y">Y</option>
    <option value="label:Z">Z</option>
    <option value="label:0">0</option>

    <option value="label:1">1</option>
    <option value="label:2">2</option>
    <option value="label:3">3</option>
    <option value="label:4">4</option>
    <option value="label:5">5</option>
    <option value="label:6">6</option>

    <option value="label:7">7</option>
    <option value="label:8">8</option>
    <option value="label:9">9</option>
  </select>


  </td>
  </tr>
  <tr>
  <td><b>Linie</b></td>
  <td>Farve:
  <select id="polyColorSELECT" onchange="updateImage();">

    <option value="000000">sort</option>
    <option value="8B4513">brun</option>
    <option value="008000">grøn</option>
    <option value="800080">lilla</option>
    <option value="FFCC00">gul</option>
    <option value="0000FF" SELECTED>blå</option> 
    <option value="808080">grå</option>

    <option value="ffa500">orange</option>
    <option value="ff0000">rød</option>
    <option value="ffffff">hvid</option>
  </select>
  </td>
  <td>
  Alpha:
  <select id="polyAlphaSELECT" onchange="updateImage();">

    <option value="19">10%</option>
    <option value="33">20%</option>
    <option value="4d">30%</option>
    <option value="66">40%</option>
    <option value="80" SELECTED>50%</option>
    <option value="99">60%</option>

    <option value="b3">70%</option>
    <option value="cc">80%</option>
    <option value="e6">90%</option>
    <option value="ff">100%</option>
  </select>
  </td>
  <td>

  Linjebredde:
  <select id="polyWeightSELECT" onchange="updateImage();">
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5" SELECTED>5</option>

    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
  </select>

  </td>
  </tr>
<tr>
  <td colspan="4"><input id="markerTypeCHECKBOX" type="checkbox" onchange="updateImage();">Gem markører som adresser, ikke koordinater (output vises i URL'en).</td>

  </tr>
  <tr>
  <td colspan="4"><input id="mapFitCHECKBOX" type="checkbox" onchange="updateImage();">Zoom automatisk ind for tilpasning af alle lag.</td>
  </tr>

  </table>
  </p>
  <div id="staticMapIMG" style="width: 500px; margin-left: 20px; height: 300px"></div>
<br clear="all"/>

<h3>3) Genereret URL</h3>
<p>Google Static Maps API kræver, at du har en gratis <a href="http://code.google.com/apis/maps/signup.html">API nøgle</a> tilknyttet din <a title="du får en Google konto her" href="http://www.google.com/accounts/">Google konto</a> og har angivet en URL-sti til dit webdomæne. Du kan få en Google Maps API <a
href="http://code.google.com/apis/maps/signup.html">her</a>, og tilføje denne som værdi i "<strong>&key=</strong>" parameteren i URLen nedenfor.

<br/>
<p>
<!--<input id="keyBUTTON" type="button" onclick="generateKey();"
value="Generate Key"/>-->
</p>
<pre id="staticMapURL" class="code">
</pre>
<h3>Opsætning af billedet</h3>
<p>Du skal nu indsætte den ovenstående generet URL i img elementet og dermed kalde det ønskede billede frem. Husk også at få Din Google Maps API nøgle tilføjet i enden.</p>
<pre class="code">&lt;img src=&quot;GENERERET_URL&quot; alt=&quot;alt teksten her&quot; /&gt;
</pre>
<p class="vcard">Generatoren lavet af <a class="fn url" href="http://www.microformats.dk/om/">Søren Johannessen</a> - Bygger på <a href="http://gmaps-samples.googlecode.com/svn/trunk/simplewizard/makestaticmap.html">Static Map Wizard v2</a> - <a title="Udgivet under Creative Commons licens" rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">CC</a> - 2009</p>
 <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-1891817-1";
urchinTracker();
</script>

</body>
</html>


