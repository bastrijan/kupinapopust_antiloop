<?php
//definirame konstanta SYSPATH za da moze da go iskoristime istiot config fajl za bazata kako za aplikacijata
define('SYSPATH' , "vrednost");

//setiranje na maximum vremeto za izvrsuvanje na scriptata
ini_set('max_execution_time', 600); //600 seconds = 10 minutes=

/***INCLUDE SECTION**/
require_once 'libs/Slim/Slim.php';
require_once 'include/functions.php';

//creiranje na object za service-ot
//povikuvanje na autoloader-ot na Slim
\Slim\Slim::registerAutoloader();

//kreiranje na objekot sto ke rabit so povicite
$app = new \Slim\Slim();

//ZEMANJE NA SITE ARTIKLI (sifrarnik na artikli)
$app->post('/:method', function($method) use ($app) {
    //echo $method;
	
	//namesti go header-ot
	$app->response()->header('Content-Type', 'application/json; charset=utf-8;');
	
	try 
	{
		//zemi go body-to od request-ot
		$request = \Slim\Slim::getInstance()->request();
		$body = $request->getBody();
		
		//napravi objekt od json-ot
		$body_obj = json_decode($body);

		//pocetok na json-ot
		$echo_str = '{';
		
		//kreiraj konekcija do baza
		$db = getConnection();
		
		switch ($method) {
			case "index_categories":
				$echo_str .= CreateJsonCategories($db);
				break;

			case "index_subcategories":
				$echo_str .= CreateJsonSubCategories($db, $body_obj->category_id);
				break;
				
			case "index_static_page":
				$echo_str .= CreateJsonStaticPage($db, $body_obj->static_page_id);
				break;
				
			case "index_list_staticpages":
				$echo_str .= CreateJsonListStaticPages($db);
				break;
				
			default:
				$app->response()->status(404);
				$echo_str .= '"error": "POST method not found"';
				break;
		}

		//unisti go objektot za bazata
		$db = null;

		
		//kraj na json-ot		
		$echo_str .= '}';
				
		//vrati response so soodvetnite podatoci
		echo $echo_str;	
	} 
	catch(PDOException $e) 
	{
		$app->response()->status(500);
		echo '{ 
				"error": "Internal database error = '.$e.'"
			  }
			  ';
	}

	
})->conditions(array('method' => '.+'));

/*
$app->get('/:method', function($method) use ($app) {
		$app->response()->status(404);
		echo '<?xml version="1.0" encoding="utf-8"?>
			  <response> 
				<error>GET method not found</error>
			  </response>
			  ';
})->conditions(array('method' => '.+'));
*/


//RUN-uvanje na service-ot
$app->run();


//funkicja za metodata: categories
function CreateJsonCategories($db_conn) {
	
	$date_string = date("Y-m-d H:i:s");
	
	$ret_var = '"error": "",
				"categories":
	';
	
	//zemi gi site artikli
	$sql = "SELECT 0 AS id, 'Сите понуди' AS name, (SELECT COUNT(id) AS cnt_deals FROM deals WHERE visible = 1 AND demo = 0 AND end_time > '".$date_string."') as cnt_deals
			UNION
			(
				SELECT c.id, c.name, COUNT(d.id) AS cnt_deals
					  FROM category AS c 
					  INNER JOIN deals AS d ON d.category_id = c.id AND d.visible = 1 AND d.demo = 0 AND d.end_time > '".$date_string."'
					  GROUP BY c.id
					  ORDER BY c.id ASC
			)
			ORDER By id
					";
				
	$stmt = $db_conn->query($sql);  
	$deals = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$ret_var .= json_encode($deals);

	return $ret_var;
}

//funkicja za metodata: subcategories
function CreateJsonSubCategories($db_conn, $category_id) {
	
	//za sekoj slucaj da ne preprakja nesto sto ne treba
	$category_id = (int) $category_id;
	
	$ret_var = '"error": "",
				"subcategories":
	';
	
	//zemi gi site artikli
	$sql = "SELECT sc.id, sc.category_id, sc.name, COUNT(d.id) AS cnt_deals
					  FROM subcategories AS sc 
					  INNER JOIN deals AS d ON d.category_id = sc.category_id AND d.subcategory_id = sc.id AND d.visible = 1 AND d.demo = 0 AND d.end_time > '".date("Y-m-d H:i:s")."'
					  WHERE sc.category_id = ".$category_id."
					  GROUP BY sc.id
					  ORDER BY sc.id ASC
					";
	$stmt = $db_conn->query($sql);  
	$deals = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$ret_var .= json_encode($deals);

	return $ret_var;
}


//funkicja za metodata: staticki strani
function CreateJsonStaticPage($db_conn, $static_page_id) {
	
	//za sekoj slucaj da ne preprakja nesto sto ne treba
	$static_page_id = (int) $static_page_id;
	
	$ret_var = '"error": "",
				"static_page":
	';
	
	//zemi gi site artikli
	$sql = "SELECT identifier, title_mk, content_mk
			FROM static_pages
			WHERE id = '$static_page_id'";
	$stmt = $db_conn->query($sql);  
	$static_page = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	die(print_r($static_page));
	
	$ret_var .= json_encode($static_page);

	return $ret_var;
}

//funkicja za metodata: categories
function CreateJsonListStaticPages($db_conn) {
	
	$ret_var = '"error": "",';
	
	//zemi gi site artikli
	$sql = "SELECT identifier, title_mk
			FROM static_pages 
			WHERE mobile_list_order > 0 AND id IN (2, 6, 9, 8, 11, 10)
			ORDER BY mobile_list_order ASC;
			
			SELECT identifier, title_mk
			FROM static_pages 
			WHERE mobile_list_order > 0 AND id IN (1, 18)
			ORDER BY mobile_list_order ASC;
			
			SELECT identifier, title_mk
			FROM static_pages 
			WHERE mobile_list_order > 0 AND id IN (4, 3)
			ORDER BY mobile_list_order ASC;
					";
	$stmt = $db_conn->query($sql);  
	
	$ret_var .= '"list_sp_poddrska":';
	$deals = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$ret_var .= json_encode($deals);
	
	$stmt->nextRowset();
	$ret_var .= ',"list_sp_zanas":';
	$deals = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$ret_var .= json_encode($deals);
	
	$stmt->nextRowset();
	$ret_var .= ',"list_sp_zastitaipravila":';
	$deals = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$ret_var .= json_encode($deals);

	return $ret_var;
}

?>