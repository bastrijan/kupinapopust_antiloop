<?php
//definirame konstanta SYSPATH za da moze da go iskoristime istiot config fajl za bazata kako za aplikacijata
define('SYSPATH' , "vrednost");

//setiranje na maximum vremeto za izvrsuvanje na scriptata
ini_set('max_execution_time', 600); //600 seconds = 10 minutes=

/***INCLUDE SECTION**/
require_once 'libs/Slim/Slim.php';
require_once 'include/functions.php';

//creiranje na object za service-ot
//povikuvanje na autoloader-ot na Slim
\Slim\Slim::registerAutoloader();

//kreiranje na objekot sto ke rabit so povicite
$app = new \Slim\Slim();

//ZEMANJE NA SITE ARTIKLI (sifrarnik na artikli)
$app->post('/:method', function($method) use ($app) {
    //echo $method;
	
	//namesti go header-ot
	$app->response()->header('Content-Type', 'application/json; charset=utf-8;');
	
	try 
	{
		//zemi go body-to od request-ot
		$request = \Slim\Slim::getInstance()->request();
		//$body = $request->getBody();
		
		//napravi objekt od json-ot
		//$body_obj = json_decode($body);

		//pocetok na json-ot
		$echo_str = '{';
		
		//kreiraj konekcija do baza
		//$db = getConnection();
		
		switch ($method) {
			case "upgrade_checkappversion":
				$echo_str .= CreateJsonCheckAppVersion();
				break;
				
			default:
				$app->response()->status(404);
				$echo_str .= '"error": "POST method not found"';
				break;
		}

		//unisti go objektot za bazata
		//$db = null;

		
		//kraj na json-ot		
		$echo_str .= '}';
				
		//vrati response so soodvetnite podatoci
		echo $echo_str;	
	} 
	catch(PDOException $e) 
	{
		$app->response()->status(500);
		echo '{ 
				"error": "Internal database error = '.$e.'"
			  }
			  ';
	}

	
})->conditions(array('method' => '.+'));

/*
$app->get('/:method', function($method) use ($app) {
		$app->response()->status(404);
		echo '<?xml version="1.0" encoding="utf-8"?>
			  <response> 
				<error>GET method not found</error>
			  </response>
			  ';
})->conditions(array('method' => '.+'));
*/


//RUN-uvanje na service-ot
$app->run();


//funkicja za metodata: categories
function CreateJsonCheckAppVersion() {
	
	$webSiteFullURL = getWebSiteFullURL();
		
	$ret_var = '"error": "",
				"upgrade_info":[
						{
							"version":"1.0",
							"url":"'.$webSiteFullURL.'/kupinapopust.apk"
						}
					
					]
	';
	


	return $ret_var;
}

?>