<?php
//definirame konstanta SYSPATH za da moze da go iskoristime istiot config fajl za bazata kako za aplikacijata
define('SYSPATH' , "vrednost");

//setiranje na maximum vremeto za izvrsuvanje na scriptata
ini_set('max_execution_time', 600); //600 seconds = 10 minutes=

/***INCLUDE SECTION**/
require_once 'libs/Slim/Slim.php';
require_once 'include/functions.php';

//creiranje na object za service-ot
//povikuvanje na autoloader-ot na Slim
\Slim\Slim::registerAutoloader();

//kreiranje na objekot sto ke rabit so povicite
$app = new \Slim\Slim();

//ZEMANJE NA SITE ARTIKLI (sifrarnik na artikli)
$app->post('/:method', function($method) use ($app) {
    //echo $method;
	
	//namesti go header-ot
	$app->response()->header('Content-Type', 'application/json; charset=utf-8;');
	
	try 
	{
		//zemi go body-to od request-ot
		$request = \Slim\Slim::getInstance()->request();
		$body = $request->getBody();
		
		//napravi objekt od json-ot
		$body_obj = json_decode($body);

		//pocetok na json-ot
		$echo_str = '{';
		
		//kreiraj konekcija do baza
		$db = getConnection();
		
		switch ($method) {
			case "deal_category_deals":
				$echo_str .= CreateJsonCategoriesDeals($db, $body_obj->category_id);
				break;

			case "deal_subcategory_deals":
				$echo_str .= CreateJsonCategoriesDeals($db, $body_obj->subcategory_id, "sub");
				break;
				
			default:
				$app->response()->status(404);
				$echo_str .= '"error": "POST method not found"';
				break;
		}

		//unisti go objektot za bazata
		$db = null;

		
		//kraj na json-ot		
		$echo_str .= '}';
				
		//vrati response so soodvetnite podatoci
		echo $echo_str;	
	} 
	catch(PDOException $e) 
	{
		$app->response()->status(500);
		echo '{ 
				"error": "Internal database error = '.$e.'"
			  }
			  ';
	}

	
})->conditions(array('method' => '.+'));

/*
$app->get('/:method', function($method) use ($app) {
		$app->response()->status(404);
		echo '<?xml version="1.0" encoding="utf-8"?>
			  <response> 
				<error>GET method not found</error>
			  </response>
			  ';
})->conditions(array('method' => '.+'));
*/


//RUN-uvanje na service-ot
$app->run();


//funkicja za metodata: deal od nekoja kategorija ili nekoja podkategorija
function CreateJsonCategoriesDeals($db_conn, $category_id, $type = "") {
	
	//za sekoj slucaj da ne preprakja nesto sto ne treba
	$category_id = (int) $category_id;
	
	$webSiteFullURL = getWebSiteFullURL();
	
	
	
	$ret_var = '"error": "",
				"deals":
	';
	
	//zemi gi site artikli
	$sql = "SELECT d.id, 
				   d.category_id,
				   d.subcategory_id,
				   d.title_mk_clean,
				   d.end_time,
				   d.min_ammount,
				   d.max_ammount,
				   d.max_per_person,
				   d.price,
				   d.price_discount,
				   CONCAT('".$webSiteFullURL."/pub/deals/', main_img) as main_img_url,
				   CONCAT('".$webSiteFullURL."/pub/deals/', side_img) as side_img_url,
				   d.tip,
				   d.price_voucher,
				   COUNT(v.id) AS cnt_vouchers 
				FROM deals AS d
				LEFT JOIN voucher AS v ON v.deal_id = d.id AND v.confirmed = 1
				WHERE d.visible = 1
				AND d.demo = 0
				AND d.end_time > '".date("Y-m-d H:i:s")."'
				AND (d.".$type."category_id = ".$category_id." OR ".$category_id." = 0)
				GROUP BY d.id 
				ORDER BY d.primary1 DESC, d.primary2 DESC, d.primary3 DESC, d.primary4 DESC, d.primary5 DESC, d.start_time DESC";
	$stmt = $db_conn->query($sql);  
	$deals = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$ret_var .= json_encode($deals);

	return $ret_var;
}

?>