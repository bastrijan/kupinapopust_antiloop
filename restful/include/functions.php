<?php	
	
//funkcija za povrzuvanje so baza
function getConnection() {
	/***INCLUDE SECTION**/
	require_once '../application/config/database.php';
	
	$dbh = new PDO("mysql:host=".$config['default']['connection']["host"].";dbname=".$config['default']['connection']["database"], $config['default']['connection']["user"], $config['default']['connection']["pass"]);	
	$dbh->exec("set names utf8");
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbh;
}

//funkcija sto ke go vrati celosnoto URL na kupinapopust.mk
function getWebSiteFullURL() {
	/***INCLUDE SECTION**/
	require_once '../application/config/facebook.php';
	
	return $config['facebookSiteProtocol']."://".$config['facebookSiteURL'];
}

//funkcija za podgotovka na string za prakjanje preku json
function prepareStrforJson($str) {

	$str_ret = "";
	
	$str_ret = trim(str_replace(array("\"", "\r\n", "\n\r", "\r", "\n"), array("", "", "", "", ""), strip_tags($str)));
	
	return $str_ret;
}


?>