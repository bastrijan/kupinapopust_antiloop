    <section id="bottom_area">
    
        <?php 
        
            // novalite_bottom_content();
            // do_action( 'novalite_socials' ); 
            
        ?>
        
        <footer id="footer" class="main">
                    <div class="za-firmi">  За сите фирми! Привлечете нови купувачи без никаков ризик.Креирајте понуда со попуст и започнете веднаш со промоција и продажба!
                        <br>Кликнете <a href="https://kupinapopust.mk/static/page/reklamiranje_partneri" class="orange" >овде</a> за повеќе детали.
                    </div>

                    <div class="footer-top-area">
                        <div class="container">
                            <div class="row row-wrap">
                                <div class="col-md-3">
                                    <a href="https://kupinapopust.mk">
                                        <img src="/wp-content/themes/nova-lite/inc/images/design/logo2.png" alt="logo" title="logo" class="logo width100 " alt="Kupinapopust Logo" />
                                    </a>
                                    
                                    <ul class="list list-social">
                                       <li>
                                            <a target="_blank" href="https://www.facebook.com/Kupinapopust.mk" data-toggle="tooltip" data-placement="top" title="Следи не на Facebook">
                                                <!-- <img align="center" src="/pub/img/fbround25x25.png"> -->
                                                <img src="/wp-content/themes/nova-lite/inc/images/design/fbround25x25.png" alt="Facebook" />
                                            </a>
                                        </li>
                                        <li>
                                            <a target="_blank" href="https://twitter.com/kupinapopust" data-toggle="tooltip" data-placement="top" title="Следи не на Twitter">
                                                <!-- <img align="center" src="/pub/img/tw25x25.png"> -->
                                                <img src="/wp-content/themes/nova-lite/inc/images/design/tw25x25.png" alt="Twitter" />
                                            </a>
                                        </li>
                                         <li>
                                            <a target="_blank" href="https://plus.google.com/+KupinapopustMk/posts" data-toggle="tooltip" data-placement="top" title="Следи не на Google+">
                                                <!-- <img align="center" src="/pub/img/googleplus.png"> -->
                                                <img src="/wp-content/themes/nova-lite/inc/images/design/googleplus.png" alt="Google+" />
                                            </a>
                                        </li>

                                        <li>
                                            <a target="_blank" href="https://www.youtube.com/kupinapopust" data-toggle="tooltip" title="Youtube">
                                                <!-- <img align="center" src="/pub/img/youtube.png"> -->
                                                <img src="/wp-content/themes/nova-lite/inc/images/design/youtube.png" alt="Youtube" />
                                            </a>
                                        </li>
                                        <li>
                                            <a target="_blank" href="https://instagram.com/kupinapopust" data-toggle="tooltip" title="Instagram">
                                                <!-- <img align="center" src="/pub/img/instagram.png"> -->
                                                <img src="/wp-content/themes/nova-lite/inc/images/design/instagram.png" alt="Instagram" />
                                            </a>
                                        </li>
                                        <li>
                                            <a target="_blank" href="https://www.linkedin.com/company/kupinapopust" data-toggle="tooltip" title="" data-original-title="LinkedIn">
                                                <!-- <img align="center" src="/pub/img/linkedin.png"> -->
                                                <img src="/wp-content/themes/nova-lite/inc/images/design/linkedin.png" alt="LinkedIn" />
                                            </a>
                                        </li>

                                    </ul>

                                    <!-- /////////////////////////////////// PREVZEMI ANDROID I IOS APLIKACIJA /////////////////////////////////// -->
                                    <!--
                                    <ul class="list list-social">
                                        <li>
                                            <a target="_blank" href="#" data-toggle="tooltip"  title="Превземи android апликација">
                                                <img src="/pub/img/androidIcon1.png" class="icon-size" alt="Android" /> Android
                                            </a>
                                        </li>
                                        <li>
                                            <a target="_blank" href="#" data-toggle="tooltip"  title="Превземи iOS апликација">
                                                <img src="/pub/img/appleIcon1.png" class="icon-size" alt="Iphone" /> Iphone
                                            </a>
                                        </li>
                                    </ul>
                                    -->
                                    
                                    <h5  class="vrab">Повеќе од <span class="big-orange">40 253</span> корисници купиле над <span class="big-orange">135 222</span> купони и заштедиле преку <span class="big-orange">57 095 832 ден.</span> Бидете и <span class="big-orange">Вие</span> еден од нив!</h5>
                                    
                                    <a href="https://kupinapopust.mk/static/page/nova_rabotna_pozicija" target="_blank">
                                        <!-- <img align="center" src="/pub/img/stolce1.png" class="icon-size"> -->
                                        <img style="vertical-align: middle" src="/wp-content/themes/nova-lite/inc/images/design/stolce1.png" class="icon-size"  alt="Stolche" />
                                        <!--  <i class="fa fa-user big-orange"></i>  -->
                                        Вработување!<br /> Биди и ти дел од kupinapopust тимот
                                    </a>
                                    
                                    <div class="gap-small"></div>
                                    
                                    <div>
                                        <a href="https://kupinapopust.mk/affiliate" target="_blank">
                                            <img class="img-responsive" src="/wp-content/themes/nova-lite/inc/images/design/affiliate-logo.jpg" alt="Affiliate програма на kupinapopust.mk" />
                                        </a>
                                    </div>

                                </div>
                                
                                <div class="col-md-3 hidden-xs hidden-sm">
                                    &nbsp;  
                                </div>
                                <div class="gap-small hidden-md hidden-lg"></div>

                                <div class="col-md-3">
                                    <!-- ////////////////  NE E ZAVRSHENO!!!  //////////////// --> 
                                    <h4>Сакам да ги добивам понудите на e-mail!</h4>
                                    <div class="gap-small"></div>
                                    <div class="box">
                                        <form id="newsletter_form">
                                            <div class="form-group mb10">
                                                <label>E-mail</label>
                                                <input type="text" class="form-control" placeholder="e-mail" id="email_field" name="mail" />
                                            </div>
                                         <!--  <p class="mb10">Ullamcorper porttitor torquent montes convallis lobortis urna</p> -->
                                            <input type="submit" class="btn btn-primary" value="Потврди" id="potvrdi_email" name="confirm_email" />
                                        </form>
                                        <script type="text/javascript">
                                        
                                            function validate(email) {
                                                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                                                if(reg.test(email) == false) {
                                                    alert("Невалидeн e-mail!")
                                                    return false;
                                                }
                                                return true;
                                            }
                                            jQuery(document).ready(function() {
                                                

                                                
                                                jQuery("#potvrdi_email").click(function (){

                                                    

                                                        if (jQuery("#email_field").val() != '' && validate(jQuery("#email_field").val()) ) {
                                                            jQuery.post(
                                                                /*
                                                                "http://kupinapopust.local/ajax/newsletter",
                                                                */
                                                                "https://kupinapopust.mk/ajax/newsletter",
                                                                
                                                                jQuery("#newsletter_form").serialize(),
                                                                function(data) {
                                                                    if (data.resp == 'success') {

                                                                        alert("Успешно го пријавивте вашиот e-mail!");

                                                                        /*
                                                                        jQuery().toastmessage('showToast', {
                                                                            // inEffectDuration:  600,   // in effect duration in miliseconds
                                                                            // stayTime:         3000,   // time in miliseconds before the item has to disappear
                                                                            text     : "Успешно го пријавивте вашиот e-mail!",
                                                                            sticky   : false,
                                                                            position : 'top-right',      // top-left, top-center, top-right, middle-left, middle-center, middle-right
                                                                            type     : 'success',        // notice, warning, error, success
                                                                            // closeText:         '',    // text which will be shown as close button,
                                                                                                         // set to '' when you want to introduce an image via css
                                                                            // close:            null    // callback function when the toastmessage is closed
                                                                        });
                                                                        */

                                                                        

                                                                    } else {

                                                                        alert("Вашиот e-mail веќе е пријавен и вие треба да добивате понуди од Kupinapopust!");
                                                                        
                                                                        /*
                                                                        jQuery().toastmessage('showToast', {
                                                                            text     : "Вашиот e-mail веќе е пријавен и вие треба да добивате понуди од Kupinapopust",
                                                                            sticky   : false,
                                                                            position : 'top-right',
                                                                            type     : 'notice'
                                                                        });
                                                                        */

                                                                        
                                                                    }      
                                                                },
                                                                "json"
                                                            );
                                                            defaultText = '';
                                                            jQuery("#email_field").val(defaultText);
                                                        }
                                                        return false;

                                                });

                                                
                                                jQuery("#email_field").keypress(function(e) {
                                                    if(e.keyCode == 13) {
                                                        jQuery('#potvrdi_email').click();
                                                        return false;
                                                    }
                                                });
                                                
                                                jQuery("#email_field").focus(function (){
                                                    defaultText = 'Внесете ја вашата e-mail адреса';
                                                    if (jQuery(this).val() == defaultText) {
                                                        jQuery(this).val("");
                                                    }
                                                })
                                                
                                                jQuery("#email_field").blur(function (){
                                                    defaultText = 'Внесете ја вашата e-mail адреса';
                                                    if (jQuery(this).val() == "") {
                                                        jQuery(this).val(defaultText);
                                                    }
                                                })



                                            });
                                            

                                            
                                        </script>
                                    </div>
                                    <br />  
                                                           
                                    <a class="btn-block" href="https://kupinapopust.mk/static/page/zanas" target="_blank"><i class="fa fa-group"></i> За нас <?php //print kohana::lang("prevod.За нас"); ?></a>
                                    <a class="btn-block" href="https://kupinapopust.mk/static/page/kontakt" target="_blank"><i class="fa fa-home"></i> Контакт <?php //print kohana::lang("prevod.Контакт"); ?></a>
                                    <a class="btn-block" href="https://kupinapopust.mk/static/page/cestiprasanja" target="_blank"><i class="fa fa-question"></i> Чести прашања? <?php //print Kohana::lang("prevod.Чести прашања?"); ?></a>
                                    <a class="btn-block" href="https://kupinapopust.mk/static/page/kakorabotime" target="_blank"><i class="fa fa-star"></i> Како работиме? <?php //print Kohana::lang("prevod.Како работиме?"); ?></a>
                                    <!-- <a class="btn btn-primary mt10" href=""> Kupinapopust.mk БЛОГ</a> -->
                                </div>
                                <div class="gap-small hidden-md hidden-lg"></div>
                                <div class="col-md-3">
                                    <h4>Информации</h4>
                                    <div class="gap-small"></div>
                                    
                                        <a class="btn-block slikicki" href="https://kupinapopust.mk/card/instructions" target="_blank">Плаќање со уплатница</a>
                                        <a class="btn-block slikicki" href="https://kupinapopust.mk/static/page/reklamiranje_partneri" target="_blank">За сите фирми! Промовирај се на kupinapopust</a>
                                        <a class="btn-block slikicki" href="https://kupinapopust.mk/static/page/prvpatkupuvam" target="_blank">Купувате преку нас првпат! Што треба да знаете?</a>
                                        <a class="btn-block slikicki" href="https://kupinapopust.mk/static/page/kakodopoeni" target="_blank">Како до повеќе kupinapopust поени?</a>
                                        <a class="btn-block slikicki" href="https://kupinapopust.mk/card" target="_blank">Подарок kupinapopust електронска картичка</a>

                                    <?php
                                    //require APPPATH . 'views/layouts/static_links.php';
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="gap-medium"></div>

                    <div class="footer-copyright">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-7">
                                      <a href="https://kupinapopust.mk/rss">RSS Feed</a> |
                                    <a href="https://kupinapopust.mk/static/page/licnipodatoci/">Заштита на личните податоци<?php //print kohana::lang("prevod.Заштита на личните податоци"); ?></a> |
                                    <a href="https://kupinapopust.mk/static/page/pravila/">Правила и Услови за користење на веб страната<?php //print kohana::lang("prevod.Правила и Услови за користење на веб страната"); ?></a>
                                    <p>2011 - 2017<?php //echo date("Y"); ?> © Kupinapopust.mk Сите права задржани<?php //print kohana::lang("prevod.Сите права задржани"); ?></p> 
                                </div>
                                <div class="col-md-4 col-md-offset-1">
                                    <div class="pull-right">
                                        <div class="gap-mini hidden-md hidden-lg"></div>
                                        <img src="/wp-content/themes/nova-lite/inc/images/design/Resposnive-design.png" style="height: 38px; width: initial" alt="IF Kreativa" />
                                        Design by <a href="http://www.ifkreativa.com">IF Kreativa</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>






        <!--
            <div class="container">
            
                <div class="row" >
                     
                    <div class="span12 copyright" >
                    
                        <p>
                            <?php //if (novalite_setting('novalite_copyright_text')): ?>
                               <?php //echo stripslashes(novalite_setting('novalite_copyright_text','html')); ?>
                            <?php //else: ?>
                              <?php //_e('Copyright','novalite'); ?> <?php //echo get_bloginfo("name"); ?> <?php //echo date("Y"); ?> 
                            <?php //endif; ?> 
                            | <?php //_e('Theme by','novalite'); ?> <a href="http://www.themeinprogress.com/" target="_blank">Theme in Progress</a> |
                            <a href="<?php //echo esc_url( __( 'http://wordpress.org/', 'novalite' ) ); ?>" title="<?php //esc_attr_e( 'A Semantic Personal Publishing Platform', 'novalite' ); ?>" rel="generator"><?php //printf( __( 'Proudly powered by %s', 'novalite' ), 'WordPress' ); ?></a>
                        
                        </p>
                    
                    </div>
                        
                </div>
        
            </div>
        -->
        </footer>
    
    </section>

</div>

<div id="back-to-top">
<a href="#" style=""><i class="icon-chevron-up"></i></a> 
</div>
    
<?php wp_footer() ?>  
 
</body>

</html>