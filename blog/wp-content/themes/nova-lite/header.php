<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
   
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.2, user-scalable=yes" />

<!--[if IE 8]>
    <script src="<?php echo get_template_directory_uri(); ?>/inc/scripts/html5.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/inc/scripts/selectivizr-min.js" type="text/javascript"></script>
<![endif]-->

<!-- Google fonts -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300' rel='stylesheet' type='text/css'>
   

<?php wp_head(); ?>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />

<!--
<link rel="stylesheet" href="/wp-content/themes/nova-lite/inc/css/jquery.toastmessage.css" media="screen" />
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.toastmessage.js'></script>
-->

</head>

<body <?php body_class('custombody'); ?>>

<div id="wrapper">
    <!--
    <header id="header">
    
        <div class="container">
        
            <div class="row">
                
                <div class="span3" >
                   
                    <div id="logo">
                            
                        <a href="<?php //echo esc_url( home_url( '/' ) ); ?>" title="<?php //bloginfo('name') ?>">
                                
                            <?php 
                                            
                                // if ( novalite_setting('novalite_custom_logo') ):
                                //     echo "<img src='".novalite_setting('novalite_custom_logo','url')."' alt='".__('Logo','novalite')."'>"; 
                                // else: 
                                //     bloginfo('name');
                                // endif; 
                                
                            ?>
                                    
                        </a>
                                
                    </div>
                    
                </div>
    
                <div class="span9" >
                  
                    <nav id="mainmenu">
                 
                        <?php //wp_nav_menu( array('theme_location' => 'main-menu', 'container' => 'false','depth' => 3  )); ?>
                 
                    </nav> 
                                   
                </div>
                
            </div>
            
        </div>
    
    </header>
    -->

    <header id="header" class="main">
        <div class="container width100 my-search-area">
            <div class="row ml0 mr0">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <a href="/" class="logo mt5 mb5 text-center" data-toggle="tooltip" data-placement="bottom" data-title="Почетна страна">
                        <img src="/wp-content/themes/nova-lite/inc/images/design/logoBlog.png" alt="Kupinapopust.mk logo" height="40"/>
                    </a>
                </div>
                
                <div class="col-lg-5 col-md-5 col-sm-6 pt10 col-xs-12 search-area search-area-white ">

                <!--
                    <form action="/index/search">
                    <label>
                        <span class="menu-touch-button hidden-md hidden-lg"></span>
                        <span class="hidden-xs hidden-sm" style="font-size: 15px; margin-top: 15px;">Барам понуда за</span>
                    </label>
                    <div class="search-area-division search-area-division-input">
                        <input class="form-control" type="text" placeholder="Банско" name="search_keyword" value="<?php //echo $search_keyword; ?>" />
                    </div>
                     <button class="btn btn-white btn-mini btn-my" type="submit"><i class="fa fa-search"></i></button>
                     </form>
                -->     
                </div>
        
                <div class="col-lg-4 col-md-4 pr0 col-sm-6 col-xs-12">
                    <!-- LOGIN REGISTER LINKS -->
                    <ul class="login-register">
                      <!--  кога си најавен да ти се покаже ова-->


                        <!--Кошничка-->
                        <!--
                        <li class="shopping-cart">
                            <a alt="" href="#"><i class="fa fa-shopping-cart"></i><span class="badge my-badge rounded-x">3</span></a> 
                            <div class="shopping-cart-box">
                                <ul class="shopping-cart-items">
                                    <li>

                                        <a href="#">
                                            <img src="/pub/img/amaze_70x70.jpg" alt="Image Alternative text" title="AMaze">
                                            <h5>Масажа</h5><span class="shopping-cart-item-price">150 ден.</span>
                                        </a>
                                    </li>
                                    <li>

                                        <a href="#">
                                            <img src="/pub/img/gamer_chick_70x70.jpg" alt="Image Alternative text" title="Gamer Chick">
                                            <h5>Банско </h5><span class="shopping-cart-item-price">300 ден.</span>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="list-inline text-center">
                                    <li><a href="#"><i class="fa fa-check-square"></i> Кон плаќање</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        -->


                        <!-- OMILENI
                        <li class="shopping-cart">
                            <a id="fav-menu-link" href="/index/favourites">
                                <i class="fa fa-star" style="<?php //if($controller == "index" && $action == "favourites") echo 'opacity: 1;'; ?>"></i><span class="badge my-badge rounded-x"></span>
                            </a>
                        </li>
                        -->

                        <!--<li class="shopping-cart"><a href="page-cart.html"><i class="fa fa-user"></i>Мој профил</a></li>-->

                        <?php if (empty($user_email)) { ?>
                            <li class="ml20"><a class="" href="/wp-login.php" target="_blank" data-effect="mfp-move-from-top" style="<?php if($controller == "customer" && $action == "login") echo 'font-weight: bold;'; ?>"><i class="fa fa-sign-in" style="<?php if($controller == "customer" && $action == "login") echo 'opacity: 1;'; ?>"></i>Најави се</a></li>
                        <?php } else { ?>
                            <!--<li class="shopping-cart"><a href="/customer" style="<?php //if($controller == "customer" ) echo 'font-weight: bold;'; ?>"><i class="fa fa-user fav-menu-active" style="<?php //if($controller == "customer" ) echo 'opacity: 1;'; ?>"></i>Мој профил</a></li>
                            --> 
                        <?php } ?> 

                        <li><a href="https://kupinapopust.mk/static/page/kontakt" target="_blank" style="<?php if($controller == "static" && $action == "page" && $arguments[0] == "kontakt") echo 'font-weight: bold;'; ?>">КОНТАКТ</a></li>
                        <!--<li><a class="popup-text" href="#" data-effect="mfp-move-from-top"><i class="fa fa-edit"></i>Регистрирај се</a></li>-->
                    
                    </ul>
                </div>

            </div>


        </div>
    </header>