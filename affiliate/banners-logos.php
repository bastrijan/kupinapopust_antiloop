<?php
include('auth/startup.php');
include('data/data-functions.php');
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();
$url = filter_input(INPUT_POST, 'url', FILTER_SANITIZE_STRING);

//$enabled_emails = check_emails($owner);

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $meta_description; ?>">
    <meta name="author" content="">

    <title><?php echo $meta_title; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/base.css" rel="stylesheet">
    <link href="assets/comp/breadcrumbs/breadcrumbs.css" rel="stylesheet">
    <link href="assets/css/custom.css" rel="stylesheet">

    <!-- Elusive and Font Awesome Icons -->
    <link href="assets/fonts/css/elusive.css" rel="stylesheet">
    <link href="assets/fonts/css/fa.css" rel="stylesheet">
    <!-- Webfont -->
    <link href='https://fonts.googleapis.com/css?family=Archivo+Narrow:400,400italic,700,700italic' rel='stylesheet'
          type='text/css'>
    <!-- Animation Effects -->
    <link href="assets/css/plugins/hover.css" rel="stylesheet" media="all">
    <!-- SweetAlert Plugin -->
    <link href="assets/css/plugins/sweetalert.css" rel="stylesheet" media="all">
    <!-- Datatables Plugin -->
    <link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css" rel="stylesheet" media="all">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
     
        #share-buttons img {
        width: 35px;
        padding: 5px;
        border: 0;
        box-shadow: 0;
        display: inline;
        }
     
    </style>

</head>

<body>
<!-- Start Top Navigation -->
<?php include('assets/comp/top-nav.php'); ?>
<!-- Start Main Wrapper -->
<div id="wrapper">
    <!-- Side Wrapper -->
    <div id="side-wrapper">
        <ul class="side-nav">
            <?php include('assets/comp/side-nav.php'); ?>
        </ul>
    </div><!-- End Main Navigation -->

    <!-- YOUR CONTENT GOES HERE -->
    <div id="page-content-wrapper">
        <div class="container-fluid">


            <?php if ($admin_user == '1') { ?>
                <div class="row">
                    <!-- Start Panel -->
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-heading panel-primary">
                                <span class="title">Додади нов банер</span>
                            </div>
                            <div class="panel-content">
                                <?php echo $message; ?>
                                <form action="data/upload-data" method="post" enctype="multipart/form-data" class="upload-form">
                                    Големина на банер:
                                    <select name="adsize" class="form-control input-md">
                                        <option value="0">Select Size</option>
                                        <option value="1">Mobile leaderboard (320x50)</option>
                                        <option value="2">Large mobile banner (320x100)</option>
                                        <option value="3">Medium Rectange (300x250)</option>
                                        <option value="4">Rectange (180x150)</option>
                                        <option value="5">Wide Skyscraper (160x600)</option>
                                        <option value="6">Leaderboard (728x90)</option>
                                    </select>
                                    <br>
                                    Линк на банер:  <input id="banner_link" name="banner_link" type="text"  class="form-control input-md">
                                    <br><br>
                                    <input type="file" name="file" size="25"/><br>
                                    <input type="submit" name="submit" value="Upload" class="btn btn-primary"/>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- End Panel -->
                </div>
            <?php } ?>


            <?php if ($admin_user == 0) { ?>
                <div class="row">
                    <!-- Start Panel -->
                    <div class="col-lg-12">

                        <div class="panel">
                            <div class="panel-heading panel-warning">
                                <span class="title">Линк за споделување</span>
                            </div>
                            <div class="panel-content">
                                <div class="row">
                                    <?php 
                                        echo '<div class="col-md-12">';
                                        echo  $main_url . '?ref=' . $owner ;
                                        echo '</div>';
                                     ?>
                                    <div class="col-md-12">    
                                        <!-- I got these buttons from simplesharebuttons.com -->
                                        <div id="share-buttons">

                                            <!-- Facebook -->
                                            <a href="https://www.facebook.com/sharer.php?u=<?php echo $main_url . '?ref=' . $owner;?>" target="_blank">
                                                <img src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" />
                                            </a>
                                            
                                            <!-- Google+ -->
                                            <a href="https://plus.google.com/share?url=<?php echo "https://kupinapopust.mk" . '?ref=' . $owner;?>" target="_blank">
                                                <img src="https://simplesharebuttons.com/images/somacro/google.png" alt="Google" />
                                            </a>
                                            
                                            <!-- Twitter -->
                                            <a href="https://twitter.com/share?url=<?php echo $main_url . '?ref=' . $owner;?>" target="_blank">
                                                <img src="https://simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Panel -->
                </div>
            <?php } ?>



            <?php if (0) { ?>
                <div class="row">
                    <!-- Start Panel -->
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-heading panel-warning">
                                <span class="title">Email маркетинг</span>
                            </div>
                            <div class="panel-content">

                                    <form action="data/upload_mails" method="post" enctype="multipart/form-data">
                                        Почитувани,
                                        <br>Ве молиме, вметнете emails со префикс <b>.txt</b>.
                                        <br>Редоследот на emails треба да биде:
                                        <br><br>test@test.com
                                        <br>test1@test.com
                                        <br>test2@test.com
                                        <br><br>
                                        односно да има нов ред меѓу нив.
                                        <br>Откако ќе селектирате Emails" ве молиме кликнете на "Upload emails"
                                        <br><br>
                                        <input type="hidden" name="owner" id="owner" value="<?php echo $owner; ?>"
                                               style="display:none;">


                                        <input type="file" name="fileToUpload" id="fileToUpload"
                                               >

                                        <br>
                                        <input type="submit" value="Upload emails" name="submit">
                                    </form>
                              </div>
                        </div>
                    </div>
                    <!-- End Panel -->
                </div>
            <?php } ?>







            <?php if ($admin_user == 1 && 0) { ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-heading panel-warning" style="text-align: center;height: 100%;">
                                <span class="title">За генерирање на нов Iframe со нови понуди(доколку се смениле топ понудите) и за додавање на iframe за нови affiliates.<br></span>
                            </div>
                            <div class="panel-content" style="text-align: center;   height: 100%;">

                                <form class="form-horizontal" method="post" action="data/generate_iframe">
                                    <input type="submit" class="btn btn-default" value="Generate Iframe  Link">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>


            <?php  //if ($admin_user == 1) { ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel">
                                    <div class="panel-heading panel-warning">
                                        <span class="title"><?php echo $lang['BANNERS_LOGOS']; ?></span>
                                    </div>
                                    <div class="panel-content">
                                        <div>
                                            <div id="status"></div>
                                            <table id="users" class="row-border" cellspacing="0" width="100%">
                                                <thead>
                                                <tr>
                                                    <th><?php echo $lang['BANNER']; ?></th>
                                                    <th>Димензии</th>
                                                    <th><?php echo $lang['CODE']; ?></th>
                                                    <?php if($admin_user=='1') { ?>
                                                        <th><?php echo $lang['ACTION']; ?></th>
                                                    <?php }//if($admin_user=='1') { ?>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <?php
                                                banner_table($owner, $domain_path, $main_url, $admin_user); ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            <?php //} ?>    




            <?php  if ($admin_user == 0) { ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-heading panel-warning">
                                <span class="title">Имате датабаза на е-маил subscriber-и? Превземете линк и испратете им е-маил за да ја зголемите продажбата.</span>
                            </div>
                            <div class="panel-content">


                                <div><a target="_blank" href="<?php echo $main_url.'/index/newsletter?ref='.$owner; ?>">Сите наши понуди</a></div>

                                <div><a target="_blank" href="<?php echo $main_url.'/index/newsletter_bestsellers?ref='.$owner; ?>">Најпродавани</a></div>

                                <div><a target="_blank" href="<?php echo $main_url.'/index/newsletter_main_offers?ref='.$owner; ?>">Најнови</a></div>

                            </div>
                        </div>
                        <!-- End Panel -->
                    </div>
                </div>
            <?php } ?>


        </div>
    </div>
        <!-- End Page Content -->

</div><!-- End Main Wrapper  -->

    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>
    <?php if ($admin_user == '1') { ?>
        <script>$('#side-wrapper > ul > li:nth-child(4)').css('background', '#FF6701');</script> <?php }    else { ?>
        <script>$('#side-wrapper > ul > li:nth-child(3)').css('background', '#FF6701');</script> <?php } ?>
    <script>$('#breadcrumbs').html('<p id="breadcrumb"><a href="dashboard">Home</a>Marketing Materials</p>');</script>
    <!-- Bootstrap -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Base JS -->
    <script src="assets/js/base.js"></script>
    <!-- SweetAlert -->
    <script src="assets/js/plugins/sweetalert.min.js"></script>
    <!-- Datatables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#users').DataTable();
            $('#emails').DataTable();

            <?php
                if(isset($_SESSION['action_saved'])){ echo 'swal("Awesome Work!", "Your changes have been applied!", "success")';}
                if(isset($_SESSION['error'])){ echo 'swal("Error", "", "error")';}
                unset($_SESSION['action_saved']);
                unset($_SESSION['error']);
            ?>
        
        });


    </script>


</body>
</html>
