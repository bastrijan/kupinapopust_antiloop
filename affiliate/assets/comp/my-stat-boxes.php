<?php 
//$cpc_on = cpc_on();
$col = '4'; 
?>

<div class="row">
    <div class="col-lg-<?php echo $col; ?> col-md-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-xs-12">
        <div class="stat-box" style="    height: 100px;">
            <a href="">
                <!--
                <div class="stat-icon stat-default hvr-bounce-in">
                    <i class="fa-rocket"></i>
                </div>
                -->
                <div class="stat-data">
                    <h2><?php my_total_referrals($owner); ?> <br><span
                            class="stat-info"><?php echo $lang['MY_TOTAL_REFERRED_VISITS']; ?></span></h2>
                </div>
            </a>
        </div>
    </div>

    <div class="col-lg-<?php echo $col; ?> col-md-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-xs-12">
        <div class="stat-box" style="    height: 100px;">
            <a href="#">
                <!--
                <div class="stat-icon stat-danger hvr-bounce-in">
                    <i class="fa-money"></i>
                </div>
                -->
                <div class="stat-data">
                    <h2><?php my_count_sales($owner); ?> <br><span
                            class="stat-info"><?php echo $lang['Total_sales']; ?></span></h2>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-<?php echo $col; ?> col-md-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-xs-12">
        <div class="stat-box" style="    height: 100px;">
            <a href="#">
                <!--
                <div class="stat-icon stat-danger hvr-bounce-in">
                    <i class="fa-money"></i>
                </div>
                -->
                <div class="stat-data">
                    <h2><?php my_unique_count_sales($owner); ?> <br><span
                            class="stat-info"><?php echo $lang['Unique_refs']; ?></span></h2>
                </div>
            </a>
        </div>
    </div>
</div>
<div class="row">    
    <div class="col-lg-<?php echo $col; ?> col-md-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-xs-12">
        <div class="stat-box" style="height:100px !important;">
            <a href="#">
                <!--
                <div class="stat-icon stat-primary hvr-bounce-in">
                    <i class="fa-money"></i>
                </div>
                -->
                <div class="stat-data">
                    <h2><?php totalPointsNewBuyerNotTransferred($owner); ?> <span
                            class="stat-info">Непрефрлени "kupinapopust" поени</span></h2>
                </div>
            </a>
        </div>
    </div>

    <div class="col-lg-<?php echo $col; ?> col-md-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-xs-12">
        <div class="stat-box" style="height:100px !important;">
            <a href="#">
                <!--
                <div class="stat-icon stat-danger hvr-bounce-in">
                    <i class="fa-money"></i>
                </div>
                -->
                <div class="stat-data">
                    <h2><?php totalPointsNewBuyerTransferred($owner); ?> <span
                            class="stat-info">Префрлени "kupinapopust" поени</span></h2>
                </div>
            </a>
        </div>
    </div>

    <?php 

    if ($admin_user == '1') { ?>
        <div class="col-lg-<?php echo $col; ?> col-md-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-xs-12">
            <div class="stat-box" style="height:100px !important;">
                <a href="my-sales">
                    <!--
                    <div class="stat-icon stat-danger hvr-bounce-in">
                        <i class="fa-money"></i>
                    </div>
                    -->
                    <div class="stat-data">
                        <h2><?php my_total_sales($owner); ?> <span
                                class="stat-info"><?php echo $lang['SALES']; ?></span></h2>
                    </div>
                </a>
            </div>
        </div> <?php } ?>
    <!--	<div class="col-lg---><?php //echo $col;?><!-- col-md---><?php //echo $col;?><!-- col-sm-6 col-xs-12">-->
    <!--		<div class="stat-box">-->
    <!--			<a href="my-sales">-->
    <!--				<div class="stat-icon stat-success hvr-bounce-in">-->
    <!--					<i class="fa-dollar"></i>-->
    <!--				</div>-->
    <!--				<div class="stat-data">-->
    <!--					<h2>--><?php //balance($owner);?><!-- <span class="stat-info">-->
    <?php //echo $lang['BALANCE'];?><!--</span></h2>-->
    <!--				</div>-->
    <!--			</a>-->
    <!--		</div>-->
    <!--	</div>-->
    <!--	<div class="col-lg---><?php //echo $col;?><!-- col-md---><?php //echo $col;?><!-- col-sm-6 col-xs-12">-->
    <!--		<div class="stat-box" style="height:120px !important;">-->
    <!--			<a href="my-converted-sales">-->
    <!--				<div class="stat-icon stat-danger hvr-bounce-in">-->
    <!--					<i class="fa-money"></i>-->
    <!--				</div>-->
    <!--				<div class="stat-data">-->
    <!--					<h2><span class="stat-info">-->
    <?php //echo $lang['Transfer_to_domain'];?><!--</span></h2>-->
    <!--				</div>-->
    <!--			</a>-->
    <!--		</div>-->
    <!--	</div>-->

    <?php if ($admin_user == '0') { ?>

        <div class="col-lg-<?php echo $col; ?> col-md-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-xs-12">
            <div class="stat-box" style="height:100px !important;">
                <a href="#">
                    <!--
                    <div class="stat-icon stat-danger hvr-bounce-in">
                        <i class="fa-money"></i>
                    </div>
                    -->
                    <div class="stat-data">
                        <h2><?php my_affiliate_earnings($owner); ?>
                            <br><span class="stat-info"><?php echo $lang['AFFILIIATE_EARNINGS']; ?> </span>
                        </h2>
                    </div>
                </a>
            </div>
        </div>
<!--        <div class="col-lg---><?php //echo $col; ?><!-- col-md---><?php //echo $col; ?><!-- col-sm-4 col-xs-12">-->
<!--            <div class="stat-box" style="height:100px !important;">-->
<!--                <a href="#">-->
<!--                    <div class="stat-icon stat-danger hvr-bounce-in">-->
<!--                        <i class="fa-money"></i>-->
<!--                    </div>-->
<!--                    <div class="stat-data">-->
<!--                        <h2>--><?php //my_total_mt_payments($owner); ?>
<!--                            <br><span class="stat-info">--><?php //echo $lang['TOTAL_MT_PAYMENTS']; ?><!-- </span>-->
<!--                        </h2>-->
<!--                    </div>-->
<!--                </a>-->
<!--            </div>-->
<!--        </div>-->
        <!--	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">-->
        <!--		<div class="stat-box" style="height:120px !important;">-->
        <!--			<a href="my-converted-sales">-->
        <!--				<div class="stat-icon stat-danger hvr-bounce-in">-->
        <!--					<i class="fa-money"></i>-->
        <!--				</div>-->
        <!--				<div class="stat-data">-->
        <!--					<h2>--><?php //balance($owner); ?><!-- <span class="stat-info"> Total Balance<span class="small-text">(selected period)</span></span></h2>-->
        <!--				</div>-->
        <!--			</a>-->
        <!--		</div>-->
        <!--	</div>-->
    <?php } ?>
</div>