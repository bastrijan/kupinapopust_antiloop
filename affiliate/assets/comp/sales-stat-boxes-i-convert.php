<div class="row">
    <?php if ($admin_user == '1') { ?>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="stat-box">
                <a href="#">
                    <!--
                    <div class="stat-icon stat-success hvr-bounce-in">
                        <i class="fa-dollar"></i>
                    </div>
                    -->
                    <div class="stat-data">
                        <h2><?php total_sales_period_i($start_date, $end_date, $owner); ?> <span
                                class="stat-info"><?php echo $lang['TOTAL_SALES']; ?> <span class="small-text">(selected period)</span></span>
                        </h2>
                    </div>
                </a>
            </div>
        </div>
    <?php } ?>
    <?php if ($admin_user == '0') { ?>
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
            <div class="stat-box">
                <a href="my-traffic">
                    <!--
                    <div class="stat-icon stat-danger hvr-bounce-in">
                        <i class="fa-money"></i>
                    </div>
                    -->
                    <div class="stat-data">
                        <h2><?php totalPointsNewBuyerNotTransferred($owner); ?> <span
                                class="stat-info"><?php echo $lang['CPC_EARNINGS']; ?></span></h2>
                    </div>
                </a>
            </div>
        </div>
    <?php } ?>






