<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="stat-box" style="    height: 100px;">
				<a href="affiliates">
					<!--
					<div class="stat-icon stat-primary hvr-bounce-in">
						<i class="fa-users"></i>
					</div>
					-->
					<div class="stat-data">
						<h2><?php total_affiliates();?> <span class="stat-info"><?php echo $lang['TOTAL_AFFILIATES'];?></span></h2>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="stat-box" style="    height: 100px;">
				<a href="referral-traffic">
					<!--
					<div class="stat-icon stat-default hvr-bounce-in">
						<i class="fa-rocket"></i>
					</div>
					-->
					<div class="stat-data">
						<h2><?php total_referrals();?> <br><span class="stat-info"><?php echo $lang['TOTAL_REFERRED_VISITS'];?></span></h2>
					</div>
				</a>
			</div>
		</div>
	
	<div style="    height: 10px;    display: inline-block;    width: 100%;"></div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="stat-box" style="    height: 100px;">
				<a href="#">
					<!--
					<div class="stat-icon stat-danger hvr-bounce-in">
						<i class="fa-money"></i>
					</div>
					-->
					<div class="stat-data">
						<h2><?php count_sales(); ?> <br><span
								class="stat-info"><?php echo $lang['Total_sales']; ?></span></h2>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="stat-box" style="    height: 100px;">
				<a href="#">
					<!--
					<div class="stat-icon stat-danger hvr-bounce-in">
						<i class="fa-money"></i>
					</div>
					-->
					<div class="stat-data">
						<h2><?php my_unique_count_sales(0); ?> <br><span
								class="stat-info"><?php echo $lang['Unique_refs']; ?></span></h2>
					</div>
				</a>
			</div>
		</div>
	<div style="    height: 10px;    display: inline-block;    width: 100%;"></div>
		
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="stat-box" style="    height: 100px;">
				<a href="#">
					<!--
					<div class="stat-icon stat-primary hvr-bounce-in">
						<i class="fa-money"></i>
					</div>
					-->
					<div class="stat-data">
						<h2><?php totalPointsNewBuyerNotTransferred(0); ?> <span
								class="stat-info">Непрефрлени "kupinapopust" поени </span>
						</h2>
					</div>
				</a>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="stat-box" style="    height: 100px;">
				<a href="#">
					<!--
					<div class="stat-icon stat-danger hvr-bounce-in">
						<i class="fa-money"></i>
					</div>
					-->
					<div class="stat-data">
						<h2><?php totalPointsNewBuyerTransferred(0); ?> <span
								class="stat-info">Префрлени "kupinapopust" поени </span>
						</h2>
					</div>
				</a>
			</div>
		</div>

	<div style="    height: 10px;    display: inline-block;    width: 100%;"></div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="stat-box" style="height:100px;">
				<a href="sales-profits">
					<!--
					<div class="stat-icon stat-success hvr-bounce-in">
						<i class="fa-dollar"></i>
					</div>
					-->
					<div class="stat-data">
						<h2><?php total_sales();?> <span class="stat-info"><?php echo $lang['TOTAL_SALES_MK'];?></span></h2>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="stat-box" style="height:100px;">
				<a href="sales-profits">
					<!--
					<div class="stat-icon stat-danger hvr-bounce-in">
						<i class="fa-money"></i>
					</div>
					-->
					<div class="stat-data">
						<h2><?php provizijaKupinapopust();?> <span class="stat-info">Вкупна заработка на kupinapopust</span></h2>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="stat-box" style="height:100px;">
				<a href="sales-profits">
					<!--
					<div class="stat-icon stat-danger hvr-bounce-in">
						<i class="fa-money"></i>
					</div>
					-->
					<div class="stat-data">
						<h2><?php affiliate_earnings();?> <span class="stat-info"><?php echo $lang['NET_EARNINGS'];?></span></h2>
					</div>
				</a>
			</div>
		</div>
</div>