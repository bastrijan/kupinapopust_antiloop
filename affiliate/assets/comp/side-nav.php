<li class="side-danger website-integration">
    <a href="<?php echo $main_url; ?>/affiliate/dashboard">
        <i class="fa-desktop"></i> <?php echo $lang['DASHBOARD']; ?>
    </a>
</li>
<?php if ($admin_user == '1') { ?>
    <li class="side-danger affiliates">
        <a href="<?php echo $main_url; ?>/affiliate/affiliates">
            <i class="fa-users"></i> <?php echo $lang['AFFILIATES']; ?>
        </a>
    </li>
    <?php $lc_on = lc_on();
    if ($lc_on == '1') { ?>
        <!--<li class="side-danger affiliates">-->
        <!--	<a href="http://--><?php //echo $domain_path;?><!--/leads">-->
        <!--		<i class="fa-user"></i> --><?php //echo $lang['LEADS'];?>
        <!--	</a>-->
        <!--</li>-->
    <?php } ?>
    <li class="side-danger sales-profits">
        <a href="<?php echo $main_url; ?>/affiliate/sales-profits">
            <i class="fa-basket"></i> <?php echo $lang['SALES']; ?>
        </a>
    </li>
    <?php $rc_on = rc_on();
    if ($rc_on == '1') { ?>
        <!--<li class="side-danger sales-profits">-->
        <!--	<a href="http://--><?php //echo $domain_path;?><!--/recurring-sales">-->
        <!--		<i class="fa-arrows-cw"></i> --><?php //echo $lang['RECURRING_COMMISSIONS'];?>
        <!--	</a>-->
        <!--</li>-->
    <?php } ?>
    <?php $mt_on = mt_on();
    if ($mt_on == '1') { ?>
        <li class="side-danger sales-profits">
            <a href="<?php echo $main_url; ?>/affiliate/multi-tier">
                <i class="fa-sitemap"></i> <?php echo $lang['MULTI_TIER_COMMISSIONS']; ?>
            </a>
        </li>
    <?php } ?>
<!--    <li class="side-danger referral-traffic">-->
<!--        <a href="http://--><?php //echo $domain_path; ?><!--/referral-traffic">-->
<!--            <i class="fa-rocket"></i> --><?php //echo $lang['REFERRAL_TRAFFIC']; ?>
<!--        </a>-->
<!--    </li>-->

    <li class="side-danger banners-logos">
        <a href="<?php echo $main_url; ?>/affiliate/banners-logos">
            <i class="fa-picture"></i> <?php echo $lang['BANNERS_AND_LOGOS']; ?>
        </a>
    </li>
    <!--
    <li class="side-danger payouts">
        <a href="http://<?php echo $domain_path; ?>/payouts">
            <i class="fa-money"></i> <?php echo $lang['PAYOUTS']; ?>
        </a>
    </li>
    -->
    
    <li class="side-danger commission">
        <a href="#" data-toggle="collapse" data-target="#cs">
            <i class="fa-award"></i> <?php echo $lang['COMMISSION_SETTINGS']; ?>
        </a>
        <ul id="cs" class="collapse">
            <li>
                <a href="fixed-commissions"><?php echo $lang['FIXED_COMMISSIONS']; ?></a>
            </li>


            <!-- STARO
            <li>
                <a href="sales-volume-commissions">
            -->
                <?php //echo $lang['SALES_VOLUME_COMMISSIONS']; ?>
            <!--
                </a>
            </li>
            -->


            <li>
                <a href="cpc-commissions"><?php echo $lang['CPC_COMMISSIONS']; ?></a>
            </li>
            <!--		<li>-->
            <!--			<a href="recurring-commissions">--><?php //echo $lang['RECURRING_COMMISSIONS'];?><!--</a>-->
            <!--		</li>-->
<!--            <li>-->
<!--                <a href="multi-tier-commissions">--><?php //echo $lang['MULTI_TIER_COMMISSIONS']; ?><!--</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href="lead-commissions">--><?php //echo $lang['LEAD_COMMISSIONS']; ?><!--</a>-->
<!--            </li>-->
        </ul>
    </li>
    <li class="side-danger website-integration">
        <a href="<?php echo $main_url; ?>/affiliate/settings">
            <i class="fa-cog"></i> <?php echo $lang['WEBSITE_INTEGRATION']; ?>
        </a>
    </li>

<?php } else { ?>

    <li class="side-danger referral-traffic">
        <a href="<?php echo $main_url; ?>/affiliate/my-converted-sales">
            <i class="fa-exchange"></i> <?php echo $lang['Transfer_to_domain_short']; ?>
        </a>
    </li>
    <li class="side-danger banners-logos">
        <a href="<?php echo $main_url; ?>/affiliate/banners-logos">
            <i class="fa-picture"></i> <?php echo $lang['BANNERS_AND_LOGOS']; ?>
        </a>
    </li>
    <li class="side-danger sales-profits">
        <a href="<?php echo $main_url; ?>/affiliate/my-sales">
            <i class="fa-chart-line"></i> <?php echo $lang['SALES']; ?>
        </a>
    </li>

    <li class="side-danger payouts">
        <a href="<?php echo $main_url; ?>/affiliate/my-payouts">
            <i class="fa-money"></i> <?php echo $lang['PAYOUTS']; ?>
        </a>
    </li>
<?php } ?>

<li class="side-danger acc-profile hidden-md hidden-lg">
    <a href="<?php echo $main_url?>/affiliate/profile">
        <i class="fa-smile"></i> <?php echo $lang['ACCOUNT_PROFILE']; ?>
    </a>
</li>

<li class="side-danger acc-logout hidden-md hidden-lg">
    <a href="<?php echo $main_url?>/affiliate/access/logout">
        <i class="fa-circle-notch"></i> <?php echo $lang['LOGOFF']; ?>
    </a>
</li>


<?php if ($admin_user == '1') { ?>
    <div class="side-heading"><?php echo $lang['TOP_AFFILIATES']; ?></div>
    <?php top_affiliates_list(); ?>
<?php } ?>