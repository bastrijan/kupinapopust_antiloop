<div class="row">
    <?php if ($admin_user == '1') { ?>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="stat-box">
                <a href="#">
                    <div class="stat-icon stat-success hvr-bounce-in">
                        <i class="fa-dollar"></i>
                    </div>
                    <div class="stat-data">
                        <h2><?php total_sales_period_i($start_date, $end_date, $owner); ?> <span
                                class="stat-info"><?php echo $lang['TOTAL_SALES']; ?> <span class="small-text">(selected period)</span></span>
                        </h2>
                    </div>
                </a>
            </div>
        </div>
    <?php } ?>
    <?php if ($admin_user == '0') { ?>

    <?php } ?>


<!--    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">-->
<!--        <div class="stat-box" style="height:120px !important;margin-bottom:10px;">-->
<!--            <a href="my-sales">-->
<!--                <div class="stat-icon stat-danger hvr-bounce-in">-->
<!--                    <i class="fa-money"></i>-->
<!--                </div>-->
<!--                <div class="stat-data">-->
<!--                    <h2>--><?php //my_total_sales($owner);?><!-- <span class="stat-info">--><?php //echo $lang['SALES'];?><!--</span></h2>-->
<!--                </div>-->
<!--            </a>-->
<!--        </div>-->
<!--    </div>-->



