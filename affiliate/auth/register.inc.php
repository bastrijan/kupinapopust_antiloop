<?php
include_once 'db-connect.php';
include_once 'config.inc.php';
include 'password.php';
include_once 'access-functions.php';





sec_session_start();
$error_msg = "";

if (isset($_POST['fullname'], $_POST['username'])) {

    // Sanitize and validate the data passed in
    $fullname = filter_input(INPUT_POST, 'fullname', FILTER_SANITIZE_STRING);
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $terms = filter_input(INPUT_POST, 'terms', FILTER_SANITIZE_STRING);
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);

    $get_email_kupi = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT email FROM customer WHERE email=\"$email\""));
    $email_kupi = $get_email_kupi['email'];

    if (!isset($email_kupi)) 
    {
        $default = 'default';
        $pw = $_POST['p'];
        $pass = md5($pw);
        $date = date("Y-m-d H:i:s");
        $insert_to_kupi = $mysqli->prepare("INSERT INTO customer (login_type, email, password, created) VALUES (?, ?, ?, ?)");
        $insert_to_kupi->bind_param('ssss', $default, $email, $pass, $date);
        $insert_to_kupi->execute();
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        // Not a valid email
        $error_msg .= '<p class="error" style="color:red;">Еmail адресата што ја внесовте не е валидна</p>';
    }

    $pw = $_POST['p'];

    $prep_stmt = "SELECT id FROM ap_members WHERE email = ? LIMIT 1";
    $stmt = $mysqli->prepare($prep_stmt);

    if ($stmt) {
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->store_result();

        if ($stmt->num_rows == 1) {
            // A user with this email address already exists
            $error_msg .= '<p class="error" style="color:red;">Веќе постои корисник со email адресата што ја внесовте.</p>';
        }
    }

    $prep_stmt = "SELECT id FROM ap_members WHERE username = ? LIMIT 1";
    $stmt = $mysqli->prepare($prep_stmt);

    if ($stmt) {
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $stmt->store_result();

        if ($stmt->num_rows == 1) {
            // A user with this email address already exists
            $error_msg .= '<p class="error" style="color:red;">Ова корисничко име веќе постои.</p>';
        }

    } else {
        $error_msg .= '<p class="error">Database error</p>';
    }

    if ($terms != '1') {
        $error_msg .= '<p class="error" style="color:red;">Мора да се согласите со Општите одредби на страната.</p>';
    }


    if (empty($error_msg)) {

        $hash = password_hash($pw, PASSWORD_DEFAULT, array("cost" => 10));

        if (isset($_COOKIE['ap_sponsor_tracking'])) {
            $sponsor = $_COOKIE['ap_sponsor_tracking'];
        } else {
            $sponsor = '0';
        }
        if ($insert_stmt = $mysqli->prepare("INSERT INTO ap_members (fullname, username, email, password, terms, sponsor) VALUES (?, ?, ?, ?, ?, ?)")) {
            $insert_stmt->bind_param('ssssss', $fullname, $username, $email, $hash, $terms, $sponsor);


            if (!$insert_stmt->execute()) {
                header('Location: ../error.php?err=Registration failure: INSERT');
            }
        }


        $prep_stmt = "SELECT id FROM ap_members WHERE username = ? LIMIT 1";
        $stmt = $mysqli->prepare($prep_stmt);

        if ($stmt) {
            mysqli_stmt_bind_param($stmt, "s", $username);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $users_id);
            mysqli_stmt_fetch($stmt);
            mysqli_stmt_close($stmt);


        }

        // XSS protection as we might print this value
        $users_id = preg_replace("/[^0-9]+/", "", $users_id);
        $_SESSION['user_id'] = $users_id;
        //$_SESSION['accttype'] = $accttype;
        // XSS protection as we might print this value
        $username = preg_replace("/[^a-zA-Z0-9_\-]+/",
            "",
            $username);
        $_SESSION['username'] = $username;
        $_SESSION['fullname'] = $fullname;
        $owner = $users_id;

        $get_email = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT email FROM ap_members WHERE id=$owner"));
        $recp_email = $get_email['email'];


        //PRAKJANJE NA MAIL ZA NOV KUPINAPOPUST.MK PROFIL
        ob_start();
        $email= $email;
        $password = $_POST['p'];
        require_once $_SERVER['DOCUMENT_ROOT'].'/application/views/emails/mk/new_customer.php';
        $newCustomerEmailContent = ob_get_contents();
        ob_end_clean();
        sendEmailPHPMailer($recp_email, "Kupinapopust.mk кориснички профил!", $newCustomerEmailContent, true);


        //PRAKJANJE NA MAIL ZA NOV AFFILIATE PROFIL
        ob_start();
        require_once $_SERVER['DOCUMENT_ROOT'].'/application/views/emails/mk/new_affiliate.php';
        $newAffiliateEmailContent = ob_get_contents();
        ob_end_clean();
        sendEmailPHPMailer($recp_email, "Kupinapopust.mk AFFILIATE профил!", $newAffiliateEmailContent, true);





        $_SESSION['first_login'] = 1;
        header('Location: dashboard');
    }
}