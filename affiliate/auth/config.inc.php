<?php
//definirame konstanta SYSPATH za da moze da go iskoristime istiot config fajl za bazata kako za aplikacijata
if(!defined('SYSPATH'))
	define('SYSPATH' , "vrednost");

/***INCLUDE SECTION**/
require $_SERVER['DOCUMENT_ROOT'].'/application/config/database.php';
require $_SERVER['DOCUMENT_ROOT'].'/application/config/facebook.php';

define("HOST", $config['default']['connection']["host"]);     // The host you want to connect to.
define("USER", $config['default']['connection']["user"]);    // The database username.
define("PASSWORD", $config['default']['connection']["pass"]);    // The database password.
define("DATABASE", $config['default']['connection']["database"]);    // The database name.
 
define("CAN_REGISTER", "any");
define("DEFAULT_ROLE", "member");
 
define("SECURE", FALSE);    // FOR DEVELOPMENT ONLY!!!!

$DOMAIN = $_SERVER['SERVER_NAME'];
//SET TO THE NAME OF THE FOLDER YOUR INSTALLATION IS INSIDE
$INSTALL_FOLDER = 'affiliate';
//URL WHERE YOU WILL GENERALLY WANT AFFILIATES TO SEND TRAFFIC TO
$main_url = $config['facebookSiteProtocol']."://".$config['facebookSiteURL'];

$domain_path = $DOMAIN.'/'.$INSTALL_FOLDER;

error_reporting(0);