<?php
include('auth/startup.php');
include('data/data-functions.php');
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

//REDIRECT ADMIN
if($admin_user!='1'){header('Location: dashboard');}

//setiraj podatoci
$banner_id = filter_input(INPUT_GET, 'banner_id', FILTER_SANITIZE_STRING);

if((int)$banner_id == 0 )
    header('Location: banners-logos');

$adsize = 0;
$banner_link = "";
$filename = "xx";

$row_data = get_banner($banner_id);

if($row_data)
{
    $adsize = $row_data["adsize"];
    $banner_link = $row_data["banner_link"];
    $filename = $row_data["filename"];
}





?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $meta_description; ?>">
    <meta name="author" content="">

    <title><?php echo $meta_title; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/base.css" rel="stylesheet">
    <link href="assets/comp/breadcrumbs/breadcrumbs.css" rel="stylesheet">
    <link href="assets/css/custom.css" rel="stylesheet">

    <!-- Elusive and Font Awesome Icons -->
    <link href="assets/fonts/css/elusive.css" rel="stylesheet">
    <link href="assets/fonts/css/fa.css" rel="stylesheet">
    <!-- Webfont -->
    <link href='https://fonts.googleapis.com/css?family=Archivo+Narrow:400,400italic,700,700italic' rel='stylesheet'
          type='text/css'>
    <!-- Animation Effects -->
    <link href="assets/css/plugins/hover.css" rel="stylesheet" media="all">
    <!-- SweetAlert Plugin -->
    <link href="assets/css/plugins/sweetalert.css" rel="stylesheet" media="all">
    <!-- Datatables Plugin -->
    <link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css" rel="stylesheet" media="all">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!-- Start Top Navigation -->
<?php include('assets/comp/top-nav.php'); ?>
<!-- Start Main Wrapper -->
<div id="wrapper">
    <!-- Side Wrapper -->
    <div id="side-wrapper">
        <ul class="side-nav">
            <?php include('assets/comp/side-nav.php'); ?>
        </ul>
    </div><!-- End Main Navigation -->

    <!-- YOUR CONTENT GOES HERE -->
    <div id="page-content-wrapper">
        <div class="container-fluid">



                <div class="row">
                    <!-- Start Panel -->
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-heading panel-primary">
                                <span class="title">Измени банер</span>
                            </div>
                            <div class="panel-content">
                                <?php echo $message; ?>
                                <form action="data/upload-data?banner_id=<?php echo $banner_id; ?>" method="post" enctype="multipart/form-data" class="upload-form">
                                    Големина на банер:
                                    <select name="adsize" class="form-control input-md">
                                        <option value="0" <?php if($adsize == 0) echo "selected" ?> >Select Size</option>
                                        <option value="1" <?php if($adsize == 1) echo "selected" ?> >Mobile leaderboard (320x50)</option>
                                        <option value="2" <?php if($adsize == 2) echo "selected" ?> >Large mobile banner (320x100)</option>
                                        <option value="3" <?php if($adsize == 3) echo "selected" ?> >Medium Rectange (300x250)</option>
                                        <option value="4" <?php if($adsize == 4) echo "selected" ?> >Rectange (180x150)</option>
                                        <option value="5" <?php if($adsize == 5) echo "selected" ?> >Wide Skyscraper (160x600)</option>
                                        <option value="6" <?php if($adsize == 6) echo "selected" ?> >Leaderboard (728x90)</option>
                                    </select>
                                    <br>
                                    Линк на банер:  <input id="banner_link" name="banner_link" type="text"  class="form-control input-md" value="<?php echo $banner_link; ?>">
                                    <br><br>
                                    <input type="file" name="file" size="25"/>  
                                    <?php 
                                        $target_dir = "data/banners/";
                                        $banner_path = $target_dir.$filename;

                                        if (file_exists($banner_path)) { 
                                    ?>
                                        <a target="_blank" href="<?php echo $main_url.'/affiliate/data/banners/'.$filename.'?x='.time(); ?>">Кликни за да ја погледнеш сликата на банерот</a>
                                        <input name="filename_current" type="hidden"   value="<?php echo $filename; ?>">
                                    <?php } ?>
                                    <br>
                                    <br>
                                    <br>
                                    <input type="submit" name="submit" value="Смени" class="btn btn-primary"/>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- End Panel -->
                </div>


        </div>
    </div>
        <!-- End Page Content -->

</div><!-- End Main Wrapper  -->

    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>
    <?php if ($admin_user == '1') { ?>
        <script>$('#side-wrapper > ul > li:nth-child(4)').css('background', '#FF6701');</script> <?php }    else { ?>
        <script>$('#side-wrapper > ul > li:nth-child(3)').css('background', '#FF6701');</script> <?php } ?>
    <script>$('#breadcrumbs').html('<p id="breadcrumb"><a href="dashboard">Home</a>Marketing Materials</p>');</script>
    <!-- Bootstrap -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Base JS -->
    <script src="assets/js/base.js"></script>



</body>
</html>
