<?php include_once '../auth/startup.php';
session_start();
$dc = filter_input(INPUT_POST, 'dcc', FILTER_SANITIZE_STRING);


if ($dc > 100) {
    $dc = '100';
}
if ($admin_user == '1') {
    $memberID = $_POST['id'];
    $update_one = $mysqli->prepare("UPDATE ap_members SET new_provizija = ? WHERE id=$memberID");
    $update_one->bind_param('s', $dc);
    $update_one->execute();
    $update_one->close();
}
$mysqli->close();
$_SESSION['action_saved'] = '1';
header('Location: ../fixed-commissions');
