<?php include_once '../auth/db-connect.php';
$owner = $_POST['owner'];

$target_dir = $_SERVER['DOCUMENT_ROOT']."/affiliate/data/emails/$owner-";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {


}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "txt" ) {
    echo "Sorry, only TXT files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
$path = $target_file;
$data = file_get_contents($path);
function extract_email_address ($data)
{
    foreach (preg_split('/\s/', $data) as $token) {
        $email = filter_var(filter_var($token, FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL);
        if ($email !== false) {
            $emails[] = $email;
        }
    }
    return $emails;
}
$mails = extract_email_address($data);

for($i=0;$i < count($mails); $i++)
{
    $emails.= $mails[$i];

    if($i < count($mails)-1 ) 
        $emails.="\r\n";
}


unlink($target_file);



$sent=0;
$datetime=$datetime = date("Y-m-d H:i:s");
if ($stmt = $mysqli->prepare("INSERT INTO ap_emails (affiliate_id, emails, sent, Date) VALUES (?, ?, ?, ?)"))
{
    $stmt->bind_param("ssss", $owner, $emails, $sent,$datetime);
    $stmt->execute();
    $stmt->close();
    echo 'ok';
} else { echo "ERROR: could not prepare SQL statement."; }



header('Location: ../banners-logos');
?>