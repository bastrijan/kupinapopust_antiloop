<?php include_once '../auth/startup.php';
session_start();
$dc = filter_input(INPUT_POST, 'dcc', FILTER_SANITIZE_STRING);

$memberID = $_POST['id'];

$update_one = $mysqli->prepare("UPDATE ap_members SET new_cpc = ? WHERE id=$memberID");
$update_one->bind_param('s', $dc);
$update_one->execute();
$update_one->close();
$_SESSION['action_saved'] = '1';
header('Location: ../cpc-commissions');
?>