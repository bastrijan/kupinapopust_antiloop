<?php include_once '../auth/startup.php';
session_start();
$affiliate_id = filter_input(INPUT_POST, 'm', FILTER_SANITIZE_STRING);
if($admin_user=='1') {
	$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members WHERE id=$affiliate_id"));
	$emails_enable = $get_affiliate['emails'];

	if($emails_enable ==0) {
		if ($stmt = $mysqli->prepare("UPDATE ap_members SET emails=1 WHERE id=$affiliate_id")) {

			$stmt->execute();
			$stmt->close();
		} else {
			echo "ERROR: could not prepare SQL statement.";
		}
	}
	else{
		if ($stmt = $mysqli->prepare("UPDATE ap_members SET emails=0 WHERE id=$affiliate_id")) {

			$stmt->execute();
			$stmt->close();
		} else {
			echo "ERROR: could not prepare SQL statement.";
		}
	}

}

$mysqli->close();
$_SESSION['action_saved'] = '1';
header('Location: ../affiliates');