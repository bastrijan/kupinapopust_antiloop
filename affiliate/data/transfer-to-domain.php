<?php
header('Content-type: text/plain; charset=utf-8');
include_once '../auth/startup.php';
include('auth/db-connect.php');

$get_tr = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(points_new_buyer) as total_cpc FROM ap_earnings WHERE affiliate_id=$owner and points_new_buyer_transferred_flag = 0"));
$tr = $get_tr['total_cpc'];

// $get_payout = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(amount) as total_payout FROM ap_payouts WHERE affiliate_id=$owner"));
// $payout = $get_payout['total_payout'];
// $tr=$tr-$payout;

if ($tr == '') {
    $tr = '0';
}


$transfer = floatval($tr);

session_start();


$memberID = $owner;

$get_user = mysqli_fetch_assoc(mysqli_query($mysqli, "select email from ap_members where id=$memberID"));
$email = $get_user['email'];
$get_ID = mysqli_fetch_assoc(mysqli_query($mysqli, "select id from customer where email='$email'"));
$ID = $get_ID['id'];

if (isset($ID)) {
    if ($ID > 0) {
        $data = array();
        $data['customer_id'] = $ID;
        $data['total'] = $transfer;
        $data['remaining'] = $transfer;
        $data['created'] = date("Y-m-d H:i:s");
        $data['ends'] = date("Y-m-d H:i:s", strtotime("+2 months"));

        $data2['affiliate_id']= $memberID;
        $data2['email']= $email;
        $data2['amount']= $transfer;

        if ($transfer > 0) {
            //zacuvaj vo tabelata POINTS

            //insertPayout($data['total'] = $transfer,$mysqli,$data2);
            insertPoints('points', $data, $mysqli,$memberID);

            $_SESSION['points_transferred']='0';
            header('Location: ../my-converted-sales');

            //zacuvaj vo tabelata POINTS_HISTORY
            // saveHistory($customerID, $transfer, $reason, "+");
        }
        else{
            $_SESSION['fail']='0';
            header('Location: ../my-converted-sales');
        }

    }
} else {
    echo '<script language="javascript">';
    echo 'alert("' . "Not Found" . '")';
    echo '</script>';
}


function insertPoints($table = '', $set = NULL, $mysqli, $memberID)
{


    $data = "(customer_id,total,remaining,created,ends) VALUES (" . $set['customer_id'] . "," . $set['total'] . "," . $set['remaining'] . ",'" . $set['created'] . "','" . $set['ends'] . "')";

    $update_one = $mysqli->prepare("INSERT INTO $table $data");

    $update_one->execute();
    $update_one->close();

    $table2 = 'points_history';
    $data2 = "(customer_id,created,reason,points) VALUES (" . $set['customer_id'] . ",'" . $set['created'] . "','префрлени од Affiliate програмата','+" . $set['total'] . "')";
    $update_two = $mysqli->prepare("INSERT INTO $table2 $data2");
    $update_two->execute();
    $update_two->close();



    $update_3 = $mysqli->prepare("UPDATE ap_earnings SET points_new_buyer_transferred_flag = 1 WHERE affiliate_id = $memberID");
    $update_3->execute();
    $update_3->close();
    

    $transfer_value = (int) $set['total'];
    //UPDATE AFFILIATE BALANCE
    $update_one = $mysqli->prepare("UPDATE ap_members SET balance = balance - ? WHERE id = $memberID"); 
    $update_one->bind_param('i', $transfer_value);
    $update_one->execute();
    $update_one->close();   

    $mysqli->close();
}



// function insertPayout($val,$mysqli,$data)
// {
//     $data['amount'] = number_format ($data['amount'], 2);
//     $datetime = date("Y-m-d H:i:s");
//     $data2 = "(affiliate_id,payment_method,payment_email,amount,datetime,status) VALUES (" . $data['affiliate_id'] . ",10,'" . $data['email'] . "', ".$data['amount']." ,'$datetime',1)";

//     $update_one = $mysqli->prepare("INSERT INTO ap_payouts $data2");

//     $update_one->execute();
//     $update_one->close();

// }