<?php include_once '../auth/db-connect.php';
include('data-functions.php');

$adsize = filter_input(INPUT_POST, 'adsize', FILTER_SANITIZE_STRING);
$banner_link = urldecode(filter_input(INPUT_POST, 'banner_link', FILTER_SANITIZE_STRING));
$banner_id = filter_input(INPUT_GET, 'banner_id', FILTER_SANITIZE_STRING);


$kacen_fajl = false;
$filename = $_FILES["file"]["name"];

if($filename != "")
{
	$target_dir = "banners/";
	$size = $_FILES["file"]["size"];
	$upload_date = date('j M Y H:i:s');
	$name = pathinfo($_FILES['file']['name'], PATHINFO_FILENAME);
	$extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	$db_filename = $name.'.'.$extension;
	$target_file = $target_dir . basename($_FILES["file"]["name"]);
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);

	//proveri dali treba da se izbrise starata slika
	$filename_current = filter_input(INPUT_POST, 'filename_current', FILTER_SANITIZE_STRING);

	if($filename_current != $filename)
		unlink($target_dir.$filename_current); 

	$kacen_fajl = true;
}



// Check if file already exists
/*
$counter = 0;
while (file_exists($target_file)) {
	$db_filename = basename($name.'_'.$counter.'.'.$extension);
    $target_file = $target_dir . basename($name.'_'.$counter.'.'.$extension);
	$counter++;
}
*/

// Check file size
/*
if ($_FILES["file"]["size"] > 500000) {
    
}
*/


//kaci go fajlot


//ako postoi banner_id => UPDATE
if((int)$banner_id > 0 )
{

	if($kacen_fajl)
	{
		$stmt = $mysqli->prepare("UPDATE ap_banners SET filename = ?, filetype = ?, adsize = ?, banner_link = ? WHERE id = ? ");
		$stmt->bind_param('sssss', $db_filename, $extension, $adsize, $banner_link, $banner_id);
	}
	else
	{
		$stmt = $mysqli->prepare("UPDATE ap_banners SET adsize = ?, banner_link = ? WHERE id = ? ");
		$stmt->bind_param('sss', $adsize, $banner_link, $banner_id);
	}

	$stmt->execute();
	$stmt->close();

	$_SESSION['action_saved'] = '1';
} 
elseif($kacen_fajl) //INSERT
{
	$stmt = $mysqli->prepare("INSERT INTO ap_banners (filename, filetype, adsize, banner_link) VALUES (?, ?, ?, ?)");
	$stmt->bind_param('ssss', $db_filename, $extension, $adsize, $banner_link);

	$stmt->execute();
	$stmt->close();

	$_SESSION['action_saved'] = '1';
}


header('Location: ../banners-logos');
?>