<?php
header('Content-Type: text/html; charset=utf-8');

/* ===========================================
	TOP NAV FUNCTION 
   ========================================= */
function admin_control($admin_user, $lang_u, $lang_a){
	if($admin_user=='1'){
	echo '<li>
		<a href="user-management"><i class="icon-default fa fa-fw fa-user"></i> '.$lang_u.'</a>
	</li>
	<li>
		<a href="settings"><i class="icon-default fa fa-fw fa-cog"></i> '.$lang_a.'</a>
	</li>';}
}

function avatar($userid){
	include('auth/db-connect.php');
	$get_email = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT email FROM ap_members WHERE id=$userid"));
	$email = $get_email['email'];
	//GRAVATAR IMAGE URL
	$size = 40;
	$grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
	//echo '<img src="'.$grav_url.'">';
	echo '';
}

function balance($owner){
	include('auth/db-connect.php');
	$get_balance= mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT balance FROM ap_members WHERE id=$owner"));
	$balance = $get_balance['balance'];

	echo $balance;
}


function balance_affiliate($owner){
	include('auth/db-connect.php');
	$get_balance= mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT balance FROM ap_members WHERE id=$owner"));
	$balance = $get_balance['balance'];

	echo $balance;
}

function balance_only($owner){
	include('auth/db-connect.php');
	$get_balance= mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT balance FROM ap_members WHERE id=$owner"));
	$balance = $get_balance['balance'];

	echo $balance;
}

/* ===========================================
	PROFILE FUNCTION
   ========================================= */

function profile_name($owner){
	include('auth/db-connect.php');
	$get_profile = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT fullname FROM ap_members WHERE id=$owner"));
	$name = $get_profile['fullname'];
	echo $name;
}

function profile_img($owner){
	include('auth/db-connect.php');
	$get_profile = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT avatar FROM ap_members WHERE id=$owner"));
	if($get_profile['avatar']==''){
		//GRAVATAR IMAGE URL
		$email = $get_profile['email'];
		$size = 180;
		$grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
		//echo '<img src="'.$grav_url.'">';
		echo '';
	}
}

function profile_details($owner){
	include('auth/db-connect.php');
	include('lang/'.$_SESSION['language'].'.php');
	$get_profile = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT fullname, email, username FROM ap_members WHERE id=$owner"));
	$profile_name = $get_profile['fullname'];
	$profile_email = $get_profile['email'];
	$profile_username = $get_profile['username'];
	echo '
		<form method="post" action="data/update-user">
			<input type="text" name="f" value="'.$profile_name.'">
			<input type="text" name="e" value="'.$profile_email.'">
			<input type="submit" class="btn btn-success" value="Зачувај">
		</form>';

}
/* ===========================================
	AFFILIATES FUNCTION
   ========================================= */
function affiliate_name($affiliate_filter){
	include('auth/db-connect.php');
	$get_profile = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT fullname FROM ap_members WHERE id=$affiliate_filter"));
	$name = $get_profile['fullname'];
	echo ucwords($name);
}

function affiliate_MLM($affiliate_filter){
	include('auth/db-connect.php');
	$query = "SELECT * FROM ap_members WHERE sponsor=$affiliate_filter";

	$query = $mysqli->real_escape_string($query);
	if($result = $mysqli->query($query)){
		$num_results = mysqli_num_rows($result);
		while($row = $result->fetch_array())
		{
			$member = $row['id'];
			//CALC AFFILIATE SALES
			$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(sale_amount) as affiliate_sales FROM ap_earnings WHERE affiliate_id=$member"));
			$affiliate_sales = $get_affiliate['affiliate_sales'];
			if($affiliate_sales==''){$affiliate_sales = '0.00';}
			//CALC AFFILIATE REFERRALS
			$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(id) as affiliate_referrals FROM ap_referral_traffic WHERE affiliate_id=$member"));
			$affiliate_referrals = $get_affiliate['affiliate_referrals'];
			if($affiliate_referrals==''){$affiliate_referrals = '0.00';}
			//MULTI CURRENCY

			echo '<tr>
						<td>
							<form method="get" action="affiliate-stats" class="pull-left">
								<input type="hidden" name="a" value="'.$row['id'].'">
								<input type="submit" class="btn btn-sm btn-primary" value="View Stats">
							</form>
							<form method="post" action="data/delete-affiliate" class="pull-left">
								<input type="hidden" name="m" value="'.$row['id'].'">
								<input type="submit" class="btn btn-sm btn-danger" value="Delete">
							</form>
						</td>
						<td>?ref='.$member.'</td>
						<td><a href="affiliate-stats?a='.$row['id'].'">'.$row['fullname'].'</a></td>
						<td>'.$row['username'].'</td>
						<td>'.$row['email'].'</td>
						<td>'.$affiliate_referrals.'</td>
						<td>'.$affiliate_sales.'</td>
						<td>'.$row['balance'].'</td>
						'; // echo '<td>'; if($row['terms']=='1'){echo 'Yes';} echo '</td>
			echo '</tr>';
		}
	}
}

function affiliates_table(){
	include('auth/db-connect.php');
	$query = "SELECT * FROM ap_members WHERE admin_user!=1 ORDER BY id DESC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$member = $row['id'];

					$query1 = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(points_new_buyer) as points FROM ap_earnings WHERE affiliate_id=$member and points_new_buyer_transferred_flag = 0"));
					$points = $query1['points'];
					if($points==''){$points = '0';}

					$query2 = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(points_new_buyer) as paid_points FROM ap_earnings WHERE affiliate_id=$member and points_new_buyer_transferred_flag = 1"));
					$paid_points = $query2['paid_points'];
					if($paid_points==''){$paid_points = '0';}


					//CALC AFFILIATE SALES
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(sale_amount) as affiliate_sales, SUM(net_earnings) as affiliate_earnings FROM ap_earnings WHERE affiliate_id=$member"));
				$affiliate_sales = $get_affiliate['affiliate_sales'];
				if($affiliate_sales==''){$affiliate_sales = '0.00';}

				//CALC PROVISION
				$provizija = $get_affiliate['affiliate_earnings'];
				if($provizija==''){$provizija = '0.00';}


				//CALC AFFILIATE REFERRALS
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(DISTINCT `email`) as unique_buyers FROM ap_earnings WHERE affiliate_id=$member AND void!=1"));
				$unique_buyers = $get_affiliate['unique_buyers'];
				if($unique_buyers==''){$unique_buyers = '0.00';}
				//MULTI CURRENCY

					if ($row['emails']==0) $emails='Дозволи да се користат Emails'; else $emails='Забрани да се користат Emails';
				echo '<tr>
						<td>'.$unique_buyers.'</td>
						<td>'.$points.'<br/>'.$paid_points.'</td>
						

						<td>'.$affiliate_sales.'</td>
						<td>'.$provizija.'</td>
						<td>?ref='.$member.'</td>
						<td><a href="affiliate-stats?a='.$row['id'].'">'.$row['fullname'].'</a></td>';
//						<td>'.$row['username'].'</td>
						echo '<td>'.$row['email'].'</td>
						';
//							<form method="get" action="affiliate-stats" class="pull-left">
//								<input type="hidden" name="a" value="'.$row['id'].'">
//								<input type="submit" class="btn btn-sm btn-primary" value="View Stats">
//							</form>
//							<form method="post" action="data/delete-affiliate" class="pull-left">
//								<input type="hidden" name="m" value="'.$row['id'].'">
//								<input type="submit" class="btn btn-sm btn-danger" value="Delete">
//							</form>
						
/*
						echo '
						<td>
						<form method="post" action="data/add-email" class="pull-left">
								<input type="hidden" name="m" value="'.$row['id'].'">
								<input type="submit" class="btn btn-sm" value="'.$emails.'">
							</form>
						</td>
						'; 
*/

					echo '</tr>';
			}
		}
}

function top_affiliates_table(){
	include('auth/db-connect.php');
	$limit = '4';
	$i = '0';
	$query = "SELECT affiliate_id, COUNT(*) as count FROM ap_earnings GROUP BY affiliate_id ORDER BY count DESC;";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				if($i < $limit){
					$affiliate_id = $row['affiliate_id'];
					$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT fullname, email FROM ap_members WHERE id=$affiliate_id"));
					$fullname = $get_affiliate['fullname'];
					$email = $get_affiliate['email'];
					//CALC AFFILIATE SALES
					$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(sale_amount) as affiliate_sales, COUNT(id) as ts FROM ap_earnings WHERE affiliate_id=$affiliate_id"));
					$affiliate_sales = $get_affiliate['affiliate_sales'];
					if($affiliate_sales==''){$affiliate_sales = '0.00';}
					$sales_count = $get_affiliate['ts'];
					//CALC AFFILIATE REFERRALS
					$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(DISTINCT `email`) as unique_buyers FROM ap_earnings WHERE affiliate_id=$affiliate_id AND void!=1"));
					$unique_buyers = $get_affiliate['unique_buyers'];
					if($unique_buyers==''){$unique_buyers = '0';}
					$referral_count = $get_affiliate['tr'];
					//MULTI CURRENCY

					echo '<tr class="top-list">
							<td>';  echo '<a href="affiliate-stats?a='.$affiliate_id.'">'.$fullname.'</a></td>
							<td>'.$unique_buyers.'</td>
							<td>'.$affiliate_sales.'</td>
							<td>'; $conv = $sales_count / $referral_count  * 100; echo number_format((float)$conv, 2, '.', '').'%'; echo '</td>
						  </tr>';
				}
				$i++;
			}
		}
}

function affiliates_table_fixed_commisions(){
	include('auth/db-connect.php');
	$query = "SELECT * FROM ap_members WHERE admin_user!=1 ORDER BY id DESC";
	$query = $mysqli->real_escape_string($query);
	if($result = $mysqli->query($query)){
		$num_results = mysqli_num_rows($result);
		while($row = $result->fetch_array())
		{
			$member = $row['id'];
			//CALC AFFILIATE SALES
			$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(sale_amount) as affiliate_sales FROM ap_earnings WHERE affiliate_id=$member"));
			$affiliate_sales = $get_affiliate['affiliate_sales'];
			if($affiliate_sales==''){$affiliate_sales = '0.00';}
			//CALC AFFILIATE REFERRALS
			$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(id) as affiliate_referrals FROM ap_referral_traffic WHERE affiliate_id=$member"));
			$affiliate_referrals = $get_affiliate['affiliate_referrals'];
			if($affiliate_referrals==''){$affiliate_referrals = '0.00';}
			//MULTI CURRENCY


			echo '<tr>
						<td>?ref='.$member.'</td>
						<td><a href="affiliate-stats?a='.$row['id'].'">'.$row['fullname'].'</a></td>

						<td>'.$row['username'].'</td>
						<td>'.$row['email'].'</td>
						<td><form action="data/set-new-commission" method="post">
						<input type="text" name="id"  value="'.$row['id'].'" placeholder="10" style="display:none;" />
						<input type="text" name="dcc"  value="'.$row['new_provizija'].'" placeholder="10" style="width:30%;"/>%
									<input type="submit" name="submit" value="Set" class="btn btn-success" /></form>
									</td>
					</tr>';
		}
	}
}

function affiliates_table_fixed_cpc(){
	include('auth/db-connect.php');
	$query = "SELECT * FROM ap_members WHERE admin_user!=1 ORDER BY id DESC";
	$query = $mysqli->real_escape_string($query);
	if($result = $mysqli->query($query)){
		$num_results = mysqli_num_rows($result);
		while($row = $result->fetch_array())
		{
			$member = $row['id'];
			//CALC AFFILIATE SALES
			$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(sale_amount) as affiliate_sales FROM ap_earnings WHERE affiliate_id=$member"));
			$affiliate_sales = $get_affiliate['affiliate_sales'];
			if($affiliate_sales==''){$affiliate_sales = '0.00';}
			//CALC AFFILIATE REFERRALS
			$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(id) as affiliate_referrals FROM ap_referral_traffic WHERE affiliate_id=$member"));
			$affiliate_referrals = $get_affiliate['affiliate_referrals'];
			if($affiliate_referrals==''){$affiliate_referrals = '0.00';}
			//MULTI CURRENCY

			echo '<tr>
						<td>?ref='.$member.'</td>
						<td><a href="affiliate-stats?a='.$row['id'].'">'.$row['fullname'].'</a></td>

						<td>'.$row['username'].'</td>
						<td>'.$row['email'].'</td>
						<td><form action="data/update-new-cpc" method="post">
						<input type="text" name="id"  value="'.$row['id'].'" placeholder="10" style="display:none;" />
						<input type="text" name="dcc"  value="'.$row['new_cpc'].'" placeholder="10" style="width:30%;"/> MKD
									<input type="submit" name="submit" value="Set" class="btn btn-success" /></form>
									</td>
					</tr>';
		}
	}
}

function top_affiliates_list(){
include('auth/db-connect.php');
	$limit = '5';
	$i = '0';
	$query = "SELECT affiliate_id, COUNT(*) as count FROM ap_earnings GROUP BY affiliate_id ORDER BY count DESC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				if($i < $limit){
					$affiliate_id = $row['affiliate_id'];
					$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT fullname, email FROM ap_members WHERE id=$affiliate_id"));
					$fullname = $get_affiliate['fullname'];
					$email = $get_affiliate['email'];
					$size = 40;
					$grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
					echo '
						<li id="'.$affiliate_id.'" class="side-danger">
							<a href="affiliate-stats?a='.$affiliate_id.'">
								<img src="'.$grav_url.'">
								'.$fullname.'
							</a>
						</li>';
				}
				$i++;
			}
		}
}

/* ===========================================
	REFERRAL TRAFFIC FUNCTIONS
   ========================================= */
function referral_table($start_date, $end_date, $affiliate_filter){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	if(isset($affiliate_filter)){$show = ' AND affiliate_id='.$affiliate_filter.'';}
	$query = "SELECT * FROM ap_referral_traffic WHERE datetime > $start_date AND datetime < $end_date $show ORDER BY datetime DESC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$affiliate_id = $row['affiliate_id'];
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members WHERE id=$affiliate_id"));
				$affiliate_user = $get_affiliate['fullname'];
				//CHECK IF CPC ENABLED
				$get_cpc_on = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT cpc_on FROM ap_other_commissions WHERE id=1"));
				$cpc_on = $get_cpc_on['cpc_on'];
				//MULTI CURRENCY

				echo '<tr>
							';
						if($cpc_on=='1'){
							echo '<td>';
							if($row['void']=='1'){ echo '<span class="red">'.'0.00'.' (VOID)</span>';} else {
									if($row['cpc_earnings']=='0'){echo '0.00';}
						 			else { echo $row['cpc_earnings']; }
							}
						echo '</td>';}
						echo '<td><a href="affiliate-stats?a='.$affiliate_id.'">'; if($affiliate_user!=''){echo $affiliate_user;}else{echo 'No Affiliate';} echo '</a></td>
						<td>'.$row['ip'].'</td>
						<td>'.$row['agent'].'</td>
						<td>'.$row['host_name'].'</td>
						<td>'.$row['landing_page'].'</td>';
						echo '<td>'.$row['datetime'].'</td>
					</tr>';
			}
		}
}

function top_url_referral_table($start_date, $end_date, $affiliate_filter){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	if(isset($affiliate_filter)){$show = ' AND affiliate_id='.$affiliate_filter.'';}
	$query = "SELECT landing_page, datetime, affiliate_id, COUNT(*) as count FROM ap_referral_traffic WHERE datetime > $start_date AND datetime < $end_date $show GROUP BY landing_page ORDER BY count DESC LIMIT 0, 5";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$affiliate_id = $row['affiliate_id'];
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members WHERE id=$affiliate_id"));
				$affiliate_user = $get_affiliate['fullname'];
				echo '<tr>
						<td><a href="affiliate-stats?a='.$affiliate_id.'">'; if($affiliate_user!=''){echo $affiliate_user;}else{echo 'No Affiliate';} echo '</a></td>
						<td>'.$row['landing_page'].'</td>
					</tr>';
			}
		}
}

function top_referring_affiliates($start_date, $end_date, $affiliate_filter){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	if(isset($affiliate_filter)){$show = ' AND affiliate_id='.$affiliate_filter.'';}
	$query = "SELECT SUM(cpc_earnings) as total_cpc, landing_page, datetime, affiliate_id, COUNT(*) as count FROM ap_referral_traffic WHERE datetime > $start_date AND datetime < $end_date $show GROUP BY affiliate_id ORDER BY count DESC LIMIT 0, 5";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$affiliate_id = $row['affiliate_id'];
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT fullname, email FROM ap_members WHERE id=$affiliate_id"));
				$affiliate_user = $get_affiliate['fullname'];
				$email = $get_affiliate['email'];
				//CHECK IF CPC ENABLED
				$get_cpc_on = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT cpc_on FROM ap_other_commissions WHERE id=1"));
				$cpc_on = $get_cpc_on['cpc_on'];
				//MULTI CURRENCY

				echo '<tr class="top-list">
						<td>'; avatar($affiliate_id); echo '<a href="affiliate-stats?a='.$affiliate_id.'">'.$affiliate_user.'</a></td>
						<td>'.$row['count'].'</td>';
						if($cpc_on=='1'){echo '<td>';
						if($row['total_cpc']=='0'){echo $row['total_cpc'];}
						 else { echo $row['total_cpc']; }
						echo '</td>';}
				echo '</tr>';
			}
		}
}

function my_referral_table($start_date, $end_date, $owner){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	$query = "SELECT * FROM ap_referral_traffic WHERE affiliate_id=$owner AND datetime > $start_date AND datetime < $end_date ORDER BY datetime DESC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$affiliate_id = $row['affiliate_id'];
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members WHERE id=$affiliate_id"));
				$affiliate_user = $get_affiliate['fullname'];
				//CHECK IF CPC ENABLED
				$get_cpc_on = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT cpc_on FROM ap_other_commissions WHERE id=1"));
				$cpc_on = $get_cpc_on['cpc_on'];
				//MULTI CURRENCY

				echo '<tr>
						<td>'.$row['ip'].'</td>
						<td>'.$row['agent'].'</td>
						<td>'.$row['host_name'].'</td>
						<td>'.$row['landing_page'].'</td>';
						if($cpc_on=='1'){echo '<td>';
						if($row['cpc_earnings']=='0'){echo '0.00';}
						 else { echo $row['cpc_earnings']; }
						echo '</td>';}
						echo '<td>'.$row['datetime'].'</td>
					</tr>';
			}
		}
}

function recent_referrals($owner){
	include('auth/db-connect.php');

	$query = "SELECT * FROM ap_referral_traffic WHERE affiliate_id=$owner ORDER BY datetime DESC LIMIT 0, 4";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$affiliate_id = $row['affiliate_id'];
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members WHERE id=$affiliate_id"));
				$affiliate_user = $get_affiliate['fullname'];
				//CHECK IF CPC ENABLED
				$get_cpc_on = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT cpc_on FROM ap_other_commissions WHERE id=1"));
				$cpc_on = $get_cpc_on['cpc_on'];
				//MULTI CURRENCY

				echo '<tr>
						<td>'.$row['ip'].'</td>
						<td>'.$row['landing_page'].'</td>';
						if($cpc_on=='1'){echo '<td>';
						if($row['cpc_earnings']=='0'){echo '0.00';}
						 else { echo $row['cpc_earnings']; }
						echo '</td>';}
						echo '<td>'.$row['datetime'].'</td>
					</tr>';
			}
		}
}
/* ===========================================
	SALES FUNCTIONS
   ========================================= */
function sales_table($admin_user='',$start_date, $end_date, $affiliate_filter){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	if(isset($affiliate_filter)){$show = ' AND affiliate_id='.$affiliate_filter.'';}
	$query = "SELECT * FROM ap_earnings WHERE datetime > $start_date AND datetime < $end_date $show ORDER BY datetime DESC";
	$query = $mysqli->real_escape_string($query);
	$i=0;
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
					$i++;
				$affiliate_id = $row['affiliate_id'];

						$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members WHERE id=$affiliate_id"));

				$affiliate_user = $get_affiliate['fullname'];
				$email= $row['email'];
					$IP= $row['IP'];
				//CHECK IF RC ENABLED
				$get_rc_on = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT rc_on FROM ap_other_commissions WHERE id=1"));
				$rc_on = $get_rc_on['rc_on'];
				//MULTI CURRENCY

				echo '<tr>
						<td>'.$i.'</td> <td><a href="affiliate-stats?a='.$affiliate_id.'">';
						if($affiliate_user!=''){echo $affiliate_user;}else{echo 'No Affiliate';} echo '</a></td>

						<td>'; if($email!=''){echo $email;}else{echo 'No email';} echo '</td>
						<td>'; if($IP!=''){echo $IP;}else{echo 'No IP';} echo '</td>
						<td>'.$row['product'].'</td>
						<td>';
							if($row['void']=='1'){ echo '<span class="red">'.'0.00'.' (Refunded)</span>'; } else { echo $row['sale_amount']; }
						echo '</td>
						<td>'.$row['comission'].'%</td>
						<td>';
							if($row['void']=='1'){ echo '<span class="red">'.'0.00'.' (Refunded)</span>'; } else { echo $row['net_earnings']; }
						echo '</td>';
//						if($rc_on=='1'){
//							if($row['stop_recurring']=='1'){
//									echo '<td><span class="red">Recurring Stopped</span></td>';
//								}else {
//									if($row['recurring']=='Non-recurring' || $row['recurring']==''){ echo '<td>Non-Recurring</td>';} else {
//										$recurring_fee = $row['recurring_fee'] / 100;
//										echo '<td>'.$row['recurring'].' @ '; $mv = $row['sale_amount'] * $recurring_fee; echo $mv;
//										echo ' ('.$row['recurring_fee'].'%)</td>';
//									}
//								}
//						}
						echo '<td>'.$row['datetime'].'</td>';
//						<td>';
//							if($row['void']!='1'){ echo '
//							<form method="post" action="data/void-transaction" class="pull-left">
//								<input type="hidden" name="m" value="'.$row['id'].'">
//								<input type="hidden" name="a" value="'.$row['affiliate_id'].'">
//								<input type="hidden" name="r" value="'.pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME).'">
//								<input type="submit" class="btn btn-sm btn-inverse" value="Refund">
//							</form>';} echo '
//							<form method="post" action="data/delete-transaction" class="pull-left">
//								<input type="hidden" name="m" value="'.$row['id'].'">
//								<input type="hidden" name="a" value="'.$row['affiliate_id'].'">
//								<input type="hidden" name="r" value="'.pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME).'">
//								<input type="submit" class="btn btn-sm btn-danger" value="Delete">
//							</form>
//						</td>
		echo 		'</tr>';
			}
		}
}
function sales_table_MLM($start_date, $end_date, $affiliate_filter){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);


	$get_profile[] = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT id FROM ap_members WHERE sponsor=$affiliate_filter"));

	$affiliate_filter = $get_profile['id'];

	if(isset($affiliate_filter)){$show = ' AND affiliate_id='.$affiliate_filter.'';}
	$query = "SELECT * FROM ap_earnings WHERE datetime > $start_date AND datetime < $end_date $show ORDER BY datetime DESC";
	$query = $mysqli->real_escape_string($query);
	if($result = $mysqli->query($query)){
		$num_results = mysqli_num_rows($result);
		while($row = $result->fetch_array())
		{
			$affiliate_id = $row['affiliate_id'];
			$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members WHERE id=$affiliate_id"));
			$affiliate_user = $get_affiliate['fullname'];
			//CHECK IF RC ENABLED
			$get_rc_on = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT rc_on FROM ap_other_commissions WHERE id=1"));
			$rc_on = $get_rc_on['rc_on'];
			//MULTI CURRENCY

			echo '<tr>
						<td><a href="affiliate-stats?a='.$affiliate_id.'">'; if($affiliate_user!=''){echo $affiliate_user;}else{echo 'No Affiliate';} echo '</a></td>
						<td>'.$row['product'].'</td>
						<td>';
			if($row['void']=='1'){ echo '<span class="red">'.'0.00'.' (Refunded)</span>'; } else { echo $row['sale_amount']; }
			echo '</td>
						<td>'.$row['comission'].'%</td>
						<td>';
			if($row['void']=='1'){ echo '<span class="red">'.'0.00'.' (Refunded)</span>'; } else { echo $row['net_earnings']; }
			echo '</td>';
			if($rc_on=='1'){
				if($row['stop_recurring']=='1'){
					echo '<td><span class="red">Recurring Stopped</span></td>';
				}else {
					if($row['recurring']=='Non-recurring' || $row['recurring']==''){ echo '<td>Non-Recurring</td>';} else {
						$recurring_fee = $row['recurring_fee'] / 100;
						echo '<td>'.$row['recurring'].' @ '; $mv = $row['sale_amount'] * $recurring_fee; echo $mv;
						echo ' ('.$row['recurring_fee'].'%)</td>';
					}
				}
			}
			echo '<td>'.$row['datetime'].'</td>
						<td>';
			if($row['void']!='1'){ echo '
							<form method="post" action="data/void-transaction" class="pull-left">
								<input type="hidden" name="m" value="'.$row['id'].'">
								<input type="hidden" name="a" value="'.$row['affiliate_id'].'">
								<input type="hidden" name="r" value="'.pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME).'">
								<input type="submit" class="btn btn-sm btn-inverse" value="Refund">
							</form>';} echo '
							<form method="post" action="data/delete-transaction" class="pull-left">
								<input type="hidden" name="m" value="'.$row['id'].'">
								<input type="hidden" name="a" value="'.$row['affiliate_id'].'">
								<input type="hidden" name="r" value="'.pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME).'">
								<input type="submit" class="btn btn-sm btn-danger" value="Delete">
							</form>
						</td>
					</tr>';
		}
	}
}

function recurring_sales_table($start_date, $end_date, $affiliate_filter){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	if(isset($affiliate_filter)){$show = ' AND affiliate_id='.$affiliate_filter.'';}
	$query = "SELECT * FROM ap_earnings WHERE datetime > $start_date AND datetime < $end_date $show AND recurring_fee > 0  ORDER BY datetime DESC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$affiliate_id = $row['affiliate_id'];
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members WHERE id=$affiliate_id"));
				$affiliate_user = $get_affiliate['fullname'];
				//CHECK IF RC ENABLED
				$get_rc_on = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT rc_on FROM ap_other_commissions WHERE id=1"));
				$rc_on = $get_rc_on['rc_on'];
				//MULTI CURRENCY

				echo '<tr>
						<td><a href="affiliate-stats?a='.$affiliate_id.'">'; if($affiliate_user!=''){echo $affiliate_user;}else{echo 'No Affiliate';} echo '</a></td>
						<td>'.$row['product'].'</td>
						<td>';
							if($row['void']=='1'){ echo '<span class="red">-'.$row['sale_amount'].' (Refunded)</span>'; } else { echo $row['net_earnings']; }
						echo '</td>';
						if($rc_on=='1'){
							if($row['stop_recurring']=='1'){
								echo '<td><span class="red">Recurring Stopped</span></td>';
							}else {
								if($row['recurring']=='Non-recurring' || $row['recurring']==''){ echo '<td>Non-Recurring</td>';} else {
									$recurring_fee = $row['recurring_fee'] / 100;
									echo '<td>'.$row['recurring'].' @ '; $mv = $row['sale_amount'] * $recurring_fee; echo $mv;
									echo ' ('.$row['recurring_fee'].'%)</td>';
								}
							}
						}
						echo '<td>'; if($row['last_reoccurance']=='0000-00-00 00:00:00'){ echo 'Never Reoccured';}else{echo $row['last_reoccurance'];} echo '</td>';
						echo '<td>';
							//MONTHLY RECURRING
							if($row['recurring']=='monthly'){
								if($row['last_reoccurance']=='0000-00-00 00:00:00'){
									echo date('Y-m-d', strtotime($row['datetime'] . ' +1 month'));
								}else{
									echo date('Y-m-d', strtotime($row['last_reoccurance'] . ' +1 month'));
								}
							}
							//WEEKLY RECURRING
							if($row['recurring']=='weekly'){
								if($row['last_reoccurance']=='0000-00-00 00:00:00'){
									echo date('Y-m-d', strtotime($row['datetime'] . ' +1 week'));
								}else{
									echo date('Y-m-d', strtotime($row['last_reoccurance'] . ' +1 week'));
								}
							}
							//BIWEEKLY RECURRING
							if($row['recurring']=='biweekly'){
								if($row['last_reoccurance']=='0000-00-00 00:00:00'){
									echo date('Y-m-d', strtotime($row['datetime'] . ' +2 weeks'));
								}else{
									echo date('Y-m-d', strtotime($row['last_reoccurance'] . ' +2 weeks'));
								}
							}
							//DAILY RECURRING
							if($row['recurring']=='daily'){
								if($row['last_reoccurance']=='0000-00-00 00:00:00'){
									echo date('Y-m-d', strtotime($row['datetime'] . ' +1 day'));
								}else{
									echo date('Y-m-d', strtotime($row['last_reoccurance'] . ' +1 day'));
								}
							}
						echo '</td>';
						echo '<td>'.$row['datetime'].'</td>
						<td>';
							if($row['stop_recurring']!='1'){ echo '
							<form method="post" action="data/stop-recurring" class="pull-left">
								<input type="hidden" name="m" value="'.$row['id'].'">
								<input type="submit" class="btn btn-sm btn-inverse" value="Stop Recurring">
							</form>';}else{ echo '
							<form method="post" action="data/start-recurring" class="pull-left">
								<input type="hidden" name="m" value="'.$row['id'].'">
								<input type="submit" class="btn btn-sm btn-success" value="Start Recurring">
							</form>';
							} echo '
							<form method="post" action="data/delete-recurring" class="pull-left">
								<input type="hidden" name="m" value="'.$row['id'].'">
								<input type="hidden" name="a" value="'.$row['affiliate_id'].'">
								<input type="hidden" name="r" value="'.pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME).'">
								<input type="submit" class="btn btn-sm btn-danger" value="Delete">
							</form>
						</td>
					</tr>';
			}
		}
}

function my_recurring_sales_table($start_date, $end_date, $affiliate_filter){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	if(isset($affiliate_filter)){$show = ' AND affiliate_id='.$affiliate_filter.'';}
	$query = "SELECT * FROM ap_earnings WHERE datetime > $start_date AND datetime < $end_date $show AND recurring_fee > 0  ORDER BY datetime DESC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				//CHECK IF RC ENABLED
				$get_rc_on = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT rc_on FROM ap_other_commissions WHERE id=1"));
				$rc_on = $get_rc_on['rc_on'];
				//MULTI CURRENCY

				echo '<tr>
						<td>'.$row['product'].'</td>
						<td>';
							if($row['void']=='1'){ echo '<span class="red">-'.$row['sale_amount'].' (Refunded)</span>'; } else { echo $row['sale_amount']; }
						echo '</td>';
						if($rc_on=='1'){
							if($row['stop_recurring']=='1'){
								echo '<td><span class="red">Recurring Stopped</span></td>';
							}else {
								if($row['recurring']=='Non-recurring' || $row['recurring']==''){ echo '<td>Non-Recurring</td>';} else {
									$recurring_fee = $row['recurring_fee'] / 100;
										echo '<td>'.$row['recurring'].' @ '; $mv = $row['sale_amount'] * $recurring_fee; echo $mv;
									echo ' ('.$row['recurring_fee'].'%)</td>';
								}
							}
						}
						echo '<td>'; if($row['last_reoccurance']=='0000-00-00 00:00:00'){ echo 'Never Reoccured';}else{echo $row['last_reoccurance'];} echo '</td>';
						echo '<td>';
							//MONTHLY RECURRING
							if($row['recurring']=='monthly'){
								if($row['last_reoccurance']=='0000-00-00 00:00:00'){
									echo date('Y-m-d', strtotime($row['datetime'] . ' +1 month'));
								}else{
									echo date('Y-m-d', strtotime($row['last_reoccurance'] . ' +1 month'));
								}
							}
							//WEEKLY RECURRING
							if($row['recurring']=='weekly'){
								if($row['last_reoccurance']=='0000-00-00 00:00:00'){
									echo date('Y-m-d', strtotime($row['datetime'] . ' +1 week'));
								}else{
									echo date('Y-m-d', strtotime($row['last_reoccurance'] . ' +1 week'));
								}
							}
							//BIWEEKLY RECURRING
							if($row['recurring']=='biweekly'){
								if($row['last_reoccurance']=='0000-00-00 00:00:00'){
									echo date('Y-m-d', strtotime($row['datetime'] . ' +2 weeks'));
								}else{
									echo date('Y-m-d', strtotime($row['last_reoccurance'] . ' +2 weeks'));
								}
							}
							//DAILY RECURRING
							if($row['recurring']=='daily'){
								if($row['last_reoccurance']=='0000-00-00 00:00:00'){
									echo date('Y-m-d', strtotime($row['datetime'] . ' +1 day'));
								}else{
									echo date('Y-m-d', strtotime($row['last_reoccurance'] . ' +1 day'));
								}
							}
						echo '</td>';
						echo '<td>'.$row['datetime'].'</td>
					</tr>';
			}
		}
}

function my_sales_table($start_date, $end_date, $owner){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	$query = "SELECT * FROM ap_earnings WHERE affiliate_id=$owner AND datetime > $start_date AND datetime < $end_date ORDER BY datetime DESC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				//CHECK IF RC ENABLED
				$get_rc_on = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT rc_on FROM ap_other_commissions WHERE id=1"));
				$rc_on = $get_rc_on['rc_on'];
				//MULTI CURRENCY

				echo '<tr>
						<td>'.$row['product'].'</td>';
//				echo 	'<td>';
							//if($row['void']=='1'){ echo '<span class="red">-'.$row['sale_amount'].' (Refunded)</span>'; } else { echo $row['sale_amount']; }
//						echo '</td>';
//				echo	'<td>'.$row['comission'].'%</td>
//				echo		'<td>';
//							if($row['void']=='1'){ echo '<span class="red">'.'0.00'.' (Refunded)</span>'; } else { echo $row['net_earnings']; }
//						echo '</td>';
//						if($rc_on=='1'){
//							if($row['stop_recurring']=='1'){
//									echo '<td><span class="red">Recurring Stopped</span></td>';
//								}else {
//									if($row['recurring']=='Non-recurring' || $row['recurring']==''){ echo '<td>Non-Recurring</td>';} else {
//										$recurring_fee = $row['recurring_fee'] / 100;
//										echo '<td>'.$row['recurring'].' @ '; $mv = $row['sale_amount'] * $recurring_fee; echo $mv;
//										echo ' ('.$row['recurring_fee'].'%)</td>';
//									}
//								}
//						}
						echo '<td>'.date("d.m.Y H:m:s", strtotime($row['datetime'])).'</td>
					</tr>';
			}
		}
}

function recurring_history_table(){
	include('auth/db-connect.php');
	$query = "SELECT * FROM ap_recurring_history ORDER by id DESC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$affiliate_id = $row['affiliate_id'];
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members WHERE id=$affiliate_id"));
				$affiliate_user = $get_affiliate['fullname'];
				//GET PRODUCT NAME
				$transaction_id = $row['transaction_id'];
				$get_product = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_earnings WHERE id=$transaction_id"));
				$product = $get_product['product'];
				//MULTI CURRENCY

				echo '<tr>
						<td>'; if($affiliate_user!=''){echo $affiliate_user;}else{echo 'No Affiliate';} echo '</td>
						<td>'.$product.'</td>
						<td>'.$row['recurring_earnings'].'</td>
						<td>'.$row['datetime'].'</td>
					</tr>';
			}
		}
}

function recent_sales_table(){
	include('auth/db-connect.php');
	$query = "SELECT * FROM ap_earnings ORDER by datetime DESC LIMIT 0, 15";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$affiliate_id = $row['affiliate_id'];
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members WHERE id=$affiliate_id"));
				$affiliate_user = $get_affiliate['fullname'];
				//CHECK IF RC ENABLED
				$get_rc_on = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT rc_on FROM ap_other_commissions WHERE id=1"));
				$rc_on = $get_rc_on['rc_on'];
				//MULTI CURRENCY

				echo '<tr>
						<td><a href="affiliate-stats?a='.$affiliate_id.'">'; if($affiliate_user!=''){echo $affiliate_user;}else{echo 'No Affiliate';} echo '</a></td>
						<td>'.$row['product'].'</td>';
				echo '<td>';
							if($row['void']=='1'){ echo '<span class="red">'.'0.00'.' (Refunded)</span>'; } else { echo $row['sale_amount']; }
						echo '</td>';
				echo	'<td>'.$row['comission'].'%</td>
						<td>';
							if($row['void']=='1'){ echo '<span class="red">'.'0.00'.' (Refunded)</span>'; } else { echo $row['net_earnings']; }
						echo '</td>';
//						if($rc_on=='1'){
//							if($row['stop_recurring']=='1'){
//									echo '<td><span class="red">Recurring Stopped</span></td>';
//								}else {
//									if($row['recurring']=='Non-recurring' || $row['recurring']==''){ echo '<td>Non-Recurring</td>';} else {
//										$recurring_fee = $row['recurring_fee'] / 100;
//										echo '<td>'.$row['recurring'].' @ '; $mv = $row['sale_amount'] * $recurring_fee; echo $mv;
//										echo ' ('.$row['recurring_fee'].'%)</td>';
//									}
//								}
//						}
						echo '<td>'.$row['datetime'].'</td>

					</tr>';
			}
		}
}

function my_recent_sales_table($owner){
	include('auth/db-connect.php');
	$query = "SELECT * FROM ap_earnings WHERE affiliate_id=$owner ORDER by datetime DESC LIMIT 0, 15";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$affiliate_id = $row['affiliate_id'];
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members WHERE id=$affiliate_id"));
				$affiliate_user = $get_affiliate['fullname'];
				//CHECK IF RC ENABLED
				$get_rc_on = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT rc_on FROM ap_other_commissions WHERE id=1"));
				$rc_on = $get_rc_on['rc_on'];
				//MULTI CURRENCY

				echo '<tr>
						<td>'.$row['product'].'</td>';
//						<td>';
//							if($row['void']=='1'){ echo '<span class="red">-'.$row['sale_amount'].' (Refunded)</span>'; } else { echo $row['sale_amount']; }
//						echo '</td>';
						echo '<td>'.$row['comission'].'%</td>
						<td>';
							if($row['void']=='1'){ echo '<span class="red">-'.'0.00'.' (Refunded)</span>'; } else { echo $row['net_earnings']; }
						echo '</td>';
//						if($rc_on=='1'){
//							if($row['stop_recurring']=='1'){
//									echo '<td><span class="red">Recurring Stopped</span></td>';
//								}else {
//									if($row['recurring']=='Non-recurring' || $row['recurring']==''){ echo '<td>Non-Recurring</td>';} else {
//										$recurring_fee = $row['recurring_fee'] / 100;
//										echo '<td>'.$row['recurring'].' @ '; $mv = $row['sale_amount'] * $recurring_fee; echo $mv;
//										echo ' ('.$row['recurring_fee'].'%)</td>';
//									}
//								}
//						}
						echo '<td>'.$row['datetime'].'</td>
					</tr>';
			}
		}
}

/* ===========================================
	LEADS FUNCTION
   ========================================= */
function leads_table(){
	include('auth/db-connect.php');
	$query = "SELECT * FROM ap_leads ORDER BY id DESC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$affiliate = $row['affiliate_id'];
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT fullname FROM ap_members WHERE id=$affiliate"));
				$affiliate_name = $get_affiliate['fullname'];
				if($affiliate_name==''){$affiliate_name = 'No Referring Affiliate';}
				//MULTI CURRENCY

				echo '<tr>
						<td><a href="affiliate-stats?a='.$row['affiliate_id'].'">'.$affiliate_name.' - ID: '.$row['affiliate_id'].'</a></td>
						<td>'.$row['fullname'].'</td>
						<td>'.$row['email'].'</td>
						<td>'.$row['phone'].'</td>
						<td>'.$row['message'].'</td>
						<td>'.$row['epl'].'</td>
						<td>';if($row['converted']=='1'){echo '<span class="green">Converted</span>';}else{echo'<span class="red">Not Converted</span>';} echo '</td>
						<td>'.$row['datetime'].'</td>
						<td>
							<form method="post" action="data/mark-converted" class="pull-left">
								<input type="hidden" name="m" value="'.$row['id'].'">
								<input type="submit" class="btn btn-sm btn-primary" value="Converted">
							</form>
							<form method="post" action="data/delete-lead" class="pull-left">
								<input type="hidden" name="m" value="'.$row['id'].'">
								<input type="submit" class="btn btn-sm btn-danger" value="Delete">
							</form>
						</td>
					</tr>';
			}
		}
}

function my_leads_table($owner){
	include('auth/db-connect.php');
	$query = "SELECT * FROM ap_leads WHERE affiliate_id=$owner ORDER BY id DESC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				//MULTI CURRENCY

				echo '<tr>
								<td>ID: '.$row['id'].'</td>
								<td>'.$row['epl'].'</td>
								<td>';if($row['converted']=='1'){echo '<span class="green">Converted</span>';}else{echo'<span class="red">Not Converted</span>';} echo '</td>
								<td>'.$row['datetime'].'</td>
							</tr>';
			}
		}
}
/* ===============================

/* ===========================================
	BANNERS FUNCTIONS
   ========================================= */
function check_emails($owner)
{
	include('auth/db-connect.php');
	$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT emails FROM ap_members WHERE id=$owner"));
	$emails_enable = $get_affiliate['emails'];

	if($emails_enable==1)
		$ok=1;
	else
		$ok=0;


	return $ok;

}

function get_banner($banner_id){

	include('auth/db-connect.php');
	$query = "SELECT * FROM ap_banners WHERE id = $banner_id ";
	$query = $mysqli->real_escape_string($query);

	if($result = $mysqli->query($query)){

		$row = $result->fetch_array();

		return $row;
	}


	return false;
}

function banner_table($owner, $domain_path, $main_url, $admin_user){
	//PRIKAZUVANJE NA IFRAME BANNER
	// echo '<tr>
	// 					<td><img src="'.$main_url.'/affiliate/assets/img/iframe.PNG" class="banners"></td>';
	// echo '<td>';
	// echo 'Iframe Banner';
	// echo '</td>';
	// echo '<td><textarea class="banner-code" style="height:100px;"><iframe width="300px" scrolling="no" height="250px" frameborder="no" style="border-width:0px; background:#FFF;" marginheight="0" marginwidth="0" src="http://'.$_SERVER['HTTP_HOST'].'/services/banner_optimized-'.$owner.'.html"></iframe></textarea></td>
	// 				<td></td></tr>';

	include('auth/db-connect.php');
	$query = "SELECT * FROM ap_banners ORDER BY id DESC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){

			while($row = $result->fetch_array())
				{
				echo '<tr>
						<td><img src="data/banners/'.$row['filename'].'?x='.time().'" class="banners"></td>';
						echo '<td>';
						if($row['adsize']=='1'){ echo 'Mobile - leaderboard (320x50)';}
						if($row['adsize']=='2'){ echo 'Mobile - Large Banner (320x100)';}
						if($row['adsize']=='3'){ echo 'Medium Rectange (300x250)';}
						if($row['adsize']=='4'){ echo 'Rectange (180x150)';}
						if($row['adsize']=='5'){ echo 'Wide Skyscraper (160x600)';}
						if($row['adsize']=='6'){ echo 'Leaderboard (728x90)';}
						echo '</td>';
						echo '<td><textarea rows="5" class="banner-code"><a  target="_blank" href="'.$main_url.'/banner/index?banner_id='.$row['id'].'&ref='.$owner.'"><img src="'.$main_url.'/affiliate/data/banners/'.$row['filename'].'"></a></textarea>'.($admin_user == '1' ? '<br/><br/>Линк на банер: <a target="_blank" href="'.$row['banner_link'].'">'.$row['banner_link'].'</a>' : '').'
							  </td>
							  
						'; 
							/*							
							if($admin_user=='1'){ echo '
							<form method="post" action="data/delete-file">
								<input type="hidden" name="f" value="'.$row['id'].'">
								<input type="submit" class="btn btn-sm btn-danger" value="Delete">
							</form> ';}
							*/

							if($admin_user=='1')
							{ 
								echo '<td>
										<a href="'.$main_url.'/affiliate/edit-banner?banner_id='.$row['id'].'">Смени</a>
									  </td>
								';
							}


							echo '
					</tr>';
			}

		}



}
function emails_table($owner, $domain_path, $main_url, $admin_user){
	include('auth/db-connect.php');
	if($owner=='2')
		$query = "SELECT * FROM ap_emails";
	else
		$query = "SELECT * FROM ap_emails where affiliate_id=$owner";

	$query = $mysqli->real_escape_string($query);
	if($result = $mysqli->query($query)){
		while($row = $result->fetch_array())
		{
			//izvadi info za memberot
			$member=$row['affiliate_id'];
			$q1 = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members where id=$member"));
			$memberName = $q1['fullname'];

			//handle-uvanje na email adresite
			$cnt_emails = explode("\r\n", $row['emails']);

			echo '<tr><td>'.count($cnt_emails).'</td>
						<td><a href="affiliate-stats?a='.$row['affiliate_id'].'">'.$memberName.'</a></td>
						<td style="height:250px"><textarea style="height:100%">'.$row['emails'].'</textarea></td>
						<td>'.date("d.m.Y H:m:s", strtotime($row['Date'])).'</td>
						<td>
						';
			echo '					<form class="form-horizontal" method="post" action="../affiliate/data/generate_email_full_content" target="_blank">
										<input type="hidden" name="owner" id="owner" value='.$row['affiliate_id'].' style="display:none;">
										<input type="submit" class="btn btn-default" value="Превземи Full Content">
									</form>
									<br>
									<form class="form-horizontal" method="post" action="../affiliate/data/generate_email_bestsellers" target="_blank">
										<input type="hidden" name="owner" id="owner" value='.$row['affiliate_id'].' style="display:none;">
										<input type="submit" class="btn btn-default" value="Превземи најпродавани">
									</form>
									<br>
									<form class="form-horizontal" method="post" action="../affiliate/data/generate_email_main_offers" target="_blank">
										<input type="hidden" name="owner" id="owner" value='.$row['affiliate_id'].' style="display:none;">
										<input type="submit" class="btn btn-default" value="Превземи Главни понуди">
									</form>
									<br>
									<form class="form-horizontal" method="post" action="../affiliate/data/generate_email_category" target="_blank">
										<input type="hidden" name="owner" id="owner" value='.$row['affiliate_id'].' style="display:none;">
										<input type="submit" class="btn btn-default" value="Превземи категорија 1">
									</form>
						</td>';
			echo '</tr>';
		}
	}
}
function my_emails_table($owner, $domain_path, $main_url, $admin_user){
	include('auth/db-connect.php');
	$query = "SELECT * FROM ap_emails where affiliate_id=$owner";
	$query = $mysqli->real_escape_string($query);
	$i=0;
	if($result = $mysqli->query($query)){
		while($row = $result->fetch_array())
		{
			$i++;
			$order=';';
			$replace="<br>";
			$newstr = str_replace($order, $replace, $row['emails']);
			echo '<tr><td>'.$i.'</td>
						<td>'.$newstr.'</td>
						<td>'.$row['Date'].'</td>';
			echo '</tr>';
		}
	}
}

function generate_iframe($owner)
{
	include("services/banner-aff.php?aff_id=".$owner."");
}
/* ===========================================
	COMMISSION FUNCTIONS
   ========================================= */
function commission_table($owner, $domain_path, $main_url){
	include('auth/db-connect.php');
	$query = "SELECT * FROM ap_commission_settings ORDER BY sales_from DESC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				//MULTI CURRENCY

				echo '<tr>
						<td>'.$row['sales_from'].'</td>
						<td>'.$row['sales_to'].'</td>
						<td>'.$row['percentage'].'%</td>
						<td>
							<form method="post" action="data/delete-commission-level">
								<input type="hidden" name="m" value="'.$row['id'].'">
								<input type="submit" class="btn btn-sm btn-danger" value="Delete">
							</form>
						</td>
					</tr>';
			}
		}
}

function highest_level(){
	include('auth/db-connect.php');
	$get_max = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT MAX(sales_to) as max FROM ap_commission_settings"));
	$max = $get_max['max'];
	echo $max;
}

function highest_level_plus(){
	include('auth/db-connect.php');
	$get_max = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT MAX(sales_to) as max FROM ap_commission_settings"));
	$max = $get_max['max'] + 1;
	echo $max.'.00';
}

function default_commission(){
	include('auth/db-connect.php');
	$get_dc = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT default_commission FROM ap_settings"));
	$dc = $get_dc['default_commission'];
	echo $dc;
}

function cpc_on(){
	include('auth/db-connect.php');
	$get_cpc = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT cpc_on FROM ap_other_commissions WHERE id=1"));
	$cpc_on = $get_cpc['cpc_on'];
	return $cpc_on;
}

function lc_on(){
	include('auth/db-connect.php');
	$get_lc = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT lc_on FROM ap_other_commissions WHERE id=1"));
	$lc_on = $get_lc['lc_on'];
	return $lc_on;
}

function epc(){
	include('auth/db-connect.php');
	$get_epc = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT epc FROM ap_other_commissions WHERE id=1"));
	$epc = $get_epc['epc'];
	echo $epc;
}

function epl(){
	include('auth/db-connect.php');
	$get_epc = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT epl FROM ap_other_commissions WHERE id=1"));
	$epl = $get_epc['epl'];
	echo $epl;
}

function rc_on(){
	include('auth/db-connect.php');
	$get_rc = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT rc_on FROM ap_other_commissions WHERE id=1"));
	$rc_on = $get_rc['rc_on'];
	return $rc_on;
}

function sv_on(){
	include('auth/db-connect.php');
	$get_sv = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT sv_on FROM ap_other_commissions WHERE id=1"));
	$sv_on = $get_sv['sv_on'];
	return $sv_on;
}

function mt_on(){
	include('auth/db-connect.php');
	$get_mt = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT mt_on FROM ap_other_commissions WHERE id=1"));
	$mt_on = $get_mt['mt_on'];
	return $mt_on;
}
/* ===========================================
	PAYOUT FUNCTIONS
   ========================================= */
function payout_table($start_date, $end_date){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	$query = "SELECT * FROM ap_payouts WHERE datetime > $start_date AND datetime < $end_date ORDER BY datetime DESC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$affiliate_id = $row['affiliate_id'];
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members WHERE id=$affiliate_id"));
				$affiliate_user = $get_affiliate['fullname'];
				$email = $get_affiliate['email'];
				//MULTI CURRENCY

				echo '<tr>
<td>';
					if($row['payment_method']=='2' || $row['payment_method']=='4' || $row['payment_method']=='5'){ echo '
							<form action="payouts-additional" method="get" class="pull-left" target="_blank">
								<input type="hidden" name="p" value="'.$row['id'].'">
								<button type="submit" class="btn btn-sm btn-primary">View Details</button>
							</form>';}
					if($row['status']=='0' && $row['payment_method']=='1'){ echo '
							<form action="https://www.paypal.com/cgi-bin/webscr" method="post" class="pull-left" target="_blank">
								<input type="hidden" name="cmd" value="_xclick">
								<input type="hidden" name="item_number" value="'.$row['id'].'">
								<input type="hidden" name="item_name" value="Affiliate Payment">
								<input type="hidden" name="amount" value="'.$row['amount'].'">
								<input type="hidden" name="business" value="'.$email.'">
								<button type="submit" class="btn btn-sm btn-primary">Redirect to <i class="fa-paypal"></i></button>
							</form>';}
					if($row['status']=='0'){echo '
							<form method="post" action="data/mark-paid" class="pull-left">
								<input type="hidden" name="m" value="'.$row['id'].'">
								<input type="submit" class="btn btn-sm btn-success" value="Mark Paid">
							</form>';} echo '
							<form method="post" action="data/delete-payout" class="pull-left">
								<input type="hidden" name="m" value="'.$row['id'].'">
								<input type="submit" class="btn btn-sm btn-danger" value="Delete">
							</form>
						</td>
						<td>'; if($affiliate_user!=''){echo $affiliate_user;}else{echo 'No Affiliate';} echo '</td>
						<td>';
							if($row['payment_method']=='1'){echo 'PayPal';}
							if($row['payment_method']=='2'){echo 'Stripe';}
							if($row['payment_method']=='3'){echo 'Skrill';}
							if($row['payment_method']=='4'){echo 'Wire Transfer';}
							if($row['payment_method']=='5'){echo 'Check';}
						echo '</td>
						<td>'.$row['amount'].'</td>
						<td>'.$row['payment_email'].'</td>
						<td>';
							if($row['status']=='0'){echo '<span class="red">Payment Pending</span>';}
							if($row['status']=='1'){echo '<span class="green">Paid Request</span>';}
							if($row['status']=='2'){echo '<span class="red">Cancelled</span>';}
						echo '</td>
						<td>'.$row['datetime'].'</td>

					</tr>';
			}
		}
}

function my_payout_table($start_date, $end_date, $owner){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	$query = "SELECT * FROM ap_payouts WHERE affiliate_id=$owner AND datetime > $start_date AND datetime < $end_date ORDER BY datetime DESC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$affiliate_id = $row['affiliate_id'];
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members WHERE id=$affiliate_id"));
				$affiliate_user = $get_affiliate['fullname'];
				$email = $get_affiliate['email'];
				//MULTI CURRENCY

				echo '<tr>
						<td>'.$row['amount'].'</td>
						<td>';
							if($row['payment_method']=='1'){echo 'PayPal';}
							if($row['payment_method']=='2'){echo 'Stripe';}
							if($row['payment_method']=='3'){echo 'Skrill';}
							if($row['payment_method']=='4'){echo 'Wire Transfer';}
							if($row['payment_method']=='5'){echo 'Check';}
							if($row['payment_method']=='10'){echo  "Transfered to  Kupinapopust profil" ;}
						echo '</td>
						<td>'.$row['payment_email'].'</td>
						<td>';
							if($row['status']=='0'){echo '<span class="red">Payment Pending</span>';}
							if($row['status']=='1'){echo '<span class="green">Transfered</span>';}
							if($row['status']=='2'){echo '<span class="red">Cancelled</span>';}
						echo '</td>
						<td>'.$row['datetime'].'</td>';
//						<td>';if($row['status']=='0'){ echo '
//							<form method="post" action="data/cancel-payout" class="pull-left">
//								<input type="hidden" name="m" value="'.$row['id'].'">
//								<input type="submit" class="btn btn-sm btn-danger" value="Cancel Request">
//							</form>';} echo '
//						</td>
					echo '</tr>';
			}
		}
}

function available_payment(){
	include('auth/db-connect.php');
	$query = "SELECT * FROM ap_settings WHERE id=1";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				if($row['paypal']=='1'){echo '<option value="1">PayPal</option>';}
				if($row['stripe']=='1'){echo '<option value="2">Stripe</option>';}
				if($row['skrill']=='1'){echo '<option value="3">Skrill</option>';}
				if($row['wire']=='1'){echo '<option value="4">Wire Transfer</option>';}
				if($row['checks']=='1'){echo '<option value="5">Check</option>';}
			}
		}
}

function payouts_additional($payout_id){
	include('auth/db-connect.php');
	$query = "SELECT * FROM ap_payouts WHERE id=$payout_id";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$affiliate_id = $row['affiliate_id'];
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members WHERE id=$affiliate_id"));
				$affiliate_user = $get_affiliate['fullname'];

				if($row['payment_method']=='2' || $row['payment_method']=='4'){
				echo '<li>Full Name: '.$affiliate_user.'</li>
				<li>Bank Name: '.$row['bn'].'</li>
				<li>Routing #'.$row['rn'].'</li>
				<li>Account #'.$row['an'].'</li>';}
				if($row['payment_method']=='5'){
				echo '<li>Full Name: '.$affiliate_user.'</li>
				<li>Street: '.$row['street'].'</li>
				<li>City: '.$row['city'].'</li>
				<li>Zip: '.$row['zip'].'</li>';}
			}
		}
}
/* ===========================================
	MULTI-TIERS FUNCTION
   ========================================= */
function tier_levels(){
	include('auth/db-connect.php');
	$get_tiers = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_other_commissions"));
	$tier2 = $get_tiers['tier2']; $tier3 = $get_tiers['tier3']; $tier4 = $get_tiers['tier4'];
	$tier5 = $get_tiers['tier5']; $tier6 = $get_tiers['tier6']; $tier7 = $get_tiers['tier7'];
	$tier8 = $get_tiers['tier8']; $tier9 = $get_tiers['tier9']; $tier10 = $get_tiers['tier10'];
	echo '
		<li>Level 2 (Affiliate\'s Sponsor) = <input type="text" name="tier2" size="2" value="'.$tier2.'">%</li>
		<li>Level 3 (Level 2\'s Sponsor) = <input type="text" name="tier3" size="2" value="'.$tier3.'">%</li>
		<li>Level 4 (Level 3\'s Sponsor) = <input type="text" name="tier4" size="2" value="'.$tier4.'">%</li>
		<li>Level 5 (Level 4\'s Sponsor) = <input type="text" name="tier5" size="2" value="'.$tier5.'">%</li>
		<li>Level 6 (Level 5\'s Sponsor) = <input type="text" name="tier6" size="2" value="'.$tier6.'">%</li>
		<li>Level 7 (Level 6\'s Sponsor) = <input type="text" name="tier7" size="2" value="'.$tier7.'">%</li>
		<li>Level 8 (Level 7\'s Sponsor) = <input type="text" name="tier8" size="2" value="'.$tier8.'">%</li>
		<li>Level 9 (Level 8\'s Sponsor) = <input type="text" name="tier9" size="2" value="'.$tier9.'">%</li>
		<li>Level 10 (Level 9\'s Sponsor) = <input type="text" name="tier10" size="2" value="'.$tier10.'">%</li>';
}

function mt_table($start_date, $end_date){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	$query = "SELECT transaction_id, datetime, COUNT(*) as total_levels FROM ap_multi_tier_transactions WHERE datetime > $start_date AND datetime < $end_date GROUP BY transaction_id ORDER BY datetime DESC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				//GET PRODUCT NAME
				$transaction_id = $row['transaction_id'];
				$get_product = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_earnings WHERE id=$transaction_id"));
				$product = $get_product['product'];
				echo '<tr>
						<td>MT-'.$row['transaction_id'].'</td>
						<td>'.$product.'</td>
						<td>'.$row['total_levels'].' Payments <a href="view-mt-payments?tid='.$row['transaction_id'].'" class="btn btn-xs btn-default">View Transactions</a> </td>
						<td>'.$row['datetime'].'</td>
					</tr>';
			}
		}
}

function mt_payments_table($transaction_id){
	include('auth/db-connect.php');

	$query = "SELECT * FROM ap_multi_tier_transactions WHERE transaction_id=$transaction_id ORDER BY id ASC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$affiliate_id = $row['affiliate_id'];
				$get_affiliate = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_members WHERE id=$affiliate_id"));
				$affiliate_user = $get_affiliate['fullname'];
				//MULTI CURRENCY

				echo '<tr>
						<td>'; if($affiliate_user!=''){echo $affiliate_user;}else{echo 'No Affiliate';} echo '</td>
						<td>Level '.$row['tier'].'</td>
						<td>'.$row['commission'].'%</td>
						<td>'; if($row['reversed']=='1'){echo '<span class="red">-'.$row['mt_earnings'].' (Reversed)';}else{echo $row['mt_earnings'];} echo '</td>
						<td>'.$row['datetime'].'</td>
						<td>';
							if($row['reversed']!='1'){ echo '
							<form method="post" action="data/reverse-mt-transaction" class="pull-left">
								<input type="hidden" name="i" value="'.$row['id'].'">
								<input type="hidden" name="a" value="'.$row['affiliate_id'].'">
								<input type="hidden" name="t" value="'.$row['transaction_id'].'">
								<input type="submit" class="btn btn-sm btn-inverse" value="Reverse Transaction">
							</form>';} echo '
							<form method="post" action="data/delete-mt" class="pull-left">
								<input type="hidden" name="i" value="'.$row['id'].'">
								<input type="hidden" name="t" value="'.$row['transaction_id'].'">
								<input type="submit" class="btn btn-sm btn-danger" value="Delete">
							</form>
							</td>
					</tr>';
			}
		}
}

function my_mt_payments_table($owner){
	include('auth/db-connect.php');

	$query = "SELECT * FROM ap_multi_tier_transactions WHERE affiliate_id=$owner ORDER BY id ASC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				//GET PRODUCT NAME
				$transaction_id = $row['transaction_id'];
				$get_product = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_earnings WHERE id=$transaction_id"));
				$product = $get_product['product'];
				//MULTI CURRENCY

				echo '<tr>
						<td>'.$product.'</td>
						<td>Level '.$row['tier'].'</td>
						<td>'.$row['commission'].'%</td>
						<td>'; if($row['reversed']=='1'){echo '<span class="red">-$'.$row['mt_earnings'].' (Reversed)';}else{echo $row['mt_earnings'];} echo '</td>
						<td>'.$row['datetime'].'</td>
					</tr>';
			}
		}
}
/* ===========================================
	TOTALS FUNCTION
   ========================================= */

function total_leads(){
	include('auth/db-connect.php');
	$sql = "SELECT * FROM ap_leads";
	$result = $mysqli->query($sql);
	$num_affiliates = $result->num_rows;
	if($num_affiliates==''){$num_affiliates='0';}
	echo $num_affiliates;
}

function total_lead_conversions(){
	include('auth/db-connect.php');
	$sql = "SELECT * FROM ap_leads WHERE converted=1";
	$result = $mysqli->query($sql);
	$num_affiliates = $result->num_rows;
	if($num_affiliates==''){$num_affiliates='0';}
	echo $num_affiliates;
}

function total_leads_i($owner){
	include('auth/db-connect.php');
	$sql = "SELECT * FROM ap_leads WHERE affiliate_id=$owner";
	$result = $mysqli->query($sql);
	$num_affiliates = $result->num_rows;
	if($num_affiliates==''){$num_affiliates='0';}
	echo $num_affiliates;
}

function total_lead_conversions_i($owner){
	include('auth/db-connect.php');
	$sql = "SELECT * FROM ap_leads WHERE affiliate_id=$owner AND converted=1";
	$result = $mysqli->query($sql);
	$num_affiliates = $result->num_rows;
	if($num_affiliates==''){$num_affiliates='0';}
	echo $num_affiliates;
}

function total_affiliate_lead_earnings(){
	include('auth/db-connect.php');
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(epl) as total_balance FROM ap_leads"));
	$tb = $get_tb['total_balance'];
	if($tb==''){$tb='0.00';}

	echo $tb;
}

function total_affiliate_lead_earnings_i($owner){
	include('auth/db-connect.php');
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(epl) as total_balance FROM ap_leads WHERE affiliate_id=$owner"));
	$tb = $get_tb['total_balance'];
	if($tb==''){$tb='0.00';}

	echo $tb;
}

function total_affiliates(){
	include('auth/db-connect.php');
	$sql = "SELECT * FROM ap_members WHERE admin_user!=1";
	$result = $mysqli->query($sql);
	$num_affiliates = $result->num_rows;
	echo $num_affiliates;
}

function total_balance(){
	include('auth/db-connect.php');
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(balance) as total_balance FROM ap_members WHERE admin_user!=1"));
	$tb = $get_tb['total_balance'];
	if($tb==''){$tb='0.00';}

	echo $tb;
}


function total_balance_with_points(){
	include('auth/db-connect.php');
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(balance) as total_balance FROM ap_members WHERE admin_user!=1"));
	$tb = $get_tb['total_balance'];
	if($tb==''){$tb='0.00';}

	echo $tb;
}

function total_sales(){
	include('auth/db-connect.php');
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(sale_amount) as total_sales FROM ap_earnings WHERE void!=1"));
	$tb = $get_tb['total_sales'];
	if($tb==''){$tb='0';}

	echo $tb;
}



function my_total_sales($owner){
	include('auth/db-connect.php');
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(sale_amount) as total_sales FROM ap_earnings WHERE affiliate_id=$owner AND void!=1"));
	$tb = $get_tb['total_sales'];
	if($tb==''){$tb='0.00';}

	echo $tb;
}

function count_sales(){
	include('auth/db-connect.php');
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(id) as total_sales FROM ap_earnings WHERE void!=1"));
	$tb = $get_tb['total_sales'];
	if($tb==''){$tb='0';}
	echo $tb;
}

function my_count_sales($owner){
	include('auth/db-connect.php');
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(id) as total_sales FROM ap_earnings WHERE affiliate_id=$owner AND void!=1"));
	$tb = $get_tb['total_sales'];
	if($tb==''){$tb='0';}
	echo $tb;
}
function my_unique_count_sales($owner = 0){
	include('auth/db-connect.php');
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(DISTINCT `email`) as total_sales FROM ap_earnings WHERE (affiliate_id=$owner OR 0=$owner) AND void!=1"));
	$tb = $get_tb['total_sales'];
	if($tb==''){$tb='0';}
	echo $tb;
}

function total_sales_period($start_date, $end_date, $affiliate_filter){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	if(isset($affiliate_filter)){$show = ' AND affiliate_id='.$affiliate_filter.'';}
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(sale_amount) as total_sales FROM ap_earnings WHERE void!=1 AND datetime > $start_date AND datetime < $end_date $show"));
	$tb = $get_tb['total_sales'];
	if($tb==''){$tb='0.00';}

	echo $tb;
}

function total_sales_period_i($start_date, $end_date, $owner){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(sale_amount) as total_sales FROM ap_earnings WHERE affiliate_id=$owner AND void!=1 AND datetime > $start_date AND datetime < $end_date $show"));
	$tb = $get_tb['total_sales'];
	if($tb==''){$tb='0.00';}

	echo $tb;
}

function affiliate_earnings(){
	include('auth/db-connect.php');
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(net_earnings) as affiliate_earnings FROM ap_earnings WHERE void!=1"));
	$tb = $get_tb['affiliate_earnings'];
	// $get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(recurring_earnings) as recurring_earnings FROM ap_recurring_history"));
	// $re = $get_tb['recurring_earnings'];
	// $get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(epl) as lead_earnings FROM ap_leads"));
	// $le = $get_tb['lead_earnings'];
	// $tb = $ae + $re + $le;
	if($tb==''){$tb='0';}

	echo $tb;
}

function provizijaKupinapopust(){
	include('auth/db-connect.php');
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(kupinapopust_provizija) as affiliate_earnings FROM ap_earnings WHERE void!=1"));
	$tb = $get_tb['affiliate_earnings'];
	// $get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(recurring_earnings) as recurring_earnings FROM ap_recurring_history"));
	// $re = $get_tb['recurring_earnings'];
	// $get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(epl) as lead_earnings FROM ap_leads"));
	// $le = $get_tb['lead_earnings'];
	// $tb = $ae + $re + $le;
	if($tb==''){$tb='0';}

	echo $tb;
}


function affiliate_earnings_period($start_date, $end_date, $affiliate_filter){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	if(isset($affiliate_filter)){$show = ' AND affiliate_id='.$affiliate_filter.'';}
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(net_earnings) as affiliate_earnings FROM ap_earnings WHERE void!=1 AND datetime > $start_date AND datetime < $end_date $show"));
	$ae = $get_tb['affiliate_earnings'];
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(recurring_earnings) as recurring_earnings FROM ap_recurring_history WHERE datetime > $start_date AND datetime < $end_date $show"));
	$re = $get_tb['recurring_earnings'];
	$tb = $ae + $re;
	if($tb==''){$tb='0.00';}

	echo $tb;
}

function affiliate_points_period($start_date, $end_date, $affiliate_filter){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	if(isset($affiliate_filter)){$show = ' AND affiliate_id='.$affiliate_filter.'';}
	$get_tr = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(cpc_earnings) as total_cpc FROM ap_referral_traffic WHERE void=0"));
	$tr = $get_tr['total_cpc'];
	$get_payout = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(amount) as total_payout FROM ap_payouts"));
	$payout = $get_payout['total_payout'];
	$tr=$tr-$payout;
	if($tr==''){$tr='0.00';}

	echo $tr;
}

function my_affiliate_earnings($owner){
	include('auth/db-connect.php');
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(net_earnings) as affiliate_earnings FROM ap_earnings WHERE affiliate_id=$owner AND void!=1"));
	$tb = $get_tb['affiliate_earnings'];
	if($tb==''){$tb='0';}

	echo $tb;
}

function affiliate_earnings_period_i($start_date, $end_date, $owner){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(net_earnings) as affiliate_earnings FROM ap_earnings WHERE affiliate_id=$owner AND void!=1 AND datetime > $start_date AND datetime < $end_date $show"));
	$tb = $get_tb['affiliate_earnings'];
	if($tb==''){$tb='0.00';}

	echo $tb;
}

function total_recurring(){
	include('auth/db-connect.php');
	$get_tr = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(id) as total_recurring FROM ap_earnings WHERE stop_recurring=0 AND recurring_fee > 0"));
	$tr = $get_tr['total_recurring'];
	if($tr==''){$tr='0';}
	echo $tr;
}

function total_recurring_i($owner){
	include('auth/db-connect.php');
	$get_tr = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(id) as total_recurring FROM ap_earnings WHERE affiliate_id=$owner AND stop_recurring=0 AND recurring_fee > 0"));
	$tr = $get_tr['total_recurring'];
	if($tr==''){$tr='0';}
	echo $tr;
}

function total_recurring_sales_period($start_date, $end_date){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(sale_amount) as sales FROM ap_earnings WHERE stop_recurring=0 AND recurring_fee > 0 AND void!=1 AND datetime > $start_date AND datetime < $end_date $show"));
	$tb = $get_tb['sales'];
	if($tb==''){$tb='0.00';}

	echo $tb;
}

function total_recurring_sales_period_i($start_date, $end_date, $owner){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(sale_amount) as sales FROM ap_earnings WHERE affiliate_id=$owner AND stop_recurring=0 AND recurring_fee > 0 AND void!=1 AND datetime > $start_date AND datetime < $end_date $show"));
	$tb = $get_tb['sales'];
	if($tb==''){$tb='0.00';}

	echo $tb;
}

function total_referrals(){
	include('auth/db-connect.php');
	$get_tr = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(id) as total_referrals FROM ap_referral_traffic"));
	$tr = $get_tr['total_referrals'];
	if($tr==''){$tr='0';}
	echo $tr;
}

function my_total_referrals($owner){
	include('auth/db-connect.php');
	$get_tr = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(id) as total_referrals FROM ap_referral_traffic WHERE affiliate_id=$owner and cpc_earnings=0"));
	$tr = $get_tr['total_referrals'];
	if($tr==''){$tr='0';}
	echo $tr;
}


function total_cpc_earnings($owner){
	include('auth/db-connect.php');
	$get_tr = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(cpc_earnings) as total_cpc FROM ap_referral_traffic WHERE void=0"));
	$tr = $get_tr['total_cpc'];
	if($tr==''){$tr='0.00';}

	echo $tr;
}

function my_total_cpc_earnings($owner){
	include('auth/db-connect.php');
	$get_tr = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(points_new_buyer) as total_cpc FROM ap_earnings WHERE affiliate_id=$owner and points_new_buyer_transferred_flag = 0"));
	$tr = $get_tr['total_cpc'];
	if($tr==''){$tr='0';}


	echo $tr;
}

function totalPointsNewBuyerNotTransferred($owner = 0){
	include('auth/db-connect.php');
	$get_tr = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(points_new_buyer) as total_cpc FROM ap_earnings WHERE (affiliate_id=$owner OR 0 = $owner) and points_new_buyer_transferred_flag = 0"));
	$tr = $get_tr['total_cpc'];
	if($tr==''){$tr='0';}


	echo $tr;
}

function totalPointsNewBuyerTransferred($owner = 0){
	include('auth/db-connect.php');
	$get_tr = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(points_new_buyer) as total_cpc FROM ap_earnings WHERE (affiliate_id=$owner OR 0 = $owner) and points_new_buyer_transferred_flag = 1"));
	$tr = $get_tr['total_cpc'];
	if($tr==''){$tr='0';}


	echo $tr;
}

function totalPointsNewBuyerAll($owner){
	include('auth/db-connect.php');
	$get_tr = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(points_new_buyer) as total_cpc FROM ap_earnings WHERE affiliate_id=$owner"));
	$tr = $get_tr['total_cpc'];
	if($tr==''){$tr='0';}


	echo $tr;
}

function my_total_cpc_earnings_only($owner,$id){
	include('auth/db-connect.php');
	$get_tr = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT new_cpc as total_cpc FROM ap_members WHERE id=$owner"));
	$tr = $get_tr['total_cpc'];

	if($tr==''){$tr='0';}


	echo floatval($tr);
}

function total_referrals_period($start_date, $end_date, $affiliate_filter){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	if(isset($affiliate_filter)){$show = ' AND affiliate_id='.$affiliate_filter.'';}
	$get_tr = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(id) as total_referrals FROM ap_referral_traffic WHERE datetime > $start_date AND datetime < $end_date $show"));
	$tr = $get_tr['total_referrals'];
	if($tr==''){$tr='0';}
	echo $tr;
}

function total_referrals_period_i($start_date, $end_date, $owner){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	$get_tr = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(id) as total_referrals FROM ap_referral_traffic WHERE affiliate_id=$owner AND datetime > $start_date AND datetime < $end_date"));
	$tr = $get_tr['total_referrals'];
	if($tr==''){$tr='0';}
	echo $tr;
}

function active_affiliates_period($start_date, $end_date){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	$get_tr = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(DISTINCT(affiliate_id)) as active_affiliates FROM ap_referral_traffic WHERE datetime > $start_date AND datetime < $end_date"));
	$tr = $get_tr['active_affiliates'];
	if($tr==''){$tr='0';}
	echo $tr;
}

function total_pending_period($start_date, $end_date){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(amount) as pending_payments FROM ap_payouts WHERE status!=1 AND datetime > $start_date AND datetime < $end_date"));
	$tb = $get_tb['pending_payments'];
	if($tb==''){$tb='0.00';}

	echo $tb;
}

function total_paid_period($start_date, $end_date){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(amount) as pending_payments FROM ap_payouts WHERE status=1 AND datetime > $start_date AND datetime < $end_date"));
	$tb = $get_tb['pending_payments'];
	if($tb==''){$tb='0.00';}

	echo $tb;
}

function my_conversion_period_i($start_date, $end_date, $owner){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	//COUNT SALES
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(id) as sales FROM ap_earnings WHERE void!=1 AND affiliate_id=$owner AND datetime > $start_date AND datetime < $end_date"));
	$sales = $get_tb['sales'];
	//COUNT REFERRALS
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(id) as referrals FROM ap_referral_traffic WHERE affiliate_id=$owner AND datetime > $start_date AND datetime < $end_date"));
	$referrals = $get_tb['referrals'];
	$rate = $sales / $referrals * 100;
	echo number_format((float)$rate, 2, '.', '').'%';
}

function total_mt_transactions(){
	include('auth/db-connect.php');
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(id) as total_mt FROM ap_multi_tier_transactions WHERE reversed!=1"));
	$tb = $get_tb['total_mt'];
	if($tb==''){$tb='0';}
	echo $tb;
}

function total_mt_payments_period($start_date, $end_date){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(mt_earnings) as total_mt FROM ap_multi_tier_transactions WHERE reversed!=1 AND datetime > $start_date AND datetime < $end_date"));
	$tb = $get_tb['total_mt'];
	if($tb==''){$tb='0.00';}

	echo $tb;
}

function total_mt_transactions_i($owner){
	include('auth/db-connect.php');
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT COUNT(id) as total_mt FROM ap_multi_tier_transactions WHERE affiliate_id=$owner AND reversed!=1"));
	$tb = $get_tb['total_mt'];
	if($tb==''){$tb='0';}
	echo $tb;
}

function total_mt_payments_period_i($start_date, $end_date, $owner){
	include('auth/db-connect.php');
	$start_date = $start_date.'000000';
	$end_date = $end_date.'235959';
	$start_date = str_replace("-", "", $start_date);
	$end_date = str_replace("-", "", $end_date);
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(mt_earnings) as total_mt FROM ap_multi_tier_transactions WHERE affiliate_id=$owner AND reversed!=1 AND datetime > $start_date AND datetime < $end_date"));
	$tb = $get_tb['total_mt'];
	if($tb==''){$tb='0.00';}

	echo $tb;
}

function my_total_mt_payments($owner){
	include('auth/db-connect.php');
	$get_tb = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT SUM(mt_earnings) as total_mt FROM ap_multi_tier_transactions WHERE affiliate_id=$owner AND reversed!=1"));
	$tb = $get_tb['total_mt'];
	if($tb==''){$tb='0.00';}

	echo $tb;
}

/* ===========================================
	USER MANAGEMENT FUNCTION
   ========================================= */
function user_table($used_space){
	include('auth/db-connect.php');
	include('lang/'.$_SESSION['language'].'.php');
	$query = "SELECT * FROM ap_members ORDER BY fullname ASC";
	$query = $mysqli->real_escape_string($query);
		if($result = $mysqli->query($query)){
			$num_results = mysqli_num_rows($result);
			while($row = $result->fetch_array())
				{
				$member = $row['id'];
				$terms = $row['terms'];
				$a = $row['admin_user'];
				echo '<tr>
						<td><span id="fullname:'.$row['id'].'" contenteditable="true" class="editable">'.$row['fullname'].'</span></td>
						<td><span id="username:'.$row['id'].'" contenteditable="true" class="editable">'.$row['username'].'</span></td>
						<td><span id="email:'.$row['id'].'" contenteditable="true" class="editable">'.$row['email'].'</span></td>
						<td>'; if($terms=='1'){echo 'Yes';} echo '</td>
						<td>
							<form method="post" action="data/change-user-level">
								<input type="hidden" name="m" value="'.$row['id'].'">
								<select name="l" onchange="this.form.submit()">
									<option value="0" ';if($a=='0'){echo 'selected';} echo '>Affiliate</option>
									<option value="1" ';if($a=='1'){echo 'selected';} echo '>Admin User</option>
								</select>
							</form>
						</td>
						<td>
							<form method="post" action="data/delete-user">
								<input type="hidden" name="m" value="'.$row['id'].'">
								<input type="submit" class="btn btn-sm btn-danger" value="'.$lang['DELETE'].'">
							</form>
						</td>
					</tr>';
			}
		}
}


/* ===========================================
	SETTINGS FUNCTIONS
   ========================================= */
function all_settings(){
	include('auth/db-connect.php');
	$get_settings = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT meta_title, meta_description, site_title, site_email FROM ap_settings"));
	$meta_title = $get_settings['meta_title'];
	$meta_description = $get_settings['meta_description'];
	$site_title = $get_settings['site_title'];
	$site_email = $get_settings['site_email'];
	$default_commission = $get_settings['default_commission'];
	return array($meta_title, $meta_description, $site_title, $site_email, $ar);
}

function settings_form(){
	include('auth/db-connect.php');
	$get_settings = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT * FROM ap_settings"));
	$rs_meta_title = $get_settings['meta_title'];
	$rs_meta_description = $get_settings['meta_description'];
	$rs_site_title = $get_settings['site_title'];
	$rs_site_email = $get_settings['site_email'];
	$default_commission = $get_settings['default_commission'];
	$min_payout = $get_settings['min_payout'];
	$currency = $get_settings['currency_fmt'];
	$paypal = $get_settings['paypal'];
	$stripe = $get_settings['stripe'];
	$skrill = $get_settings['skrill'];
	$wire = $get_settings['wire'];
	$checks = $get_settings['checks'];
	echo '<fieldset>
	<hr><h3>Basic Settings</h3><hr>
	<div class="control-group">
		<label class="control-label" for="">Meta Title</label>
		<div class="controls">
		<input id="" name="mt" type="text" placeholder="" class="input-xlarge" value="'.$rs_meta_title.'">
		</div>
	</div>
		<div class="control-group">
			<label class="control-label" for="">Meta Description</label>
			<div class="controls">
			<input id="" name="md" type="text" placeholder="" class="input-xlarge" value="'.$rs_meta_description.'">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="">Site Title</label>
			<div class="controls">
			<input id="" name="st" type="text" placeholder="" class="input-xlarge" value="'.$rs_site_title.'">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="">Site Email</label>
			<div class="controls">
			<input id="" name="se" type="text" placeholder="" class="input-xlarge" value="'.$rs_site_email.'">
			</div>
		</div>




	</div>
	<div class="control-group">
		<label class="control-label" for=""></label>
		<div class="controls">
		<button type="submit" class="btn btn-success">Save</button>
		</div>
	</div>
	</fieldset>';
}
