<?php include_once '../auth/db-connect.php';
include('../auth/password.php');


	$account_id = filter_input(INPUT_GET, 'account_id', FILTER_SANITIZE_STRING);
	$hash = filter_input(INPUT_GET, 'hash', FILTER_SANITIZE_STRING);
	$buildHash = "";

	$query = "SELECT email, id, fullname, username, password FROM `ap_members` WHERE id= ? ";
	  if($stmt = $mysqli->prepare($query)){
	      $stmt->bind_param("i", $account_id);
	      if($stmt->execute()){
	          $stmt->store_result();
	     
	            $stmt->bind_result($email_r, $account_id_r, $fullname_r, $username_r, $password_r);
	            $stmt->fetch();
	            if ($stmt->num_rows == 1){
	              //ACCOUNT EXISTS SENT RESET PIN AND EMAIL INSTRUCTIONS
		            $salt = "kp.mk";

		            $buildHash = md5($account_id_r . $salt . $password_r);

	            }

	            if($stmt->num_rows == 0 || $buildHash != $hash)
	            {
		            	header('Location: ../reset?account_id='.$account_id.'&hash='.$hash);
		            	die();
	            }
	      }
	  }


	//DATA REQURED
	$pwd = filter_input(INPUT_POST, 'new_pass', FILTER_SANITIZE_STRING);
	$cpwd = filter_input(INPUT_POST, 'c_pass', FILTER_SANITIZE_STRING);


							
		//PIN VALIDATED
		if($pwd == $cpwd){ 
			//HASH NEW PASSWORD
			$new_pass_hash = password_hash($pwd, PASSWORD_DEFAULT, array("cost" => 10));
			//UPDATE DB
			$reset = '';
			$update_one = $mysqli->prepare("UPDATE ap_members SET password = ? WHERE id= ?"); 
			$update_one->bind_param('si', $new_pass_hash, $account_id);
			$update_one->execute();
			$update_one->close();
			
			header('Location: ../reset?account_id='.$account_id.'&hash='.$hash.'&success=1');
		}else{
			//PASSWORDS DO NOT MATCH
			header('Location: ../reset?account_id='.$account_id.'&hash='.$hash.'&error=1');
		}


	

?>