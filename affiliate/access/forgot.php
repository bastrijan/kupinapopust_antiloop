<?php 
include_once '../auth/db-connect.php';
include('../auth/password.php');
include_once '../auth/access-functions.php';

	//DATA REQURED
	$search_email = filter_input(INPUT_POST, 'se', FILTER_SANITIZE_STRING);

  //SEARCH FOR ACCOUNT MATCHING INPUT EMAIL
  $query = "SELECT email, id, fullname, username, password FROM `ap_members` WHERE email= ? ";
  if($stmt = $mysqli->prepare($query)){
      $stmt->bind_param("s", $search_email);
      if($stmt->execute()){
          $stmt->store_result();
            $email_check= "";         
            $stmt->bind_result($email_check, $account_id, $fullname, $username, $password);
            $stmt->fetch();
            if ($stmt->num_rows == 1){
              //ACCOUNT EXISTS SENT RESET PIN AND EMAIL INSTRUCTIONS
              $salt = "kp.mk";
              $hash = md5($account_id . $salt . $password);

              //PRAKJANJE NA MAIL ZA ZABORAVENA LOZINKA
              ob_start();
              require_once $_SERVER['DOCUMENT_ROOT'].'/application/views/emails/mk/passwordreset_affiliate.php';
              $forgotPasswordContent = ob_get_contents();
              ob_end_clean();
              sendEmailPHPMailer($search_email, "Kupinapopust.mk Affiliate - Промена на лозинка", $forgotPasswordContent, true);

              header('Location: ../forgot?success=1');
            }else{
              //NO EMAIL ADDRESS FOUND
              header('Location: ../forgot?error=1');
            }
      }
  }