<?php
include('auth/register.inc.php'); 
include('data/data-functions.php');
//new
// include('controller/sponsor-tracking.php');

//new
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $meta_title;?></title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/base.css" rel="stylesheet">
		<link href="assets/css/login.css" rel="stylesheet">
		<link href="assets/fonts/css/fa.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="full">

    <!-- Navigation -->
    

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <form action="index" method="post" class="form-horizontal login-form">
					<fieldset>
						<legend>Придружете се на нашата Affiliate програма</legend>

								<?php echo '<div style="color:red;">'.$error_msg.'</div>'; ?>
								<div class="control-group">
								  <label class="control-label" for="fullname">Назив на Порталот</label>
								  <div class="controls">
									<input id="textinput" name="fullname" type="text" placeholder="Назив на Порталот" class="input-xlarge"  value="<?php echo $_POST['fullname'];?>" required="required">
								  </div>
								</div>
								
								<div class="control-group">
								  <label class="control-label" for="username">Корисничко име</label>
								  <div class="controls">
									<input id="textinput" name="username" type="text" placeholder="username" class="input-xlarge" value="<?php echo $_POST['username'];?>" required>
								  </div>
								</div>
								
								<div class="control-group">
								  <label class="control-label" for="email">E-Mail адреса</label>
								  <div class="controls">
									<input id="textinput" name="email" type="email" placeholder="email@provider.com" class="input-xlarge" value="<?php echo $_POST['email'];?>" required>
								  </div>
								</div>

								<!-- Password input-->
								<div class="control-group">
								  <label class="control-label" for="passwordinput">Лозинка</label>
								  <div class="controls">
									<input id="passwordinput" name="p" type="password" placeholder="password" class="input-xlarge" value="<?php echo $_POST['qp'];?>" required>
								  </div>
								</div>
								
								<div class="control-group">
								  <label class="control-label" for="passwordinput">Потврди Лозинка</label>
								  <div class="controls">
									<input id="passwordinput" name="confirmpwd" type="password" placeholder="Confirm" class="input-xlarge" value="<?php echo $_POST['qcp'];?>" required>
								  </div>
								</div>
								
								<div class="control-group terms">
								   <div class="controls">
									<input type="checkbox" value="1" name="terms" <?php if($_POST['terms']=='1') echo 'checked';?>> Се согласувам со <a href="https://kupinapopust.mk/static/page/opstiodredbi">Општите одредби</a> на страната
								  </div>
								</div>
								
								<!-- Submit-->
								<div class="control-group">
								  <div class="controls">
									<input type="submit" class="btn btn-primary login-btn" value="Регистрирај се">
								  </div>
								</div>
						</fieldset>
				</form> 
            </div>
			<div class="col-lg-6">
                <form method="post" action="access/process_login" class="form-horizontal login-form">
								<fieldset>
									<legend>Најави се</legend>
											<?php if($_GET['logoff']=='1'){
													echo '<span class="success-text">Успешно се одјавивте</span>';
											} 
											if($_GET['error']=='1'){
													echo '<span class="red">Погрешно Корисничко име или Лозинка</span>';
											}?>
											<!-- Username -->
											<div class="control-group">
												<label class="control-label" for="textinput">Корисничко име или E-Mail адреса</label>
												<div class="controls">
												<input id="textinput" name="email" type="text" placeholder="demo" class="input-xlarge">
												</div>
											</div>

											<!-- Password input-->
											<div class="control-group">
												<label class="control-label" for="passwordinput">Лозинка</label>
												<div class="controls">
												<input id="passwordinput" name="p" type="password" placeholder="password" class="input-xlarge">
												</div>
											</div>

											<div class="control-group forgot-link">
												<a href="<?php echo $main_url; ?>/affiliate/forgot">Заборавена лозинка?</a>
											</div>

											<!-- Submit-->
											<div class="control-group">
												<div class="controls">
												<input type="submit" class="btn btn-primary login-btn" value="Најави се">
												</div>
											</div>

										<img style="max-width: 350px;min-width: 200px;width: 100%;"
											 src="assets/img/Affiliate-LOGO.jpg">

								</fieldset>
							</form> 
							<div class="affiliate-description">
								<h1>Зошто да бидете наш Affiliate партнер?</h1>
								<ul>
									<li><i class="fa fa-ok-circled2"></i> Заработете 35% провизија од секоj продаден ваучер!<li>
									<li><i class="fa fa-ok-circled2"></i> Заработете 30 kupinapopust поени од секој корисник кој првпат ќе купи на kupinapopust.mk<li>
									<!--
									<li><i class="fa fa-ok-circled2"></i> Договорете други Affiliate портали и заработете кога тие продаваат!<li>
									-->
									<li><i class="fa fa-ok-circled2"></i> Заработете предложувајќи ни нови и интересни маркетинг модели!<li>
								</ul>
								<br>
							</div>
            </div>
        </div>
    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>

</body>

</html>
