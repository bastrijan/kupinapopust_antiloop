<?php
$path = dirname(__FILE__);
$path = substr($path, 0, -10);
include($path.'/auth/db-connect.php');
$ap_tracking = 'ap_ref_tracking';
$datetime = date("Y-m-d H:i:s");

//error_reporting(-1);
//ini_set('display_errors', 'On');

//IF AFFILIATE ID IS PRESENT
if($kp_aff_program_id)
{

	$ref_id = intval($kp_aff_program_id);

	if((int) $ref_id > 0 )
	{
		//GET COMMISSION PERCENTAGE
		$get_dc = mysqli_fetch_assoc(mysqli_query($mysqli, "SELECT new_provizija FROM ap_members WHERE id= $ref_id "));

		//echo("test 1 --");

		if($get_dc)
		{
			//RECORD SALE
			$percentage = $get_dc['new_provizija'];
			$net_earnings = round(($kupinapopust_provizija * $percentage)/100);

			//echo("test -".$net_earnings.'--'.$percentage."--");


			$ip = $_SERVER['REMOTE_ADDR'];
			$recurring_period = "Non-recurring";
			$recurring_fee = 0;

			$stmt = $mysqli->prepare("INSERT INTO ap_earnings (affiliate_id, product, comission, sale_amount, kupinapopust_provizija, net_earnings, recurring, recurring_fee, datetime,email,IP,voucher_attempt_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			$stmt->bind_param('sssssssisssi', $ref_id, $product, $percentage , $sale_amount, $kupinapopust_provizija, $net_earnings, $recurring_period, $recurring_fee, $datetime, $email, $ip, $voucher_attempt_id);
			$stmt->execute();
			$ap_earnings_transaction_id = $stmt->insert_id;
			$stmt->close();


			//echo("test 2---");

			//UPDATE AFFILIATE BALANCE
			$update_one = $mysqli->prepare("UPDATE ap_members SET balance = balance + ? WHERE id = $ref_id"); 
			$update_one->bind_param('i', $net_earnings);
			$update_one->execute();
			$update_one->close();	
					


			//echo("test 3--");

		}

	}
	
}

//die("end");

if(isset($_POST['reset'])){
unset($_SESSION['ap_sale_hit']);
unset($_SESSION['saved_affiliate']);
unset($_SESSION['saved_net']);
//header("Refresh:0");
}
?>