<?php
$path = dirname(__FILE__);
$path = substr($path, 0, -10);
include_once($path.'/auth/db-connect.php');
$ap_tracking = 'ap_ref_tracking';
$ref_id = filter_input(INPUT_GET, 'ref');
$days_to_expiration = '14';
//SET A NEW COOKIE
if(isset($ref_id) && empty($_COOKIE[$ap_tracking])) {
	
	setcookie($ap_tracking, $ref_id, time() + (86400 * $days_to_expiration), '/');

		

	//RECORD REFERRAL TRAFFIC DATA
	$agent = $_SERVER['HTTP_USER_AGENT'];
	$ip = $_SERVER['REMOTE_ADDR'];
	$host_name = gethostbyaddr($_SERVER['REMOTE_ADDR']);
	$landing_page = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$datetime = date("Y-m-d H:i:s");



    $stmt = $mysqli->prepare("INSERT INTO ap_referral_traffic (affiliate_id, agent, ip, host_name, landing_page, datetime) VALUES (?, ?, ?, ?, ?, ?)");

	$stmt->bind_param('ssssss', $ref_id, $agent, $ip, $host_name, $landing_page, $datetime);
	$stmt->execute();
	$stmt->close();

	header("Refresh:0");
}
if(isset($_POST['delete'])){
setcookie("ap_ref_tracking", '', 1, '/');
header("Refresh:0");
}
