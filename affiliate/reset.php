<?php
include('auth/register.inc.php'); 
include('data/data-functions.php');
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();


$account_id = filter_input(INPUT_GET, 'account_id', FILTER_SANITIZE_STRING);
$hash = filter_input(INPUT_GET, 'hash', FILTER_SANITIZE_STRING);
$buildHash = "";
$error_msg = "";

if(!isset($_GET['error']) && !isset($_GET['success']))
{
	  $query = "SELECT email, id, fullname, username, password FROM `ap_members` WHERE id= ? ";
	  if($stmt = $mysqli->prepare($query)){
	      $stmt->bind_param("i", $account_id);
	      if($stmt->execute()){
	          $stmt->store_result();
	     
	            $stmt->bind_result($email_r, $account_id_r, $fullname_r, $username_r, $password_r);
	            $stmt->fetch();
	            if ($stmt->num_rows == 1){
	              //ACCOUNT EXISTS SENT RESET PIN AND EMAIL INSTRUCTIONS
		            $salt = "kp.mk";

		            $buildHash = md5($account_id_r . $salt . $password_r);

	            }

	            if($stmt->num_rows == 0 || $buildHash != $hash)
	            {
		            	$error_msg = 'Линкот за промена на лозинка веќе не е активен. Побарајте нов на <a href="'.$main_url.'/affiliate/forgot">страната за заборавена лозинка</a>';
	            }
	      }
	  }

}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $meta_description;?>">
    <meta name="author" content="">

    <title><?php echo $meta_title;?></title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/base.css" rel="stylesheet">
		<link href="assets/css/login.css" rel="stylesheet">
		<link href="assets/fonts/css/fa.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="full"><body>

    <!-- Page Content -->
    <div class="container">
			
        <div class="row">
						<div class="col-sm-12 col-md-6 col-md-offset-3">
						<?php if($error_msg == "") { ?>
                			<form method="post" action="data/reset-password?account_id=<?php print $account_id ; ?>&hash=<?php print $hash ; ?>" class="form-horizontal login-form">
								<fieldset>
								
											<?php 
											if(isset($_GET['error']) && $_GET['error']=='1'){
													echo '<span class="red">Внесените лозинки не се совпаѓаат</span>';
											}

											if(isset($_GET['success']) && $_GET['success']=='1'){
													echo '<span class="success-text forgot-link">Успешно ја сменивте вашата лозинка. Можете да се логирате на <a href="'.$main_url.'/affiliate/">страната за логирање</a></span>';
											} 

											?>


											<h4>
												Избор на лозинка
											</h4>
											

											<div class="control-group">
												<label class="control-label" for="textinput">Новата лозинка</label>
												<div class="controls">
												<input id="textinput" name="new_pass" type="password" placeholder="" class="input-xlarge" required>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label" for="textinput">Новата лозинка - повторно</label>
												<div class="controls">
												<input id="textinput" name="c_pass" type="password" placeholder="" class="input-xlarge" required>
												</div>
											</div>



											<!-- Submit-->
											<div class="control-group">
												<div class="controls">
												<input type="submit" class="btn btn-primary btn-block login-btn" value="Зачувај">
												</div>
											</div>



											<div class="control-group">
												<div class="controls">


												<img style="max-width: 350px;min-width: 200px;width: 100%;"
													 src="assets/img/Affiliate-LOGO.jpg">
												
												</div>
											</div>

								</fieldset>
							</form> 
						<?php } else { ?>
								<div class="form-horizontal login-form">

									<span class="red forgot-link">
										<?php print $error_msg  ?>
									</span>
									<div class="row"></div>
												<img style="max-width: 350px;min-width: 200px;width: 100%;"
													 src="assets/img/Affiliate-LOGO.jpg">
								</div>
						<?php } ?>
            </div>
        </div>
    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>

</body>

</html>
