<?php	
class DatabaseBanner
{
	private $link;
	
	function DatabaseBanner()
	{
	     /* Connecting, selecting database */
	     $this->link = mysqli_connect(HOSTM, dbUser, dbPassword, dbToUse)
		 or die("Could not connect to database ");
	     
		//set names to utf8
		 mysqli_query($this->link, "set names utf8") or die("Could not set names to utf8");

	}
	function  Execute($strSQL)
	{
		$result = mysqli_query($this->link, $strSQL) or die("Query failed : " . mysqli_error($this->link));
	    return $result;

	}
}
?>