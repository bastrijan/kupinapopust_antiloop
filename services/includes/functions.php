<?php	

  function Truncate($string, $length = 130) 
{
			
		//truncates a string to a certain char length, stopping on a word if not specified otherwise.
		if (mb_strlen ($string) > $length) {
			//limit hit!
			$string = mb_substr ($string,0,($length -3));
			
			$cnt_spaces = mb_substr_count($string, " ");
			
			if($cnt_spaces < 10)
			{
				$length -= 5;
				$string = mb_substr ($string,0,$length);
			}
			
			$string .= '...';
			
		}
		return $string;
}
	
function Redirect($pstrPath_file)
{	
	header("Location: ".$pstrPath_file); 
}


function HtmlEntitiesCust($str)
{
	return  htmlentities($str, ENT_QUOTES, "UTF-8");
}


function StripSlashesCust($str)
{
	$return = "";
	
	if (get_magic_quotes_gpc()) {
		$return = stripslashes($str);
	} else {
		$return = $str;
	}
	
	return  $return;

}
function AddSlashesCust($str)
{
	$return = "";
	
	if (!get_magic_quotes_gpc()) {
		$return = addslashes($str);
	} else {
		$return = $str;
	}
	
	return  $return;
	
}

function NewLineinHTML($pstrString)
{
	$badChars = array("\n");
	$newChars   = array("<BR>");

	$pstrString = str_replace($badChars, $newChars, $pstrString);
	return  $pstrString;
}
function NewLinefromHTML($pstrString)
{
	$badChars = array("<BR>");
	$newChars   = array("\n");

	$pstrString = str_replace($badChars, $newChars, $pstrString);
	return  $pstrString;
}
function BackSlash($pstrString)
{
	$badChars = array("\\");
	$newChars   = array("\\\\");

	$pstrString = str_replace($badChars, $newChars, $pstrString);
	return  $pstrString;
}

function SQLSafeString($pstrString)
{
	$badChars = array("'","\'");
	$newChars   = array("''","''");

	$pstrString = str_replace($badChars, $newChars, $pstrString);
	return  $pstrString;
}
function JSSafeString($pstrString)
{
	$badChars = array("'");
	$newChars   = array("\'");

	$pstrString = str_replace($badChars, $newChars, $pstrString);
	return  $pstrString;
}

function SaveFile($filename,$somecontent,$opentype)
{

	if ($opentype=="N")
	{
		$strOpen="w+";
	}
	elseif ($opentype=="E")
	{
		$strOpen="w+";
	}
	else
	{
	     return "Specified open type is wrong";
	}
	// Let's make sure the file exists and is writable first.
	if (is_writable($filename) || ($opentype=="N")) {

	   // In our example we're opening $filename in append mode.
	   // The file pointer is at the bottom of the file hence 
	   // that's where $somecontent will go when we fwrite() it.
	   if (!$handle = fopen($filename, $strOpen)) {
	         return "Cannot open file ($filename)";
	   }

	   // Write $somecontent to our opened file.
	   if (fwrite($handle, stripslashes($somecontent)) === FALSE) {
	       return "Cannot write to file ($filename)";
	   }
   

	   fclose($handle);
                   
	} 
	else 
	{
		return "The file $filename is not writable";
	}
}

function rmdir_r ( $dir, $DeleteMe = TRUE )
{
	if ( ! $dh = @opendir ( $dir ) ) return;
	while ( false !== ( $obj = readdir ( $dh ) ) )
	{
		if ( $obj == '.' || $obj == '..') continue;
		if ( ! @unlink ( $dir . '/' . $obj ) ) rmdir_r ( $dir . '/' . $obj, true );
	}
	
	closedir ( $dh );
	if ( $DeleteMe )
	{
		@rmdir ( $dir );
	}
}



?>