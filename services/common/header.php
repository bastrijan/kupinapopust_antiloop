<HTML>
	<HEAD>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">	
		
		<!-- jQuery library (served from Google) -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<!-- bxSlider Javascript file -->
		<script src="/includes/bxSlider/jquery.bxslider.min.js"></script>
		<script type="text/javascript" src="/includes/js/jquery.countdown.js"></script>
		<script type="text/javascript" src="/includes/js/jquery.countdown-mk.js"></script>
		
		
		<!-- bxSlider CSS file -->
		<link href="/includes/bxSlider/jquery.bxslider.css" rel="stylesheet" />	
		
		<link href="/includes/css/style.css" rel="stylesheet" />	
		
		<script type="text/javascript">
		  $(document).ready(function(){
		    
				$('.bxslider').bxSlider({
				  auto: true,
				  autoHover: true,
				  speed: 2000,
				  pause: 6000,
				  pager: false,
				  slideWidth: 300
				});
		  });
		</script>
	</HEAD>
	<BODY>
